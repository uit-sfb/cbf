# CBF

CBF stands for Contextual Biodata Framework and is a software platform aiming at storing, accessing, visualizing, and
promoting scientific (big-)data.
CBF is not domain-specific and can work with any kind of semi-structured data.
Relying heavily on [linked data](https://www.w3.org/standards/semanticweb/data) CBF ensures both [FAIR data](https://www.go-fair.org/fair-principles/) and opens up the world of the semantic web.

## Features

By using CBF as your data framework, you get:

- a database to store your (bid-)data: store and access in real time millions of records!
- a comprehensive [REST API](https://databasesapi.sfb.uit.no/docs/api) to access or edit data programmatically
- a role based access model to make sure only the right people can access or edit the data
- a user interface including diverse visualizations widgets such as graphs and maps
- a [Wordpress](https://wordpress.com/) plugin to easily embed CBF widgets in any Wordpress-powered web page.

![CBF](resources/Collage.png)  
*Browser, Map, Cart and Graph widgets*

## Real world use cases

CBF powers the following services (non-exhaustive list):
- [SARS-CoV-2 database](https://covid19.sfb.uit.no/sars-cov/)
- [Marine Metagenomics Portal](https://mmp2.sfb.uit.no/)
- [EBP-Nor database](https://ebp-nor.sfb.uit.no/)

## Documentation

Please, visit our [Wiki pages](https://gitlab.com/uit-sfb/cbf/-/wikis/Home) to learn more about the project.
