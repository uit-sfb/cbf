addSbtPlugin("com.github.sbt" % "sbt-native-packager" % "1.9.2") //Do not update to 1.9.3 as it breaks compatibility
addSbtPlugin("com.eed3si9n" % "sbt-buildinfo" % "0.10.0")
addSbtPlugin("com.gilcloud" % "sbt-gitlab" % "0.0.6")
addSbtPlugin("com.typesafe.sbt" % "sbt-git" % "1.0.1")
addSbtPlugin("com.typesafe.play" % "sbt-plugin" % "2.8.8")
addSbtPlugin("com.iheart" % "sbt-play-swagger" % "0.10.5-PLAY2.8")
addSbtPlugin("org.portable-scala" % "sbt-scalajs-crossproject" % "1.0.0")
addSbtPlugin("org.scala-js" % "sbt-scalajs" % "1.7.0")
addSbtPlugin("ch.epfl.scala" % "sbt-web-scalajs-bundler" % "0.20.0")
addSbtPlugin("com.typesafe.sbt" % "sbt-gzip" % "1.0.2")
addSbtPlugin("com.typesafe.sbt" % "sbt-digest" % "1.1.4")
addSbtPlugin("com.eed3si9n" % "sbt-dirty-money" % "0.2.0")
//addDependencyTreePlugin
