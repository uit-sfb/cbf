import com.typesafe.sbt.packager.docker.DockerPlugin.autoImport.{dockerBaseImage, dockerRepository, dockerUsername}
import sbtcrossproject.CrossProject

ThisBuild / scalaVersion := "2.13.8"
ThisBuild / organization     := "no.uit.sfb"
ThisBuild / organizationName := "uit-sfb"
ThisBuild / maintainer := "mma227@uit.no"

lazy val gitCommit = settingKey[String]("Git SHA")
lazy val gitRefName = settingKey[String]("Git branch or tag")
//lazy val isRelease = settingKey[Boolean]("Is release")
lazy val dockerRegistry = settingKey[String]("Docker registry")
lazy val dockerImageName = settingKey[String]("Docker image name")

useJGit

ThisBuild / gitCommit := { sys.env.getOrElse("CI_COMMIT_SHA", "unknown")}
ThisBuild / gitRefName := { sys.env.getOrElse("CI_COMMIT_REF_NAME", git.gitCurrentBranch.value) }
//ThisBuild / isRelease := { sys.env.getOrElse("CI_COMMIT_REF_PROTECTED", "false").toBoolean && sys.env.getOrElse("CI_COMMIT_TAG", "").nonEmpty}
ThisBuild / dockerRegistry := { sys.env.getOrElse("CI_REGISTRY", s"registry.gitlab.com") }
ThisBuild / dockerImageName := {
  s"${sys.env.getOrElse("CI_REGISTRY_IMAGE", s"${dockerRegistry.value}/${organizationName.value}/cbf")}"
}
ThisBuild / version := gitRefName.value //Do not use slug here as it would replace '.' with '-'

ThisBuild / scalacOptions ++= Seq(
  "-feature",
  "-deprecation"
)
ThisBuild / resolvers += "gitlab" at "https://gitlab.com/api/v4/groups/2067095/-/packages/maven"

lazy val commonDep = Seq("org.scalatest" %% "scalatest" % "3.2.11" % Test)
val pac4jVersion = "5.2.1"
val circeVersion = "0.14.1"
val scalaUtilsVer = "0.3.0"
val akkaVersion = "2.6.14" //DO NOT update 2.6.14

lazy val root = project.in(file("."))
  .settings(
    publish := {},
    publishLocal  := {}
  )
  .aggregate(cbfSharedRoot, dataToolset, databaseApi, dataWidgetsRoot)

lazy val cbfSharedRoot = project.in(file("cbfShared"))
  .aggregate(cbfShared.jvm, cbfShared.js)
  .settings(
    publish := {},
    publishLocal := {}
  )

lazy val cbfShared = crossProject(JSPlatform, JVMPlatform).in(file("cbfShared"))
.settings(
  libraryDependencies ++= Seq(
    "io.circe" %%% "circe-core" % circeVersion,
    "io.circe" %%% "circe-parser" % circeVersion,
    "io.circe" %%% "circe-generic" % circeVersion,
    "io.circe" %%% "circe-generic-extras" % circeVersion,
    "io.lemonlabs" %%% "scala-uri" % "2.3.1",
    "io.github.cquiroz" %%% "scala-java-time" % "2.1.0"
  ),
  publishConfiguration := publishConfiguration.value.withOverwrite(true), //Always allow overwrite since Scala API documentation gets published by docker:publish somehow
  useCoursier := false,
  Docker / publish := {}
)
  .jvmSettings(
    libraryDependencies ++= Seq(
      "no.uit.sfb" %% "scala-utils-common" % "0.3.0",
      "com.typesafe.scala-logging" %% "scala-logging" % "3.9.4",
      "org.json4s" %% "json4s-native" % "4.0.5",
      "org.mongodb.scala" %% "mongo-scala-driver" % "4.4.0", //DO NOT update 4.4.0
      "org.scalatest" %% "scalatest" % "3.2.9" % Test,
      "org.leadpony.justify" % "justify" % "3.1.0",
      "org.leadpony.joy" % "joy-classic" % "2.1.0",
      "com.github.blemale" %% "scaffeine" % "4.0.2" % "compile", //DO NOT update 4.0.2
      "org.scala-lang.modules" %% "scala-xml" % "1.2.0", //Do NOT update 1.2.0
      "org.scala-lang.modules" %% "scala-parallel-collections" % "1.0.0",
      "com.lightbend.akka" %% "akka-stream-alpakka-file" % "3.0.4",
      "com.typesafe.akka" %% "akka-stream" % akkaVersion
),
    buildInfoKeys := Seq[BuildInfoKey](
      name,
      version,
      scalaVersion,
      sbtVersion,
      gitCommit
    ),
    buildInfoPackage := s"${organization.value}.info.${name.value.toLowerCase.replace('-', '_')}"
  )
  .jsSettings(
    // Add JS-specific settings here
  )
  .enablePlugins(BuildInfoPlugin)

lazy val dataToolset = project.in(file("dataToolset"))
  .enablePlugins(BuildInfoPlugin, JavaAppPackaging, DockerPlugin)
  .settings(
    libraryDependencies ++= commonDep ++ Seq(
      "com.github.scopt" %% "scopt" % "4.0.1",
      "ch.qos.logback" % "logback-classic" % "1.2.11",
      "com.softwaremill.sttp.client3" %% "async-http-client-backend-future" % "3.5.2",
      "no.uit.sfb" %% "cdch4s" % "main"
    ),
    dockerLabels := Map("gitCommit" -> s"${gitCommit.value}@${gitRefName.value}"),
    Docker / dockerRepository := Some(dockerRegistry.value),
    Docker / dockerUsername := Some(dockerImageName.value.split("/").tail.mkString("/")),
    dockerBaseImage := "openjdk:11", //Do NOT add "in Docker"
    Docker / daemonUser := "sfb",
    Docker / maintainer := (ThisBuild / maintainer).value,
    publishConfiguration := publishConfiguration.value.withOverwrite(true), //Always allow overwrite since Scala API documentation gets published by docker:publish somehow
    useCoursier := false,
    buildInfoKeys := Seq[BuildInfoKey](
      name,
      version,
      scalaVersion,
      sbtVersion,
      gitCommit
    ),
    buildInfoPackage := s"${organization.value}.info.${name.value.toLowerCase.replace('-', '_')}"
    //buildInfoOptions += BuildInfoOption.BuildTime
  )
  .dependsOn(cbfShared.jvm)

lazy val databaseApi = project.in(file("databaseApi"))
  .enablePlugins(BuildInfoPlugin, JavaAppPackaging, DockerPlugin, PlayScala, SwaggerPlugin)
  .settings(
    libraryDependencies ++= commonDep ++ Seq(
      guice,
      caffeine,
      ws,
      specs2 % Test,
      "org.webjars" % "swagger-ui" % "4.1.2", //DO NOT update from 4.1.2
      "no.uit.sfb" %% "scala-utils-common" % scalaUtilsVer,
      "org.scalatestplus.play" %% "scalatestplus-play" % "5.1.0" % Test,
      "com.typesafe.play" %% "play-json" % "2.9.2", //needed at runtime for SwaggerPlugin
      "com.typesafe" % "config" % "1.4.1",
      "com.github.blemale" %% "scaffeine" % "4.0.2" % "compile", //DO NOT update 4.0.2
      //"org.apache.shiro" % "shiro-core" % "1.7.1",
      "org.pac4j" %% "play-pac4j" % "11.0.0-PLAY2.8",
      "org.pac4j" % "pac4j-mongo" % pac4jVersion exclude("com.fasterxml.jackson.core", "jackson-databind"),
      "org.pac4j" % "pac4j-http" % pac4jVersion exclude("com.fasterxml.jackson.core", "jackson-databind"),
      "org.pac4j" % "pac4j-jwt" % pac4jVersion exclude("com.fasterxml.jackson.core", "jackson-databind"),
      "org.pac4j" % "pac4j-oidc" % pac4jVersion exclude("commons-io", "commons-io") exclude("com.fasterxml.jackson.core", "jackson-databind"),
      "org.kie.modules" % "org-apache-commons-compress" % "6.5.0.Final",
      "com.typesafe.play" %% "play-mailer" % "8.0.1",
      "com.typesafe.play" %% "play-mailer-guice" % "8.0.1",
      "com.ibm.icu" % "icu4j" % "70.1"
    ),
    /*dependencyOverrides ++= Seq(
      "com.fasterxml.jackson.module" %% "jackson-module-scala" % "2.11.4",
      "com.fasterxml.jackson.core" %% "jackson-databind" % "2.11.4",
      "com.fasterxml.jackson.core" %% "jackson-core" % "2.11.4",
    ),*/
    dockerLabels := Map("gitCommit" -> s"${gitCommit.value}@${gitRefName.value}"),
    Docker / dockerRepository := Some(dockerRegistry.value),
    Docker / dockerUsername := Some(dockerImageName.value.split("/").tail.mkString("/")),
    dockerBaseImage := "openjdk:11", //Do NOT add "in Docker"
    Docker / daemonUser := "sfb",
    Docker / maintainer := (ThisBuild / maintainer).value,
    publishConfiguration := publishConfiguration.value.withOverwrite(true), //Always allow overwrite since Scala API documentation gets published by docker:publish somehow
    useCoursier := false,

    swaggerPrettyJson := true,
    swaggerV3 := true,
    swaggerDomainNameSpaces := Seq("models"),
    buildInfoKeys := Seq[BuildInfoKey](
      name,
      version,
      scalaVersion,
      sbtVersion,
      gitCommit
    ),
    buildInfoPackage := s"${organization.value}.info.${name.value.toLowerCase.replace('-', '_')}"
    //buildInfoOptions += BuildInfoOption.BuildTime
  )
  .dependsOn(cbfShared.jvm)

lazy val dataWidgetsRoot = project.in(file("dataWidgets"))
  .aggregate(dataWidgets.js, dataWidgets.jvm)
  .settings(
    publish := {},
    publishLocal := {}
  )

lazy val dataWidgets: CrossProject = crossProject(JSPlatform, JVMPlatform).in(file("dataWidgets"))
  .settings(
    libraryDependencies ++= Seq(),
    publishConfiguration := publishConfiguration.value.withOverwrite(true),
    useCoursier := false
  )
  .jvmSettings(
    libraryDependencies ++= commonDep ++ Seq(
      "com.vmunier" %% "scalajs-scripts" % "1.2.0",
      ws,
      guice,
      caffeine,
      specs2 % Test,
      "org.pac4j" %% "play-pac4j" % "11.0.0-PLAY2.8",
      "org.pac4j" % "pac4j-mongo" % pac4jVersion exclude("com.fasterxml.jackson.core", "jackson-databind"),
      "org.pac4j" % "pac4j-http" % pac4jVersion exclude("com.fasterxml.jackson.core", "jackson-databind"),
      "org.pac4j" % "pac4j-jwt" % pac4jVersion exclude("com.fasterxml.jackson.core", "jackson-databind"),
      "org.pac4j" % "pac4j-oidc" % pac4jVersion exclude("commons-io", "commons-io") exclude("com.fasterxml.jackson.core", "jackson-databind")
    ),
    dockerLabels := Map("gitCommit" -> s"${gitCommit.value}@${gitRefName.value}"), //Do NOT add in Docker
    Docker / dockerRepository := Some(dockerRegistry.value),
    Docker / dockerUsername := Some(dockerImageName.value.split("/").tail.mkString("/")),
    dockerBaseImage := "openjdk:11", //Do NOT add "in Docker"
    //Docker / dockerChmodType := DockerChmodType.UserGroupWriteExecute,
    //Docker / dockerPermissionStrategy := DockerPermissionStrategy.CopyChown,
    //Docker / daemonGroup := "0",
    Docker / daemonUser := "sfb", //"in Docker" needed for this parameter,
    Docker / maintainer := (ThisBuild / maintainer).value,
    scalaJSProjects := Seq(dataWidgets.js),
    Assets / pipelineStages := Seq(scalaJSPipeline),
    pipelineStages := Seq(digest, gzip),
    // triggers scalaJSPipeline when using compile or continuous compilation
    Compile / compile := ((Compile / compile) dependsOn scalaJSPipeline).value,
    buildInfoKeys := Seq[BuildInfoKey](
      name,
      version,
      scalaVersion,
      sbtVersion,
      gitCommit
    ),
    buildInfoPackage := s"${organization.value}.info.${name.value.toLowerCase.replace('-', '_')}",
    //buildInfoOptions += BuildInfoOption.BuildTime
  )
  .jvmConfigure(_.dependsOn(cbfShared.jvm)
    .enablePlugins(PlayScala, DockerPlugin, JavaAppPackaging, BuildInfoPlugin, WebScalaJSBundlerPlugin)
  )
  .jsSettings(
    scalaJSUseMainModuleInitializer := false,
    webpackBundlingMode := BundlingMode.LibraryAndApplication(), //https://stackoverflow.com/a/64317357/4965515
    webpack / version := "4.47.0", //Cannot update to 5+
    startWebpackDevServer / version := "3.11.2",
    webpackConfigFile := Some(baseDirectory.value / "webpack.config.js"),
    libraryDependencies ++= Seq(
      "org.scala-js" %%% "scalajs-dom" % "1.1.0",
      "com.github.japgolly.scalajs-react" %%% "extra" % "1.7.7",
      "no.uit.sfb" %%% "scalajs-bindings" % "master"
    ),
    Compile / npmDependencies ++= Seq(
      "react" -> "17.0.1",
      "react-dom" -> "17.0.1",
      "react-bootstrap" -> "1.6.3",
      "react-vis" -> "1.11.7",
      "d3-geo" -> "^1.11.7",
      "d3-scale" -> "3.2.3",
      "topojson-client" -> "^3.1.0",
      "react-icons" -> "4.3.1",
      "jsdom" -> "16.5.3",
      "leaflet" -> "1.7.1", //needed as a dependency of react-leaflet
      "react-leaflet" -> "3.2.0",
      "babel-loader" -> "7.1.5", //Needed to transpile reticent libraries (cf webpack.config.js)
      "babel-core" -> "7.0.0-bridge.0", //Needed by babel-loader
      "@babel/preset-env" -> "7.14.2",
      "@babel/core" -> "7.14.2", //Needed by @babel/preset-env
    ),
    test := {} //We disable the test as webpack breaks it since (for some reason) scalajs-bundler does not generate the scalajs-bundler/test/scalajs.webpack.config.js
    //Test / npmDependencies ++= Seq(),
    //Test / requireJsDomEnv := true,
  )
  .jsConfigure(_.dependsOn(cbfShared.js)
  .enablePlugins(ScalaJSBundlerPlugin))

//Automatic reload
Global / onChangedBuildSource := ReloadOnSourceChanges