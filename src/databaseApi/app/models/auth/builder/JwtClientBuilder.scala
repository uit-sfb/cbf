package models.auth.builder

import com.nimbusds.jose.JWSAlgorithm
import org.pac4j.http.client.direct.DirectBearerAuthClient
import org.pac4j.oidc.config.OidcConfiguration
import org.pac4j.oidc.credentials.authenticator.UserInfoOidcAuthenticator
import play.api.Configuration

object JwtClientBuilder extends ClientBuilderLike {
  lazy val direct = true
  override def name(cfg: Configuration) = s"Jwt${super.name(cfg)}"
  def apply(cfg: Configuration) = {
    val config = new OidcConfiguration
    config.setClientId(cfg.get[String]("id"))
    config.setSecret(cfg.get[String]("secret"))
    config.setDiscoveryURI(cfg.get[String]("discovery"))
    config.setPreferredJwsAlgorithm(JWSAlgorithm.RS256)
    config.setExpireSessionWithToken(false)
    //Set clock skew to 5 minutes as it can be an issue with the default value (30s)
    //config.setMaxClockSkew(300)
    config.setMaxClockSkew(3600 * 24) //Quick fix: use clock skew of 1 day to make the token valid for 1 day...
    val authenticator = new UserInfoOidcAuthenticator(config)
    val client = new DirectBearerAuthClient(authenticator)
    client.setName(name(cfg))
    client
  }
}
