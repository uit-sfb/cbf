package models.auth.builder

import org.pac4j.core.client.direct.AnonymousClient
import play.api.Configuration

object AnonymousClientBuilder extends ClientBuilderLike {
  lazy val direct = true

  override def name(cfg: Configuration) = "AnonymousClient"

  def apply(cfg: Configuration) = {
    new AnonymousClient()
  }
}
