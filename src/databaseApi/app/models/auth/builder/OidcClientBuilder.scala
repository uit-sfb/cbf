package models.auth.builder

import com.nimbusds.jose.JWSAlgorithm
import org.pac4j.oidc.client.OidcClient
import org.pac4j.oidc.config.OidcConfiguration
import play.api.Configuration

object OidcClientBuilder extends ClientBuilderLike {
  lazy val direct = false
  def apply(cfg: Configuration): OidcClient = {
    val oidcConfiguration = new OidcConfiguration()
    oidcConfiguration.setClientId(cfg.get[String]("id"))
    oidcConfiguration.setSecret(cfg.get[String]("secret"))
    oidcConfiguration.setDiscoveryURI(cfg.get[String]("discovery"))
    oidcConfiguration.setPreferredJwsAlgorithm(JWSAlgorithm.RS256)
    oidcConfiguration.setScope(cfg.get[String]("scope"))
    //This setting MUST remain commented out so that the prompt mode is automatically set to the needed value.
    //oidcConfiguration.addCustomParam("prompt", "consent") //'consent' or 'none'
    oidcConfiguration.setExpireSessionWithToken(false)
    //Set clock skew to 5 minutes as it can be an issue with the default value (30s)
    oidcConfiguration.setMaxClockSkew(300)
    val oidcClient =
      new OidcClient(oidcConfiguration)
    oidcClient.setName(name(cfg))
    oidcClient
  }
}
