package models.auth

import modules.Authorizer
import org.pac4j.core.authorization.authorizer.DefaultAuthorizers
import org.pac4j.core.profile.CommonProfile
import org.pac4j.play.scala.{SecureAction, Security}
import play.api.mvc.{ActionBuilder, AnyContent, Request}

trait SecurityLike extends Security[CommonProfile] {
  def authorizer: Authorizer

  def secure(
    clients: String
  ): SecureAction[CommonProfile, AnyContent, AuthenticatedRequest] =
    Secure(clients, authorizer.authMode)

  //Use this ActionBuilder instead of Action when no Authorization is strictly needed,
  //but nice to do in case the Authorization header is present in cases where the Action uses the authorizer
  val open = authorizer.openActionBuilder(
    secure(authorizer.clients),
    Secure("AnonymousClient", DefaultAuthorizers.NONE)
  )

  val secure: SecureAction[CommonProfile, AnyContent, AuthenticatedRequest] =
    secure(authorizer.clients)

  val secureWithoutFallback
    : SecureAction[CommonProfile, AnyContent, AuthenticatedRequest] = secure(
    authorizer.clientsWithoutDefault
  )

  //NOTE: In non-AJAX cases, all of the following methods will force log in
  //In AJAX cases (which is 99% of the API use cases), the indirect clients are NOT used.

  def reader(dbName: String): ActionBuilder[Request, AnyContent] = {
    val grantAccess = authorizer.cm.datasetConfig(dbName).grantReadAccess
    if (grantAccess == "anonymous")
      open
    else
      secure(authorizer.clients)
        .andThen(authorizer.roleActionBuilder(grantAccess)(dbName))
  }

  def writer(dbName: String): ActionBuilder[Request, AnyContent] = {
    secure(authorizer.clients)
      .andThen(authorizer.roleActionBuilder("writer")(dbName))
  }

  def owner(dbName: String): ActionBuilder[Request, AnyContent] = {
    secure(authorizer.clients)
      .andThen(authorizer.roleActionBuilder("owner")(dbName))
  }

  def admin: ActionBuilder[Request, AnyContent] = {
    secure(authorizer.clients).andThen(authorizer.adminActionBuilder)
  }
}
