package models.auth

import models.auth.User.UserId
import com.nimbusds.jose.shaded.json.JSONArray
import org.pac4j.core.profile.CommonProfile
import org.pac4j.oidc.profile.OidcProfile

import scala.util.Try

case class User(id: UserId,
                username: String = "",
                name: String = "",
                email: String = "",
                country: String = "",
                accessToken: Option[String] = None)

object User {
  type UserId = String

  def apply(p: CommonProfile): User = {
    val tokens = p match {
      case oidcProfile: OidcProfile =>
        Some(oidcProfile.getAccessToken)
      case _ => None
    }
    User(
      Option(p.getAttribute("sub").asInstanceOf[String]).getOrElse(""),
      Option(p.getUsername).getOrElse(""),
      Option(p.getDisplayName).getOrElse(""),
      Option(p.getEmail).getOrElse(""),
      Option(p.getAttribute("country").asInstanceOf[JSONArray])
        .flatMap { x =>
          Try { x.toArray()(0).asInstanceOf[String] }.toOption
        }
        .getOrElse(""),
      tokens.map {
        _.getValue
      }
    )
  }
}
