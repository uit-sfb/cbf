package models.openapi

case class Info(name: String, ver: String, git: String, `DB conn. OK`: Boolean)
