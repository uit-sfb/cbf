package models.binders

import play.api.mvc.QueryStringBindable
import org.mongodb.scala.model.Projections

import scala.util.Try

trait ProjectorLike extends BsonFormLike

//Key is the type
case class Projector(fields: Map[String, Seq[String]] = Map())
    extends ProjectorLike {
  lazy val bson = {
    //We ignore the type (ex: fields[records]=...)
    val projs = fields.values map { fs =>
      Projections.include(fs.map{s => s"attrs.$s.obj"}: _*)
    }
    Projections.fields(projs.toSeq: _*)
  }
}

object Projector {
  def bindImpl(
    key: String,
    params: Map[String, Seq[String]],
    prefix: Option[String] = None
  ): Option[Either[String, Projector]] = {
    val pre = prefix match {
      case Some(p) => s"$p."
      case None    => ""
    }
    val res = Try {
      val reg = s"$pre$key\\[([a-zA-Z0-9_-]+)\\]".r
      val projs: Map[String, Seq[String]] = params collect {
        case (reg(t), value) =>
          val reg = "\\w+\\.(\\w+)".r
          t -> value.last.split(',').toSeq.map {
            case reg(attribute) =>
              attribute
            case str =>
              str
          }
      }
      Projector(projs)
    }.toEither.left.map {
      _.getMessage
    }
    Some(res)
  }

  def unbindImpl(
    key: String,
    projector: Projector,
    prefix: Option[String] = None
  )(implicit stringBinder: QueryStringBindable[String]): String = {
    val reg = prefix match {
      case Some(p) =>
        s"$p\\.(.+)".r
      case None =>
        s"(.+)".r
    }
    (projector.fields map {
      case (tpe, fields) =>
        stringBinder.unbind(
          key + s"[$tpe]",
          fields
            .map {
              case reg(p) => p
              case path   => path
            }
            .mkString(",")
        )
    }).mkString("&")
  }

  implicit def queryStringBinder(
    implicit stringBinder: QueryStringBindable[String]
  ): QueryStringBindable[Projector] = new QueryStringBindable[Projector] {
    override def bind(
      key: String,
      params: Map[String, Seq[String]]
    ): Option[Either[String, Projector]] =
      bindImpl(key, params)

    override def unbind(key: String, projector: Projector): String =
      unbindImpl(key, projector)
  }
}
