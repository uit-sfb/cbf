package models.binders

import no.uit.sfb.cbf.shared.errors.BadRequestClientError
import models.graph.{AggregateMethodLike, GroupMethodLike, _}
import org.mongodb.scala.Document
import org.mongodb.scala.bson._
import org.mongodb.scala.model.{Aggregates, BsonField}
import play.api.mvc.QueryStringBindable

import scala.util.Try
import scala.util.matching.Regex

case class Graph(filter: Filter = Filter(),
                 xDef: Xdef = null,
                 yDefs: Seq[Ydef] = Seq(),
                 sorter: Sorter = Sorter(Seq("x" -> true))) {
  lazy val neededIndexes = filter.neededIndexes ++ xDef.neededIndexes ++ yDefs
    .flatMap { _.neededIndexes }
  protected def matchFilter(release: Int) =
    Aggregates.`match`(filter.bson(release))

  protected lazy val project = {
    Aggregates.project(
      Document(
        Seq(Some("_id" -> BsonInt32(0)), xDef.projectX).flatten ++
          yDefs.flatMap {
            _.projectY
          }
      )
    )
  }
  protected lazy val group = Aggregates.group(xDef.projectX match {
    case Some(_) => BsonString(s"$$${xDef.key}")
    case None    => BsonNull()
  }, yDefs.map { ydef =>
    val (n, f) = ydef.method.reduceY(ydef.key)
    BsonField(n, f)
  }: _*)

  protected lazy val renameX =
    Aggregates.project(Document(s"""{
         |_id: 0,
         |x: "$$_id",
         |${yDefs.map { ydef =>
                                       s""""${ydef.key}": 1"""
                                     }
                                     .mkString(",\n")}
         |}""".stripMargin))
  //By default we sort the x. In case it is a CIRI, it will then not be sorted by the printed value.
  protected lazy val sort = Aggregates.sort(sorter.bson)

  def pipeline(release: Int) =
    Seq(matchFilter(release), project, group, renameX, sort)
}

object Graph {
  implicit def queryStringBinder(
    implicit stringBinder: QueryStringBindable[String]
  ): QueryStringBindable[Graph] = new QueryStringBindable[Graph] {
    protected def expand(attr: String): Option[String] = {
      Option(attr).flatMap { a =>
        if (a.isEmpty)
          None
        else
          Some(s"attrs.$a")
      }
    }

    override def bind(
      key: String,
      params: Map[String, Seq[String]]
    ): Option[Either[String, Graph]] = {
      val filters = Filter
        .bindImpl("filter", params)
        .getOrElse(Right(Filter()))
      val sort = Sorter
        .bindImpl("sort", params)
        .getOrElse(Right(Sorter(Seq("x" -> true))))
      val eGraph = filters
        .flatMap(
          f =>
            sort.map { s =>
              f -> s
          }
        )
        .flatMap {
          case (filter, sorter) =>
            Try {
              val reg: Regex = s"^([\\w:]+)(?:\\[([\\.:a-zA-Z0-9_-]*)\\])?$$".r
              val axes: Seq[AxeDefLike] = (params collect {
                case (reg("x", attr), values) =>
                  val vals = values.filter {
                    _.nonEmpty
                  }
                  vals map { value =>
                    Xdef(expand(attr), GroupMethodLike(value))
                  }
                case (reg(key, attr), values) if key.startsWith("y_") =>
                  val vals = values.filter {
                    _.nonEmpty
                  }
                  vals map { value =>
                    Ydef(key, expand(attr), AggregateMethodLike(value))
                  }
              }).flatten.toSeq
              val xs = axes.collect { case x: Xdef => x }
              if (xs.size != 1)
                throw BadRequestClientError(
                  s"Exactly one 'x' parameter is admissible (found '${xs.size}')"
                )
              else {
                val ys = axes.collect { case y: Ydef => y }
                Graph(filter, xs.head, ys, sorter)
              }
            }.toEither.left.map(_.getMessage)
        }
      Some(eGraph)
    }

    override def unbind(key: String, graph: Graph): String = {
      (Filter.unbindImpl("filter", graph.filter) +:
        Sorter.unbindImpl("sort", graph.sorter) +:
        graph.xDef.unbind +:
        graph.yDefs.map {
        _.unbind
      }).mkString("&")
    }
  }
}
