package models.binders

import play.api.mvc.QueryStringBindable
import org.mongodb.scala.model.Sorts._

// Boolean means "forward"
case class Sorter(fields: Seq[(String, Boolean)] = Seq()) extends BsonFormLike {
  lazy val bson = {
    orderBy(fields.map {
      case (f, true) =>
        ascending(f)
      case (f, false) =>
        descending(f)
    }: _*)
  }

  lazy val isEmpty = fields.isEmpty
}

object Sorter {
  val default = Sorter(Seq("id" -> false))

  def bindImpl(key: String,
               params: Map[String, Seq[String]],
               prefix: Option[String] = None)(
    implicit stringBinder: QueryStringBindable[String]
  ): Option[Either[String, Sorter]] = {
    def addPrefix(str: String) = {
      prefix match {
        case Some(p) => s"$p.$str"
        case None    => str
      }
    }
    stringBinder
      .bind(key, params) map {
      case Right(fields) =>
        Right(Sorter(fields.split(',').toSeq map { str: String =>
          if (str.startsWith("-"))
            addPrefix(str.drop(1)) -> false
          else
            addPrefix(str) -> true
        }))
      case _ => Left("Unable to bind a Sorter")
    }
  }

  def unbindImpl(key: String, sorter: Sorter, prefix: Option[String] = None)(
    implicit stringBinder: QueryStringBindable[String]
  ): String = {
    val reg = prefix match {
      case Some(p) =>
        s"$p\\.(.+)".r
      case None =>
        s"(.+)".r
    }
    stringBinder.unbind(
      key,
      (sorter.fields map {
        case (str, forward) =>
          val reducedAttr = str match {
            case reg(attribute) =>
              attribute
            case _ =>
              str
          }
          if (forward) reducedAttr else s"-$reducedAttr"
      }).mkString(",")
    )
  }

  implicit def queryStringBinder(
    implicit stringBinder: QueryStringBindable[String]
  ): QueryStringBindable[Sorter] = new QueryStringBindable[Sorter] {
    override def bind(
      key: String,
      params: Map[String, Seq[String]]
    ): Option[Either[String, Sorter]] = {
      bindImpl(key, params)
    }

    override def unbind(key: String, sorter: Sorter): String = {
      unbindImpl(key, sorter)
    }
  }
}
