package models.binders

import org.mongodb.scala.bson.conversions.Bson

//Attention: Any implementation that adds fields will need to override equals() and hashCode() too
trait BsonFormLike {
  def bson: Bson

  //Since comparing Filter equality is important for caching purpose, and since not all filters are case classes,
  //it was necessary to override equals() (and hashCode()) so the cache would behave normally.

  override def equals(o: Any): Boolean = {
    o match {
      case oo: BsonFormLike =>
        bson.equals(oo.bson)
      case _ =>
        false
    }
  }

  override def hashCode(): Int = bson.hashCode()
}
