package models.binders

import no.uit.sfb.cbf.shared.model.v1.nativ.CompactIRI
import org.mongodb.scala.Document
import org.mongodb.scala.bson.conversions.Bson
import org.mongodb.scala.model.{Filters, TextSearchOptions}

trait FilterLike extends BsonFormLike {
  def neededIndex: Seq[String]
}

object FilterLike {
  def and(filters: FilterLike*): FilterLike = {
    new FilterLike {
      lazy val neededIndex = filters.foldLeft(Seq[String]()) {
        case (acc, v) => acc ++ v.neededIndex
      }
      lazy val bson = Filters.and(filters.map { _.bson }: _*)
    }
  }
  def or(filters: FilterLike*): FilterLike = {
    new FilterLike {
      lazy val neededIndex = filters.foldLeft(Seq[String]()) {
        case (acc, v) => acc ++ v.neededIndex
      }
      lazy val bson = Filters.or(filters.map { _.bson }: _*)
    }
  }
}

case class TextSearch(text: String, caseSensitive: Boolean = false)
    extends FilterLike {
  lazy val neededIndex = Seq()
  lazy val bson = {
    if (text.nonEmpty) {
      val modifiedText =
        if (caseSensitive)
          text
        else
          text.toLowerCase
      Filters.text(
        modifiedText,
        new TextSearchOptions().caseSensitive(caseSensitive)
      )
    } else
      Filters.empty()
  }
}

//Used by RecordsController
case class IsIn(path: String, options: Set[String]) extends FilterLike {
  lazy val neededIndex = Seq(path)
  lazy val bson = {
    Filters.in(path, options.toSeq: _*)
  }
}

case class CiriSearch(m: Map[CompactIRI, Seq[String]]) extends FilterLike {
  lazy val neededIndex = m.flatMap {
    case (_, attrRefs) =>
      attrRefs.map { attrRef =>
        s"attrs.$attrRef.obj.iri"
      }
  }.toSeq
  lazy val bson = {
    if (m.nonEmpty) {
      val transpose = m.toSeq
        .flatMap { case (iri, s) => s.map { _ -> iri } }
        .groupBy { case (k, _) => k }
        .map { case (k, l) => k -> l.map { _._2 }.toSet }
      Filters.or(
        //inner.bson +: //Issue: https://stackoverflow.com/questions/48146146/text-search-with-or-results-in-no-query-solutions
        transpose.map {
          case (attrRef, ciris) =>
            Filters.in(
              s"attrs.$attrRef.obj.iri",
              ciris.flatMap { ciri =>
                Seq(ciri.asString, s"${ciri.prefix.toUpperCase}:${ciri.id}")
              }.toSeq: _*
            ) //Second is needed in case an upper case is stored in the database
        }.toSeq: _* //We make sure to remove duplicate filters
      )
    } else
      Filters.empty()
  }
}

case class NativeFilter(bson: Bson) extends FilterLike {
  lazy val neededIndex = {
    val reg = "\"attrs\\.[.:\\w-]+\"".r
    reg.findAllIn(bson.toString).map { _.drop(1).dropRight(1) }.toSeq
  }
}
