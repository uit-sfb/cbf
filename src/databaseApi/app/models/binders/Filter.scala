package models.binders

import org.mongodb.scala.bson.BsonDocument
import org.mongodb.scala.bson.conversions.Bson
import play.api.mvc.QueryStringBindable
import org.mongodb.scala.model.Filters._

import scala.util.Try

case class Filter(filters: Seq[FilterLike] = Seq()) {
  lazy val neededIndexes = filters.flatMap { _.neededIndex }

  def bson(release: Option[Int]): Bson = {
    val fs = filters map {
      _.bson
    }

    val flts = release match {
      case Some(rel) =>
        Seq(
          lte("valid.start", rel),
          or(exists("valid.end", false), gt("valid.end", rel))
        ) ++ fs
      case None =>
        fs
    }

    if (flts.isEmpty)
      new BsonDocument()
    else
      and(flts: _*)
  }

  def bson(latestRelease: Int,
           ignoreStartCondition: Boolean = false,
           ignoreEndCondition: Boolean = false): Bson = {
    val fs = filters map {
      _.bson
    }
    val flts = Seq(
      if (ignoreStartCondition)
        None
      else
        Some(lte("valid.start", latestRelease)),
      if (ignoreEndCondition)
        None
      else
        Some(or(exists("valid.end", false), gt("valid.end", latestRelease)))
    ).flatten ++ fs

    if (flts.isEmpty)
      new BsonDocument()
    else
      and(flts: _*)
  }

  lazy val isEmpty = filters.isEmpty
}

object Filter {
  def bindImpl(
    key: String,
    params: Map[String, Seq[String]]
  ): Option[Either[String, Filter]] = {
    val res = Try {
      val freeTextReg = "^'.+'$".r
      val nativeReg = "^\\{.+\\}$".r
      val filters: Seq[Seq[FilterLike]] = (params collect {
        case (`key`, value) =>
          value flatMap {
            case v if freeTextReg.matches(v) =>
              Some(TextSearch(v.drop(1).dropRight(1)))
            case v if v.nonEmpty && nativeReg.matches(v) =>
              Some(NativeFilter(BsonDocument(v)))
            case _ => None
          }
      }).toSeq
      Filter(filters.flatten)
    }.toEither.left.map {
      _.getMessage
    }
    Some(res)
  }

  def unbindImpl(key: String, filter: Filter)(
    implicit stringBinder: QueryStringBindable[String]
  ): String = {
    (filter.filters collect {
      case TextSearch(text, _) =>
        stringBinder.unbind(key, s"'$text'")
      case NativeFilter(doc) =>
        stringBinder.unbind(key, doc.toString)
    }).mkString("&")
  }

  implicit def queryStringBinder(
    implicit stringBinder: QueryStringBindable[String]
  ): QueryStringBindable[Filter] = new QueryStringBindable[Filter] {
    override def bind(
      key: String,
      params: Map[String, Seq[String]]
    ): Option[Either[String, Filter]] =
      bindImpl(key, params)

    override def unbind(key: String, filter: Filter): String =
      unbindImpl(key, filter)
  }
}
