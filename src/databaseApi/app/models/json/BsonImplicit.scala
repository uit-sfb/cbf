package models.json

import org.mongodb.scala.bson.Document
import play.api.libs.json.{Json, Writes}

object BsonImplicit {
  implicit val documentWriter = new Writes[Document] {
    def writes(doc: Document) = Json.parse(doc.toJson())
  }
}
