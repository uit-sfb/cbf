package models.json

import org.mongodb.scala.bson.BsonValue

object BsonSort {
  implicit val ord = new Ordering[BsonValue] {
    def compare(x: BsonValue, y: BsonValue): Int = {
      if (x.isString) {
        if (y.isString)
          x.asString().getValue.compare(y.asString().getValue)
        else -1 //(string before everything else)
      }
      else if (x.isNumber) {
        if (y.isString)
          1 //String before anything else
        else if (y.isNumber)
          x.asNumber().doubleValue().compare(y.asNumber().doubleValue())
        else
          -1 //Number before null
      }
      else
        1 //Null After everybody
    }
  }
}
