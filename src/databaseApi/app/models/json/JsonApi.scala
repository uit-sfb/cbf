package models.json

import no.uit.sfb.cbf.shared.jsonapi._
import org.mongodb.scala.bson.Document
import play.api.libs.json._
import io.circe.{Encoder, Json => CircleJson}
import io.circe.parser._
import io.circe.generic.extras.auto._
import io.circe.generic.extras.Configuration
import io.circe.syntax._
import no.uit.sfb.cbf.shared.auth.UserInfo
import no.uit.sfb.cbf.shared.config._
import no.uit.sfb.cbf.shared.dataset.{
  DatasetRegistration,
  DatasetRegistrationForm
}
import no.uit.sfb.cbf.shared.model.v1.nativ.Record
import no.uit.sfb.cbf.shared.model.v1.versioninfo.VersionInfo

import models.member._

import scala.util.Try

object JsonApi {

  implicit private val customConfig: Configuration =
    Configuration.default.withDefaults

  import BsonImplicit._

  implicit val config = JsonConfiguration(
    optionHandlers = OptionHandlers.WritesNull
  )

  //Just to make circe happy
  implicit val dummyEncoder: Encoder[Document] = new Encoder[Document] {
    final def apply(doc: Document): CircleJson =
      parse(doc.toJson()).toTry.get
  }
  implicit val mapOptionFormat = new Format[Map[String, Option[String]]] {
    def writes(obj: Map[String, Option[String]]): JsValue =
      Json.parse(obj.asJson.noSpaces)

    def reads(json: JsValue): JsResult[Map[String, Option[String]]] = {
      JsResult.fromTry(
        decode[Map[String, Option[String]]](json.toString()).toTry
      )
    }
  }
  implicit val formatRecord = new Format[Record] {
    def writes(rec: Record): JsValue = Json.parse(rec.serialize(false))

    def reads(json: JsValue): JsResult[Record] = {
      JsResult.fromTry(Try {
        Record(json.toString())
      })
    }
  }
  implicit val formatDocument = new Format[Document] {
    def writes(doc: Document): JsValue = Json.parse(doc.toJson())

    def reads(json: JsValue): JsResult[Document] = {
      JsResult.fromTry(Try {
        Document(json.toString())
      })
    }
  }
  implicit val formatUserInfo =
    Json.using[Json.WithDefaultValues].format[UserInfo]
  implicit val formatMemberInfo =
    Json.using[Json.WithDefaultValues].format[MemberInfo]
  implicit val formatVersionInfo =
    Json.using[Json.WithDefaultValues].format[VersionInfo]
  implicit val formatRelationship =
    Json.using[Json.WithDefaultValues].format[Relationship]
  implicit val formatRelationships =
    Json.using[Json.WithDefaultValues].format[Relationships]
  implicit val formatAttributeDefWriter =
    Json.using[Json.WithDefaultValues].format[AttributeDef]

  implicit val formatTabWriter = Json.using[Json.WithDefaultValues].format[Tab]

  implicit val formatLineagedTypeTypeWriter =
    Json.using[Json.WithDefaultValues].format[LineagedType]
  implicit val formatCustomTypeTypeWriter =
    Json.using[Json.WithDefaultValues].format[CustomType]
  implicit val formatCustomResolverTypeWriter =
    Json.using[Json.WithDefaultValues].format[CustomResolver]
  implicit val formatDatasetConfigWriter =
    Json.using[Json.WithDefaultValues].format[DatasetConfig]
  implicit val formatDatasetRegistrationWriter =
    Json.using[Json.WithDefaultValues].format[DatasetRegistration]
  implicit val formatDatasetCreationFormWriter =
    Json.using[Json.WithDefaultValues].format[DatasetRegistrationForm]

  implicit val formatDocumentData =
    Json.using[Json.WithDefaultValues].format[Data[Document]]
  implicit val formatUserInfoData =
    Json.using[Json.WithDefaultValues].format[Data[UserInfo]]
  implicit val formatMemberInfoData =
    Json.using[Json.WithDefaultValues].format[Data[MemberInfo]]
  implicit val formatVersionInfoData =
    Json.using[Json.WithDefaultValues].format[Data[VersionInfo]]
  implicit val formatAttributeDefData =
    Json.using[Json.WithDefaultValues].format[Data[AttributeDef]]

  implicit val formatTabData =
    Json.using[Json.WithDefaultValues].format[Data[Tab]]

  implicit val formatDatasetConfigData =
    Json.using[Json.WithDefaultValues].format[Data[DatasetConfig]]
  implicit val formatDatasetRegistrationData =
    Json.using[Json.WithDefaultValues].format[Data[DatasetRegistration]]
  implicit val formaDatasetCreationFormData =
    Json.using[Json.WithDefaultValues].format[Data[DatasetRegistrationForm]]
  implicit val formatRecordData =
    Json.using[Json.WithDefaultValues].format[Data[Record]]
  implicit val formatErr = Json.using[Json.WithDefaultValues].format[Err]
  implicit val formatDocumentSingleResponse =
    Json.using[Json.WithDefaultValues].format[SingleResponse[Document]]
  implicit val formatRecordSingleResponse =
    Json.using[Json.WithDefaultValues].format[SingleResponse[Record]]
  implicit val formatDatasetConfigSingleResponse =
    Json.using[Json.WithDefaultValues].format[SingleResponse[DatasetConfig]]
  implicit val formatDatasetRegistrationSingleResponse =
    Json
      .using[Json.WithDefaultValues]
      .format[SingleResponse[DatasetRegistration]]
  implicit val formatDatasetCreationFormSingleResponse =
    Json
      .using[Json.WithDefaultValues]
      .format[SingleResponse[DatasetRegistrationForm]]
  implicit val formatUserInfoSingleResponse =
    Json.using[Json.WithDefaultValues].format[SingleResponse[UserInfo]]
  implicit val formatMemberInfoSingleResponse =
    Json.using[Json.WithDefaultValues].format[SingleResponse[MemberInfo]]
  implicit val formatVersionInfoSingleResponse =
    Json.using[Json.WithDefaultValues].format[SingleResponse[VersionInfo]]
  implicit val formatDocumentMultipleResponse =
    Json.using[Json.WithDefaultValues].format[MultipleResponse[Document]]
  implicit val formatDatasetConfigMultipleResponse =
    Json.using[Json.WithDefaultValues].format[MultipleResponse[DatasetConfig]]
  implicit val formatDatasetRegistrationMultipleResponse =
    Json
      .using[Json.WithDefaultValues]
      .format[MultipleResponse[DatasetRegistration]]
  implicit val formatRecordMultipleResponse =
    Json.using[Json.WithDefaultValues].format[MultipleResponse[Record]]
  implicit val formatUserInfoMultipleResponse =
    Json.using[Json.WithDefaultValues].format[MultipleResponse[UserInfo]]
  implicit val formatMemberInfoMultipleResponse =
    Json.using[Json.WithDefaultValues].format[MultipleResponse[MemberInfo]]
  implicit val formatVersionInfoMultipleResponse =
    Json.using[Json.WithDefaultValues].format[MultipleResponse[VersionInfo]]
  implicit val formatErrorResponse =
    Json.using[Json.WithDefaultValues].format[ErrorResponse]
}
