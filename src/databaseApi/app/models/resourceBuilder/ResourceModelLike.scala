package models.resourceBuilder

import no.uit.sfb.cbf.shared.errors.BadRequestClientError
import no.uit.sfb.cbf.shared.config.CustomResolver

trait ResourceModelLike {
  def keys: Set[String]

  def modes: Map[String, ResourceBuilderLike]

  //Path on disk
  def path(resourceId: String, key: String): String

  def resourceBuilder(mode: String = "default"): ResourceBuilderLike = {
    modes.getOrElse(
      mode,
      throw BadRequestClientError(
        s"Mode '$mode' does not exist. Please use one of the following: '${modes.keys.mkString(", ")}'."
      )
    )
  }

  def filter(keySelect: Set[String]): Set[String] = {
    if (keySelect.isEmpty)
      keys
    else
      keys.intersect(keySelect)
  }
}

object ResourceModelLike {
  def apply(cr: CustomResolver,
            dbName: String = "",
            ver: String = "",
            apiHost: String = ""): ResourceModelLike = {
    new ResourceModelLike {
      val keys = cr.kvp.collect {
        case (k, _) if k.startsWith("+key:") => k.split(':').tail.mkString(":")
      }.toSet

      val modes = {
        val set = cr.kvp
          .getOrElse("+modes", "")
          .split('|')
          .toSet
          .filter(_.nonEmpty)
        //We ensure default is always present
        (set + "default").map { m =>
          m -> ResourceBuilderLike(m)(this)
        }.toMap
      }

      def path(resourceId: String, key: String): String = {
        val k = s"+key:$key"
        cr.kvp
          .getOrElse(
            k,
            throw BadRequestClientError(
              s"No key '$k' found for type '${cr.prefix}'."
            )
          )
          .replace("{id}", resourceId)
          .replace("{ver}", ver)
          .replace("{dsName}", dbName)
          .replace("{api}", apiHost)
      }
    }
  }
}
