package models.resourceBuilder

import akka.NotUsed
import akka.stream.IOResult
import akka.stream.alpakka.file.TarArchiveMetadata
import akka.stream.alpakka.file.scaladsl.Archive
import akka.stream.scaladsl.{FileIO, Flow, Keep, Source}
import akka.util.ByteString
import com.typesafe.scalalogging.LazyLogging
import org.apache.commons.compress.archivers.tar.TarArchiveEntry
import org.apache.commons.compress.utils.IOUtils

import java.io.{File, FileInputStream}
import java.nio.file.{Files, Paths}
import scala.concurrent.Future

sealed trait ResourceBuilderLike {
  //If selectors empty, use all selectors. Otherwise, only use the specified selectors
  protected def writeArchiveImpl(
    resourceIds: Iterator[String],
    keySelect: Set[String]
  ): Iterator[(TarArchiveMetadata, Source[ByteString, NotUsed])]

  def writeArchive(resourceIds: Iterator[String],
                   keySelect: Set[String]): Source[ByteString, NotUsed] = {
    val streams = writeArchiveImpl(resourceIds, keySelect)
    Source
      .fromIterator(() => streams)
      .log("Archiving")
      .via(Archive.tar().via(akka.stream.scaladsl.Compression.gzip))
  }

}

object ResourceBuilderLike {
  def apply(mode: String) = {
    mode match {
      case "merged" => MergedResourceBuilder
      case _        => IndividualResourceBuilder
    }
  }
}

//Files from different records are kept separate
case class IndividualResourceBuilder(model: ResourceModelLike)
    extends ResourceBuilderLike
    with LazyLogging {
  def writeArchiveImpl(
    resourceIds: Iterator[String],
    keySelect: Set[String]
  ): Iterator[(TarArchiveMetadata, Source[ByteString, NotUsed])] = {
    resourceIds.flatMap { resourceId =>
      model.filter(keySelect).flatMap { key =>
        val p = Paths.get(model.path(resourceId, key))
        if (Files.exists(p)) {
          val ext = p.getFileName.toString.split('.').tail.mkString(".")
          val name = s"$resourceId/$key.$ext"
          val src = TarArchiveMetadata(
            name,
            Files.size(p),
            Files.getLastModifiedTime(p).toInstant
          ) -> FileIO
            .fromPath(p)
            .viaMat(Flow.fromFunction(identity))(Keep.right)
          Some(src)
        } else {
          logger.warn(s"Wrong resource path '$p'")
          None
        }
      }
    }
  }
}

//All files with the same name are concatenated together (merged)
case class MergedResourceBuilder(model: ResourceModelLike)
    extends ResourceBuilderLike
    with LazyLogging {
  def writeArchiveImpl(
    resourceIds: Iterator[String],
    keySelect: Set[String]
  ): Iterator[(TarArchiveMetadata, Source[ByteString, NotUsed])] = {
    val rIds = resourceIds.toVector //There is no other solution other than materializing the iterator. Duplicating the stream would lead to the same same memory footprint.
    model.filter(keySelect).iterator.map { key =>
      val size = rIds.foldLeft(0L) {
        case (acc, id) =>
          val p = Paths.get(model.path(id, key))
          if (Files.exists(p))
            acc + Files.size(p) + 1 //+1 for the newline
          else
            acc
      }
      val ext = Paths
        .get(model.path("", key))
        .getFileName
        .toString
        .split('.')
        .tail
        .mkString(".")
      val name = s"$key.$ext"
      TarArchiveMetadata(name, size) -> rIds
        .foldLeft(Source.empty[ByteString]) {
          case (acc, resourceId) =>
            val p = Paths.get(model.path(resourceId, key))
            if (Files.exists(p)) {
              acc ++ FileIO.fromPath(p) ++ Source(Seq(ByteString("\n")))
            } else {
              logger.warn(s"Wrong resource path '$p'")
              acc
            }
        }
    }
  }
}
