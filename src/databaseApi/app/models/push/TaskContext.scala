package models.push

import com.ibm.icu.text.CharsetDetector
import com.typesafe.scalalogging.LazyLogging
import no.uit.sfb.cbf.shared.model.v1.task.UpdateDsTaskStatus
import no.uit.sfb.cbf.shared.utils.Tsv
import no.uit.sfb.scalautils.common.FileUtils

import java.io.{BufferedInputStream, FileInputStream}
import java.nio.charset.StandardCharsets
import java.nio.file.Path
import scala.io.{BufferedSource, Source}

case class TaskContext(dbName: String,
                       ver: String,
                       method: String,
                       nbRecords: Long,
                       cnt: Long,
                       errors: Long,
                       state: StateLike,
                       path: String,
                       encoding: String = "UTF-8") {
  lazy val progress: Int = ((cnt.toDouble / nbRecords) * 100).toInt
  def incr(incr: Int): TaskContext = copy(cnt = Math.min(cnt + incr, nbRecords))
  def newState(s: StateLike): TaskContext = copy(state = s)
  lazy val toExchange = UpdateDsTaskStatus(
    dbName,
    ver,
    method,
    progress,
    nbRecords,
    cnt,
    errors,
    state.name
  )
}

sealed trait StateLike {
  def name: String
  def active: Boolean = false
}

case object Starting extends StateLike {
  val name = "Starting"
  //Not an active state
}

case object Running extends StateLike {
  val name = "Running"
  override val active: Boolean = true
}

case object Success extends StateLike {
  val name = "Success"
}

case object Failed extends StateLike {
  val name = "Failed"
}

case class TaskContextBuilder(localPath: Path) extends LazyLogging {
  def file(dbName: String, ver: String, method: String) =
    localPath.resolve(s"$dbName/$ver/$method.tsv").toFile

  def build(dbName: String, ver: String, method: String) = {
    val f = file(dbName, ver, method)
    val charsetDetector = new CharsetDetector()
    val enc = if (FileUtils.isGzipped(f.toPath)) {
      logger.info("Detected gzip: assuming UTF-8.")
      //Can't get bothered detecting encoding for gzip as unGzipStream does not support mark (needed by charsetDetector.setText())
      "UTF-8"
    } else {
      val is = new BufferedInputStream(new FileInputStream(f))
      //Does not read the whole file. Just what is needed.
      val cm = try {
        charsetDetector.setText(is)
        //charsetDetector.enableInputFilter(true)
        Option(charsetDetector.detect())
      } finally {
        is.close()
      }
      cm match {
        case Some(m) if m.getConfidence > 5 =>
          val detectedEnc = m.getName
          logger.info(
            s"Detected $detectedEnc (confidence: ${m.getConfidence}%)"
          )
          detectedEnc
        case _ =>
          logger.warn(s"File encoding could not be detected. Using UTF-8.")
          StandardCharsets.UTF_8.name()
      }
    }
    val s: BufferedSource = Source.fromInputStream(
      FileUtils.unGzipStream(new BufferedInputStream(new FileInputStream(f))),
      enc
    )
    //This is what explains the time taken for large files before the UI displays that the action is taking place.
    val nbRecords = try {
      Tsv.cleanIt(s.getLines()).count { _.nonEmpty } - 1 //-1 for the header
    } finally {
      s.close()
    }
    TaskContext(dbName, ver, method, nbRecords, 0, 0, Starting, f.toString, enc)
  }
}
