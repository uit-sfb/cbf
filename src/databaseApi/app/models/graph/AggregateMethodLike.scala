package models.graph

import no.uit.sfb.cbf.shared.errors.BadRequestClientError
import models.graph.postprocess._
import no.uit.sfb.cbf.transform.resolv.Resolution
import org.mongodb.scala.bson.{BsonDocument, BsonDouble, BsonValue}

import scala.concurrent.{ExecutionContext, Future}

//MethodLike.func() is applied to all values taken by each y_ after the pipeline aggregation
sealed trait AggregateMethodLike extends MethodLike {
  def method: String = name

  def check(attr: Option[String], key: String) =
    assume(
      attr.nonEmpty,
      s"The attribute should be provided (ex: '$key[some:attribute]=$name')"
    )

  def reduceY(key: String): (String, BsonDocument) =
    key -> BsonDocument(s"""{ $$$method: "$$$key" }""")

  def projectY(key: String,
               attr: Option[String]): Option[(String, BsonDocument)] =
    attr map { a =>
      key -> BsonDocument(s"""{
           |  $$convert : {
           |    input: {
           |      $$first: { $$first: "$$$a.obj.val" }
           |    },
           |    to: "double",
           |    onError: null
           |  }
           |}""".stripMargin)
    }
}

object AggregateMethodLike {

  private val all = Set(
    Avg,
    Max,
    Min,
    Sum,
    SumP,
    SumC,
    Count,
    CountP,
    CountC,
    CountCP,
    SetAcc,
    SetAccUrl,
    SetAccR,
    UnwindAcc,
    UnwindAccR,
    /*UnwindAccP,
    UnwindAccRP,*/
  )

  def apply(name: String): AggregateMethodLike = {
    all
      .find {
        _.name == name
      }
      .getOrElse(
        throw BadRequestClientError(
          s"Could not find AggregateMethodLike with name '$name'"
        )
      )
  }
}

trait CountLike extends AggregateMethodLike {
  override val method = "count"

  override def check(attr: Option[String], key: String) =
    assume(
      attr.isEmpty,
      s"The attribute should not be provided when using method '$name' (use: '$key=$name')"
    )

  override def reduceY(key: String): (String, BsonDocument) =
    key -> BsonDocument(s"""{ $$sum: 1 }""")
}

case object Count extends CountLike with NoFuncLike {
  val name = "count"
}

case object CountP extends CountLike with PercentLike {
  val name = "countP"
}

case object CountC extends CountLike with CumulLike {
  val name = "countC"
}

case object CountCP extends CountLike with PercentLike {
  val name = "countCP"

  override def func(dsName: String, version: String)(key: String)(
    s: Seq[BsonValue]
  )(implicit resolver: Resolution,
    ec: ExecutionContext): Future[Seq[Map[String, BsonValue]]] = {
    super.func(dsName, version)(key)(s).map { percent =>
      val (_, res) = percent.foldLeft((0.0, Seq[Map[String, BsonDouble]]())) {
        case ((count, m), values) =>
          val (k, v) = values.head //We know that percent will only return a Map of size 1
          val newCount = count + v.asNumber().doubleValue()
          newCount -> (m ++ Seq(Map(k -> BsonDouble(newCount))))
      }
      res
    }
  }
}

case object Avg extends AggregateMethodLike with NoFuncLike {
  val name = "avg"
}

case object Max extends AggregateMethodLike with NoFuncLike {
  val name = "max"
}

case object Min extends AggregateMethodLike with NoFuncLike {
  val name = "min"
}

trait SetAccLike extends AggregateMethodLike {
  override def method = "addToSet"

  override def projectY(key: String,
                        attr: Option[String]): Option[(String, BsonDocument)] =
    attr map { a =>
      key -> BsonDocument(s"""{
           |  $$ifNull: [
           |    { $$first: {
           |        $$first: { $$first: "$$$a.obj.iri" }
           |      }
           |    },
           |    { $$cond: {
           |        if: { $$eq: [ { $$first: { $$first: "$$$a.obj.val" } }, "" ] },
           |        then: null,
           |        else: { $$first: { $$first: "$$$a.obj.val" } }
           |      }
           |    }
           |  ]
           |}""".stripMargin)
    }
}

case object SetAcc extends SetAccLike with ExpressLike {
  val name = "set"
}

case object SetAccUrl extends SetAccLike with ExpressLike {
  val name = "setU"
  override val metaRef = "_iri"
}

case object SetAccR extends SetAccLike with NoFuncLike {
  val name = "setR"
}

trait UnwindAccLike extends SetAccLike {
  override val method = "push"
}

case object UnwindAcc extends UnwindAccLike with ReduceExpressListLike {
  val name = "unwind"
}

case object UnwindAccR extends UnwindAccLike with ReduceListLike {
  val name = "unwindR"
}

/*case object UnwindAccRP extends UnwindAccLike with PercentReduceListLike {
  val name = "unwindRP"
}

case object UnwindAccP extends UnwindAccLike with PercentReduceExpressListLike {
  val name = "unwindP"
}*/

trait SumLike extends AggregateMethodLike {
  override val method = "sum"
}

case object Sum extends SumLike with NoFuncLike {
  val name = "sum"
}

case object SumP extends SumLike with PercentLike {
  val name = "sumP"
}

case object SumC extends SumLike with CumulLike {
  val name = "sumC"
}
