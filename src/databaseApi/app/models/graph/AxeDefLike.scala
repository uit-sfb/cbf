package models.graph

import play.api.mvc.QueryStringBindable

sealed trait AxeDefLike {
  def key: String
  def attr: Option[String]
  def method: MethodLike
  def unbind(implicit stringBinder: QueryStringBindable[String]) = {
    val reg = s"attrs\\.(.+)".r
    val opt = attr match {
      case Some(a) =>
        val trimmed = a match {
          case reg(attr) =>
            attr
          case _ =>
            a
        }
        s"[$trimmed]"
      case None => ""
    }
    stringBinder.unbind(s"$key$opt", method.name)
  }
  def neededIndexes: List[String]
}

case class Xdef(attr: Option[String], method: GroupMethodLike)
    extends AxeDefLike {
  val key = "x"
  method.check(attr, key)
  final lazy val projectX = method.projectX(attr) map { px =>
    key -> px
  }
  lazy val neededIndexes = (attr.toList).map { method.neededIndex }
}

case class Ydef(key: String,
                attr: Option[String],
                method: AggregateMethodLike,
                //postProcess: PostProcessLike
) extends AxeDefLike {
  method.check(attr, key)

  lazy val projectY = method.projectY(key, attr)
  lazy val neededIndexes = List()
}
