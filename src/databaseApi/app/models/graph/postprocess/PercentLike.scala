package models.graph.postprocess

import models.graph.MethodLike
import no.uit.sfb.cbf.transform.resolv.Resolution
import org.mongodb.scala.bson.{BsonDouble, BsonValue}

import scala.concurrent.{ExecutionContext, Future}

trait PercentLike {
  this: MethodLike =>

  def func(dsName: String, version: String)(key: String)(s: Seq[BsonValue])(
    implicit resolver: Resolution,
    ec: ExecutionContext
  ): Future[Seq[Map[String, BsonValue]]] = {
    val total = s.foldLeft(0.0) {
      case (acc, v) => acc + v.asNumber().doubleValue()
    }
    Future.successful(s.map { v =>
      Map(key -> BsonDouble(v.asNumber().doubleValue / total))
    })
  }
}
