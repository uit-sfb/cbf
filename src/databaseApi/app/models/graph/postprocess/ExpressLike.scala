package models.graph.postprocess

import models.graph.MethodLike
import no.uit.sfb.cbf.shared.model.v1.nativ.CompactIRI
import no.uit.sfb.cbf.transform.resolv.Resolution
import org.mongodb.scala.bson.{BsonArray, BsonString, BsonValue}

import scala.concurrent.{ExecutionContext, Future}

trait ExpressLike {
  this: MethodLike =>

  def metaRef: String = "val"

  def func(dsName: String, version: String)(key: String)(s: Seq[BsonValue])(
    implicit resolver: Resolution,
    ec: ExecutionContext
  ): Future[Seq[Map[String, BsonValue]]] = {

    Future.sequence(s.map { v =>
      ExpressLike.expressValue(v, dsName, version, metaRef) map { x =>
        Map(key -> x)
      }
    })
  }
}

object ExpressLike {
  import scala.jdk.CollectionConverters._
  def expressValue(
    value: BsonValue,
    dsName: String,
    version: String,
    metaRef: String = "val"
  )(implicit resolver: Resolution, ec: ExecutionContext): Future[BsonValue] = {
    if (value.isString) {
      val idStr = value.asString().getValue
      CompactIRI(idStr) match {
        case Some(x) =>
          resolver
            .getOption(x, dsName, version)
            .map {
            case Some(m) =>
              m.getOrElse(metaRef, idStr)
            case None =>
                idStr
            }
            .map {
              BsonString(_)
            }
        case None =>
          Future.successful(value)
      }
    } else if (value.isArray) {
      Future
        .sequence(value.asArray().getValues.asScala.toList.map { x =>
          expressValue(x, dsName, version, metaRef)
        })
        .map { x =>
          BsonArray.fromIterable(x)
        }
    } else {
      Future.successful(value)
    }
  }
}
