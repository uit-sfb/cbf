package models.graph.postprocess

import models.graph.MethodLike
import no.uit.sfb.cbf.transform.resolv.Resolution
import org.bson.BsonNull
import org.mongodb.scala.bson.{
  BsonArray,
  BsonDouble,
  BsonInt32,
  BsonString,
  BsonValue
}

import scala.jdk.CollectionConverters._
import scala.concurrent.{ExecutionContext, Future}

trait ReduceListLike {
  this: MethodLike =>

  protected def keyTranslation(k: String, dsName: String, version: String)(
    implicit resolver: Resolution,
    ec: ExecutionContext
  ): Future[String] =
    Future.successful(k)
  protected def toStringValue(bv: BsonValue): String = {
    bv match {
      case _: BsonNull => "Undefined"
      case v           => v.asString().getValue
    }
  }
  def func(dsName: String, version: String)(key: String)(s: Seq[BsonValue])(
    implicit resolver: Resolution,
    ec: ExecutionContext
  ): Future[Seq[Map[String, BsonValue]]] = {
    val mapped = s.map {
      case arr: BsonArray =>
        arr.getValues.asScala.groupBy(toStringValue).map {
          case (k, v) =>
            keyTranslation(k, dsName, version).map { _ -> BsonInt32(v.size) }
        }
      case _ => throw new Exception("Wrong type")
    }
    Future
      .sequence(mapped.map { x =>
        Future.sequence(x)
      })
      .map {
        _.map { _.toMap }
      }
  }
}

trait ReduceExpressListLike extends ReduceListLike {
  this: MethodLike =>

  override def keyTranslation(k: String, dsName: String, version: String)(
    implicit resolver: Resolution,
    ec: ExecutionContext
  ): Future[String] = {
    ExpressLike.expressValue(BsonString(k), dsName, version).map {
      _.asString().getValue
    }
  }
}

//Wrong implementation of percent: it should be per point instead of per line

/*trait PercentReduceListLike extends ReduceListLike {
  override def func(key: String)(s: Seq[BsonValue])(
      implicit resolver: Resolution,
      ec: ExecutionContext): Future[Seq[Map[String, BsonValue]]] = {
    super.func(key)(s).map { PercentReduceListLike.toPercent }
  }
}

object PercentReduceListLike {
  def toPercent(
      x: Seq[Map[String, BsonValue]]): Seq[Map[String, BsonDouble]] = {
    val total = x.flatten.foldLeft(0.0) {
      case (acc, (_, v)) => acc + v.asNumber().doubleValue()
    }
    x.map {
      _.view.mapValues { d =>
        BsonDouble(d.asNumber().doubleValue() / total)
      }.toMap
    }
  }
}

trait PercentReduceExpressListLike extends ReduceExpressListLike {
  override def func(key: String)(s: Seq[BsonValue])(
      implicit resolver: Resolution,
      ec: ExecutionContext): Future[Seq[Map[String, BsonValue]]] = {
    super.func(key)(s).map { PercentReduceListLike.toPercent }
  }
}*/
