package models.graph.postprocess

import models.graph.MethodLike
import no.uit.sfb.cbf.transform.resolv.Resolution
import org.mongodb.scala.bson.BsonValue

import scala.concurrent.{ExecutionContext, Future}

trait NoFuncLike {
  this: MethodLike =>

  def func(dsName: String, version: String)(key: String)(s: Seq[BsonValue])(
    implicit resolver: Resolution,
    ec: ExecutionContext
  ): Future[Seq[Map[String, BsonValue]]] = {
    Future.successful(s.map { v =>
      Map(key -> v)
    })
  }
}
