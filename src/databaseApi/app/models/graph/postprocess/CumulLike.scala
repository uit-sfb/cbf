package models.graph.postprocess

import models.graph.MethodLike
import no.uit.sfb.cbf.transform.resolv.Resolution
import org.mongodb.scala.bson.{BsonDouble, BsonValue}

import scala.concurrent.{ExecutionContext, Future}

trait CumulLike {
  this: MethodLike =>

  def func(dsName: String, version: String)(key: String)(s: Seq[BsonValue])(
    implicit resolver: Resolution,
    ec: ExecutionContext
  ): Future[Seq[Map[String, BsonValue]]] = {
    val (_, l) = s.foldLeft((0.0, List[BsonDouble]())) {
      case ((count, list), v) =>
        val newCount = count + v.asNumber().doubleValue()
        newCount -> (list :+ BsonDouble(newCount))
    }
    Future.successful(l.map { v =>
      Map(key -> v)
    })
  }
}
