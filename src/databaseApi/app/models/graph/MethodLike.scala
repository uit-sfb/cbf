package models.graph

import no.uit.sfb.cbf.transform.resolv.Resolution
import org.mongodb.scala.bson.BsonValue

import scala.concurrent.{ExecutionContext, Future}

trait MethodLike {
  def name: String

  def check(attr: Option[String], key: String): Unit

  //Mapping of keys and values
  //In most cases the key will stay identical (the string in the return type)
  //In addition, the Map in the return is used in case one wants to split an array into separate Ys.
  //In most cases the Map will contain a single element.
  def func(dsName: String, version: String)(key: String)(s: Seq[BsonValue])(
    implicit resolver: Resolution,
    ec: ExecutionContext
  ): Future[Seq[Map[String, BsonValue]]]
}
