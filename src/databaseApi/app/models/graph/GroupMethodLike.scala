package models.graph

import no.uit.sfb.cbf.shared.errors.BadRequestClientError
import models.graph.postprocess.{ExpressLike, NoFuncLike}
import org.mongodb.scala.bson.BsonDocument

//MethodLike.func() is applied to all values taken by x after the pipeline aggregation
sealed trait GroupMethodLike extends MethodLike {
  def check(attr: Option[String], key: String) =
    assume(
      attr.nonEmpty,
      s"The attribute should be provided (ex: '$key[some:attribute]=$name')"
    )

  def projectX(attr: Option[String]): Option[BsonDocument]
  def neededIndex: String => String
}

object GroupMethodLike {
  private def all: Set[GroupMethodLike] = Set(
    DailyTimeSeries,
    MonthlyTimeSeries,
    YearlyTimeSeries,
    Each,
    EachU,
    EachR,
    Reduce
  )

  def apply(name: String): GroupMethodLike =
    all
      .find {
        _.name == name
      }
      .getOrElse(
        throw BadRequestClientError(
          s"Could not find GroupMethodLike with name '$name'"
        )
      )
}

case object Reduce extends GroupMethodLike with NoFuncLike {
  val name = "reduce"
  val neededIndex = _ + ".obj.val"

  override def check(attr: Option[String], key: String) =
    assume(
      attr.isEmpty,
      s"The attribute should not be provided when using method '$name' (use: '$key=$name')"
    )

  def projectX(attr: Option[String]) = None
}

trait SeriesLike extends GroupMethodLike {
  def projectX(attr: Option[String]) = {
    attr map { a =>
      BsonDocument(s"""{
           |  $$ifNull: [
           |    { $$first: {
           |        $$first: { $$first: "$$$a.obj.iri" }
           |      }
           |    },
           |    { $$cond: {
           |        if: { $$eq: [ { $$first: { $$first: "$$$a.obj.val" } }, "" ] },
           |        then: null,
           |        else: { $$convert: {
           |                  input: { $$first: { $$first: "$$$a.obj.val" } },
           |                  to: "double",
           |                  onError: { $$first: { $$first: "$$$a.obj.val" } },
           |                }
           |              }
           |      }
           |    }
           |  ]
           |}""".stripMargin)
    }
  }
}

case object Each extends SeriesLike with ExpressLike {
  val name = "each"
  final def neededIndex = _ + ".obj.val"
}

case object EachU extends SeriesLike with ExpressLike {
  val name = "eachU"
  override val metaRef = "_iri"
  final def neededIndex = _ + ".obj.iri"
}

case object EachR extends SeriesLike with NoFuncLike {
  val name = "eachR"
  lazy val neededIndex = _ + ".obj.iri"
}

trait TimeSeriesLike extends GroupMethodLike with NoFuncLike {
  final def neededIndex = _ + ".obj.val"
  final protected def dateFromString(attr: String,
                                     suffix: String = "",
                                     onError: String = "null") = {
    //Doesn't work to only parse %Y-%m or %Y
    s"""{
       |  $$dateFromString: {
       |    dateString: { $$concat: [ { $$first: { $$first: "$$$attr.obj.val" } }, "$suffix" ] },
       |    format: "%Y-%m-%d",
       |    onError: $onError
       |  }
       |}""".stripMargin
  }

  protected def onError(attr: String): String

  protected def format: String

  final def projectX(attr: Option[String]) = {
    attr map { a =>
      BsonDocument(s"""{
           |  $$dateToString: {
           |    date: ${dateFromString(a, "", onError(a))},
           |    format: "$format"
           |  }
           |}""".stripMargin)
    }
  }
}

case object DailyTimeSeries extends TimeSeriesLike {
  val name = "dts"

  protected def format: String = "%Y-%m-%d"

  protected def onError(attr: String): String = "null"
}

case object MonthlyTimeSeries extends TimeSeriesLike {
  val name = "mts"

  def format: String = "%Y-%m"

  protected def onError(attr: String): String =
    dateFromString(attr, "-01")
}

case object YearlyTimeSeries extends TimeSeriesLike {
  val name = "yts"

  protected def format: String = "%Y"

  protected def onError(attr: String): String =
    dateFromString(attr, "-01", dateFromString(attr, "-01-01"))
}
