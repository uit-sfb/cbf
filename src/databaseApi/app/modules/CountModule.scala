package modules

import com.github.blemale.scaffeine.{AsyncLoadingCache, LoadingCache, Scaffeine}
import com.typesafe.scalalogging.LazyLogging
import models.binders.Filter
import no.uit.sfb.cbf.mongodb.Collection
import no.uit.sfb.cbf.shared.version.CbfVersion
import org.mongodb.scala.MongoClient.DEFAULT_CODEC_REGISTRY
import org.mongodb.scala.bson.{BsonDocument, Document}

import javax.inject._
import scala.concurrent.{ExecutionContext, Future}
import scala.concurrent.duration._

@Singleton
class CountModule @Inject()(mgc: MongodbClient)(
  implicit executionContext: ExecutionContext
) extends LazyLogging {

  private lazy val cache
    : AsyncLoadingCache[(String, CbfVersion, Filter), Long] =
    Scaffeine()
      .maximumSize(100)
      .expireAfterWrite(7.days) //refreshAfterWrite not working!
      .buildAsyncFuture {
        case (dbName, cbfVersion, filter) =>
          logger.info(
            s"Counting $dbName $cbfVersion -- ${filter.bson(cbfVersion.release).toBsonDocument(classOf[BsonDocument], DEFAULT_CODEC_REGISTRY)}"
          )
          val col: Collection[Document] =
            mgc.cbf(dbName).recordsCol(cbfVersion, filter.neededIndexes)
          val flt = filter.bson(cbfVersion.release)
          col.count(flt)
      }

  def get(dbName: String,
          cbfVersion: CbfVersion,
          filter: Filter): Future[Long] =
    cache
      .get((dbName, cbfVersion, filter))

  def invalidate(dbName: String): Unit = {
    logger.info(s"Invalidating CountModule cache for dataset '$dbName'.")
    val syncCache = cache.synchronous()
    val keys =
      syncCache.asMap().keys.filter { case (name, _, _) => name == dbName }
    syncCache.invalidateAll(keys)
  }

}
