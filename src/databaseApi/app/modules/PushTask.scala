package modules

import akka.Done

import javax.inject.Inject
import akka.actor.{ActorSystem, CoordinatedShutdown}
import no.uit.sfb.scalautils.common.FileUtils

import scala.concurrent.duration.DurationInt
import play.api.inject.SimpleModule
import play.api.inject._

import scala.concurrent.Future

class PushTask @Inject()(actorSystem: ActorSystem,
                         pm: PushModule,
                         executor: TasksCustomExecutionContext,
                         cs: CoordinatedShutdown) {
  val handle = actorSystem.scheduler
    .scheduleWithFixedDelay(initialDelay = 10.seconds, delay = 5.seconds)({
      () =>
        //actorSystem.log.info("Executing push task")
        val triplets = FileUtils.filesUnder(pm.localPath).map { p =>
          (
            p.getParent.getParent.getFileName.toString,
            p.getParent.getFileName.toString,
            p.getFileName.toString.split('.').head
          )
        }
        pm.deleteMissing(triplets)
        triplets foreach { t =>
          pm.report(t._1, t._2, t._3)
        }
    })(executor) // using the custom execution context

  cs.addTask(CoordinatedShutdown.PhaseServiceUnbind, "stop-push-task") { () =>
    Future.successful {
      handle.cancel()
      Done
    }
  }
}

class PushTasksModule extends SimpleModule(bind[PushTask].toSelf.eagerly())
