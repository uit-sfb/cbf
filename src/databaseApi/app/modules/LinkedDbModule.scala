package modules

import com.github.blemale.scaffeine.{AsyncLoadingCache, Scaffeine}
import com.typesafe.scalalogging.LazyLogging
import no.uit.sfb.cbf.utils.graph.{Edge, Node, OrientedGraph}

import javax.inject._
import scala.concurrent.duration._
import scala.concurrent.{Await, ExecutionContext}

@Singleton
class LinkedDbModule @Inject()(mgc: MongodbClient, cm: ConfigModule)(
  implicit executionContext: ExecutionContext
) extends LazyLogging {
  private lazy val cache: AsyncLoadingCache[Unit, OrientedGraph] =
    Scaffeine()
      .maximumSize(1)
      .expireAfterWrite(1.hour) //refreshAfterWrite not working!
      .buildAsyncFuture { _ =>
        mgc
          .shared()
          .datasets
          .getAll()
          .map {
            _.map {
              _._id
            }
          }
          .map {
            _.flatMap { dsName =>
              cm.getDatasetConfig(dsName) map { config =>
                val edges = config.attrs.collect {
                  case attr if attr.lType.isSubtypeOf("dblink:direct") =>
                    Edge(attr.ref, attr.lType.tertiary)
                }
                Node(dsName, edges.toSet)
              }
            }
          }
          .map { nodes =>
            val og = new OrientedGraph(nodes.toSet)
            logger.info(s"DbLinks:\n$og")
            og
          }
      }

  def get(): OrientedGraph = {
    Await.result(cache.get(()), 15.seconds)
  }

  def invalidate(): Unit = {
    logger.info(s"Invalidating LinkedDbModule cache.")
    val syncCache = cache.synchronous()
    syncCache.invalidateAll()
    //We reload straight away to detect potential cycles as early as possible
    get()
  }
}
