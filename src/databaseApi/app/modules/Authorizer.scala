package modules

import models.auth.User
import models.auth.builder.ClientListBuilder
import no.uit.sfb.cbf.shared.errors.{
  ConflictClientError,
  ForbiddenClientError,
  NotFoundClientError,
  UnauthorizedClientError
}
import no.uit.sfb.cbf.mongodb.{CbfDataset, Collection}
import no.uit.sfb.cbf.shared.auth.UserInfo
import no.uit.sfb.cbf.shared.model.v1.role.RoleOrdering
import no.uit.sfb.cbf.shared.version.CbfVersion
import org.mongodb.scala.Document
import org.mongodb.scala.model.{Filters => MongoFilters}
import org.pac4j.core.authorization.authorizer.DefaultAuthorizers
import org.pac4j.core.profile.CommonProfile
import org.pac4j.play.scala.Pac4jScalaTemplateHelper
import play.api.mvc._
import play.api.{Configuration, Logging}

import javax.inject._
import scala.concurrent.duration._
import scala.concurrent.{Await, ExecutionContext, Future}

@Singleton
class Authorizer @Inject()(
  parser: BodyParsers.Default,
  mongoClient: MongodbClient,
  val cm: ConfigModule,
  implicit val ec: ExecutionContext,
  implicit val pac4jTemplateHelper: Pac4jScalaTemplateHelper[CommonProfile],
  implicit val cfg: Configuration
) extends Logging {
  protected val clientBuilder = new ClientListBuilder(cfg)

  //Attention: setting header X-Requested-With: XMLHttpRequest disables direct clients
  //Attention: unless one has already logged in and a profile has been created, AnonymousClient will always have precedence over indirect clients
  //This means that if you want to force using a direct client, you need to remove AnonymousClient from the client list.
  val clients = clientBuilder.clients.mkString(",")

  val clientsWithoutDefault = {
    val tmp =
      clientBuilder.clients.filter { _ != "AnonymousClient" }.mkString(",")
    if (tmp.isEmpty)
      "AnonymousClient"
    else
      tmp
  }

  val bypassSecurity = clientBuilder.directClients == Seq("AnonymousClient")

  val authMode =
    if (bypassSecurity)
      DefaultAuthorizers.IS_ANONYMOUS
    else
      DefaultAuthorizers.IS_AUTHENTICATED

  //Note: anonymous is a valid user
  def isLoggedIn(implicit request: RequestHeader): Boolean = {
    if (bypassSecurity) {
      pac4jTemplateHelper.getCurrentProfile.isDefined
    } else {
      pac4jTemplateHelper.getCurrentProfile
        .map {
          _.getId
        }
        .getOrElse("anonymous") != "anonymous"
    }
  }

  //Throws if not logged in
  //Note: anonymous is a valid user if AnonymousClient is used
  def user(implicit request: RequestHeader): User = {
    pac4jTemplateHelper.getCurrentProfile match {
      case Some(profile) =>
        User(profile)
      case None =>
        throw UnauthorizedClientError(s"No user logged in.")
    }
  }

  protected def perm(dbName: String, userId: String): String = {
    if (bypassSecurity || dbName == "-")
      "owner"
    else {
      val cbf = mongoClient.cbf(dbName)
      val c: Collection[UserInfo] = cbf.authCol
      val rawRole = Await.result(
        c.getOne(MongoFilters.eq("_id", userId))
          .map {
            case Some(userInfo) =>
              userInfo.role
            case None =>
              "none"
          },
        1.minute
      )
      //We elevate the role accordingly grantReadAccess, while making sure it does not go above "reader"
      RoleOrdering.max(
        rawRole,
        RoleOrdering.min(cm.datasetConfig(dbName).grantReadAccess, "reader")
      )
    }
  }

  //get pending role
  protected def pendingRole(dbName: String, userId: String): Option[String] = {
    if (bypassSecurity || dbName == "-")
      None
    else {
      val cbf = mongoClient.cbf(dbName)
      val c: Collection[UserInfo] = cbf.authCol
      val pending = Await.result(c.getOne(MongoFilters.eq("_id", userId)).map {
        _.flatMap { _.pending }
      }, 1.minute)
      //We elevate the role accordingly grantReadAccess, while making sure it does not go above "reader"
      pending
    }
  }

  def isAdmin(implicit request: RequestHeader): Boolean = {
    if (!isLoggedIn)
      false
    else {
      if (bypassSecurity)
        true
      else {
        val coll: Collection[Document] = mongoClient.shared().admin
        Await
          .result(coll.getOne(MongoFilters.eq("_id", user.id)), 1.minute)
          .nonEmpty
      }
    }
  }

  //owner, writer, reader, none or anonymous (if not logged in). Admin is not a dataset role: it is an install wide role.
  def role(dbName: String)(implicit request: RequestHeader): String = {
    if (!isLoggedIn)
      "anonymous"
    else {
      perm(dbName, user.id)
    }
  }

  //owner, writer, reader, none (if not logged in). Empty string if member request is accepted
  def pending(
    dbName: String
  )(implicit request: RequestHeader): Option[String] = {
    if (!isLoggedIn)
      None
    else {
      pendingRole(dbName, user.id)
    }
  }

  protected def isRole(
    role: String
  )(dbName: String)(implicit request: RequestHeader): Boolean = {
    RoleOrdering.lteq(role, perm(dbName, user.id))
  }

  def isOwner(dbName: String)(implicit request: RequestHeader): Boolean = {
    isRole("owner")(dbName)
  }

  def isWriter(dbName: String)(implicit request: RequestHeader): Boolean = {
    isRole("writer")(dbName)
  }

  def isReader(dbName: String)(implicit request: RequestHeader): Boolean = {
    isRole("reader")(dbName)
  }

  val loggedInActionBuilder = new ActionBuilderImpl(parser) {
    override def invokeBlock[A](
      request: Request[A],
      block: Request[A] => Future[Result]
    ): Future[Result] = {
      Future(isLoggedIn(request))(ec).flatMap {
        case true => block(request)
        case false =>
          Future(
            throw UnauthorizedClientError(
              s"Logging in is required to access this resource."
            )
          )(ec)
      }(ec)
    }
  }

  //Checks the header "Authorization" to decide whether to use Action or Secure
  def openActionBuilder(
    secureActionBuilder: ActionBuilder[Request, AnyContent],
    insecureActionBuilder: ActionBuilder[Request, AnyContent]
  ) =
    new ActionBuilderImpl[AnyContent](parser) with Logging {
      override def invokeBlock[A](
        request: Request[A],
        block: Request[A] => Future[Result]
      ): Future[Result] = {
        val actionBuilder =
          if (request.headers.hasHeader("Authorization") || request
                .getQueryString("access-token")
                .nonEmpty)
            secureActionBuilder
          else
            insecureActionBuilder
        actionBuilder.invokeBlock[A](request, block)
      }
    }

  def roleActionBuilder(role: String)(
    dbName: String
  ): ActionBuilderImpl[AnyContent] = new ActionBuilderImpl(parser) {
    override def invokeBlock[A](
      request: Request[A],
      block: Request[A] => Future[Result]
    ): Future[Result] = {
      if (isRole(role)(dbName)(request)) {
        block(request)
      } else {
        logger.info(s"Rejected request '$request' needing role '$role'.")
        Future.failed(
          ForbiddenClientError(
            s"Role '$role' is required to access this resource."
          )
        )
      }
    }
  }

  val traceActionBuilder: ActionBuilderImpl[AnyContent] = new ActionBuilderImpl(
    parser
  ) {
    override def invokeBlock[A](
      request: Request[A],
      block: Request[A] => Future[Result]
    ): Future[Result] = {
      println(
        s"---\nNEW REQUEST $request\nHeaders:\n${request.headers.headers.mkString("\n")}"
      )
      block(request)
    }
  }

  val adminActionBuilder = new ActionBuilderImpl(parser)(ec) {
    override def invokeBlock[A](
      request: Request[A],
      block: Request[A] => Future[Result]
    ): Future[Result] = {
      if (isAdmin(request))
        block(request)
      else {
        logger.info(s"Rejected request '$request' since not admin.")
        Future.failed(
          ForbiddenClientError(
            s"Permission 'admin' is required to access this resource."
          )
        )
      }
    }
  }

  /** Returns the matching CbfVersion (filtered using writer role and onlyDrafts)
    *
    * @param dbName
    * @param version
    * @param onlyDrafts If true, a released version will be rejected
    * @param cbf
    * @param requestHeader
    * @return
    */
  def validatedVersion(dbName: String,
                       version: String,
                       onlyDrafts: Boolean = false)(
    implicit cbf: CbfDataset,
    requestHeader: RequestHeader
  ): Future[CbfVersion] = {
    cbf.versions(isWriter(dbName)).map { versions =>
      val cbfVersion = cbf
        .parseVersion(version)
      val ver = cbfVersion.ver
      val released = versions.filter {
        !_.isDraft
      }
      if (onlyDrafts && released.exists {
            _._id == ver
          }) {
        throw ConflictClientError(
          s"Dataset '$dbName' version '$ver' has already been released and may not be altered."
        )
      }
      if (!versions.exists(_._id == ver))
        throw NotFoundClientError(
          s"Dataset '$dbName' version '$ver' does not exit."
        )
      cbfVersion
    }
  }
}
