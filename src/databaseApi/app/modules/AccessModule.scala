package modules

import com.typesafe.scalalogging.LazyLogging

import javax.inject._
import models.binders.{CiriSearch, Filter, FilterLike, TextSearch}
import no.uit.sfb.cbf.shared.model.v1.nativ.CompactIRI
import no.uit.sfb.cbf.transform.Expressed
import no.uit.sfb.cbf.transform.resolv.Resolution

import scala.concurrent.{ExecutionContext, Future}

/**
  * Convert 'latest' to the latest version
  *
  * @param executionContext
  */

@Singleton
class AccessModule @Inject()(mongoClient: MongodbClient, cm: ConfigModule)(
  implicit executionContext: ExecutionContext
) extends LazyLogging {
  def resolver(dbName: String) = {
    val datasetConfig = cm.datasetConfig(dbName)
    new Resolution(mongoClient.connection, datasetConfig.customResolvers)
  }

  def expression(dbName: String, version: String, apiHost: String) =
    new Expressed(resolver(dbName), dbName, version, apiHost)

  //The search word is replaced by possible CIRIs
  //This is wanted when we follow the CIRI search by a text search
  //Return the original text filter when no CIRI was found for the search query
  def ciriFilter(filter: Filter, dbName: String): Future[Filter] = {
    val resolv = resolver(dbName)
    val datasetConfig = cm.datasetConfig(dbName)
    Future
      .traverse(filter.filters) {
        //use plain text search for ebp databases TODO: remove this and fix ciri search for EBP
        case flt @ TextSearch(_, _) if dbName.contains("ebp-") => Future.successful(flt)
        //flt is bind to TextSearch
        case flt @ TextSearch(raw_search, _) =>
          val search = raw_search.replace("\"", "")
          //Firstly, we search for exact ciri or value match in persisted resolvers
          (CompactIRI(search) match {
            case Some(ciri) =>
              resolv.getOption(ciri).map {
                _.map { _ =>
                  ciri
                }
              }
            case None =>
              Future.successful(None)
          }).map {
              _.toSet
            }
            .flatMap { primary =>
              //If already valid CIRI, then don't search further
              if (primary.nonEmpty) {
                Future.successful(primary)
              } else {
                //Otherwise search in persisted ciris
                resolv
                  .search(search)
              }
            }
            .map { ciris =>
              //We get the list of attributes that may contain the ciris we found
              val attrss = {
                //If we did not find any persisted ciris, we assume all unpersisted resolvers as potential candidates
                val extCiris = if (ciris.isEmpty) {
                  Resolution
                    .resolvers(datasetConfig.customResolvers)
                    .filter { resolv =>
                      (!resolv.persistable) && resolv.regexp.matches(search)
                    }
                    .map { resolv =>
                      CompactIRI(resolv.prefix, search)
                    }
                } else
                  ciris
                extCiris.map { ciri =>
                  ciri ->
                    datasetConfig.persistedAttributes.collect {
                      case attr
                          if attr.resolvers
                            .map {
                              _.toLowerCase
                            }
                            .contains(ciri.prefix) =>
                        attr.ref
                    }
                }.toMap
              }
              if (attrss.isEmpty)
                flt
              else
                FilterLike.or(CiriSearch(attrss), flt)
            }
        case flt => Future.successful(flt)
      }
      .map {
        Filter(_)
      }
  }
}
