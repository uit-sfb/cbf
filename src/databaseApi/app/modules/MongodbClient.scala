package modules

import akka.Done
import akka.actor.CoordinatedShutdown
import com.github.blemale.scaffeine.{LoadingCache, Scaffeine}
import no.uit.sfb.cbf.shared.errors.NotFoundClientError

import javax.inject.{Inject, _}
import no.uit.sfb.cbf.mongodb.{CbfDataset, MongodbConnect, SharedDatabase}
import org.mongodb.scala.MongoDatabase
import org.mongodb.scala.model.Filters
import play.api.Configuration

import scala.concurrent.duration._
import scala.concurrent.{Await, ExecutionContext, Future, blocking}

@Singleton
class MongodbClient @Inject()(cfg: Configuration, cs: CoordinatedShutdown)(
  implicit executionContext: ExecutionContext
) {
  val connection = MongodbConnect(
    cfg.get[String]("mongodb.host"),
    cfg.get[String]("mongodb.userName"),
    cfg.get[String]("mongodb.userPassword"),
    ssl = cfg.get[Boolean]("mongodb.ssl")
  )

  def listDbs(): Future[Seq[String]] = {
    connection.listDb
  }

  //Use cbf() instead
  private def db(dbName: String): MongoDatabase = {
    connection.db(dbName)
  }

  private lazy val cbfCache: LoadingCache[String, CbfDataset] =
    Scaffeine()
      .maximumSize(10)
      .expireAfterAccess(1.hours)
      .build { dbName =>
        new CbfDataset(db(dbName))
      }

  def exists(dbName: String): Boolean = {
    Await
      .result(shared().datasets.getOne(Filters.eq("_id", dbName)), 15.seconds)
      .nonEmpty
  }

  def cbf(dbName: String): CbfDataset = {
    if (!exists(dbName))
      throw NotFoundClientError(s"Dataset '$dbName' does not exist.")
    cbfCache.get(dbName)
  }

  def shared(dbName: String = "_cbf") = SharedDatabase(connection, dbName)

  cs.addTask(
    CoordinatedShutdown.PhaseServiceStop,
    "Close connection to MongoDb"
  ) { () =>
    Future {
      blocking {
        connection.close
        Done
      }
    }
  }

  def checkClient(): Future[Unit] = {
    Future {
      blocking {
        connection.listDb
        ()
      }
    }
  }
}
