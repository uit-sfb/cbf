package modules

import com.typesafe.scalalogging.LazyLogging

import javax.inject._
import no.uit.sfb.cbf.mongodb.Collection
import no.uit.sfb.cbf.shared.dataset.DatasetRegistration
import no.uit.sfb.cbf.shared.model.v1.role.RoleOrdering
import play.api.mvc.RequestHeader

import scala.concurrent.{ExecutionContext, Future}
import scala.util.Try

@Singleton
class DiscoverModule @Inject()(
  mongoClient: MongodbClient,
  implicit val authorizer: Authorizer
)(implicit executionContext: ExecutionContext)
    extends LazyLogging {

  def datasets(
    implicit request: RequestHeader
  ): Future[Map[String, (String, Boolean, String, String)]] = {
    val col: Collection[DatasetRegistration] = mongoClient.shared().datasets
    col.getAll().map { dbs =>
      dbs.flatMap { db =>
        val dbName = db._id
        Try {
          val oConfig = mongoClient.cbf(dbName).config()
          val isDemo = false //db.capped > 0 || db.deleteAfter > 0 //Not implemented
          val role = authorizer.role(dbName)
          if (authorizer.isAdmin || RoleOrdering.gteq(role, "reader"))
            Some(
              dbName -> (role, isDemo, oConfig
                .map {
                  _.description
                }
                .getOrElse(""), oConfig
                .map {
                  _.contextUrl
                }
                .getOrElse(""))
            )
          else
            None
        }.recover {
            case e =>
              logger.error(
                s"Either '$dbName' does not exist or it contains forbidden characters."
              )
              None
          }
          .toOption
          .flatten
      }.toMap
    }
  }
}
