package modules

import com.github.blemale.scaffeine.Scaffeine
import no.uit.sfb.cbf.shared.version.CbfVersion
import org.mongodb.scala.bson.Document

import java.util.UUID
import java.util.concurrent.atomic.AtomicInteger
import javax.inject._
import scala.concurrent.duration._
import scala.concurrent.{Await, ExecutionContext, Future}

@Singleton
class IdProviderModule @Inject()(mgc: MongodbClient)(
  implicit executionContext: ExecutionContext
) {

  private val seedCache = Scaffeine()
    .maximumSize(100)
    .build[String, AtomicInteger] { dsName: String =>
      val mdb = mgc.cbf(dsName)
      mdb.counterKeySeed
    }

  def getNext(dsName: String): String = {
    val mdb = mgc.cbf(dsName)
    val cfg = mdb.config().get
    val idAttr = cfg.attr("id").get
    if (idAttr.lType.isSubtypeOf("num")) {
      val i = seedCache.get(dsName).incrementAndGet()
      if (i > 999999999)
        throw new Exception("Automatic Id range may include at most 9 digits.")
      //If this exception happens, the solution is to increase the number of digits below.
      f"$i%09d"
    } else {
      UUID.randomUUID().toString
    }
  }
}
