package modules

import com.typesafe.scalalogging.LazyLogging
import models.push._
import no.uit.sfb.cbf.bulktransform.{TrPatch, TrPush, TransformPushErrorsLike}
import no.uit.sfb.cbf.shared.utils.IteratorWithCallback
import no.uit.sfb.cbf.shared.version.CbfVersion
import no.uit.sfb.scalautils.common.FileUtils
import play.api.Configuration

import java.io.{BufferedInputStream, File, FileInputStream}
import java.nio.file.{Files, Paths}
import javax.inject._
import scala.collection.mutable
import scala.concurrent.duration.DurationInt
import scala.concurrent.{Await, ExecutionContext, Future}
import scala.io.{BufferedSource, Source => IoSource}
import scala.util.Try

@Singleton
class PushModule @Inject()(
  cfg: Configuration,
  mongoClient: MongodbClient,
  cm: CountModule,
  gm: GraphModule,
  idProviderModule: IdProviderModule
)(implicit executionContext: ExecutionContext)
    extends LazyLogging {

  val localPath = Paths.get(cfg.get[String]("app.paths.local"))
  Files.createDirectories(localPath)
  val tb = TaskContextBuilder(localPath)

  //Key is dsName/ver
  protected val tasks = mutable.LinkedHashMap[String, TaskContext]()

  protected def key(dbName: String, ver: String) = s"$dbName/$ver"

  def active(dbName: String, ver: String): Boolean =
    task(dbName: String, ver: String).exists { _.state.active }

  def task(dbName: String, ver: String): Option[TaskContext] = synchronized {
    tasks.get(key(dbName, ver))
  }

  def resetTask(dbName: String, ver: String, method: String): Unit =
    synchronized {
      val t = tb.build(dbName, ver, method)
      tasks.addOne(key(t.dbName, t.ver), t)
    }

  //For incr, we need to get the latest value, that is why update takes a function TaskContext => TaskContext
  def updateTask(dbName: String, ver: String)(
    f: TaskContext => TaskContext
  ): Unit = synchronized {
    val t = task(dbName, ver).get
    tasks.addOne(key(t.dbName, t.ver), f(t))
  }

  def activateTask(dbName: String, ver: String, method: String): Unit = {
    val t = synchronized {
      val tmp = task(dbName, ver).getOrElse(tb.build(dbName, ver, method))
      tasks.addOne(key(dbName, ver), tmp.copy(state = Running))
      tmp
    }
    runTask(t)
  }

  protected def runTask(t: TaskContext): Unit = {
    val file = new File(t.path)
    def end(): Unit = {
      val db = mongoClient.cbf(t.dbName)
      val errors =
        Await.result(db.errorsCol(CbfVersion(t.ver)).count(), 1.minute)
      if (errors != 0L) {
        logger.info(s"$errors errors found!")
        updateTask(t.dbName, t.ver)(_.newState(Failed).copy(errors = errors))
      } else {
        logger.info("Successfully wrote all records to Mongodb")
        updateTask(t.dbName, t.ver)(_.newState(Success))
      }
      Files.deleteIfExists(file.toPath)
    }
    val io: BufferedSource = IoSource.fromInputStream(
      FileUtils.unGzipStream(
        new BufferedInputStream(new FileInputStream(file))
      ),
      t.encoding
    )
    val inputIt = new IteratorWithCallback(io.getLines(), io.close())
    val transform = t.method match {
      case "push" =>
        val tr = new TrPush(
          mongoClient.connection,
          t.dbName,
          t.ver,
          inputIt,
          idProviderModule.getNext(t.dbName)
        ) with TransformPushErrorsLike {
          override def afterBatch(): Unit = {
            super.afterBatch()
            updateTask(t.dbName, t.ver)(_.incr(grpSize))
          }

          override protected def post(): Unit = {
            end()
          }
        }
        Some(tr)
      case "patch" =>
        val tr = new TrPatch(
          mongoClient.connection,
          t.dbName,
          t.ver,
          inputIt,
          idProviderModule.getNext(t.dbName)
        ) with TransformPushErrorsLike {
          override def afterBatch(): Unit = {
            super.afterBatch()
            updateTask(t.dbName, t.ver)(_.incr(grpSize))
          }

          override protected def post(): Unit = {
            end()
          }
        }
        Some(tr)
      case _ =>
        logger.warn(s"Method '${t.method}' not implemented. Do nothing.")
        None
    }
    Future {
      Try {
        transform
          .map {
            _.run()
          }
          .map { _ =>
            cm.invalidate(t.dbName)
            gm.invalidate(t.dbName, CbfVersion(t.ver))
          }
      } recover {
        case e =>
          logger.warn(
            s"An error occurred during the execution of push task '${t.dbName}/${t.ver}'.",
            e
          )
          updateTask(t.dbName, t.ver)(_.newState(Failed).copy(errors = 1))
          Files.deleteIfExists(file.toPath)
      }
    }
  }

  def report(dbName: String, ver: String, method: String): Unit = {
    val tb = TaskContextBuilder(localPath)
    task(dbName, ver) match {
      case Some(t) if t.state.active =>
        //Already registered and running
        if (t.method != method) {
          //Should not happen!
          val file = new File(tb.build(dbName, ver, method).path)
          Files.delete(file.toPath)
        }
      case _ =>
        activateTask(dbName, ver, method)
    }
  }

  //We delete active tasks that do not have input files
  def deleteMissing(triplets: Seq[(String, String, String)]) =
    synchronized {
      val keys = triplets.map { case (dbName, ver, _) => key(dbName, ver) }
      tasks.filterInPlace {
        case (k, t) if t.state.active || t.state == Starting =>
          val keep = keys.contains(k)
          if (!keep)
            logger.warn(
              s"Deleting $k as its input file is missing. The occurrence of this message may indicate that another instance of push task is already running."
            )
          keep
        case _ => true
      }
    }
}
