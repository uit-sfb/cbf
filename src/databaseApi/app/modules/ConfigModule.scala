package modules

import com.github.blemale.scaffeine.{AsyncLoadingCache, LoadingCache, Scaffeine}
import com.typesafe.scalalogging.LazyLogging
import no.uit.sfb.cbf.shared.errors.NotFoundClientError

import javax.inject._
import no.uit.sfb.cbf.shared.config.{AttributeDef, DatasetConfig}
import no.uit.sfb.cbf.shared.model.v1.builder.RecordBuilder
import org.mongodb.scala.model.Filters

import scala.concurrent.duration._
import scala.concurrent.{Await, ExecutionContext}
import scala.util.Try

@Singleton
class ConfigModule @Inject()(mgc: MongodbClient)(
  implicit executionContext: ExecutionContext
) extends LazyLogging {

  //Cache of current configs
  private lazy val cache: LoadingCache[String, DatasetConfig] =
    Scaffeine()
      .maximumSize(100)
      .expireAfterWrite(1.hour) //refreshAfterWrite not working!
      .build { dbName =>
        val f = mgc
          .shared()
          .datasets
          .getOne(Filters.eq("_id", dbName))
          .map {
            case Some(_) =>
              Try { mgc.cbf(dbName).config() }.toOption.flatten
                .getOrElse(
                  throw new Exception(s"No config found for '$dbName'.")
                )
            case None =>
              throw NotFoundClientError(
                s"Dataset '$dbName' does not exist (it should be registered in _cbf/datasets)."
              )
          }
        Await.result(f, 15.seconds)
      }

  def invalidate(dbName: String): Unit = {
    logger.info(s"Invalidating ConfigModule cache for dataset '$dbName'.")
    cache.invalidate(dbName)
  }

  def getDatasetConfig(dbName: String): Option[DatasetConfig] = {
    Try { datasetConfig(dbName) }.toOption
  }

  def datasetConfig(dbName: String): DatasetConfig =
    cache
      .get(dbName)

  def rb(dbName: String,
         attrWhiteList: AttributeDef => Boolean = _ => true): RecordBuilder = {
    val dbConf = cache
      .get(dbName)

    new RecordBuilder(
      dbConf.persistedAttributes.filter(attrWhiteList),
      dbConf.valueSeparatorChar,
      dbConf.subValueSeparators
    )
  }
}
