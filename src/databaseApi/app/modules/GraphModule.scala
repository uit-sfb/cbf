package modules

import com.github.blemale.scaffeine.{AsyncLoadingCache, Scaffeine}
import com.typesafe.scalalogging.LazyLogging
import models.binders.Graph
import no.uit.sfb.cbf.shared.version.CbfVersion
import org.mongodb.scala.Document
import org.mongodb.scala.MongoClient.DEFAULT_CODEC_REGISTRY
import org.mongodb.scala.bson.{BsonArray, BsonDocument, BsonValue}

import javax.inject._
import scala.concurrent.{ExecutionContext, Future}
import scala.concurrent.duration.DurationInt

/**
  * Convert 'latest' to the latest version
  *
  * @param executionContext
  */

@Singleton
class GraphModule @Inject()(mongoClient: MongodbClient, am: AccessModule)(
  implicit executionContext: ExecutionContext
) extends LazyLogging {

  def get(dsName: String, ver: CbfVersion, graph: Graph) =
    cache.get(dsName, ver, graph)

  def invalidate(dsName: String, ver: CbfVersion, graph: Graph): Unit = {
    logger.info(
      s"Invalidating GraphModule cache for $dsName ${ver.ver} (a specific graph)."
    )
    cache.synchronous().invalidate(dsName, ver, graph)
  }

  def invalidate(dsName: String, ver: CbfVersion): Unit = {
    val filteredKeys = cache.synchronous().asMap().keys.filter {
      case (`dsName`, `ver`, _) => true
      case _                    => false
    }
    logger.info(s"Invalidating GraphModule cache for $dsName ${ver.ver}.")
    cache.synchronous().invalidateAll(filteredKeys)
  }

  protected lazy val cache
    : AsyncLoadingCache[(String, CbfVersion, Graph), Document] =
    Scaffeine()
      .maximumSize(100)
      .expireAfterAccess(7.days)
      .buildAsyncFuture {
        case o @ (dbName, cbfVersion, g) =>
          logger.info(
            s"Building graph $dbName $cbfVersion -- ${g.filter.bson(cbfVersion.release).toBsonDocument(classOf[BsonDocument], DEFAULT_CODEC_REGISTRY)} (some details are hidden)."
          )
          buildGraph(dbName, cbfVersion, g)
      }

  protected def buildGraph(dbName: String, version: CbfVersion, g: Graph) = {
    implicit val resolver = am.resolver(dbName)
    val cdb = mongoClient.cbf(dbName)
    am.ciriFilter(g.filter, dbName).flatMap { extFilter =>
      val updatedGraph = g.copy(filter = extFilter)
      Future {
        cdb
          .recordsCol(version, g.neededIndexes)
          .aggregate(updatedGraph.pipeline(version.release), true)
          .toSeq //Unfortunately it is not possible to go trough the transpose and other reduce functions without materializing the iterator
      }.flatMap { docs =>
        //x and Ys
        val all: Seq[
          (String,
           String => Seq[BsonValue] => Future[Seq[Map[String, BsonValue]]])
        ] = (updatedGraph.xDef.key -> updatedGraph.xDef.method
          .func(dbName, version.ver) _) +: updatedGraph.yDefs
          .map { yDef =>
            yDef.key -> yDef.method.func(dbName, version.ver) _
          }
        val k = Future
          .sequence(all.map {
            case (key, func) =>
              //Future[Seq[Map[String, BsonValue]]]
              func(key)(docs.map { doc =>
                doc(key)
              })
          })
        val updatedDocs = k.map { pts =>
          pts.transpose
            .map {
              _.flatten
            }
            .map { s =>
              Document(s)
            }
        }
        updatedDocs.map { docs =>
          //If docs need to be sorted post CIRI translation, do it here using BsonSort
          //I've no idea why BsonArray wrap twice in an array...
          val array = BsonArray(docs).getValues.get(0)
          Document("graph" -> array)
        }
      }
    }
  }
}
