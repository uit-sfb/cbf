package modules

import play.api.libs.Files.logger

import javax.inject.{Inject, Singleton}
import play.api.libs.mailer._

import scala.util.Try

@Singleton
class MailerModule @Inject()(mailerClient: MailerClient) {

  def send(receiver: String, title: String, content: String = "") = {
    val email = Email(
      title,
      "noreply@cbf.sfb.uit.no",
      Seq(s"TO <$receiver>"),
      bodyText = Some(s"""Dear CBF user,
             |
             |$content
             |
             |Best regards,
             |UiT@elixir.no team
             |""".stripMargin)
    )
    Try {
      mailerClient.send(email)
    } recover {
      case e =>
        logger.warn("Could not send email", e)
    }
  }
}
