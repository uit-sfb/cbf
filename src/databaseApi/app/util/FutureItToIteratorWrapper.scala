package util

import scala.concurrent.Future
import scala.util.{Failure, Success}

/**
  * Wrapper for a Future of Iterator which implements an Iterator returning the provided zero value as long as the future did not complete
  * or the values of the returned Iterator if available.
  *
  * Note: zero should include some kind of waiting in order to not exhaust the CPU.
  *
  * @param f
  * @param zero
  * @tparam T
  */
class FutureItToIteratorWrapper[T](f: Future[Iterator[T]], zero: => T)
    extends Iterator[T] {
  private var consummedLast = false

  def hasNext = !consummedLast
  def next() = {
    f.value match {
      case Some(Success(it)) =>
        if (it.hasNext) {
          val v = it.next()
          if (it.hasNext)
            consummedLast = true
          v
        } else {
          throw new NoSuchElementException()
        }
      case Some(Failure(e)) =>
        throw e
      case None =>
        zero
    }
  }
}
