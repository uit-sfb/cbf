package util

import scala.concurrent.Future
import scala.util.{Failure, Success}

/**
  * Wrapper for a Future which implements an Iterator returning the provided zero value as long as the future did not complete
  * or the successful value if available.
  *
  * Note: zero should include some kind of waiting in order to not exhaust the CPU.
  *
  * @param f
  * @param zero
  * @tparam T
  */
class FutureToIteratorWrapper[T](f: Future[T], zero: => T) extends Iterator[T] {
  private var consummedLast = false

  def hasNext = !consummedLast
  def next() = {
    f.value match {
      case Some(Success(v)) =>
        consummedLast = true
        v
      case Some(Failure(e)) =>
        consummedLast = true
        throw e
      case None =>
        zero
    }
  }
}
