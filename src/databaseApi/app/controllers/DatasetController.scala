package controllers

import io.circe.generic.auto._
import models.auth.SecurityLike
import no.uit.sfb.cbf.shared.errors.{BadRequestClientError, ConflictClientError}
import modules.{Authorizer, ConfigModule, MongodbClient}
import no.uit.sfb.cbf.shared.jsonapi.{
  Data,
  MultipleResponse,
  ResponseLike,
  SingleResponse
}
import play.api.libs.concurrent.Futures
import play.api.libs.json.Json
import play.api.mvc._
import play.api.{Configuration, Logging}

import javax.inject._
import scala.concurrent.ExecutionContext
import models.json.JsonApi._
import no.uit.sfb.cbf.shared.auth.UserInfo
import org.mongodb.scala.model
import org.pac4j.core.profile.CommonProfile
import org.pac4j.play.scala.{SecureAction, SecurityComponents}
import no.uit.sfb.cbf.shared.config.{DatasetConfig, DefaultAttrs, Tab}
import no.uit.sfb.cbf.shared.dataset.{
  DatasetRegistration,
  DatasetRegistrationForm
}
import no.uit.sfb.cbf.shared.utils.Date
import no.uit.sfb.scalautils.common.FileUtils
import org.mongodb.scala.bson.Document

import java.nio.file.Paths

@Singleton
class DatasetController @Inject()(val controllerComponents: SecurityComponents,
                                  mongoClient: MongodbClient,
                                  cm: ConfigModule,
                                  val authorizer: Authorizer,
                                  implicit val f: Futures,
                                  implicit val cfg: Configuration,
                                  implicit val ec: ExecutionContext)
    extends SecurityLike
    with Logging {
  private val resourceType = "datasets"

  def getDatasets() = admin.async { implicit request: Request[AnyContent] =>
    mongoClient.shared().datasets.getAll() map { ddSeq =>
      val rawResp =
        MultipleResponse[DatasetRegistration](ddSeq.map { dd =>
          Data[DatasetRegistration](resourceType, dd._id, dd)
        })
      val self = routes.DatasetController
        .getDatasets()
        .absoluteURL()
      val resp =
        rawResp.copy(links = rawResp.links ++ Map("self" -> Some(self)))
      val content = Json.toJson(resp)
      Ok(content).as(ResponseLike.contentType)
    }
  }

  def getDataset(dbName: String) = admin.async {
    implicit request: Request[AnyContent] =>
      mongoClient
        .shared()
        .datasets
        .getOne(model.Filters.eq("_id", dbName))
        .map { oDd =>
          val rawResp = SingleResponse[DatasetRegistration](oDd.map { dd =>
            Data[DatasetRegistration](resourceType, dd._id, dd)
          })
          val self = routes.DatasetController
            .getDataset(dbName)
            .absoluteURL()
          val resp =
            rawResp.copy(links = rawResp.links ++ Map("self" -> Some(self)))
          val content = Json.toJson(resp)
          Ok(content).as(ResponseLike.contentType)
        }
  }

  def createDataset() =
    secure.async { implicit request =>
      request.body.asJson.flatMap {
        _.as[SingleResponse[DatasetRegistrationForm]].data
      } match {
        case Some(data) =>
          if (data.`type` != resourceType) {
            throw BadRequestClientError(
              s"Data submitted with type '${data.`type`}', while this resource is '$resourceType'."
            )
          } else if (data.id.nonEmpty)
            throw BadRequestClientError(
              s"Data submitted with an id, while this resource does not support client-generated ids."
            )
          else {
            val drf = data.attributes
            DatasetRegistrationForm.validate(drf)
            val dd = DatasetRegistration(
              drf.name,
              creationDate = Date.nowDate(),
              lastAccess = Date.nowDate(),
              deleteAfter = 180,
              capped = 1000
            )
            val dbName = dd._id
            if (mongoClient.exists(dbName)) {
              throw ConflictClientError(s"Db '$dbName' already exists.")
            } else {
              mongoClient.shared().datasets.insert(dd).flatMap { res =>
                val defaultCfg =
                  DatasetConfig(
                    _id = Date.nowDate(),
                    name = dd._id,
                    description = drf.description,
                    dmp = drf.dmp,
                    attrs = DefaultAttrs(),
                    tabs = Seq(
                      Tab(
                        name = "Record metadata",
                        attrs = Seq(
                          "_time:creation_date",
                          "_time:update_date",
                          "_time:implementation_date",
                          "_time:release_version"
                        )
                      )
                    )
                  )
                val cbf = mongoClient.cbf(dbName)
                cbf.configsCol
                  .insert(Document(Json.toJson(defaultCfg).toString()))
                  .flatMap { res =>
                    val user = authorizer.user
                    cbf.authCol
                      .insert(UserInfo(user.id, user.email, "owner"))
                      .map { _ =>
                        Created("")
                      }
                  }
              }
            }
          }
        case None =>
          throw BadRequestClientError("No data or wrong format received.")
      }
    }

  def deleteDataset(dbName: String) = {
    owner(dbName).async { implicit request: Request[AnyContent] =>
      val localPath = Paths.get(cfg.get[String]("app.paths.local"))
      FileUtils.deleteDirIfExists(localPath.resolve(dbName))
      val cbf = mongoClient.cbf(dbName)
      cbf
        .drop()
        .map { _ =>
          mongoClient
            .shared()
            .datasets
            .delete(model.Filters.eq("_id", dbName), true)
        }
        .map { _ =>
          cm.invalidate(dbName)
          NoContent.as(ResponseLike.contentType)
        }
    }
  }
}
