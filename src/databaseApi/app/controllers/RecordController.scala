package controllers

import java.io.ByteArrayInputStream
import akka.NotUsed
import akka.stream.scaladsl.{Source, StreamConverters}

import javax.inject._
import models.binders.{IsIn, Pager, Projector, Sorter, Filter => FilterP}
import no.uit.sfb.cbf.shared.errors._
import models.json.JsonApi._
import io.circe.generic.auto._
import models.auth.SecurityLike
import modules._
import no.uit.sfb.cbf.bulktransform.{TrPatch, TrPush}
import no.uit.sfb.cbf.mongodb.Collection
import org.mongodb.scala.bson.Document
import play.api.libs.concurrent.Futures
import play.api.mvc._
import play.api.{Configuration, Logging}

import scala.concurrent.{Await, ExecutionContext, Future}
import play.api.libs.json.Json

import scala.concurrent.duration._
import no.uit.sfb.cbf.shared.jsonapi._
import no.uit.sfb.cbf.jsonld.DataRecord
import no.uit.sfb.cbf.shared.config.AttributeDef
import no.uit.sfb.cbf.shared.model.v1.builder.RecordBuilder
import no.uit.sfb.cbf.shared.model.v1.nativ.Record
import no.uit.sfb.cbf.shared.utils.JsonArray
import no.uit.sfb.cbf.shared.version.CbfVersion
import no.uit.sfb.cbf.transform.{Expressed, Linked}
import no.uit.sfb.cbf.utils.JsonLdSerializer
import org.pac4j.play.scala.SecurityComponents

import scala.collection.parallel.CollectionConverters.ImmutableSeqIsParallelizable
import scala.jdk.CollectionConverters.SeqHasAsJava
import scala.util.Try

@Singleton
class RecordController @Inject()(val controllerComponents: SecurityComponents,
                                 mongoClient: MongodbClient,
                                 am: AccessModule,
                                 cfgMod: ConfigModule,
                                 cm: CountModule,
                                 gm: GraphModule,
                                 ldbm: LinkedDbModule,
                                 idProviderModule: IdProviderModule,
                                 implicit val authorizer: Authorizer,
                                 implicit val futures: Futures,
                                 implicit val cfg: Configuration,
                                 implicit val ec: ExecutionContext)
    extends SecurityLike
    with Logging {
  private val resourceType = "records"
  private val grpSize = 100 //Higher values increases mem consumption without increasing speed

  //If you want to return an error message to the client, please throw an ErrorLike

  private def jsonapiResponse(records: Iterator[Document])(
    implicit rb: RecordBuilder,
    ex: Expressed,
    linker: Linked
  ): MultipleResponse[Record] = {
    MultipleResponse[Record](
      records
        .grouped(grpSize)
        .flatMap { docs =>
          docs.par.map { doc =>
            val id = doc.getString("id")
            val rec = rb.fromSingleJson(doc.toJson())
            val expressed = ex(linker(rec))
            Data[Record](resourceType, id, expressed)
          }
        }
        .toSeq
    )
  }

  private def jsonldResponse(
    records: Iterator[Document],
    url: String => String
  )(implicit rb: RecordBuilder, linker: Linked): Iterator[String] = {
    val it = records.grouped(grpSize).flatMap { recs =>
      recs.par.map { doc =>
        val id = doc.getString("id")
        val rec = rb.fromSingleJson(doc.toJson())
        val dr = DataRecord(linker(rec), Some(url(id)))
        JsonLdSerializer.serialize(dr)
      }
    }
    JsonArray.toJson[String](it, identity)
  }

  private def jsonResponse(records: Iterator[Document], compact: Boolean)(
    implicit rb: RecordBuilder,
    ex: Expressed,
    linker: Linked
  ): Iterator[String] = {
    val it = records.grouped(grpSize).flatMap { docs =>
      docs.par.map { doc =>
        val rec = rb.fromSingleJson(doc.toJson())
        ex(linker(rec))
      }
    }
    JsonArray.toJson[Record](it, _.serialize(!compact))
  }

  private def tsvResponse(records: Iterator[Document])(
    implicit rb: RecordBuilder,
    ex: Expressed,
    linker: Linked
  ): Iterator[String] = {
    val parsedRecords = records.grouped(grpSize).flatMap { docs =>
      docs.par.map { doc =>
        val rec = rb.fromSingleJson(doc.toJson())
        ex(linker(rec))
      }
    }
    rb.toTsv(parsedRecords)
  }

  def records(dsName: String,
              oVer: Option[String],
              pager: Pager,
              sorter: Sorter,
              projector: Projector,
              filter: FilterP,
              format: String,
              inline: Boolean,
              quick: Boolean,
              curatorView: Boolean): Action[AnyContent] = reader(dsName).async {
    implicit request: Request[AnyContent] =>
      def self(ver: Option[String], p: Pager) =
        routes.RecordController
          .records(
            dsName,
            ver,
            p,
            sorter,
            projector,
            filter,
            format,
            inline,
            quick
          )

      implicit val cdb = mongoClient.cbf(dsName)
      val compact = request.headers
        .get("X-Requested-With")
        .contains("XMLHttpRequest")
      oVer match {
        case Some(version) =>
          //Because the pipeline makes queries before release, we need to disable
          //auth check and return draft versions.
          //authorizer.validatedVersion(dsName, version)
          cdb
            .version(version, true)
            .map {
              case Some(vi) =>
                cdb.parseVersion(vi._id)
              case None =>
                throw BadRequestClientError(
                  s"Version '$version' does not exist for dataset '$dsName'."
                )
            } flatMap { cbfVersion =>
            def attrWhiteList(attr: AttributeDef): Boolean = {
              val whiteList = projector.fields.flatMap { _._2 }.toSeq
              if (whiteList.nonEmpty)
                whiteList
                  .contains(attr.ref) //We are in the case of a projection
              else {
                if (attr.sticky || attr.ref == "id") //Legacy: we make "id" attr sticky
                  true
                else {
                  if (curatorView)
                    !attr.isFullyControlled
                  else
                    !attr.hidden
                }
              }
            }
            val ver = cbfVersion.ver
            implicit val rbInstance =
              cfgMod.rb(dsName, attrWhiteList)
            implicit val expressor = am.expression(dsName, ver, request.host)
            implicit val linker = new Linked(
              ldbm.get(),
              dsName,
              cfgMod.datasetConfig(dsName),
              getLatestRecord
            )
            val f = am.ciriFilter(filter, dsName) map { flt =>
              val emptyFlt = FilterP() //Used for counting the total number of unfiltered records
              lazy val emptyFilterCountF = cm
                .get(dsName, cbfVersion, emptyFlt)
                .map {
                  _.toInt
                }
              lazy val filterCountF = cm
                .get(dsName, cbfVersion, flt)
                .map {
                  _.toInt
                }
              val ciriFilter = flt.bson(cbfVersion.release)
              val fs =
                if (emptyFlt == flt)
                  emptyFilterCountF zip emptyFilterCountF
                else
                  filterCountF zip emptyFilterCountF
              fs -> ciriFilter
            }
            val col: Collection[Document] =
              cdb.recordsCol(cbfVersion, filter.neededIndexes)
            //Collation prevents Mongo from using keys!!
            /*val collation = Collation
                .builder()
                .locale("en_US")
                .numericOrdering(true)
                .build()*/
            f.map {
              case (fNumRecords, ciriFilter) =>
                //Note: Because Play actions are single threaded, we are actually waiting for the future anyways
                lazy val (numRecords, unfilteredNumRecords) =
                  Await.result(fNumRecords, 15.minutes)
                lazy val skip =
                  if (pager.offset < 0)
                    numRecords + pager.offset
                  else
                    pager.offset
                lazy val limit =
                  if (pager.size < 0)
                    numRecords - skip
                  else
                    pager.size

                def expressNumRecordCalc(numRecs: Int): (Int, Int) = {
                  val a =
                    if (fNumRecords.isCompleted || !quick)
                      numRecords
                    else if (numRecs < limit)
                      numRecs
                    else
                      -1
                  val b =
                    if (fNumRecords.isCompleted || !quick)
                      unfilteredNumRecords
                    else
                      -1
                  a -> b
                }

                //Value used to decide whether we want to enable the automatic sorting or not
                lazy val pseudoNumRec = fNumRecords.value.flatMap{
                  _.toOption.map{_._1}
                }.getOrElse(Int.MaxValue)

                //We use a default sorting (by id), but only if there is no filter or the amount of records is not too large (for performance reasons)
                val sorting = if (sorter.isEmpty && (filter.isEmpty || pseudoNumRec < 10_000))
                  Sorter.default
                else
                  sorter

                lazy val recs = {
                  col.getIt(
                    filter = ciriFilter,
                    project = projector.bson,
                    sort = sorting.bson,
                    //collation = collation,
                    skip = Some(skip),
                    limit = Some(limit)
                  )
                }
                format match {
                  case "jsonapi" =>
                    val strictLimit = 500
                    if (limit > strictLimit)
                      throw BadRequestClientError(
                        s"Cannot display more than $strictLimit records in jsonapi. Please set 'page[size]' to a value smaller than $strictLimit, or use a format meant for exporting data such as TSV, JSON, or JsonLd using the 'format=' query string parameter"
                      )
                    val rawResp = jsonapiResponse(recs)
                    val numRecs = rawResp.data.size
                    lazy val (expressNumRecord, expressUnfilteredNumRecords) =
                      expressNumRecordCalc(numRecs)
                    val itemStart = if (numRecs == 0) 0 else skip + 1
                    val itemEnd =
                      if (numRecs == 0) 0 else itemStart + numRecs - 1
                    val resp = rawResp.copy(
                      meta = rawResp.meta ++ Map(
                        "itemStart" -> itemStart.toString,
                        "itemEnd" -> itemEnd.toString,
                      ) ++
                        (if (expressNumRecord >= 0) {
                           Map(
                             "totalFilteredItems" -> expressNumRecord.toString
                           )
                         } else
                           Map()) ++
                        (if (expressUnfilteredNumRecords >= 0) {
                           Map(
                             "totalUnfilteredItems" -> expressUnfilteredNumRecords.toString
                           )
                         } else
                           Map()),
                      links = rawResp.links ++ Map(
                        "self" -> Some(
                          self(Some(ver), pager)
                            .absoluteURL()
                        ),
                        "first" -> {
                          if (skip == 0)
                            None
                          else
                            Some(
                              self(Some(ver), pager.copy(offset = 0))
                                .absoluteURL()
                            )
                        },
                        "last" -> {
                          lazy val newSkip = ((expressNumRecord - skip) / limit) * limit + skip
                          if (limit == 0
                              || expressNumRecord < 0
                              || skip == newSkip
                              || expressNumRecord - newSkip <= 0)
                            None
                          else
                            Some(
                              self(Some(ver), pager.copy(offset = newSkip))
                                .absoluteURL()
                            )
                        },
                        "prev" -> {
                          val prev = skip - limit
                          if (skip <= 0)
                            None
                          else
                            Some(
                              self(
                                Some(ver),
                                pager
                                  .copy(
                                    offset =
                                      if (prev < 0) 0
                                      else prev
                                  )
                              ).absoluteURL()
                            )
                        },
                        "next" -> {
                          val next = skip + limit
                          if (next >= expressNumRecord)
                            None
                          else
                            Some(
                              self(
                                Some(ver),
                                pager
                                  .copy(offset = next)
                              ).absoluteURL()
                            )
                        }
                      )
                    )
                    Ok(Json.toJson(resp)).as(ResponseLike.contentType)
                  case _ =>
                    val content: Iterator[String] = format match {
                      case "json" =>
                        jsonResponse(recs, compact)
                      case "jsonld" =>
                        def url(id: String): String =
                          routes.RecordController
                            .record(dsName, Some(ver), id)
                            .absoluteURL()
                        jsonldResponse(recs, url)
                      case "tsv" =>
                        tsvResponse(recs)
                      case "raw" =>
                        JsonArray.toJson[Document](recs, _.toJson())
                      case _ =>
                        throw BadRequestClientError(
                          s"The format '$format' is not supported."
                        )
                    }
                    val fn =
                      if (inline)
                        None
                      else
                        Some(s"${dsName}_$ver.$format")
                    val stream: Source[String, NotUsed] =
                      Source.fromIterator(() => content)
                    Ok.chunked(stream, inline, fn)
                }
            }
          }
        case None =>
          val proto = Try {
            request.headers("X-FORWARDED-PROTO")
          }.toOption
            .getOrElse("http")
          cdb.latest(authorizer.isWriter(dsName)).map {
            case Some(v) =>
              Redirect(
                self(Some(v._id), pager)
                  .absoluteURL(proto == "https")
              )
            case None =>
              val resp = MultipleResponse[Record](
                data = Seq(),
                links = Map(
                  "self" -> Some(
                    self(None, pager)
                      .absoluteURL()
                  )
                )
              )
              Ok(Json.toJson(resp)).as(ResponseLike.contentType)
          }
      }
  }

  private def jsonapiResponse(record: Option[Document])(
    implicit rb: RecordBuilder,
    ex: Expressed,
    linker: Linked
  ): SingleResponse[Record] = {
    record match {
      case Some(doc) =>
        val rec = rb.fromSingleJson(doc.toJson())
        val expressed = ex(linker(rec))
        SingleResponse[Record](
          Some(Data[Record](resourceType, expressed.id.get, expressed))
        )
      case None =>
        SingleResponse[Record](None)
    }
  }

  private def jsonResponse(record: Option[Document], compact: Boolean)(
    implicit rb: RecordBuilder,
    ex: Expressed,
    linker: Linked
  ): Option[String] = {
    record.map { doc =>
      val rec = rb.fromSingleJson(doc.toJson())
      ex(linker(rec)).serialize(!compact)
    }
  }

  private def jsonldResponse(
    record: Option[Document],
    url: String
  )(implicit rb: RecordBuilder, linker: Linked): Option[String] = {
    record map { doc =>
      val rec = rb.fromSingleJson(doc.toJson())
      val dr = DataRecord(linker(rec), Some(url))
      JsonLdSerializer.serialize(dr)
    }
  }

  private def tsvResponse(record: Option[Document])(
    implicit rb: RecordBuilder,
    ex: Expressed,
    linker: Linked
  ): Option[String] = {
    record.map { doc =>
      val rec = rb.fromSingleJson(doc.toJson())
      ex(linker(rec)).asString
    }
  }

  //Special function used to retrieve a record with the latest version with read
  protected def getLatestRecord(
    dsName: String,
    id: String
  )(implicit req: RequestHeader, rb: RecordBuilder): Future[Option[Record]] = {
    Try { mongoClient.cbf(dsName) }.toOption match {
      case Some(cdb) =>
        cdb.latest(authorizer.isReader(dsName)).flatMap {
          case Some(v) =>
            val cbfVersion = CbfVersion(v._id)
            val filter = models.binders
              .Filter(Seq(IsIn("id", Set(id))))
            val col: Collection[Document] = cdb.recordsCol(cbfVersion)
            val extendedFilter = filter.bson(cbfVersion.release)
            col.getOne(extendedFilter).map {
              _.map { doc =>
                rb.fromSingleJson(doc.toJson())
              }
            }
          case None =>
            Future.successful(None)
        }
      case None =>
        Future.successful(None)
    }
  }

  def record(dsName: String,
             oVer: Option[String],
             id: String,
             format: String,
             inline: Boolean): Action[AnyContent] = reader(dsName).async {
    implicit request: Request[AnyContent] =>
      implicit val cdb = mongoClient.cbf(dsName)
      implicit val rbInstance = cfgMod.rb(dsName) //We do not propose curator view for single record
      implicit val linker = new Linked(
        ldbm.get(),
        dsName,
        cfgMod.datasetConfig(dsName),
        getLatestRecord
      )
      val compact = request.headers
        .get("X-Requested-With")
        .contains("XMLHttpRequest")
      val filter = models.binders
        .Filter(Seq(IsIn("id", Set(id))))
      val foDoc: Future[(Option[Document], CbfVersion)] = oVer match {
        case Some(version) =>
          //Because the pipeline makes queries before release, we need to disable
          //auth check and return draft versions.
          //authorizer.validatedVersion(dsName, version)
          cdb
            .version(version, true)
            .map {
              case Some(vi) =>
                cdb.parseVersion(vi._id)
              case None =>
                throw BadRequestClientError(
                  s"Version '$version' does not exist for dataset '$dsName'."
                )
            } flatMap { cbfVersion =>
            val col: Collection[Document] = cdb.recordsCol(cbfVersion)
            val extendedFilter = filter.bson(cbfVersion.release)
            col.getOne(extendedFilter).map {
              _ -> cbfVersion
            }
          }
        case None =>
          cdb.latest(authorizer.isWriter(dsName)).flatMap {
            case Some(v) =>
              val latest = CbfVersion(v._id)
              val col: Collection[Document] = cdb.recordsCol(latest)
              //We want to get all the docs to find the latest one
              val extendedFilter = filter.bson(latest.release, true, true)
              col.get(extendedFilter).map { docs =>
                if (docs.isEmpty)
                  None -> latest
                else {
                  //We choose the doc with the highest valid.start, BUT <= latest.release
                  val doc = docs
                    .maxBy { d =>
                      val s =
                        d.getEmbedded[Int](Seq("valid", "start").asJava, -1)
                      if (s > latest.release)
                        -1
                      else
                        s
                    }
                  val latestVersionOfThisDoc = {
                    val tmp =
                      doc.getEmbedded[Int](Seq("valid", "end").asJava, -1)
                    if (tmp < 0 || tmp > latest.release)
                      latest
                    else
                      CbfVersion(s"${latest.branch.dropRight(1)}$tmp")
                  }
                  Some(doc) -> latestVersionOfThisDoc
                }
              }
            case None =>
              Future.successful(None -> CbfVersion("0.0"))
          }
      }
      foDoc.map {
        case (oDoc, thisVersion) =>
          implicit val ex = am.expression(dsName, thisVersion.ver, request.host)
          format match {
            case "jsonapi" =>
              val rawResp = jsonapiResponse(oDoc)
              val self = routes.RecordController
                .record(dsName, Some(thisVersion.ver), id)
                .absoluteURL()
              val resp =
                rawResp.copy(
                  links = rawResp.links ++ Map("self" -> Some(self)),
                  meta = rawResp.meta + ("recordVersion" -> thisVersion.ver)
                )
              Ok(Json.toJson(resp)).as(ResponseLike.contentType)
            case _ =>
              val oStr = format match {
                case "json" =>
                  jsonResponse(oDoc, compact)
                case "jsonld" =>
                  val url = routes.RecordController
                    .record(dsName, Some(thisVersion.ver), id)
                    .absoluteURL()
                  jsonldResponse(oDoc, url)
                case "tsv" =>
                  tsvResponse(oDoc)
                case "raw" =>
                  oDoc.map { _.toJson() }
                case _ =>
                  throw BadRequestClientError(
                    s"The format '$format' is not supported."
                  )
              }
              oStr match {
                case Some(content) =>
                  if (inline)
                    Ok(content)
                  else {
                    val io = new ByteArrayInputStream(content.getBytes())
                    val stream = StreamConverters
                      .fromInputStream(() => io)
                      .mapMaterializedValue(_.onComplete { _ =>
                        io.close()
                      })
                    Ok.streamed(stream, None, inline, Some(s"$id.$format"))
                  }
                case None =>
                  throw NotFoundClientError(
                    s"Could not find record '$dsName/${thisVersion.ver}/$id'"
                  )
              }
          }
      }
  }

  protected def postImpl(dsName: String, cbfVersion: CbfVersion): Unit = {
    val db = mongoClient.cbf(dsName)
    val errors = Await.result(db.errorsCol(cbfVersion).count(), 1.minute)
    if (errors != 0) {
      throw UnprocessableEntityClientError(
        s"$errors errors found! Errors available at ${routes.PushController
          .errorsSpreadsheet(dsName, cbfVersion.ver)}"
      )
    } else
      logger.info("Successfully wrote all records to Mongodb")
  }

  //Note: does not throw 409 if exist. Instead, the record is replaced.
  def createRecord(dsName: String, version: String) =
    writer(dsName).async { implicit request =>
      implicit val cdb = mongoClient.cbf(dsName)
      authorizer.validatedVersion(dsName, version).flatMap { cbfVersion =>
        val ver = cbfVersion.ver
        request.body.asJson.flatMap {
          _.as[SingleResponse[Record]].data
        } match {
          case Some(data) =>
            if (data.`type` != resourceType) {
              throw BadRequestClientError(
                s"Data submitted with type '${data.`type`}', while this resource is '$resourceType'."
              )
            } else if (data.id.nonEmpty)
              throw BadRequestClientError(
                s"Data submitted with an id, while this resource does not support client-generated ids."
              )
            else {
              val record = data.attributes.ensureIdStmPresent
              val rb = cfgMod.rb(dsName)
              val inputIt: Iterator[String] =
                rb.toTsv(Iterator(record))
              val transform =
                new TrPush(
                  mongoClient.connection,
                  dsName,
                  ver,
                  inputIt,
                  idProviderModule.getNext(dsName)
                )
              val (errors, ids) = transform.run()
              if (errors.nonEmpty) {
                val resp = ErrorResponse(errors.map { e =>
                  Err("422", "Validation error", e)
                })
                Future.successful(Status(422)(Json.toJson(resp))
                  .as(ResponseLike.contentType))
              } else {
                (ids.headOption match {
                  case Some(id) =>
                  cdb.recordsCol(cbfVersion).getOne(org.mongodb.scala.model.Filters.eq("_id", id))
                  case None =>
                    Future.successful(None)
                }).map{
                  case Some(doc) =>
                    val recId = doc.getString("id")
                    val raw = SingleResponse[Document](
                      Some(Data[Document](resourceType, recId, doc))
                    )
                    val self = routes.RecordController
                      .record(dsName, Some(version), recId)
                      .absoluteURL()
                    val resp =
                      raw.copy(links = raw.links ++ Map("self" -> Some(self)))
                    cm.invalidate(dsName)
                    gm.invalidate(dsName, cbfVersion)
                    Created(Json.toJson(resp))
                      .as(ResponseLike.contentType)
                      .withHeaders("Location" -> self)
                  case None =>
                    //If modified
                    val recId = record.getId
                    val raw = SingleResponse[Record](
                      Some(Data[Record](resourceType, recId, record))
                    )
                    val self = routes.RecordController
                      .record(dsName, Some(version), recId)
                      .absoluteURL()
                    val resp =
                      raw.copy(links = raw.links ++ Map("self" -> Some(self)))
                    cm.invalidate(dsName)
                    gm.invalidate(dsName, cbfVersion)
                    Created(Json.toJson(resp))
                      .as(ResponseLike.contentType)
                      .withHeaders("Location" -> self)
                }
              }
            }
          case None =>
            throw BadRequestClientError("No data or wrong format received.")
        }
      }
    }

  def patchRecordImpl(recordPatch: Record,
                      dsName: String,
                      ver: String,
                      force: Boolean = false,
                      sideEffect: => Unit = ()): Result = {
    val inputIt: Iterator[String] =
      cfgMod
        .rb(dsName, attr => recordPatch.keys.contains(attr.ref))
        .toTsv(Iterator(recordPatch))
    val transform =
      new TrPatch(
        mongoClient.connection,
        dsName,
        ver,
        inputIt,
        idProviderModule.getNext(dsName),
        force
      )
    val (errors, ids) = transform.run()
    if (errors.nonEmpty) {
      val resp = ErrorResponse(errors.map { e =>
        Err("422", "Validation error", e)
      })
      Status(422)(Json.toJson(resp))
        .as(ResponseLike.contentType)
    } else {
      sideEffect
      NoContent
        .as(ResponseLike.contentType)
    }
  }

  def patchRecord(dsName: String, id: String, version: String) =
    writer(dsName).async { implicit request: Request[AnyContent] =>
      implicit val cdb = mongoClient.cbf(dsName)
      authorizer.validatedVersion(dsName, version) map { cbfVersion =>
        val ver = cbfVersion.ver
        request.body.asJson.flatMap {
          _.as[SingleResponse[Record]].data
        } match {
          case Some(data) =>
            if (data.`type` != resourceType) {
              throw BadRequestClientError(
                s"Data submitted with type '${data.`type`}', while this resource is '$resourceType'."
              )
            } else if (data.id != id)
              throw BadRequestClientError(
                s"Id '${data.id}' does not match provided id '$id'."
              )
            else {
              val recordPatch = data.attributes
              patchRecordImpl(
                recordPatch,
                dsName,
                ver,
                sideEffect = gm.invalidate(dsName, cbfVersion)
              )
            }
          case None =>
            throw BadRequestClientError("No data or wrong format received.")
        }
      }
    }

  def patchLink(dsName: String, ver: String) = secure.async {
    request: Request[AnyContent] =>
      val datasetConfig = cfgMod.datasetConfig(dsName)
      request.body.asJson.map {
        _.as[Record]
      } match {
        case Some(patchRecord) => {
          val attrs = patchRecord.keys.toSet -- Set("id")
          if (attrs.size != 1)
            throw BadRequestClientError(
              s"Data should contain exclusively id and a 'dblink:direct:*' attribute, but found '${patchRecord.keys}'."
            )
          else {
            val attr = attrs.head //We just checked the size
            datasetConfig
              .attr(attr)
              .flatMap { a =>
                datasetConfig.customTypes.find { _.ref == a.`type` }
              }
              .map { t =>
                if (t.lt.isSubtypeOf("dblink:direct"))
                  t.lt.tertiary
                else
                  throw BadRequestClientError(
                    s"Data should contain exclusively id and a 'dblink:direct:*' attribute, but found attr '$attr' which is of type '${t.ref}'."
                  )
              }
          }
        } match {
          case Some(requestingDsName) =>
            //We check the right on affiliatedDsName, but we do patch dsName
            if (authorizer.isWriter(requestingDsName)(request)) {
              Future(patchRecordImpl(patchRecord, dsName, ver, force = true))
            } else
              Future.failed(
                ForbiddenClientError(
                  s"Role 'writer' for dataset '$requestingDsName' is required to modify links on dataset '$dsName'."
                )
              )
          case None =>
            println(s"Could not find affiliate dataset for dataset '$dsName'.")
            Future.successful(
              NoContent
                .as(ResponseLike.contentType)
            )
        }
        case None =>
          throw BadRequestClientError("No data or wrong format received.")
      }
  }

  //Note: does not throw 404 if does not exist
  def deleteRecord(dsName: String, id: String, ver: String) =
    writer(dsName) { implicit request: Request[AnyContent] =>
      //We use TrPush because it handles the update of valid.end and/or dblinks if necessary
      val inputIt = Iterator("id", s"!$id")
      val transform =
        new TrPush(
          mongoClient.connection,
          dsName,
          ver,
          inputIt,
          idProviderModule.getNext(dsName)
        )
      val (errors, ids) = transform.run()
      if (errors.nonEmpty) {
        val resp = ErrorResponse(errors.map { e =>
          Err("422", "Validation error", e)
        })
        Status(422)(Json.toJson(resp))
          .as(ResponseLike.contentType)
      } else {
        cm.invalidate(dsName)
        gm.invalidate(dsName, CbfVersion(ver))
        NoContent.as(ResponseLike.contentType)
      }
    }
}
