package controllers

import javax.inject._
import modules.{Authorizer, DiscoverModule, MongodbClient}
import play.api.libs.json.Json
import play.api.{Configuration, Logging}
import play.api.mvc._
import no.uit.sfb.info.databaseapi.BuildInfo
import models.auth.SecurityLike
import models.openapi.Info
import no.uit.sfb.cbf.shared.auth.UserInfo
import org.pac4j.play.scala.SecurityComponents
import play.api.libs.concurrent.Futures

import scala.concurrent.{Await, ExecutionContext}
import scala.concurrent.duration.DurationInt
import scala.util.Try

/**
  * This controller creates an `Action` to handle HTTP requests to the
  * application's home page.
  */
@Singleton
class HomeController @Inject()(val controllerComponents: SecurityComponents,
                               dbMod: DiscoverModule,
                               mongoClient: MongodbClient,
                               implicit val authorizer: Authorizer,
                               implicit val f: Futures,
                               implicit val cfg: Configuration,
                               implicit val ec: ExecutionContext)
    extends SecurityLike
    with Logging {

  def index() = Action { implicit request: Request[AnyContent] =>
    Ok(views.html.index(BuildInfo.version))
  }

  def info() = Action {
    val conn =
      Try(Await.result(mongoClient.listDbs(), 45.seconds)).toOption match {
        case Some(_) => true
        case None    => false
      }
    val res = Info(BuildInfo.name, BuildInfo.version, BuildInfo.gitCommit, conn)
    implicit val writer = Json.writes[Info]
    Ok(Json.toJson(res))
  }

  def userInfo(dbName: String): Action[AnyContent] =
    open { implicit request: Request[AnyContent] =>
      implicit val writer = Json.writes[UserInfo]
      Ok(
        Json.toJson(
          UserInfo(
            authorizer.user.id,
            authorizer.user.email,
            authorizer.role(dbName),
            pending = authorizer.pending(dbName)
          )
        )
      )
    }

  def isAdmin(): Action[AnyContent] =
    open { implicit request: Request[AnyContent] =>
      implicit val writer = Json.writes[UserInfo]
      Ok(authorizer.isAdmin.toString)
    }

  def discover(): Action[AnyContent] =
    open.async { implicit request: Request[AnyContent] =>
      dbMod.datasets.map { ds =>
        Ok(Json.toJson(ds))
      }
    }

  def api() = Action { implicit request: Request[AnyContent] =>
    logger.info("Redirecting to swagger generated API")

    Redirect(routes.Assets.at("index.html?url=/assets/swagger.json"))
  }

  def identity() = {
    secureWithoutFallback { implicit request: Request[AnyContent] =>
      Ok(views.html.identity())
    }
  }
}
