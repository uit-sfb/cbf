package controllers

import akka.stream.scaladsl.Source
import com.mongodb.client.model.Filters
import io.circe.generic.auto._
import models.auth.SecurityLike
import models.binders.Projector
import no.uit.sfb.cbf.shared.errors.BadRequestClientError
import models.json.JsonApi._
import modules.{Authorizer, ConfigModule, LinkedDbModule, MongodbClient}
import no.uit.sfb.cbf.shared.config.DatasetConfig
import no.uit.sfb.cbf.shared.jsonapi.{
  Data,
  MultipleResponse,
  ResponseLike,
  SingleResponse
}
import no.uit.sfb.cbf.shared.utils.Date
import org.mongodb.scala.bson.Document
import org.pac4j.play.scala.SecurityComponents
import play.api.libs.concurrent.Futures
import play.api.libs.json.Json
import play.api.mvc._
import play.api.{Configuration, Logging}

import javax.inject._
import scala.concurrent.{ExecutionContext, Future}

@Singleton
class ConfigController @Inject()(val controllerComponents: SecurityComponents,
                                 mongoClient: MongodbClient,
                                 cm: ConfigModule,
                                 ldbm: LinkedDbModule,
                                 val authorizer: Authorizer,
                                 implicit val f: Futures,
                                 implicit val cfg: Configuration,
                                 implicit val ec: ExecutionContext)
    extends SecurityLike
    with Logging {
  private val resourceType = "configs"

  def getConfigs(dsName: String, projector: Projector) = owner(dsName).async {
    implicit request: Request[AnyContent] =>
      mongoClient
        .cbf(dsName)
        .configsCol
        .get(project = projector.bson)
        .map { docs =>
          docs.sortBy { _.getString("_id") }.reverse
        }
        .map { configs =>
          val rawResp =
            MultipleResponse[Document](configs.map { doc =>
              Data[Document](resourceType, doc.getString("_id"), doc)
            })
          val self = routes.ConfigController
            .getConfigs(dsName, projector)
            .absoluteURL()
          val resp =
            rawResp.copy(links = rawResp.links ++ Map("self" -> Some(self)))
          val content = Json.toJson(resp)
          val rawRequest =
            if (request.headers
                  .get("X-Requested-With")
                  .contains("XMLHttpRequest"))
              Ok(content)
            else
              Ok(Json.prettyPrint(content))
          rawRequest.as(ResponseLike.contentType)
        }
  }

  //Everyone should be able to get access to the config
  def getConfig(dsName: String, id: String, inline: Boolean) = Action {
    implicit request: Request[AnyContent] =>
      val oConfig =
        if (id == "current")
          cm.getDatasetConfig(dsName)
        else
          mongoClient.cbf(dsName).config(Some(id))
      lazy val content = {
        val rawResp =
          SingleResponse[DatasetConfig](oConfig.map { c =>
            Data[DatasetConfig](resourceType, c.name, c)
          })
        val self = routes.ConfigController
          .getConfig(dsName, id, inline)
          .absoluteURL()
        val resp =
          rawResp.copy(links = rawResp.links ++ Map("self" -> Some(self)))
        Json.toJson(resp)
      }
      if (inline) {
        Ok(Json.prettyPrint(content)).as(ResponseLike.contentType)
      } else {
        val rawRequest =
          if (request.headers
                .get("X-Requested-With")
                .contains("XMLHttpRequest")) {
            Ok.streamed(
              Source(Seq(content)),
              None,
              inline,
              Some(s"$dsName-config.json")
            )
          } else
            Ok.streamed(
              Source(Seq(Json.prettyPrint(Json.toJson(oConfig)))),
              None,
              inline,
              Some(s"$dsName-config.json")
            )
        rawRequest.as(ResponseLike.contentType)
      }
  }

  def createConfig(dsName: String) =
    owner(dsName).async { implicit request =>
      request.body.asJson.flatMap { r =>
        r.as[SingleResponse[DatasetConfig]]
          .data
          .orElse(Some(Data(resourceType, attributes = r.as[DatasetConfig]))) //In case we upload a file containing the JSON data
      } match {
        case Some(data) =>
          if (data.`type` != resourceType) {
            throw BadRequestClientError(
              s"Data submitted with type '${data.`type`}', while this resource is '$resourceType'."
            )
          } else if (data.id.nonEmpty)
            throw BadRequestClientError(
              s"Data submitted with an id, while this resource does not support client-generated ids."
            )
          else {
            val cfg = data.attributes.copy(_id = Date.nowDate())
            val cbf = mongoClient.cbf(dsName)
            val filter = Filters.eq("_id", cfg._id)
            cbf.configsCol
              .replace(filter, Document(Json.toJson(cfg).toString()), true)
              .map { r =>
                val raw = SingleResponse[DatasetConfig](
                  Some(Data[DatasetConfig]("configs", cfg.name, cfg))
                )
                val self = routes.ConfigController
                  .getConfig(dsName, cfg._id) //use getDb instead of createDb here!
                  .absoluteURL()
                val resp =
                  raw.copy(links = raw.links ++ Map("self" -> Some(self)))
                cm.invalidate(dsName)
                ldbm.invalidate()
                Created(Json.toJson(resp))
                  .as(ResponseLike.contentType)
                  .withHeaders("Location" -> self)
              }
          }
        case None =>
          throw BadRequestClientError("No data or wrong format received.")
      }
    }

  def deleteConfig(dsName: String, id: String) = {
    owner(dsName).async { implicit request: Request[AnyContent] =>
      val cbf = mongoClient.cbf(dsName)
      if (cm.datasetConfig(dsName)
            ._id == id) { //We make sure we do not delete the active config
        Future.successful(Conflict.as(ResponseLike.contentType))
      } else
        cbf.configsCol.delete(Filters.eq("_id", id), true).map { _ =>
          cm.invalidate(dsName)
          ldbm.invalidate()
          NoContent.as(ResponseLike.contentType)
        }
    }
  }

  def revertTo(dsName: String, id: String) = {
    owner(dsName).async { implicit request: Request[AnyContent] =>
      val cbf = mongoClient.cbf(dsName)
      mongoClient
        .cbf(dsName)
        .configsCol
        .get(project = Projector(Map("" -> Seq("_id"))).bson)
        .map { docs =>
          docs.map { _.getString("_id") }
        }
        .collect {
          case ids if ids.nonEmpty =>
            val safeId = Seq(id, ids.min).max //We make sure we will not delete the smallest id
            val toDelete = ids.filter(_ > safeId)
            if (toDelete.nonEmpty)
              cbf.configsCol.delete(Filters.eq("_id", toDelete.head), false)
            else
              Future.successful(())
        }
        .map { _ =>
          cm.invalidate(dsName)
          ldbm.invalidate()
          NoContent.as(ResponseLike.contentType)
        }
    }
  }
}
