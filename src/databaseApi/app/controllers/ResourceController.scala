package controllers

import akka.stream.Materializer
import models.auth.SecurityLike
import models.binders.{Filter => FilterP}
import no.uit.sfb.cbf.shared.errors.{
  BadRequestClientError,
  ForbiddenClientError,
  NotFoundClientError,
  UnauthorizedClientError
}
import models.resourceBuilder._
import modules.{AccessModule, Authorizer, ConfigModule, MongodbClient}
import no.uit.sfb.cbf.mongodb.{CbfDataset, Collection}
import no.uit.sfb.cbf.shared.config.AttributeDef
import no.uit.sfb.cbf.shared.model.v1.nativ.{CompactIRI, Record}
import no.uit.sfb.cbf.shared.version.CbfVersion
import org.mongodb.scala.bson.{BsonDocument, Document}
import org.mongodb.scala.model
import org.mongodb.scala.model.Aggregates
import org.pac4j.play.scala.SecurityComponents
import play.api.libs.concurrent.Futures
import play.api.libs.json.Json
import play.api.mvc._
import play.api.{Configuration, Logging}
import javax.inject._
import scala.concurrent.{ExecutionContext, Future}
import scala.util.Try

@Singleton
class ResourceController @Inject()(val controllerComponents: SecurityComponents,
                                   mongoClient: MongodbClient,
                                   am: AccessModule,
                                   cfgMod: ConfigModule,
                                   implicit val mat: Materializer,
                                   implicit val authorizer: Authorizer,
                                   implicit val futures: Futures,
                                   implicit val cfg: Configuration,
                                   implicit val ec: ExecutionContext)
    extends SecurityLike
    with Logging {

  //If you want to return an error message to the client, please throw an ErrorLike

  //Returns the Seq of _iri of the resource attribute
  protected def fetchResourceCiris(dbName: String,
                                   version: CbfVersion,
                                   filter: FilterP,
                                   attribute: AttributeDef,
  )(implicit cdb: CbfDataset): Future[Iterator[CompactIRI]] = {
    val f = am.ciriFilter(filter, dbName) map { flt =>
      flt.bson(version.release)
    }
    val col: Collection[Document] =
      cdb.recordsCol(version, filter.neededIndexes)
    f.map { ciriFilter =>
      val project = Aggregates.project(
        Document(
          "_id" ->
            BsonDocument(
              s"""{ $$first: { $$first: { $$first: "$$attrs.${attribute.ref}.obj.iri" } } }"""
            )
        )
      )
      val pipeline1 = Seq(Aggregates.`match`(ciriFilter), project)
      col
        .aggregate(pipeline1, true, batchSize = 50)
        .flatMap { doc =>
          val key = doc.getString("_id") //_id because it is the result of a projection
          CompactIRI(key)
        }
    }
  }

  protected def findAttribute(attr: Option[String],
                              attrs: Seq[AttributeDef]) = {
    attr match {
      case Some(a) =>
        attrs
          .find(_.ref == a)
          .getOrElse(
            throw BadRequestClientError(
              s"Attribute '$a' is not of type 'resource'. Please use one of: [${attrs
                .map {
                  _.ref
                }
                .mkString(", ")}]"
            )
          )
      case None =>
        if (attrs.size == 1)
          attrs.head
        else
          throw BadRequestClientError(
            s"Multiple resource attribute found. Please retry the request with 'attr' set to one of: [${attrs
              .map {
                _.ref
              }
              .mkString(", ")}]"
          )
    }
  }

  def resources(dbName: String,
                oVer: Option[String],
                attr: Option[String],
                filter: FilterP,
                mode: String,
                keys: String): Action[AnyContent] = reader(dbName).async {
    implicit request: Request[AnyContent] =>
      val datasetConfig = cfgMod.datasetConfig(dbName)
      val attrs = datasetConfig.resourceAttributes
      val attribute = attr match {
        case Some(a) =>
          attrs
            .find(_.ref == a)
            .getOrElse(
              throw BadRequestClientError(
                s"Attribute '$a' is not using a 'resource_*' resolver. Please use one of: [${attrs
                  .map {
                    _.ref
                  }
                  .mkString(", ")}]"
              )
            )
        case None =>
          if (attrs.size == 1)
            attrs.head
          else
            throw BadRequestClientError(
              s"Multiple resource attribute found. Please retry the request with 'attr' set to one of: [${attrs
                .map {
                  _.ref
                }
                .mkString(", ")}]"
            )
      }
      implicit val cdb = mongoClient.cbf(dbName)
      oVer match {
        case Some(version) =>
          authorizer
            .validatedVersion(dbName, version)
            .flatMap { cbfVersion =>
              lazy val ver = cbfVersion.ver
              lazy val crRef = attribute.resolvers
                .find(_.startsWith("resource"))
                .head //We already checked that there was one
              lazy val cr = datasetConfig.customResolvers
                .find(_.prefix == crRef)
                .getOrElse(
                  throw new Exception(s"No custom resolver '$crRef' defined.")
                )
              lazy val model = ResourceModelLike(cr, dbName, ver, request.host)
              lazy val builder = model.resourceBuilder(mode)
              lazy val keySelect = keys.split(',').toSet.filter {
                _.nonEmpty
              }
              fetchResourceCiris(dbName, cbfVersion, filter, attribute).map {
                ciris =>
                  val source = builder
                    .writeArchive(ciris.map {
                      _.id
                    }, keySelect)
                  Ok.streamed(source, None, false, Some(s"${dbName}_$ver.tgz"))
              }
            }
        case None =>
          val proto = Try {
            request.headers("X-FORWARDED-PROTO")
          }.toOption
            .getOrElse("http")
          cdb.latest(authorizer.isWriter(dbName)).map {
            case Some(v) =>
              Redirect(
                routes.ResourceController
                  .resources(dbName, Some(v._id), attr, filter, mode, keys)
                  .absoluteURL(proto == "https")
              )
            case None =>
              if (authorizer.isLoggedIn)
                throw ForbiddenClientError()
              else
                throw UnauthorizedClientError()
          }
      }
  }

  def resource(dbName: String,
               oVer: Option[String],
               id: String,
               attr: Option[String],
               keys: String): Action[AnyContent] = reader(dbName).async {
    implicit request: Request[AnyContent] =>
      val datasetConfig = cfgMod.datasetConfig(dbName)
      val attrs = datasetConfig.resourceAttributes
      implicit val cdb = mongoClient.cbf(dbName)
      oVer match {
        case Some(version) =>
          authorizer
            .validatedVersion(dbName, version) flatMap { cbfVersion =>
            val ver = cbfVersion.ver
            val f = CompactIRI(id) match {
              case Some(ciri) =>
                val attr = attrs
                  .find {
                    _.resolvers.contains(ciri.prefix)
                  }
                  .getOrElse(
                    throw BadRequestClientError(
                      s"No attribute having '${ciri.prefix}' in its identifier list found."
                    )
                  )
                Future.successful {
                  attr -> ciri.id
                }
              case None =>
                //We assume a recordId was provided
                val a = findAttribute(attr, attrs)
                val fCiri: Future[CompactIRI] = {
                  val col: Collection[Document] = cdb.recordsCol(cbfVersion)
                  col
                    .getOne(model.Filters.eq("id", id))
                    .map {
                      _.getOrElse(
                        throw BadRequestClientError(
                          s"Record id '$id' not found."
                        )
                      )
                    }
                    .map { doc =>
                      val rec = Record(doc.toJson())
                      rec
                        .stm(a.ref)
                        .flatMap { stm =>
                          stm.obj.headOption.toSeq.flatMap { _.ciris }
                        }
                        .headOption
                        .getOrElse(
                          throw NotFoundClientError(
                            s"Record id '$id' does not link to any resource for attribute '${a.ref}'."
                          )
                        )
                    }
                }
                fCiri.map {
                  a -> _.id
                }
            }
            f.map {
              case (attribute, key) =>
                val crRef = attribute.resolvers
                  .find(_.startsWith("resource"))
                  .head //We already check that there was one
                val cr = datasetConfig.customResolvers
                  .find(_.prefix == crRef)
                  .getOrElse(
                    throw new Exception(s"No custom resolver '$crRef' defined.")
                  )
                val model =
                  ResourceModelLike(cr, dbName, ver, request.host)
                val builder = model.resourceBuilder()
                val keySelect = keys.split(',').toSet.filter {
                  _.nonEmpty
                }
                val source = builder
                  .writeArchive(Iterator(key), keySelect)
                Ok.streamed(
                  source,
                  None,
                  false,
                  Some(s"${dbName}_${ver}_$key.tgz")
                )
            }
          }
        case None =>
          val proto = Try {
            request.headers("X-FORWARDED-PROTO")
          }.toOption
            .getOrElse("http")
          cdb.latest(authorizer.isWriter(dbName)).map {
            case Some(v) =>
              Redirect(
                routes.ResourceController
                  .resource(dbName, Some(v._id), id, attr, keys)
                  .absoluteURL(proto == "https")
              )
            case None =>
              if (authorizer.isLoggedIn)
                throw ForbiddenClientError()
              else
                throw UnauthorizedClientError()
          }
      }
  }

  def resourceInfo(dbName: String) = reader(dbName) {
    implicit request: Request[AnyContent] =>
      val datasetConfig = cfgMod.datasetConfig(dbName)

      val attrs: Seq[AttributeDef] = datasetConfig.resourceAttributes
      val res = attrs.map { attr =>
        val crRef = attr.resolvers.find(_.startsWith("resource_")).head //We know from above that there is at least one
        val cr = datasetConfig.customResolvers
          .find(_.prefix == crRef)
          .getOrElse(
            throw new Exception(s"No custom resolver '$crRef' defined.")
          )
        val model = ResourceModelLike(cr)
        val m = Map("modes" -> model.modes.keySet, "keys" -> model.keys)
        attr.ref -> m
      }
      Ok(Json.toJson(res))
  }
}
