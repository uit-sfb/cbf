package controllers

import akka.stream.Materializer
import akka.stream.scaladsl.Source
import models.auth.SecurityLike
import no.uit.sfb.cbf.shared.errors.ConflictClientError
import modules.{Authorizer, ConfigModule, MongodbClient, PushModule}
import no.uit.sfb.cbf.mongodb.CbfDataset
import org.pac4j.play.scala.SecurityComponents
import play.api.libs.concurrent.Futures
import play.api.{Configuration, Logging}

import javax.inject._
import scala.concurrent.{ExecutionContext, Future}
import io.circe.generic.auto._
import io.circe.syntax._
import no.uit.sfb.cbf.bulktransform.ErrorUtils
import no.uit.sfb.cbf.shared.model.v1.task.UpdateDsTaskStatus
import no.uit.sfb.scalautils.common.FileUtils

import java.nio.file.Paths

@Singleton
class PushController @Inject()(val controllerComponents: SecurityComponents,
                               mongoClient: MongodbClient,
                               pm: PushModule,
                               cm: ConfigModule,
                               implicit val authorizer: Authorizer,
                               implicit val futures: Futures,
                               implicit val mat: Materializer,
                               implicit val cfg: Configuration,
                               implicit val ec: ExecutionContext)
    extends SecurityLike
    with Logging {

  val localPath = Paths.get(cfg.get[String]("app.paths.local"))

  //If you want to return an error message to the client, please throw an ErrorLike

  def upload(dbName: String, version: String, patch: Boolean) =
    writer(dbName)(parse.temporaryFile).async { implicit request =>
      val method = if (patch) "patch" else "push"
      implicit val cbf = mongoClient.cbf(dbName)
      authorizer.validatedVersion(dbName, version, true).flatMap { cbfVersion =>
        val ver = cbfVersion.ver
        val file = localPath.resolve(s"$dbName/$ver/$method.tsv").toFile
        if (pm.active(dbName, ver))
          throw ConflictClientError(
            s"Dataset '$dbName' version '$ver' is currently being updated. Please wait a few minutes before submitting this request again."
          )
        else {
          //We use a temporary file to not get deadlocked in case the upload fails!
          if (pm.active(dbName, ver))
            throw ConflictClientError(
              s"Dataset '$dbName' version '$ver' is currently being updated. Please wait a few minutes before submitting this request again."
            )
          else {
            cbf.errorsCol(cbfVersion).drop().map { _ =>
              synchronized {
                val parentDir = file.toPath.getParent
                //We delete the directory to delete any other files that may be there such as the error.tsv
                FileUtils.deleteDirIfExists(parentDir)
                FileUtils.createDirs(parentDir)
                request.body.atomicMoveWithFallback(file)
                pm.resetTask(dbName, ver, method)
                Ok
              }
            }
          }
        }
      }
    }

  def status(dbName: String, version: String) = writer(dbName).async {
    implicit request =>
      implicit val cbf = mongoClient.cbf(dbName)
      authorizer
        .validatedVersion(dbName, version) flatMap { cbfVersion =>
        val ver = cbfVersion.ver
        val f = pm.task(dbName, ver) match {
          case Some(r) =>
            Future.successful(r.toExchange)
          case None =>
            cbf.errorsCol(cbfVersion).count() map { errs =>
              val tmp = UpdateDsTaskStatus(
                dbName,
                ver,
                "",
                100,
                -1,
                -1,
                errs,
                "Success"
              )
              if (errs == 0)
                tmp
              else
                tmp.copy(state = "Failed")
            }
        }
        f.map { res =>
          Ok(res.asJson.spaces2)
        }
      }
  }

  def errorsSpreadsheet(dbName: String, version: String) =
    writer(dbName).async { implicit request =>
      implicit val cbf = mongoClient.cbf(dbName)
      authorizer
        .validatedVersion(dbName, version) map { cbfVersion =>
        val source = Source.fromIterator(
          () =>
            ErrorUtils.generateErrorTsv(cbf.errorsCol(cbfVersion)).map {
              _ + "\n"
          }
        )
        Ok.chunked(
          source,
          false,
          Some(s"${dbName}_${cbfVersion.ver}_errors.tsv")
        )
      }
    }

  def emptySpreadsheet(dbName: String, method: String) = writer(dbName) {
    implicit request =>
      val conf = cm.datasetConfig(dbName)
      val keys = method match {
        case "patch" => Seq("id")
        case _ =>
          conf.persistedAttributes.collect {
            case attr
                if !attr.isFullyControlled || attr.hidden || attr.sticky =>
              attr.ref
          }
      }
      val str = (keys.mkString("\t") ++ "\n").toCharArray.map { _.toByte }
      val source = Source.single(str)
      Ok.streamed(
        source,
        Some(str.length),
        false,
        Some(s"${dbName}_empty_spreadsheet.tsv")
      )
  }
}
