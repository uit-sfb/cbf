package controllers

import javax.inject._
import models.binders.Pager
import modules.{Authorizer, ConfigModule, MailerModule, MongodbClient}
import io.circe.generic.auto._
import io.circe.syntax._
import io.circe.parser.decode
import models.auth.SecurityLike
import no.uit.sfb.cbf.shared.errors._
import play.api.libs.concurrent.Futures
import play.api.mvc._
import play.api.{Configuration, Logging}
import no.uit.sfb.cbf.shared.jsonapi._
import org.mongodb.scala.bson.{BsonDocument, Document}
import play.api.libs.json.Json
import models.json.JsonApi._
import no.uit.sfb.cbf.shared.auth.UserInfo
import no.uit.sfb.cbf.shared.model.v1.versioninfo.VersionInfo
import no.uit.sfb.cbf.shared.utils.GenericVersioning
import org.mongodb.scala.model
import org.pac4j.play.scala.SecurityComponents
import play.api.routing.Router

import scala.concurrent.{ExecutionContext, Future}

@Singleton
class MemberController @Inject()(val controllerComponents: SecurityComponents,
                                 mongoClient: MongodbClient,
                                 cm: ConfigModule,
                                 implicit val authorizer: Authorizer,
                                 implicit val futures: Futures,
                                 implicit val cfg: Configuration,
                                 implicit val ec: ExecutionContext,
                                 implicit val mailer: MailerModule)
    extends SecurityLike
    with Logging {
  private val resourceType = "members"

  //If you want to return an error message to the client, please throw an ErrorLike

  def members(dbName: String, pager: Pager): Action[AnyContent] =
    owner(dbName).async { implicit request: Request[AnyContent] =>
      val cbf = mongoClient.cbf(dbName)
      val fDocs =
        cbf.authCol
          .getAll()
          .map { members =>
            val selected =
              if (pager.size < 0)
                members.drop(pager.offset)
              else
                members.slice(pager.offset, pager.offset + pager.size)
            selected.sortBy(_._id).map { vi =>
              Document(vi.asJson.noSpaces)
            }
          }
      fDocs map { docs =>
        val rawResp = MultipleResponse[Document](docs map { doc =>
          val id = doc.getString("_id")
          Data[Document](resourceType, id, doc)
        })

        val resp = rawResp.copy(
          links = rawResp.links ++ Map(
            "self" -> Some(
              routes.MemberController
                .members(dbName, pager)
                .absoluteURL()
            )
          )
        )
        Ok(Json.toJson(resp)).as(ResponseLike.contentType)
      }
    }

  def member(dbName: String, id: String): Action[AnyContent] =
    owner(dbName).async { implicit request: Request[AnyContent] =>
      val cdb = mongoClient.cbf(dbName)
      val oDoc = cdb.authCol
        .getOne(model.Filters.eq("_id", id))
        .map { member =>
          val self = routes.MemberController
            .member(dbName, id)
            .absoluteURL()

          val resp = member match {
            case Some(member) =>
              val doc = Document(member.asJson.noSpaces)
              val data = Some(Data[Document](resourceType, id, doc))
              SingleResponse[Document](data)
            case _ =>
              SingleResponse[Document](None)
          }

          resp
            .copy(links = Map("self" -> Some(self)))

        }
      oDoc.map { resp =>
        Ok(Json.toJson(resp)).as(ResponseLike.contentType)
      }

    }

  //May remain open, but the role for new user request should be set to "none"
  def createMember(dbName: String) =
    open.async { implicit request =>
      request.body.asJson.flatMap {
        _.as[SingleResponse[UserInfo]].data
      } match {
        case Some(data) =>
          if (data.`type` != resourceType) {
            throw BadRequestClientError(
              s"Data submitted with type '${data.`type`}', while this resource is '$resourceType'."
            )
          } else if (data.id.isEmpty)
            throw BadRequestClientError(
              s"Data submitted without an id, while this resource does not support client-generated ids."
            )
          else {

            val userInfo =
              UserInfo(
                data.id,
                data.attributes.email,
                data.attributes.role,
                data.attributes.name,
                data.attributes.pending
              )

            implicit val cdb = mongoClient.cbf(dbName)

            cdb.authCol
              .getOne(model.Filters.eq("_id", data.id))
              .flatMap { res =>
                cdb.authCol
                  .replace(
                    model.Filters.eq("_id", data.id),
                    if (res.nonEmpty) userInfo
                    else userInfo.copy(role = "none"),
                    upsert = true
                  )
                  .map { res =>
                    if (res.wasAcknowledged()) {
                      val raw = SingleResponse[UserInfo](
                        Some(Data[UserInfo](resourceType, data.id, userInfo))
                      )
                      val self = routes.MemberController
                        .member(dbName, data.id)
                        .absoluteURL()

                      val resp =
                        raw.copy(links = raw.links ++ Map("self" -> Some(self)))
                      //send an email to all owners of this db, add content of type String
                      cdb.authCol.getAll().map { members =>
                        members.filter(_.role == "owner").foreach { m =>
                          mailer.send(
                            m.email,
                            s"Database $dbName access request",
                            s"""There is a new request for database '$dbName'.
                               |${cm
                                 .datasetConfig(dbName)
                                 .contextUrl}""".stripMargin
                          )
                        }
                      }
                      Created(Json.toJson(resp))
                        .as(ResponseLike.contentType)
                        .withHeaders("Location" -> self)
                    } else {
                      throw BadGatewayError(
                        s"Could not create member for db '$dbName'."
                      )
                    }
                  }
              }
          }
        case None =>
          throw BadRequestClientError("No data or wrong format received.")
      }
    }

  // Accept or reject a pending request
  def updateMember(dbName: String) =
    owner(dbName).async { implicit request =>
      request.body.asJson.flatMap {
        _.as[SingleResponse[Document]].data
      } match {
        case Some(data) =>
          if (data.`type` != resourceType) {
            throw BadRequestClientError(
              s"Data submitted with type '${data.`type`}', while this resource is '$resourceType'."
            )
          } else {
            val patched = data.attributes.toJson()
            implicit val cdb = mongoClient.cbf(dbName)
            //check if content of "patched" is a valid user info
            val updatedUser =
              decode[UserInfo](data.attributes.toJson()) match {
                case Right(value) => value
                case Left(e)      => throw BadRequestClientError(s"""
                                                             |Wrong format of data received,
                                                             |${e.getMessage}
                                                             |""".stripMargin)
              }
            val updatedFields = BsonDocument(s"""{
                 |$$set: $patched
                 |
                 |}""".stripMargin)

            //If user is present, update field (and send email if role is being updated)
            cdb.authCol
              .getOne(model.Filters.eq("_id", data.id))
              .map {
                case Some(u) =>
                  cdb.authCol
                    .update(model.Filters.eq("_id", data.id), updatedFields)
                    .map { res =>
                      if (res.wasAcknowledged()) {
                        if (u.role != updatedUser.role) {
                          //send an email to the user which has applied for a membership
                          mailer.send(
                            u.email,
                            s"Database $dbName access request",
                            s"""Your request for accessing database '$dbName' has been accepted.
                               |${cm
                                 .datasetConfig(dbName)
                                 .contextUrl}""".stripMargin
                          )
                        }
                        //If role wasn't changed but pending role was set to None means pending request was rejected by the owner
                        else if (updatedUser.pending.isEmpty) {
                          mailer.send(
                            u.email,
                            s"Database $dbName access request",
                            s"""We are sorry to inform you that your request for accessing database '$dbName' has been rejected. Please contact us if this is a mistake.
                               |mailto:mmp@uit.no""".stripMargin
                          )
                        }
                      }
                    }
                case _ =>
                  throw NotFoundClientError(
                    s"User '$dbName:${data.id}' not found."
                  )
              }
              .map { _ =>
                NoContent
              }
          }
        case None =>
          throw BadRequestClientError("No data or wrong format received.")
      }
    }

  //Is open, owner is granted rights to remove other users, other users can only remove themselves
  def deleteMember(dbName: String, id: String) =
    open.async { implicit request: Request[AnyContent] =>
      implicit val cdb = mongoClient.cbf(dbName)

      cdb.authCol
        .getOne(model.Filters.eq("_id", id))
        .flatMap {

          case Some(userInfo) =>
            if (authorizer.isOwner(dbName)) {

              if (userInfo.role == "owner") {
                //cannot remove last owner of the database
                cdb.authCol
                  .get(model.Filters.eq("role", "owner"))
                  .map {
                    case Seq(owner) if owner._id == id =>
                      throw ConflictClientError(
                        s"Cannot remove last owner of '$dbName'"
                      )
                    case _ => userInfo
                  }
              } else {
                Future(userInfo)
              }

              //if user not an owner
            } else {

              //user can only remove itself
              if (userInfo._id != id) {
                Future.failed(
                  ConflictClientError(s"Cannot remove other user of '$dbName'")
                )
              } else
                Future(userInfo)
            }

          case _ =>
            Future.failed(NotFoundClientError(s"User '$dbName:$id' not found."))
        }
        .flatMap { ui =>
          cdb.authCol
            .delete(model.Filters.eq("_id", id))
            .map { res =>
              if (res.getDeletedCount > 0) {
                mailer.send(
                  ui.email,
                  s"Database $dbName membership",
                  if (authorizer.isOwner(dbName))
                    s"""We are sorry to inform you that your membership as '${ui.role}' for database '$dbName' has been terminated. Please contact us if this is a mistake.
                       |mailto:mmp@uit.no""".stripMargin
                  else
                    s"You have been successfully unsubscribed from the database '$dbName'."
                )
                NoContent.as(ResponseLike.contentType)
              } else
                throw NotFoundClientError(s"User '$dbName:$id' not found.")
            }
        }

    }
}
