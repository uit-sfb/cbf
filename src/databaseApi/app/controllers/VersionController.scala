package controllers

import javax.inject._
import models.binders.Pager
import modules.{Authorizer, MongodbClient}
import io.circe.generic.auto._
import io.circe.syntax._
import io.circe.parser.decode
import models.auth.SecurityLike
import no.uit.sfb.cbf.shared.errors._
import play.api.libs.concurrent.Futures
import play.api.mvc._
import play.api.{Configuration, Logging}
import no.uit.sfb.cbf.shared.jsonapi._
import org.mongodb.scala.bson.Document
import play.api.libs.json.Json
import models.json.JsonApi._
import no.uit.sfb.cbf.shared.model.v1.versioninfo.VersionInfo
import no.uit.sfb.cbf.shared.utils.GenericVersioning
import org.mongodb.scala.model
import org.pac4j.play.scala.SecurityComponents

import scala.concurrent.{ExecutionContext, Future}

@Singleton
class VersionController @Inject()(val controllerComponents: SecurityComponents,
                                  mongoClient: MongodbClient,
                                  implicit val authorizer: Authorizer,
                                  implicit val futures: Futures,
                                  implicit val cfg: Configuration,
                                  implicit val ec: ExecutionContext)
    extends SecurityLike
    with Logging {
  private val resourceType = "versions"

  //If you want to return an error message to the client, please throw an ErrorLike

  def versions(dbName: String, pager: Pager): Action[AnyContent] =
    reader(dbName).async { implicit request: Request[AnyContent] =>
      val cbf = mongoClient.cbf(dbName)
      val fDocs =
        cbf
          .versions(authorizer.isWriter(dbName))
          .map { versions =>
            val selected =
              if (pager.size < 0)
                versions.drop(pager.offset)
              else
                versions.slice(pager.offset, pager.offset + pager.size)
            selected.sortBy(_._id)(GenericVersioning).map { vi =>
              Document(vi.asJson.noSpaces)
            }
          }
      fDocs map { docs =>
        val rawResp = MultipleResponse[Document](docs map { doc =>
          val id = doc.getString("_id")
          Data[Document](resourceType, id, doc)
        })

        val resp = rawResp.copy(
          links = rawResp.links ++ Map(
            "self" -> Some(
              routes.VersionController
                .versions(dbName, pager)
                .absoluteURL()
            )
          )
        )
        Ok(Json.toJson(resp)).as(ResponseLike.contentType)
      }
    }

  def version(dbName: String, version: String): Action[AnyContent] =
    reader(dbName).async { implicit request: Request[AnyContent] =>
      implicit val cdb = mongoClient.cbf(dbName)
      authorizer
        .validatedVersion(dbName, version)
        .flatMap { cbfVersion =>
          val ver = cbfVersion.ver
          val oDoc: Future[Option[Document]] =
            cdb.version(ver, authorizer.isWriter(dbName)).map {
              _.map { vi =>
                Document(vi.asJson.noSpaces)
              }
            }
          oDoc.map { doc =>
            val self = routes.VersionController
              .version(dbName, ver)
              .absoluteURL()
            val data = doc map { d =>
              Data[Document](resourceType, ver, d)
            }
            val resp =
              SingleResponse[Document](data)
                .copy(links = Map("self" -> Some(self)))
            Ok(Json.toJson(resp)).as(ResponseLike.contentType)
          }
        }
    }

  //Create draft or release draft
  def createVersion(dbName: String) =
    owner(dbName).async { implicit request =>
      request.body.asJson.flatMap {
        _.as[SingleResponse[VersionInfo]].data
      } match {
        case Some(data) =>
          if (data.`type` != resourceType) {
            throw BadRequestClientError(
              s"Data submitted with type '${data.`type`}', while this resource is '$resourceType'."
            )
          } else if (data.id.nonEmpty)
            throw BadRequestClientError(
              s"Data submitted with an id, while this resource does not support client-generated ids."
            )
          else {
            val versionInfo = data.attributes
            implicit val cdb = mongoClient.cbf(dbName)
            val cbfVersion = cdb
              .parseVersion(versionInfo._id)
            val ver = cbfVersion.ver
            cdb.versionsCol
              .getOne(model.Filters.eq("_id", ver))
              .flatMap {
                case Some(doc)
                    if doc
                      .getOrElse("releaseDate", "")
                      .asString()
                      .getValue
                      .nonEmpty =>
                  throw ConflictClientError(
                    s"Dataset '$dbName' version '$ver' already exists."
                  )
                case _ =>
                  cdb.versionsCol
                    .replace(
                      model.Filters.eq("_id", ver),
                      Document(Json.toJson(versionInfo).toString()),
                      upsert = true
                    )
                    .map { res =>
                      if (res.wasAcknowledged()) {
                        val raw = SingleResponse[VersionInfo](
                          Some(
                            Data[VersionInfo](resourceType, ver, versionInfo)
                          )
                        )
                        val self = routes.VersionController
                          .version(dbName, ver)
                          .absoluteURL()
                        val resp =
                          raw.copy(
                            links = raw.links ++ Map("self" -> Some(self))
                          )
                        Created(Json.toJson(resp))
                          .as(ResponseLike.contentType)
                          .withHeaders("Location" -> self)
                      } else {
                        throw BadGatewayError()
                      }
                    }
              }
          }
        case None =>
          throw BadRequestClientError("No data or wrong format received.")
      }
    }

  def updateVersion(dbName: String) =
    owner(dbName).async { implicit request =>
      request.body.asJson.flatMap {
        _.as[SingleResponse[Document]].data
      } match {
        case Some(data) =>
          if (data.`type` != resourceType) {
            throw BadRequestClientError(
              s"Data submitted with type '${data.`type`}', while this resource is '$resourceType'."
            )
          } else {
            val patchDoc = data.attributes
            implicit val cdb = mongoClient.cbf(dbName)
            //check if content of "patchDoc" is a valid version info
            decode[VersionInfo](data.attributes.toJson()) match
            {
              case Right(_) =>
              case Left(e) => throw BadRequestClientError(s"""
                                                             |Wrong format of data received,
                                                             |${e.getMessage}
                                                             |""".stripMargin)
            }
            authorizer
              .validatedVersion(dbName, data.id)
              .flatMap { cbfVersion =>
                val ver = cbfVersion.ver
                cdb.versionsCol
                  .getOne(model.Filters.eq("_id", ver))
                  .flatMap {
                    case Some(doc) =>
                      val patched = doc ++ patchDoc
                      cdb.versionsCol
                        .replace(
                          model.Filters.eq("_id", ver),
                          Document(Json.toJson(patched).toString())
                        )
                        .map { _ =>
                          NoContent
                        }
                    case None =>
                      throw NotFoundClientError(ver)
                  }
              }
          }
        case None =>
          throw BadRequestClientError("No data or wrong format received.")
      }
    }

  //Attention: deletes the data too (every record having valid.start == x)
  def deleteVersion(dbName: String, version: String) =
    owner(dbName).async { implicit request: Request[AnyContent] =>
      implicit val cdb = mongoClient.cbf(dbName)
      authorizer.validatedVersion(dbName, version).flatMap { cbfVersion =>
        val ver = cbfVersion.ver
        println(cbfVersion.release)
        cdb
          .recordsCol(cbfVersion)
          .delete(model.Filters.eq("valid.start", cbfVersion.release), false)
          .zip(cdb.versionsCol.delete(model.Filters.eq("_id", ver), true))
          .map {
            case (_, res) =>
              if (res.getDeletedCount > 0)
                NoContent.as(ResponseLike.contentType)
              else
                throw NotFoundClientError(s"Version '$dbName:$ver' not found.")
          }
      }
    }
}
