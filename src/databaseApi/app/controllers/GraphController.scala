package controllers

import akka.NotUsed
import akka.stream.scaladsl.Source
import models.auth.SecurityLike

import javax.inject._
import models.binders.Graph
import no.uit.sfb.cbf.shared.errors.BadRequestClientError
import models.json.BsonImplicit._
import modules.{Authorizer, GraphModule, MongodbClient}
import org.pac4j.play.scala.SecurityComponents
import play.api.libs.concurrent.Futures
import play.api.libs.json.Json
import play.api.mvc._
import play.api.{Configuration, Logging}
import util.FutureToIteratorWrapper

import scala.concurrent.ExecutionContext
import scala.util.Try

@Singleton
class GraphController @Inject()(val controllerComponents: SecurityComponents,
                                mongoClient: MongodbClient,
                                gm: GraphModule,
                                val authorizer: Authorizer,
                                implicit val futures: Futures,
                                implicit val cfg: Configuration,
                                implicit val ec: ExecutionContext)
    extends SecurityLike
    with Logging {

  //If you want to return an error message to the client, please throw an ErrorLike

  def graph(dbName: String,
            oVer: Option[String],
            graph: Option[Graph]): Action[AnyContent] = reader(dbName).async {
    implicit request: Request[AnyContent] =>
      implicit val cdb = mongoClient.cbf(dbName)
      oVer match {
        case Some(version) =>
          //Because the pipeline makes graph queries before release, we need to disable
          //auth check and return draft versions.
          //authorizer.validatedVersion(dbName, version)
          cdb
            .version(version, true)
            .map {
              case Some(vi) =>
                cdb.parseVersion(vi._id)
              case None =>
                throw BadRequestClientError(
                  s"Version '$version' does not exist for dataset '$dbName'."
                )
            }
            .map { cbfVersion =>
              graph.map { g =>
                gm.get(dbName, cbfVersion, g)
              } match {
                case Some(f) =>
                  val fRes = f.map { doc =>
                    Json.stringify(Json.toJson(doc))
                  }
                  val fit = new FutureToIteratorWrapper(fRes, {
                    Thread.sleep(500)
                    " "
                  })
                  val stream: Source[String, NotUsed] =
                    Source.fromIterator(() => fit)
                  Ok.chunked(stream, true, None)
                case None =>
                  Ok("""{ "graph": [] }""")
              }
            }
        case None =>
          val proto = Try {
            request.headers("X-FORWARDED-PROTO")
          }.toOption
            .getOrElse("http")
          cdb.latest(authorizer.isWriter(dbName)).map {
            case Some(v) =>
              Redirect(
                routes.GraphController
                  .graph(dbName, Some(v._id), graph)
                  .absoluteURL(proto == "https")
              )
            case None =>
              Ok("""{ "graph": [] }""")
          }
      }
  }
}
