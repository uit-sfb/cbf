package controllers

import javax.inject._
import no.uit.sfb.cbf.shared.errors.BadRequestClientError
import modules.{AccessModule, Authorizer}
import no.uit.sfb.cbf.shared.jsonapi._
import no.uit.sfb.cbf.shared.model.v1.nativ.CompactIRI
import play.api.libs.concurrent.Futures
import play.api.libs.json.{JsArray, JsString, Json}
import play.api.mvc._
import play.api.{Configuration, Logging}
import io.circe.generic.auto._
import io.circe.parser
import io.circe.syntax._
import models.auth.SecurityLike
import models.binders.Pager
import models.json.JsonApi._
import org.mongodb.scala.Document
import org.pac4j.play.scala.SecurityComponents
import play.api.libs.ws.WSClient

import scala.concurrent.duration._
import scala.concurrent.{Await, ExecutionContext, blocking}

@Singleton
class CiriController @Inject()(val controllerComponents: SecurityComponents,
                               am: AccessModule,
                               val ws: WSClient,
                               val authorizer: Authorizer,
                               implicit val futures: Futures,
                               implicit val cfg: Configuration,
                               implicit val ec: ExecutionContext)
    extends SecurityLike
    with Logging {
  //If you want to return an error message to the client, please throw an ErrorLike

  private def jsonapiResponse(
    it: Iterator[Map[String, String]]
  ): MultipleResponse[Document] = {
    MultipleResponse[Document](it.map { m =>
      val doc = Document(m)
      val id = doc.getString("_id")
      Data[Document]("ciris", id, doc)
    }.toSeq)
  }

  def ciris(dbName: String,
            filter: Option[String],
            pager: Pager): Action[AnyContent] =
    Action.async { implicit request: Request[AnyContent] =>
      val size =
        if (pager.size < 0)
          None
        else
          Some(pager.size)
      val resolver = am.resolver(dbName)
      resolver.search(filter.getOrElse(""), Some(pager.offset), size).map {
        ciris =>
          val it = ciris.iterator.map { ciri =>
            Await.result(blocking(resolver.get(ciri)), 15.seconds)
          }
          val rawResp = jsonapiResponse(it)
          val numRecs = rawResp.data.size
          val itemStart = if (numRecs == 0) 0 else pager.offset + 1
          val itemEnd = if (numRecs == 0) 0 else itemStart + numRecs - 1
          val resp = rawResp.copy(
            meta = rawResp.meta ++ Seq(
              if (size.isEmpty || numRecs < size.get)
                Some("totalItems" -> numRecs.toString)
              else
                None,
              Some("itemStart" -> itemStart.toString),
              Some("itemEnd" -> itemEnd.toString),
            ).flatten.toMap,
            links = rawResp.links ++ Map(
              "self" -> Some(
                routes.CiriController
                  .ciris(dbName, filter, pager)
                  .absoluteURL()
              )
            )
          )
          Ok(Json.toJson(resp)).as(ResponseLike.contentType)
      }
    }

  def ciri(dbName: String, ciri: String): Action[AnyContent] = Action.async {
    implicit request: Request[AnyContent] =>
      val fRes = CompactIRI(ciri) match {
        case Some(x) =>
          am.resolver(dbName).getOption(x)
        case None =>
          throw BadRequestClientError(s"'$ciri' is not a compact IRI")
      }
      fRes map { oRes =>
        val d = oRes.map { res =>
          Data[Document]("ciris", ciri, Document(res.asJson.noSpaces))
        }
        val self = routes.CiriController
          .ciri(dbName, ciri)
          .absoluteURL()
        val resp =
          SingleResponse[Document](d)
            .copy(links = Map("self" -> Some(self)))
        Ok(Json.toJson(resp)).as(ResponseLike.contentType)
      }
  }

  //This method is used to discover prefixes on Identifiers.org and EbiOls in order to pre-fill resolvers details
  def resolver(prefix: String) = Action.async {
    val p = prefix.toLowerCase
    //Get pattern from Identifiers.org (if exists there)
    val identifiersDotOrgAsync = ws
      .url(
        s"https://registry.api.identifiers.org/restApi/namespaces/search/findByPrefix?prefix=$p"
      )
      .get()
      .map { resp =>
        if (resp.status < 400)
          (resp.json \\ "pattern").headOption.map {
            _.asInstanceOf[JsString].value
          } else
          None
      }
    //Get baseUri from EbiOls if (exists there)
    val ebiOlsAsync = ws
      .url(s"https://www.ebi.ac.uk/ols/api/ontologies/$p")
      .get()
      .map { resp =>
        if (resp.status < 400)
          (resp.json \\ "config").headOption.flatMap { j =>
            (j \\ "baseUris").headOption.flatMap {
              _.asInstanceOf[JsArray].value.headOption.map {
                _.asInstanceOf[JsString].value
              }
            }
          } else None
      }
    identifiersDotOrgAsync.zip(ebiOlsAsync).map {
      case (oPattern, oBaseUri) =>
        val a: Map[String, String] = oPattern match {
          case Some(pattern) =>
            val reg = "^\\^?(?:(\\w+):)(.*)".r
            //In some cases (such as RRID), the prefix is included in the pattern and needs
            //  1) to be removed from the regexp
            //  2) to be used in the redirect URL
            val (customPrefix, customPattern) = pattern match {
              case reg(prefix, rest) =>
                prefix -> rest
              case _ =>
                p -> pattern
            }
            //In some cases the patterns registered in indentifiers.org are wrong.
            //We override them here
            val overriddenPattern =
              p match {
                case "biosample" =>
                  "^SAM[NED][A-Z]?(\\d+)$"
                case "biotools" =>
                  "^[-A-Za-z0-9\\_]*$"
                case _ =>
                  if (customPattern.startsWith("^")) customPattern
                  else s"^$customPattern"
              }
            Map(
              "_iri" -> s"https://identifiers.org/$customPrefix:{id}",
              "pattern" -> overriddenPattern
            )
          case None =>
            Map()
        }
        val b: Map[String, String] = oBaseUri match {
          case Some(baseUri) =>
            Map("baseUri" -> baseUri)
          case None =>
            Map()
        }
        val c =
          if (a.contains("_iri"))
            Map()
          else
            oBaseUri match {
              case Some(baseUri) =>
                Map(
                  "_iri" -> s"https://www.ebi.ac.uk/ols/ontologies/$prefix/terms?iri=$baseUri{id}"
                )
              case None =>
                Map()
            }
        Ok(Json.toJson(a ++ b ++ c))
    }
  }
}
