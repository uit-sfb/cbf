import com.typesafe.scalalogging.LazyLogging
import play.api.http.HttpErrorHandler
import play.api.mvc._
import play.api.mvc.Results._

import scala.concurrent._
import javax.inject.Singleton
import no.uit.sfb.cbf.shared.errors.{
  ErrorLike,
  GatewayTimeoutError,
  InternalServerErrorError
}
import play.api.libs.json.Json
import models.json.JsonApi._
import no.uit.sfb.cbf.shared.jsonapi.{Err, ErrorResponse, ResponseLike}

import java.util.concurrent.CompletionException

@Singleton
class ErrorHandler extends HttpErrorHandler with LazyLogging {
  def onClientError(request: RequestHeader,
                    statusCode: Int,
                    message: String): Future[Result] = {
    Future.successful {
      logger.error(s"An error occurred ($statusCode): $message")
      Status(statusCode)(
        Json.toJson(
          ErrorResponse(
            Seq(Err(statusCode.toString, "A client error occurred", message))
          )
        )
      )
    }
  }

  def onServerError(request: RequestHeader,
                    exception: Throwable): Future[Result] = {
    exception match {
      case e: CompletionException =>
        onServerError(request, e.getCause)
      case e: ExecutionException => //Boxed Error
        onServerError(request, e.getCause)
      case e: ErrorLike =>
        logger.error("An error occurred:", e)
        Future.successful(
          Status(e.statusCode)(Json.toJson(ErrorResponse(Seq(e.toErr))))
            .as(ResponseLike.contentType)
        )
      case e: TimeoutException =>
        logger.error("A timeout error occurred:", e)
        e.printStackTrace()
        onServerError(request, GatewayTimeoutError(e.getMessage))
      case e: Throwable =>
        logger.error("An unexpected error occurred:", e)
        e.printStackTrace()
        onServerError(request, InternalServerErrorError(e.getMessage))
    }
  }
}
