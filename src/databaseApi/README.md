# Database API

This is the backend serving the contextual database API.

## Requirements

- Java 11 or later
- MongoDB running (on port 27017)

## Getting started

Run:
- `cd src`
- `sbt "databaseApi / run"`

The UI should be available at `http://localhost:9000`.
SBT is able to detect code change and compile on-the-fly.

### Pre-populated database

Run:
- `cd src`
- `sbt "dataToolset / testOnly *PushTest"`

This will run a task populating the database with two datasets (`test` and `test2`).
