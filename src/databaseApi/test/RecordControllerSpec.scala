import org.junit.runner._
import org.specs2.runner._
import play.api.test._

/**
  * Add your spec here.
  * You can mock out a whole application including requests, plugins etc.
  * For more information, consult the wiki.
  */
@RunWith(classOf[JUnitRunner])
class RecordControllerSpec() extends PlaySpecification {

  "Application" should {

    "send 404 on a bad request" in new WithApplication {
      route(app, FakeRequest(GET, "/boum/badaboum")) must beSome.which(
        status(_) == NOT_FOUND)
    }

    "render the ver page" in new WithApplication {
      val home = route(app, FakeRequest(GET, "/info")).get

      status(home) must equalTo(OK)
      contentType(home) must beSome.which(_ == "application/json")
      //contentAsString(home) must contain ("shouts out")
    }
  }
}
