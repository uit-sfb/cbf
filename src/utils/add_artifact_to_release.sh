#!/usr/bin/env bash

set -e

THIS_PATH="$( cd "$( dirname "$0" )" && pwd )"

if [[ -z "$GITLAB_SECRET_API" ]]; then
  echo "You must first set up GITLAB_SECRET_API envir (containing Gitlab private token with API rights)"
  exit 1
fi

POSITIONAL=()
while [[ $# -gt 0 ]]
do
  case "$1" in
      -h|--help)
      echo "Add link to Gitlab release"
      echo ""
      echo "Usage:"
      echo "--project-id <projectId>"
      echo "--version <release version or tag>"
      echo "--artifact <artifactName>"
      echo ""
      echo "[-h|--help]"
      echo "print this help message"
      exit 0
      ;;
    --project-id)
      PROJECT_ID=$2
      shift
      shift
      ;;
    --version)
     VERSION=$2
     shift
     shift
     ;;
    --artifact)
      ARTIFACT_NAME=$2
      shift
      shift
      ;;
      *) #Unknown
      POSITIONAL+=("$1")
      shift
      ;;
  esac
done

curl --request POST --header "PRIVATE-TOKEN: $GITLAB_SECRET_API" \
--data name="$(basename $ARTIFACT_NAME)" \
--data url="https://artifactory.metapipe.uit.no/artifactory/generic-local/no.uit.sfb/${ARTIFACT_NAME}" \
"https://gitlab.com/api/v4/projects/${PROJECT_ID}/releases/${VERSION}/assets/links"
