package modules

import com.google.inject.{AbstractModule, Provides}
import com.typesafe.scalalogging.LazyLogging
import models.auth.builder.ClientListBuilder
import org.pac4j.core.client.Clients
import org.pac4j.core.config.Config
import org.pac4j.core.context.session.SessionStore
import org.pac4j.core.http.url.DefaultUrlResolver
import org.pac4j.play.http.PlayHttpActionAdapter
import org.pac4j.play.scala.{DefaultSecurityComponents, SecurityComponents}
import org.pac4j.play.store.PlayCacheSessionStore
import org.pac4j.play.{CallbackController, LogoutController}
import play.api.libs.ws.WSClient
import play.api.{Configuration, Environment}

import scala.concurrent.ExecutionContext

/**
  * Guice DI module to be included in application.conf
  */
//Do NOT remove Environment!
class SecurityModule(environment: Environment, configuration: Configuration)
    extends AbstractModule
    with LazyLogging {

  override def configure(): Unit = {
    bind(classOf[SessionStore]).to(classOf[PlayCacheSessionStore])

    bind(classOf[SecurityComponents]).to(classOf[DefaultSecurityComponents])

    // callback
    val callbackController = new CallbackController()
    callbackController.setRenewSession(false)
    bind(classOf[CallbackController]).toInstance(callbackController)

    // logout
    val logoutController = new LogoutController()
    //For some reason, the default url is not inserted in the call to the oidc server (post_logout_redirect_uri parameter) when doing a central logout
    //logoutController.setCentralLogout(true)
    //Needs to be an absolute path. To override it, add url=/my/logout/path to the call to /logout route
    logoutController.setDefaultUrl("/")
    bind(classOf[LogoutController]).toInstance(logoutController)
  }

  @Provides
  def provideConfig()(implicit ws: WSClient, ec: ExecutionContext): Config = {
    val cls = new ClientListBuilder(configuration).buildAll
    val clients = new Clients(cls: _*)
    //When using a relative Url with completeRelativeUrl set to true, it seems that the web context indicates http when it should be https...
    configuration.getOptional[String]("app.externalUrl").flatMap {
      case "" => None
      case x  => Some(x)
    } match {
      case Some(url) =>
        clients.setCallbackUrl(url + "/callback")
        clients.setUrlResolver(new DefaultUrlResolver(false))
      case None =>
        clients.setCallbackUrl("/callback")
        clients.setUrlResolver(new DefaultUrlResolver(true))
    }
    val config = new Config(clients)
    config.setHttpActionAdapter(new PlayHttpActionAdapter())
    config
  }
}
