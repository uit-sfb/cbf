package modules

import models.User
import models.auth.builder.ClientListBuilder
import org.pac4j.core.authorization.authorizer.DefaultAuthorizers
import org.pac4j.core.profile.CommonProfile
import org.pac4j.play.scala.Pac4jScalaTemplateHelper
import play.api.mvc.RequestHeader
import play.api.{Configuration, Logging}

import javax.inject.{Inject, Singleton}
import scala.concurrent.ExecutionContext

@Singleton
class SimpleAuthorizer @Inject()(
  implicit val pac4jTemplateHelper: Pac4jScalaTemplateHelper[CommonProfile],
  implicit val cfg: Configuration,
  implicit val ex: ExecutionContext
) extends Logging {
  protected val clientBuilder = new ClientListBuilder(cfg)

  //Attention: setting header X-Requested-With: XMLHttpRequest disables direct clients
  //Attention: unless one has already logged in and a profile has been created, AnonymousClient will always have precedence over indirect clients
  //This means that if you want to force using a direct client, you need to remove AnonymousClient from the client list.
  val clients = clientBuilder.clients.mkString(",")
  val clientsWithoutDefault = {
    val tmp =
      clientBuilder.clients.filter { _ != "AnonymousClient" }.mkString(",")
    if (tmp.isEmpty)
      "AnonymousClient"
    else
      tmp
  }

  val bypassSecurity = clientBuilder.indirectClients.isEmpty

  val authMode =
    if (bypassSecurity)
      DefaultAuthorizers.IS_ANONYMOUS
    else
      DefaultAuthorizers.IS_AUTHENTICATED

  def isLoggedIn(implicit request: RequestHeader) =
    pac4jTemplateHelper.getCurrentProfile.isDefined

  def user(implicit request: RequestHeader): Option[User] = {
    pac4jTemplateHelper.getCurrentProfile.map { User(_) }
  }
}
