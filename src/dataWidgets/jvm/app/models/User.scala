package models

import models.User.UserId
import org.pac4j.core.profile.CommonProfile
import org.pac4j.oidc.profile.OidcProfile

case class User(id: UserId,
                username: String,
                name: String,
                email: String,
                accessToken: Option[String] = None)

object User {
  type UserId = String

  def apply(p: CommonProfile): User = {
    val tokens = p match {
      case oidcProfile: OidcProfile =>
        Some(oidcProfile.getAccessToken)
      case _ => None
    }
    User(p.getId, p.getUsername, p.getDisplayName, p.getEmail, tokens.map {
      _.getValue
    })
  }
}
