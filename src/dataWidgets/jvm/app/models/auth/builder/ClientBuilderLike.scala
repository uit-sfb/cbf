package models.auth.builder

import org.pac4j.core.client.BaseClient
import play.api.Configuration

trait ClientBuilderLike {
  def direct: Boolean
  def name(cfg: Configuration) = cfg.get[String]("name")
  def apply(cfg: Configuration): BaseClient
}
