package models.auth.builder

import org.pac4j.core.client.Client
import play.api.Configuration

class ClientListBuilder(configuration: Configuration) {
  lazy val indirectClients = builders.collect {
    case (b, conf) if !b.direct => b.name(conf)
  }
  lazy val directClients = builders.collect {
    case (b, conf) if b.direct => b.name(conf)
  }
  //Always indirect before direct clients
  lazy val clients = indirectClients ++ directClients

  protected val clientsConfig =
    configuration
      .getOptional[Seq[Configuration]]("app.auth.clients")
      .getOrElse(Seq())

  protected def builders = {
    val clients = clientsConfig.map {
      case cfg if cfg.get[String]("type") == "oidc" =>
        OidcClientBuilder -> cfg
      case cfg =>
        throw new Exception(
          s"Unknown auth client type '${cfg.get[String]("type")}'"
        )
    }
    clients :+ (AnonymousClientBuilder -> configuration)
  }

  def buildAll: Seq[Client] = {
    builders.map { case (b, conf) => b(conf) }
  }
}
