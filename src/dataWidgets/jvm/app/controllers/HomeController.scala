package controllers

import modules.SimpleAuthorizer

import javax.inject._
import play.api.Configuration
import play.api.mvc._
import no.uit.sfb.info.datawidgets.BuildInfo
import org.pac4j.core.profile.CommonProfile
import org.pac4j.play.scala.{Security, SecurityComponents}
import play.api.libs.json.Json

import scala.concurrent.{Await, ExecutionContext}
import scala.concurrent.duration.DurationInt
import scala.util.Try

@Singleton
class HomeController @Inject()(val controllerComponents: SecurityComponents,
                               implicit val ec: ExecutionContext,
                               implicit val conf: Configuration,
                               implicit val authorizer: SimpleAuthorizer)
    extends Security[CommonProfile] {

  def index = Action { implicit request: Request[AnyContent] =>
    Ok(views.html.index(BuildInfo.version))
  }

  def info() = Action { implicit request: Request[AnyContent] =>
    val res = Map(
      "name" -> BuildInfo.name,
      "ver" -> BuildInfo.version,
      "git" -> BuildInfo.gitCommit
    )
    Ok(Json.toJson(res))
  }

  def api() = Action { implicit request: Request[AnyContent] =>
    val apiHost = conf.get[String]("app.apiHost")
    PermanentRedirect(apiHost + "/docs/api")
  }
}
