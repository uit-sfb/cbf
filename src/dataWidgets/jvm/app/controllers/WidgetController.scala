package controllers

import modules.SimpleAuthorizer
import org.pac4j.core.authorization.authorizer.DefaultAuthorizers
import org.pac4j.core.profile.CommonProfile
import org.pac4j.play.scala.{Security, SecurityComponents}

import javax.inject._
import play.api.Configuration
import play.api.mvc._

import scala.concurrent.ExecutionContext
import scala.util.Try

@Singleton
class WidgetController @Inject()(val controllerComponents: SecurityComponents,
                                 implicit val ec: ExecutionContext,
                                 implicit val conf: Configuration,
                                 implicit val authorizer: SimpleAuthorizer)
    extends Security[CommonProfile] {

  protected def secureAction(auth: Boolean) = {
    if (auth) {
      Secure(authorizer.clientsWithoutDefault, authorizer.authMode)
    } else
      Secure(authorizer.clients, DefaultAuthorizers.NONE)
  }

  def browser(dbName: String,
              auth: Boolean,
              targetId: String,
              targetDs: String) =
    secureAction(auth) { implicit request: Request[AnyContent] =>
      val accessToken = authorizer.user.flatMap { _.accessToken }.getOrElse("")
      Ok(
        views.html
          .browser(accessToken, dbName, targetId, targetDs)
      )
    }

  def record(dbName: String,
             version: Option[String],
             id: String,
             auth: Boolean) =
    secureAction(auth) { implicit request: Request[AnyContent] =>
      val accessToken = authorizer.user.flatMap { _.accessToken }.getOrElse("")
      Ok(
        views.html
          .record(accessToken, dbName, version, id)
      )
    }

  def graph(dbName: String,
            version: Option[String],
            searchQuery: Option[String],
            x: String,
            y: String,
            pres: Option[String],
            height: Int,
            auth: Boolean) = secureAction(auth) {
    implicit request: Request[AnyContent] =>
      val accessToken = authorizer.user.flatMap { _.accessToken }.getOrElse("")
      Ok(
        views.html
          .graph(accessToken, dbName, version, searchQuery, x, y, pres, height)
      )
  }

  def map(dbName: String,
          version: Option[String],
          searchQuery: Option[String],
          attr: Option[String],
          center: String,
          zoom: Int,
          height: Int,
          auth: Boolean) = secureAction(auth) {
    implicit request: Request[AnyContent] =>
      val accessToken = authorizer.user.flatMap { _.accessToken }.getOrElse("")
      Ok(
        views.html
          .map(
            accessToken,
            dbName,
            version,
            searchQuery,
            attr,
            center,
            zoom,
            height
          )
      )
  }

  def geo(dbName: String,
          version: Option[String],
          searchQuery: Option[String],
          x: String,
          y: String,
          auth: Boolean) = secureAction(auth) {
    implicit request: Request[AnyContent] =>
      val accessToken = authorizer.user.flatMap { _.accessToken }.getOrElse("")
      Ok(
        views.html
          .geo(accessToken, dbName, version, searchQuery, x, y)
      )
  }

  def settings(dbName: String, auth: Boolean) = secureAction(auth) {
    implicit request: Request[AnyContent] =>
      val accessToken = authorizer.user.flatMap { _.accessToken }.getOrElse("")
      Ok(
        views.html
          .settings(accessToken, dbName)
      )
  }

  def launcher(auth: Boolean) = secureAction(auth) {
    implicit request: Request[AnyContent] =>
      val accessToken = authorizer.user.flatMap { _.accessToken }.getOrElse("")
      Ok(views.html.launcher(accessToken))
  }
}
