const path = require('path');

//We load the default config
module.exports = require('./scalajs.webpack.config');

//Some libraries are not plain Javascript so they need to be transpiled before bundling
//Add them in the include array
const babelLoader = {
    test: /\.m?js$/,
    include: [
        path.resolve(__dirname, "node_modules/@react-leaflet"),
        path.resolve(__dirname, "node_modules/react-leaflet")
    ],
    use: {
        loader: 'babel-loader',
        options: {
            presets: ['@babel/preset-env']
        }
    }
};

//We modify the default config here
module.exports.module.rules.push(babelLoader);
