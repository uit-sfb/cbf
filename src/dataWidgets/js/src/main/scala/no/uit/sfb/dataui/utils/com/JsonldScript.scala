package no.uit.sfb.dataui.utils.com

import japgolly.scalajs.react.AsyncCallback
import org.scalajs.dom

import scala.scalajs.js.annotation.{JSExport, JSExportTopLevel}

@JSExportTopLevel("JsonldScript")
class JsonldScript(apiEndpoint: String,
                   accessToken: String,
                   dbName: String,
                   version: String,
                   recordId: String) {

  private val ver = if (version.isEmpty) None else Some(version)

  private val dbApi =
    DatabaseApi(
      apiEndpoint,
      if (accessToken.isEmpty) None else Some(accessToken),
      dbName
    )

  private def loadJsonld(): AsyncCallback[String] = {
    dbApi
      .get(
        dbApi
          .record(
            ver,
            Some(recordId),
            format = Some("jsonld"),
            inline = Some(false)
          )
          .toString(),
        contentType = Some("application/ld+json")
      )
      .map {
        _.responseText
      }
  }

  def addJsonLd(jsonld: String) = {
    val script = dom.document.createElement("script")
    script.setAttribute("type", "application/ld+json")
    script.textContent = jsonld
    dom.document.head.appendChild(script)
  }

  @JSExport
  val add: Unit = {
    val insertJsonLd = loadJsonld() |> {
      addJsonLd _
    }
    insertJsonLd.runNow()
  }
}
