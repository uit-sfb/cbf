package no.uit.sfb.dataui.components.settings

import japgolly.scalajs.react.Ref.Simple
import japgolly.scalajs.react.component.Scala.BackendScope
import japgolly.scalajs.react.vdom.html_<^._
import japgolly.scalajs.react.{Callback, CallbackTo, Ref, ScalaComponent}
import no.uit.sfb.cbf.shared.config._
import no.uit.sfb.dataui.utils.comp.{ButtonField, LabelField, TextField}
import no.uit.sfb.facade.bootstrap.Table
import no.uit.sfb.facade.bootstrap.form._
import no.uit.sfb.facade.bootstrap.grid.{Col, Row}
import no.uit.sfb.facade.icon.Glyphicon
import org.scalajs.dom
import org.scalajs.dom.html
import org.scalajs.dom.raw.HTMLInputElement

object ParsingSettingsComp {

  case class Props(draft: DatasetConfig, updateDraft: DatasetConfig => Callback, noEdit: Boolean)

  class Backend($: BackendScope[Props, Unit]) {
    private val ref: Simple[html.Input] = Ref[html.Input]

    def render(p: Props): VdomElement = {
      val alternateNames = Row()(
        Col(md = 2)(LabelField("Alternate attribute names", info = "Names that may be used in the header of the CSV/TSV file instead of the attribute reference or name.")()),
        Col()(
        Table()(<.tbody(
          p.draft.attrs.collect{ case attr if ! attr.isFullyControlled => //We exclude template attributes since they are always ignored on import anyways
            val selectId = s"attrAlternateName-${attr.ref}"
            <.tr(
              <.td(
                attr.ref
              ),
              <.td(
                Table()(<.tbody(
                  attr.alternateNames.map { r =>
                    <.tr(
                      <.td(r),
                      <.td(^.textAlign.center,
                        if (p.noEdit) <.div() else
                        ButtonField(
                          variant = "danger",
                          icon = Glyphicon("Trash"),
                          onClick =
                            CallbackTo {
                              p.draft.copy(attrs = p.draft.attrs.map(t =>
                                if (t.ref != attr.ref)
                                  t
                                else
                                  attr.copy(alternateNames = attr.alternateNames.filter(_ != r))
                              ))
                            } >>=
                              p.updateDraft,
                          info = "Remove name"
                        )
                      )
                    )
                  } ++ (if (p.noEdit) Seq() else Seq(<.tr(
                    <.td(
                      //Do not convert to TextField
                      FormControl.uncontrolled(
                        ref,
                        id = s"$selectId-name-add",
                      )()),
                    <.td(^.textAlign.center,
                      if (p.noEdit) <.div() else
                      ButtonField(
                        variant = "primary",
                        icon = Glyphicon("Add"),
                        onClick = CallbackTo { //Note we NEED to build the new draft within a callback otherwise the logic is executed at render time!
                          val newResElem = dom.document
                            .getElementById(s"$selectId-name-add")
                            .asInstanceOf[HTMLInputElement]
                          val newRes = newResElem.value
                          newResElem.value = newResElem.defaultValue
                          if (newRes.nonEmpty)
                            p.draft.copy(attrs = p.draft.attrs.map { t =>
                              if (t.ref == attr.ref && !attr.alternateNames.contains(newRes))
                                attr.copy(alternateNames = attr.alternateNames :+ newRes)
                              else
                                attr
                            })
                          else
                            p.draft
                        } >>= p.updateDraft,
                        info = "Add name"
                      )
                    )
                  ))):_*)
                )
              )
            )
          }:_*
        ))
      ))
      <.div(
        <.p("The following settings are only use when importing CSV/TSV data files."),
        FormGroup("valueSeparatorInput")(
          TextField(
            disabled = if (p.noEdit) Some("") else None,
            labelWidth = 2,
            label = "CSV/TSV separator",
            value = p.draft.valueSeparator,
            onChange = newValue =>
                p.updateDraft(p.draft.copy(valueSeparator = newValue)),
            info = "Character used to separate columns."
          )()
        ),
        FormGroup("subValueSeparatorInput")(
          TextField(
            disabled = if (p.noEdit) Some("") else None,
            labelWidth = 2,
            label = "Multiple value separator",
            value = p.draft.subValueSeparator,
            onChange =
              newValue => p.updateDraft(p.draft.copy(subValueSeparator = newValue)),
            info = "Character used to separate multiple values within one cell."
          )()
        ),
        alternateNames
      )
    }
  }

  private lazy val component = ScalaComponent
    .builder[Props]
    .renderBackend[Backend]
    .build

  def apply(draft: DatasetConfig, updateDraft: DatasetConfig => Callback, noEdit: Boolean) =
    component(Props(draft, updateDraft, noEdit))
}
