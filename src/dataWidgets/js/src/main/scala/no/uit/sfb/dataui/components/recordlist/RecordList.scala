package no.uit.sfb.dataui.components.recordlist

import japgolly.scalajs.react.component.Scala.BackendScope
import japgolly.scalajs.react.vdom.html_<^._
import japgolly.scalajs.react.{Callback, Reusable, ScalaComponent, ~=>}
import no.uit.sfb.cbf.shared.auth.UserInfo
import no.uit.sfb.cbf.shared.config.DatasetConfig
import no.uit.sfb.cbf.shared.config.ui.TabObject
import no.uit.sfb.cbf.shared.jsonapi.Data
import no.uit.sfb.cbf.shared.model.v1.nativ.{Record, Statement}
import no.uit.sfb.cbf.shared.model.v1.versioninfo.VersionInfo
import no.uit.sfb.dataui.components.record.MultiStatementComp
import no.uit.sfb.dataui.utils.VirtualAttributeResolver
import no.uit.sfb.dataui.utils.com.{DatabaseApi, ResourceUrl}
import no.uit.sfb.facade.bootstrap._
import no.uit.sfb.facade.bootstrap.overlay.{OverlayTrigger, Tooltip}
import org.scalajs.dom

object RecordList {

  case class Props(ver: Option[String],
                   versions: Seq[VersionInfo],
                   data: Seq[Data[Record]],
                   recordUrl: String => String,
                   tab: TabObject,
                   availableSizes: Seq[Int],
                   pageSize: Int,
                   itemStart: Option[Int] = None,
                   itemEnd: Option[Int] = None,
                   totalFilteredItems: Option[Int] = None,
                   totalUnfilteredItems: Option[Int] = None,
                   links: Map[String, Option[String]],
                   changePageOffset: Int ~=> Callback,
                   changePageSize: Int ~=> Callback,
                   dbApi: DatabaseApi,
                   resourceUrl: ResourceUrl,
                   config: DatasetConfig,
                   userInfo: UserInfo,
                   newRecord: Boolean = false,
                   hideRecord: Callback,
                   targetId: String,
                   targetDs: String
                  ) {
    def newRecordSupplier(): Record = {
      if (targetDs.isEmpty || targetId.isEmpty)
        Record()
      else {
        val typeName = s"dblink:direct:$targetDs"
        config.attrs.find(attr => attr.`type` == typeName) match {
          case Some(attr) =>
            Record(Vector(Map(attr.ref -> Statement(targetId))))
          case None =>
            println(s"Could not find attribute of type '$typeName'")
            Record()
        }
      }
    }
  }

  case class State(showRecord: Option[Record] = None)

  class Backend($: BackendScope[Props, State]) {
    val showRecord =
      Reusable.fn((record: Record) =>
        $.modState(_.copy(showRecord = Some(record))))
    val hideRecord =
      $.modState(_.copy(showRecord = None))

    def renderModal(p: Props, s: State): VdomNode = {
      s.showRecord map { record =>
        RecordModal(
          record,
          p.config,
          p.dbApi,
          p.resourceUrl,
          p.ver,
          p.versions,
          hideRecord >> p.hideRecord,
          p.userInfo,
          p.newRecord,
          p.targetId,
          p.targetDs
        )
      }
    }

    def render(p: Props, s: State): VdomNode = {
      val vAttrResolver = new VirtualAttributeResolver(p.config)
      val controlComp = ControlComp(
        ControlComp.Props(
          p.availableSizes,
          p.pageSize,
          p.itemStart,
          p.itemEnd,
          p.totalFilteredItems,
          p.totalUnfilteredItems,
          p.links,
          p.changePageOffset,
          p.changePageSize
        ))
      val tableComp = Table(responsive = "md", hover = true)(
        <.thead(
          <.tr(
            p.tab.attrs.map { attr =>
              <.th(
                OverlayTrigger(
                  overlay = Tooltip.text(attr.descr)
                )(attr.name))
            }: _*
          )),
        <.tbody(
          p.data.toTagMod { dataRecord =>
            val verParam = p.ver match {
              case Some(v) =>
                s"?ver=$v"
              case None =>
                ""
            }
            val url = s"/${p.dbApi.dsName}/records/${dataRecord.attributes.id.get}$verParam"
            <.tr((^.onClick ==> {
              case e if e.shiftKey =>
                Callback {
                  dom.window.location.assign(url)
                }
              case e if e.ctrlKey =>
                Callback {
                  dom.window.open(url)
                }
              case _ =>
                showRecord(dataRecord.attributes)
            }) +:
              p.tab.attrs.map { attr =>
                <.td(^.className := "small-font",
                  MultiStatementComp(vAttrResolver.resolve(attr.ref,
                    dataRecord.attributes),
                    false,
                    //We do not update from the record list
                    newLinkedRecordCallback = targetDs => {
                      val dbApi = p.dbApi.copy(dsName = targetDs)
                      s.showRecord.map{_.getId} match {
                        case Some(id) =>
                          Callback {
                            dom.window.open(s"/${dbApi.dsName}/browser?target-id=$id&target-ds=${p.dbApi.dsName}")
                            dom.window.location.reload()
                          }
                        case None =>
                          println("No id defined for this record.")
                          Callback.empty
                      }
                    }
                  )
                )
              }: _*)
          }
        )
      )
      <.div(
        renderModal(p, s),
        controlComp,
        <.div(^.overflowX.auto,
        tableComp),
        controlComp
      )
    }
  }

  private val component = ScalaComponent
    .builder[Props]
    .initialStateFromProps{ p => if (p.newRecord) State(Some(p.newRecordSupplier())) else State()}
    .renderBackend[Backend]
    .componentDidUpdate {
      lf =>
        if (lf.prevProps.newRecord != lf.currentProps.newRecord)
          lf.modState(_.copy(showRecord = if (lf.currentProps.newRecord) Some(lf.currentProps.newRecordSupplier()) else None))
        else
          Callback.empty
    }
    .build

  def apply(p: Props) = component(p)
}
