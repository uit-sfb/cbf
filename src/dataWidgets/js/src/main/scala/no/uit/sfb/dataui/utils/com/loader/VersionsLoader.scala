package no.uit.sfb.dataui.utils.com.loader

import io.circe.generic.extras.auto._
import io.circe.parser.decode
import io.circe.generic.extras.Configuration
import japgolly.scalajs.react.AsyncCallback
import no.uit.sfb.cbf.shared.jsonapi.MultipleResponse
import no.uit.sfb.cbf.shared.model.v1.versioninfo.VersionInfo
import no.uit.sfb.dataui.utils.com.DatabaseApi

trait VersionsLoader {
  protected def loadVersions(
    dbApi: DatabaseApi
  ): AsyncCallback[Seq[VersionInfo]] = {

    implicit val customConfig: Configuration =
      Configuration.default.withDefaults
    dbApi
      .get(
        dbApi.versions().toString,
        contentType = Some("application/vnd.api+json")
      )
      .map { xhr =>
        val mr =
          decode[MultipleResponse[VersionInfo]](xhr.responseText).toTry.get
        mr.data.map { _.attributes }
      }
  }
}
