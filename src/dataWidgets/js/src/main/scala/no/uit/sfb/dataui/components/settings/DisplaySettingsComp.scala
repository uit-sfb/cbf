package no.uit.sfb.dataui.components.settings

import japgolly.scalajs.react.Ref.Simple
import japgolly.scalajs.react.component.Scala.BackendScope
import japgolly.scalajs.react.vdom.html_<^._
import japgolly.scalajs.react.{Callback, CallbackTo, Ref, ScalaComponent}
import no.uit.sfb.cbf.shared.config._
import no.uit.sfb.dataui.utils.comp.SelectOption
import no.uit.sfb.dataui.utils.comp._
import no.uit.sfb.dataui.utils.core.SwappingList
import no.uit.sfb.facade.bootstrap.Table
import no.uit.sfb.facade.bootstrap.dropdown.{
  Dropdown,
  DropdownButton,
  DropdownItem,
  DropdownMenu,
  DropdownToggle
}
import no.uit.sfb.facade.bootstrap.form._
import no.uit.sfb.facade.bootstrap.grid.{Col, Row}
import no.uit.sfb.facade.bootstrap.inputGroup.InputGroup
import no.uit.sfb.facade.bootstrap.overlay.{OverlayTrigger, Tooltip}
import no.uit.sfb.facade.icon.Glyphicon
import org.scalajs.dom
import org.scalajs.dom.{Node, console, html}
import org.scalajs.dom.raw.HTMLInputElement
import scala.util.Try
import scala.util.Random

object DisplaySettingsComp {

  case class Props(draft: DatasetConfig,
                   updateDraft: DatasetConfig => Callback,
                   noEdit: Boolean)

  case class State(selector: String = "")

  class Backend($ : BackendScope[Props, State]) {
    private val ref: Simple[html.Input] = Ref[html.Input]

    def changeSelector(newSelector: String) =
      $.modState(_.copy(selector = newSelector))

    def render(p: Props, s: State): VdomElement = {
      val header = FormGroup("headerInput")(
        SelectField(
          labelWidth = 2,
          label = "Header",
          value = p.draft.header,
          onChange = newValue => p.updateDraft(p.draft.copy(header = newValue)),
          info = "Attribute to use as record header.",
          options = p.draft.attrs.map { attr =>
            SelectOption(attr.ref, attr.name)
          }
        )()
      )

      def displayTabAttrs(tab: Tab, locked: Boolean = false): VdomElement = {
        val selectId = s"addAttrToTabInput-${tab.name}"
        Table(hover = false)( //For some weird reason, hover = false is needed here
          <.tbody(
            tab.attrs.zipWithIndex
              .flatMap {
                case (ref, idx) =>
                  p.draft.attrs.find(_.ref == ref).map {
                    attr =>
                      <.tr(
                        <.td(attr.name) +:
                          (if (p.noEdit) Seq()
                         else {
                           Seq(
                             <.td(
                               ^.textAlign.center,
                               ButtonField(
                                 variant = "link",
                                 icon = Glyphicon("Up"),
                                 disabled =
                                   if (idx <= 0 || locked) Some("") else None,
                                 onClick =
                                   CallbackTo {
                                     if (tab.name.isEmpty) {
                                       //It is _table
                                       p.draft.copy(
                                         tableAttributes =
                                           SwappingList.up(tab.attrs, idx)
                                       )
                                     } else {
                                       //regular tab
                                       p.draft.copy(tabs = p.draft.tabs.map {
                                         t =>
                                           if (t.name == tab.name) {
                                             tab.copy(
                                               attrs =
                                                 SwappingList.up(tab.attrs, idx)
                                             )
                                           } else
                                             t
                                       })
                                     }
                                   } >>=
                                     p.updateDraft,
                                 info = "Move up"
                               ),
                               ButtonField(
                                 variant = "danger",
                                 icon = Glyphicon("Trash"),
                                 disabled = if (locked) Some("") else None,
                                 onClick =
                                   CallbackTo {
                                     if (tab.name.isEmpty) {
                                       //It is _table
                                       p.draft.copy(
                                         tableAttributes = tab.attrs.filter {
                                           _ != attr.ref
                                         }
                                       )
                                     } else {
                                       p.draft.copy(tabs = p.draft.tabs.map {
                                         t =>
                                           if (t.name == tab.name) {
                                             tab.copy(attrs = tab.attrs.filter {
                                               _ != attr.ref
                                             })
                                           } else
                                             t
                                       })
                                     }
                                   } >>=
                                     p.updateDraft,
                                 info = "Remove attribute"
                               ),
                               ButtonField(
                                 variant = "link",
                                 icon = Glyphicon("Down"),
                                 disabled =
                                   if (idx >= tab.attrs.size - 1 || locked)
                                     Some("")
                                   else None,
                                 onClick =
                                   CallbackTo {
                                     if (tab.name.isEmpty) {
                                       //It is _table
                                       p.draft.copy(
                                         tableAttributes =
                                           SwappingList.down(tab.attrs, idx)
                                       )
                                     } else {
                                       p.draft.copy(tabs = p.draft.tabs.map {
                                         t =>
                                           if (t.name == tab.name) {
                                             tab.copy(
                                               attrs = SwappingList
                                                 .down(tab.attrs, idx)
                                             )
                                           } else
                                             t
                                       })
                                     }
                                   } >>=
                                     p.updateDraft,
                                 info = "Move down"
                               )
                             )
                           )
                         }): _*
                      )
                  }
              } ++ (if (p.noEdit) Seq()
                    else
                      Seq(
                        <.tr(
                          <.td(
                            FormGroup(selectId)(
                              //Do not convert to SelectField
                              FormControl.uncontrolled(
                                ref,
                                as = "select",
                                defaultValue = ""
                              )(
                                <.option(
                                  ^.value := "",
                                  ^.disabled := true,
                                  "- Select -"
                                ) +:
                                  p.draft.attrs.sortBy(_.name).collect {
                                  case attr if !tab.attrs.contains(attr.ref) =>
                                    <.option(^.value := attr.ref, attr.name)
                                }: _*
                              )
                            )
                          ),
                          <.td(
                            ^.textAlign.center,
                            ButtonField(
                              icon = Glyphicon("Add"),
                              variant = "primary",
                              disabled = if (locked) Some("") else None,
                              onClick = CallbackTo { //Note we NEED to build the new draft within a callback otherwise the logic is executed at render time!
                                val newAttrRef = dom.document
                                  .getElementById(selectId)
                                  .asInstanceOf[HTMLInputElement]
                                  .value
                                if (newAttrRef.nonEmpty) {
                                  if (tab.name.isEmpty) {
                                    //It is _table
                                    p.draft.copy(
                                      tableAttributes = p.draft.tableAttributes :+ newAttrRef
                                    )
                                  } else {
                                    p.draft.copy(tabs = p.draft.tabs.map { t =>
                                      if (t.name == tab.name && !tab.attrs
                                            .contains(newAttrRef)) {
                                        tab
                                          .copy(attrs = tab.attrs :+ newAttrRef)
                                      } else
                                        t
                                    })
                                  }
                                } else
                                  p.draft
                              } >>= p.updateDraft,
                              info = "Add attribute"
                            )
                          )
                        )
                      )): _*
          )
        )
      }

      val browserDefaultAttrs = Row()(
        Col(md = 2)(
          OverlayTrigger(
            overlay = Tooltip
              .text("Attributes displayed by default in the record browser.")
          )(
            "Browser attributes",
            <.span(^.paddingLeft := "5px"),
            Glyphicon("Help")
          )
        ),
        Col()(displayTabAttrs(Tab(name = "", attrs = p.draft.tableAttributes)))
      )

      val tabs = {
        Row()(
          Col(md = 2)(
            OverlayTrigger(
              overlay = Tooltip
                .text(
                  "Tabs logically group related attributes together." +
                    "Tabs may be grouped into categories in which case the first tab of a category determine its placement relative to the other tabs/categories." +
                    "Click on a line to edit."
                )
            )("Tabs", <.span(^.paddingLeft := "5px"), Glyphicon("Help"))
          ),
          Col()(
            Table(hover = true)(<.tbody(p.draft.tabs.zipWithIndex.map {
              case (tab, idx) =>
                val locked = false //tab.name == "Record metadata"
                val selectId = s"editTabNameInput-${tab.name}"
                <.tr(
                  Seq(^.onClick --> changeSelector(tab.name)) ++
                    ((if (p.noEdit) Seq()
                      else
                        Seq(
                          <.td(
                            <.div(
                              ^.textAlign.center,
                              ButtonField(
                                variant = "link",
                                icon = Glyphicon("Up"),
                                disabled = if (idx == 0) Some("") else None,
                                onClick =
                                  CallbackTo {
                                    p.draft
                                      .copy(
                                        tabs =
                                          SwappingList.up(p.draft.tabs, idx)
                                      )
                                  } >>=
                                    p.updateDraft,
                                info = "Move up"
                              )
                            ),
                            <.div(
                              ^.textAlign.center,
                              ButtonField(
                                variant = "danger",
                                icon = Glyphicon("Trash"),
                                disabled = if (locked) Some("") else None,
                                onClick =
                                  CallbackTo {
                                    p.draft.copy(tabs = p.draft.tabs.filter {
                                      t =>
                                        t.name != tab.name
                                    })
                                  } >>=
                                    p.updateDraft,
                                info = "Remove tab"
                              )
                            ),
                            <.div(
                              ^.textAlign.center,
                              ButtonField(
                                variant = "link",
                                icon = Glyphicon("Down"),
                                disabled =
                                  if (idx >= p.draft.tabs.size - 1) Some("")
                                  else None,
                                onClick =
                                  CallbackTo {
                                    p.draft
                                      .copy(
                                        tabs =
                                          SwappingList.down(p.draft.tabs, idx)
                                      )
                                  } >>=
                                    p.updateDraft,
                                info = "Move down"
                              )
                            )
                          )
                        )) ++
                      (if (tab.name == s.selector) {
                         Seq(
                           <.td(
                             //Selection of other tabs
                             FormGroup(selectId)(
                               TextField(
                                 value = tab.category,
                                 placeholder = "Category",
                                 disabled =
                                   if (locked || p.noEdit) Some("") else None,
                                 onChange = value =>
                                   CallbackTo {
                                     p.draft.copy(tabs = p.draft.tabs.map { t =>
                                       if (t.name == tab.name)
                                         tab.copy(category = value)
                                       else
                                         t
                                     })
                                   } >>=
                                     p.updateDraft
                               )(),
                               TextField(
                                 value = tab.name,
                                 placeholder = "Tab name",
                                 disabled =
                                   if (locked || p.noEdit) Some("") else None,
                                 onChange = value =>
                                   CallbackTo {
                                     p.draft.copy(tabs = p.draft.tabs.map { t =>
                                       if (t.name == tab.name)
                                         tab.copy(name = value)
                                       else
                                         t
                                     })
                                   } >>=
                                     p.updateDraft
                               )()
                             )
                           ),
                           <.td(displayTabAttrs(tab, locked))
                         )
                       } else {
                         Seq(
                           <.td(
                             ^.colSpan := 2,
                             s"${if (tab.category.nonEmpty) s"${tab.category} : "
                             else ""}${tab.name}"
                           )
                         )
                       })): _*
                )
            }: _*)),
            if (p.noEdit) <.div()
            else
              FormGroup("newTabNameInput")(
                InputGroup()(
                  FormControl.uncontrolled(ref, placeholder = "Tab name")(),
                  ButtonField(
                    variant = "outline-primary",
                    icon = "Add",
                    onClick =
                      CallbackTo {
                        val value = {
                          val elem = dom.document
                            .getElementById("newTabNameInput")
                            .asInstanceOf[HTMLInputElement]
                          val tmp = elem.value
                          //We rest the input
                          elem.value = elem.defaultValue
                          tmp
                        }
                        val exists = p.draft.tabs.exists(_.name == value)
                        if (value.nonEmpty && !exists)
                          p.draft.copy(
                            tabs = p.draft.tabs :+ Tab(
                              name = value,
                              attrs = Seq()
                            )
                          )
                        else {
                          if (exists)
                            dom.window.alert(s"Item '$value' already exists.")
                          p.draft
                        }
                      } >>=
                        p.updateDraft,
                    info = "New tab"
                  )
                )
              ),
          )
        )
      }
      <.div(header, <.br, browserDefaultAttrs, <.br, tabs)
    }
  }

  private lazy val component = ScalaComponent
    .builder[Props]
    .initialState(State())
    .renderBackend[Backend]
    .build

  def apply(draft: DatasetConfig,
            updateDraft: DatasetConfig => Callback,
            noEdit: Boolean) =
    component(Props(draft, updateDraft, noEdit))
}
