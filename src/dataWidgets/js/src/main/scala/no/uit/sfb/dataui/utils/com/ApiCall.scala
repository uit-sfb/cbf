package no.uit.sfb.dataui.utils.com

import japgolly.scalajs.react.Callback
import japgolly.scalajs.react.extra.Ajax
import japgolly.scalajs.react.extra.Ajax.{Step2, isStatusSuccessful}
import no.uit.sfb.cbf.shared.jsonapi.{MultipleResponse, SingleResponse}

import scala.concurrent.duration.Duration
import scala.scalajs.js
import scala.scalajs.js.URIUtils
import io.circe.generic.extras.auto._
import io.circe.parser.decode
import io.circe.generic.extras.Configuration
import io.lemonlabs.uri.RelativeUrl
import no.uit.sfb.cbf.shared.errors.ErrorLike

trait ApiCall {
  def apiEndpoint: String
  def accessToken: Option[String]

  def proxyUrl(path: String,
               timeout: Option[Double] = None,
               accessTokenParameter: Boolean = true): String = {
    val rUrl = RelativeUrl.parse(path)
    val tmp = accessToken match {
      case Some(ac) if accessTokenParameter =>
        rUrl.addParam("access-token", ac)
      case _ =>
        rUrl
    }
    val tmp2 = timeout match {
      case Some(to) => tmp.addParam("timeout", to / 1000)
      case None     => tmp
    }
    s"$apiEndpoint$tmp2"
  }

  /**
    * ATTENTION: Do NOT use >> as it runs AT THE SAME TIME as the previous callback (therefore ignoring errors)
    *  The successCallback and errorCallback, or, if desired the following pattern:
    *  run(...).asAsyncCallback.void //.void only if we do not care about xhr
    *    .map { _ => //Success
          ...
        }
        .handleError{ t => //Failure
          AsyncCallback.pure(...)
        }
        .map { _ => //Always run this
          ...
        }
    */
  protected def call(
    method: String,
    path: String,
    body: js.Any = js.undefined,
    contentType: Option[String] = None,
    timeout: Option[Double] = None,
    validation: (Callback, Throwable => Callback) => Step2 => Step2,
    successCallback: Callback,
    errorCallback: Throwable => Callback
  ): Step2 = {
    val t1 = Ajax(method, proxyUrl(path, timeout, false))
      .setRequestHeader("X-Requested-With", "XMLHttpRequest")
    val t2 = accessToken match {
      case Some(at) => t1.setRequestHeader("Authorization", s"Bearer $at")
      case _        => t1
    }
    val t3 = contentType match {
      case Some(ct) =>
        t2.setRequestHeader("Content-Type", ct)
      case None =>
        t2
    }
    val t4 = t3.send(body)
    val t5 = timeout match {
      case Some(t) =>
        t4.withTimeout(t, xhr => {
          val e = new Exception(s"AJAX query timed out")
          errorCallback(e) >> Callback.throwException(e)
        })
      case None =>
        t4
    }
    validation(
      successCallback,
      e =>
        Callback(println(e)) >> errorCallback(e) >> Callback.throwException(e)
    )(t5)
  }

  def get(path: String,
          contentType: Option[String] = None,
          timeout: Option[Duration] = None,
          validation: (Callback, Throwable => Callback) => Step2 => Step2 =
            withUnauthorizeFallback,
          successCallback: Callback = Callback.empty,
          errorCallback: Throwable => Callback = _ => Callback.empty) =
    call(
      "GET",
      path,
      contentType = contentType,
      timeout = timeout.map { _.toMillis.toDouble },
      validation = validation,
      successCallback = successCallback,
      errorCallback = errorCallback
    ).asAsyncCallback

  def patch(
    path: String,
    contentType: Option[String] = None, //MUST be set to "application/json" if sending json
    timeout: Option[Duration] = None,
    validation: (Callback, Throwable => Callback) => Step2 => Step2 =
      withUnauthorizeFallback,
    successCallback: Callback = Callback.empty,
    errorCallback: Throwable => Callback = _ => Callback.empty
  )(body: js.Any) =
    call(
      "PATCH",
      path,
      body,
      contentType = contentType,
      timeout = timeout.map { _.toMillis.toDouble },
      validation,
      successCallback = successCallback,
      errorCallback = errorCallback
    ).asAsyncCallback

  def post(
    path: String,
    contentType: Option[String] = None, //MUST be set to "application/json" if sending json
    timeout: Option[Duration] = None,
    validation: (Callback, Throwable => Callback) => Step2 => Step2 =
      withUnauthorizeFallback,
    successCallback: Callback = Callback.empty,
    errorCallback: Throwable => Callback = _ => Callback.empty
  )(body: js.Any) =
    call(
      "POST",
      path,
      body,
      contentType = contentType,
      timeout = timeout.map { _.toMillis.toDouble },
      validation,
      successCallback = successCallback,
      errorCallback = errorCallback
    ).asAsyncCallback

  def delete(path: String,
             contentType: Option[String] = None,
             timeout: Option[Duration] = None,
             validation: (Callback, Throwable => Callback) => Step2 => Step2 =
               withUnauthorizeFallback,
             successCallback: Callback = Callback.empty,
             errorCallback: Throwable => Callback = _ => Callback.empty) =
    call(
      "DELETE",
      path,
      contentType = contentType,
      timeout = timeout.map { _.toMillis.toDouble },
      validation = validation,
      successCallback = successCallback,
      errorCallback = errorCallback
    ).asAsyncCallback

  def send(
    path: String,
    as: String,
    contentType: Option[String] = None, //MUST be set to "application/json" if sending json
    timeout: Option[Duration] = None,
    validation: (Callback, Throwable => Callback) => Step2 => Step2 =
      withUnauthorizeFallback,
    successCallback: Callback = Callback.empty,
    errorCallback: Throwable => Callback = _ => Callback.empty
  )(body: js.Any) =
    call(
      as,
      path,
      body,
      contentType = contentType,
      timeout = timeout.map { _.toMillis.toDouble },
      validation,
      successCallback = successCallback,
      errorCallback = errorCallback
    ).asAsyncCallback

  protected def withUnauthorizeFallback(
    successCallback: Callback = Callback.empty,
    errorCallback: Throwable => Callback = Callback.throwException
  )(s: Step2): Step2 = {
    implicit val customConfig: Configuration =
      Configuration.default.withDefaults
    s.onComplete { xhr =>
      val status = xhr.status
      if (isStatusSuccessful(status))
        successCallback //Note: we do not check if the data contains JsonAPI errors, but if needed be, it should be done here.
      else {
        errorCallback(
          decode[SingleResponse[Unit]](xhr.responseText).toTry.toOption
            .orElse(
              decode[MultipleResponse[Unit]](xhr.responseText).toTry.toOption
            )
            .map {
              _.errors
            } match {
            case Some(err :: _) => //We only take the first error
              new ErrorLike {
                def statusCode: Int = err.status.toInt
                def title: String = err.title
                def details: String = err.detail
              }
            case _ =>
              new ErrorLike {
                def statusCode: Int = xhr.status
                def title: String =
                  if (xhr.statusText.nonEmpty) xhr.statusText
                  else "Oops, something went wrong."
                def details: String =
                  xhr.responseURL.toOption
                    .map { url =>
                      val decodedUrl = URIUtils.decodeURIComponent(url)
                      if (decodedUrl.nonEmpty)
                        s"While accessing '$decodedUrl''"
                      else
                        "It may help to reload the page."
                    }
                    .getOrElse("")
              }
          }
        )
      }
    }
  }
}
