package no.uit.sfb.dataui.components.upload

import io.circe.generic.extras.Configuration
import japgolly.scalajs.react.component.Scala.BackendScope
import japgolly.scalajs.react.vdom.html_<^._
import japgolly.scalajs.react.{AsyncCallback, Callback, ScalaComponent, ~=>}
import no.uit.sfb.cbf.shared.model.v1.task.UpdateDsTaskStatus
import no.uit.sfb.facade.bootstrap.buttons.Button
import no.uit.sfb.facade.bootstrap.form.{FormFile, FormGroup}
import org.scalajs.dom
import org.scalajs.dom.raw.HTMLInputElement

import scala.concurrent.duration.DurationInt
import scala.scalajs.js
import io.circe.parser.decode
import io.circe.generic.extras.auto._
import no.uit.sfb.dataui.utils.com.DatabaseApi
import no.uit.sfb.facade.bootstrap.ProgressBar
import no.uit.sfb.dataui.utils.comp.Spin

import scala.scalajs.js.timers.SetIntervalHandle

object UploadComp {

  implicit val customConfig: Configuration =
    Configuration.default.withDefaults

  case class Props(dbApi: DatabaseApi,
                   ver: String,
                   searchQuery: String,
                   releaseFromExternal: Boolean,
                   error: Throwable ~=> Callback)

  case class State(status: Option[UpdateDsTaskStatus] = None,
                   timer: Option[SetIntervalHandle] = None)

  class Backend($ : BackendScope[Props, State]) {

    protected def uploadFile(p: Props)(patch: Boolean = false): Callback = {
      p.dbApi
        .post(
          p.dbApi
            .updateDataset(p.ver, patch)
            .toString,
          errorCallback = p.error
        ) {
          //Note: It was clumsy to work with FormData (new FormData(formElement) did not work)
          //In addition it created a multipart which was not really required since we only wanted to send one file
          //Instead we simply return the file alone.
          val elem = dom.document
            .getElementById("inputFile")
            .asInstanceOf[HTMLInputElement]
          //Alternatively:
          /*val elem = e.target
            .asInstanceOf[HTMLFormElement]
            .elements(0)
            .asInstanceOf[HTMLInputElement]*/
          elem.files(0)
        } >> {
        pullState(p)
      }.flatMap { res =>
        $.modStateAsync(_.copy(status = res))
      }
    }.toCallback

    def render(p: Props, s: State): VdomElement = {
      s.status match {
        case Some(status) =>
          val stateName = status.state
          val locked = Seq("Starting", "Running").contains(stateName)
          val st = stateName match {
            case "Starting" =>
              <.div(<.h4("Starting..."), Spin())
            case "Running" =>
              <.div(
                <.h4(
                  s"${status.method.capitalize}ing ${status.nbRecords} records..."
                ),
                ProgressBar(
                  status.progress,
                  animated = true,
                  label = Some(s"${status.progress}%")
                )()
              )
            case "Success" =>
              <.div(
                <.h4("Last upload succeeded"),
                <.div(
                  if (status.method.nonEmpty)
                    s"Successfully ${status.method}ed ${status.nbRecords} records."
                  else
                    ""
                ),
                ProgressBar(
                  status.progress,
                  variant = Some("success"),
                  label = Some(s"${status.progress}%")
                )()
              )
            case "Failed" =>
              <.div(
                <.h4("Last upload failed"),
                <.div(
                  s"${status.errors} errors",
                  if (status.nbRecords > 0)
                    s" (out of ${status.nbRecords} records)"
                  else
                    "",
                  <.br,
                  "The failed records need to be re-submitted using the procedure described below (the records that did not trigger an error are now uploaded to the database and do not need to be re-submitted)."
                ),
                status.ratioFailed match {
                  case Some(ratioFailed) =>
                    ProgressBar(
                      ratioFailed,
                      variant = Some("danger"),
                      label = Some(s"$ratioFailed%")
                    )()
                  case None =>
                    //After server restart we do not have the record count, so we cannot compute the ratio
                    ProgressBar(
                      100,
                      variant = Some("danger"),
                      label = Some(s"${status.errors} errors")
                    )()
                }
              )
            case _ => <.div() //Should not happen
          }
          val edit = {
            if (Seq("Starting", "Running").contains(status.state)) {
              <.div(s"Please wait for the current task to complete.")
            } else {
              <.div(
                UploadFormComp(
                  p.dbApi,
                  p.ver,
                  p.searchQuery,
                  locked,
                  uploadFile = uploadFile(p),
                  state = s.status.map { _.state }.getOrElse("")
                )
              )
            }
          }
          <.div(st, <.hr, <.h4("New edit"), edit)
        case None =>
          Spin()
      }
    }
  }

  protected def pullState(
    p: Props
  ): AsyncCallback[Option[UpdateDsTaskStatus]] = {
    p.dbApi
      .get(p.dbApi.updateDatasetStatus(p.ver).toString)
      .map { xhr =>
        decode[UpdateDsTaskStatus](xhr.responseText).toOption
      }
  }

  private lazy val component = ScalaComponent
    .builder[Props]
    .initialState(State())
    .renderBackend[Backend]
    .componentDidMount { lf =>
      def updateState(p: Props): AsyncCallback[Unit] = {
        pullState(p).flatMap { res =>
          lf.modStateAsync(_.copy(status = res))
        }
      }

      if (lf.state.timer.nonEmpty)
        Callback.empty
      else {
        //We fetch the data straight away
        updateState(lf.props).toCallback >>
          lf.toModStateWithPropsFn
            .modState { //and start the timer which unfortunately does not take a Callback, so we need to use runNow()
              case (s, p) =>
                s.copy(
                  timer =
                    Some(js.timers.setInterval(5.seconds.toMillis.toDouble) {
                      updateState(p)
                        .runNow()
                    })
                )
            }
      }
    }
    .componentWillUnmount(
      lf =>
        Callback(lf.state.timer.map { timer =>
          js.timers.clearInterval(timer)
        })
    )
    .build

  def apply(dbApi: DatabaseApi,
            ver: String,
            searchQuery: String,
            releaseFromExternal: Boolean,
            error: Throwable ~=> Callback) =
    component(Props(dbApi, ver, searchQuery, releaseFromExternal, error))
}
