package no.uit.sfb.dataui.components.settings

import io.circe.parser
import japgolly.scalajs.react.Ref.Simple
import japgolly.scalajs.react.component.Scala.BackendScope
import japgolly.scalajs.react.vdom.html_<^._
import japgolly.scalajs.react.{Callback, CallbackTo, Ref, ScalaComponent}
import no.uit.sfb.cbf.shared.config.{CustomResolver, DatasetConfig}
import no.uit.sfb.cbf.shared.model.v1.nativ.CompactIRI
import no.uit.sfb.dataui.utils.com.DatabaseApi
import no.uit.sfb.dataui.utils.comp._
import no.uit.sfb.facade.bootstrap.Table
import no.uit.sfb.facade.bootstrap.form._
import no.uit.sfb.facade.bootstrap.inputGroup.InputGroup
import no.uit.sfb.facade.bootstrap.overlay.{OverlayTrigger, Tooltip}
import no.uit.sfb.facade.icon.Glyphicon
import org.scalajs.dom
import org.scalajs.dom.html
import org.scalajs.dom.raw.HTMLInputElement

object ResolversSettingsComp {

  case class Props(draft: DatasetConfig,
                   updateDraft: DatasetConfig => Callback,
                   noEdit: Boolean,
                   dbApi: DatabaseApi)

  case class State(selector: String = "")

  class Backend($ : BackendScope[Props, State]) {
    private val ref: Simple[html.Input] = Ref[html.Input]

    def changeSelector(newSelector: String) =
      $.modState(_.copy(selector = newSelector))

    def renderSelectedItem(p: Props, resolv: CustomResolver) = {
      val selectId2 = s"resolver-${resolv.prefix}"
      <.tr(
        <.td(
          if (p.noEdit) <.div()
          else
            ButtonField(
              variant = "danger",
              icon = Glyphicon("Trash"),
              onClick =
                CallbackTo {
                  p.draft.copy(
                    customResolvers = p.draft.customResolvers
                      .filter(_.prefix != resolv.prefix)
                  )
                } >>=
                  p.updateDraft,
              info = "Remove resolver"
            ),
          s" ${resolv.prefix}"
        ) +: Seq(
          <.td(
            FormGroup(selectId2 + "regexp")(
              //FormLabel()("Regexp"),
              TextField(
                disabled = if (p.noEdit) Some("") else None,
                label = "Regular expression",
                value = resolv.regexp,
                info = "A regular expression validating the identifier.",
                onChange = value =>
                  CallbackTo {
                    p.draft.copy(customResolvers = p.draft.customResolvers.map {
                      t =>
                        if (t.prefix != resolv.prefix)
                          t
                        else
                          resolv.copy(regexp = value)
                    })
                  } >>=
                    p.updateDraft
              )()
            )
          ),
          <.td(
            LabelField(
              "Fields",
              info =
                "'{id}', '{ver}', '{dsName}', '{api}', '{<capture group number>}' are supported tokens."
            )(),
            Table()(
              <.tbody(
                resolv.kvp.map {
                  case (k, v) =>
                    val selectId = s"resolver-$k-$v"
                    <.tr(
                      Seq(
                        <.td(
                          FormGroup(selectId)(
                            TextField(
                              disabled = if (p.noEdit) Some("") else None,
                              label = k,
                              value = v,
                              onChange = value =>
                                CallbackTo {
                                  p.draft.copy(
                                    customResolvers =
                                      p.draft.customResolvers.map { t =>
                                        if (t.prefix != resolv.prefix)
                                          t
                                        else
                                          resolv.copy(
                                            kvp = resolv.kvp ++ Map(k -> value)
                                          )
                                      }
                                  )
                                } >>=
                                  p.updateDraft
                            )()
                          )
                        )
                      ) ++ (if (p.noEdit) Seq()
                            else
                              Seq(
                                <.td(
                                  ^.textAlign.center,
                                  ButtonField(
                                    variant = "danger",
                                    icon = Glyphicon("Trash"),
                                    onClick =
                                      CallbackTo {
                                        p.draft.copy(
                                          customResolvers =
                                            p.draft.customResolvers.map(
                                              x =>
                                                if (x.prefix == resolv.prefix)
                                                  x.copy(
                                                    kvp =
                                                      x.kvp.filter(_._1 != k)
                                                  )
                                                else x
                                            )
                                        )
                                      } >>=
                                        p.updateDraft,
                                    info = "Remove resolver"
                                  )
                                )
                              )): _*
                    )
                }.toSeq ++ (if (p.noEdit) Seq()
                            else
                              Seq(
                                <.tr(
                                  <.td(
                                    FormGroup(selectId2)(
                                      FormControl.uncontrolled(
                                        ref,
                                        placeholder = "Field name"
                                      )()
                                    )
                                  ),
                                  <.td(
                                    ^.textAlign.center,
                                    if (p.noEdit) <.div()
                                    else
                                      ButtonField(
                                        variant = "primary",
                                        icon = Glyphicon("Add"),
                                        onClick = CallbackTo { //Note we NEED to build the new draft within a callback otherwise the logic is executed at render time!
                                          val newFieldKey = dom.document
                                            .getElementById(selectId2)
                                            .asInstanceOf[HTMLInputElement]
                                            .value
                                          if (newFieldKey.nonEmpty)
                                            p.draft.copy(
                                              customResolvers =
                                                p.draft.customResolvers.map {
                                                  r =>
                                                    if (r.prefix == resolv.prefix && !r.kvp.keySet
                                                          .contains(
                                                            newFieldKey
                                                          ))
                                                      r.copy(
                                                        kvp = r.kvp + (newFieldKey -> "")
                                                      )
                                                    else
                                                      r
                                                }
                                            )
                                          else
                                            p.draft
                                        } >>= p.updateDraft,
                                        info = "Add field"
                                      )
                                  )
                                )
                              )): _*
              )
            )
          )
        ): _*
      )
    }

    def render(p: Props, s: State): VdomElement = {
      <.div(
        Table(hover = true)(
          <.thead(
            <.tr(
              <.th(
                OverlayTrigger(
                  overlay = Tooltip
                    .text("Click on a line to edit.")
                )("Prefix", <.span(^.paddingLeft := "5px"), Glyphicon("Help"))
              ),
              <.th("Regular expression")
            )
          ),
          <.tbody(p.draft.customResolvers.map {
            case resolv if resolv.prefix == s.selector =>
              renderSelectedItem(p, resolv)
            case resolv =>
              <.tr(
                ^.onClick --> changeSelector(resolv.prefix),
                <.td(resolv.prefix),
                <.td(^.colSpan := 2, resolv.regexp)
              )
          }: _*)
        ),
        if (p.noEdit) <.div()
        else
          FormGroup("newResolverNameInput")(
            InputGroup()(
              FormControl.uncontrolled(ref, placeholder = "Resolver prefix")(),
              ButtonField(
                variant = "outline-primary",
                icon = "Add",
                onClick = CallbackTo {
                  {
                    val elem = dom.document
                      .getElementById("newResolverNameInput")
                      .asInstanceOf[HTMLInputElement]
                    val tmp = elem.value
                    //We rest the input
                    elem.value = elem.defaultValue
                    tmp
                  }
                }.async
                  .flatMap { value =>
                    val exists =
                      p.draft.customResolvers.exists(_.prefix == value)
                    if (value.nonEmpty && !exists) {
                      p.dbApi.get(p.dbApi.resolver(value).toString).map {
                        resp =>
                          val resolverMap: Map[String, String] = parser
                            .parse(resp.responseText)
                            .map { _.as[Map[String, String]] }
                            .toOption
                            .flatMap { _.toOption }
                            .getOrElse(Map())
                          p.draft.copy(
                            customResolvers = p.draft.customResolvers :+ CustomResolver(
                              value,
                              if (resolverMap.get("pattern").exists(_.nonEmpty))
                                resolverMap("pattern")
                              else CompactIRI.idReg,
                              if (resolverMap.get("_iri").exists(_.nonEmpty))
                                resolverMap.filter {
                                  case (k, _) => k == "_iri"
                                } else Map(),
                              resolverMap.get("baseUri")
                            )
                          )
                      }
                    } else {
                      CallbackTo {
                        if (exists)
                          dom.window.alert(s"Item '$value' already exists.")
                        p.draft
                      }.async
                    }
                  }
                  .flatMap {
                    p.updateDraft(_).async
                  }
                  .toCallback,
                info = "New resolver"
              )
            )
          )
      )
    }
  }

  private lazy val component = ScalaComponent
    .builder[Props]
    .initialState(State())
    .renderBackend[Backend]
    .build

  def apply(draft: DatasetConfig,
            updateDraft: DatasetConfig => Callback,
            noEdit: Boolean,
            dbApi: DatabaseApi) =
    component(Props(draft, updateDraft, noEdit, dbApi))
}
