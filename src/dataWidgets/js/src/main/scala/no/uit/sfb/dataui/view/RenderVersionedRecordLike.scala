package no.uit.sfb.dataui.view

import japgolly.scalajs.react.Callback
import japgolly.scalajs.react.vdom.html_<^._
import no.uit.sfb.cbf.shared.config.DatasetConfig
import no.uit.sfb.cbf.shared.model.v1.nativ.Record
import no.uit.sfb.cbf.shared.model.v1.versioninfo.VersionInfo
import no.uit.sfb.cbf.shared.utils.GenericVersioning
import no.uit.sfb.dataui.components.record.RecordComp
import no.uit.sfb.dataui.utils.VirtualAttributeResolver
import no.uit.sfb.dataui.utils.com.DatabaseApi
import no.uit.sfb.facade.bootstrap.alert.Alert
import no.uit.sfb.facade.bootstrap.Badge
import no.uit.sfb.facade.bootstrap.buttons.Button
import org.scalajs.dom

trait RenderVersionedRecordLike {
  protected def renderVersionedRecord(record: Record,
                                      dbApi: DatabaseApi,
                                      config: DatasetConfig,
                                      recordVersion: Option[VersionInfo],
                                      latestDbVersion: Option[String],
                                      editMode: Boolean,
                                      editedRecord: Option[Record] = None,
                                      editCallback: Record => Callback = _ =>
                                        Callback.empty): VdomElement = {
    val latestVersion = latestDbVersion.getOrElse("0.0") //Just to get the smallest version possible
    val (banner, badge) =
      (latestVersion, recordVersion) match {
        case (latest, Some(recVer)) =>
          val (alert, level) =
            if (recVer.deprecated)
              Alert(variant = "danger")(
                s"You are watching a deprecated version of this record."
              ) -> "danger"
            else
              (GenericVersioning.compare(recVer._id, latest) match {
                case x if x < 0 =>
                  Alert(variant = "warning")(
                    s"You are watching an older version of this record (it may have been updated or deleted in more recent releases)."
                  ) -> "secondary"
                case x if x > 0 =>
                  Alert(variant = "warning")(
                    s"You are watching a draft version of this record."
                  ) -> "warning"
                case _ =>
                  <.div() -> "primary"
              })
          alert -> Badge(variant = level)(recVer.displayStr)
        case (_, None) =>
          <.div() -> Badge(variant = "secondary")("Unknown")
      }
    val vAttrResolver = new VirtualAttributeResolver(config)
    val header = vAttrResolver
      .resolve(config.header, record)
      .headOption
      .map {
        _.simpleValue
      }
      .getOrElse("-")
    val clickableBadge = Button(
      size = "sm",
      variant = "link",
      onClick = _ =>
        Callback {
          dom.window.location.assign(s"/${dbApi.dsName}/browser${recordVersion
            .map { v =>
              s"?ver=$v"
            }
            .getOrElse("")}")
      }
    )(badge)
    <.div(
      banner,
      <.span(<.h2(^.display.inline, header), "  ", clickableBadge),
      RecordComp(
        editedRecord.getOrElse(record),
        dbApi,
        config,
        editMode,
        editCallback
      )
    )
  }
}
