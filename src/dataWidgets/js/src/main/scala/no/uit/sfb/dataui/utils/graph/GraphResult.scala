package no.uit.sfb.dataui.utils.graph

import no.uit.sfb.dataui.utils.com.{ADT, ADTInt, ADTString}

case class GraphResult(graph: Seq[Map[String, ADT]]) {
  //Converts Json data into Seq[(Any, Any)]
  //filters out point (x, y) if fx(x) or fy(y) returns None
  def data(key: String, fx: ADT => Option[ADT] = x => {
    if (x.`null`)
      Some(ADTString("Unknown"))
    else
      Some(x)
  }, fy: ADT => Option[ADT] = y => {
    if (y.numeric)
      Some(y)
    else if (y.isCollection) {
      Some(y)
    } else
      None
  }): Seq[(Any, Any)] = {
    graph.flatMap { m =>
      fx(m("x")) -> fy(m.getOrElse(key, ADTInt(0))) match {
        case (Some(x), Some(y)) =>
          Some(x.v -> y.v)
        case _ =>
          None
      }
    }
  }

  lazy val allKeys: Seq[String] = {
    graph.flatten.toMap.keys.tail.toSeq //We remove x
  }
}
