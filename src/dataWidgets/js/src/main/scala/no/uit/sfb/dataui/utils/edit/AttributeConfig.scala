package no.uit.sfb.dataui.utils.edit

import japgolly.scalajs.react.Callback
import no.uit.sfb.cbf.shared.config.DatasetConfig
import no.uit.sfb.cbf.shared.model.v1.nativ.{Record, Statement, ValueRef}

case class AttributeConfig(
                            ref: String,
                            attrName: String,
                            locked: Boolean,
                            `type`: DisplayableTypeLike,
                            suffix: String,
                            template: String,
                            dependencies: Seq[(Seq[Statement], AttributeConfig, (Int, ValueRef) => Callback)], //Dependencies come from reference in templates Int is idx (NOT idxSubValue)
) {
  lazy val isTemplate = template.nonEmpty
}

object AttributeConfig {
  def apply(
             ref: String,
             config: DatasetConfig,
             rec: Record,
             editCallback: (String, Int, Int, ValueRef) => Callback
  ): Option[AttributeConfig] = {
    config.attr(ref).map{
      attr =>
        val t = DisplayableTypeLike(ref, config).get //Should be ok since config.attr(attr) was non-empty
        AttributeConfig(
          ref,
          attr.name,
          attr.isFullyControlled,
          t,
          attr.suffix,
          if (attr.hasActiveTemplate)
          attr.template.getOrElse("")
          else
          "",
          attr.dependencies.flatMap { ref =>
            AttributeConfig(
              ref,
              config,
              rec,
              editCallback
            ) map {
              ac =>
                (rec.stm(ref),
                  ac,
                  (idx, vr) => editCallback(ref, idx, 0, vr) //We know dependencies are always level 0
                )
            }
          }
        )
    }
  }
}
