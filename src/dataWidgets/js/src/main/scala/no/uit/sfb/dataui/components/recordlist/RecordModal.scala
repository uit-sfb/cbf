package no.uit.sfb.dataui.components.recordlist

import io.circe.generic.auto._
import io.circe.parser.decode
import japgolly.scalajs.react.{Callback, CallbackTo, ScalaComponent}
import japgolly.scalajs.react.component.Scala.BackendScope
import japgolly.scalajs.react.vdom.html_<^._
import no.uit.sfb.cbf.shared.auth.UserInfo
import no.uit.sfb.cbf.shared.config.DatasetConfig
import no.uit.sfb.cbf.shared.jsonapi.SingleResponse
import no.uit.sfb.cbf.shared.model.v1.nativ.Record
import no.uit.sfb.cbf.shared.model.v1.versioninfo.VersionInfo
import no.uit.sfb.dataui.components.record.RecordMenuLike
import no.uit.sfb.dataui.utils.com.{DatabaseApi, ResourceUrl}
import no.uit.sfb.dataui.utils.edit.DbDirectLink
import no.uit.sfb.dataui.view.{RenderVersionedRecordLike, ViewBackendLike}
import no.uit.sfb.facade.bootstrap.buttons.Button
import no.uit.sfb.facade.bootstrap.modal._
import org.scalajs.dom
import no.uit.sfb.facade.bootstrap.overlay.{OverlayTrigger, Tooltip}

object RecordModal {

  case class Props(record: Record,
                   datasetConfig: DatasetConfig,
                   dbApi: DatabaseApi,
                   resourceUrl: ResourceUrl,
                   ver: Option[String],
                   versions: Seq[VersionInfo],
                   hide: Callback,
                   userInfo: UserInfo,
                   newRecord: Boolean,
                   targetId: String,
                   targetDs: String) {
    lazy val oVersion = versions.find(_._id == ver.getOrElse(""))
    lazy val isRelease = oVersion match {
      case Some(v) if !v.isDraft => true
      case _                     => false
    }
  }

  case class State(editedRecord: Option[Record] = None) {
    val editMode = editedRecord.nonEmpty
    lazy val hasErrors = editedRecord.exists {
      _.attributes.exists { case (_, stm) => stm.hasError }
    }
  }

  class Backend($ : BackendScope[Props, State])
      extends ViewBackendLike[Props, State]
      with RenderVersionedRecordLike
      with RecordMenuLike {
    protected val toEditMode =
      $.toModStateWithPropsFn.modState {
        case (s, p) =>
          s.copy(editedRecord = Some(p.record.trimmed))
      }

    def updateRecord(update: Record) =
      $.modState(_.copy(editedRecord = Some(update)))

    override def render(p: Props, s: State): VdomNode = {
      Modal(
        animation = false,
        size = "xl",
        show = true,
        backdrop = if (s.editMode) "static" else true,
        keyboard = !s.editMode,
        onHide = _ => p.hide
      )(
        //ModalHeader()(
        //ModalTitle()(p.record.id.get), //Should be in a ModalHeader but the facade is broken!
        //),
        ModalBody()(
          super.render(p, s),
          ModalFooter()(
            if (s.editMode) {
              <.div(
                ^.textAlign.right,
                Button(
                  variant = "outline-secondary",
                  onClick = _ =>
                    (if (p.newRecord || !s.editedRecord.contains(p.record))
                       CallbackTo {
                         dom.window.confirm(
                           s"""Are you sure you would like to discard any change made to this record?""".stripMargin
                         )
                       } else
                       CallbackTo(true)).flatMap { proceed =>
                      if (proceed)
                        p.hide
                      else
                        Callback.empty
                  }
                )("Cancel"),
                <.span(^.paddingLeft := "20px"),
                Button(
                  disabled = s.hasErrors,
                  onClick = _ =>
                    s.editedRecord match {
                      case Some(rec) =>
                        p.dbApi
                          .postRecord(
                            rec.trimmed,
                            p.ver,
                            errorCallback =
                              e => Callback { dom.window.alert(e.getMessage) }
                          )
                          .map { xhr =>
                            decode[SingleResponse[Record]](xhr.responseText).toOption
                              .flatMap { _.data.map { _.attributes } }
                          }
                          .flatMap {
                            case Some(rec)
                                if p.targetId.nonEmpty && p.targetDs.nonEmpty =>
                              DbDirectLink(p.targetDs)
                                .createLink("", Some(p.targetId))(rec, p.dbApi)
                                .map { _ =>
                                  dom.window.close()
                                }
                            case _ =>
                              Callback {
                                dom.window.location.reload()
                              }.asAsyncCallback
                          }
                          .toCallback
                      case None =>
                        Callback.empty
                  }
                )(
                  OverlayTrigger(
                    overlay = Tooltip.text(
                      if (s.hasErrors)
                        "Please correct the reported errors before submitting the changes."
                      else ""
                    )
                  )("Save")
                )
              )
            } else
              Button(onClick = _ => p.hide)("Close")
          )
        )
      )
    }

    protected def renderData(p: Props, s: State): VdomNode = {
      renderVersionedRecord(
        p.record,
        p.dbApi,
        p.datasetConfig,
        p.versions.find(_._id == p.ver.getOrElse("")),
        p.versions.findLast { !_.isDraft }.map { _._id },
        s.editMode,
        s.editedRecord,
        updateRecord
      )
    }

    protected override def menuItems(p: Props, s: State): Seq[VdomNode] = {
      p.record.id match {
        case Some(id) =>
          menu(
            p.datasetConfig,
            p.dbApi,
            p.ver,
            id,
            isRelease = p.isRelease,
            hasAccess = p.userInfo.is("writer") && !s.editMode,
            editCallback = toEditMode
          )
        case None => Seq()
      }
    }
  }

  private val component = ScalaComponent
    .builder[Props]
    .initialStateFromProps { p =>
      State(editedRecord = if (p.newRecord) Some(p.record) else None)
    }
    .renderBackend[Backend]
    .build

  def apply(record: Record,
            datasetConfig: DatasetConfig,
            dbApi: DatabaseApi,
            resourceUrl: ResourceUrl,
            ver: Option[String],
            versions: Seq[VersionInfo],
            hide: Callback,
            userInfo: UserInfo,
            newRecord: Boolean,
            targetId: String = "",
            targetDs: String = "") =
    component(
      Props(
        record,
        datasetConfig,
        dbApi,
        resourceUrl,
        ver,
        versions,
        hide,
        userInfo,
        newRecord,
        targetId,
        targetDs
      )
    )
}
