package no.uit.sfb.dataui.view

import japgolly.scalajs.react.vdom.html_<^.VdomElement
import no.uit.sfb.cbf.shared.config.DatasetConfig
import no.uit.sfb.facade.icon.Glyphicon

sealed trait ViewSelectorLike {
  def name: String

  def icon: VdomElement

  def enabled(config: Option[DatasetConfig]): Boolean = true
}

case object TableViewSelector extends ViewSelectorLike {
  val name: String = "Table view"
  val icon: VdomElement = Glyphicon("ViewList")
}

case object GraphViewSelector extends ViewSelectorLike {
  val name: String = "Graph view"
  val icon: VdomElement = Glyphicon("Graph")

  override def enabled(config: Option[DatasetConfig]): Boolean = {
    val o = config.flatMap {
      _.persistedAttributes.find(
        attr => Seq("num", "cat", "date").contains(attr.lType.primary)
      )
    }
    o.nonEmpty
  }
}

case object GeoViewSelector extends ViewSelectorLike {
  val name: String = "Geo view"
  val icon: VdomElement = Glyphicon("Globe")

  override def enabled(config: Option[DatasetConfig]): Boolean = {
    val o = config.flatMap {
      _.persistedAttributes.find(_.lType.secondary == "geo")
    }
    o.nonEmpty
  }
}

case object MapViewSelector extends ViewSelectorLike {
  val name: String = "Map view"
  val icon: VdomElement = Glyphicon("GeoGlobe")

  override def enabled(config: Option[DatasetConfig]): Boolean = {
    val o = config.flatMap {
      _.persistedAttributes.find(attr => attr.lType.primary.contains("latlon"))
    }
    o.nonEmpty
  }
}

object ViewSelectorLike {
  val all =
    Seq(TableViewSelector, GraphViewSelector, GeoViewSelector, MapViewSelector)
}
