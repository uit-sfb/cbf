package no.uit.sfb.dataui.components.menu

import japgolly.scalajs.react.ScalaComponent
import japgolly.scalajs.react.component.Scala.BackendScope
import japgolly.scalajs.react.vdom.html_<^._

object MenuComp {

  case class Props(items: Seq[VdomNode])

  //ATTENTION: Do NOT embed in another div or the sticky would not work
  class Backend($ : BackendScope[Props, Unit]) {
    def render(p: Props): VdomElement = {
      if (p.items.nonEmpty) {
        //Do NOT embed in another div or the sticky would not work
        <.div(
          (Seq(^.className := "sticky-top" /*, <.h5("Menu")*/ ) ++ p.items): _*
        )
      } else
        <.div()
    }
  }

  private lazy val component = ScalaComponent
    .builder[Props]
    .renderBackend[Backend]
    .build

  def apply(items: Seq[VdomNode]) =
    component(Props(items))
}
