package no.uit.sfb.dataui.utils.com.loader

import io.circe.generic.extras.Configuration
import io.circe.generic.extras.auto._
import io.circe.parser.decode
import japgolly.scalajs.react.AsyncCallback
import no.uit.sfb.cbf.shared.jsonapi.MultipleResponse
import no.uit.sfb.cbf.shared.model.v1.nativ.Record
import no.uit.sfb.dataui.utils.com.DatabaseApi

trait CollSizeLoader {
  protected def loadColSize(
    dbApi: DatabaseApi,
    ver: Option[String],
    searchQuery: String
  ): AsyncCallback[(Option[Int], Option[Int])] = {
    implicit val customConfig: Configuration =
      Configuration.default.withDefaults
    dbApi
      .get(
        dbApi.numItems(ver, searchQuery).toString,
        contentType = Some("application/vnd.api+json")
      )
      .map { xhr =>
        val mr = decode[MultipleResponse[Record]](xhr.responseText).toTry.get
        mr.meta.get("totalFilteredItems").map {
          _.toInt
        } -> mr.meta.get("totalUnfilteredItems").map {
          _.toInt
        }
      }
  }
}
