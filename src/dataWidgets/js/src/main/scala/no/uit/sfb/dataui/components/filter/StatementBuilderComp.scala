package no.uit.sfb.dataui.components.filter

import japgolly.scalajs.react._
import japgolly.scalajs.react.vdom.html_<^._
import no.uit.sfb.cbf.shared.config.{AttributeDef, DatasetConfig}
import no.uit.sfb.dataui.utils.edit.DisplayableTypeLike
import no.uit.sfb.facade.bootstrap.buttons.Button
import no.uit.sfb.facade.bootstrap.form.FormControl
import no.uit.sfb.facade.bootstrap.overlay.{OverlayTrigger, Tooltip}

object StatementBuilderComp {
  case class Props(datasetConfig: DatasetConfig,
                   addStatement: String => Callback) {
    def typeOf(ref: String) = DisplayableTypeLike(ref, datasetConfig)
  }

  case class State(attr: Option[String] = None,
                   operator: Option[String] = None,
                   operand: Map[String, String] = Map())

  class Backend($ : BackendScope[Props, State]) {
    def changeAttr(ref: String) =
      $.toModStateWithPropsFn(
        {
          case (s, p) =>
            //We exclude any operator whose filter is empty (those are only used for display in edit record mode)
            val headOperator =
              p.typeOf(ref)
                .flatMap { _.operators.find { _.filter("", Map()).nonEmpty } }
                .map {
                  _.label
                }
            Some(
              s.copy(attr = Some(ref), operator = headOperator, operand = Map())
            )
        },
        Callback.empty
      )

    def changeOperator(op: String) = $.modState(_.copy(operator = Some(op)))

    def changeOperand(key: String, op: String) =
      $.modState(s => s.copy(operand = s.operand + (key -> op)))

    def render(p: Props, s: State) = {
      val tp = s.attr.flatMap { p.typeOf }
      val operators = tp.toSeq.flatMap { _.operators }
      val operator = operators.find(_.label == s.operator.getOrElse(""))
      val builder = <.div(
        (FormControl(
          as = "select",
          value = s.attr.getOrElse(""),
          onChange = e => changeAttr(e.target.value)
        )(
          p.datasetConfig.persistedAttributes
            .sortBy(_.name)
            .map { attr =>
              <.option(^.value := attr.ref)(attr.name)
            }: _*
        ) +: s.attr.toSeq.flatMap { ref =>
          val operatorField =
            FormControl(
              as = "select",
              value = s.operator.getOrElse(""),
              onChange = e => changeOperator(e.target.value)
            )(
              operators
                .map { op =>
                  <.option()(op.label)
                }: _*
            )
          val operandField = operator
            .map {
              _.html(
                s.operand,
                changeOperand,
                tp.map { _.tpe }.getOrElse(""),
                isFilter = true
              )
            }
            .getOrElse(<.div())
          Seq(operatorField, operandField)
        }): _*
      )
      val add = {
        val oStm = s.attr.zip(operator).map {
          case (ref, op) =>
            op.filter(ref, s.operand)
        }
        <.div(
          <.br,
          OverlayTrigger(
            overlay = Tooltip.text(
              "Add statement to selected filter. All statements in a filter are ORed."
            )
          )(
            Button(
              disabled = oStm.isEmpty,
              variant = "outline-primary",
              onClick = _ =>
                oStm match {
                  case Some(stm) if s.operand.nonEmpty =>
                    p.addStatement(stm)
                  case _ =>
                    Callback.empty
              }
            )("Add")
          )
        )
      }
      <.div(builder, add)
    }
  }

  private val component = ScalaComponent
    .builder[Props]
    .initialStateFromProps(p => {
      val oRef = p.datasetConfig.attrs.headOption.map { _.ref }
      val oLbl =
        oRef.flatMap { p.typeOf }.flatMap { _.operators.headOption }.map {
          _.label
        }
      State(oRef, oLbl)
    })
    .renderBackend[Backend]
    .build

  def apply(datasetConfig: DatasetConfig, addStatement: String => Callback) =
    component(Props(datasetConfig, addStatement))
}
