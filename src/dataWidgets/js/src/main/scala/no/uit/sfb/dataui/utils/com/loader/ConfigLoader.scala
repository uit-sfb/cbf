package no.uit.sfb.dataui.utils.com.loader

import io.circe.generic.auto._
import io.circe.parser.decode
import io.circe.generic.extras.Configuration
import japgolly.scalajs.react.AsyncCallback
import no.uit.sfb.cbf.shared.config.DatasetConfig
import no.uit.sfb.cbf.shared.jsonapi.SingleResponse
import no.uit.sfb.dataui.utils.com.DatabaseApi

trait ConfigLoader {
  protected def loadConfig(
    dbApi: DatabaseApi,
    configId: Option[String] = None
  ): AsyncCallback[SingleResponse[DatasetConfig]] = {
    implicit val customConfig: Configuration =
      Configuration.default.withDefaults
    val id = configId.getOrElse("current")
    dbApi
      .get(
        dbApi.config(id).toString,
        contentType = Some("application/vnd.api+json")
      )
      .map { xhr =>
        decode[SingleResponse[DatasetConfig]](xhr.responseText).toTry.get
      }
  }
}
