package no.uit.sfb.dataui.utils.edit

import japgolly.scalajs.react.Callback
import japgolly.scalajs.react.vdom.html_<^._
import no.uit.sfb.dataui.utils.comp.{SelectOption, UncontrolledFormControl}
import no.uit.sfb.facade.bootstrap.form.{FormControlFeedback, FormSelect}
import no.uit.sfb.facade.bootstrap.inputGroup.{InputGroup, InputGroupText}
import no.uit.sfb.facade.bootstrap.overlay.{OverlayTrigger, Tooltip}

trait OperatorLike {
  def label: String
  //If filter is empty string it will not be picked up as a filter operator
  def filter(ref: String, operand: Map[String, String]): String
  def html(operand: Map[String, String],
           onChange: (String, String) => Callback,
           tpe: String = "",
           placeholder: String = "",
           locked: Boolean = false,
           suffix: String = "",
           err: Option[String] = None,
           isFilter: Boolean = false): VdomElement
}

trait LinkLike {
  def filter(ref: String, operand: Map[String, String]): String = ""

  def html(operand: Map[String, String],
           onChange: (String, String) => Callback,
           tpe: String = "",
           placeholder: String = "",
           locked: Boolean = false,
           suffix: String = "",
           err: Option[String] = None,
           isFilter: Boolean = false): VdomElement = {
    val link = operand.getOrElse("", "")
    val oFcf = err.map { FormControlFeedback()(_) }
    val isError = oFcf.nonEmpty
    if (link.isEmpty) {
      <.div() //We do not display anything when empty
    } else {
      lazy val valueComp: VdomNode = UncontrolledFormControl(
        `type` = tpe,
        placeholder = placeholder,
        onChange = value => onChange("", value),
        value = link,
        disabled = true,
        isInvalid = isError
      )

      val tooltip =
        if (locked)
          Some(
            "This attribute is automatic and may therefore not be edited manually."
          )
        else
          Some("")

      OverlayTrigger(overlay = Tooltip.text(tooltip))(
        InputGroup()(
          Seq(
            Some(valueComp),
            if (suffix.nonEmpty)
              Some(InputGroupText()(suffix))
            else None,
            oFcf
          ).flatten: _*
        )
      )
    }
  }
}

case object ContainsLinkOp extends OperatorLike with LinkLike {
  val label = "contains"
}

case object IsContainedByLinkOp extends OperatorLike with LinkLike {
  val label = "is contained by"
}

trait ValueFilterLike {
  def mongo: String
  def filter(ref: String, operand: Map[String, String]): String = {
    def baseFilter(item: String) =
      s"""{"attrs.$ref.obj.$item": {$$$mongo: "${operand.getOrElse("", "")}"}}"""
    s"""{"$$or": [${baseFilter("val")}, ${baseFilter("iri")}]}"""
  }

  protected def htmlImpl(operand: Map[String, String],
                         onChange: (String, String) => Callback,
                         tpe: String = "",
                         placeholder: String = "",
                         locked: Boolean = false,
                         suffix: String = "",
                         err: Option[String] = None,
                         choices: Seq[String] = Seq(),
                         selectTarget: String = ""): VdomElement = {
    val select = operand.getOrElse(selectTarget, "")
    val isEnum = selectTarget.isEmpty && choices.nonEmpty
    val isRegexp = selectTarget == "opts"
    val id = operand.getOrElse("", "")
    val oFcf = err.map { FormControlFeedback()(_) }
    val isError = oFcf.nonEmpty
    val showSelect = choices.nonEmpty
    val headOption =
      if (!isRegexp)
        Seq(SelectOption("", "-"))
      else
        Seq()
    lazy val options = headOption ++ choices
      .map {
        SelectOption(_)
      }
    val selectComp =
      if (showSelect)
        Some(
          FormSelect(
            value = select,
            disabled = locked,
            onChange = e => onChange(selectTarget, e.currentTarget.value),
            isInvalid = isError
          )(options.map {
            case SelectOption(k, v, d) =>
              <.option(^.value := k, ^.disabled := d, v)
          }: _*)
        )
      else None
    lazy val valueComp: VdomNode = UncontrolledFormControl(
      `type` = tpe,
      placeholder = placeholder,
      onChange = value => onChange("", value),
      value = id,
      disabled = locked,
      isInvalid = isError
    )

    val tooltip = if (locked)
      Some(
        "This attribute is automatic and may therefore not be edited manually."
      )
    else None

    OverlayTrigger(overlay = Tooltip.text(tooltip))(
      InputGroup()(
        Seq(
          selectComp,
          if (showSelect && !isEnum && !isRegexp)
            Some(InputGroupText()(":"))
          else None,
          if (isEnum) None
          else
            Some(valueComp),
          if (suffix.nonEmpty)
            Some(InputGroupText()(suffix))
          else None,
          oFcf
        ).flatten: _*
      )
    )
  }

  def html(operand: Map[String, String],
           onChange: (String, String) => Callback,
           tpe: String = "",
           placeholder: String = "",
           locked: Boolean = false,
           suffix: String = "",
           err: Option[String] = None,
           isFilter: Boolean = false): VdomElement = {
    htmlImpl(operand, onChange, tpe, placeholder, locked, suffix, err)
  }
}

trait NumericValueFilterLike extends ValueFilterLike {
  override def filter(ref: String, operand: Map[String, String]): String = {
    s"""{"attrs.$ref.obj.num": {$$$mongo: ${operand
      .get("")
      .flatMap { _.toDoubleOption }
      .getOrElse(0.0)}}}"""
  }
}

case object EqOp extends OperatorLike with ValueFilterLike {
  lazy val label = "equals"
  lazy val mongo = "eq"
}
case object NeOp extends OperatorLike with ValueFilterLike {
  lazy val label = "not equal"
  lazy val mongo = "ne"
}
case object EqNumOp extends OperatorLike with NumericValueFilterLike {
  lazy val label = "equals"
  lazy val mongo = "eq"
}
case object NeNumOp extends OperatorLike with NumericValueFilterLike {
  lazy val label = "not equal"
  lazy val mongo = "ne"
}
case object LtNumOp extends OperatorLike with NumericValueFilterLike {
  lazy val label = "less than"
  lazy val mongo = "lt"
}
case object LteNumOp extends OperatorLike with NumericValueFilterLike {
  lazy val label = "less than or equal"
  lazy val mongo = "lte"
}
case object GtNumOp extends OperatorLike with NumericValueFilterLike {
  lazy val label = "greater than"
  lazy val mongo = "gt"
}
case object GteNumOp extends OperatorLike with NumericValueFilterLike {
  lazy val label = "greater than or equal"
  lazy val mongo = "gte"
}
case object BeforeOp extends OperatorLike with ValueFilterLike {
  lazy val label = "before"
  lazy val mongo = "lt"
}
case object BeforeOrEqOp extends OperatorLike with ValueFilterLike {
  lazy val label = "before (inclusive)"
  lazy val mongo = "lte"
}
case object AfterOp extends OperatorLike with ValueFilterLike {
  lazy val label = "after"
  lazy val mongo = "gt"
}
case object AfterOrEqOp extends OperatorLike with ValueFilterLike {
  lazy val label = "after (inclusive)"
  lazy val mongo = "gte"
}
case object RegexpOp extends OperatorLike with ValueFilterLike {
  lazy val label = "matches (regex)"
  lazy val mongo = "regex"

  override def filter(ref: String, operand: Map[String, String]): String = {
    //cf https://docs.mongodb.com/manual/reference/operator/query/regex/#mongodb-query-op.-options
    //i m x s
    val opts = {
      val tmp = operand.getOrElse("opts", "")
      if (tmp == "Case insensitive")
        "i"
      else
        ""
    }
    def baseFilter(item: String) =
      s"""{"attrs.$ref.obj.$item": {$$$mongo: /${operand.getOrElse("", "")}/$opts}}"""
    s"""{"$$or": [${baseFilter("val")}, ${baseFilter("iri")}]}"""
  }

  override def html(operand: Map[String, String],
                    onChange: (String, String) => Callback,
                    tpe: String = "",
                    placeholder: String = "",
                    locked: Boolean = false,
                    suffix: String = "",
                    err: Option[String] = None,
                    isFilter: Boolean = false): VdomElement = {
    htmlImpl(
      operand,
      onChange,
      tpe,
      placeholder,
      locked,
      suffix,
      err,
      Seq("Case sensitive", "Case insensitive"),
      "opts"
    )
  }
}

trait EnumOpLike extends ValueFilterLike {
  def terms: Seq[String]
  override def html(operand: Map[String, String],
                    onChange: (String, String) => Callback,
                    tpe: String = "",
                    placeholder: String = "",
                    locked: Boolean = false,
                    suffix: String = "",
                    err: Option[String] = None,
                    isFilter: Boolean = false): VdomElement = {
    htmlImpl(operand, onChange, tpe, placeholder, locked, suffix, err, terms)
  }
}

case class EqEnumOp(terms: Seq[String]) extends OperatorLike with EnumOpLike {
  lazy val label = "is"
  lazy val mongo = "eq"
}

case class NeEnumOp(terms: Seq[String]) extends OperatorLike with EnumOpLike {
  lazy val label = "is not"
  lazy val mongo = "ne"
}

case class CiriOpLike(resolvers: Seq[String])
    extends OperatorLike
    with ValueFilterLike {
  lazy val label = "is (Compact IRI)"
  lazy val mongo = "eq"
  override def filter(ref: String, operand: Map[String, String]): String = {
    val prefix = operand.getOrElse("prefix", "")
    if (prefix.isEmpty)
      s"""{"attrs.$ref.obj.val": {$$$mongo: "${operand
        .getOrElse("", "")}"}}"""
    else
      s"""{"attrs.$ref.obj.iri": {$$$mongo: "${operand.getOrElse("prefix", "")}:${operand
        .getOrElse("", "")}"}}"""
  }

  override def html(operand: Map[String, String],
                    onChange: (String, String) => Callback,
                    tpe: String = "",
                    placeholder: String = "",
                    locked: Boolean = false,
                    suffix: String = "",
                    err: Option[String] = None,
                    isFilter: Boolean = false): VdomElement = {
    htmlImpl(
      operand,
      onChange,
      tpe,
      placeholder,
      locked,
      suffix,
      err,
      resolvers,
      "prefix"
    )
  }
}

trait GeoFilterLike {
  def mongo: String
  //Attention, long comes BEFORE lat
  def filter(ref: String, operand: Map[String, String]): String =
    s"""{
       |     location:
       |       { $$near :
       |          {
       |            $$geometry: { type: "Point",  coordinates: [ ${operand
         .getOrElse("long", "")}, ${operand.getOrElse("lat", "")} ] },
       |            $$$mongo: ${operand
         .getOrElse("distance", "0")
         .toDouble * 1000}
       |          }
       |       }
       |   }""".stripMargin

  def html(operand: Map[String, String],
           onChange: (String, String) => Callback,
           tpe: String = "",
           placeholder: String = "",
           locked: Boolean = false,
           suffix: String = "",
           err: Option[String] = None,
           isFilter: Boolean = false): VdomElement = {
    val tooltip = if (locked)
      Some(
        "This attribute is automatic and may therefore not be edited manually."
      )
    else None
    val latValue = operand.getOrElse("lat", "")
    val longValue = operand.getOrElse("long", "")
    val radiusValue = operand.getOrElse("radius", "")
    val lat: VdomElement = UncontrolledFormControl(
      placeholder = "Latitude",
      `type` = "number",
      min = -90,
      max = 90,
      onChange = value => onChange("lat", value),
      value = latValue,
      isInvalid = err.nonEmpty,
      disabled = locked
    )
    val long: VdomElement = UncontrolledFormControl(
      placeholder = "Longitude",
      `type` = "number",
      min = -180,
      max = 180,
      onChange = value => onChange("long", value),
      value = longValue,
      isInvalid = err.nonEmpty,
      disabled = locked
    )
    val radius = UncontrolledFormControl(
      placeholder = "Radius",
      `type` = "number",
      min = 0,
      onChange = value => onChange("radius", value),
      value = radiusValue,
      isInvalid = err.nonEmpty,
      disabled = locked
    ): VdomElement
    val oFcf = err.map { FormControlFeedback()(_) }
    OverlayTrigger(overlay = Tooltip.text(tooltip))(
      Seq(
        Some(
          InputGroup()(
            Seq(
              Some(lat),
              Some(InputGroupText()("N")),
              Some(<.span(^.paddingRight := "10px")),
              Some(long),
              Some(InputGroupText()("E")),
              oFcf
            ).flatten: _*
          )
        ),
        if (isFilter)
          Some(InputGroup()(radius, InputGroupText()("km")))
        else None
      ).flatten: _*
    )
  }
}

case object LteGeoOp extends OperatorLike with GeoFilterLike {
  lazy val label = "within"
  lazy val mongo = "maxDistance"
}
case object GtGeoOp extends OperatorLike with GeoFilterLike {
  lazy val label = "not within"
  lazy val mongo = "minDistance"
}
