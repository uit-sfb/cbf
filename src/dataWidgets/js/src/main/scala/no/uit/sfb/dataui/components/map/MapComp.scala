package no.uit.sfb.dataui.components.map

import io.circe.generic.extras.Configuration
import io.circe.generic.extras.auto._
import io.circe.parser.decode
import no.uit.sfb.dataui.utils.com.GenericDerivation._
import japgolly.scalajs.react.vdom.html_<^._
import japgolly.scalajs.react.{
  AsyncCallback,
  BackendScope,
  Callback,
  ScalaComponent,
  ~=>
}
import no.uit.sfb.cbf.shared.config.AttributeDef
import no.uit.sfb.dataui.utils.com._
import no.uit.sfb.dataui.utils.graph.GraphResult
import no.uit.sfb.facade.reactleaflet
import no.uit.sfb.facade.reactleaflet.options.PositionOption
import no.uit.sfb.facade.reactleaflet.{CircleMarker, EventObject, Tooltip}
import org.scalajs.dom
import org.scalajs.dom.window

object MapComp {

  implicit val customConfig: Configuration =
    Configuration.default.withDefaults

  case class Props(dbApi: DatabaseApi,
                   url: String,
                   attrs: Seq[AttributeDef] = Nil,
                   center: (Double, Double),
                   zoom: Int,
                   height: Int,
                   error: Throwable ~=> Callback)

  case class State(data: Seq[Map[String, Any]] = Seq())

  def createMarker(p: Props, s: State) = {
    s.data.flatMap { m =>
      val url = m.getOrElse("x", "").toString
      val position = m.getOrElse("latlon", "").toString //It could be empty in case the value returned was none too
      if (position.isEmpty)
        None
      else {
        val fields: Seq[(String, String)] = p.attrs.map { attr =>
          attr.name -> m.getOrElse(attr.ref, "-").toString
        }

        val marker = CircleMarker(
          center = PositionOption(position),
          eventHandlers = EventObject(_ => Callback(window.open(url)))
        )(
          Tooltip()(
            fields.headOption.toSeq.map { case (_, v) => <.h5(v) } :+
              <.table(
                <.tbody(
                  fields.tail
                    .map { case (n, v) => <.tr(<.td(<.b(n)), <.td(v)) }: _*
                )
              ): _*
          )
        )
        Some(marker)
      }
    }
  }

  def fetchData(p: Props): AsyncCallback[GraphResult] = {
    //Requires no.uit.sfb.dataui.utils.com.GenericDerivation._
    p.dbApi
      .get(p.url, contentType = Some("application/json"))
      .map { xhr =>
        //Requires to import no.uit.sfb.dataui.utils.com.GenericDerivation._
        val res = decode[GraphResult](xhr.responseText).toTry.get
        res
      }
  }

  class Backend($ : BackendScope[Props, State]) {

    def render(p: Props, s: State): VdomNode = {
      val markers = createMarker(p, s)
      val elements = reactleaflet.TileLayer(
        attribution =
          "&copy; <a href=\"http://osm.org/copyright\">OpenStreetMap</a> contributors",
        url = "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
      )() +: markers
      reactleaflet.MapContainer(
        center = p.center,
        zoom = p.zoom,
        height = p.height
      )(elements: _*)
    }
  }

  private def dropPrefix(k: String) = {
    if (k.startsWith("y_"))
      k.drop(2)
    else
      k
  }

  private def data(gr: GraphResult) = {
    gr.graph.map {
      _.map {
        case (k, adt: ADTList) =>
          dropPrefix(k) -> adt.v
            .collect {
              case x: ADTString => x.v
              case x: ADTInt    => x.v.toString
              case x: ADTDouble => x.v.toString
            }
            .mkString("|")
        case (k, adt: ADT) =>
          dropPrefix(k) -> adt.v
      }
    }
  }

  private val component = ScalaComponent
    .builder[Props]
    .initialState(State())
    .renderBackend[Backend]
    .componentDidMount(lf => {
      val p = lf.props

      fetchData(p)
        .flatMap { graphRes =>
          lf.modStateAsync(_.copy(data = data(graphRes)))
        }
        .handleError { p.error(_).asAsyncCallback }
        .toCallback
    })
    .componentDidUpdate(lf => {
      val p = lf.currentProps
      if (lf.prevProps.url == p.url)
        Callback.empty //Avoid infinite update loop
      else {
        fetchData(p)
          .flatMap { graphRes =>
            lf.modStateAsync(_.copy(data = data(graphRes)))
          }
          .handleError { p.error(_).asAsyncCallback }
          .toCallback
      }
    })
    .build

  def apply(dbApi: DatabaseApi,
            url: String,
            attributes: Seq[AttributeDef],
            center: (Double, Double) = (51.505, -0.09),
            zoom: Int = 2,
            height: Int = dom.window.innerHeight.toInt / 2,
            error: Throwable ~=> Callback) =
    component(Props(dbApi, url, attributes, center, zoom, height, error))
}
