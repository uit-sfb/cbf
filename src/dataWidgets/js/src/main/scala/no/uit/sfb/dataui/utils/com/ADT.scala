package no.uit.sfb.dataui.utils.com

import cats.syntax.functor._
import io.circe._
import io.circe.syntax._

import scala.util.Try

//Algebraic Data Type
//https://circe.github.io/circe/codecs/adt.html
sealed trait ADT {
  def v: Any

  def numeric: Boolean

  def `null`: Boolean

  def isCollection: Boolean
}

abstract class ADTScalar extends ADT {
  val isCollection: Boolean = false
}

case class ADTNull() extends ADTScalar {
  val v: Unit = ()
  val numeric = false
  val `null` = true
}

case class ADTInt(v: Int) extends ADTScalar {
  val numeric = true
  val `null` = false
}

case class ADTDouble(v: Double) extends ADTScalar {
  val numeric = true
  val `null` = false
}

case class ADTString(v: String) extends ADTScalar {
  val numeric = false
  val `null` = false
}

case class ADTList(v: List[ADTScalar]) extends ADT {
  val isCollection = true
  val numeric = false
  val `null` = false
}

object GenericDerivation {
  implicit val decodeADTNull: Decoder[ADTNull] = new Decoder[ADTNull] {
    final def apply(c: HCursor): Decoder.Result[ADTNull] = {
      if (c.value.isNull)
        Right(ADTNull())
      else
        Left(DecodingFailure("Not Null", List()))
    }
  }

  implicit val decodeADTInt: Decoder[ADTInt] = Decoder.decodeInt.emapTry { i =>
    Try {
      ADTInt(i)
    }
  }

  implicit val decodeADTDouble: Decoder[ADTDouble] =
    Decoder.decodeDouble.emapTry { d =>
      Try {
        ADTDouble(d)
      }
    }

  implicit val decodeADTString: Decoder[ADTString] =
    Decoder.decodeString.emapTry { str =>
      Try {
        ADTString(str)
      }
    }

  implicit val decodeADTScalar: Decoder[ADTScalar] =
    List[Decoder[ADTScalar]](
      Decoder[ADTNull].widen,
      Decoder[ADTInt].widen,
      Decoder[ADTDouble].widen,
      Decoder[ADTString].widen,
    ).reduceLeft(_ or _)

  implicit val encodeADTScalar: Encoder[ADTScalar] = Encoder.instance {
    case _: ADTNull   => Json.Null
    case x: ADTInt    => x.v.asJson
    case x: ADTDouble => x.v.asJson
    case x: ADTString => x.v.asJson
  }

  implicit val decodeADTList: Decoder[ADTList] =
    Decoder.decodeList[ADTScalar].emapTry { v =>
      Try {
        ADTList(v)
      }
    }

  implicit val encodeListADT: Encoder[List[ADTScalar]] =
    Encoder.encodeList[ADTScalar]

  implicit val encodeADT: Encoder[ADT] = Encoder.instance {
    case x: ADTScalar => encodeADTScalar(x)
    case x: ADTList   => x.v.asJson
  }

  implicit val decodeADT: Decoder[ADT] =
    List[Decoder[ADT]](Decoder[ADTScalar].widen, Decoder[ADTList].widen)
      .reduceLeft(_ or _)
}
