package no.uit.sfb.dataui.utils.com.loader

import io.circe.generic.extras.auto._
import io.circe.generic.extras.Configuration
import io.circe.parser.decode
import japgolly.scalajs.react.AsyncCallback
import no.uit.sfb.cbf.shared.auth.UserInfo
import no.uit.sfb.dataui.utils.com.DatabaseApi

trait UserInfoLoader {
  protected def loadUserInfo(dbApi: DatabaseApi): AsyncCallback[UserInfo] = {
    implicit val customConfig: Configuration = {
      Configuration.default.withDefaults
    }
    dbApi
      .get(dbApi.userInfo().toString, contentType = Some("application/json"))
      .map { xhr =>
        decode[UserInfo](xhr.responseText).toTry.get
      }
  }
}
