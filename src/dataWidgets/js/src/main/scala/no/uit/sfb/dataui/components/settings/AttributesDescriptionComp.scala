package no.uit.sfb.dataui.components.settings

import japgolly.scalajs.react.component.Scala.BackendScope
import japgolly.scalajs.react.vdom.html_<^._
import japgolly.scalajs.react.ScalaComponent
import no.uit.sfb.cbf.shared.config.DatasetConfig
import no.uit.sfb.facade.bootstrap.Table

object AttributesDescriptionComp {

  case class Props(dc: DatasetConfig)

  class Backend($ : BackendScope[Props, Unit]) {

    def render(p: Props): VdomElement = {
      <.div(
        Table(hover = false)(
          <.thead(
            <.tr(
              <.th("Reference"),
              <.th("Name"),
              <.th("Description"),
              <.th("Unit"),
            )
          ),
          <.tbody(p.dc.attrs.map { attr =>
            <.tr(
              <.td(attr.ref),
              <.td(attr.name),
              <.td(attr.descr),
              <.td(attr.suffix)
            )
          }: _*)
        )
      )
    }
  }

  private lazy val component = ScalaComponent
    .builder[Props]
    .renderBackend[Backend]
    .build

  def apply(dc: DatasetConfig) =
    component(Props(dc))
}
