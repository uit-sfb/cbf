package no.uit.sfb.dataui.utils.com

import io.lemonlabs.uri.AbsoluteUrl

object Secure {
  def apply(url: String) = {
    val parsed = AbsoluteUrl.parse(url)
    parsed.replaceParams("auth", true).toString()
  }
}
