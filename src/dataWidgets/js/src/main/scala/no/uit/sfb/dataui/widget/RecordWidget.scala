package no.uit.sfb.dataui.widget

import japgolly.scalajs.react.ScalaComponent
import japgolly.scalajs.react.component.Scala.BackendScope
import japgolly.scalajs.react.vdom.html_<^._
import no.uit.sfb.cbf.shared.config.DatasetConfig
import no.uit.sfb.dataui.utils.com.{
  DatabaseApi,
  ResourceUrl,
  ToasterLike,
  ToasterStateLike
}
import no.uit.sfb.dataui.utils.com.loader.ConfigLoader
import no.uit.sfb.dataui.utils.comp.{Spin, ToastLike, ToastsComp}
import no.uit.sfb.dataui.view.RecordView
import org.scalajs.dom

import scala.scalajs.js.annotation.{JSExport, JSExportTopLevel}

@JSExportTopLevel("RecordWidget")
object RecordWidget extends ConfigLoader {

  case class Props(dbApi: DatabaseApi,
                   resourceUrl: ResourceUrl,
                   ver: Option[String],
                   recordId: String)

  case class State(datasetConfig: Option[DatasetConfig] = None,
                   toasts: Seq[ToastLike] = Seq())
      extends ToasterStateLike {
    def newToasts(ts: Seq[ToastLike]) =
      copy(toasts = ts).asInstanceOf[this.type]
  }

  class Backend(val $ : BackendScope[Props, State])
      extends ToasterLike[Props, State] {

    def render(p: Props, s: State): VdomNode = {
      <.div(
        ToastsComp(s.toasts, deleteToast),
        s.datasetConfig match {
          case Some(cfg) =>
            RecordView(
              RecordView
                .Props(p.dbApi, p.resourceUrl, p.ver, p.recordId, cfg, error)
            )
          case None =>
            Spin()
        }
      )
    }
  }

  private val component = ScalaComponent
    .builder[Props]
    .initialState(State())
    .renderBackend[Backend]
    .componentDidMount(lf => {
      val p = lf.props
      val loadConfigAsync = loadConfig(p.dbApi)
        .flatMap { datasetConfigResp =>
          val datasetConfig = datasetConfigResp.data.get.attributes
          lf.modStateAsync(_.copy(datasetConfig = Some(datasetConfig)))
        }
        .handleError { err =>
          lf.backend.error(err).asAsyncCallback
        }
      loadConfigAsync.toCallback
    })
    .build

  def apply(apiEndpoint: String,
            accessToken: Option[String],
            loginUrl: Option[String],
            logoutUrl: Option[String],
            resourceUrl: String,
            dbName: String,
            ver: Option[String],
            recordId: String) =
    component(
      Props(
        DatabaseApi(apiEndpoint, accessToken, dbName),
        ResourceUrl(resourceUrl),
        ver,
        recordId
      )
    )

  @JSExport("renderInto")
  def renderInto(apiEndpoint: String,
                 accessToken: String,
                 loginUrl: String,
                 logoutUrl: String,
                 resourceUrl: String,
                 dbName: String,
                 version: String,
                 recordId: String,
                 elemId: String = "entrypoint") =
    apply(
      apiEndpoint,
      if (accessToken.isEmpty) None else Some(accessToken),
      if (loginUrl.isEmpty) None else Some(loginUrl),
      if (logoutUrl.isEmpty) None else Some(logoutUrl),
      resourceUrl,
      dbName,
      if (version.isEmpty) None else Some(version),
      recordId
    ).renderIntoDOM(dom.document.getElementById(elemId))
}
