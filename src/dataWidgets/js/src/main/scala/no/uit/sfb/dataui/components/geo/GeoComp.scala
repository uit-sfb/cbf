package no.uit.sfb.dataui.components.geo

import io.circe.generic.extras.Configuration
import io.circe.parser.decode
import io.circe.generic.extras.auto._
import japgolly.scalajs.react.extra.Ajax
import no.uit.sfb.dataui.utils.com.GenericDerivation._
import japgolly.scalajs.react.vdom.html_<^
import japgolly.scalajs.react.vdom.svg_<^._
import japgolly.scalajs.react.{
  AsyncCallback,
  BackendScope,
  Callback,
  CallbackTo,
  Reusable,
  ScalaComponent,
  ~=>
}
import no.uit.sfb.cbf.shared.config.{AttributeDef, CustomType}
import no.uit.sfb.dataui.utils.com.{DatabaseApi, ResourceUrl}
import no.uit.sfb.dataui.utils.comp.Spin
import no.uit.sfb.dataui.utils.geo.MyGeometry
import no.uit.sfb.dataui.utils.graph.{
  DataSource,
  DataSourceBuilder,
  GraphResult
}
import no.uit.sfb.dataui.view.ViewBackendLike
import no.uit.sfb.facade.geojson._
import no.uit.sfb.facade.d3.geo.projection._
import no.uit.sfb.facade.d3.geo.{GeoPath, Projection}
import no.uit.sfb.facade.d3.scale.Scale
import no.uit.sfb.facade.topojson._

import scala.scalajs.js
import scala.scalajs.js.JSON

object GeoComp {

  implicit val customConfig: Configuration =
    Configuration.default.withDefaults

  //Does not correspond to real size, but are rather used to indicate a scale
  protected val svgWidth = 800
  protected val svgHeight = 450

  case class GeoSettings(url: String, //Geo url
                         projection: Projection)

  object GeoSettings {
    def apply(oct: Option[CustomType],
              resourceUrl: ResourceUrl): GeoSettings = {
      val ct = oct.getOrElse(CustomType("cat:geo:default", Map()))
      val url = resourceUrl.absolute(
        ct("topojson").getOrElse(s"/geo/countries.topojson")
      )
      val projection = ct("projection").getOrElse("mercator") match {
        case "naturalEarth1" =>
          GeoNaturalEarth1()
        case _ =>
          GeoMercator()
      }
      GeoSettings(url, projection)
    }
  }

  case class Props(dbApi: DatabaseApi,
                   resourceUrl: ResourceUrl, //Keep me
                   ver: Option[String],
                   searchQuery: String,
                   xAxis: DataSource,
                   yAxis: DataSource,
                   geoSettings: GeoSettings,
                   error: Throwable ~=> Callback) {
    lazy val url: String = {
      dbApi
        .graph(
          ver,
          Seq(
            ("x", xAxis.attrRef, "eachR"),
            ("y_1", yAxis.attrRef, yAxis.method)
          ),
          filter = Seq(None -> searchQuery),
          None
        )
        .toString()
    }

    lazy val yAxisLabel: String = yAxis.label
  }

  case class State(geometryArray: Option[Seq[MyGeometry]] = None,
                   data: Map[Any, Any] = Map(),
                   selectedCountry: String = "",
  )

  def fetchGeoData(p: Props): AsyncCallback[Seq[MyGeometry]] = {
    //Requires no.uit.sfb.dataui.utils.com.GenericDerivation._
    //This is NOT an API call! (otherwise we could not point to external topojson files)
    Ajax("GET", p.geoSettings.url).send.asAsyncCallback
      .map { xhr =>
        val topo = JSON.parse(xhr.responseText).asInstanceOf[Topology]
        val featureColl = feature(topo, topo.objects.main)
          .asInstanceOf[FeatureCollection]
        val path = GeoPath(
          p.geoSettings.projection.fitSize((svgWidth, svgHeight), featureColl)
        )
        featureColl.features.map { f =>
          MyGeometry(path = path(f).asInstanceOf[String], f)
        }.toSeq
      }
  }

  def loadCountryData(p: Props): AsyncCallback[GraphResult] = {
    p.dbApi
      .get(p.url, contentType = Some("application/json"))
      .map { xhr =>
        //Requires to import no.uit.sfb.dataui.utils.com.GenericDerivation._
        decode[GraphResult](xhr.responseText).toTry.get
      }
  }

  class Backend($ : BackendScope[Props, State])
      extends ViewBackendLike[Props, State] {
    val selectCountry = Reusable.fn((country: String) => {
      $.modState(_.copy(selectedCountry = country))
    })

    def getStrokeStyle(s: State, country: String): StrokeStyle = {
      if (country == s.selectedCountry) StrokeStyle("#FF8C00", 0.6)
      else StrokeStyle("#000000", 0.1)
    }

    def getColorValue(s: State, v: Option[Any]) = {

      import scala.language.implicitConversions
      //use implicit conversion to avoid ADTNull exception
      implicit def convertToNumeric(v: Any) = v.asInstanceOf[Number].doubleValue

      lazy val data = s.data.values.map { elem =>
        elem.asInstanceOf[Number].doubleValue match {
          case n if n >= 0.0 => n
          case _ =>
            throw new Exception("Support of negative values is not supported")
        }
      }
      lazy val max = if (data.nonEmpty) data.max else 0.0
      v match {
        case Some(v) => //make input value as double number between 0 and 1 for proper scaling
          val res =
            if (max > 0.0)
              BigDecimal(v / max)
                .setScale(2, BigDecimal.RoundingMode.HALF_UP)
                .toDouble
            else 0.0
          val scale = Scale
            .scaleLinear()
            .domain(js.Array(0.0, 1.0))
            .range(js.Array("#D6EAF8", "#1B4F72"))
          scale(res).asInstanceOf[String]
        case None => "#EEEEEE" //grey
      }
    }

    override def renderData(p: Props, s: State): VdomNode = {

      s.geometryArray match {
        case Some(geometries) =>
          <.svg(
            ^.id := "map",
            ^.width := "100%",
            ^.height := "100%",
            ^.viewBox := s"0 0 $svgWidth $svgHeight",
            ^.preserveAspectRatio := "xMidYMid meet",
            <.g(geometries.map { g =>
              {
                val featureName = g.f.properties.name.toOption.getOrElse("-")
                val featureId =
                  g.f.properties.gaz.toOption.getOrElse(featureName)
                val strokeStyle = getStrokeStyle(s, featureId)
                val yValue = s.data.get(featureId)
                <.g(
                  <.path(
                    ^.id := g.f.properties.name,
                    ^.d := g.path,
                    ^.fill := getColorValue(s, yValue),
                    ^.stroke := strokeStyle.strokeColor,
                    ^.strokeWidth := strokeStyle.strokeWidth,
                    html_<^.^.onMouseOver ==> { e =>
                      selectCountry(featureId)
                    },
                    html_<^.^.onMouseOut ==> { e =>
                      selectCountry("")
                    },
                    <.title(
                      s"$featureName\n${p.yAxisLabel} ${yValue.getOrElse("Unknown")}"
                    ),
                  ),
                )
              }
            }: _*),
          )
        case None =>
          Spin()
      }
    }
  }

  private val component = ScalaComponent
    .builder[Props]
    .initialState(State())
    .renderBackend[Backend]
    .componentDidMount(lf => {
      val p = lf.props
      val geoAsync = fetchGeoData(p).map { geoData =>
        Some(geoData)
      }
      val dataAsync = loadCountryData(p).map { graphRes =>
        graphRes.data("y_1").toMap
      }
      geoAsync
        .zip(dataAsync)
        .flatMap {
          case (geo, data) =>
            lf.modStateAsync(_.copy(geometryArray = geo, data = data))
        }
        .handleError { err =>
          p.error(err).asAsyncCallback.map { _ =>
            Seq()
          }
        }
        .toCallback
    })
    .componentDidUpdate(lf => {
      val p = lf.currentProps
      if (p.geoSettings == lf.prevProps.geoSettings && p.url == lf.prevProps.url)
        Callback.empty //Prevent infinite loop
      else {
        val geoAsync =
          if (p.geoSettings == lf.prevProps.geoSettings)
            CallbackTo(lf.currentState.geometryArray).asAsyncCallback
          else
            fetchGeoData(p).map { geoData =>
              Some(geoData)
            }
        val dataAsync =
          if (p.url == lf.prevProps.url)
            CallbackTo(lf.currentState.data).asAsyncCallback
          else {
            loadCountryData(p).map { graphRes =>
              graphRes.data("y_1").toMap
            }
          }
        geoAsync
          .zip(dataAsync)
          .flatMap {
            case (geo, data) =>
              lf.modStateAsync(_.copy(geometryArray = geo, data = data))
          }
          .handleError { err =>
            p.error(err).asAsyncCallback
          }
          .toCallback
      }
    })
    .build

  def apply(dbApi: DatabaseApi,
            resourceUrl: ResourceUrl,
            ver: Option[String],
            searchQuery: String,
            xAxis: DataSource,
            yAxis: DataSource,
            geoSettings: GeoSettings,
            error: Throwable ~=> Callback) =
    component(
      Props(
        dbApi,
        resourceUrl,
        ver,
        searchQuery,
        xAxis,
        yAxis,
        geoSettings,
        error
      )
    )
}
