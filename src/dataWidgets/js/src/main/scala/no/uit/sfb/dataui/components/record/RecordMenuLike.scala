package no.uit.sfb.dataui.components.record

import japgolly.scalajs.react.{Callback, CallbackTo}
import japgolly.scalajs.react.vdom.html_<^.VdomNode
import no.uit.sfb.cbf.shared.config.DatasetConfig
import no.uit.sfb.dataui.components.menu.MenuItemComp
import no.uit.sfb.dataui.utils.com.DatabaseApi
import no.uit.sfb.facade.icon.Glyphicon
import org.scalajs.dom

trait RecordMenuLike {
  def menu(datasetConfig: DatasetConfig,
           dbApi: DatabaseApi,
           ver: Option[String],
           id: String,
           isLoaded: Boolean = true,
           isRelease: Boolean = false,
           hasAccess: Boolean = true,
           editCallback: Callback = Callback.empty): Seq[VdomNode] = {
    lazy val deleteCallback = CallbackTo {
      dom.window.confirm(
        "Are you sure you would like to delete this record?".stripMargin
      )
    }.flatMap { proceed =>
      if (proceed)
        dbApi
          .delete(
            dbApi
              .record(ver, Some(id))
              .toString(),
            Some("application/json"),
            successCallback = Callback {
              dom.window.location.assign(s"/${dbApi.dsName}/browser${ver
                .map { v =>
                  s"?ver=$v"
                }
                .getOrElse("")}")
            },
            errorCallback = e => Callback { dom.window.alert(e.getMessage) }
          )
          .toCallback
      else
        Callback.empty
    }
    val resourcesAttr = datasetConfig.resourceAttributes
    val resourceDownloads = resourcesAttr.map { attr =>
      attr.ref -> attr.name
    }
    val resourceDownloadsWithSeparator =
      if (resourceDownloads.nonEmpty)
        Seq("" -> "") ++ //Separator
          resourceDownloads
      else
        resourceDownloads
    val downloadMenu = MenuItemComp(
      "Download",
      Glyphicon("CloudDownload"),
      Seq("json" -> "Contextual (JSON)", "tsv" -> "Contextual (TSV)") ++
        resourceDownloadsWithSeparator,
      sel =>
        Some(dbApi.proxyUrl(if (Seq("json", "tsv").contains(sel)) {
          dbApi
            .record(ver, Some(id), format = Some(sel), inline = Some(false))
            .toString()
        } else {
          dbApi.resource(ver, id, Some(sel)).toString
        })),
      disabled =
        if (isLoaded) None else Some("Please wait for the data to be loaded.")
    ): VdomNode

    val editMenu =
      if (hasAccess)
        Some(
          MenuItemComp(
            "Edit",
            Glyphicon("Edit"),
            callback = _ => editCallback,
            disabled =
              if (isRelease) Some("Editing a released record is not allowed.")
              else None
          ): VdomNode
        )
      else
        None

    val trashMenu =
      if (hasAccess)
        Some(
          MenuItemComp(
            "Delete record",
            Glyphicon("Trash"),
            callback = _ => deleteCallback,
            disabled =
              if (isRelease) Some("Deleting a released record is not allowed.")
              else None
          ): VdomNode
        )
      else
        None

    Seq(Some(downloadMenu), editMenu, trashMenu).flatten
  }
}
