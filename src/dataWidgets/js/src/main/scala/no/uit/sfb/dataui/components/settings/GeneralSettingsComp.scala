package no.uit.sfb.dataui.components.settings

import japgolly.scalajs.react.component.Scala.BackendScope
import japgolly.scalajs.react.vdom.html_<^._
import japgolly.scalajs.react.{Callback, ScalaComponent}
import no.uit.sfb.cbf.shared.config.DatasetConfig
import no.uit.sfb.cbf.shared.config._
import no.uit.sfb.dataui.utils.comp.SelectOption
import no.uit.sfb.dataui.utils.comp.{SelectField, SwitchField, TextField}
import no.uit.sfb.facade.bootstrap.form._

object GeneralSettingsComp {

  case class Props(draft: DatasetConfig,
                   updateDraft: DatasetConfig => Callback,
                   noEdit: Boolean)

  class Backend($ : BackendScope[Props, Unit]) {
    def render(p: Props): VdomElement = {
      <.div(
        FormGroup("nameInput")(
          TextField(
            labelWidth = 2,
            label = "Name",
            value = p.draft.name,
            onChange = newValue => p.updateDraft(p.draft.copy(name = newValue))
          )()
        ),
        FormGroup("descrInput")(
          TextField(
            disabled = if (p.noEdit) Some("") else None,
            labelWidth = 2,
            label = "Description",
            smallPrint = "(optional)",
            value = p.draft.description,
            onChange = newValue =>
              p.updateDraft(p.draft.copy(description = newValue))
          )()
        ),
        FormGroup("contextInput")(
          TextField(
            disabled = if (p.noEdit) Some("") else None,
            labelWidth = 2,
            label = "Website URL",
            info = "URL of the website where this dataset can be accessed.",
            smallPrint = "(optional)",
            value = p.draft.contextUrl,
            onChange = newValue =>
              p.updateDraft(p.draft.copy(contextUrl = newValue))
          )()
        ),
        FormGroup("dmpInput")(
          TextField(
            disabled = if (p.noEdit) Some("") else None,
            labelWidth = 2,
            label = "DMP",
            value = p.draft.dmp,
            onChange = newValue => p.updateDraft(p.draft.copy(dmp = newValue)),
            info = "URL to Data Management Plan.",
            smallPrint = "(optional)"
          )()
        ),
        FormGroup("grantReadAccessInput")(
          SelectField(
            disabled = if (p.noEdit) Some("") else None,
            labelWidth = 2,
            label = "Data privacy",
            value = p.draft.grantReadAccess,
            onChange = newValue =>
              p.updateDraft(p.draft.copy(grantReadAccess = newValue)),
            options = Seq(
              "anonymous" -> "Public",
              "none" -> "Semi-private",
              "reader" -> "Private"
            ).map { case (k, v) => SelectOption(k, v) },
            info =
              "Public: anyone can access the data. \n Semi-private: users must log in to get access to the data. \n Private: users must log in and have read access."
          )()
        ),
        FormGroup("latestOnlyInput")(
          SwitchField(
            disabled = if (p.noEdit) Some("") else None,
            labelWidth = 2,
            label = "Latest only",
            checked = p.draft.latestOnly,
            onChange = checked =>
              p.updateDraft(p.draft.copy(latestOnly = checked)),
            info =
              "Switch on to prevent regular users from having access to past releases (by hiding the versions selector)."
          )()
        ),
        FormGroup("releaseFromExternalInput")(
          SwitchField(
            disabled = if (p.noEdit) Some("") else None,
            labelWidth = 2,
            label = "External release",
            checked = p.draft.releaseFromExternal,
            onChange = checked =>
              p.updateDraft(p.draft.copy(releaseFromExternal = checked)),
            info =
              "Switch on if this dataset is driven by a pipeline; releasing versions from the user interface is disabled."
          )()
        )
      )
    }
  }

  private lazy val component = ScalaComponent
    .builder[Props]
    .renderBackend[Backend]
    .build

  def apply(draft: DatasetConfig,
            updateDraft: DatasetConfig => Callback,
            noEdit: Boolean) =
    component(Props(draft, updateDraft, noEdit))
}
