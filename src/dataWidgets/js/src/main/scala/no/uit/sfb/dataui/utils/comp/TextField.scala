package no.uit.sfb.dataui.utils.comp

import japgolly.scalajs.react.Callback

import scala.scalajs.js.{UndefOr, undefined, |}

case class TextField(
  value: String,
  label: String = "",
  labelWidth: Int = 0,
  //id: UndefOr[String] = undefined,
  placeholder: String = "",
  info: String = "",
  smallPrint: String = "",
  disabled: Option[String] = None, //None: enabled, Some: disabled and str is displayed as popup (overriding tooltip field)
  onChange: String => Callback,
  validate: Option[String => Option[String]] = None,
  textAreaLines: Option[Int] = None, //If None -> input, otherwise textarea with the defined number of lines (<- nb of lines not implemented yet)
  `type`: String = "",
  max: UndefOr[Double | Int] = undefined,
  min: UndefOr[Double | Int] = undefined,
  suffix: String = ""
) extends FieldLike {
  val message = disabled
  val disabledFlag = disabled.nonEmpty
  val tooltip = info

  val fieldComp = UncontrolledFormControlWithValidation(
    //id = p.id,
    value = value,
    placeholder = placeholder,
    disabled = disabledFlag,
    onChange = newValue => onChange(newValue),
    validate = validate,
    textAreaLines = textAreaLines,
    `type` = `type`,
    max = max.toString,
    min = min.toString,
    suffix = suffix
  )

  def apply() = render(label, tooltip, smallPrint)
}
