package no.uit.sfb.dataui.components.search

import japgolly.scalajs.react.Ref.Simple
import japgolly.scalajs.react.component.Scala.BackendScope
import japgolly.scalajs.react.vdom.html_<^._
import japgolly.scalajs.react._
import no.uit.sfb.cbf.shared.config.DatasetConfig
import no.uit.sfb.dataui.components.filter.FiltersComp
import no.uit.sfb.dataui.utils.comp.UncontrolledFormControlWithValidation
import no.uit.sfb.facade.bootstrap.buttons.{Button, ButtonGroup}
import no.uit.sfb.facade.bootstrap.modal.{Modal, ModalBody, ModalFooter}
import no.uit.sfb.facade.bootstrap.overlay.{OverlayTrigger, Tooltip}
import no.uit.sfb.facade.icon.Glyphicon
import org.scalajs.dom.html
import org.scalajs.dom.html.Input

object SearchComp {

  case class Props(searchQuery: String,
                   changeSearchQuery: String ~=> Callback,
                   datasetConfig: Option[DatasetConfig],
                   origFilters: Array[Seq[String]])

  case class State(showModal: Boolean = false,
                   filters: Array[Seq[String]] = Array())

  protected def filtersToString(filters: Array[Seq[String]]) = {
    val disjunctions = filters
      .flatMap { flt =>
        val stms = flt.filter(_.nonEmpty)
        if (stms.isEmpty)
          None
        else
          Some(s"""{"$$or": [${stms.mkString(", ")}]}""")
      }
      .filter(_.nonEmpty)
    if (disjunctions.isEmpty)
      ""
    else
      s"""{"$$and": [${disjunctions.mkString(", ")}]}"""
  }

  class Backend($ : BackendScope[Props, State]) {
    //private val ref = Ref.toJsComponent(UncontrolledFormControl.componentJS)
    private val ref: Simple[Input] = Ref[html.Input]

    lazy val showModal = $.modState(_.copy(showModal = true))
    lazy val closeModal = $.modState(_.copy(showModal = false))

    //If inserting, idx is ignored
    def upsertFilter(idx: Int, update: Seq[String]): Callback = {
      $.modState { s =>
        val newFilter = if (s.filters.length > idx && idx >= 0) {
          s.filters.updated(idx, update)
        } else {
          s.filters.appended(update)
        }
        s.copy(filters = newFilter)
      }
    }

    def removeFilter(idx: Int): Callback = {
      $.modState { s =>
        val newFilter = s.filters.patch(idx, Nil, 1)
        s.copy(filters = newFilter)
      }
    }
    val removeAllFilters: Callback = {
      $.modState { _.copy(filters = Array()) }
    }

    def render(p: Props, s: State): VdomElement = {
      val advSearch = s.filters.nonEmpty
      <.div(
        Modal(
          animation = false,
          backdrop = "static",
          size = "xl",
          show = s.showModal,
          onHide = _ => closeModal
        )(
          ModalBody()(<.h2("Filters"), p.datasetConfig match {
            case Some(dc) =>
              FiltersComp(dc, s.filters, upsertFilter, removeFilter)
            case None =>
              <.div()
          }),
          ModalFooter()(
            OverlayTrigger(
              overlay = Tooltip.text(
                "Complex queries can be built by overlaying multiple filters (ANDed with each other). Each filter is composed of one or more statements ORed with each other."
              )
            )(Button(disabled = false, variant = "outline-secondary")("?")),
            Button(
              onClick =
                _ => closeModal >> removeAllFilters >> p.changeSearchQuery(""),
              variant = "outline-danger"
            )("Clear all"),
            Button(
              disabled = p.origFilters.sameElements(s.filters),
              onClick = _ =>
                closeModal >> p.changeSearchQuery(filtersToString(s.filters)),
              variant = "success"
            )("Apply")
          )
        ),
        ButtonGroup()(
          OverlayTrigger(
            overlay = Tooltip.text(
              if (advSearch)
                "Text search disabled when filters are in use."
              else ""
            )
          )(
            //Do NOT convert to a TextFieldUncontrolled as it would interfere with the layout of the Add button
            UncontrolledFormControlWithValidation(
              disabled = advSearch,
              value =
                if (advSearch) s"${s.filters.length} filter(s) applied"
                else p.searchQuery,
              placeholder = "Text search...",
              onChange = p.changeSearchQuery
            ),
          ),
          Button(
            onClick = _ => showModal,
            variant = if (advSearch) "primary" else "outline-primary",
            disabled = !advSearch && p.searchQuery.nonEmpty
          )(
            OverlayTrigger(
              overlay = Tooltip.text(
                "Advanced search" + (if (p.searchQuery.nonEmpty)
                                       "\n(disabled when text search is in use)."
                                     else "")
              )
            )(Glyphicon("AdvancedSearch"))
          )
        )
      )
    }
  }

  private lazy val component = ScalaComponent
    .builder[Props]
    .initialStateFromProps { p =>
      State(filters = p.origFilters)
    }
    .renderBackend[Backend]
    .build

  def apply(searchQuery: String,
            changeSearchQuery: String ~=> Callback,
            datasetConfig: Option[DatasetConfig],
            filters: Array[Seq[String]]) =
    component(Props(searchQuery, changeSearchQuery, datasetConfig, filters))
}
