package no.uit.sfb.dataui.utils.com

import io.lemonlabs.uri.{QueryString, RelativeUrl}
import japgolly.scalajs.react.{AsyncCallback, Callback}
import no.uit.sfb.cbf.shared.jsonapi.{Data, SingleResponse}
import no.uit.sfb.cbf.shared.model.v1.nativ.Record
import io.circe.generic.auto._
import io.circe.syntax._
import org.scalajs.dom.XMLHttpRequest

case class DatabaseApi(apiEndpoint: String,
                       accessToken: Option[String],
                       dsName: String = "-",
                       prefilter: String = "")
    extends ApiCall {
  protected val base = RelativeUrl.empty
  protected val apiVer = "v1"

  protected def restPath(dbBound: Boolean,
                         resourceName: String,
                         id: Option[String] = None) =
    Seq("rest", apiVer, if (dbBound) dsName else "-", resourceName) ++ id.toList

  protected def rpcPath(dbBound: Boolean,
                        resourceName: String,
                        id: Option[String] = None) =
    Seq("rpc", apiVer, if (dbBound) dsName else "-", resourceName) ++ id.toList

  def api() = {
    base.withPathParts(Seq("docs", "api"))
  }

  def configs(fields: Seq[String] = Seq()) = {
    val resourceName = "configs"
    val query = fields.map { v =>
      s"fields[$resourceName]" -> v
    }
    base
      .withPathParts(restPath(true, resourceName))
      .withQueryString(QueryString.fromTraversable(query))
  }

  def config(id: String = "current", inline: Boolean = false) = {
    val query: Seq[(String, String)] =
      Seq("id" -> id, "inline" -> inline.toString)
    base
      .withPathParts(restPath(true, "configs", Some(id)))
      .withQueryString(QueryString.fromTraversable(query))
  }

  def revertToConfig(id: String) = {
    base
      .withPathParts(rpcPath(true, "revert-config", Some(id)))
  }

  def versions(ver: Option[String] = None) = {
    base.withPathParts(restPath(true, "versions", ver))
  }

  def userInfo() = {
    //val query = Seq("auth" -> id.nonEmpty)
    base
      .withPathParts(rpcPath(true, "user-info"))
    //.withQueryString(QueryString.fromTraversable(query))
  }

  def members() = {
    base
      .withPathParts(restPath(true, "members"))
    //.withQueryString(QueryString.fromTraversable(query))
  }

  def members(id: Option[String] = None) = {
    base
      .withPathParts(restPath(true, "members", id))
    //.withQueryString(QueryString.fromTraversable(query))
  }

  def newRole() = {
    base
      .withPathParts(rpcPath(true, "new-role"))
  }

  def records(ver: Option[String],
              offset: Int = 0,
              size: Int = -1,
              filter: Seq[(Option[String], String)] = Seq(),
              sort: Option[String] = None,
              fields: Seq[String] = Seq(),
              format: Option[String] = None,
              inline: Option[Boolean] = None,
              quick: Option[Boolean] = None,
              curator: Option[Boolean] = None) = {
    val resourceName = "records"
    val query: Seq[(String, String)] = Seq(ver.map {
      "ver" -> _
    }).flatten ++ Seq(
      s"page[offset]" -> offset.toString,
      s"page[size]" -> size.toString,
    ) ++
      quick.toList.map { v =>
        s"quick" -> v.toString
      } ++
      curator.toList.map { v =>
        s"curator" -> v.toString
      } ++
      inline.toList.map { v =>
        s"inline" -> v.toString
      } ++
      format.toList.map { v =>
        s"format" -> v
      } ++
      sort.toList.map { v =>
        s"sort" -> v
      } ++
      buildFilter(filter) ++
      fields.map { v =>
        s"fields[$resourceName]" -> v
      }
    base
      .withPathParts(restPath(true, resourceName))
      .withQueryString(QueryString.fromTraversable(query))
  }

  def postRecord(
    rec: Record,
    ver: Option[String],
    successCallback: Callback = Callback.empty,
    errorCallback: Throwable => Callback = Callback.throwException
  ): AsyncCallback[XMLHttpRequest] = {
    post(
      record(ver).toString(),
      Some("application/json"),
      successCallback = successCallback,
      errorCallback = errorCallback
    )(
      SingleResponse(Some(Data(`type` = "records", attributes = rec))).asJson.noSpaces
    )
  }

  def patchRecordWithLink(
    rec: Record,
    ver: Option[String],
    successCallback: Callback = Callback.empty,
    errorCallback: Throwable => Callback = Callback.throwException
  ): AsyncCallback[XMLHttpRequest] = {
    post(
      patchLink(ver).toString(),
      Some("application/json"),
      successCallback = successCallback,
      errorCallback = errorCallback
    )(rec.asJson.noSpaces)
  }

  def numItems(ver: Option[String], searchQuery: String) =
    records(ver, 0, 0, filter = Seq(None -> searchQuery), quick = Some(false))

  protected def buildFilter(filter: Seq[(Option[String], String)]) = {
    val preFilter = if (prefilter.nonEmpty) Seq(None -> prefilter) else Seq()
    (preFilter ++ filter).map {
      case (specif, value) =>
        val specifier = specif match {
          case Some(s) =>
            s"[$s]"
          case None => ""
        }
        val nativeReg = "^\\{.+\\}$".r
        val v =
          if (nativeReg.matches(value))
            value
          else
            s"'$value'"
        s"filter$specifier" -> v
    }
  }

  def resources(ver: Option[String],
                filter: Seq[(Option[String], String)] = Seq(),
                attr: Option[String] = None,
                mode: Option[String] = None,
                keys: Option[String] = None) = {
    val query: Seq[(String, String)] = Seq(ver.map {
      "ver" -> _
    }).flatten ++
      buildFilter(filter) ++ Seq(attr.map {
      "attr" -> _
    }, mode.map {
      "mode" -> _
    }, keys.map {
      "keys" -> _
    }).flatten
    base
      .withPathParts(rpcPath(true, "resources"))
      .withQueryString(QueryString.fromTraversable(query))
  }

  def resource(ver: Option[String],
               id: String,
               attr: Option[String] = None,
               keys: Option[String] = None) = {
    val query: Seq[(String, String)] = Seq(ver.map {
      "ver" -> _
    }).flatten ++
      Seq(attr.map {
        "attr" -> _
      }, keys.map {
        "keys" -> _
      }).flatten
    base
      .withPathParts(rpcPath(true, "resources", Some(id)))
      .withQueryString(QueryString.fromTraversable(query))
  }

  def record(ver: Option[String],
             id: Option[String] = None,
             format: Option[String] = None,
             inline: Option[Boolean] = None) = {
    val query: Seq[(String, String)] = Seq(ver.map {
      "ver" -> _
    }).flatten ++
      inline.toList.map { v =>
        s"inline" -> v.toString
      } ++
      format.toList.map { v =>
        s"format" -> v
      }
    base
      .withPathParts(restPath(true, "records", id))
      .withQueryString(QueryString.fromTraversable(query))
  }

  def patchLink(ver: Option[String]) = {
    val query: Seq[(String, String)] = Seq(ver.map {
      "ver" -> _
    }).flatten
    base
      .withPathParts(rpcPath(true, "patch-link"))
      .withQueryString(QueryString.fromTraversable(query))
  }

  def graph(ver: Option[String],
            xys: Seq[(String, Option[String], String)],
            filter: Seq[(Option[String], String)] = Seq(),
            sort: Option[String] = None,
  ) = {
    val query: Seq[(String, String)] = Seq(ver.map {
      "ver" -> _
    }).flatten ++
      xys.map {
        case (name, specif, value) =>
          assert(!Seq("filter", "sort", "ver").contains(name))
          val specifier = specif match {
            case Some(s) =>
              s"[$s]"
            case None => ""
          }
          s"$name$specifier" -> value
      } ++
      sort.toList.map { v =>
        s"sort" -> v
      } ++
      buildFilter(filter)
    base
      .withPathParts(rpcPath(true, "graphs"))
      .withQueryString(QueryString.fromTraversable(query))
  }

  def updateDataset(ver: String, patch: Boolean = false) = {
    val query: Seq[(String, String)] =
      Seq("ver" -> ver, "patch" -> patch.toString)

    base
      .withPathParts(rpcPath(true, "update-dataset"))
      .withQueryString(QueryString.fromTraversable(query))
  }

  def updateDatasetStatus(ver: String) = {
    val query: Seq[(String, String)] = Seq("ver" -> ver)

    base
      .withPathParts(rpcPath(true, "update-dataset/status"))
      .withQueryString(QueryString.fromTraversable(query))
  }

  def updateDatasetErrorsSpreadsheet(ver: String) = {
    val query: Seq[(String, String)] = Seq("ver" -> ver)

    base
      .withPathParts(rpcPath(true, "update-dataset/errors"))
      .withQueryString(QueryString.fromTraversable(query))
  }

  def updateDatasetEmptySpreadsheet(patch: Boolean) = {
    val query = Seq("method" -> (if (patch) "patch" else "push"))
    base
      .withPathParts(rpcPath(true, "update-dataset/empty-spreadsheet"))
      .withQueryString(QueryString.fromTraversable(query))
  }

  def datasetsGeneric() = {
    base
      .withPathParts(restPath(false, "datasets", None))
  }

  def datasets() = {
    base
      .withPathParts(restPath(false, "datasets", Some(dsName)))
  }

  def discover() = {
    //val query = Seq("auth" -> id.nonEmpty)
    base
      .withPathParts(rpcPath(false, "discover"))
    //.withQueryString(QueryString.fromTraversable(query))
  }

  def resolver(prefix: String) = {
    val query: Seq[(String, String)] =
      Seq("prefix" -> prefix)

    base
      .withPathParts(rpcPath(false, "resolver"))
      .withQueryString(QueryString.fromTraversable(query))
  }
}
