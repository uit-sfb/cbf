package no.uit.sfb.dataui.utils.comp

import japgolly.scalajs.react.component.Scala.BackendScope
import japgolly.scalajs.react.vdom.html_<^._
import japgolly.scalajs.react.{Callback, ScalaComponent}
import no.uit.sfb.facade.bootstrap.form.{FormControl, FormControlFeedback}
import no.uit.sfb.facade.bootstrap.inputGroup.{InputGroup, InputGroupText}

import scala.scalajs.js.{UndefOr, undefined, |}

/**
  * For some reason, uncontrolled elements are not suitable when working with movable items (controlled elements move properly, while uncontrolled elements get messed-up)
  * See here: https://reactjs.org/blog/2018/06/07/you-probably-dont-need-derived-state.html
  * In the meantime it is possible to use this wrapper essentially implementing the behavior of an uncontrolled FormControl.
  * NOTE: in React onChange is mapped to onInput. Here onChange will be trigger in the HTML normal meaning (i.e. at blurring)
  */
object UncontrolledFormControl {

  case class Props(
    value: String,
    onChange: String => Callback,
    id: UndefOr[String],
    placeholder: String,
    disabled: Boolean,
    textAreaLines: Option[Int] = None, //If None -> input, otherwise textarea with the defined number of lines (<- nb of lines not implemented yet)
    `type`: String = "",
    isInvalid: Boolean = false,
    isValid: Boolean = false,
    max: UndefOr[String] = undefined,
    min: UndefOr[String] = undefined,
  ) {
    assert(
      textAreaLines.getOrElse(1) >= 1,
      "textAreaLines should be greater than 0."
    )
  }

  case class State(internalValue: String)

  class Backend($ : BackendScope[Props, State]) {

    val update = (v: String) =>
      $.modState {
        _.copy(internalValue = v)
    }

    def render(p: Props, s: State): VdomElement = {
      FormControl(
        id = p.id,
        as = p.textAreaLines match {
          case Some(l) => "textarea"
          case None    => "input"
        },
        value = s.internalValue,
        placeholder = p.placeholder,
        disabled = p.disabled,
        onChange = e => update(e.currentTarget.value),
        onKeyDown = e =>
          if (e.keyCode == 13)
            Callback { e.target.blur() } else Callback.empty,
        //onFocus = _ => update(s.internalValue),
        onBlur =
          _ => //We send the update when the component is blurred, and only if the value changed
            if (p.value != s.internalValue) p.onChange(s.internalValue)
            else Callback.empty,
        `type` = p.`type`,
        isValid = p.isValid,
        isInvalid = p.isInvalid,
        max = p.max,
        min = p.min
      )()
    }
  }

  private lazy val component = ScalaComponent
    .builder[Props]
    .initialStateFromProps(p => State(p.value))
    .renderBackend[Backend]
    .componentDidUpdate(lf => {
      val p = lf.currentProps
      if (p.value != lf.prevProps.value)
        lf.modState(_ => State(p.value))
      else
        Callback.empty
    })
    .build

  def apply(value: String,
            id: UndefOr[String] = undefined,
            onChange: String => Callback,
            placeholder: String = "",
            disabled: Boolean = false,
            textAreaLines: Option[Int] = None,
            `type`: String = "",
            isInvalid: Boolean = false,
            isValid: Boolean = false,
            max: UndefOr[String | Int] = undefined,
            min: UndefOr[String | Int] = undefined) = {
    component(
      Props(
        value,
        onChange,
        id,
        placeholder,
        disabled,
        textAreaLines,
        `type`,
        isInvalid,
        isValid,
        max.map(_.toString),
        min.map(_.toString)
      )
    )
  }
}
