package no.uit.sfb.dataui.view

import japgolly.scalajs.react._
import japgolly.scalajs.react.component.Scala.BackendScope
import japgolly.scalajs.react.vdom.html_<^._
import no.uit.sfb.cbf.shared.config.AttributeDef
import no.uit.sfb.dataui.components.graph.{
  GenericGraph,
  Linear,
  Log,
  Percent,
  PresentationOption
}
import no.uit.sfb.dataui.utils.com.{DatabaseApi, ResourceUrl}
import no.uit.sfb.dataui.utils.graph.{DataSource, DataSourceBuilder}
import no.uit.sfb.facade.bootstrap.form.{
  Form,
  FormControl,
  FormGroup,
  FormLabel
}

import scala.util.Random

object GraphView {

  case class Props(dbApi: DatabaseApi,
                   resourceUrl: ResourceUrl,
                   ver: Option[String],
                   searchQuery: String,
                   attributes: Seq[AttributeDef] = Nil,
                   error: Throwable ~=> Callback)

  case class State(x: Option[DataSource],
                   ys: Seq[DataSource],
                   presentationOption: PresentationOption = Linear)

  class Backend($ : BackendScope[Props, State])
      extends ViewBackendLike[Props, State] {

    val changeXAttr =
      Reusable.fn(
        (xAttr: DataSource) =>
          $.modState((s, p) => {
            val dataSrcBuilder = DataSourceBuilder(p.attributes)
            s.copy(
              x = Some(xAttr),
              ys = Seq(dataSrcBuilder.buildY(">count")),
              presentationOption = Linear
            )
          })
      )

    val changeYAttr =
      Reusable.fn(
        (ys: Seq[DataSource]) =>
          $.modState(_.copy(ys = ys, presentationOption = Linear))
      )

    val changePresOpt =
      Reusable.fn((optRef: String) => {
        $.modState(_.copy(presentationOption = PresentationOption(optRef)))
      })

    def renderData(p: Props, s: State): VdomNode = {
      val dataSrcBuilder = DataSourceBuilder(p.attributes)
      //We modify the axis according to the presentation option wishes
      val graphDisplay =
        GenericGraph(
          p.dbApi,
          p.ver,
          p.searchQuery,
          s.x,
          s.ys,
          s.presentationOption,
          error = p.error
        )
      val xAxis = Form(inline = true)(
        FormGroup(s"xAxisSelect-${Random.alphanumeric.take(5).mkString("")}")(
          FormLabel(column = true)("X-axis"),
          FormControl(
            as = "select",
            onChange = (ev: ReactEventFromInput) =>
              changeXAttr(dataSrcBuilder.buildX(ev.target.value)),
            value = s.x
              .flatMap {
                _.attrRef
              }
              .getOrElse("")
          )(
            p.attributes
              .filter { attr =>
                Seq("cat", "num", "date").contains(attr.lType.primary)
              }
              .map(attr => <.option(^.value := attr.ref, attr.name)): _*
          )
        )
      )
      val yAxis = Form(inline = true)(
        FormGroup(s"yAxisSelect-${Random.alphanumeric.take(5).mkString("")}")(
          FormLabel(column = true)("Y-axis"),
          FormControl(
            as = "select",
            onChange = (ev: ReactEventFromInput) => {
              changeYAttr(Seq(dataSrcBuilder.buildY(ev.target.value)))
            },
            value = s.ys.headOption
              .map { attr =>
                attr.attrRef.getOrElse(s">${attr.method}")
              }
              .getOrElse("")
          )(
            (Seq(
              Some(">count" -> "Count"),
              if (s.x.exists { attr =>
                    Seq("num", "date").contains(attr.lType.primary)
                  })
                Some(">countC" -> "Cumulative count")
              else
                None,
            ).flatten ++
              p.attributes
                .filter { attr =>
                  Set("num", "cat").contains(attr.lType.primary) &&
                  s.x
                    .flatMap {
                      _.attrRef
                    }
                    .getOrElse("") != attr.ref //We prevent user from selecting X = Y
                }
                .map { attr =>
                  attr.ref -> attr.name
                })
              .map { case (ref, name) => <.option(^.value := ref, name) }: _*
          )
        ),
        FormGroup(s"yAxisFormat-${Random.alphanumeric.take(5).mkString("")}")(
          FormLabel(column = true)("Format"),
          FormControl(
            as = "select",
            onChange =
              (ev: ReactEventFromInput) => changePresOpt(ev.target.value),
            value = s.presentationOption.ref
          )({
            val tmp =
              if (s.x
                    .map {
                      _.lType.primary
                    }
                    .contains("cat") || s.ys.headOption
                    .map(_.method)
                    .getOrElse("") == "unwind") //It is likely to get value 0 with unwind -> ln(0) = NaN
                PresentationOption.all - Log
              else
                PresentationOption.all
            if (!Seq("count", "countC")
                  .contains(s.ys.headOption.map(_.method).getOrElse("")))
              tmp - Percent
            else tmp
          }.map { po =>
            <.option(^.value := po.ref, po.label)
          }.toSeq: _*)
        )
      )
      <.div(xAxis, yAxis, graphDisplay)
    }
  }

  private val component = ScalaComponent
    .builder[Props]
    .initialStateFromProps { p =>
      {
        val dataSrcBuilder = DataSourceBuilder(p.attributes)
        State(
          x = p.attributes
            .find(
              attr => Seq("cat", "num", "date").contains(attr.lType.primary)
            )
            .map { x =>
              dataSrcBuilder.buildX(x.ref)
            },
          ys = Seq(dataSrcBuilder.buildY(">count"))
        )
      }
    }
    .renderBackend[Backend]
    .build

  def apply(dbApi: DatabaseApi,
            resourceUrl: ResourceUrl,
            ver: Option[String],
            searchQuery: String,
            attributes: Seq[AttributeDef],
            error: Throwable ~=> Callback) =
    component(Props(dbApi, resourceUrl, ver, searchQuery, attributes, error))
}
