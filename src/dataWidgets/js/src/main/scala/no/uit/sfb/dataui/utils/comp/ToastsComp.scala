package no.uit.sfb.dataui.utils.comp

import japgolly.scalajs.react.vdom.html_<^._
import japgolly.scalajs.react._
import no.uit.sfb.facade.bootstrap.toast._

trait ToastLike {
  def id: String //Ids should be built to denote an equality
  def toastComp(deleteCallback: Callback): TagMod
}

trait StandardToastLike extends ToastLike {
  val id = toString
  def variant: String
  def autoHide: Boolean
  def header: String
  def message: String
  def toastComp(deleteCallback: Callback): TagMod =
    Toast(
      autoHide = if (autoHide) Some(15000) else None,
      onClose = _ => deleteCallback,
      show = true,
      variant = variant
    )(
      ToastHeader()(<.strong(header)),
      (if (message.nonEmpty) Option(ToastBody()(message)) else None)
    )
}

case class InfoToast(header: String, message: String = "")
    extends StandardToastLike {
  val variant = "Light"
  val autoHide = true
}
case class SuccessToast(header: String, message: String = "")
    extends StandardToastLike {
  val variant = "Success"
  val autoHide = true
}
case class WarningToast(header: String, message: String = "")
    extends StandardToastLike {
  val variant = "Warning"
  val autoHide = false
}
case class ErrorToast(header: String, message: String = "")
    extends StandardToastLike {
  val variant = "Danger"
  val autoHide = false
}

object ToastsComp {

  case class Props(toasts: Seq[ToastLike], deleteCallback: String => Callback)

  class Backend($ : BackendScope[Props, Unit]) {

    def render(p: Props): VdomNode = {
      <.div(
        Seq(
          ^.position.fixed,
          ^.zIndex := "1",
          ^.bottom := "0",
          ^.minWidth := "250px"
        ) ++
          p.toasts.map { t =>
            t.toastComp(p.deleteCallback(t.id))
          }: _*
      )
    }
  }

  private val component = ScalaComponent
    .builder[Props]
    .renderBackend[Backend]
    .build

  def apply(toasts: Seq[ToastLike], deleteCallback: String => Callback) =
    component.apply(Props(toasts, deleteCallback))
}
