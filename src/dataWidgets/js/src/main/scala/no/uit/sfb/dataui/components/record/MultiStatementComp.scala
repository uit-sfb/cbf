package no.uit.sfb.dataui.components.record

import japgolly.scalajs.react.{Callback, ScalaComponent}
import japgolly.scalajs.react.component.Scala.BackendScope
import japgolly.scalajs.react.vdom.html_<^._
import no.uit.sfb.cbf.shared.model.v1.nativ.Statement
import no.uit.sfb.cbf.shared.model.v1.nativ._
import no.uit.sfb.dataui.utils.edit.{AttributeConfig, DbDirectLink}
import no.uit.sfb.facade.GlyphiconExt
import no.uit.sfb.facade.bootstrap.Badge
import no.uit.sfb.facade.bootstrap.buttons.Button
import no.uit.sfb.facade.bootstrap.grid.{Col, Container, Row}
import no.uit.sfb.facade.bootstrap.overlay.{OverlayTrigger, Tooltip}

object MultiStatementComp {

  case class Props(stms: Seq[Statement],
                   displayAdditionalInfo: Boolean,
                   suffix: String,
                   editMode: Boolean,
                   attrConfig: Option[AttributeConfig],
                   editCallback: Int => (Int, ValueRef) => Callback,
                   insertCallback: Int => Int => Callback,
                   deleteCallback: Int => Int => Callback,
                   newLinkedRecordCallback: String => Callback)

  class Backend($ : BackendScope[Props, Unit]) {

    def render(p: Props): VdomElement = {
      //Since we already filtered out empty statements in RecordModal, we known that there should not be any "ghost" statements
      lazy val head = {
        if (p.attrConfig.exists { _.`type`.centralizedAdd })
          Seq()
        else if (p.editMode)
          Seq(Statement(Seq(ValueRef.empty)))
        else
          Seq(Statement(Seq(ValueRef("-", Seq()))))
      }
      val stms =
        if (p.stms.nonEmpty)
          p.stms
        else
          head
      val stmComp = (stm: Statement, idx: Int) =>
        if (p.editMode)
          EditStatementComp(
            stm,
            p.attrConfig,
            (idxSubValue, vr) => p.editCallback(idx)(idxSubValue, vr),
            p.insertCallback(idx),
            p.deleteCallback(idx),
            idx = idx
          )
        else
          StatementComp(stm, p.displayAdditionalInfo, p.suffix)
      <.div(
        <.div(stms.size match {
          case 0 =>
            <.div()
          case 1 =>
            stmComp(stms.head, 0)
          case _ =>
            <.div(stms.zipWithIndex.map {
              case (stm, idx) =>
                if (p.editMode)
                  stmComp(stm, idx): TagMod
                else
                  <.div(
                    Badge(variant = "light")(GlyphiconExt("Dash")),
                    " ",
                    stmComp(stm, idx)
                  )
            }: _*)
        }),
        if (p.editMode) {
          if (p.attrConfig.exists { _.`type`.centralizedAdd }) {
            val link = p.attrConfig.exists { _.`type`.useLinkButtons }
            Container()(
              Row()(
                Col()(
                  Button(
                    variant = "outline-primary",
                    size = "sm",
                    onClick = _ =>
                      p.insertCallback(Math.max(stms.size - 1, 0))(-1)
                  )(
                    OverlayTrigger(
                      overlay = Tooltip.text(
                        if (link) "Create link"
                        else "Add value"
                      )
                    )(
                      GlyphiconExt(
                        if (link) "AddLink"
                        else "Add"
                      )
                    )
                  )
                )
              )
            )
          } else <.div()
        } else {
          val isDirectLink = p.attrConfig.exists { _.`type`.centralizedAdd }
          if (isDirectLink)
            Button(
              variant = "outline-primary",
              size = "sm",
              onClick = _ =>
                p.attrConfig
                  .map { c =>
                    c.`type` match {
                      case DbDirectLink(targetDs) =>
                        p.newLinkedRecordCallback(targetDs)
                      case _ =>
                        //Should not happen
                        println(
                          s"Was expecting  an instance of DbLinkDirect, but got ${c.`type`}."
                        )
                        Callback.empty
                    }
                  }
                  .getOrElse(Callback.empty)
            )(
              OverlayTrigger(overlay = Tooltip.text("Create linked record"))(
                GlyphiconExt("NewFile")
              )
            )
          else
            <.div()
        }
      )
    }
  }

  private lazy val component = ScalaComponent
    .builder[Props]
    .renderBackend[Backend]
    .build

  def apply(stms: Seq[Statement],
            displayCuration: Boolean = true,
            suffix: String = "",
            editMode: Boolean = false,
            attrConfig: Option[AttributeConfig] = None,
            editCallback: Int => (Int, ValueRef) => Callback = _ =>
              (_, _) => Callback.empty,
            insertCallback: Int => Int => Callback = _ => _ => Callback.empty,
            deleteCallback: Int => Int => Callback = _ => _ => Callback.empty,
            newLinkedRecordCallback: String => Callback = _ => Callback.empty) =
    component(
      Props(
        stms,
        displayCuration,
        suffix,
        editMode,
        attrConfig,
        editCallback,
        insertCallback,
        deleteCallback,
        newLinkedRecordCallback
      )
    )
}
