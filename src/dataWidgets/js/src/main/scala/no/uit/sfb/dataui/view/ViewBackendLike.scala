package no.uit.sfb.dataui.view

import japgolly.scalajs.react.vdom.html_<^._
import no.uit.sfb.dataui.components.menu.MenuComp
import no.uit.sfb.dataui.utils.com.{DatabaseApi, ResourceUrl}
import no.uit.sfb.facade.bootstrap.grid.{Col, Container, Row}

trait ViewBackendLike[P <: { def resourceUrl: ResourceUrl }, S] {
  protected def menuItems(p: P, s: S): Seq[VdomNode] = Seq()

  protected def renderData(p: P, s: S): VdomNode

  def render(p: P, s: S): VdomNode = {
    import scala.language.reflectiveCalls
    val mItems = menuItems(p, s)
    //The fixed button was a bad idea is it prevented to click on any button located at the same level
    /*lazy val footer = <.div(
      //^.className := "fixed-bottom",
      <.small(^.className := "text-muted", "Powered by:"),
      <.br,
      <.a(
        ^.href := "https://elixir.no/organization/elixir-uit",
        ^.target.blank,
        ^.rel := "noopener noreferrer"
      )(
        <.img(
          ^.src := p.resourceUrl.absolute("/images/elixir_no.png"),
          ^.className := "img-link",
          ^.width := "80px"
        )
      )
    )*/
    Container(fluid = true)(
      Row()(Col(xs = 1)(MenuComp(mItems)), Col()(renderData(p, s)))
    )
  }
}
