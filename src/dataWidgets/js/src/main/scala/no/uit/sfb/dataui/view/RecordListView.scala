package no.uit.sfb.dataui.view

import io.circe.generic.extras.Configuration
import io.circe.generic.extras.auto._
import io.circe.parser.decode
import io.lemonlabs.uri.{QueryString, RelativeUrl, UrlPath}
import japgolly.scalajs.react.component.Scala.BackendScope
import japgolly.scalajs.react.vdom.html_<^._
import japgolly.scalajs.react._
import no.uit.sfb.cbf.shared.auth.UserInfo
import no.uit.sfb.cbf.shared.config.DatasetConfig
import no.uit.sfb.cbf.shared.config.ui.TabObject
import no.uit.sfb.cbf.shared.jsonapi.{Data, MultipleResponse}
import no.uit.sfb.cbf.shared.model.v1.nativ.Record
import no.uit.sfb.cbf.shared.model.v1.versioninfo.VersionInfo
import no.uit.sfb.dataui.components.menu
import no.uit.sfb.dataui.components.menu.{MenuItemComp, MenuItemSelectComp}
import no.uit.sfb.dataui.components.recordlist.RecordList
import no.uit.sfb.dataui.components.upload.UploadComp
import no.uit.sfb.dataui.utils.com.loader.CollSizeLoader
import no.uit.sfb.dataui.utils.com.{DatabaseApi, ResourceUrl}
import no.uit.sfb.facade.bootstrap.buttons.Button
import no.uit.sfb.facade.bootstrap.grid.{Col, Container, Row}
import no.uit.sfb.facade.bootstrap.modal.{Modal, ModalBody, ModalFooter}
import no.uit.sfb.facade.bootstrap.overlay.{OverlayTrigger, Tooltip}
import no.uit.sfb.facade.icon.Glyphicon
import no.uit.sfb.dataui.utils.comp.{Spin, ToastLike}
import no.uit.sfb.facade.GlyphiconExt

object RecordListView extends CollSizeLoader {
  private val pageSizes = Seq(10, 25, 50, 100, 250, 500)

  case class Props(datasetConfig: DatasetConfig,
                   dbApi: DatabaseApi,
                   resourceUrl: ResourceUrl,
                   ver: Option[String],
                   versions: Seq[VersionInfo],
                   userInfo: UserInfo,
                   isReleased: Boolean,
                   searchQuery: String,
                   error: Throwable ~=> Callback,
                   targetId: String = "",
                   targetDs: String = "",
  ) {
    //Do NOT simplify (it includes defaulting all attributes when empty)
    lazy val defaultAttrRefs =
      datasetConfig
        .tabObj(datasetConfig.tableAttributes)
        .attrs
        .map {
          _.ref
        }

    lazy val orderedAttributes = {
      val (defaults, notDefaults) = datasetConfig.attrs.partition { attr =>
        defaultAttrRefs.contains(attr.ref)
      }
      defaults.sortBy(ref => defaultAttrRefs.indexWhere(_ == ref.ref)) ++ notDefaults
    }
  }

  case class State(
    apiResp: Option[MultipleResponse[Record]] = None,
    pageOffset: Int = 0,
    totalFilteredItems: Option[Int] = None,
    totalUnfilteredItems: Option[Int] = None,
    pageSize: Int = pageSizes(1), //Attention, the default page size MUST match one of the values of availableSizes in SearchAndPaginationSettings
    attributesToShow: Seq[(String, String, Boolean)] = Seq(),
    modal: Option[String] = None,
    toasts: Seq[(String, ToastLike)] = Seq(),
    newRecord: Boolean = false
  )

  implicit val customConfig: Configuration = Configuration.default.withDefaults

  private def loadRecords(p: Props,
                          s: State): AsyncCallback[MultipleResponse[Record]] = {
    p.dbApi
      .get(
        p.dbApi
          .records(
            p.ver,
            s.pageOffset,
            s.pageSize,
            Seq(None -> p.searchQuery),
            quick = Some(true)
          )
          .toString(),
        contentType = Some("application/vnd.api+json")
      )
      .map { xhr =>
        decode[MultipleResponse[Record]](xhr.responseText).toTry.get
      }
  }

  class Backend(val $ : BackendScope[Props, State])
      extends ViewBackendLike[Props, State] {

    val setNewRecord = $.modState(_.copy(newRecord = true))
    val closeRecord = $.modState(_.copy(newRecord = false))

    val changePageSize =
      Reusable.fn((size: Int) => $.modState(_.copy(pageSize = size)))

    val changePageOffset =
      Reusable.fn((offset: Int) => $.modState(_.copy(pageOffset = offset)))

    val changeModal =
      Reusable.fn((modal: Option[String]) => $.modState(_.copy(modal = modal)))

    val changeAttrSelect =
      (attrRef: String, select: Boolean) => {
        $.modState(
          s =>
            s.copy(attributesToShow = s.attributesToShow.map {
              case (ref, name, _) if ref == attrRef => (ref, name, select)
              case (ref, name, sel)                 => (ref, name, sel)
            })
        )
      }

    def renderData(p: Props, s: State): VdomNode = {
      def renderRecords(data: Seq[Data[Record]],
                        links: Map[String, Option[String]],
                        meta: Map[String, String]): VdomElement = {
        lazy val tab = TabObject("", s.attributesToShow.collect {
          case (ref, _, true) =>
            p.datasetConfig.attr(ref)
        }.flatten)
        Container(fluid = true)(
          Row()(
            Col()(
              RecordList(
                RecordList.Props(
                  p.ver,
                  p.versions,
                  data,
                  id =>
                    RelativeUrl(
                      UrlPath(Seq(p.dbApi.dsName, "records", id)),
                      QueryString.fromTraversable(Seq("ver" -> p.ver)),
                      None
                    ).toString(),
                  tab,
                  pageSizes,
                  s.pageSize,
                  meta.get("itemStart").map {
                    _.toInt
                  },
                  meta.get("itemEnd").map {
                    _.toInt
                  },
                  s.totalFilteredItems,
                  s.totalUnfilteredItems,
                  links,
                  changePageOffset,
                  changePageSize,
                  p.dbApi,
                  p.resourceUrl,
                  p.datasetConfig,
                  p.userInfo,
                  s.newRecord,
                  closeRecord,
                  p.targetId,
                  p.targetDs
                )
              )
            )
          )
        )
      }

      s.apiResp match {
        case Some(resp) =>
          renderRecords(resp.data, resp.links, resp.meta)
        case None =>
          Spin()
      }
    }

    protected override def menuItems(p: Props, s: State): Seq[VdomNode] = {
      val columnSettingMenu = MenuItemSelectComp(
        "Attributes",
        Glyphicon("Columns"),
        s.attributesToShow,
        changeAttrSelect
      ): VdomNode

      val resourcesAttr = p.datasetConfig.resourceAttributesWithModes
      val resourceDownloads = resourcesAttr.flatMap {
        case (attr, modes) =>
          val uniqueMode = modes.size == 1
          modes.map { mode =>
            s"${attr.ref}@$mode" -> s"${attr.name}${if (uniqueMode) ""
            else s" ($mode)"}"
          }
      }
      val resourceDownloadsWithSeparator =
        if (resourceDownloads.nonEmpty)
          Seq("" -> "") ++ //Separator
            resourceDownloads
        else
          resourceDownloads
      val downloadMenu = MenuItemComp(
        "Download",
        Glyphicon("CloudDownload"),
        Seq("json" -> "Contextual (JSON)", "tsv" -> "Contextual (TSV)") ++
          resourceDownloadsWithSeparator,
        sel =>
          if (Seq("json", "tsv").contains(sel)) {
            Some(
              p.dbApi.proxyUrl(
                p.dbApi
                  .records(
                    p.ver,
                    filter = Seq(None -> p.searchQuery),
                    format = Some(sel),
                    inline = Some(false)
                  )
                  .toString()
              )
            )
          } else {
            val (ref, mode) = {
              val splt = sel.split('@')
              splt.init.mkString("@") -> splt.lastOption.getOrElse("default")
            }
            Some(
              p.dbApi.proxyUrl(
                p.dbApi
                  .resources(
                    p.ver,
                    Seq(None -> p.searchQuery),
                    Some(ref),
                    Some(mode)
                  )
                  .toString()
              )
            )
        }
      ): VdomNode

      val uploadModalName = p.ver match {
        case Some(v) => s"Upload to v$v"
        case None    => "Upload"
      }
      val uploadModal = Modal(
        animation = false,
        size = "lg",
        show = s.modal.contains("upload"),
        onHide = _ => changeModal(None)
      )(
        ModalBody()(<.h4(uploadModalName)),
        UploadComp(
          p.dbApi,
          p.ver.getOrElse("0.0"), //Just to have a value
          p.searchQuery,
          p.datasetConfig.releaseFromExternal,
          p.error
        ),
        ModalFooter()(
          Button(
            onClick = _ => changeModal(None),
            variant = "outline-secondary"
          )(
            OverlayTrigger(
              overlay = Tooltip
                .text("Closing this window will not affect any running task.")
            )("Close")
          )
        )
      )

      val addMenu =
        if (p.userInfo.is("writer")) {
          if (p.isReleased) {
            menu.MenuItemComp(
              "Add data",
              GlyphiconExt("Add"),
              callback = _ => Callback.empty,
              disabled = if (p.isReleased)
                Some(
                  "Creating a new record on a released version is not allowed."
                )
              else None
            ): VdomNode
          } else {
            MenuItemComp(
              "Add data",
              Glyphicon("Add"),
              Seq("singleRecord" -> "New record", "tsv" -> "Upload TSV"),
              callback = {
                case "singleRecord" => setNewRecord
                case "tsv"          => changeModal(Some("upload"))
                case _              => Callback.empty
              }
            ): VdomNode
          }
        } else
          <.div()

      val helpModal = Modal(
        animation = false,
        size = "lg",
        show = s.modal.contains("help"),
        onHide = _ => changeModal(None)
      )(
        ModalBody()(
          <.h4("Help"),
          <.hr,
          <.div(
            "Click on any line to display a detailed view of the selected record."
          ),
          <.div(
            "Shift + Click on any line to open a detailed view of the selected record."
          ),
          <.div(
            "CTRL + Click on any line to open a detailed view of the selected record in a new tab."
          ),
          <.br(),
          <.h6("Text search"),
          <.div(
            "Enter a term in the search field in order to search for records containing this term. The search is case-insensitive and may be slow as all the attributes are queried."
          ),
          <.br(),
          <.h6("Advanced search"),
          <.div(
            "Empty the search field and open the query builder by clicking on the loop button. The search is more precise and much quicker."
          ),
        ),
        ModalFooter()(Button(onClick = _ => changeModal(None))("Close"))
      )

      val helpMenu = menu.MenuItemComp(
        "Help",
        Glyphicon("Help"),
        callback = _ => changeModal(Some("help"))
      ): VdomNode

      Seq(
        columnSettingMenu,
        downloadMenu,
        addMenu,
        uploadModal,
        helpMenu,
        helpModal
      )
    }
  }

  private val component =
    ScalaComponent
      .builder[Props]
      .initialStateFromProps { p =>
        State(attributesToShow = {
          p.orderedAttributes
            .map { attr =>
              (attr.ref, attr.name, p.defaultAttrRefs.contains(attr.ref))
            }
        }, newRecord = p.targetId.nonEmpty)
      }
      .renderBackend[Backend]
      .componentDidMount(lf => {
        val p = lf.props
        val s = lf.state

        lazy val loadCollSizeAsync = loadColSize(p.dbApi, p.ver, p.searchQuery)
          .flatMap {
            case (oFilteredCount, oUnfilteredCount) =>
              lf.modStateAsync(
                _.copy(
                  totalFilteredItems = oFilteredCount,
                  totalUnfilteredItems = oUnfilteredCount
                )
              )
          }

        lazy val loadRecordAsync: AsyncCallback[Unit] = loadRecords(p, s)
          .flatMap { resp =>
            lf.modStateAsync(_.copy(apiResp = Some(resp)))
          }
          .handleError { err =>
            p.error(err).asAsyncCallback
          }
        loadCollSizeAsync.toCallback >> loadRecordAsync.toCallback
      })
      .componentDidUpdate(lf => {
        val p = lf.currentProps
        val s = lf.currentState

        lazy val loadCollSizeAsync = loadColSize(p.dbApi, p.ver, p.searchQuery)
          .flatMap {
            case (oFilteredCount, oUnfilteredCount) =>
              lf.modStateAsync(
                _.copy(
                  totalFilteredItems = oFilteredCount,
                  totalUnfilteredItems = oUnfilteredCount
                )
              )
          }

        lazy val loadRecordAsync: AsyncCallback[Unit] = loadRecords(p, s)
          .flatMap { resp =>
            lf.modStateAsync(_.copy(apiResp = Some(resp)))
          }
          .handleError { err =>
            p.error(err).asAsyncCallback
          }

        val conditionalLoadCol =
          if (lf.prevProps.ver == p.ver &&
              lf.prevProps.searchQuery == p.searchQuery)
            Callback.empty
          else
            loadCollSizeAsync.toCallback

        val conditionalLoadRecords =
          if (lf.prevProps.ver == p.ver &&
              lf.prevProps.searchQuery == p.searchQuery &&
              lf.prevState.pageOffset == s.pageOffset &&
              lf.prevState.pageSize == s.pageSize)
            Callback.empty
          else {
            //If we change the pageOffset or pageOffset is 0, we do the query as usual
            if (lf.prevState.pageOffset != s.pageOffset || s.pageOffset == 0)
              lf.modState(_.copy(apiResp = None)) >> loadRecordAsync.toCallback
            else //otherwise, we reset pageOffset and the reset will be feedback re-call componentDidUpdate and we will find ourselves in the case above.
              lf.modState(_.copy(pageOffset = 0))
          }
        conditionalLoadCol >> conditionalLoadRecords
      })
      .build

  def apply(p: Props) = component(p)
}
