package no.uit.sfb.dataui.utils

import no.uit.sfb.cbf.shared.config.DatasetConfig
import no.uit.sfb.cbf.shared.model.v1.nativ.{Record, Statement}

//Template resolver for virtual attributes
//Should only be used on Record object returned by the API (as they are expressed)
class VirtualAttributeResolver(config: DatasetConfig) {
  def resolve(key: String, rec: Record): Vector[Statement] = {
    val stms = rec.stm(key)
    if (stms.isEmpty) {
      config.virtualAttributes
        .find {
          _.ref == key
        }
        .toVector
        .flatMap { attr =>
          rec.resolveTemplate(attr.template.getOrElse("-")) map {
            Statement.apply
          }
        }
    } else
      stms
  }
}
