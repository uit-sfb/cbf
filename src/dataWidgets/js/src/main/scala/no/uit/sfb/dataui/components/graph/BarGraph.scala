package no.uit.sfb.dataui.components.graph

import japgolly.scalajs.react.{Callback, ~=>}
import japgolly.scalajs.react.vdom.html_<^._
import no.uit.sfb.cbf.shared.utils.GenericVersioning
import no.uit.sfb.dataui.utils.com.DatabaseApi
import no.uit.sfb.dataui.utils.graph.{GraphLike, PropsLike}
import no.uit.sfb.facade.d3.format.Format
import no.uit.sfb.facade.reactvis.obj.Point
import no.uit.sfb.facade.reactvis.series.VerticalBarSeries
import no.uit.sfb.facade.reactvis.{HorizontalGridLines, VerticalGridLines, _}

import scala.scalajs.js
import scala.scalajs.js.{undefined, |}

case class PropsBarGraph(
    dbApi: DatabaseApi,
    ver: Option[String],
    url: String,
    xAxis: String = "",
    xType: String = "ordinal",
    yAxis: String = "",
    yType: String = "linear",
    lineColor: String = "",
    opacity: Double = 0.0,
    scientificNotation: Boolean = false,
    percentNotation: Boolean = false,
    xSort: Option[String] = None, //Override data order
    height: Int = 600,
    error: Throwable ~=> Callback
) extends PropsLike

object BarGraph extends GraphLike[PropsBarGraph, Point] {

  def renderGraph(p: PropsBarGraph, s: State, b: Backend): VdomNode = {
    def fn(v: String) =
      if (p.percentNotation)
        Format(".2%")
          .asInstanceOf[js.Function1[Double | Int | String, String]](v)
      else
        v
    val graph = s.graph.get
    XYPlot(xType = p.xType,
           yType = p.yType,
           margin = js.Dynamic.literal(bottom = 120),
           height = p.height)(
      graph.allKeys.zipWithIndex.map {
        case (key, idx) =>
          val data = graph.data(key)
          val sortedData = p.xSort match {
            case Some(sort) =>
              data.sortWith { (a, b) =>
                val x = a._1.toString
                val y = b._1.toString
                sort match {
                  case "semver" =>
                    GenericVersioning.compare(x, y) < 0
                  case _ =>
                    if (x == "Unknown")
                      true
                    else
                      x < y
                }
              }
            case None =>
              data
          }
          VerticalBarSeries(
            data = sortedData.map {
              case (x, y) =>
                new Point(x, y, opacity = if (s.hoverValue.exists { v =>
                                                v.x == x || v.x0 == x
                                              } && s.hoverValue.exists { v =>
                                                v.y == y
                                              }) {
                  1.0
                } else
                  0.7, label = if (graph.allKeys.size > 1) key else undefined)
            },
            color = if (p.lineColor.nonEmpty) p.lineColor else Palette(idx),
            opacity = if (p.opacity != 0.0) p.opacity else undefined,
            onValueMouseOver =
              (datapoint, _) => b.onChangeHintValue(Some(datapoint)),
            onSeriesMouseOut = _ => b.onChangeHintValue(None)
          )()
      } ++ Seq(XAxis(p.xAxis, tickLabelAngle = -90, cutString = 16)(),
               YAxis(p.yAxis,
                     scientificNotation = p.scientificNotation,
                     percentNotation = p.percentNotation,
               )()) ++
        s.hoverValue.map { v =>
          Hint(v)(
            <.div(^.background := "black",
                  if (v.label.isDefined)
                    <.div(v.label)
                  else
                    <.div(),
                  <.div(s"${p.xAxis}: ${v.x.toString}"),
                  <.div(s"${p.yAxis}: ${fn(v.y.toString)}"))
          )
        }.toSeq: _*
    )
  }

  def apply(
      dbApi: DatabaseApi,
      ver: Option[String],
      xAxisVars: Tuple3[String, Option[String], String],
      yAxisVars: Tuple3[String, Option[String], String],
      filter: Tuple2[Option[String], String],
      sort: Option[String] = None,
      xAxis: String = "",
      xType: String = "ordinal",
      yAxis: String = "",
      yType: String = "linear",
      scientificNotation: Boolean = false,
      percentNotation: Boolean = false,
      forceY0: Boolean = false, //Force Y axis to include 0
      xSort: Option[String] = None, //Override data order
      height: Int = 600,
      error: Throwable ~=> Callback
  ) = {
    val urlAbs = dbApi
      .graph(ver,
        Seq(
               xAxisVars,
               yAxisVars
             ),
             filter = Seq(filter),
             sort = sort)
      .toString()
    component(
      PropsBarGraph(
        dbApi,
        ver,
        url = urlAbs,
        xAxis = xAxis,
        yAxis = yAxis,
        yType = yType,
        scientificNotation = scientificNotation,
        percentNotation = percentNotation,
        xSort = xSort,
        height = height,
        error = error
      ))
  }
}
