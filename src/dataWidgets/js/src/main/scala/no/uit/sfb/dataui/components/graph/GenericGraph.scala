package no.uit.sfb.dataui.components.graph

import japgolly.scalajs.react._
import japgolly.scalajs.react.component.Scala.BackendScope
import japgolly.scalajs.react.vdom.html_<^._
import no.uit.sfb.dataui.utils.com.DatabaseApi
import no.uit.sfb.dataui.utils.graph.DataSource
import org.scalajs.dom

import scala.util.{Failure, Success, Try}

object GenericGraph {

  case class Props(dbApi: DatabaseApi,
                   ver: Option[String],
                   searchQuery: String,
                   x: Option[DataSource],
                   ys: Seq[DataSource],
                   presentationOption: PresentationOption = Linear,
                   height: Int,
                   error: Throwable ~=> Callback)

  class Backend($ : BackendScope[Props, Unit]) {

    def render(p: Props): VdomNode = {
      p.ys.headOption match { //We have a look at the first one only for now
        case Some(yHead) =>
          //We modify the axis according to the presentation option
          val y = p.presentationOption.mod(yHead)
          Try {
            val gr: VdomNode = p.x match {
              case Some(x) =>
                x.lType.primary match {
                  case "cat" =>
                    BarGraph(
                      p.dbApi,
                      p.ver,
                      xAxisVars = ("x", x.attrRef, x.method),
                      yAxisVars = ("y_1", y.attrRef, y.method),
                      filter = None -> p.searchQuery,
                      xAxis = x.label,
                      yAxis = y.label,
                      //yType = s.presentationOption.yType, //logarithm doesn't work for graph
                      scientificNotation = !p.presentationOption.yAxisPercent,
                      percentNotation = p.presentationOption.yAxisPercent,
                      xSort = Some(x.lType.secondary),
                      height = p.height,
                      error = p.error
                    )
                  case "date" =>
                    LineGraph(
                      p.dbApi,
                      p.ver,
                      xAxisVars = ("x", x.attrRef, x.method),
                      yAxisVars = ("y_1", y.attrRef, y.method),
                      filter = None -> p.searchQuery,
                      xAxis = x.label,
                      xType = "time",
                      yAxis = y.label,
                      yType = p.presentationOption.yType,
                      scientificNotation = !p.presentationOption.yAxisPercent,
                      percentNotation = p.presentationOption.yAxisPercent,
                      height = p.height,
                      error = p.error
                    )
                  case "num" =>
                    LineGraph(
                      p.dbApi,
                      p.ver,
                      xAxisVars = ("x", x.attrRef, x.method),
                      yAxisVars = ("y_1", y.attrRef, y.method),
                      filter = None -> p.searchQuery,
                      xAxis = x.label,
                      xType = "linear",
                      yAxis = y.label,
                      yType = p.presentationOption.yType,
                      scientificNotation = !p.presentationOption.yAxisPercent,
                      percentNotation = p.presentationOption.yAxisPercent,
                      height = p.height,
                      error = p.error
                    )
                  case _ =>
                    throw new Exception(
                      s"Error: Attr type '${x.lType}' not supported."
                    )
                }
              case None =>
                <.span("Select a data source for X")
            }
            gr
          } match {
            case Success(g) => g
            case Failure(e) =>
              println(e.getMessage)
              <.span(s"An error occurred.")
          }
        case None =>
          <.span("Select a data source for Y")
      }
    }
  }

  private val component = ScalaComponent
    .builder[Props]
    .renderBackend[Backend]
    .build

  def apply(dbApi: DatabaseApi,
            ver: Option[String],
            searchQuery: String,
            x: Option[DataSource],
            ys: Seq[DataSource],
            presentationOption: PresentationOption = Linear,
            height: Int = (dom.window.innerHeight * 2 / 3).toInt,
            error: Throwable ~=> Callback) =
    component(
      Props(dbApi, ver, searchQuery, x, ys, presentationOption, height, error)
    )
}
