package no.uit.sfb.dataui.utils.com

object MapUrlBuilder {
  def apply(dbApi: DatabaseApi,
            ver: Option[String],
            searchQuery: String,
            attrs: Seq[String],
            latlonAttr: String): String = {
    val axis = Seq(
      ("x", Some("id"), "eachU"),
      ("y_latlon", Some(latlonAttr), "set")
    ) ++
      attrs.map { v =>
        (s"y_$v", Some(v), "set")
      }

    dbApi
      .graph(ver, axis, Seq(None -> searchQuery))
      .toString
  }
}
