package no.uit.sfb.dataui.components.recordlist

import io.lemonlabs.uri.AbsoluteUrl
import japgolly.scalajs.react.component.Scala.BackendScope
import japgolly.scalajs.react.vdom.html_<^._
import japgolly.scalajs.react.{
  Callback,
  ReactEventFromInput,
  ScalaComponent,
  ~=>
}
import no.uit.sfb.facade.bootstrap._
import no.uit.sfb.facade.bootstrap.form.{
  FormControl,
  Form,
  FormGroup,
  FormLabel
}
import no.uit.sfb.facade.bootstrap.grid.{Col, Container, Row}
import no.uit.sfb.facade.bootstrap.pagination.{PageItem, Pagination}

import scala.util.Random

object ControlComp {

  case class Props(availableSizes: Seq[Int],
                   pageSize: Int,
                   itemStart: Option[Int] = None,
                   itemEnd: Option[Int] = None,
                   totalFilteredItems: Option[Int] = None,
                   totalUnfilteredItems: Option[Int] = None,
                   links: Map[String, Option[String]],
                   changePageOffset: Int ~=> Callback,
                   changePageSize: Int ~=> Callback,
  )

  class Backend(bs: BackendScope[Props, Unit]) {
    def render(p: Props): VdomNode = {
      val pageSizeControl = Form(inline = true)(
        FormGroup(
          s"paginationSelectSize${Random.alphanumeric.take(5).mkString("")}"
        )(
          FormLabel(column = true)("Page size "),
          FormControl(
            as = "select",
            onChange = (ev: ReactEventFromInput) =>
              p.changePageSize(ev.target.value.toInt),
            value = p.pageSize.toString
          )(p.availableSizes.map(v => <.option(v)): _*)
        )
      )
      if (!p.availableSizes.contains(p.pageSize))
        println(
          s"The page size is not one of the available size (${p.availableSizes.mkString(", ")}). The page size will take the first value in the list, while the number of entries will be different."
        )

      def oIntToString(oInt: Option[Int]): String =
        oInt map {
          _.toString
        } getOrElse "-"

      val showingComp =
        if (p.totalFilteredItems.getOrElse(0) > 0) {
          val prefix =
            s"Showing records ${oIntToString(p.itemStart)} to ${oIntToString(p.itemEnd)} out of ${oIntToString(p.totalFilteredItems)}"
          val suffix =
            if (p.totalFilteredItems != p.totalUnfilteredItems)
              s"(total dataset count: ${oIntToString(p.totalUnfilteredItems)})"
            else
              ""
          <.div(prefix, <.br, suffix)
        } else
          <.p(s"No record matching this filter")
      val paginationComp = {
        val keys = Seq("first", "prev", "next", "last") map { key =>
          key -> p.links.get(key).flatten
        }
        Pagination()(keys.map {
          case (name, oUrl) =>
            val disabled = oUrl.isEmpty
            val onClick = {
              oUrl match {
                case Some(url) =>
                  val offset = AbsoluteUrl
                    .parse(url)
                    .query
                    .param("page[offset]")
                    .map {
                      _.toInt
                    }
                    .getOrElse(0)
                  p.changePageOffset(offset)
                case None =>
                  Callback.empty
              }
            }
            PageItem.special(name, disabled, _ => onClick)
        }: _*)
      }
      Container(fluid = true)(
        Row()(
          Col()(showingComp),
          Col()(
            <.div(
              ^.display.flex,
              ^.alignItems.center,
              ^.justifyContent.center,
              paginationComp
            )
          ),
          Col()(
            <.div(^.display.flex, ^.justifyContent.flexEnd, pageSizeControl)
          )
        )
      )
    }
  }

  private val component = ScalaComponent
    .builder[Props]
    .renderBackend[Backend]
    .build

  def apply(p: Props) = component(p)
}
