package no.uit.sfb.dataui.components.graph

import no.uit.sfb.dataui.utils.graph.DataSource

sealed trait PresentationOption {
  def ref: String
  def label: String
  def yType = "linear"
  def mod(axis: DataSource): DataSource = axis
  def yAxisPercent: Boolean = false
}
case object Linear extends PresentationOption {
  val ref = "lin"
  val label = "Linear"
}
case object Percent extends PresentationOption {
  val ref = "pc"
  val label = "Percent"
  override def mod(axis: DataSource): DataSource = {
    axis.copy(method = s"${axis.method}P")
  }
  override val yAxisPercent: Boolean = true
}
case object Log extends PresentationOption {
  val ref = "log"
  val label = "Logarithmic"
  override val yType = "log"
}

object PresentationOption {
  val all = Set(Linear, Percent, Log)
  def get(ref: String): Option[PresentationOption] = all.find(_.ref == ref)
  def apply(ref: String): PresentationOption = get(ref).getOrElse(Linear)
}
