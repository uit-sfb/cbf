package no.uit.sfb.dataui.utils.com

class ResourceUrl(url: String) {
  def absolute(pathOrUrl: String): String = {
    //If pathOrUrl is path
    if (pathOrUrl.startsWith("/"))
      s"$url$pathOrUrl"
    else
      pathOrUrl
  }
}

object ResourceUrl {
  def apply(resourceUrl: String) = {
    new ResourceUrl(
      if (resourceUrl.isEmpty) s"/versionedAssets"
      else resourceUrl
    )
  }
}
