package no.uit.sfb.dataui.widget

import japgolly.scalajs.react._
import japgolly.scalajs.react.component.Scala.BackendScope
import japgolly.scalajs.react.vdom.html_<^._
import no.uit.sfb.cbf.shared.auth.UserInfo
import no.uit.sfb.cbf.shared.config.DatasetConfig
import no.uit.sfb.dataui.components.mgmt.{
  MembersComp,
  SettingsComp,
  VersionsComp
}
import no.uit.sfb.dataui.components.settings.AttributesDescriptionComp
import no.uit.sfb.dataui.utils.com.loader.{ConfigLoader, UserInfoLoader}
import no.uit.sfb.dataui.utils.com.{
  DatabaseApi,
  ResourceUrl,
  ToasterLike,
  ToasterStateLike
}
import no.uit.sfb.dataui.utils.comp.{Login, Spin, ToastLike, ToastsComp}
import no.uit.sfb.facade.bootstrap.buttons._
import no.uit.sfb.facade.bootstrap.grid._
import no.uit.sfb.facade.bootstrap.navs._
import org.scalajs.dom
import no.uit.sfb.facade.icon.Glyphicon

import scala.scalajs.js.annotation.{JSExport, JSExportTopLevel}

@JSExportTopLevel("SettingsWidget")
object SettingsWidget extends ConfigLoader with UserInfoLoader {

  case class Props(dbApi: DatabaseApi,
                   resourceUrl: ResourceUrl,
                   loginUrl: Option[String],
                   logoutUrl: Option[String])

  case class State(datasetConfig: Option[DatasetConfig] = None,
                   userInfo: Option[UserInfo] = None,
                   tab: String = "settings",
                   toasts: Seq[ToastLike] = Seq())
      extends ToasterStateLike {
    def newToasts(ts: Seq[ToastLike]) =
      copy(toasts = ts).asInstanceOf[this.type]
  }

  class Backend(val $ : BackendScope[Props, State])
      extends ToasterLike[Props, State] {

    protected val changeTab =
      Reusable.fn(
        (tabName: String) =>
          $.modState((s: State, p: Props) => {
            s.copy(tab = tabName)
          })
      )

    protected def deleteCallback(p: Props) =
      CallbackTo {
        dom.window.prompt(
          s"""You are about to delete PERMANENTLY dataset '${p.dbApi.dsName}'.
           |Please confirm by copying '${p.dbApi.dsName}' in the box below and click 'OK':""".stripMargin
        ) == p.dbApi.dsName
      }.flatMap { proceed =>
        if (proceed)
          p.dbApi
            .delete(p.dbApi.datasets().toString, successCallback = Callback {
              dom.window.location.replace("/launcher")
            }, errorCallback = error)
            .toCallback
        else
          Callback.empty
      }

    def render(p: Props, s: State): VdomNode = {
      <.div(
        ToastsComp(s.toasts, deleteToast),
        Container(fluid = true)(
          Row()(
            Col()(
              <.h3(
                s.datasetConfig
                  .map { _.name }
                  .getOrElse(p.dbApi.dsName): String, //Type needed for som reason
                <.span(
                  ^.paddingLeft := "10px",
                  <.a(
                    ^.href := s"/${p.dbApi.dsName}/browser",
                    Glyphicon("ExternalLink")
                  )
                )
              )
            ),
            Col(2)(
              <.span(
                ^.textAlign.right,
                Login(p.loginUrl, p.logoutUrl).dropdownButton(s.userInfo)
              )
            )
          ),
          Row()(Col()((s.userInfo zip s.datasetConfig) match {
            case None =>
              Spin()
            case Some(ui -> dc) if ui.is("owner") =>
              <.div(
                Nav(variant = "tabs", onSelect = changeTab)(
                  NavItem()(
                    NavLink(
                      eventKey = "settings",
                      active = s.tab == "settings"
                    )("Settings")
                  ),
                  NavItem()(
                    NavLink(
                      eventKey = "releases",
                      active = s.tab == "releases"
                    )("Releases")
                  ),
                  NavItem()(
                    NavLink(eventKey = "members", active = s.tab == "members")(
                      "Members"
                    )
                  ),
                  NavItem()(
                    NavLink(eventKey = "delete", active = s.tab == "delete")(
                      "Delete dataset"
                    )
                  ),
                ),
                s.tab match {
                  case "settings" =>
                    SettingsComp(dc, p.dbApi, error)
                  case "releases" =>
                    VersionsComp(dc, p.dbApi, error)
                  case "members" =>
                    MembersComp(dc, p.dbApi, error)
                  case "delete" =>
                    <.div(
                      "Deleting a dataset will permanently remove all data, versions and configurations.",
                      <.br,
                      <.br,
                      Button(
                        variant = "danger",
                        onClick = _ => deleteCallback(p)
                      )("Delete dataset")
                    )
                }
              )
            case Some(ui -> dc) =>
              AttributesDescriptionComp(dc)
          }))
        )
      )
    }
  }

  private val component =
    ScalaComponent
      .builder[Props]
      .initialState(State())
      .renderBackend[Backend]
      .componentDidMount(lf => {
        val p = lf.props
        val loadConfigAsync =
          loadConfig(p.dbApi)
            .map { datasetConfigResp =>
              datasetConfigResp.data.get.attributes
            }
        val userInfoAsync = loadUserInfo(p.dbApi)
        loadConfigAsync
          .zip(userInfoAsync)
          .flatMap {
            case (cfg, ui) =>
              lf.modStateAsync(
                _.copy(datasetConfig = Some(cfg), userInfo = Some(ui))
              )
          }
          .handleError { err =>
            lf.backend.error(err).asAsyncCallback
          }
          .toCallback
      })
      .build

  def apply(apiEndpoint: String,
            accessToken: Option[String],
            loginUrl: Option[String],
            logoutUrl: Option[String],
            resourceUrl: String,
            dbName: String) =
    component(
      Props(
        DatabaseApi(apiEndpoint, accessToken, dbName),
        ResourceUrl(resourceUrl),
        loginUrl,
        logoutUrl
      )
    )

  @JSExport("renderInto")
  def renderInto(apiEndpoint: String,
                 accessToken: String,
                 loginUrl: String,
                 logoutUrl: String,
                 resourceUrl: String,
                 dbName: String,
                 elemId: String = "entrypoint") =
    apply(
      apiEndpoint,
      if (accessToken.isEmpty) None else Some(accessToken),
      if (loginUrl.isEmpty) None else Some(loginUrl),
      if (logoutUrl.isEmpty) None else Some(logoutUrl),
      resourceUrl,
      dbName
    ).renderIntoDOM(dom.document.getElementById(elemId))
}
