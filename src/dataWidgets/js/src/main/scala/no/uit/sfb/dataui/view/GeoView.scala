package no.uit.sfb.dataui.view

import japgolly.scalajs.react._
import japgolly.scalajs.react.vdom.html_<^._
import no.uit.sfb.cbf.shared.config.{AttributeDef, CustomType}
import no.uit.sfb.dataui.components.geo.GeoComp
import no.uit.sfb.dataui.components.geo.GeoComp.GeoSettings
import no.uit.sfb.dataui.utils.com.{DatabaseApi, ResourceUrl}
import no.uit.sfb.dataui.utils.graph.DataSourceBuilder
import no.uit.sfb.facade.bootstrap.form._

import scala.util.Random

object GeoView {

  case class Props(dbApi: DatabaseApi,
                   resourceUrl: ResourceUrl,
                   ver: Option[String],
                   searchQuery: String,
                   attributes: Seq[AttributeDef],
                   customTypes: Seq[CustomType],
                   error: Throwable ~=> Callback)

  case class State(x: String, y: String)

  class Backend($ : BackendScope[Props, State])
      extends ViewBackendLike[Props, State] {

    val changeXAttr =
      Reusable.fn((x: String) => $.modState(_.copy(x = x)))

    val changeYAttr =
      Reusable.fn((y: String) => $.modState(_.copy(y = y)))

    override def renderData(p: Props, s: State): VdomNode = {
      val dataSrcBuilder = DataSourceBuilder(p.attributes)
      val xAttr = p.attributes.find(_.ref == s.x).get
      val xAxis = dataSrcBuilder.buildX(s.x)
      val yAxis = dataSrcBuilder.buildY(s.y)
      val oct = p.customTypes.find { ct =>
        xAttr.lType == ct.lt
      }
      val geoSettings = GeoSettings(oct, p.resourceUrl)
      val mapDisplay =
        GeoComp(
          p.dbApi,
          p.resourceUrl,
          p.ver,
          p.searchQuery,
          xAxis,
          yAxis,
          geoSettings,
          p.error
        )
      val attrs = p.attributes.filter(_.lType.isSubtypeOf("cat:geo"))
      val xAxisComp =
        if (attrs.size <= 1) <.div()
        else
          Form(inline = true)(
            FormGroup(
              s"xAxisSelect-${Random.alphanumeric.take(5).mkString("")}"
            )(
              FormLabel(column = true)("Map"),
              FormControl(
                as = "select",
                onChange = (ev: ReactEventFromInput) =>
                  changeXAttr(ev.target.value),
                value = s.x
              )(attrs.map { attr =>
                <.option(^.value := attr.ref, attr.name)
              }: _*)
            )
          )
      val yAxisComp = Form(inline = true)(
        FormGroup(s"yAxisSelect-${Random.alphanumeric.take(5).mkString("")}")(
          FormLabel(column = true)("Data"),
          FormControl(
            as = "select",
            onChange = (ev: ReactEventFromInput) => changeYAttr(ev.target.value),
            value = yAxis.attrRef.getOrElse(s">${yAxis.method}")
          )(
            (Seq(Some(">count" -> "Count"), None).flatten ++
              p.attributes
                .filter { attr =>
                  Set("num").contains(attr.lType.primary)
                }
                .map { attr =>
                  attr.ref -> attr.name
                })
              .map {
                case (ref, name) =>
                  <.option(^.value := ref, name)
              }: _*
          )
        )
      )
      <.div(xAxisComp, yAxisComp, mapDisplay)
    }
  }

  private val component = ScalaComponent
    .builder[Props]
    .initialStateFromProps(p => {
      val x = p.attributes
        .find {
          _.lType.isSubtypeOf("cat:geo")
        }
        .map { _.ref }
        .get //We know that there is at least one otherwise we the widget would be disabled
      State(x, ">count")
    })
    .renderBackend[Backend]
    .build

  def apply(dbApi: DatabaseApi,
            resourceUrl: ResourceUrl,
            ver: Option[String],
            searchQuery: String,
            attributes: Seq[AttributeDef],
            customTypes: Seq[CustomType],
            error: Throwable ~=> Callback) =
    component(
      Props(
        dbApi,
        resourceUrl,
        ver,
        searchQuery,
        attributes,
        customTypes,
        error
      )
    )
}
