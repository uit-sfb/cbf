package no.uit.sfb.dataui.utils.comp

import japgolly.scalajs.react.vdom.html_<^._
import japgolly.scalajs.react.Callback
import no.uit.sfb.facade.bootstrap.form._

//not possible to display a VdomNode: only a string.
case class SelectOption(key: String, value: String, disabled: Boolean = false)

object SelectOption {
  def apply(key: String): SelectOption = SelectOption(key, key)
  def disabled(value: String): SelectOption = SelectOption("", value, true)
}

case class SelectField(
  value: String,
  options: Seq[SelectOption],
  label: String,
  labelWidth: Int = 0,
  //id: UndefOr[String] = undefined,
  smallPrint: String = "",
  info: String = "",
  disabled: Option[String] = None, //None: enabled, Some: disabled and str is displayed as popup (overriding tooltip field)
  onChange: String => Callback,
  error: Boolean = false
) extends FieldLike {
  val message = disabled
  val disabledFlag = disabled.nonEmpty
  val tooltip = info

  protected val fieldComp =
    FormSelect(
      //id = s"$selectId-type",
      value = value,
      disabled = disabledFlag,
      onChange = e => onChange(e.currentTarget.value),
      isInvalid = error
    )(options.map {
      case SelectOption(k, v, d) =>
        <.option(^.value := k, ^.disabled := d, v)
    }: _*)

  def apply() = render(label, tooltip, smallPrint)
}
