package no.uit.sfb.dataui.utils.comp

import japgolly.scalajs.react.vdom.html_<^._
import japgolly.scalajs.react.Callback
import no.uit.sfb.facade.bootstrap.form._
import no.uit.sfb.facade.bootstrap.grid.{Col, Row}

case class SwitchField(
                        checked: Boolean,
                        label: String,
                        labelWidth: Int = 0,
                        onChange: Boolean => Callback,
                        //id: UndefOr[String],
                        info: String = "",
                        smallPrint: String = "",
                        disabled: Option[String] = None, //None: enabled, Some: disabled and str is displayed as popup (overriding tooltip field)
) extends FieldLike {
  val message = disabled
  val disabledFlag = disabled.nonEmpty
  val tooltip = info

  protected val fieldComp = {
    if (inline)
      FormCheck(
        //id = p.id,
        checked = checked,
        disabled = disabledFlag,
        onChange = e => onChange(e.currentTarget.checked),
        `type` = "switch"
      )()
    else
      FormCheck()(
      FormCheckInput(
        //id = p.id,
        checked = checked,
        disabled = disabledFlag,
        onChange = e => onChange(e.currentTarget.checked),
        //`type` = "switch" //Cannot use a switch here
      )(),
        FormCheckLabel()(label, tooltipComp(tooltip))
      )
  }


  override def render(label: String, tooltip: String, smallPrint: String): VdomElement = {
    if (inline)
    //For some reason it doesn't align properly when using the column property of the FormLabel
    Row()(
      Col(md = labelColumnSize)(FormLabel()(label, tooltipComp(tooltip))),
      Col()(fieldComp, FormText()(smallPrint))
    )
    else
      fieldComp
  }

  def apply() = render(label, tooltip, smallPrint)
}
