package no.uit.sfb.dataui.components.filter

import japgolly.scalajs.react._
import japgolly.scalajs.react.vdom.html_<^._
import no.uit.sfb.cbf.shared.config.DatasetConfig
import no.uit.sfb.facade.bootstrap.Table
import no.uit.sfb.facade.bootstrap.buttons.Button
import no.uit.sfb.facade.bootstrap.grid.{Col, Container, Row}
import no.uit.sfb.facade.icon.Glyphicon

object FiltersComp {
  case class Props(datasetConfig: DatasetConfig,
                   filters: Array[Seq[String]],
                   upsertFilter: (Int, Seq[String]) => Callback,
                   removeFilter: Int => Callback)

  case class State(selected: Option[Int])

  class Backend($ : BackendScope[Props, State]) {

    def switchToFilter(idx: Option[Int]) = {
      $.modState(_.copy(selected = idx))
    }

    def addStatement(idx: Int)(stm: String): Callback = {
      $.props
        .flatMap { p =>
          val stms =
            if (p.filters.length <= idx) Seq()
            else p.filters(idx).filter { _.nonEmpty }
          p.upsertFilter(idx, stms :+ stm)
        }
    }
    def removeStatement(idx: Int)(stm: String): Callback = {
      $.props
        .flatMap { p =>
          val flt = p.filters(idx)
          p.upsertFilter(
            idx,
            flt
              .filter {
                _ != stm
              }
          )
        }
    }

    def render(p: Props, s: State) = {
      Container()(
        Row()(
          Col(sm = 12, lg = 8)(
            ConjunctionsComp(
              p.filters,
              switchToFilter,
              p.removeFilter,
              removeStatement,
              s.selected
            )
          ),
          Col()(s.selected match {
            case Some(idx) =>
              <.div(
                <.h4("Add statement"),
                StatementBuilderComp(p.datasetConfig, addStatement(idx))
              )
            case None =>
              <.em("Please select a filter.")
          })
        )
      )
    }
  }

  private val component = ScalaComponent
    .builder[Props]
    .initialStateFromProps { p =>
      State(if (p.filters.isEmpty) Some(0) else None)
    }
    .renderBackend[Backend]
    .build

  def apply(datasetConfig: DatasetConfig,
            filters: Array[Seq[String]],
            upsertFilter: (Int, Seq[String]) => Callback,
            removeFilter: Int => Callback) =
    component(Props(datasetConfig, filters, upsertFilter, removeFilter))
}
