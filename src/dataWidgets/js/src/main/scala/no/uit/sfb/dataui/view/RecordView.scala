package no.uit.sfb.dataui.view

import japgolly.scalajs.react.{
  Callback,
  CallbackTo,
  Reusable,
  ScalaComponent,
  ~=>
}
import io.circe.generic.auto._
import io.circe.syntax._
import japgolly.scalajs.react.component.Scala.BackendScope
import japgolly.scalajs.react.vdom.html_<^._
import no.uit.sfb.cbf.shared.auth.UserInfo
import no.uit.sfb.cbf.shared.config.DatasetConfig
import no.uit.sfb.cbf.shared.jsonapi.{Data, SingleResponse}
import no.uit.sfb.cbf.shared.model.v1.nativ.Record
import no.uit.sfb.cbf.shared.model.v1.versioninfo.VersionInfo
import no.uit.sfb.dataui.components.record.RecordMenuLike
import no.uit.sfb.dataui.utils.com.{DatabaseApi, ResourceUrl}
import no.uit.sfb.dataui.utils.com.loader.{
  RecordLoader,
  UserInfoLoader,
  VersionsLoader
}
import no.uit.sfb.dataui.utils.comp.Spin
import no.uit.sfb.facade.bootstrap.buttons.Button
import no.uit.sfb.facade.bootstrap.overlay.{OverlayTrigger, Tooltip}
import org.scalajs.dom

object RecordView extends RecordLoader with VersionsLoader with UserInfoLoader {

  case class Props(dbApi: DatabaseApi,
                   resourceUrl: ResourceUrl,
                   ver: Option[String],
                   recordId: String,
                   datasetConfig: DatasetConfig,
                   error: Throwable ~=> Callback)

  case class State(apiResp: Option[SingleResponse[Record]] = None,
                   versions: Seq[VersionInfo] = Seq(),
                   userInfo: UserInfo = UserInfo("anonymous"),
                   editMode: Boolean = false,
                   editRecord: Option[Record] = None) {
    val isLoaded = apiResp.flatMap {
      _.data
    }.nonEmpty

    lazy val hasErrors = editRecord.exists {
      _.attributes.exists { case (_, stm) => stm.hasError }
    }
  }

  class Backend(val $ : BackendScope[Props, State])
      extends ViewBackendLike[Props, State]
      with RenderVersionedRecordLike
      with RecordMenuLike {

    protected val changeEditMode =
      Reusable.fn(
        (editMode: Boolean) => $.modState(_.copy(editMode = editMode))
      )

    def updateRecord(update: Record) =
      $.modState(_.copy(editRecord = Some(update)))

    protected def renderData(p: Props, s: State): VdomNode = {
      <.div(s.apiResp match {
        case Some(sr) =>
          sr.data.map {
            _.attributes
          } match {
            case Some(record) =>
              val ver = sr.meta.get("recordVersion")
              <.div(
                renderVersionedRecord(
                  record,
                  p.dbApi,
                  p.datasetConfig,
                  s.versions.find(_._id == ver.getOrElse("")),
                  s.versions
                    .findLast {
                      !_.isDraft
                    }
                    .map {
                      _._id
                    },
                  s.editMode,
                  s.editRecord,
                  updateRecord
                ),
                if (s.editMode) {
                  <.div(
                    <.br,
                    <.hr,
                    <.div(
                      ^.textAlign.center,
                      Button(
                        variant = "outline-secondary",
                        onClick = _ =>
                          CallbackTo {
                            dom.window.confirm(
                              s"""Are you sure you would like to discard any change made to this record?""".stripMargin
                            )
                          }.flatMap { proceed =>
                            if (proceed)
                              changeEditMode(false)
                            else
                              Callback.empty
                        }
                      )("Cancel"),
                      <.span(^.paddingLeft := "20px"),
                      Button(
                        disabled = s.hasErrors,
                        onClick = _ =>
                          s.editRecord match {
                            case Some(rec) =>
                              p.dbApi
                                .postRecord(rec, p.ver, Callback {
                                  dom.window.location.reload()
                                }, e => Callback { dom.window.alert(e.getMessage) })
                                .toCallback
                            case None =>
                              Callback.empty
                        }
                      )(
                        OverlayTrigger(
                          overlay = Tooltip.text(
                            if (s.hasErrors)
                              "Please correct the reported errors before submitting the changes."
                            else ""
                          )
                        )("Save")
                      )
                    ),
                    <.br
                  )
                } else <.div()
              )
            case None =>
              <.div(s"Record '${p.recordId}' not found.")
          }
        case None =>
          Spin()
      })
    }

    protected override def menuItems(p: Props, s: State): Seq[VdomNode] = {
      lazy val oVersion = s.apiResp.flatMap { sr =>
        val ver = sr.meta.get("recordVersion")
        s.versions.find(_._id == ver.getOrElse(""))
      }
      lazy val isRelease = oVersion match {
        case Some(v) if !v.isDraft => true
        case _                     => false
      }
      menu(
        p.datasetConfig,
        p.dbApi,
        oVersion.map { _._id },
        p.recordId,
        s.isLoaded,
        isRelease,
        s.userInfo.is("writer") && !s.editMode,
        editCallback = changeEditMode(true)
      )
    }
  }

  private val component =
    ScalaComponent
      .builder[Props]
      .initialState {
        State()
      }
      .renderBackend[Backend]
      .componentDidMount(lf => {
        val p = lf.props
        val loadVersionsAsync =
          loadVersions(p.dbApi)
            .flatMap { versions =>
              lf.modStateAsync {
                _.copy(versions = versions)
              }
            }
            .handleError { err =>
              p.error(err).asAsyncCallback
            }
            .handleError { err =>
              p.error(err).asAsyncCallback
            }
        loadVersionsAsync.toCallback
      })
      .componentDidUpdate(lf => {
        val p = lf.currentProps
        val s = lf.currentState
        //To prevent infinite loops
        //We need to check for lf.prevState.versions == s.versions so that loadRecord will run after componentDidMount
        if (lf.prevProps.ver == p.ver && lf.prevProps.recordId == p.recordId && lf.prevState.versions == s.versions)
          Callback.empty
        else {
          val userInfoAsync = loadUserInfo(p.dbApi)
          val recordAsync = loadRecord(p.dbApi, p.ver, p.recordId)
          recordAsync
            .zip(userInfoAsync)
            .flatMap {
              case (rec, user) =>
                val oRec =
                  rec.data.map {
                    _.attributes
                  }
                lf.modStateAsync(
                  s =>
                    s.copy(
                      apiResp = Some(rec),
                      editRecord = oRec,
                      userInfo = user
                  )
                )
            }
            .handleError { err =>
              p.error(err).asAsyncCallback
            }
            .toCallback
        }
      })
      .build

  def apply(p: Props) = component(p)
}
