package no.uit.sfb.facade

import japgolly.scalajs.react.{Children, JsComponent}
import japgolly.scalajs.react.vdom.VdomElement
import no.uit.sfb.facade.icon.Glyphicon

import scala.collection.MapView
import scala.scalajs.js
import scala.scalajs.js.annotation.JSImport

object GlyphiconExt {

  class Props() extends js.Object

  @JSImport("react-icons/ai", "AiOutlineCloudUpload")
  @js.native
  object AiOutlineCloudUpload extends js.Object

  @JSImport("react-icons/ai", "AiOutlineExclamationCircle")
  @js.native
  object AiOutlineExclamationCircle extends js.Object

  @JSImport("react-icons/ai", "AiOutlineWarning")
  @js.native
  object AiOutlineWarning extends js.Object

  @JSImport("react-icons/ai", "AiOutlineLink")
  @js.native
  object AiOutlineLink extends js.Object

  @JSImport("react-icons/md", "MdPlaylistAdd")
  @js.native
  object MdPlaylistAdd extends js.Object

  @JSImport("react-icons/md", "MdOutlineAddLink")
  @js.native
  object MdOutlineAddLink extends js.Object

  @JSImport("react-icons/md", "MdOutlineLinkOff")
  @js.native
  object MdOutlineLinkOff extends js.Object

  @JSImport("react-icons/vsc", "VscDash")
  @js.native
  object VscDash extends js.Object

  @JSImport("react-icons/vsc", "VscNewFile")
  @js.native
  object VscNewFile extends js.Object

  private lazy val resources: MapView[String, VdomElement] =
    Map(
      "CloudUpload" -> AiOutlineCloudUpload,
      "Attention" -> AiOutlineExclamationCircle,
      "Warning" -> AiOutlineWarning,
      "Link" -> AiOutlineLink,
      "AddLink" -> MdOutlineAddLink,
      "RemoveLink" -> MdOutlineLinkOff,
      "AddValue" -> MdPlaylistAdd,
      "Dash" -> VscDash,
      "NewFile" -> VscNewFile,
    ).view.mapValues { obj =>
      val comp = JsComponent[Props, Children.Varargs, Null](obj)
      comp(new Props())()
    }

  def apply(glyph: String): VdomElement = {
    resources.getOrElse(glyph, Glyphicon(glyph))
  }
}
