package no.uit.sfb.dataui.components.record

import japgolly.scalajs.react.{Callback, ScalaComponent}
import japgolly.scalajs.react.component.Scala.BackendScope
import japgolly.scalajs.react.vdom.html_<^._
import no.uit.sfb.cbf.shared.model.v1.nativ.{Statement, ValueRef}
import no.uit.sfb.dataui.utils.edit.{AttributeConfig, Other, Template}
import no.uit.sfb.facade.GlyphiconExt
import no.uit.sfb.facade.bootstrap.buttons.{Button, ButtonGroup}
import no.uit.sfb.facade.bootstrap.grid.{Col, Container, Row}
import no.uit.sfb.facade.bootstrap.overlay.{OverlayTrigger, Tooltip}

object EditStatementComp {

  case class Props(
    statement: Statement,
    attrConfig: AttributeConfig,
    editCallback: (Int, ValueRef) => Callback, //Int is idxSubValue
    insertCallback: Int => Callback, //Int is idxSubValue
    deleteCallback: Int => Callback, //Int is idxSubValue
    isDependency: Boolean,
    isSubValue: Boolean,
    idx: Int
  )

  class Backend($ : BackendScope[Props, Unit]) extends RenderValueRefLike {
    def renderStm(p: Props): VdomElement = {
      lazy val dependencies = p.attrConfig.dependencies
      lazy val t =
        if (p.attrConfig.isTemplate) Template(p.attrConfig.template)
        else if (p.isSubValue) Other()
        else p.attrConfig.`type`
      lazy val vr = p.statement.obj.headOption.getOrElse(ValueRef.empty)
      lazy val subValues =
        if (p.statement.obj.isEmpty) Seq() else p.statement.obj.tail
      <.div(
        //All the operators should have the same .html method, so we just take the first one
        t.operators.headOption
          .map { op =>
            op.html(
              t.operands(vr),
              (field, newValue) =>
                p.editCallback(0, t.buildNewStatement(vr, field, newValue)), // We have already set the idx when passing the callback, but if not we assume it is 0
              t.tpe,
              t.placeholder,
              p.attrConfig.locked,
              suffix = p.attrConfig.suffix,
              err = vr.err
            ): VdomElement
          }
          .getOrElse(<.span()),
        Container()(
          (if (!p.isSubValue)
             dependencies
               .map {
                 case (stms, ac, cb) =>
                   val stm =
                     if (stms.size > p.idx)
                       stms(p.idx)
                     else
                       Statement.empty
                   Row()(
                     Col(sm = 4)(
                       <.span(
                         OverlayTrigger(
                           overlay = Tooltip
                             .text(
                               s"This attribute (${ac.ref}) is used in the template above."
                             )
                         )(GlyphiconExt("Link")),
                         s" ${ac.attrName}"
                       )
                     ),
                     Col()(
                       EditStatementComp(
                         stm,
                         Some(ac),
                         (_, v) => cb(p.idx, v),
                         _ => Callback.empty,
                         _ => Callback.empty,
                         true,
                         false,
                         p.idx
                       )
                     )
                   )
               } ++
               (if (!p.isDependency && !p.attrConfig.isTemplate) {
                  subValues.zipWithIndex.map {
                    case (vr, i) =>
                      val idxSubValue = i + 1 //Since we have already removed the head
                      Row()(
                        Col(sm = 1)(
                          OverlayTrigger(
                            overlay = Tooltip
                              .text(
                                s"A sub-value is an optional field that may be used to add information to the attribute value. It is not subject to type validation."
                              )
                          )(GlyphiconExt("Next"))
                        ),
                        Col()(
                          EditStatementComp(
                            Statement(Seq(vr)),
                            Some(p.attrConfig),
                            (_, v) => p.editCallback(idxSubValue, v),
                            _ => p.insertCallback(idxSubValue),
                            _ => p.deleteCallback(idxSubValue),
                            false,
                            true,
                            idx = p.idx
                          )
                        )
                      )
                  }
                } else Seq())
           else Seq()): _*
        )
      )
    }

    def render(p: Props): VdomElement = {
      lazy val localButtons =
        if (p.attrConfig.locked || p.isDependency)
          <.div()
        else
          ButtonGroup(size = "sm")(
            Seq(
              if (p.attrConfig.`type`.admitsSubValues)
                Some(
                  Button(
                    variant = "outline-primary",
                    onClick = _ => p.insertCallback(0) //If we are at Statement level, we use -1 to create the first sub-value, if we are at sub-value level, -1 is not used since the index has been set
                  )(
                    OverlayTrigger(overlay = Tooltip.text("Add sub-value"))(
                      GlyphiconExt("AddValue")
                    )
                  )
                )
              else None,
              if (!p.isSubValue && !p.attrConfig.`type`.centralizedAdd)
                Some(
                  Button(
                    variant = "outline-primary",
                    onClick = _ => p.insertCallback(-1)
                  )(
                    OverlayTrigger(
                      overlay = Tooltip.text(
                        if (p.attrConfig.`type`.useLinkButtons) "Create link"
                        else "Add value"
                      )
                    )(
                      GlyphiconExt(
                        if (p.attrConfig.`type`.useLinkButtons) "AddLink"
                        else "Add"
                      )
                    )
                  )
                )
              else None,
              Some(
                Button(
                  variant = "outline-danger",
                  onClick = _ => p.deleteCallback(0)
                )(
                  OverlayTrigger(
                    overlay = Tooltip.text(
                      if (p.attrConfig.`type`.useLinkButtons)
                        "Unlink (no data will be deleted)."
                      else "Delete"
                    )
                  )(
                    GlyphiconExt(
                      if (p.attrConfig.`type`.useLinkButtons) "RemoveLink"
                      else "Trash"
                    )
                  )
                )
              )
            ).flatten: _*
          )
      if (p.attrConfig.`type`.centralizedAdd && p.statement.obj.isEmpty)
        <.div()
      else
        Container()(Row()(Col(sm = 10)(renderStm(p)), Col()(localButtons)))
    }
  }

  private lazy val component = ScalaComponent
    .builder[Props]
    .renderBackend[Backend]
    .build

  def apply(statement: Statement,
            attrConfig: Option[AttributeConfig],
            editCallback: (Int, ValueRef) => Callback,
            addCallback: Int => Callback,
            deleteCallback: Int => Callback,
            isDependency: Boolean = false,
            isSubValue: Boolean = false,
            idx: Int = 0) =
    component(
      Props(
        statement,
        attrConfig.get,
        editCallback,
        addCallback,
        deleteCallback,
        isDependency,
        isSubValue,
        idx
      )
    )
}
