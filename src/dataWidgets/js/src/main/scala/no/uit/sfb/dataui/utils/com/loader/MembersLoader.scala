package no.uit.sfb.dataui.utils.com.loader

import io.circe.parser.decode
import japgolly.scalajs.react.AsyncCallback
import no.uit.sfb.dataui.utils.com.DatabaseApi
import io.circe.generic.extras.Configuration
import io.circe.generic.extras.auto._
import no.uit.sfb.cbf.shared.auth.UserInfo
import no.uit.sfb.cbf.shared.jsonapi.MultipleResponse

import scala.concurrent.duration.DurationInt

trait MembersLoader {
  protected def loadMembers(
    dbApi: DatabaseApi
  ): AsyncCallback[MultipleResponse[UserInfo]] = {

    implicit val customConfig: Configuration =
      Configuration.default.withDefaults
    dbApi
      .get(
        dbApi.members().toString,
        contentType = Some("application/vnd.api+json"),
        timeout = Some(15.seconds)
      )
      .map { xhr =>
        decode[MultipleResponse[UserInfo]](xhr.responseText).toTry.get
      }
  }
}
