package no.uit.sfb.dataui.utils.com.loader

import io.circe.generic.extras.auto._
import io.circe.parser.decode
import io.circe.generic.extras.Configuration
import japgolly.scalajs.react.AsyncCallback
import no.uit.sfb.cbf.shared.jsonapi.{MultipleResponse, SingleResponse}
import no.uit.sfb.cbf.shared.model.v1.nativ.Record
import no.uit.sfb.dataui.utils.com.DatabaseApi

trait RecordLoader {
  protected def loadRecord(
    dbApi: DatabaseApi,
    ver: Option[String],
    recordId: String
  ): AsyncCallback[SingleResponse[Record]] = {
    import io.circe.generic.extras.auto._

    implicit val customConfig: Configuration =
      Configuration.default.withDefaults

    dbApi
      .get(
        dbApi.record(ver, Some(recordId)).toString(),
        contentType = Some("application/vnd.api+json")
      )
      .map { xhr =>
        decode[SingleResponse[Record]](xhr.responseText).toTry.getOrElse {
          val mr = decode[MultipleResponse[Record]](xhr.responseText).toTry.get
          SingleResponse[Record](
            mr.data.headOption,
            mr.links,
            mr.errors,
            mr.meta
          )
        }
      }
  }
}
