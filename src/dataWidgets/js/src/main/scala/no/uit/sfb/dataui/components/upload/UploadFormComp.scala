package no.uit.sfb.dataui.components.upload

import io.circe.generic.extras.Configuration
import japgolly.scalajs.react.component.Scala.BackendScope
import japgolly.scalajs.react.vdom.html_<^._
import japgolly.scalajs.react.{Callback, ScalaComponent, ~=>}
import no.uit.sfb.dataui.utils.com.DatabaseApi
import no.uit.sfb.facade.bootstrap.buttons.Button
import no.uit.sfb.facade.bootstrap.form.{
  Form,
  FormCheck,
  FormCheckInput,
  FormCheckLabel,
  FormFile,
  FormGroup
}
import no.uit.sfb.facade.bootstrap.overlay.{OverlayTrigger, Tooltip}
import no.uit.sfb.facade.icon.Glyphicon

object UploadFormComp {

  implicit val customConfig: Configuration =
    Configuration.default.withDefaults

  case class Props(dbApi: DatabaseApi,
                   ver: String,
                   searchQuery: String,
                   locked: Boolean = false,
                   uploadFile: Boolean => Callback,
                   state: String = "")

  case class State(patch: Boolean = false)

  class Backend($ : BackendScope[Props, State]) {

    def changePatch(b: Boolean) = $.modState(_.copy(patch = b))

    def render(p: Props, s: State): VdomElement = {

      val form = {
        //Note: If ever preventDefaultCB seems to not work (and a POST is done leading to a CSRF error), it is because uploadFile is somehow crashing
        Form(onSubmit = e => {
          p.uploadFile(s.patch) >> e.preventDefaultCB
        })(
          FormGroup("inputFile")(
            FormFile(disabled = p.locked, label = "", required = true)()
          ),
          FormCheck()(
            FormCheckInput(
              checked = !s.patch,
              `type` = "radio",
              onClick = _ => changePatch(false)
            )(),
            FormCheckLabel()(
              <.span(
                "Create/Overwrite",
                <.span(
                  ^.paddingLeft := "8px",
                  OverlayTrigger(
                    overlay = Tooltip.text(
                      """Create new records or overwrite existing ones.
                        |Attention: when overwriting existing records, the TSV file needs to contain ALL the non-template attributes (otherwise they will be reset)!""".stripMargin
                    )
                  )(Glyphicon("Help"))
                )
              )
            )
          ),
          FormCheck()(
            FormCheckInput(
              checked = s.patch,
              `type` = "radio",
              onClick = _ => changePatch(true)
            )(),
            FormCheckLabel()(
              "Update existing",
              <.span(
                ^.paddingLeft := "8px",
                OverlayTrigger(
                  overlay = Tooltip.text(
                    """Update existing records.
                      |The 'id' column is required and ALL records must already exist.""".stripMargin
                  )
                )(Glyphicon("Help"))
              )
            )
          ),
          <.br,
          Button(`type` = "submit", disabled = p.locked)(
            s"Upload (${if (s.patch) "update" else "create"})"
          )
        )
      }

      val recommendation = <.div(
        "We recommend using ",
        <.a(
          ^.href := "https://openrefine.org/",
          ^.target.blank,
          ^.rel := "noopener noreferrer"
        )("OpenRefine"),
        " if your input file contains hundreds of entries or more."
      )
      val tsvWikiUrl =
        "https://gitlab.com/uit-sfb/cbf/-/wikis/Data/TSV-format"

      val prepareDataText = {
        if (p.state == "Failed") {
          <.div(
            "The previous submission failed and errors must be corrected (see ",
            <.a(
              ^.href := tsvWikiUrl,
              ^.target.blank,
              ^.rel := "noopener noreferrer"
            )("here"),
            " for the expected syntax). Please, download the ",
            <.a(
              ^.href := p.dbApi.proxyUrl(
                p.dbApi
                  .updateDatasetErrorsSpreadsheet(p.ver)
                  .toString
              )
            )("annotated spreadsheet"),
            " and fix the issues reported in the first column.",
            <.br,
            recommendation
          )
        } else {
          <.div(
            "Addition, update and deletion of entries is done by submitting a ",
            <.a(
              ^.href := tsvWikiUrl,
              ^.target.blank,
              ^.rel := "noopener noreferrer"
            )("TSV file"),
            ", which you can build from the following ways:",
            <.ul(
              <.li(
                <.div(
                  "Download an ",
                  <.a(
                    ^.href := p.dbApi.proxyUrl(
                      p.dbApi
                        .updateDatasetEmptySpreadsheet(false)
                        .toString
                    )
                  )("empty spreadsheet"),
                  " and fill it up with your data."
                )
              ),
              <.li(
                <.div(
                  "Download the ",
                  <.a(
                    ^.href := p.dbApi.proxyUrl(
                      p.dbApi
                        .records(
                          Some(p.ver),
                          filter = Seq(None -> p.searchQuery),
                          format = Some("tsv"),
                          inline = Some(false),
                          curator = Some(true)
                        )
                        .toString
                    )
                  )("minified dataset"),
                  " (filter is applied) and edit it as needed. ",
                  recommendation
                )
              )
            )
          )
        }
      }

      lazy val prepareData =
        <.div("Prepare data", prepareDataText)
      val uploadData = <.div("Upload data (.tsv or .tsv.gz)", form)
      <.ol(<.li(prepareData), <.li(uploadData))
    }
  }

  private lazy val component = ScalaComponent
    .builder[Props]
    .initialState(State())
    .renderBackend[Backend]
    .build

  def apply(dbApi: DatabaseApi,
            ver: String,
            searchQuery: String,
            locked: Boolean,
            uploadFile: Boolean => Callback,
            state: String) =
    component(Props(dbApi, ver, searchQuery, locked, uploadFile, state))
}
