package no.uit.sfb.dataui.components.menu

import japgolly.scalajs.react.component.Scala.BackendScope
import japgolly.scalajs.react.vdom.html_<^._
import japgolly.scalajs.react.{Callback, ScalaComponent}
import no.uit.sfb.facade.bootstrap.buttons.Button
import no.uit.sfb.facade.bootstrap.dropdown._
import no.uit.sfb.facade.bootstrap.overlay.{OverlayTrigger, Tooltip}

object MenuItemComp {

  case class Props(descr: String,
                   icon: VdomNode,
                   choices: Seq[(String, String)] = Seq(),
                   url: String => Option[String] = _ => None,
                   callback: String => Callback = _ => Callback.empty,
                   disabled: Option[String] = None)

  class Backend($ : BackendScope[Props, Unit]) {

    import no.uit.sfb.facade.OptionToUndefOr._

    def render(p: Props): VdomElement = {
      val tooltip = p.disabled match {
        case Some(msg) =>
          s"${p.descr}: $msg"
        case None =>
          p.descr
      }
      if (p.choices.isEmpty) {
        <.div(
          OverlayTrigger(overlay = Tooltip.text(tooltip))(
            Button(
              variant = "link",
              onClick = _ => p.callback(""),
              disabled = p.disabled.nonEmpty
            )(p.icon)
          )
        )
      } else {
        Dropdown(drop = "right")(
          DropdownToggle(variant = "link", id = s"${p.descr}-dropdown")(
            OverlayTrigger(overlay = Tooltip.text(tooltip))(p.icon)
          ),
          DropdownMenu()(p.choices.map {
            case ("", _) => //separator
              DropdownDivider()()
            case (ref, label) =>
              DropdownItem(
                disabled = p.disabled.nonEmpty,
                href = p.url(ref), //Either one or the other is used
                onClick = _ => p.callback(ref),
              )(label)
          }: _*)
        )
      }
    }
  }

  private lazy val component = ScalaComponent
    .builder[Props]
    .renderBackend[Backend]
    .build

  def apply(
    descr: String,
    icon: VdomNode,
    choices: Seq[(String, String)] = Seq(), //ref, name
    url: String => Option[String] = _ => None, //use url...
    callback: String => Callback = _ => Callback.empty, //... or callback
    disabled: Option[String] = None
  ) =
    component(Props(descr, icon, choices, url, callback, disabled))
}
