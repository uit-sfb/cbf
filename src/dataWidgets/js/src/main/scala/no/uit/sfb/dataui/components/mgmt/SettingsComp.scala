package no.uit.sfb.dataui.components.mgmt

import japgolly.scalajs.react.component.Scala.BackendScope
import japgolly.scalajs.react.vdom.html_<^._
import japgolly.scalajs.react.{Callback, CallbackTo, ScalaComponent, ~=>}
import no.uit.sfb.cbf.shared.config.DatasetConfig
import no.uit.sfb.dataui.components.settings.DatasetSettingsComp
import no.uit.sfb.dataui.utils.com.loader.{ConfigIdsLoader, ConfigLoader}
import no.uit.sfb.dataui.utils.com.DatabaseApi
import no.uit.sfb.dataui.utils.comp.{ButtonField, Spin}
import no.uit.sfb.facade.bootstrap.buttons.{Button, ButtonGroup}
import no.uit.sfb.facade.bootstrap.form.{Form, FormFile, FormGroup}
import no.uit.sfb.facade.bootstrap.modal.{Modal, ModalBody, ModalFooter}
import no.uit.sfb.facade.bootstrap.overlay.{OverlayTrigger, Tooltip}
import no.uit.sfb.facade.bootstrap.{Badge, Table}
import no.uit.sfb.facade.icon.Glyphicon
import org.scalajs.dom
import org.scalajs.dom.raw.HTMLInputElement

object SettingsComp extends ConfigLoader with ConfigIdsLoader {

  case class Props(datasetConfig: DatasetConfig,
                   dbApi: DatabaseApi,
                   error: Throwable ~=> Callback)

  case class State(configIds: Option[Seq[String]] = None,
                   configToShow: Option[DatasetConfig] = None,
                   modalId: Option[String] = None)

  class Backend(val $ : BackendScope[Props, State]) {

    protected lazy val closeModal = $.modState(_.copy(modalId = None))

    protected def show(id: String): Callback = {
      $.props.async.flatMap{
        p =>
          loadConfig(p.dbApi, Some(id))
      }.flatMap {
        dc =>
          $.modStateAsync(_.copy(configToShow = dc.data.map{_.attributes}, modalId = Some("view")))
      }.toCallback
    }

    protected lazy val edit: Callback = {
      $.modState(_.copy(modalId = Some("edit")))
    }

    protected lazy val upload: Callback = {
      $.modState(_.copy(modalId = Some("upload")))
    }

    protected def uploadFile(p: Props): Callback = {
      Callback.empty.flatMap { _ => //Needed otherwise, even though it is a def, the body is computed when the component is being mounted.
        p.dbApi
          .post(
            p.dbApi
              .configs()
              .toString,
            contentType = Some("application/json"),
            successCallback = Callback {
              dom.window.location.reload()
            },
            errorCallback = p.error
          ) {
            //Note: It was clumsy to work with FormData (new FormData(formElement) did not work)
            //In addition it created a multipart which was not really required since we only wanted to send one file
            //Instead we simply return the file alone.
            val elem = dom.document
              .getElementById("inputFile")
              .asInstanceOf[HTMLInputElement]
            //Alternatively:
            /*val elem = e.target
        .asInstanceOf[HTMLFormElement]
        .elements(0)
        .asInstanceOf[HTMLInputElement]*/
            elem.files(0)
          }
          .toCallback
      }
    }

    def render(p: Props, s: State): VdomElement = {
      val editModal = Modal(
        animation = false,
        backdrop = "static",
        keyboard = false,
        size = "xl",
        show = s.modalId.contains("edit"),
        onHide = _ => closeModal
      )(
        ModalBody()(
          DatasetSettingsComp(p.datasetConfig, p.dbApi, false, p.error, closeModal)
        ), /*ModalFooter()(
          Button(onClick = closeModal, variant = "outline-secondary")("Close")
        )*/
      )
      val viewModal = Modal(
        animation = false,
        size = "xl",
        show = s.modalId.contains("view"),
        onHide = _ => closeModal
      )(
        ModalBody()(
          s.configToShow match { case Some(dc) =>
            DatasetSettingsComp(dc, p.dbApi, true, p.error, closeModal)
          case None =>
            //Should not happen
              "Configuration missing."
          }
        ),
        ModalFooter()(
          Button(onClick = _ => closeModal, variant = "outline-secondary")("Close")
        )
      )
      val uploadModal = Modal(
        animation = false,
        size = "sm",
        show = s.modalId.contains("upload"),
        onHide = _ => closeModal
      )(
        ModalBody()(
          <.h4("Upload configuration"),
          Form(onSubmit = _.preventDefaultCB)(
            FormGroup("inputFile")(
              FormFile(label = "Config file", required = true)()
            )
          )
        ),
        ModalFooter()(
          Button(onClick = _ => closeModal, variant = "outline-secondary")("Close"),
          Button(onClick = _ => uploadFile(p) >> closeModal)("Import")
        )
      )
        s.configIds match {
          case None =>
            Spin()
          case Some(ids) =>
            <.div(
              uploadModal,
              viewModal,
              editModal,
              <.br,
              <.div(
                ^.textAlign.center,
                Button(onClick = _ => edit)(OverlayTrigger(overlay = Tooltip.text("Edit active configuration"))("Edit")),
                <.span(^.paddingLeft := "20px"),
                Button(variant = "outline-primary", onClick = _ => upload)(OverlayTrigger(overlay = Tooltip.text("Upload configuration file"))("Upload"))
              ),
              <.br,
              Table()(<.tbody(ids.map { id =>
                val active = p.datasetConfig._id == id
                <.tr(
                  <.td(id,
                    if (active) {
                      <.span(^.paddingLeft := "20px",
                      Badge(variant = "success")("Active"))
                    } else
                    <.div()
                  ),
                  <.td(
                    ButtonGroup()(
                      Seq[VdomNode](
                    ButtonField(variant = "primary", onClick = show(id),
                      icon = Glyphicon("View"),
                      info = "View"
                    ),
                    ButtonField(variant = "primary", onClick = Callback { dom.window.location.assign(p.dbApi.proxyUrl(p.dbApi.config(id, inline = false).toString)) },
                      icon = Glyphicon("DocumentDownload"),
                      info = "Download"
                    )) ++
                        (if (active) Seq[VdomNode]() else
                    Seq[VdomNode](ButtonField(variant = "danger", onClick = CallbackTo {
                      dom.window.prompt(
                        s"""You are about to revert the configuration to '$id'. This will delete any ulterior configurations.
                           |Please confirm by copying '$id' in the box below and click 'OK':""".stripMargin
                      ) == id
                    } flatMap { proceed =>
                      if (proceed) {
                        p.dbApi.get(p.dbApi.revertToConfig(id).toString, successCallback = Callback {
                          dom.window.location.reload()
                        }, errorCallback = p.error).toCallback
                      } else
                        Callback.empty
                    },
                      icon = Glyphicon("Revert"),
                      info = "Revert to this configuration"
                    )
                    )):_*)
                  )
                )
              }: _*))
            )
        }
    }
  }

  private lazy val component = ScalaComponent
    .builder[Props]
    .initialState(State())
    .renderBackend[Backend]
    .componentDidMount(lf => { //No need to reload in componentDidUpdate since the only parameter is p.dbApi, which is a constant
      val p = lf.props
      loadConfigIds(p.dbApi)
        .flatMap { ids =>
            lf.modStateAsync(_.copy(configIds = Some(ids)))
        }
        .handleError { err =>
          p.error(err).asAsyncCallback
        }
        .toCallback
    })
    .build

  def apply(datasetConfig: DatasetConfig,
            dbApi: DatabaseApi,
            error: Throwable ~=> Callback) =
    component(Props(datasetConfig, dbApi, error))
}
