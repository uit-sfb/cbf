package no.uit.sfb.dataui.utils.comp

import japgolly.scalajs.react.vdom.html_<^._

case class LabelField(label: String,
                      labelWidth: Int = 0,
                      //id: UndefOr[String],
                      disabled: Option[String] = None,
                      info: String = "",
                      smallPrint: String = "")
    extends FieldLike {
  val tooltip = info
  val message = disabled

  protected val fieldComp = {
    <.div()
  }

  def apply() = render(label, tooltip, smallPrint)
}
