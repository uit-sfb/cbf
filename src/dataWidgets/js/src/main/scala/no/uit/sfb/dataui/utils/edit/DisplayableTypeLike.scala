package no.uit.sfb.dataui.utils.edit

import japgolly.scalajs.react.{AsyncCallback, Callback, CallbackTo}
import no.uit.sfb.cbf.shared.config.DatasetConfig
import no.uit.sfb.cbf.shared.model.v1.nativ.{Record, Statement, ValueRef}
import no.uit.sfb.dataui.utils.com.DatabaseApi
import no.uit.sfb.dataui.utils.com.loader.{ConfigLoader, RecordLoader}
import org.scalajs.dom

import scala.util.Try

/**
  * Defines how a type should be displayed both,
  *   - in the record edit view (the first operator of the list is used)
  *   - in the filter builder pane (one type has multiple operators each having its own display)
  */
trait DisplayableTypeLike {
  def operators: Seq[OperatorLike]
  def tpe: String = ""
  def placeholder: String = ""
  def operands(vr: ValueRef): Map[String, String] = Map("" -> vr.`val`)
  def buildNewStatement(origValueRef: ValueRef,
                        field: String,
                        newVal: String): ValueRef = {
    //We reset iri and meta as they are now out of sync with the new value.
    //They will be repopulated when the updated record is submitted to the database.
    ValueRef(newVal).getOrElse(ValueRef.empty)
  }
  //If true, this type does not support sub-values
  def admitsSubValues: Boolean = true
  //If true, this type use centralized add button
  def centralizedAdd: Boolean = false
  //If true, this type uses link buttons
  def useLinkButtons: Boolean = false

  def insertStatement(
    ref: String,
    idx: Int
  )(draftRecord: Record, dbApi: DatabaseApi): AsyncCallback[Record] = {
    val stms = draftRecord.stm(ref)
    val newStatements =
      if (stms.isEmpty)
        Seq(Statement.empty, Statement.empty) //If the attr is not present, there is already one empty field displayed, so we add two instances otherwise it looks like nothing is happening in the UI
      else
        Seq(Statement.empty)
    val newStms = stms.take(idx + 1) ++ newStatements ++ stms.drop(idx + 1)
    val updatedRecord = draftRecord.addAttrs(Map(ref -> newStms), true)
    CallbackTo(updatedRecord).asAsyncCallback
  }

  def insertSubValue(ref: String, idx: Int, idxSubValue: Int)(
    draftRecord: Record
  ): AsyncCallback[Record] = {
    assert(idxSubValue >= 0, "Should not be called when idxSubValue < 0")
    val stms = draftRecord.stm(ref)
    val newStms = if (stms.size > idx) {
      val stm: Statement = stms(idx)
      val updatedStatement = stm.copy(
        obj =
          if (stm.obj.nonEmpty)
            (stm.obj.take(idxSubValue + 1) :+ ValueRef.empty) ++ stm.obj
              .drop(idxSubValue + 1)
          else Seq(ValueRef.empty, ValueRef.empty)
      )
      if (stms.isEmpty) Vector(updatedStatement)
      else (stms.take(idx) :+ updatedStatement) ++ stms.drop(idx + 1)
    } else
      Vector(Statement(Seq(ValueRef.empty, ValueRef.empty)))
    val updatedRecord = draftRecord.addAttrs(Map(ref -> newStms), true)
    CallbackTo(updatedRecord).asAsyncCallback
  }

  def deleteStatement(
    ref: String,
    idx: Int
  )(draftRecord: Record, dbApi: DatabaseApi): AsyncCallback[Record] = {
    val stms = draftRecord.stm(ref)
    val newStms = stms.take(idx) ++ stms.drop(idx + 1)
    val updatedRecord = draftRecord.addAttrs(Map(ref -> newStms), true)
    CallbackTo(updatedRecord).asAsyncCallback
  }

  def deleteSubValue(ref: String, idx: Int, idxSubValue: Int)(
    draftRecord: Record
  ): AsyncCallback[Record] = {
    assert(idxSubValue != 0, "Should not be called when idxSubValue == 0")

    val stms = draftRecord.stm(ref)
    val stm: Statement = stms(idx)
    val updatedStatement = stms(idx).copy(
      obj = stm.obj.take(idxSubValue) ++ stm.obj.drop(idxSubValue + 1)
    )
    val newStms = (stms.take(idx) :+ updatedStatement) ++ stms.drop(idx + 1)
    val updatedRecord = draftRecord.addAttrs(Map(ref -> newStms), true)
    CallbackTo(updatedRecord).asAsyncCallback
  }
}

object DisplayableTypeLike {
  def apply(ref: String,
            datasetConfig: DatasetConfig): Option[DisplayableTypeLike] = {
    datasetConfig.attr(ref).map { attr =>
      if (attr.lType.isSubtypeOf("date"))
        DateType
      else if (attr.lType.isSubtypeOf("num"))
        NumType
      else if (attr.lType.isSubtypeOf("cat:enum")) {
        val terms = {
          datasetConfig.customTypes.find(_.ref == attr.`type`).toSeq.flatMap {
            ct =>
              ct.kvp.get("terms").toSeq.flatMap {
                _.split("\\|")
              }
          }
        }
        EnumType(terms)
      } else if (attr.lType.isSubtypeOf("dblink:direct")) {
        val dsname = {
          datasetConfig.customTypes
            .find(_.ref == attr.`type`)
            .map { ct =>
              ct.lt.tertiary
            }
            .getOrElse("")
        }
        DbDirectLink(dsname)
      } else if (attr.lType.isSubtypeOf("latlon"))
        LatLon
      else {
        Other(attr.resolvers)
      }
    }
  }
}

trait DbLinkLike extends DisplayableTypeLike {
  def targetDs: String

  lazy val operators =
    Seq(ContainsLinkOp, EqOp, NeOp)
  override val admitsSubValues: Boolean = false
  override val useLinkButtons: Boolean = true

  def createLink(
    ref: String,
    targetId: Option[String] = None
  )(draftRecord: Record, dbApi: DatabaseApi): AsyncCallback[Record] =
    CallbackTo(draftRecord).asAsyncCallback
  override final def insertStatement(ref: String, idx: Int)(
    draftRecord: Record,
    dbApi: DatabaseApi
  ): AsyncCallback[Record] = createLink(ref)(draftRecord, dbApi)
  override final def insertSubValue(ref: String, idx: Int, idxSubValue: Int)(
    draftRecord: Record
  ): AsyncCallback[Record] = CallbackTo(draftRecord).asAsyncCallback
  def deleteLink(ref: String, idx: Int)(
    draftRecord: Record,
    dbApi: DatabaseApi
  ): AsyncCallback[Record] = CallbackTo(draftRecord).asAsyncCallback
  override final def deleteStatement(ref: String, idx: Int)(
    draftRecord: Record,
    dbApi: DatabaseApi
  ): AsyncCallback[Record] = deleteLink(ref, idx)(draftRecord, dbApi)
  override final def deleteSubValue(ref: String, idx: Int, idxSubValue: Int)(
    draftRecord: Record
  ): AsyncCallback[Record] = CallbackTo(draftRecord).asAsyncCallback
}

case class DbDirectLink(targetDs: String)
    extends DbLinkLike
    with RecordLoader
    with ConfigLoader {
  override val centralizedAdd: Boolean = true

  private def getTargetRecord(
    dbApi: DatabaseApi,
    parentDatasetName: String,
    parentId: String
  ): AsyncCallback[(Record, Option[String])] = {
    loadRecord(dbApi.copy(dsName = parentDatasetName), None, parentId)
      .map { res =>
        if (res.errors.nonEmpty)
          throw new Exception(res.errors.map { _.toString }.mkString("\n"))
        else
          res.data match {
            case Some(d) =>
              d.attributes -> res.meta.get("recordVersion")
            case None =>
              throw new Exception(
                s"Could not get record '$parentId' in dataset '$parentDatasetName'."
              )
          }
      }
  }

  private def getParentDbConfig(
    dbApi: DatabaseApi,
    parentDatasetName: String
  ): AsyncCallback[DatasetConfig] = {
    loadConfig(dbApi.copy(dsName = parentDatasetName), None)
      .map { res =>
        if (res.errors.nonEmpty)
          throw new Exception(res.errors.map { _.toString }.mkString("\n"))
        else
          res.data match {
            case Some(d) =>
              d.attributes
            case None =>
              throw new Exception(
                s"Could not get config for dataset '$parentDatasetName'."
              )
          }
      }
  }

  override def createLink(
    ref: String,
    targetId: Option[String] = None
  )(draftRecord: Record, dbApi: DatabaseApi): AsyncCallback[Record] = {
    def addLink(rec: Record,
                link: String,
                attrRef: String,
                throwIfExists: Boolean = false): Record = {
      val stms = rec.stm(attrRef).map { _.trimmed }.filter { !_.isEmpty }
      stms.find(_.simpleValue == link) match {
        case Some(_) =>
          //Duplicate
          if (throwIfExists)
            throw new Exception("Link already exists.")
          else
            rec
        case None =>
          val newStm = Statement(link)
          rec.addAttrs(Map(attrRef -> (stms :+ newStm).filter {
            !_.isEmpty
          }), true)
      }
    }

    getParentDbConfig(dbApi, targetDs)
      .map { _.getMatchingLinkAttrRef(dbApi.dsName) }
      .map {
        _.map { targetRef =>
          Try {
            draftRecord.getId
          }.getOrElse(
            throw new Exception(
              "Record id missing. You need to save the record first before you can create this link."
            )
          )
          targetRef
        }
      }
      .flatMap { oTargetRef =>
        CallbackTo {
          val target =
            if (targetId.nonEmpty)
              targetId
            else
              Option(
                dom.window.prompt(
                  "Enter the identifier of the record you want to link to.",
                  ""
                )
              )
          oTargetRef -> target
        }.asAsyncCallback
      }
      .flatMap {
        case (oTargetRef, Some(id)) =>
          val updatedTargetCallback = oTargetRef match {
            case Some(targetRef) =>
              getTargetRecord(dbApi, targetDs, id)
                .flatMap {
                  case (targetRecord, targetVersion) =>
                    val strippedTargetRecord =
                      Record(targetRecord.attrs.filter { m =>
                        Seq("id", targetRef)
                          .contains(m.keys.headOption.getOrElse(""))
                      })
                    val updatedTargetRecord =
                      addLink(
                        strippedTargetRecord,
                        draftRecord.getId, //Safe: we have already checked that it existed
                        targetRef
                      )
                    dbApi
                      .copy(dsName = targetDs)
                      .patchRecordWithLink(updatedTargetRecord, targetVersion)
                      .map { _ =>
                        ()
                      }
                }
            case None =>
              Callback(()).asAsyncCallback
          }
          if (targetId.nonEmpty)
            updatedTargetCallback.map { _ =>
              draftRecord
            } else
            updatedTargetCallback.map { _ =>
              addLink(draftRecord, id, ref, true)
            }
        case _ =>
          //User cancelled
          CallbackTo(draftRecord).asAsyncCallback
      }
  }

  override def deleteLink(
    ref: String,
    idx: Int
  )(draftRecord: Record, dbApi: DatabaseApi): AsyncCallback[Record] = {
    val id = draftRecord.stm(ref)(idx).simpleValue
    def removeLink(rec: Record, link: String, attrRef: String): Record = {
      rec.addAttrs(
        Map(
          attrRef -> rec
            .stm(attrRef)
            .filter(stm => stm.simpleValue != link && !stm.isEmpty)
        ),
        true,
        id == rec.getId //So that we don't end up with a ghost statement
      )
    }
    getParentDbConfig(dbApi, targetDs)
      .map { _.getMatchingLinkAttrRef(dbApi.dsName) }
      .flatMap {
        case Some(parentRef) =>
          getTargetRecord(dbApi, targetDs, id)
            .flatMap {
              case (parentRecord, parentVersion) =>
                val stripedParentRecord =
                  Record(parentRecord.attrs.filter { m =>
                    Seq("id", parentRef)
                      .contains(m.keys.headOption.getOrElse(""))
                  })
                val updatedParentRecord =
                  removeLink(stripedParentRecord, draftRecord.getId, parentRef)
                dbApi
                  .copy(dsName = targetDs)
                  .patchRecordWithLink(updatedParentRecord, parentVersion)
                  .map { _ =>
                    removeLink(draftRecord, id, ref)
                  }
            }
        case None =>
          throw new Exception(
            s"Could not find linked attribute for dataset '${dbApi.dsName}' in dataset '$targetDs'."
          )
      }
  }
}

case object DateType extends DisplayableTypeLike {
  lazy val operators =
    Seq(EqOp, NeOp, BeforeOp, BeforeOrEqOp, AfterOp, AfterOrEqOp)
  override def placeholder: String = "YYYY-MM-DD or YYYY-MM or YYYY"
}
case object NumType extends DisplayableTypeLike {
  lazy val operators =
    Seq(EqNumOp, NeNumOp, LtNumOp, LteNumOp, GtNumOp, GteNumOp)
  override val tpe = "number"
  override def placeholder: String = "Number"
}
case class EnumType(terms: Seq[String]) extends DisplayableTypeLike {
  lazy val operators = Seq(EqEnumOp(terms), NeEnumOp(terms))
}
case object LatLon extends DisplayableTypeLike {
  lazy val operators = Seq(LteGeoOp, GtGeoOp)
  override val tpe = "number"
  override def operands(vr: ValueRef): Map[String, String] = {
    val splt = vr.`val`.split(",", 2)
    if (splt.length == 2)
      Map("lat" -> splt(0), "long" -> splt(1))
    else
      Map("lat" -> "", "long" -> "")
  }
  override def buildNewStatement(origValueRef: ValueRef,
                                 field: String,
                                 newVal: String): ValueRef = {
    val newMap = operands(origValueRef) + (field -> newVal)
    val newLat = newMap.getOrElse("lat", "")
    val newLong = newMap.getOrElse("long", "")
    val newLatLong = s"$newLat,$newLong"
    super.buildNewStatement(origValueRef, "", newLatLong)
  }
}

case class Other(resolvers: Seq[String] = Seq()) extends DisplayableTypeLike {
  lazy val operators = {
    (if (resolvers.isEmpty)
       Seq(EqOp)
     else
       Seq(CiriOpLike(resolvers))) ++
      Seq(RegexpOp)
  }
  override def operands(vr: ValueRef): Map[String, String] = {
    vr.ciris.headOption match {
      case Some(ciri) =>
        Map("prefix" -> ciri.prefix, "" -> ciri.id)
      case None =>
        vr.otherValues.headOption match {
          case Some(o) if o.nonEmpty =>
            val splt = o.split(':')
            val prefix = splt.head //Size checked
            val suffix = splt.tail.mkString(":")
            Map("prefix" -> prefix, "" -> suffix)
          case _ =>
            Map("" -> vr.simpleValue)
        }
    }
  }
  override def buildNewStatement(origValueRef: ValueRef,
                                 field: String,
                                 newVal: String): ValueRef = {
    val newMap = operands(origValueRef) + (field -> newVal)
    val newPrefix = newMap.getOrElse("prefix", "")
    val newSuffix = newMap.getOrElse("", "")
    val newCiri =
      if (newPrefix.nonEmpty)
        s"§$newPrefix:$newSuffix"
      else
        newSuffix
    super.buildNewStatement(origValueRef, "", newCiri)
  }
}

case class Template(template: String) extends DisplayableTypeLike {
  lazy val operators = Seq(RegexpOp)
  override def operands(vr: ValueRef): Map[String, String] = {
    Map("" -> s"$template")
  }
}
