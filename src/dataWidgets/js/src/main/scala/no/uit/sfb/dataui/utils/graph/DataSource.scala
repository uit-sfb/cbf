package no.uit.sfb.dataui.utils.graph

import no.uit.sfb.cbf.shared.config.{AttributeDef, LineagedType}
import no.uit.sfb.cbf.shared.config.AttributeDef

case class DataSource(
  attrRef: Option[String], //Null in case of virtual attributes (count, countC)
  label: String,
  method: String,
  lType: LineagedType
)

case class DataSourceBuilder(attributes: Seq[AttributeDef] = Seq()) {
  def buildX(attrRef: String,
             methodOverride: Option[String] = None): DataSource = {
    attributes.find(_.ref == attrRef) match {
      case Some(attr) =>
        val defaultMeth = if (attr.lType.primary == "date") "mts" else "each"
        DataSource(
          Some(attrRef),
          attr.name,
          methodOverride.getOrElse(defaultMeth),
          attr.lType
        )
      case None =>
        throw new Exception(s"Error: Attr ref '$attrRef' does not exist.")
    }
  }

  def buildY(attrRef: String,
             methodOverride: Option[String] = None): DataSource = {
    def extendedName(name: String, meth: String) = {
      meth match {
        case "avg" =>
          s"$name (average)"
        case "unwind" =>
          s"$name (count)"
        case _ =>
          s"$name ($meth)"
      }
    }

    attrRef match {
      case ">count" =>
        DataSource(None, "Count", "count", LineagedType(""))
      case ">countC" =>
        DataSource(None, "Cumulative count", "countC", LineagedType(""))
      case ref =>
        attributes.find(_.ref == ref) match {
          case Some(attr) =>
            val attrPrimary =
              attributes.find(_.ref == attrRef).map {
                _.lType.primary
              }
            val defaultMeth =
              if (attrPrimary.getOrElse("") == "cat") "unwind" else "avg"
            val meth = methodOverride.getOrElse(defaultMeth)
            DataSource(
              Some(attrRef),
              extendedName(attr.name, meth),
              meth,
              attr.lType
            )
          case None =>
            throw new Exception(s"Error: Attr ref '$ref' does not exist.")
        }
    }
  }
}
