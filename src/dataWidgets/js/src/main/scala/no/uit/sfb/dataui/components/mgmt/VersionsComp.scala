package no.uit.sfb.dataui.components.mgmt

import io.circe.parser.decode
import io.circe.generic.extras.Configuration
import io.circe.generic.extras.auto._
import io.circe.syntax._
import japgolly.scalajs.react._
import japgolly.scalajs.react.component.Scala.BackendScope
import japgolly.scalajs.react.vdom.html_<^._
import no.uit.sfb.cbf.shared.config._
import no.uit.sfb.cbf.shared.jsonapi.Data
import no.uit.sfb.dataui.utils.com.loader.VersionsLoader
import no.uit.sfb.dataui.utils.com.DatabaseApi
import no.uit.sfb.dataui.utils.comp._
import no.uit.sfb.facade.bootstrap.buttons.{Button, ButtonGroup}
import no.uit.sfb.facade.bootstrap.form.{Form, FormControl, FormGroup}
import no.uit.sfb.facade.bootstrap.inputGroup.InputGroup
import no.uit.sfb.facade.bootstrap.modal.{Modal, ModalBody, ModalFooter}
import no.uit.sfb.facade.bootstrap.Table
import no.uit.sfb.dataui.utils.comp.Spin
import no.uit.sfb.facade.icon.Glyphicon
import org.scalajs.dom
import org.scalajs.dom.raw.HTMLInputElement
import japgolly.scalajs.react.Ref.Simple
import no.uit.sfb.cbf.shared.config.DatasetConfig
import no.uit.sfb.cbf.shared.jsonapi.SingleResponse
import no.uit.sfb.cbf.shared.model.v1.task.UpdateDsTaskStatus
import no.uit.sfb.cbf.shared.model.v1.versioninfo.VersionInfo
import no.uit.sfb.cbf.shared.version.CbfVersion
import org.scalajs.dom.html

object VersionsComp extends VersionsLoader {

  implicit val customConfig: Configuration =
    Configuration.default.withDefaults

  case class Props(datasetConfig: DatasetConfig,
                   dbApi: DatabaseApi,
                   error: Throwable ~=> Callback)

  case class State(versions: Option[Seq[VersionInfo]] = None,
                   versionDraft: Option[VersionInfo] = None,
                   reload: Boolean = false,
                   release: Boolean = false)

  class Backend(val $ : BackendScope[Props, State]) {

    private val ref: Simple[html.Input] = Ref[html.Input]

    protected def pullLock(p: Props, ver: String): AsyncCallback[String] = {
      p.dbApi
        .get(p.dbApi.updateDatasetStatus(ver).toString)
        .map { xhr =>
          decode[UpdateDsTaskStatus](xhr.responseText).toOption
            .map {
              _.state
            }
            .getOrElse("Unknown")
        }
    }

    protected lazy val closeModal = $.modState(_.copy(versionDraft = None))
    protected def setVersionDraft(vi: Option[VersionInfo]) = {
      $.modState(_.copy(versionDraft = vi))
    }
    protected def setVersionDraft(vi: Option[VersionInfo], create: Boolean) = {
      $.modState(_.copy(versionDraft = vi, release = create))
    }

    protected lazy val reload = $.modState(_.copy(reload = true))

    def render(p: Props, s: State): VdomElement = {
      lazy val externalPipelineMessage =
        "This dataset release procedure is driven by an external pipeline. It is therefore not possible to manually publish a release or create a draft."

      val modal = Modal(
        animation = false,
        size = "sm",
        show = s.versionDraft.nonEmpty,
        onHide = _ => closeModal
      )((s.versionDraft match {
        case Some(vi) =>
          Seq(
            ModalBody()(
              <.h4("Release details"),
              Form(onSubmit = _.preventDefaultCB)(
                FormGroup("inputEdit")(
                  TextField(
                    vi._id,
                    label = "ID",
                    disabled = Some(
                      "The release Id may not be modified after creation."
                    ),
                    onChange = _ => Callback.empty
                  )(),
                  TextField(
                    vi.label,
                    label = "Label",
                    info = "A label may be used instead of the release id.",
                    onChange = v =>
                      setVersionDraft(s.versionDraft.map {
                        _.copy(label = v)
                      })
                  )(),
                  TextField(
                    vi.description,
                    label = "Description",
                    info =
                      "Informational only. Recommended when the release is deprecated.",
                    onChange = v =>
                      setVersionDraft(s.versionDraft.map {
                        _.copy(description = v)
                      })
                  )(),
                  SwitchField(
                    vi.deprecated,
                    label = "Deprecated",
                    info = "Deprecated versions are hidden from regular users.",
                    onChange = v =>
                      setVersionDraft(s.versionDraft.map {
                        _.copy(deprecated = v)
                      })
                  )()
                )
              )
            ),
            ModalFooter()(
              Button(onClick = _ => closeModal, variant = "outline-secondary")(
                "Cancel"
              ),
              Button(
                onClick = _ =>
                  p.dbApi
                    .send(
                      p.dbApi.versions().toString,
                      if (s.release) "POST" else "PATCH",
                      Some("application/json"), //Do NOT use application/vnd.api+json here as the data is not transmitted for some reason.
                      successCallback =
                        if (s.release)
                          p.dbApi
                            .post(
                              p.dbApi.versions().toString,
                              Some("application/json"), //Do NOT use application/vnd.api+json here as the data is not transmitted for some reason.
                              successCallback = reload,
                              errorCallback = p.error
                            )(
                              SingleResponse[VersionInfo](
                                Some(
                                  Data(
                                    "versions",
                                    attributes = VersionInfo(
                                      CbfVersion(vi._id).incr.toString
                                    ),
                                  )
                                )
                              ).asJson.noSpaces
                            )
                            .toCallback
                        else
                          reload,
                      errorCallback = p.error
                    )(
                      SingleResponse[VersionInfo](
                        Some(
                          Data(
                            "versions",
                            attributes = vi,
                            id =
                              if (s.release) ""
                              else vi._id
                          )
                        )
                      ).asJson.noSpaces
                    )
                    .toCallback >> closeModal
              )("Save")
            )
          )
        case None =>
          Seq()
      }): _*)
      s.versions match {
        case None =>
          Spin()
        case Some(versions) =>
          val (drafts, releases) = versions.reverse.partition(_.isDraft)
          <.div(
            modal,
            <.h4("Drafts"),
            Table()(<.tbody(drafts.map {
              vi =>
                def proceed =
                  if (dom.window.confirm(
                        """Creating a release will permanently lock the current state of the dataset (further updates may still be done on a new version).
                                                          |Do you want to proceed?""".stripMargin
                      ))
                    setVersionDraft(Some(VersionInfo.now(vi._id)), true)
                      .runNow()
                <.tr(
                  <.td(vi._id),
                  <.td(
                    ^.textAlign.center,
                    ButtonGroup()(
                      Seq[VdomNode](
                        ButtonField(
                          variant = "primary",
                          onClick = pullLock(p, vi._id).map {
                            case st
                                if Seq("Starting", "Running").contains(st) =>
                              dom.window.alert(
                                "This version is still being worked on and can therefore not be released right now. See progress by visiting the dataset's page and click on 'Edit'."
                              )
                            case "Failed" =>
                              if (dom.window.confirm(
                                    "This version still has some errors (go to the dataset's page and click on 'Edit' to see them). Are you sure you want to ignore them?"
                                  ))
                                proceed
                            case _ =>
                              proceed
                          }.toCallback,
                          icon = Glyphicon("Send"),
                          info = "Publish",
                          disabled =
                            if (p.datasetConfig.releaseFromExternal)
                              Some(externalPipelineMessage)
                            else None
                        ),
                        ButtonField(variant = "danger", onClick = CallbackTo {
                          dom.window.confirm(
                            """Deleting a version will permanently delete any data it may contain.
                                  |Do you want to proceed?""".stripMargin
                          )
                        }.flatMap { proceed =>
                          if (proceed)
                            p.dbApi
                              .delete(
                                p.dbApi.versions(Some(vi._id)).toString,
                                successCallback = reload,
                                errorCallback = p.error
                              )
                              .toCallback
                          else
                            Callback.empty
                        }, icon = Glyphicon("Trash"), info = "Delete")
                      ): _*
                    )
                  )
                )
            }: _*)),
            if (p.datasetConfig.releaseFromExternal)
              <.em(externalPipelineMessage)
            else {
              val newVersionsOptions = {
                val branches: Seq[String] = releases.map { vi =>
                  CbfVersion(vi._id).branch
                }.distinct
                val draftBranches: Seq[String] = drafts.map { vi =>
                  CbfVersion(vi._id).branch
                }.distinct
                val undraftBranches = branches.filter {
                  !draftBranches.contains(_)
                }
                def findLatest(branch: String): Option[VersionInfo] = {
                  versions.findLast(vi => CbfVersion(vi._id).branch == branch)
                }
                undraftBranches
                  .flatMap { branch =>
                    findLatest(branch).map { _._id }
                  }
                  .map { latest =>
                    CbfVersion(latest).incr.toString
                  }
              }
              FormGroup("newVersionInput")(
                LabelField(
                  "New draft revision",
                  info =
                    """Draft revision creation is mostly an automatic process (a new draft is automatically created after a release).
                      | Only branches which do not already have a draft revision will appear in the list below.""".stripMargin
                )(),
                InputGroup()(
                  FormControl.uncontrolled(
                    ref,
                    as = "select",
                    //defaultValue = newVersionsOptions.head,
                    onChange = _ => Callback.empty
                  )(newVersionsOptions.map { <.option(_) }: _*),
                  ButtonField(
                    variant = "outline-primary",
                    icon = "Create",
                    onClick = CallbackTo {
                      val elem = dom.document
                        .getElementById("newVersionInput")
                        .asInstanceOf[HTMLInputElement]
                      val tmp = elem.value
                      //Reset value
                      //elem.value = elem.defaultValue //Is undefined for some reason (even when setting defaultValue. But it works fine without anyways)
                      tmp
                    } flatMap {
                      value =>
                        val exists = versions.map { _._id }.contains(value)
                        if (value.nonEmpty && !exists)
                          p.dbApi
                            .post(
                              p.dbApi.versions().toString,
                              Some("application/json"), //Do NOT use application/vnd.api+json here as the data is not transmitted for some reason.
                              successCallback = reload,
                              errorCallback = p.error
                            )(
                              SingleResponse[VersionInfo](
                                Some(
                                  Data(
                                    "versions",
                                    attributes = VersionInfo(value),
                                  )
                                )
                              ).asJson.noSpaces
                            )
                            .toCallback
                        else {
                          if (exists)
                            Callback {
                              dom.window
                                .alert(s"Release '$value' already exists.")
                            } else
                            Callback.empty
                        }
                    },
                    info = "New draft revision"
                  )
                )
              )
            },
            <.h4("Releases"),
            Table()(
              <.thead(
                <.tr(
                  <.th(LabelField("ID")()),
                  <.th(LabelField("Release date")()),
                  <.th(
                    LabelField(
                      "Label",
                      info = "A label may be used instead of the release id."
                    )()
                  ),
                  <.th(
                    LabelField(
                      "Description",
                      info = "Informational only. Recommended when the release is deprecated."
                    )()
                  ),
                  <.th(
                    LabelField(
                      "Deprecated",
                      info =
                        "Deprecated versions are hidden from regular users."
                    )()
                  ),
                  <.th(LabelField("Actions")())
                ),
              ),
              <.tbody(releases.map { vi =>
                <.tr(
                  <.td(vi._id),
                  <.td(vi.releaseDate),
                  <.td(vi.label),
                  <.td(vi.description),
                  <.td(
                    if (vi.deprecated)
                      <.div(^.textAlign.center, Glyphicon("Deprecated"))
                    else ""
                  ),
                  <.td(
                    ^.textAlign.center,
                    ButtonGroup()(
                      ButtonField(
                        variant = "primary",
                        onClick = setVersionDraft(Some(vi), false),
                        icon = Glyphicon("Edit"),
                        info = "Edit"
                      )
                    )
                  )
                )
              }: _*)
            )
          )
      }
    }
  }

  private lazy val component = ScalaComponent
    .builder[Props]
    .initialState(State())
    .renderBackend[Backend]
    .componentDidMount(lf => {
      val p = lf.props
      loadVersions(p.dbApi)
        .flatMap { versions =>
          lf.modStateAsync(_.copy(versions = Some(versions)))
        }
        .handleError { err =>
          p.error(err).asAsyncCallback
        }
        .toCallback

    })
    .componentDidUpdate(lf => { //We need to reload because of the reload boolean flag (data changed in database and need to be pulled again)
      val p = lf.currentProps
      val s = lf.currentState
      if (s.reload) {
        loadVersions(p.dbApi)
          .flatMap { versions =>
            lf.modStateAsync(_.copy(versions = Some(versions), reload = false))
          }
          .handleError { err =>
            p.error(err).asAsyncCallback
          }
          .toCallback
      } else
        Callback.empty
    })
    .build

  def apply(datasetConfig: DatasetConfig,
            dbApi: DatabaseApi,
            error: Throwable ~=> Callback) =
    component(Props(datasetConfig, dbApi, error))
}
