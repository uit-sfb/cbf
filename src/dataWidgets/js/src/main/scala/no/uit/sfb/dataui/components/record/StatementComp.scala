package no.uit.sfb.dataui.components.record

import japgolly.scalajs.react.ScalaComponent
import japgolly.scalajs.react.component.Scala.BackendScope
import japgolly.scalajs.react.vdom.html_<^._
import no.uit.sfb.cbf.shared.model.v1.nativ.{Evidence, Statement}
import no.uit.sfb.cbf.shared.model.v1.nativ._
import no.uit.sfb.facade.bootstrap._
import no.uit.sfb.facade.bootstrap.buttons.Button
import no.uit.sfb.facade.bootstrap.overlay.OverlayTrigger
import no.uit.sfb.facade.bootstrap.popover.{
  Popover,
  PopoverContent,
  PopoverTitle
}
import no.uit.sfb.facade.icon.Glyphicon

import scala.util.Random

object StatementComp {

  case class Props(statement: Statement,
                   displayAdditionalInfo: Boolean,
                   suffix: String)

  class Backend($ : BackendScope[Props, Unit]) extends RenderValueRefLike {
    protected def renderEvidenceButton(
      evi: Seq[Evidence]
    ): Option[VdomElement] = {
      if (evi.nonEmpty) {
        val comp =
          OverlayTrigger(
            overlay = Some(
              Popover(Random.alphanumeric.take(10).mkString(""))(
                PopoverTitle()("Evidences"),
                PopoverContent()(EvidenceComp(evi))
              )
            ),
            trigger = Seq("click")
          )(Button(variant = "link", size = "sm")(Glyphicon("Announcement")))
        Some(comp)
      } else None
    }

    def render(p: Props): VdomElement = {
      val value = {
        val sep = <.span(
          <.br,
          <.span(
            ^.paddingLeft := "15px",
            Badge(variant = "light")(Glyphicon("Next")),
            " "
          )
        )
        p.statement.obj.mkTagMod(sep) { vr =>
          renderValueRef(vr, p.suffix, p.displayAdditionalInfo)
        }
      }
      <.span(
        ^.whiteSpace.`pre-line`,
        value,
        " ",
        if (p.displayAdditionalInfo)
          renderEvidenceButton(p.statement.evi)
        else
          None
      )
    }
  }

  private lazy val component = ScalaComponent
    .builder[Props]
    .renderBackend[Backend]
    .build

  def apply(statement: Statement,
            displayCuration: Boolean = true,
            suffix: String = "") =
    component(Props(statement, displayCuration, suffix))
}
