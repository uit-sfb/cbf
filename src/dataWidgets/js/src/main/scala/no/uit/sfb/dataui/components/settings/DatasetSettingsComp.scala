package no.uit.sfb.dataui.components.settings

import io.circe.generic.auto._
import io.circe.syntax._
import japgolly.scalajs.react.component.Scala.BackendScope
import japgolly.scalajs.react.vdom.html_<^._
import japgolly.scalajs.react._
import no.uit.sfb.cbf.shared.config.DatasetConfig
import no.uit.sfb.cbf.shared.jsonapi.{Data, SingleResponse}
import no.uit.sfb.cbf.shared.config._
import no.uit.sfb.dataui.utils.com.DatabaseApi
import no.uit.sfb.facade.bootstrap.accordion._
import no.uit.sfb.facade.bootstrap.buttons.Button
import no.uit.sfb.facade.bootstrap.card._
import no.uit.sfb.facade.bootstrap.form._
import org.scalajs.dom

object DatasetSettingsComp {

  case class Props(datasetConfig: DatasetConfig,
                   dbApi: DatabaseApi,
                   onError: Throwable => Callback,
                   onCancel: Callback,
                   noEdit: Boolean)

  case class State(draft: DatasetConfig)

  protected def formatConfig(c: DatasetConfig,
                             dbName: String): DatasetConfig = {
    c.copy(
      name = if (c.name.nonEmpty) c.name else dbName,
      attrs = c.attrs
        .map { a =>
          a.copy(resolvers = a.resolvers.sorted)
        }
        .sortBy(_.ref),
      customResolvers = c.customResolvers.sortBy(_.prefix),
      customTypes = c.customTypes.sortBy(_.ref),
    )
  }

  class Backend($ : BackendScope[Props, State]) {

    val updateDraft = Reusable.fn(
      (newDraft: DatasetConfig) =>
        $.modState { (s, p) =>
          s.copy(draft = formatConfig(newDraft, p.dbApi.dsName))
      }
    )

    protected def uploadConfig(p: Props,
                               draft: DatasetConfig)(e: ReactFormEvent) = {
      val msg =
        SingleResponse[DatasetConfig](Some(Data("configs", attributes = draft)))
      p.dbApi
        .post(
          p.dbApi.configs().toString,
          contentType = Some("application/json"),
          successCallback = Callback {
            dom.window.location.reload()
          },
          errorCallback = p.onError,
        )(msg.asJson.noSpaces)
        .void
    }

    def render(p: Props, s: State): VdomElement = {
      Form(
        onSubmit =
          e => uploadConfig(p, s.draft)(e).toCallback >> e.preventDefaultCB,
        preventEnter = true
      )(
        <.div(
          Accordion(defaultActiveKey = "general")(
            Card()(
              AccordionToggle("general")(CardHeader()("General")),
              AccordionCollapse("general")(
                CardBody()(GeneralSettingsComp(s.draft, updateDraft, p.noEdit))
              )
            ),
            Card()(
              AccordionToggle("attributes")(CardHeader()("Attributes")),
              AccordionCollapse("attributes")(
                CardBody()(
                  AttributesSettingsComp(s.draft, updateDraft, p.noEdit)
                )
              )
            ),
            Card()(
              AccordionToggle("types")(CardHeader()("Custom types")),
              AccordionCollapse("types")(
                CardBody()(
                  CustomTypesSettingsComp(s.draft, updateDraft, p.noEdit)
                )
              )
            ),
            Card()(
              AccordionToggle("resolvers")(CardHeader()("Resolvers")),
              AccordionCollapse("resolvers")(
                CardBody()(
                  ResolversSettingsComp(s.draft, updateDraft, p.noEdit, p.dbApi)
                )
              )
            ),
            Card()(
              AccordionToggle("display")(CardHeader()("Display")),
              AccordionCollapse("display")(
                CardBody()(DisplaySettingsComp(s.draft, updateDraft, p.noEdit))
              )
            ),
            Card()(
              AccordionToggle("parsing")(CardHeader()("Parsing")),
              AccordionCollapse("parsing")(
                CardBody()(ParsingSettingsComp(s.draft, updateDraft, p.noEdit))
              )
            ),
          ),
        ),
        if (p.noEdit)
          <.div()
        else
          <.div(
            <.br,
            <.hr,
            <.div(
              ^.textAlign.center,
              Button(variant = "outline-secondary", onClick = _ => p.onCancel)(
                "Cancel"
              ),
              <.span(^.paddingLeft := "20px"),
              Button(`type` = "submit")("Submit")
            ),
            <.br
          )
      )
    }
  }

  private lazy val component = ScalaComponent
    .builder[Props]
    .initialStateFromProps(
      p => State(formatConfig(p.datasetConfig, p.dbApi.dsName))
    )
    .renderBackend[Backend]
    .build

  def apply(datasetConfig: DatasetConfig,
            dbApi: DatabaseApi,
            noEdit: Boolean,
            onError: Throwable => Callback,
            onCancel: Callback = Callback.empty,
  ) =
    component(Props(datasetConfig, dbApi, onError, onCancel, noEdit))
}
