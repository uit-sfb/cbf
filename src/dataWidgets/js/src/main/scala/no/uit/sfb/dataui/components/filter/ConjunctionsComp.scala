package no.uit.sfb.dataui.components.filter

import japgolly.scalajs.react._
import japgolly.scalajs.react.vdom.html_<^._
import no.uit.sfb.cbf.shared.config.DatasetConfig
import no.uit.sfb.facade.bootstrap.Table
import no.uit.sfb.facade.bootstrap.buttons.Button
import no.uit.sfb.facade.bootstrap.grid.{Col, Container, Row}
import no.uit.sfb.facade.bootstrap.overlay.{OverlayTrigger, Tooltip}
import no.uit.sfb.facade.icon.Glyphicon

object ConjunctionsComp {
  case class Props(filters: Array[Seq[String]],
                   switchToFilter: Option[Int] => Callback,
                   removeFilter: Int => Callback,
                   removeStatement: Int => String => Callback,
                   selected: Option[Int])

  class Backend($ : BackendScope[Props, Unit]) {

    def render(p: Props) = {
      <.div(
        Table(striped = false, bordered = false) {
          val flts =
            if (p.selected.getOrElse(-1) >= p.filters.length)
              p.filters :+ Seq()
            else
              p.filters
          <.tbody(flts.toIndexedSeq.zipWithIndex.map {
            case (flt, idx) =>
              <.tr(
                ^.onClick --> p.switchToFilter(Some(idx)),
                if (p.selected.getOrElse(-1) == idx) ^.className := "selected"
                else ^.className := "",
                <.td(
                  Container()(
                    Row()(
                      Col(className = "content_center")("AND"),
                      Col(sm = 10)(
                        DisjunctionsComp(flt, p.removeStatement(idx))
                      ),
                      Col(className = "content_center")(
                        OverlayTrigger(overlay = Tooltip.text("Delete filter"))(
                          Button(
                            size = "sm",
                            variant = "danger",
                            onClick = e =>
                              e.stopPropagationCB >> p
                                .switchToFilter(None) >> p.removeFilter(idx)
                          )(Glyphicon("Trash"))
                        )
                      )
                    )
                  )
                )
              )
          }: _*)
        },
        OverlayTrigger(
          overlay = Tooltip.text(
            "Create new filter. All filters are ANDed together to produce the final query."
          )
        )(
          Button(
            variant = "outline-primary",
            onClick = _ => p.switchToFilter(Some(p.filters.length))
          )("New filter")
        )
      )
    }
  }

  private val component = ScalaComponent
    .builder[Props]
    .renderBackend[Backend]
    .build

  def apply(filters: Array[Seq[String]],
            switchToFilter: Option[Int] => Callback,
            removeFilter: Int => Callback,
            removeStatement: Int => String => Callback,
            selected: Option[Int]) =
    component(
      Props(filters, switchToFilter, removeFilter, removeStatement, selected)
    )
}
