package no.uit.sfb.dataui.utils.comp

import japgolly.scalajs.react.component.Scala.BackendScope
import japgolly.scalajs.react.vdom.html_<^._
import japgolly.scalajs.react.{Callback, CallbackTo, ScalaComponent}
import no.uit.sfb.facade.bootstrap.buttons.Button
import no.uit.sfb.facade.bootstrap.form.{FormLabel, FormText}
import no.uit.sfb.facade.bootstrap.overlay.{OverlayTrigger, Tooltip}
import no.uit.sfb.facade.icon.Glyphicon

object ButtonField {

  case class Props(variant: String,
                   icon: VdomNode,
                   onClick: Callback,
                   //id: UndefOr[String],
                   disabled: Boolean,
                   tooltip: String)

  class Backend($ : BackendScope[Props, Unit]) {

    def render(p: Props): VdomNode = {
      Button(
        variant = p.variant,
        size = "sm",
        disabled = p.disabled,
        onClick = _ => p.onClick
      )(OverlayTrigger(overlay = Tooltip.text(p.tooltip))(p.icon))
    }
  }

  private lazy val component = ScalaComponent
    .builder[Props]
    .renderBackend[Backend]
    .build

  def apply(
    variant: String,
    icon: VdomNode,
    //id: UndefOr[String] = undefined,
    info: String = "",
    disabled: Option[String] = None, //None: enabled, Some: disabled and str is displayed as popup (overriding tooltip field)
    onClick: Callback
  ) =
    component(
      Props(
        variant,
        icon,
        onClick, /*id, */
        disabled = disabled.nonEmpty,
        tooltip = disabled.getOrElse(info)
      )
    )
}
