package no.uit.sfb.dataui.utils.graph

import io.circe.generic.extras.Configuration
import io.circe.generic.extras.auto._
import io.circe.parser.decode
import no.uit.sfb.dataui.utils.com.GenericDerivation._
import japgolly.scalajs.react.vdom.html_<^._
import japgolly.scalajs.react.{
  AsyncCallback,
  BackendScope,
  Callback,
  CallbackTo,
  ScalaComponent,
  ~=>
}
import no.uit.sfb.dataui.utils.com.DatabaseApi
import no.uit.sfb.dataui.utils.comp.Spin

trait PropsLike {
  def dbApi: DatabaseApi
  def url: String
  def error: Throwable ~=> Callback
}

trait GraphLike[Props <: PropsLike, DataType] {

  case class State(hoverValue: Option[DataType] = None,
                   graph: Option[GraphResult] = None)

  final protected def loadGraphData(p: Props): AsyncCallback[GraphResult] = {
    implicit val customConfig: Configuration =
      Configuration.default.withDefaults
    p.dbApi
      .get(p.url, contentType = Some("application/json"))
      .map { xhr =>
        //Requires to import no.uit.sfb.dataui.utils.com.GenericDerivation._
        decode[GraphResult](xhr.responseText).toTry.get
      }
  }

  def renderGraph(p: Props, s: State, b: Backend): VdomNode

  class Backend($ : BackendScope[Props, State]) {

    def onChangeHintValue(v: Option[DataType]): CallbackTo[Unit] = { //modState is async in React and returns state from when callback is constructed not when it's executed!
      $.modState(s => s.copy(hoverValue = v))
    }

    def render(p: Props, s: State): VdomNode = {
      s.graph match {
        case Some(g) if g.graph.isEmpty =>
          <.div(<.br, <.p("No data to display for this query."))
        case Some(_) =>
          renderGraph(p, s, this)
        case None =>
          Spin()
      }
    }
  }

  protected val component = ScalaComponent
    .builder[Props]
    .initialState(State())
    .renderBackend[Backend]
    .componentDidMount(lf => {
      val p = lf.props
      loadGraphData(p)
        .flatMap { graph =>
          lf.modStateAsync(_.copy(graph = Some(graph)))
        }
        .handleError { err =>
          p.error(err).asAsyncCallback
        }
        .toCallback
    })
    .componentDidUpdate(lf => {
      val p = lf.currentProps
      if (lf.prevProps.url == p.url)
        Callback.empty //Avoid infinite update loop
      else
        (lf.modStateAsync(_ => State()) >> //We reset the state to force the spinner
          loadGraphData(p)
            .flatMap { graph =>
              lf.modStateAsync(_.copy(graph = Some(graph)))
            }).handleError { err =>
          p.error(err).asAsyncCallback
        }.toCallback
    })
    .build
}
