package no.uit.sfb.dataui.components.graph

import japgolly.scalajs.react.vdom.html_<^._
import japgolly.scalajs.react.{Callback, ~=>}
import no.uit.sfb.dataui.utils.com.DatabaseApi
import no.uit.sfb.dataui.utils.graph.{GraphLike, PropsLike}
import no.uit.sfb.facade.reactvis._
import no.uit.sfb.facade.reactvis.obj.RadialData

import scala.scalajs.js

case class PropsRadialGraph(
    dbApi: DatabaseApi,
    ver: Option[String],
    url: String,
    height: Int = 600,
    error: Throwable ~=> Callback
) extends PropsLike

object RadialGraph extends GraphLike[PropsRadialGraph, RadialData] {

  def renderGraph(p: PropsRadialGraph, s: State, b: Backend): VdomNode = {
    val data = s.graph.get
      .data("y_1",
            fy = y =>
              if (y.numeric && y.v.toString.toDouble >= 0.05) Some(y)
              else None)
      .map {
        case (x, y) =>
          x.toString -> y.toString.toDouble
      }
    <.div(
      RadialChart(
        data = data.map {
          case (label, angle) =>
            new RadialData(
              angle,
              value = s"${(angle * 100).toInt} %",
              label = label,
              className =
                if (s.hoverValue
                      .map { _.label.getOrElse("") }
                      .getOrElse("") == label)
                  "highlighted"
                else
                  js.undefined
            )
        },
        showLabels = false,
        padAngle = 0.04,
        colorType = "category",
        //colorDomain = Seq(0.0, 1.0),
        //colorRange = Seq("blue", "white"),
        onValueMouseOver =
          (datapoint, _) => b.onChangeHintValue(Some(datapoint)),
        onSeriesMouseOut = _ => b.onChangeHintValue(None),
        height = p.height,
        width = p.height
      )(s.hoverValue.map { v =>
        Hint(v)(
          <.div(^.background := "black", <.div(v.label), <.div(v.value))
        )
      }),
    )
  }

  def apply(
      dbApi: DatabaseApi,
      ver: Option[String],
      xAxisVars: Tuple3[String, Option[String], String],
      yAxisVars: Tuple3[String, Option[String], String],
      filter: Tuple2[Option[String], String],
      sort: Option[String] = None,
      height: Int = 600,
      error: Throwable ~=> Callback
  ) = {
    val urlAbs = dbApi
      .graph(ver,
        Seq(
               xAxisVars,
               yAxisVars
             ),
             filter = Seq(filter),
             sort = sort)
      .toString()
    component(PropsRadialGraph(dbApi, ver, url = urlAbs, height = height, error))
  }
}
