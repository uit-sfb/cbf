package no.uit.sfb.dataui.utils.comp

import japgolly.scalajs.react.component.Scala.BackendScope
import japgolly.scalajs.react.vdom.html_<^._
import japgolly.scalajs.react.{Callback, ScalaComponent}
import no.uit.sfb.facade.bootstrap.form.{FormControl, FormControlFeedback}
import no.uit.sfb.facade.bootstrap.inputGroup.{InputGroup, InputGroupText}

import scala.scalajs.js.{UndefOr, undefined, |}

/**
  * For some reason, uncontrolled elements are not suitable when working with movable items (controlled elements move properly, while uncontrolled elements get messed-up)
  * See here: https://reactjs.org/blog/2018/06/07/you-probably-dont-need-derived-state.html
  * In the meantime it is possible to use this wrapper essentially implementing the behavior of an uncontrolled FormControl.
  * NOTE: in React onChange is mapped to onInput. Here onChange will be trigger in the HTML normal meaning (i.e. at blurring)
  */
object UncontrolledFormControlWithValidation {

  case class Props(
    value: String,
    onChange: String => Callback,
    id: UndefOr[String],
    placeholder: String,
    disabled: Boolean,
    validate: Option[String => Option[String]] = None,
    textAreaLines: Option[Int] = None, //If None -> input, otherwise textarea with the defined number of lines (<- nb of lines not implemented yet)
    `type`: String = "",
    max: UndefOr[String] = undefined,
    min: UndefOr[String] = undefined,
    suffix: String = ""
  ) {
    assert(
      textAreaLines.getOrElse(1) >= 1,
      "textAreaLines should be greater than 0."
    )
  }

  case class State(internalValue: String,
                   error: Option[String] = None,
                   isValidating: Boolean = false) {
    lazy val isValid = isValidating && error.isEmpty
    lazy val isInvalid = isValidating && error.nonEmpty
  }

  class Backend($ : BackendScope[Props, State]) {

    val update = (v: String) =>
      $.toModStateWithPropsFn.modState {
        case (_, p) =>
          State(v, p.validate.flatMap { _(v) }, p.validate.nonEmpty)
    }

    def render(p: Props, s: State): VdomElement = {
      val fc =
        FormControl(
          id = p.id,
          as = p.textAreaLines match {
            case Some(_) => "textarea"
            case None    => "input"
          },
          value = s.internalValue,
          placeholder = p.placeholder,
          disabled = p.disabled,
          onChange = e => update(e.currentTarget.value),
          onKeyDown = e =>
            if (e.keyCode == 13) {
              if (p.textAreaLines.nonEmpty) {
                //Some browser plugins (LastPass??) may cause the newline to not be printed in textarea.
                //The following is a non-perfect workaround which adds the new-line at the end of the string.
                update(e.currentTarget.value + "\n")
              } else
                Callback { e.target.blur() }
            } else Callback.empty,
          //onFocus = _ => update(s.internalValue),
          onBlur =
            _ => //We send the update when the component is blurred, and only if the value changed
              if (p.value != s.internalValue) p.onChange(s.internalValue)
              else Callback.empty,
          isValid = s.isValid,
          isInvalid = s.isInvalid,
          `type` = p.`type`,
          min = p.min,
          max = p.max
        )()

      val oFcf =
        if (s.isInvalid)
          Some(FormControlFeedback()(s.error.get))
        else None

      InputGroup()(
        Seq(
          Some(fc),
          oFcf,
          if (p.suffix.nonEmpty) Some(InputGroupText()(p.suffix)) else None
        ).flatten: _*
      )
    }
  }

  private lazy val component = ScalaComponent
    .builder[Props]
    .initialStateFromProps(
      p =>
        State(p.value, p.validate.flatMap { _(p.value) }, p.validate.nonEmpty)
    )
    .renderBackend[Backend]
    .componentDidUpdate(lf => {
      val p = lf.currentProps
      if (p.value != lf.prevProps.value || p.validate
            .flatMap { _(p.value) } != lf.prevProps.validate
            .flatMap { _(lf.prevProps.value) })
        lf.modState(
          _ =>
            State(
              p.value,
              p.validate.flatMap { _(p.value) },
              p.validate.nonEmpty
          )
        )
      else
        Callback.empty
    })
    .build

  def apply(value: String,
            id: UndefOr[String] = undefined,
            onChange: String => Callback,
            placeholder: String = "",
            disabled: Boolean = false,
            validate: Option[String => Option[String]] = None,
            textAreaLines: Option[Int] = None,
            `type`: String = "",
            max: UndefOr[String | Int] = undefined,
            min: UndefOr[String | Int] = undefined,
            suffix: String = "") = {
    component(
      Props(
        value,
        onChange,
        id,
        placeholder,
        disabled,
        validate,
        textAreaLines,
        `type`,
        max.map(_.toString),
        min.map(_.toString),
        suffix
      )
    )
  }
}
