package no.uit.sfb.dataui.utils.com.loader

import io.circe.generic.extras.auto._
import io.circe.generic.extras.Configuration
import io.circe.parser.decode
import japgolly.scalajs.react.AsyncCallback
import no.uit.sfb.cbf.shared.jsonapi.MultipleResponse
import no.uit.sfb.dataui.utils.com.DatabaseApi

trait ConfigIdsLoader {
  protected def loadConfigIds(
    dbApi: DatabaseApi
  ): AsyncCallback[Seq[String]] = {
    case class Id(_id: String)
    implicit val customConfig: Configuration =
      Configuration.default.withDefaults
    dbApi
      .get(
        dbApi.configs(Seq("_id")).toString,
        contentType = Some("application/vnd.api+json")
      )
      .map { xhr =>
        val mr =
          decode[MultipleResponse[Id]](xhr.responseText).toTry.get
        mr.data.map { _.attributes._id }
      }
  }
}
