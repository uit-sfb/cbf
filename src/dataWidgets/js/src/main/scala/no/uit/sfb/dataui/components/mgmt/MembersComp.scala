package no.uit.sfb.dataui.components.mgmt

import io.circe.generic.auto._
import io.circe.syntax._
import japgolly.scalajs.react.Ref.Simple
import japgolly.scalajs.react.component.Scala.BackendScope
import japgolly.scalajs.react.vdom.VdomElement
import japgolly.scalajs.react.vdom.html_<^._
import japgolly.scalajs.react.{Callback, CallbackTo, Ref, ScalaComponent, ~=>}
import no.uit.sfb.cbf.shared.auth.UserInfo
import no.uit.sfb.cbf.shared.config.DatasetConfig
import no.uit.sfb.cbf.shared.jsonapi.{Data, SingleResponse}
import no.uit.sfb.cbf.shared.model.v1.role.RoleOrdering
import no.uit.sfb.dataui.utils.com.loader.MembersLoader
import no.uit.sfb.dataui.utils.com.DatabaseApi
import no.uit.sfb.dataui.utils.comp.{ButtonField, LabelField, Spin}
import no.uit.sfb.facade.bootstrap.Table
import no.uit.sfb.facade.bootstrap.buttons.ButtonGroup
import no.uit.sfb.facade.bootstrap.dropdown._
import no.uit.sfb.facade.icon.Glyphicon
import org.scalajs.dom
import org.scalajs.dom.html

object MembersComp extends MembersLoader {

  case class Props(datasetConfig: DatasetConfig,
                   dbApi: DatabaseApi,
                   error: Throwable ~=> Callback)

  case class State(members: Option[Seq[UserInfo]] = None,
                   reload: Boolean = false,
                   release: Boolean = false)

  class Backend(val $ : BackendScope[Props, State]) {

    private val ref: Simple[html.Input] = Ref[html.Input]

    private val roles: Seq[String] = Seq("owner", "writer", "reader")

    protected lazy val reload = $.modState(_.copy(reload = true))

    implicit val roleOrdering = RoleOrdering

    def render(p: Props, s: State): VdomElement = {
      s.members match {
        case None =>
          Spin()
        case Some(membersList) =>
          val membersTable: (Seq[UserInfo],Boolean) => VdomElement = { //show table only if memberList not empty
            case (Seq(),btnFlag) => <.div()
            case (ml,btnFlag) =>
              Table()(
                <.thead(
                  <.tr(
                    <.th(LabelField("ID")()),
                    <.th(LabelField("Name")()),
                    <.th(LabelField("Email")()),
                    <.th(LabelField("Role")()),
                    <.th(LabelField("Requested role")()),
                    <.th(LabelField("Actions")())
                  ),
                ),
                <.tbody(ml.sortBy(_.role).reverse.map { ui =>
                  <.tr(
                    <.td(ui._id),
                    <.td(ui.name.getOrElse(""): String),
                    <.td(ui.email),
                    <.td(
                      if (btnFlag) {
                        ui.role
                      } else {
                        Dropdown()(
                          DropdownToggle(
                            id = "role-dropdown",
                            variant = "secondary btn-sm"
                          )(ui.role),
                          DropdownMenu()(roles.map {
                            r =>
                              DropdownItem(
                                onClick = _ =>
                                  p.dbApi
                                    .patch(
                                      p.dbApi.members().toString(),
                                      Some("application/json"),
                                      successCallback = reload,
                                      errorCallback = p.error,
                                    )(
                                      SingleResponse[UserInfo](
                                        Some(
                                          Data(
                                            "members",
                                            attributes = ui.copy(role = r),
                                            id = ui._id
                                          )
                                        )
                                      ).asJson.noSpaces
                                    )
                                    .toCallback
                              )(<.label(r))
                          }: _*)
                        )
                      }
                    ),
                    <.td(ui.pending.getOrElse(""):String),
                    <.td(
                      ^.textAlign.center,
                      ButtonGroup()(
                        Seq[Option[VdomElement]](
                          if (btnFlag)
                            Some(
                              ButtonField(
                                variant = "success",
                                onClick = p.dbApi
                                  .patch(
                                    p.dbApi.members().toString(),
                                    Some("application/json"),
                                    successCallback = reload,
                                    errorCallback = p.error,
                                  )(
                                    SingleResponse[UserInfo](
                                      Some(
                                        Data(
                                          "members",
                                          attributes = ui.copy(role = ui.pending.getOrElse(ui.role), pending = None),
                                          id = ui._id
                                        )
                                      )
                                    ).asJson.noSpaces
                                  )
                                  .toCallback,
                                icon = Glyphicon("Check"),
                                info = "Accept"
                              )
                            )
                          else None, //Optional button for accepting pending membership requests

                          Some(
                            ButtonField(
                              variant = "danger",

                              onClick =
                                if (ui.pending.nonEmpty) {
                                  p.dbApi
                                    .patch(
                                      p.dbApi.members().toString(),
                                      Some("application/json"),
                                      successCallback = reload,
                                      errorCallback = p.error,
                                    )(
                                      SingleResponse[UserInfo](
                                        Some(
                                          Data(
                                            "members",
                                            attributes = ui.copy(pending = None),
                                            id = ui._id
                                          )
                                        )
                                      ).asJson.noSpaces
                                    )
                                    .toCallback
                                }
                                else {
                                  CallbackTo {
                                    dom.window.confirm("""Deleting a member will permanently remove this member's account from the current database.
                                                         |Do you want to proceed?""".stripMargin)
                                  }.flatMap { proceed =>
                                    if (proceed)
                                      p.dbApi
                                        .delete(
                                          p.dbApi.members(Some(ui._id)).toString,
                                          successCallback = reload,
                                          errorCallback = p.error
                                        )
                                        .toCallback
                                    else
                                      Callback.empty
                                }
                              },

                              icon = Glyphicon("Trash"),
                              info = "Delete"
                            )
                          )


                        ).flatMap { b =>
                          b
                        }: _*
                      )
                    ),
                  )
                }: _*)
              )
          }
          <.div(
            <.h4("Members"),
            <.br,
            membersTable(membersList.filter(_.role != "none"),false),
            <.h4("Pending requests"),
            membersTable(membersList.filter(_.pending.nonEmpty),true),
          )
      }
    }

  }

  private lazy val component = ScalaComponent
    .builder[Props]
    .initialState(State())
    .renderBackend[Backend]
    .componentDidMount(lf => {
      val p = lf.props
      loadMembers(p.dbApi)
        .flatMap { resp =>
          if (resp.errors.nonEmpty)
            throw new Exception(
              s"Errors:\n  - ${resp.errors.mkString("\n  - ")}"
            )
          else {
            val members = resp.data.flatMap {
              case m: Data[UserInfo] => Some(m.attributes)
              case _                 => None
            }
            lf.modStateAsync(_.copy(members = Some(members)))
          }
        }
        .handleError { err =>
          p.error(err).asAsyncCallback
        }
        .toCallback
    })
    .componentDidUpdate(lf => {
      val p = lf.currentProps
      val s = lf.currentState
      if (s.reload) {
        loadMembers(p.dbApi)
          .flatMap {
            resp =>
              if (resp.errors.nonEmpty)
                throw new Exception(
                  s"Errors:\n  - ${resp.errors.mkString("\n  - ")}"
                )
              else {
                val members = resp.data.flatMap {
                  case m: Data[UserInfo] => Some(m.attributes)
                  case _                 => None
                }
                lf.modStateAsync(
                  _.copy(members = Some(members), reload = false)
                )
              }
          }
          .handleError { err =>
            p.error(err).asAsyncCallback
          }
          .toCallback
      } else {
        Callback.empty
      }
    })
    .build

  def apply(datasetConfig: DatasetConfig,
            dbApi: DatabaseApi,
            error: Throwable ~=> Callback) =
    component(Props(datasetConfig, dbApi, error))

}
