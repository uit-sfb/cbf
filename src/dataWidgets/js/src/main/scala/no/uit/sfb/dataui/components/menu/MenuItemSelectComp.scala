package no.uit.sfb.dataui.components.menu

import japgolly.scalajs.react.component.Scala.BackendScope
import japgolly.scalajs.react.vdom.html_<^._
import japgolly.scalajs.react.{Callback, ScalaComponent}
import no.uit.sfb.facade.bootstrap.dropdown.{
  Dropdown,
  DropdownItem,
  DropdownMenu,
  DropdownToggle
}
import no.uit.sfb.facade.bootstrap.form.{Form, FormCheck}
import no.uit.sfb.facade.bootstrap.overlay.{OverlayTrigger, Tooltip}

object MenuItemSelectComp {

  case class Props(
    descr: String,
    icon: VdomNode,
    choices: Seq[(String, String, Boolean)], //ref, name, selected
    changeAttributeSelect: (String, Boolean) => Callback = //ref, selected
      (_, _) => Callback.empty
  )

  class Backend($ : BackendScope[Props, Unit]) {

    def render(p: Props): VdomElement = {
      Dropdown(drop = "right")(
        DropdownToggle(variant = "link", id = s"${p.descr}-dropdown")(
          OverlayTrigger(overlay = Tooltip.text(p.descr))(p.icon)
        ),
        DropdownMenu()(
          <.div(
            ^.maxHeight := "500px",
            ^.overflow.auto,
            DropdownItem()(Form()(p.choices.map {
              case (ref, name, selected) =>
                FormCheck(label = name, checked = selected, onClick = e => {
                  e.stopPropagationCB >>
                    p.changeAttributeSelect(ref, !selected)
                })()
            }: _*))
          )
        )
      )
    }
  }

  private lazy val component = ScalaComponent
    .builder[Props]
    .renderBackend[Backend]
    .build

  def apply(descr: String,
            icon: VdomNode,
            choices: Seq[(String, String, Boolean)], //ref, name, selected
            changeAttributeSelect: (String, Boolean) => Callback =
              (_, _) => //ref, selected
                Callback.empty,
  ) =
    component(Props(descr, icon, choices, changeAttributeSelect))
}
