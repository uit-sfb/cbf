package no.uit.sfb.dataui.widget

import japgolly.scalajs.react._
import japgolly.scalajs.react.component.Scala.BackendScope
import japgolly.scalajs.react.vdom.html_<^._
import no.uit.sfb.cbf.shared.config.DatasetConfig
import no.uit.sfb.dataui.components.map.MapComp
import no.uit.sfb.dataui.utils.com.loader.ConfigLoader
import no.uit.sfb.dataui.utils.com.{
  DatabaseApi,
  MapUrlBuilder,
  ResourceUrl,
  ToasterLike,
  ToasterStateLike
}
import org.scalajs.dom
import no.uit.sfb.dataui.utils.comp.{Spin, ToastLike, ToastsComp}

import scala.scalajs.js.annotation.{JSExport, JSExportTopLevel}
import scala.util.Try

@JSExportTopLevel("MapWidget")
object MapWidget extends ConfigLoader {
  case class Props(dbApi: DatabaseApi,
                   resourceUrl: ResourceUrl,
                   ver: Option[String],
                   searchQuery: String,
                   attr: Option[String],
                   center: (Double, Double),
                   zoom: Int,
                   height: Int)

  case class State(datasetConfig: Option[DatasetConfig] = None,
                   toasts: Seq[ToastLike] = Seq())
      extends ToasterStateLike {
    def newToasts(ts: Seq[ToastLike]) =
      copy(toasts = ts).asInstanceOf[this.type]
  }

  class Backend(val $ : BackendScope[Props, State])
      extends ToasterLike[Props, State] {

    def render(p: Props, s: State): VdomNode = {
      <.div(
        ToastsComp(s.toasts, deleteToast),
        s.datasetConfig match {
          case Some(config) =>
            val attrsToReturn =
              (config.header +: "id" +: config.tabs.head.attrs).distinct
                .flatMap { ref =>
                  config.persistedAttributes.find(_.ref == ref)
                }
            val latlonAttr = p.attr
              .flatMap { ref =>
                config.persistedAttributes.find(_.ref == ref)
              }
              .orElse(
                config.persistedAttributes.find(_.lType.isSubtypeOf("latlon"))
              )
              .getOrElse(
                throw new Exception(
                  s"Could not find latlon attribute in dataset '${p.dbApi.dsName}'."
                )
              )
              .ref
            MapComp(
              p.dbApi,
              MapUrlBuilder(p.dbApi, p.ver, p.searchQuery, attrsToReturn.map {
                _.ref
              }, latlonAttr),
              attrsToReturn,
              p.center,
              p.zoom,
              p.height,
              error = error
            )
          case None =>
            Spin()
        }
      )
    }
  }

  private val component = ScalaComponent
    .builder[Props]
    .initialState(State())
    .renderBackend[Backend]
    .componentDidMount(lf => {
      val p = lf.props
      loadConfig(p.dbApi)
        .flatMap { datasetConfigResp =>
          val datasetConfig = datasetConfigResp.data.get.attributes
          lf.modStateAsync(_.copy(datasetConfig = Some(datasetConfig)))
        }
        .handleError { err =>
          lf.backend.error(err).asAsyncCallback
        }
        .toCallback
    })
    .build

  def apply(apiEndpoint: String,
            accessToken: Option[String],
            loginUrl: Option[String],
            logoutUrl: Option[String],
            resourceUrl: String,
            dbName: String,
            ver: Option[String],
            searchQuery: String,
            attr: Option[String],
            center: (Double, Double),
            zoom: Int,
            height: Int) =
    component(
      Props(
        DatabaseApi(apiEndpoint, accessToken, dbName),
        ResourceUrl(resourceUrl),
        ver,
        searchQuery,
        attr,
        center,
        zoom,
        height
      )
    )

  @JSExport("renderInto")
  def renderInto(apiEndpoint: String,
                 accessToken: String,
                 loginUrl: String,
                 logoutUrl: String,
                 resourceUrl: String,
                 dbName: String,
                 ver: String,
                 searchQuery: String,
                 attr: String, //May be empty string
                 center: String,
                 zoom: Int,
                 height: Int,
                 elemId: String = "entrypoint") = {
    val centerParsed = Try {
      val splt = center.split(',').map {
        _.toDouble
      }
      splt.head -> splt.tail.head
    }.getOrElse((51.505, -0.09))
    apply(
      apiEndpoint,
      if (accessToken.isEmpty) None else Some(accessToken),
      if (loginUrl.isEmpty) None else Some(loginUrl),
      if (logoutUrl.isEmpty) None else Some(logoutUrl),
      resourceUrl,
      dbName,
      if (ver.isEmpty) None else Some(ver),
      searchQuery,
      if (attr.isEmpty) None else Some(attr),
      centerParsed,
      zoom,
      height
    ).renderIntoDOM(dom.document.getElementById(elemId))
  }
}
