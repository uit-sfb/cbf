package no.uit.sfb.dataui.widget

import japgolly.scalajs.react._
import japgolly.scalajs.react.component.Scala.BackendScope
import japgolly.scalajs.react.vdom.html_<^._
import no.uit.sfb.cbf.shared.config.DatasetConfig
import no.uit.sfb.dataui.components.graph.{GenericGraph, PresentationOption}
import no.uit.sfb.dataui.utils.com.{
  DatabaseApi,
  ResourceUrl,
  ToasterLike,
  ToasterStateLike
}
import no.uit.sfb.dataui.utils.com.loader.ConfigLoader
import no.uit.sfb.dataui.utils.graph.DataSourceBuilder
import org.scalajs.dom
import no.uit.sfb.dataui.utils.comp.{Spin, ToastLike, ToastsComp}

import scala.scalajs.js.annotation.{JSExport, JSExportTopLevel}

@JSExportTopLevel("GraphWidget")
object GraphWidget extends ConfigLoader {
  case class Props(dbApi: DatabaseApi,
                   resourceUrl: ResourceUrl,
                   ver: Option[String],
                   searchQuery: String,
                   x: String,
                   y: String,
                   presOption: PresentationOption,
                   height: Int)

  case class State(datasetConfig: Option[DatasetConfig] = None,
                   toasts: Seq[ToastLike] = Seq())
      extends ToasterStateLike {
    def newToasts(ts: Seq[ToastLike]) =
      copy(toasts = ts).asInstanceOf[this.type]
  }

  class Backend(val $ : BackendScope[Props, State])
      extends ToasterLike[Props, State] {

    def render(p: Props, s: State): VdomNode = {
      <.div(
        ToastsComp(s.toasts, deleteToast),
        s.datasetConfig match {
          case Some(config) =>
            val dataSrcBuilder = DataSourceBuilder(config.persistedAttributes)
            GenericGraph(
              p.dbApi,
              p.ver,
              p.searchQuery,
              Some(dataSrcBuilder.buildX(p.x)),
              Seq(dataSrcBuilder.buildY(p.y)),
              p.presOption,
              p.height,
              error = error
            )
          case None =>
            Spin()
        }
      )
    }
  }

  private val component = ScalaComponent
    .builder[Props]
    .initialState(State())
    .renderBackend[Backend]
    .componentDidMount(lf => {
      val p = lf.props
      loadConfig(p.dbApi)
        .flatMap { datasetConfigResp =>
          val datasetConfig = datasetConfigResp.data.get.attributes
          lf.modStateAsync(_.copy(datasetConfig = Some(datasetConfig)))
        }
        .handleError { err =>
          lf.backend.error(err).asAsyncCallback
        }
        .toCallback
    })
    .build

  def apply(apiEndpoint: String,
            accessToken: Option[String],
            loginUrl: Option[String],
            logoutUrl: Option[String],
            resourceUrl: String,
            dbName: String,
            ver: Option[String],
            searchQuery: String,
            x: String,
            y: String,
            presOption: String,
            height: Int) =
    component(
      Props(
        DatabaseApi(apiEndpoint, accessToken, dbName),
        ResourceUrl(resourceUrl),
        ver,
        searchQuery,
        x,
        y,
        PresentationOption(presOption),
        height
      )
    )

  @JSExport("renderInto")
  def renderInto(apiEndpoint: String,
                 accessToken: String,
                 loginUrl: String,
                 logoutUrl: String,
                 resourceUrl: String,
                 dbName: String,
                 ver: String,
                 searchQuery: String,
                 x: String,
                 y: String,
                 presOption: String,
                 height: Int,
                 elemId: String = "entrypoint") =
    apply(
      apiEndpoint,
      if (accessToken.isEmpty) None else Some(accessToken),
      if (loginUrl.isEmpty) None else Some(loginUrl),
      if (logoutUrl.isEmpty) None else Some(logoutUrl),
      resourceUrl,
      dbName,
      if (ver.isEmpty) None else Some(ver),
      searchQuery,
      x,
      y,
      presOption,
      height
    ).renderIntoDOM(dom.document.getElementById(elemId))
}
