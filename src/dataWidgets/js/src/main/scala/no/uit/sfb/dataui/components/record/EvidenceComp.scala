package no.uit.sfb.dataui.components.record

import japgolly.scalajs.react.ScalaComponent
import japgolly.scalajs.react.component.Scala.BackendScope
import japgolly.scalajs.react.vdom.html_<^._
import no.uit.sfb.cbf.shared.model.v1.nativ.Evidence
import no.uit.sfb.cbf.shared.model.v1.nativ._

object EvidenceComp {
  case class Props(evi: Seq[Evidence])

  class Backend($ : BackendScope[Props, Unit]) extends RenderValueRefLike {
    def renderEvi(evi: Evidence) = {
      val sourcesComp = if (evi.src.nonEmpty) {
        Some(<.div(<.span("Sources:"), <.ul(evi.src.toTagMod { source =>
          renderValueRef(source)
        })))
      } else None
      <.div(renderValueRef(evi), sourcesComp)
    }

    def render(p: Props) = {
      <.div(p.evi.toTagMod { renderEvi })
    }
  }

  private lazy val component = ScalaComponent
    .builder[Props]
    .renderBackend[Backend]
    .build

  def apply(evi: Seq[Evidence]) =
    component(Props(evi))
}
