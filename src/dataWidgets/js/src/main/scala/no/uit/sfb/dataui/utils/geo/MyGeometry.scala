package no.uit.sfb.dataui.utils.geo

import no.uit.sfb.facade.geojson._

case class MyGeometry(path: String, f: Feature) {
  val getGeometryArraySize: Int = this.f.geometry.coordinates.length
}
