package no.uit.sfb.dataui.utils.comp

import io.lemonlabs.uri.Url
import japgolly.scalajs.react._
import japgolly.scalajs.react.vdom.html_<^._
import no.uit.sfb.cbf.shared.auth.UserInfo
import no.uit.sfb.facade.bootstrap.buttons.{Button, ButtonGroup}
import no.uit.sfb.facade.bootstrap.dropdown._
import org.scalajs.dom

/**
  * Either use:
  *  - Login.button(...) to insert a login/logout button,
  *  - or Login.loginButton()/Login.logoutButton() for only the login/logout buttons,
  *  - or add Login.overlay manually and use the Login.action in an onClick callback.
  */
case class Login(loginUrl: Option[String], logoutUrl: Option[String]) {
  protected val loginAction = Callback {
    val url = loginUrl.getOrElse {
      val parsedUrl = Url.parse(dom.window.location.href)
      parsedUrl.addParam("auth", Some(true)).toStringRaw
    }
    dom.window.location
      .assign(url)
  }

  protected val logoutAction = Callback {
    val url = logoutUrl.getOrElse(
      s"/logout?url=" + dom.window.location.pathname + dom.window.location.search
    )
    dom.window.location
      .assign(url)
  }

  //Do not wrap in a div or you'll struggle to get buttons next to each other
  def loginButton(variant: String = "success") =
    Button(variant = variant, onClick = _ => loginAction)("Log in")

  def logoutButton(variant: String = "secondary") =
    Button(variant = variant, onClick = _ => logoutAction)("Log out")

  def dropdownButton(userInfo: Option[UserInfo],
                     extraOptions: Seq[(String, Callback)] = Seq()) = {
    //Do not wrap in a div or you'll struggle to get buttons next to each other
    userInfo match {
      case Some(info) if info._id.nonEmpty && info._id != "anonymous" =>
        val variant = "secondary"
        val dropDownItemList = (
          Seq(
            "Log out" -> logoutAction,
            "User info" -> Callback(dom.window.alert(s"""id: ${info._id}
                                                      |email: ${info.email}
                                                      |role: ${info.role}
                                                      |pending role: ${info.pending.getOrElse("")}
                                                      |""".stripMargin))
          ) ++ extraOptions
        ).map {
          case (k, callback) =>
            DropdownItem(onClick = _ => callback)(k)
        }

        Dropdown()(
          ButtonGroup()(
            logoutButton(variant),
            DropdownToggle(split = true, variant = variant)(),
            DropdownMenu()(dropDownItemList: _*)
          )
        )
      case _ =>
        val variant = "success"
        Dropdown()(
          ButtonGroup()(
            loginButton(variant),
            DropdownToggle(split = true, variant = variant)(),
            DropdownMenu()(
              DropdownItem(onClick = _ => loginAction)("Login"),
              DropdownItem(href = "https://elixir-europe.org/register")(
                "Register"
              )
            )
          )
        )
    }
  }
}
