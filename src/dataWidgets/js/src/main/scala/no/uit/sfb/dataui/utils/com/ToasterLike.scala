package no.uit.sfb.dataui.utils.com

import japgolly.scalajs.react.component.Scala.BackendScope
import japgolly.scalajs.react.Reusable
import no.uit.sfb.cbf.shared.errors.ErrorLike
import no.uit.sfb.dataui.utils.comp.{ErrorToast, ToastLike}

//Re-implement as top component using Children
//if you're using renderBackend then just add a PropsChildren argument to your render method. If you're using render_P or whatever then add a C to it like render_PC. If you're using just render($ => ... then you'll find it on $.propsChildren

trait ToasterStateLike {
  def toasts: Seq[ToastLike]
  final def addToast(newToast: ToastLike): Seq[ToastLike] =
    if (toasts.exists(_.id == newToast.id)) toasts else newToast +: toasts
  final def removeToast(rmToast: String): Seq[ToastLike] = toasts.filter {
    _.id != rmToast
  }
  def newToasts(ts: Seq[ToastLike]): this.type
}

trait ToasterLike[P, S <: ToasterStateLike] {
  def $ : BackendScope[P, S]

  val error = Reusable.fn(
    (err: Throwable) =>
      $.modState(s => {
        val toast = err match {
          case e: ErrorLike =>
            ErrorToast(e.title, e.details)
          case e =>
            ErrorToast(
              "Oops, something went wrong.",
              e.getMessage //"Please retry in a few seconds and let us know if the problem persists."
            )
        }
        s.newToasts(s.addToast(toast))
      })
  )

  val addToast = Reusable.fn(
    (toast: ToastLike) => $.modState(s => s.newToasts(s.addToast(toast)))
  )
  val deleteToast = Reusable.fn((toastId: String) => {
    $.modState(s => {
      s.newToasts(s.removeToast(toastId))
    })
  })
}
