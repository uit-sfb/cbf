package no.uit.sfb.dataui.components.record

import japgolly.scalajs.react.component.Scala.BackendScope
import japgolly.scalajs.react.vdom.html_<^._
import japgolly.scalajs.react.{Callback, Reusable, ScalaComponent}
import no.uit.sfb.cbf.shared.config.{DatasetConfig, Tab}
import no.uit.sfb.cbf.shared.config.ui.TabObject
import no.uit.sfb.cbf.shared.model.v1.nativ.Record
import no.uit.sfb.cbf.shared.model.v1.nativ._
import no.uit.sfb.cbf.shared.validate.QuickValidator
import no.uit.sfb.dataui.utils.VirtualAttributeResolver
import no.uit.sfb.dataui.utils.com.DatabaseApi
import no.uit.sfb.dataui.utils.edit.AttributeConfig
import no.uit.sfb.facade.GlyphiconExt
import no.uit.sfb.facade.bootstrap._
import no.uit.sfb.facade.bootstrap.dropdown.{Dropdown, DropdownItem, DropdownMenu, DropdownToggle}
import no.uit.sfb.facade.bootstrap.navs.{Nav, NavLink}
import no.uit.sfb.facade.bootstrap.overlay.{OverlayTrigger, Tooltip}
import org.scalajs.dom

object RecordComp {

  case class Props(record: Record,
                   dbApi: DatabaseApi,
                   config: DatasetConfig,
                   editMode: Boolean,
                   editCallback: Record => Callback = _ => Callback.empty) {
    //We do not validate templates as templates must be handled on the backend side since they require record expression.
    lazy val validator = new QuickValidator(config, true)
    //We need to validate here to ensure the error annotations are present from the start
    lazy val validatedRecord = validator(record)
    lazy val vAttrResolver = new VirtualAttributeResolver(config)
  }

  case class State(tab: Option[TabObject] = None, displayHeader: Boolean = true)

  class Backend($ : BackendScope[Props, State]) {

    val changeTab =
      Reusable.fn(
        (tabName: String) =>
          $.modState((s: State, p: Props) => {
            //change tab
            val t = p.config.tabObj(tabName)
            if (t.isEmpty)
              println(s"Undefined tab '$tabName'")
            s.copy(tab = t)
          })
      )

    protected def editStatementCallback(ref: String,
                     idx: Int,
                     idxSubValue: Int,
                     vr: ValueRef): Callback = {
      $.props.flatMap { p =>
        val draftRecord = p.validatedRecord
        val stms = draftRecord.stm(ref)
        //TODO: EVI??
        val updatedRecord = {
          if (stms.size > idx) {
            val stm = stms(idx)
            val updatedStatement = if (stm.obj.size > idxSubValue) {
              val origVr = stm.obj(idxSubValue)
              val updatedVr: ValueRef =
                origVr.copy(`val` = vr.`val`, iri = vr.iri)
              stm.copy(
                obj = (stm.obj.take(idxSubValue) :+ updatedVr) ++ stm.obj
                  .drop(idxSubValue + 1)
              )
            } else {
              Statement(Seq(vr))
            }
            val updatedStms = (stms.take(idx) :+ updatedStatement) ++ stms.drop(
              idx + 1
            )
            draftRecord.addAttrs(Map(ref -> updatedStms), true)
          } else {
            //We ignore idxSubValue here since it should be 0 if we are here
            val newStms = (stms.take(idx) :+ Statement(Seq(vr))) ++ stms.drop(
              idx + 1
            )
            draftRecord.addAttrs(Map(ref -> newStms), false)
          }
        }
        val validated = p.validator(updatedRecord)
        p.editCallback(validated)
      }
    }

    def renderTab(tab: TabObject,
                  rec: Record,
                  dbApi: DatabaseApi,
                  vAttrResolver: VirtualAttributeResolver,
                  attrCfgList: Seq[AttributeConfig],
                  editCallback: Record => Callback,
                  editMode: Boolean) = {
      <.div(
        <.h3(tab.name),
        Table(hover = false, responsive = "md")(<.tbody(tab.attrs.toTagMod {
          attr =>
            val attrCfg = attrCfgList.find(_.ref == attr.ref)
            val stms = vAttrResolver.resolve(attr.ref, rec)
            val hasError = stms.exists(_.hasError) || attrCfg.exists {
              _.dependencies.exists(_._1.exists(_.hasError))
            }
            <.tr(
              <.td(
                OverlayTrigger(
                  overlay = Tooltip.text(s"${attr.descr}\nRef: ${attr.ref}")
                )(
                  if (hasError)
                    <.span(^.color.red, GlyphiconExt("Warning"))
                  else GlyphiconExt("Info"),
                  <.span(s" ${attr.name}")
                )
              ),
              <.td(
                MultiStatementComp(
                  stms,
                  suffix = attr.suffix,
                  editMode = editMode,
                  attrConfig = attrCfg,
                  editCallback = idx =>
                    (idxSubValue, vr) =>
                      editStatementCallback(attr.ref, idx, idxSubValue, vr),
                  insertCallback = idx =>
                    idxSubValue =>
                  attrCfg match {
                    case Some(c) =>
                      val updatedRecordCb = if (idxSubValue < 0)
                        c.`type`.insertStatement(attr.ref, idx)(rec, dbApi)
                      else
                        c.`type`.insertSubValue(attr.ref, idx, idxSubValue)(rec)
                      //No need to validate as we simply delete a statement
                      updatedRecordCb.flatMap{editCallback(_).asAsyncCallback}
                        .handleErrorSync{ t =>
                          println(t)
                          Callback{dom.window.alert(t.getMessage)
                          }
                        }
                        .toCallback //Attention: toCallback hides errors
                    case _ => Callback.empty
                  },
                  deleteCallback = idx =>
                    idxSubValue =>
                      attrCfg match {
                        case Some(c) =>
                          val updatedRecordCb = if (idxSubValue == 0)
                            c.`type`.deleteStatement(attr.ref, idx)(rec, dbApi)
                          else
                            c.`type`.deleteSubValue(attr.ref, idx, idxSubValue)(rec)
                          //No need to validate as we simply delete a statement
                          updatedRecordCb.flatMap{editCallback(_).asAsyncCallback}
                            .handleErrorSync(Callback.throwException)
                            .handleErrorSync{ t =>
                              println(t)
                              Callback{dom.window.alert(t.getMessage)
                              }
                            }
                            .toCallback //Attention: toCallback hides errors
                        case _ =>
                        Callback.empty
                      },
                  newLinkedRecordCallback = targetDs => {
                    val targetDbApi = dbApi.copy(dsName = targetDs)
                        Callback {
                          dom.window.open(s"/${targetDbApi.dsName}/browser?target-id=${rec.getId}&target-ds=${dbApi.dsName}")
                        }
                  }
                )
              )
            )
        }))
      )
    }

    def renderNav(currentTabName: String,
                  tabs: Seq[(Tab, Boolean)]) = {
      val orderedCategories = tabs.foldLeft(Seq[(String, Seq[(String, Boolean)])]()) {
        case (acc, v) =>
          val currentCategory = v._1.category
          lazy val oCat = acc.find(_._1 == currentCategory)
          //Note: We do NOT group empty categories together
          if (currentCategory.isEmpty || oCat.isEmpty)
            acc :+ (currentCategory -> Seq(v._1.name -> v._2))
          else {
            val (c, ts) = oCat.get
            val idx = acc.indexWhere(_._1 == c)
            (acc.take(idx) :+ (c -> (ts :+ (v._1.name -> v._2)))) ++ acc.drop(idx + 1)
          }
      }
      Nav(activeKey = currentTabName, variant = "tabs")(
         orderedCategories.flatMap {
          case ("", ts) =>
            //There should be only one tab in the case where cat = "" since that is how we constructed orderedCategories
            ts.headOption.map {
              case (tName, hasError) =>
                NavLink(
                  eventKey = tName,
                  onSelect = key => changeTab(key),
                  active = currentTabName == tName
                )(
                  if (hasError) {
                    <.div(s"$tName ", GlyphiconExt("Warning"))
                  } else
                    tName
                )
            }
          case (cat, ts) =>
            if (ts.nonEmpty)
              Some(renderCategoryDropdown(currentTabName, cat, ts))
            else
              None
        }:_*
        )
    }

    def renderCategoryDropdown(activeTab: String,
                               cat: String,
                               tabs: Seq[(String, Boolean)]): VdomElement = {
      <.div(
        Dropdown(drop = "down")(
          DropdownToggle(variant = "link", id = s"$cat-dropdown")(
            <.span(
              cat,
              GlyphiconExt("Warning").when(!tabs.forall(_._2 == false))
            )
          ),
          DropdownMenu()(tabs.map {
            case (tName, hasError) =>
              DropdownItem(
                eventKey = tName,
                onClick = _ => changeTab(tName),
                active = activeTab == tName
              )(
                if (hasError) {
                  <.div(s"$tName ", GlyphiconExt("Warning"))
                } else
                  tName
              )
          }: _*),
        )
      )
    }

    def render(p: Props, s: State): VdomElement = {
      val rec: Record = p.validatedRecord

      //Tabs, use category names instead
      val tabs = p.config.tabs.filter { t =>
        t.name.nonEmpty && !t.name.startsWith("_")
      }

      val defaultTabObj = tabs.headOption match {
        case Some(t) =>
          p.config.tabObj(t.name).get
        case None =>
          TabObject("Attributes", p.config.attrs)
      }

      val tab: TabObject = s.tab
        .getOrElse(defaultTabObj)

      val attrCfgList = p.config.attrs.flatMap { attr =>
        AttributeConfig(attr.ref, p.config, rec, editStatementCallback)
      }

      val validatedTabs: Seq[(Tab, Boolean)] = tabs.flatMap { t =>
        //We look for any validation error in all the attributes belonging to this tab and their dependencies
        val hasError = t.attrs.exists { attr =>
          rec.stm(attr).exists(_.hasError) || attrCfgList
            .find(_.ref == attr)
            .exists(_.dependencies.exists(_._1.exists(_.hasError)))
        }
        p.config.tab(t.name).map { _ -> hasError }
      }

      <.div(
        renderNav(tab.name, validatedTabs),
        renderTab(tab, rec, p.dbApi, p.vAttrResolver, attrCfgList, p.editCallback, p.editMode)
      )
    }
  }

  private lazy val component = ScalaComponent
    .builder[Props]
    .initialState(State())
    .renderBackend[Backend]
    .build

  def apply(record: Record,
            dbApi: DatabaseApi,
            config: DatasetConfig,
            editMode: Boolean = false,
            editCallback: Record => Callback = _ => Callback.empty) =
    component(Props(record, dbApi, config, editMode, editCallback))
}
