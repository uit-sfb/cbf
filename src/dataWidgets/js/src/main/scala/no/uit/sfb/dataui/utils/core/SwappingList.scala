package no.uit.sfb.dataui.utils.core

object SwappingList {
  def down[A](list: Seq[A], idx: Int): Seq[A] = {
    assert(idx < list.length - 1)
    list.take(idx) ++
      list.slice(idx + 1, idx + 2) ++
      list.slice(idx, idx + 1) ++
      list.drop(idx + 2)
  }

  def up[A](list: Seq[A], idx: Int): Seq[A] = {
    assert(idx > 0)
    list.take(idx - 1) ++
      list.slice(idx, idx + 1) ++
      list.slice(idx - 1, idx) ++
      list.drop(idx + 1)
  }
}
