package no.uit.sfb.dataui.components.record

import japgolly.scalajs.react.{Callback, ReactMouseEvent}
import japgolly.scalajs.react.vdom.html_<^._
import no.uit.sfb.cbf.shared.model.v1.nativ.ValueRefLike
import no.uit.sfb.facade.bootstrap.Badge
import no.uit.sfb.facade.bootstrap.overlay.{OverlayTrigger, Tooltip}
import no.uit.sfb.facade.icon.Glyphicon

trait RenderValueRefLike {
  protected def renderValueRef(
    valRef: ValueRefLike,
    suffix: String = "",
    displayAdditionalInfo: Boolean = true
  ): VdomElement = {
    val url = valRef.meta.get("_iri").orElse(valRef.urls.headOption)
    val value = {
      if (valRef.`val`.isEmpty)
        valRef.meta
          .get("val")
          .orElse(valRef.meta.get("_id"))
          .orElse(valRef.urls.headOption)
          .getOrElse("-")
      else
        s"${valRef.`val`} $suffix"
    }

    def stopPropagation(e: ReactMouseEvent) = e.stopPropagationCB

    val valueComp = <.a(^.href :=? url, ^.onClick ==> { e =>
      if (url.nonEmpty)
        stopPropagation(e)
      else
        Callback.empty
    }, ^.target.blank, ^.rel := "noopener noreferrer")(value)
    val info = {
      def buildInfoComp(meta: Map[String, String]) = {
        meta.get("_id") map { ciri =>
          <.span(
            " ",
            OverlayTrigger(
              overlay = Some(
                Tooltip()(
                  <.div(
                    Badge(variant = "link")(ciri),
                    meta.get("synonyms") flatMap { syn =>
                      syn.split('|').toList match {
                        case Nil => None
                        case l =>
                          Some(<.div(<.br, "Synonyms:", <.ul(l.map { s =>
                            <.li(s.capitalize)
                          }: _*)))
                      }
                    },
                    meta.get("description") flatMap {
                      case "" => None
                      case descr =>
                        Some(<.div(<.br, s"Description: ${descr.capitalize}"))
                    }
                  )
                )
              )
            )(Glyphicon("Info"))
          )
        }
      }

      if (displayAdditionalInfo && valRef.meta.contains("_id") && (valRef.meta
            .contains("synonyms") || valRef.meta.contains("description"))) {
        buildInfoComp(valRef.meta)
      } else
        None
    }
    val badge =
      if (displayAdditionalInfo && valRef.meta
            .getOrElse("_key", "") == valRef.`val`)
        <.span(
          ^.marginRight := "4px",
          Badge(true, "secondary")(valRef.meta.get("_type"))
        )
      else <.span()
    <.span(badge, valueComp, info)
  }
}
