package no.uit.sfb.dataui.view

import japgolly.scalajs.react.{
  BackendScope,
  Callback,
  ReactEventFromInput,
  Reusable,
  ScalaComponent,
  ~=>
}
import japgolly.scalajs.react.vdom.html_<^._
import no.uit.sfb.cbf.shared.config.AttributeDef
import no.uit.sfb.dataui.components.map.MapComp
import no.uit.sfb.dataui.utils.com.{DatabaseApi, MapUrlBuilder, ResourceUrl}
import no.uit.sfb.facade.bootstrap.form.{
  Form,
  FormControl,
  FormGroup,
  FormLabel
}

import scala.util.Random

object MapView {

  case class Props(dbApi: DatabaseApi,
                   resourceUrl: ResourceUrl,
                   ver: Option[String],
                   searchQuery: String,
                   attrsToReturn: Seq[String] = Nil,
                   allPersistedAttributes: Seq[AttributeDef] = Nil,
                   error: Throwable ~=> Callback) {
    lazy val latlonAttrs = allPersistedAttributes
      .filter(_.lType.isSubtypeOf("latlon"))
  }

  case class State(attr: String = "")

  class Backend($ : BackendScope[Props, State])
      extends ViewBackendLike[Props, State] {

    val changeAttr =
      Reusable.fn((x: String) => $.modState(_.copy(attr = x)))

    override def renderData(p: Props, s: State): VdomNode = {
      val attrSelectComp =
        if (p.latlonAttrs.size <= 1) <.div()
        else
          Form(inline = true)(
            FormGroup(
              s"xAxisSelect-${Random.alphanumeric.take(5).mkString("")}"
            )(
              FormLabel(column = true)("Map"),
              FormControl(
                as = "select",
                onChange = (ev: ReactEventFromInput) =>
                  changeAttr(ev.target.value),
                value = s.attr
              )(p.latlonAttrs.map { attr =>
                <.option(^.value := attr.ref, attr.name)
              }: _*)
            )
          )
      val mapDisplay = MapComp(
        p.dbApi,
        MapUrlBuilder(p.dbApi, p.ver, p.searchQuery, p.attrsToReturn, s.attr),
        p.attrsToReturn.flatMap { ref =>
          p.allPersistedAttributes.find(_.ref == ref)
        },
        error = p.error
      )
      <.div(attrSelectComp, mapDisplay)
    }
  }

  private val component = ScalaComponent
    .builder[Props]
    .initialStateFromProps { p =>
      val latlonAttr = p.latlonAttrs.headOption
        .getOrElse(
          throw new Exception(
            s"No attribute of type latlon found in dataset '${p.dbApi.dsName}'."
          )
        ) // Wouldn't make sens to display a map not having a latlon attribute
        .ref
      State(latlonAttr)
    }
    .renderBackend[Backend]
    .build

  def apply(dbApi: DatabaseApi,
            resourceUrl: ResourceUrl,
            ver: Option[String],
            searchQuery: String,
            attrsToReturn: Seq[String],
            allPersistedAttributes: Seq[AttributeDef],
            error: Throwable ~=> Callback) =
    component(
      Props(
        dbApi,
        resourceUrl,
        ver,
        searchQuery,
        attrsToReturn,
        allPersistedAttributes,
        error
      )
    )
}
