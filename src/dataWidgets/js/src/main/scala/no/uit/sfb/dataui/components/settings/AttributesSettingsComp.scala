package no.uit.sfb.dataui.components.settings

import japgolly.scalajs.react.Ref.Simple
import japgolly.scalajs.react.component.Scala.BackendScope
import japgolly.scalajs.react.vdom.html_<^._
import japgolly.scalajs.react.{Callback, CallbackTo, Ref, ScalaComponent}
import no.uit.sfb.cbf.shared.config.{
  AttributeDef,
  Constants,
  CustomResolver,
  DatasetConfig
}
import no.uit.sfb.cbf.shared.types.InternalType
import no.uit.sfb.dataui.utils.comp._
import no.uit.sfb.facade.bootstrap.Table
import no.uit.sfb.facade.bootstrap.form._
import no.uit.sfb.facade.bootstrap.inputGroup.InputGroup
import no.uit.sfb.facade.bootstrap.overlay.{OverlayTrigger, Tooltip}
import no.uit.sfb.facade.icon.Glyphicon
import org.scalajs.dom
import org.scalajs.dom.html
import org.scalajs.dom.raw.{HTMLInputElement, HTMLSelectElement}

object AttributesSettingsComp {

  case class Props(draft: DatasetConfig,
                   updateDraft: DatasetConfig => Callback,
                   noEdit: Boolean)

  case class State(selector: String = "")

  class Backend($ : BackendScope[Props, State]) {
    private val ref: Simple[html.Input] = Ref[html.Input]

    def changeSelector(newSelector: String) =
      $.modState(_.copy(selector = newSelector))

    protected def renderAttrEdit(p: Props, attr: AttributeDef) = {
      //Although id is a default attribute, we do not want to lock it
      val noDelete = Constants.defaultAttributes.contains(attr.ref)
      val locked = noDelete && attr.ref != "id"
      val activeTemplate = attr.hasActiveTemplate
      val it = InternalType(attr.`type`)
      val impliedAutomatic = it.exists(_.impliesAutomatic)
      val impliedVirtual = it.exists(_.impliesVirtual)
      val noModifResolver = it.toSeq
        .flatMap {
          _.overrideResolverList
        }
        .flatten
        .nonEmpty
      val selectId = s"attr-${attr.ref}"
      <.tr(
        <.td(
          OverlayTrigger(
            overlay = Tooltip.text(
              if (locked && !p.noEdit)
                "This default attribute may not be modified."
              else ""
            )
          )(
            if (p.noEdit) <.div()
            else
              ButtonField(
                variant = "danger",
                icon = Glyphicon("Trash"),
                disabled =
                  if (p.noEdit || locked)
                    Some("")
                  else if (noDelete)
                    Some("This attribute may not be deleted.")
                  else None,
                onClick = CallbackTo {
                  p.draft.copy(attrs = p.draft.attrs.filter(_.ref != attr.ref))
                } >>=
                  p.updateDraft,
                info = "Remove attribute"
              ),
            s" ${attr.ref}"
          )
        ),
        <.td(
          OverlayTrigger(
            overlay = Tooltip.text(
              if (locked && !p.noEdit)
                "This default attribute may not be modified."
              else ""
            )
          )(
            TextField(
              label = "Name",
              value = attr.name,
              disabled = if (p.noEdit || locked) Some("") else None,
              placeholder = "Attribute name",
              onChange = newValue =>
                CallbackTo {
                  p.draft.copy(attrs = p.draft.attrs.map { t =>
                    if (t.ref != attr.ref)
                      t
                    else
                      attr.copy(
                        name = if (newValue.isEmpty) t.ref else newValue
                      )
                  })
                } >>=
                  p.updateDraft
            )(),
            TextField(
              label = "Description",
              disabled = if (p.noEdit || locked) Some("") else None,
              value = attr.descr,
              placeholder = "Description",
              onChange = newValue =>
                CallbackTo {
                  p.draft.copy(attrs = p.draft.attrs.map { t =>
                    if (t.ref != attr.ref)
                      t
                    else
                      attr.copy(descr = newValue)
                  })
                } >>=
                  p.updateDraft,
              textAreaLines = Some(2)
            )(),
            TextField(
              label = "Suffix",
              value = attr.suffix,
              disabled = if (p.noEdit || locked) Some("") else None,
              placeholder = "Suffix",
              info =
                "This may be a unit or more generally a suffix displayed along the data.",
              onChange = newValue =>
                CallbackTo {
                  p.draft.copy(attrs = p.draft.attrs.map { t =>
                    if (t.ref != attr.ref)
                      t
                    else
                      attr.copy(suffix = newValue)
                  })
                } >>=
                  p.updateDraft
            )()
          )
        ),
        <.td(
          OverlayTrigger(
            overlay = Tooltip.text(
              if (locked && !p.noEdit)
                "This default attribute may not be modified."
              else ""
            )
          )(
            TextField(
              label = "Template",
              info =
                "Templates are used for computing values from other attributes using the following syntax:\n" +
                  "'{other_attribute}', or '{other_attribute@key}' to use an attribute's metadata referenced by 'key' rather than its value.",
              value = attr.template.getOrElse(""),
              placeholder = "Template",
              disabled = if (p.noEdit || locked) Some("") else None,
              onChange = value =>
                CallbackTo {
                  p.draft.copy(attrs = p.draft.attrs.map { t =>
                    if (t.ref != attr.ref)
                      t
                    else {
                      val tmp =
                        if (value.isEmpty)
                          attr.copy(template = None)
                        else
                          attr.copy(template = Some(value))
                      if (!attr.hasActiveTemplate)
                        tmp.copy(virtual = false, automatic = true)
                      else
                        tmp
                    }
                  })
                } >>=
                  p.updateDraft
            )(),
            SwitchField(
              label = "Virtual",
              info =
                "Virtual attributes are not persisted in the database and are instead computed on demand thanks to their template.",
              checked = attr.virtual,
              disabled =
                if (p.noEdit || locked) Some("")
                else if (attr.ref == "id")
                  Some("Attribute 'id' may NOT be virtual.")
                else if (impliedVirtual)
                  Some(
                    "Because of the type used, this attribute must be virtual."
                  )
                else if (!activeTemplate)
                  Some(
                    "A virtual attribute requires a non-empty template which does not reference itself."
                  )
                else None,
              onChange = newValue =>
                CallbackTo {
                  p.draft.copy(attrs = p.draft.attrs.map { t =>
                    if (t.ref != attr.ref)
                      t
                    else
                      attr.copy(virtual = newValue)
                  })
                } >>=
                  p.updateDraft
            )(),
            SwitchField(
              label = "Automatic",
              info =
                "Automatic attributes are filled automatically by a template or an external pipeline.",
              checked = attr.isFullyControlled,
              disabled = if (p.noEdit || locked) Some("")
              else if (activeTemplate)
                Some(
                  "An active template is applied, therefore this attribute must be automatic."
                )
              else if (impliedAutomatic)
                Some(
                  "Because of the type used, this attribute must be automatic."
                )
              else None,
              onChange = newValue =>
                CallbackTo {
                  p.draft.copy(attrs = p.draft.attrs.map { t =>
                    if (t.ref != attr.ref)
                      t
                    else
                      attr.copy(automatic = newValue)
                  })
                } >>=
                  p.updateDraft
            )(),
            SwitchField(
              label = "Hidden",
              info =
                "Hidden attributes are only accessible to curators during tsv export.",
              checked = attr.hidden,
              disabled = if (p.noEdit || locked) Some("") else None,
              onChange = newValue =>
                CallbackTo {
                  p.draft.copy(attrs = p.draft.attrs.map { t =>
                    if (t.ref != attr.ref)
                      t
                    else
                      attr.copy(hidden = newValue)
                  })
                } >>=
                  p.updateDraft
            )()
          )
        ),
        <.td(
          OverlayTrigger(
            overlay = Tooltip.text(
              if (locked && !p.noEdit)
                "This default attribute may not be modified."
              else ""
            )
          )(
            SwitchField(
              label = "Compulsory",
              checked = attr.forceType,
              disabled =
                if (p.noEdit || locked) Some("")
                else if (attr.ref == "id")
                  Some("Attribute 'id' must be compulsory.")
                else None,
              onChange = newValue =>
                CallbackTo {
                  p.draft.copy(attrs = p.draft.attrs.map { t =>
                    if (t.ref != attr.ref)
                      t
                    else
                      attr.copy(forceType = newValue)
                  })
                } >>=
                  p.updateDraft,
              info =
                "A compulsory attribute may not be left undefined or empty."
            )(),
            SelectField(
              label = "Type",
              value = attr.`type`,
              disabled = if (p.noEdit || locked) Some("") else None,
              info = "A data type may optionally be used for both data validation and visualization." +
                "Note that the type applies only to the first sub-value.",
              onChange = newValue =>
                CallbackTo {
                  val oldOverrideList =
                    it.flatMap(_.overrideResolverList).getOrElse(Seq())
                  val newIt = InternalType(newValue)
                  val typeImpliesAutomatic = newIt.exists { _.impliesAutomatic }
                  val typeImpliesVirtual = newIt.exists { _.impliesVirtual }
                  val newOverrideList = newIt.flatMap { _.overrideResolverList }
                  val rList = newOverrideList.getOrElse(attr.resolvers.filter {
                    !oldOverrideList.contains(_)
                  }) //We filter out the items coming from the previous override list
                  val missingResolvers = rList.filter { r =>
                    !p.draft.customResolvers.map { _.prefix }.contains(r)
                  }
                  p.draft.copy(
                    customResolvers = p.draft.customResolvers ++ missingResolvers
                      .map { CustomResolver(_) },
                    attrs = p.draft.attrs.map {
                      t =>
                        if (t.ref != attr.ref)
                          t
                        else
                          attr.copy(
                            `type` = newValue,
                            forceResolver = newIt
                              .flatMap { _.forceResolvers }
                              .getOrElse(
                                it.flatMap { _.forceResolvers }.isEmpty && attr.forceResolver
                              ),
                            resolvers = rList,
                            automatic =
                              if (typeImpliesAutomatic)
                                true
                              else
                                attr.automatic,
                            virtual =
                              if (typeImpliesVirtual)
                                true
                              else {
                                if (t.hasActiveTemplate)
                                  attr.virtual
                                else
                                  false
                              }
                          )
                    }
                  )
                } >>=
                  p.updateDraft,
              options = {
                val staticTypes = Constants.defaultTypes.map { t =>
                  SelectOption(t.ref, t.name)
                }
                if (attr.ref == "id")
                  staticTypes.filter { so =>
                    Seq("num:int+", "acc").contains(so.key)
                  } else
                  staticTypes ++ Seq(SelectOption.disabled("---")) ++
                    p.draft.customTypes.map { _.ref }.map {
                      ref =>
                        val oIt = InternalType(ref)
                        val v = oIt match {
                          case Some(t) =>
                            t.name
                          case None =>
                            //Should not happen
                            dom.console
                              .warn(s"Type '$ref' is not an internal type")
                            ref
                        }
                        val disabled = oIt.exists { _.unique } && p.draft.attrs
                          .exists { _.`type` == ref }
                        SelectOption(ref, v, disabled)
                    }
              }
            )(),
            OverlayTrigger(
              overlay = Tooltip.text(
                if (noModifResolver && !locked)
                  s"It is not allowed to modify the resolver settings with the selected type."
                else ""
              )
            )(
              <.div(
                ^.paddingLeft := "25px",
                LabelField(
                  "Resolvers",
                  disabled =
                    if (p.noEdit || locked || noModifResolver) Some("")
                    else None,
                  info = "A resolver links an identifier to a URL(s), custom, and/or retrieved external data."
                )(),
                SwitchField(
                  label = "Strict",
                  checked = attr.forceResolver,
                  disabled =
                    if (p.noEdit || locked || noModifResolver) Some("")
                    else None,
                  onChange = newValue =>
                    CallbackTo {
                      p.draft.copy(attrs = p.draft.attrs.map { t =>
                        if (t.ref != attr.ref)
                          t
                        else
                          attr.copy(forceResolver = newValue)
                      })
                    } >>=
                      p.updateDraft,
                  info = "At least one of the listed resolvers must match. No effect if the resolver list is empty."
                )(),
                Table()(
                  <.tbody(
                    attr.resolvers.map { r =>
                      <.tr(
                        <.td(r),
                        <.td(
                          ^.textAlign.center,
                          if (p.noEdit) <.div()
                          else
                            ButtonField(
                              variant = "danger",
                              icon = Glyphicon("Trash"),
                              disabled =
                                if (p.noEdit || locked || noModifResolver)
                                  Some("")
                                else None,
                              onClick =
                                CallbackTo {
                                  p.draft.copy(
                                    attrs = p.draft.attrs.map(
                                      t =>
                                        if (t.ref != attr.ref)
                                          t
                                        else {
                                          val newResolvers =
                                            attr.resolvers.filter(_ != r)
                                          attr.copy(
                                            resolvers =
                                              attr.resolvers.filter(_ != r),
                                            forceResolver = attr.forceResolver && newResolvers.nonEmpty
                                          )
                                      }
                                    )
                                  )
                                } >>=
                                  p.updateDraft,
                              info = "Remove resolver"
                            )
                        )
                      )
                    } ++ (if (p.noEdit) Seq()
                          else
                            Seq(
                              <.tr(
                                <.td(
                                  //We do NOT want to convert this to SelectFieldUncontrolled otherwise it would mess up with the layout
                                  FormControl.uncontrolled(
                                    ref,
                                    as = "select",
                                    id = s"$selectId-resolver-add",
                                    disabled = p.noEdit || locked || noModifResolver,
                                    defaultValue = ""
                                  )(
                                    <.option(
                                      ^.value := "",
                                      ^.disabled := true,
                                      "- Select -"
                                    ) +:
                                      p.draft.customResolvers
                                      .collect {
                                        case resolv
                                            if !attr.resolvers.contains(
                                              resolv.prefix
                                            ) => //We make sure we cannot add the same twice
                                          resolv.prefix
                                      }
                                      .map {
                                        <.option(_)
                                      }: _*
                                  )
                                ),
                                <.td(
                                  ^.textAlign.center,
                                  ButtonField(
                                    variant = "primary",
                                    icon = Glyphicon("Add"),
                                    disabled =
                                      if (p.noEdit || locked || noModifResolver)
                                        Some("")
                                      else None,
                                    onClick = CallbackTo { //Note we NEED to build the new draft within a callback otherwise the logic is executed at render time!
                                      val value = {
                                        val elem = dom.document
                                          .getElementById(
                                            s"$selectId-resolver-add"
                                          )
                                          .asInstanceOf[HTMLSelectElement]
                                        val tmp = elem.value
                                        elem.value = ""
                                        tmp
                                      }
                                      if (value.nonEmpty)
                                        p.draft.copy(attrs = p.draft.attrs.map {
                                          t =>
                                            if (t.ref == attr.ref && !attr.resolvers
                                                  .contains(value)) {
                                              attr.copy(
                                                resolvers = attr.resolvers :+ value
                                              )
                                            } else
                                              t
                                        })
                                      else
                                        p.draft
                                    } >>= p.updateDraft,
                                    info = "Add resolver"
                                  )
                                )
                              )
                            )): _*
                  )
                )
              )
            )
          )
        )
      )
    }

    def render(p: Props, s: State): VdomElement = {
      <.div(
        Table(hover = true)(
          <.thead(
            <.tr(
              <.th(
                OverlayTrigger(
                  overlay = Tooltip
                    .text("Click on a line to edit.")
                )(
                  "Reference",
                  <.span(^.paddingLeft := "5px"),
                  Glyphicon("Help")
                )
              ),
              <.th("Name"),
              <.th("Description"),
              <.th("Unit")
            )
          ),
          <.tbody(p.draft.attrs.collect {
            case attr if s.selector == attr.ref =>
              renderAttrEdit(p, attr)
            case attr =>
              <.tr(
                ^.onClick --> changeSelector(attr.ref),
                <.td(attr.ref),
                <.td(attr.name),
                <.td(attr.descr),
                <.td(attr.suffix)
              )
          }: _*)
        ),
        if (p.noEdit) <.div()
        else
          FormGroup("newAttributeRefInput")(
            InputGroup()(
              //Do NOT convert this to a TextFieldUncontrolled as it would mess up with the layout of the Add button
              FormControl.uncontrolled(ref, placeholder = "Attribute ref")(),
              ButtonField(
                variant = "outline-primary",
                icon = "Add",
                onClick =
                  CallbackTo {
                    val value = {
                      val elem = dom.document
                        .getElementById("newAttributeRefInput")
                        .asInstanceOf[HTMLInputElement]
                      val tmp = elem.value
                      //Reset value
                      elem.value = elem.defaultValue
                      tmp
                    }
                    val exists = p.draft.attrs.exists(_.ref == value)
                    if (value.nonEmpty && !exists)
                      p.draft.copy(
                        attrs = p.draft.attrs :+ AttributeDef(value, value)
                      )
                    else {
                      if (exists)
                        dom.window.alert(s"Item '$value' already exists.")
                      p.draft
                    }
                  } >>=
                    p.updateDraft,
                info = "New attribute"
              )
            )
          )
      )
    }
  }

  private lazy val component = ScalaComponent
    .builder[Props]
    .initialState(State())
    .renderBackend[Backend]
    .build

  def apply(draft: DatasetConfig,
            updateDraft: DatasetConfig => Callback,
            locked: Boolean = false) =
    component(Props(draft, updateDraft, locked))
}
