package no.uit.sfb.dataui.widget

import japgolly.scalajs.react.component.Scala.BackendScope
import japgolly.scalajs.react.vdom.html_<^._
import japgolly.scalajs.react._
import no.uit.sfb.cbf.shared.auth.UserInfo
import no.uit.sfb.cbf.shared.config.DatasetConfig
import no.uit.sfb.cbf.shared.jsonapi.{Data, SingleResponse}
import no.uit.sfb.cbf.shared.model.v1.versioninfo.VersionInfo
import no.uit.sfb.dataui.components.search
import no.uit.sfb.dataui.view._
import no.uit.sfb.dataui.utils.com.loader.{
  ConfigLoader,
  UserInfoLoader,
  VersionsLoader
}
import no.uit.sfb.dataui.utils.com.{
  DatabaseApi,
  ResourceUrl,
  ToasterLike,
  ToasterStateLike
}
import no.uit.sfb.dataui.utils.comp.{
  Login,
  SelectField,
  SelectOption,
  Spin,
  ToastLike,
  ToastsComp
}
import no.uit.sfb.facade.bootstrap.alert.Alert
import no.uit.sfb.facade.bootstrap.buttons.{
  Button,
  ButtonToolbar,
  ToggleButton,
  ToggleButtonGroup
}
import no.uit.sfb.facade.bootstrap.dropdown.{DropdownButton, DropdownItem}
import no.uit.sfb.facade.bootstrap.form.{Form, FormGroup}
import no.uit.sfb.facade.bootstrap.grid.{Col, Container, Row}
import no.uit.sfb.facade.bootstrap.modal.{Modal, ModalBody, ModalFooter}
import no.uit.sfb.facade.bootstrap.overlay.{OverlayTrigger, Tooltip}
import no.uit.sfb.facade.icon.Glyphicon
import org.scalajs.dom
import io.circe.generic.auto._
import io.circe.syntax._
import no.uit.sfb.cbf.shared.model.v1.role.RoleOrdering

import scala.scalajs.js.annotation.{JSExport, JSExportTopLevel}

@JSExportTopLevel("BrowserWidget")
object BrowserWidget
    extends ConfigLoader
    with VersionsLoader
    with UserInfoLoader {

  case class Props(dbApi: DatabaseApi,
                   loginUrl: Option[String],
                   logoutUrl: Option[String],
                   resourceUrl: ResourceUrl,
                   targetId: String,
                   targetDs: String)

  case class State(ver: Option[VersionInfo] = None,
                   datasetConfig: Option[DatasetConfig] = None,
                   view: ViewSelectorLike = TableViewSelector,
                   searchQuery: String = "",
                   filters: Array[Seq[String]] = Array(),
                   userInfo: UserInfo = UserInfo("anonymous"),
                   versions: Seq[VersionInfo] = Seq(),
                   toasts: Seq[ToastLike] = Seq(),
                   showRequestAccessModal: Boolean = false,
                   showUnsubscribe: Boolean = false,
                   disableSubmit: Boolean = true)
      extends ToasterStateLike {
    def newToasts(ts: Seq[ToastLike]) =
      copy(toasts = ts).asInstanceOf[this.type]

    lazy val versionInfo = ver flatMap { v =>
      versions.find(_._id == v._id)
    }
    lazy val isReleased = versionInfo.exists { !_.isDraft }
    lazy val isDeprecated = versionInfo.exists { _.deprecated }
  }

  class Backend(val $ : BackendScope[Props, State])
      extends ToasterLike[Props, State] {
    val changeVersion =
      Reusable.fn(
        (ver: VersionInfo) =>
          $.modState {
            _.copy(ver = Some(ver))
        }
      )

    val changeView =
      Reusable.fn((view: ViewSelectorLike) => $.modState(_.copy(view = view)))

    val changeSearchQuery =
      Reusable.fn((searchQuery: String) => {
        $.modState(_.copy(searchQuery = searchQuery))
      })

    def changeRequestAccessModal(b: Boolean) =
      $.modState(_.copy(showRequestAccessModal = b))

    def changeUnsubscribeModal(b: Boolean) =
      $.modState(_.copy(showUnsubscribe = b))

    def disableSubmit(b: Boolean) = $.modState(_.copy(disableSubmit = b))

    val chooseRole =
      Reusable.fn((roleName: String) => {
        $.modState(
          s => s.copy(userInfo = s.userInfo.copy(pending = Some(roleName)))
        )
      })

    protected def logo(url: String,
                       tooltip: String,
                       imageName: String,
                       resourceUrl: ResourceUrl): VdomNode = {
      OverlayTrigger(placement = "top", overlay = Tooltip.text(tooltip))(
        <.a(^.href := url, ^.target.blank, ^.rel := "noopener noreferrer")(
          ^.padding := "5px",
          <.img(
            ^.src := resourceUrl.absolute(s"/images/$imageName"),
            ^.className := "icon img-link"
          )
        )
      )
    }

    def render(p: Props, s: State): VdomNode = {

      def dbaliases(dbName: String): String = {
        if (dbName.contains("ebp-")) {
          return dbName.split("_").last.capitalize
        }
        dbName
      }


      val roles = Seq(
        "reader" -> "Read access",
        "writer" -> "Write access",
        "owner" -> "Owner"
      )
      val role = if (s.userInfo.role.isEmpty) "none" else s.userInfo.role
      val filteredRoles = roles.filter {
        case (k, _) => RoleOrdering.compare(k, role) > 0
      }
      val unsubscribeModal = //Unsubscribing means removing your data from the database
        Modal(
          animation = false,
          size = "m",
          show = s.showUnsubscribe,
          onHide = _ => changeUnsubscribeModal(false)
        )(
          ModalBody()(
            <.h4("Are you sure you want to unsubscribe from this database?"),
            <.div(
              "This will permanently remove your account from the current database."
            )
          ),
          ModalFooter()(
            Button(
              onClick = _ => changeUnsubscribeModal(false),
              variant = "outline-secondary"
            )("Close"),
            Button(
              onClick = _ =>
                p.dbApi
                  .delete(
                    p.dbApi.members(Some(s.userInfo._id)).toString,
                    successCallback = Callback.empty,
                    errorCallback = error
                  )
                  .toCallback
                  >> changeUnsubscribeModal(false)
            )("Ok")
          )
        )
      val memberModal =
        if (filteredRoles.isEmpty) None
        else
          Some(
            Modal(
              animation = false,
              size = "m",
              show = s.showRequestAccessModal,
              onHide = _ => changeRequestAccessModal(false)
            )(
              ModalBody()(
                <.h4("Request access"),
                Form(onSubmit = _.preventDefaultCB)(
                  FormGroup("inputFile")(
                    SelectField(
                      labelWidth = 2,
                      label = "Select desired role",
                      value = s.userInfo.pending.getOrElse(
                        filteredRoles.head._1
                      ), //Ok since we checked that filteredRoles is non-empty
                      onChange = newValue => chooseRole(newValue),
                      options = filteredRoles.map {
                        case (k, v) => SelectOption(k, v)
                      },
                      info = "Select which role to apply for."
                    )()
                  ),
                ) //Form end
              ),
              ModalFooter()(
                Button(
                  onClick = _ => changeRequestAccessModal(false),
                  variant = "outline-secondary"
                )("Close"),
                Button(
                  disabled = s.userInfo.pending.isEmpty,
                  onClick = _ =>
                    p.dbApi
                      .post(
                        p.dbApi.members().toString(),
                        Some("application/json"),
                        successCallback = Callback.empty,
                        errorCallback = error,
                      )(
                        SingleResponse[UserInfo](
                          Some(
                            Data(
                              "members",
                              attributes = s.userInfo,
                              id = s.userInfo._id
                            )
                          )
                        ).asJson.noSpaces
                      )
                      .toCallback
                      .flatMap { _ =>
                        changeRequestAccessModal(false)
                    }
                )("Submit")
              )
            )
          )

      val browser = {
        lazy val viewRendering: VdomElement = {
          s.datasetConfig match {
            case Some(datasetConfig) =>
              <.div(
                if (s.isDeprecated)
                  Alert(variant = "danger")("This release is deprecated.")
                else <.div(),
                s.view match {
                  case TableViewSelector =>
                    RecordListView(
                      RecordListView.Props(
                        datasetConfig,
                        p.dbApi,
                        p.resourceUrl,
                        s.ver.map {
                          _._id
                        },
                        s.versions,
                        s.userInfo,
                        s.isReleased,
                        s.searchQuery,
                        error,
                        p.targetId,
                        p.targetDs
                      )
                    )
                  case GraphViewSelector =>
                    GraphView(
                      p.dbApi,
                      p.resourceUrl,
                      s.ver.map { _._id },
                      s.searchQuery,
                      datasetConfig.persistedAttributes,
                      error
                    )
                  case GeoViewSelector =>
                    GeoView(
                      p.dbApi,
                      p.resourceUrl,
                      s.ver.map { _._id },
                      s.searchQuery,
                      datasetConfig.persistedAttributes,
                      datasetConfig.customTypes,
                      error
                    )
                  case MapViewSelector =>
                    MapView(
                      p.dbApi,
                      p.resourceUrl,
                      s.ver.map { _._id },
                      s.searchQuery,
                      (datasetConfig.header +: "id" +: datasetConfig.tabs.head.attrs).distinct,
                      datasetConfig.persistedAttributes,
                      error
                    )
                }
              )
            case None =>
              Spin()
          }
        }
        val searchComp = search.SearchComp(
          s.searchQuery,
          changeSearchQuery,
          s.datasetConfig,
          s.filters
        )
        val settings = {
          Button(
            variant = "outline-primary",
            onClick = _ =>
              Callback {
                dom.window.open(s"/${p.dbApi.dsName}/settings", "_blank")
            }
          )(
            OverlayTrigger(
              overlay = Tooltip.text(
                if (s.userInfo.is("owner")) "Settings"
                else "Definitions"
              )
            )(Glyphicon(if (s.userInfo.is("owner")) "Settings" else "Info2"))
          )
        }
        val viewSelect = ToggleButtonGroup(
          name = "views",
          `type` = "radio",
          value = s.view.name
        )(ViewSelectorLike.all.collect {
          case view if view.enabled(s.datasetConfig) =>
            ToggleButton(value = view.name, onChange = _ => changeView(view))(
              OverlayTrigger(overlay = Tooltip.text(view.name))(view.icon)
            )
        } :+ settings: _*)

        def versionDisplay(vi: VersionInfo) = {
          if (vi.deprecated)
            <.span(
              ^.textDecoration.`line-through`,
              OverlayTrigger(
                placement = "top",
                overlay = Tooltip.text("Deprecated")
              )(vi.displayStr)
            )
          else if (vi.isDraft)
            <.span(
              ^.fontStyle.italic,
              OverlayTrigger(
                placement = "top",
                overlay = Tooltip.text("Draft")
              )(vi.displayStr)
            )
          else <.span(vi.displayStr)
        }
        lazy val versionSelect =
          if (s.datasetConfig.exists {
                _.latestOnly
              } && !s.userInfo
                .is("writer"))
            <.div()
          else {
            <.div(
              ^.paddingLeft := "30px",
              DropdownButton(
                id = "version-dropdown",
                title = s.ver
                  .map {
                    _.displayStr
                  }
                  .getOrElse("") /*,
                variant =
                  if (s.ver.exists { _.deprecated })
                    "danger"
                  else if (s.ver.exists { _.isDraft })
                    "warning"
                  else "primary"*/
              )(s.versions.reverse.map { ver =>
                DropdownItem(onClick = _ => changeVersion(ver))(
                  versionDisplay(ver)
                )
              }: _*)
            )
          }
        val links = <.div(
          logo(
            s"${p.dbApi.apiEndpoint}${p.dbApi.api().toString()}",
            "API",
            "swagger.png",
            p.resourceUrl
          ),
          logo(
            "https://gitlab.com/uit-sfb/cbf",
            "Source code",
            "gitlab.png",
            p.resourceUrl
          )
        )
        val additionalOptions = Seq(
          if (filteredRoles.nonEmpty)
            Some("Request access" -> changeRequestAccessModal(true))
          else None,
          //Maybe need to check if user is logged in here?
          Some("Unsubscribe" -> changeUnsubscribeModal(true))
        ).flatten
        val login = Login(p.loginUrl, p.logoutUrl)
          .dropdownButton(Some(s.userInfo), extraOptions = additionalOptions)
        <.div(
          ^.paddingTop := "10px",
          Container(fluid = true)(
            Row()(Col()(<.h3(^.paddingLeft := "30px", s.datasetConfig.map { v =>
              dbaliases(v.name)
            }))),
            Row()(
              Col(1)(<.div()),
              Col(2)(versionSelect),
              Col()(
                <.div(^.className := "d-flex justify-content-end", searchComp)
              ),
              Col(4)(
                ButtonToolbar("justify-content-between")(
                  viewSelect,
                  <.div(^.paddingRight := "30px", login)
                )
              )
            ),
            Row()(<.div(^.paddingTop := "10px")),
            Row()(Col()(viewRendering)),
            Row()(
              Col()(
                <.div(
                  ^.display.flex,
                  ^.alignItems.center,
                  ^.justifyContent.flexEnd,
                  links
                )
              )
            )
          )
        )
      }

      <.div(
        Seq(
          memberModal,
          Some(unsubscribeModal),
          Some(ToastsComp(s.toasts, deleteToast): VdomElement),
          Some(browser)
        ).flatten: _*
      )
    }
  }


  private val component =
    ScalaComponent
      .builder[Props]
      .initialState(State())
      .renderBackend[Backend]
      .componentDidMount(
        lf => { //We do not need to reload config, version and userInfo in componentDidUpdate since the only parameter is p.dbApi, which is a constant
          val p = lf.props
          val loadConfigAsync =
            loadConfig(p.dbApi)
              .map { datasetConfigResp =>
                Some(datasetConfigResp.data.get.attributes)
              }
          val loadVersionsAsync =
            loadVersions(p.dbApi)
          val userInfoAsync = loadUserInfo(p.dbApi)
          loadConfigAsync
            .zip(loadVersionsAsync)
            .zip(userInfoAsync)
            .flatMap {
              case ((config, versions), userInfo) =>
                lf.modStateAsync(
                  s =>
                    s.copy(
                      datasetConfig = config,
                      ver = versions.lastOption,
                      versions = versions,
                      userInfo = userInfo
                  )
                )
            }
            .handleError { err =>
              lf.backend.error(err).asAsyncCallback.map { _ =>
                Seq()
              }
            }
            .toCallback
        }
      )
      .build

  def apply(apiEndpoint: String,
            accessToken: Option[String],
            loginUrl: Option[String],
            logoutUrl: Option[String],
            resourceUrl: String,
            dbName: String,
            prefilter: String,
            targetId: String,
            targetDs: String) =
    component(
      Props(
        DatabaseApi(apiEndpoint, accessToken, dbName, prefilter),
        loginUrl,
        logoutUrl,
        ResourceUrl(resourceUrl),
        targetId,
        targetDs
      )
    )
  //TODO: Rename here for ebp?
  @JSExport("renderInto")
  def renderInto(apiEndpoint: String,
                 accessToken: String,
                 loginUrl: String,
                 logoutUrl: String,
                 resourceUrl: String,
                 dbName: String,
                 prefilter: String = "",
                 targetId: String = "",
                 targetDs: String = "",
                 elemId: String = "entrypoint") = {
    apply(
      apiEndpoint,
      if (accessToken.isEmpty) None else Some(accessToken),
      if (loginUrl.isEmpty) None else Some(loginUrl),
      if (logoutUrl.isEmpty) None else Some(logoutUrl),
      resourceUrl,
      dbName,
      prefilter,
      targetId,
      targetDs,
    ).renderIntoDOM(dom.document.getElementById(elemId))
  }
}
