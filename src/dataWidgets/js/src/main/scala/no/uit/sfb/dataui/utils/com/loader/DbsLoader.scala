package no.uit.sfb.dataui.utils.com.loader

import io.circe.generic.extras.auto._
import io.circe.generic.extras.Configuration
import io.circe.parser.decode
import japgolly.scalajs.react.AsyncCallback
import no.uit.sfb.dataui.utils.com.DatabaseApi

trait DbsLoader {
  protected def loadDbs(
    dbApi: DatabaseApi
  ): AsyncCallback[Map[String, (String, Boolean, String, String)]] = {

    implicit val customConfig: Configuration =
      Configuration.default.withDefaults
    dbApi
      .get(dbApi.discover().toString, contentType = Some("application/json"))
      .map { xhr =>
        decode[Map[String, (String, Boolean, String, String)]](xhr.responseText).toTry.get
      }
  }
}
