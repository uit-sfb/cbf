package no.uit.sfb.dataui.widget

import japgolly.scalajs.react._
import japgolly.scalajs.react.component.Scala.BackendScope
import japgolly.scalajs.react.vdom.html_<^._
import no.uit.sfb.cbf.shared.dataset.DatasetRegistrationForm
import no.uit.sfb.cbf.shared.jsonapi.{Data, SingleResponse}
import no.uit.sfb.cbf.shared.model.v1.role.RoleOrdering
import no.uit.sfb.dataui.utils.com.loader.{DbsLoader, UserInfoLoader}
import no.uit.sfb.dataui.utils.com.{DatabaseApi, ResourceUrl, ToasterLike, ToasterStateLike}
import no.uit.sfb.dataui.utils.comp.{Login, Spin, TextField, ToastLike, ToastsComp}
import no.uit.sfb.facade.bootstrap.{Badge, Table}
import no.uit.sfb.facade.bootstrap.buttons.Button
import no.uit.sfb.facade.bootstrap.grid.{Col, Row}
import no.uit.sfb.facade.bootstrap.overlay.{OverlayTrigger, Tooltip}
import no.uit.sfb.facade.icon.Glyphicon
import org.scalajs.dom
import io.circe.generic.auto._
import io.circe.syntax._
import no.uit.sfb.cbf.shared.auth.UserInfo
import no.uit.sfb.cbf.shared.errors.ErrorLike
import no.uit.sfb.facade.bootstrap.modal.{Modal, ModalBody, ModalFooter}

import scala.scalajs.js.annotation.{JSExport, JSExportTopLevel}
import scala.util.Try

@JSExportTopLevel("LauncherWidget")
object LauncherWidget extends DbsLoader with UserInfoLoader {

  case class Props(dbApi: DatabaseApi,
                   resourceUrl: ResourceUrl,
                   loginUrl: Option[String],
                   logoutUrl: Option[String])

  case class State(
    datasets: Option[Map[String, (String, Boolean, String, String)]] = None,
    form: Option[DatasetRegistrationForm] = None,
    userInfo: UserInfo = UserInfo("anonymous"),
    toasts: Seq[ToastLike] = Seq()
  ) extends ToasterStateLike {
    def newToasts(ts: Seq[ToastLike]) = copy(toasts = ts).asInstanceOf[this.type]

    lazy val validatedForm = Try { form.map { drf =>
      DatasetRegistrationForm.validate(drf)
      drf
    } }.toOption.flatten

    lazy val isValid = validatedForm.nonEmpty
  }

  class Backend(val $ : BackendScope[Props, State])
      extends ToasterLike[Props, State] {

    protected lazy val newForm = updateForm(
      Some(DatasetRegistrationForm("", "", ""))
    )
    protected def updateForm(form: Option[DatasetRegistrationForm]) =
      $.modState(_.copy(form = form))
    protected lazy val closeModal = updateForm(None)

    def render(p: Props, s: State): VdomNode = {
      lazy val creatButton = <.div(
        ^.textAlign.center,
        Button(variant = "outline-primary", onClick = _ => newForm)(
          "Create a dataset"
        )
      )
      val modal = Modal(animation = false, show = s.form.nonEmpty, onHide = _ => closeModal)(
          ModalBody()(
            TextField(
              s.form.map { _.name }.getOrElse(""),
              labelWidth = 2,
              label = "Dataset name",
              onChange = v => updateForm(s.form.map { _.copy(name = v) }),
              validate = Some(str =>
                Try { DatasetRegistrationForm.validate(DatasetRegistrationForm(str)); None }.recover{
                  case e: ErrorLike =>
                    Some(e.details)
                  case e => Some(e.getMessage())
                }.toOption.flatten)
            )(),
            TextField(
              s.form.map { _.description }.getOrElse(""),
              labelWidth = 2,
              label = "Description",
              onChange = v => updateForm(s.form.map { _.copy(description = v) }),
              smallPrint = "(optional)"
            )()
          ),
          ModalFooter()(
            Button(onClick = _ => closeModal, variant = "outline-secondary")(
              "Cancel"
            ),
            Button(
              //disabled = !s.isValid, //We do not use it as it requires first loosing focus on the newly fixed element to allow the button.
              onClick = _ => CallbackTo {
                s.form
            } >>= {
              case Some(form) =>
                p.dbApi
                  .post(
                    p.dbApi.datasetsGeneric().toString(),
                    Some("application/json"), //Do NOT use application/vnd.api+json here as the data is not transmitted for some reason.
                    successCallback = closeModal >> Callback {
                      dom.window.location.reload()
                    },
                    errorCallback = e => closeModal >> error(e)
                  )(
                    SingleResponse[DatasetRegistrationForm](
                      Some(Data("datasets", attributes = form))
                    ).asJson.noSpaces
                  )
                  .toCallback
              case None => Callback.empty
            })("Create")
          )
        )
      lazy val table =
        if (s.userInfo.role == "anonymous") {
          <.div(^.textAlign.center, <.p("Please log in"), Login(p.loginUrl, p.logoutUrl).loginButton())
        } else {
          s.datasets match {
            case Some(dbs) =>
              <.div(
                creatButton,
                <.br,
                <.h4("Your datasets"),
                Table()(
                  <.tbody(
                    dbs
                      .groupBy {
                        case (_, (role, _, _, _)) => role
                      }
                      .toSeq
                      .sortBy(_._1)(RoleOrdering)
                      .reverse
                      .flatMap {
                        case (_, m) =>
                          m.toSeq
                            .sortBy(_._1)
                      }
                      .map {
                        case (
                            dbName,
                            (role, isDemo, description, contextUrl)
                            ) =>
                          <.tr(
                            <.td(<.a(^.href := s"/$dbName/browser", dbName)),
                            <.td(description),
                            <.td(
                              Row()(
                                Col(md = 2)(<.div(Badge(variant = role match {
                                  case "owner"  => "success"
                                  case "writer" => "warning"
                                  case "reader" => "info"
                                  case _        => "secondary"
                                })(role))),
                                Col(sm = 2)(
                                  <.div(
                                    if (contextUrl.nonEmpty)
                                      Button(
                                        variant = "link",
                                        href = contextUrl
                                      )(
                                        OverlayTrigger(
                                          overlay = Tooltip
                                            .text("Website")
                                        )(Glyphicon("ExternalLink"))
                                      )
                                    else ""
                                  )
                                ),
                                Col(md = 2)(
                                  <.div(
                                    if (RoleOrdering
                                          .gteq(role, "owner"))
                                      Button(
                                        variant = "link",
                                        href = s"/$dbName/settings"
                                      )(
                                        OverlayTrigger(
                                          overlay = Tooltip.text("Settings")
                                        )(Glyphicon("Settings"))
                                      )
                                    else ""
                                  )
                                ),
                                Col(md = 2)(
                                  <.div(
                                    if (isDemo)
                                      OverlayTrigger(
                                        overlay = Tooltip.text(
                                          "Trial dataset (some functionalities are limited)."
                                        )
                                      )(Glyphicon("Unlock"))
                                    else
                                      ""
                                  )
                                )
                              )
                            )
                          )
                      }: _*
                  )
                )
              )
            case None =>
              Spin()
          }
        }
      <.div(
        ToastsComp(s.toasts, deleteToast),
        modal,
        table
      )
    }
  }

  private val component =
    ScalaComponent
      .builder[Props]
      .initialState(State())
      .renderBackend[Backend]
      .componentDidMount(lf => {
        val p = lf.props
        val userInfoAsync = loadUserInfo(p.dbApi)
          userInfoAsync.zip(loadDbs(p.dbApi))
            .flatMap { case (ui, datasets) =>
              lf.modStateAsync(_.copy(datasets = Some(datasets), userInfo = ui))
            }
            .handleError { err =>
              lf.backend.error(err).asAsyncCallback
            }
          .toCallback
      })
      .build

  def apply(apiEndpoint: String,
            accessToken: Option[String],
            loginUrl: Option[String],
            logoutUrl: Option[String],
            resourceUrl: String) =
    component(Props(DatabaseApi(apiEndpoint, accessToken), ResourceUrl(resourceUrl), loginUrl, logoutUrl))

  @JSExport("renderInto")
  def renderInto(apiEndpoint: String,
                 accessToken: String,
                 loginUrl: String,
                 logoutUrl: String,
                 resourceUrl: String,
                 elemId: String = "entrypoint") = {
    apply(apiEndpoint,
      if (accessToken.isEmpty) None else Some(accessToken),
      if (loginUrl.isEmpty) None else Some(loginUrl),
      if (logoutUrl.isEmpty) None else Some(logoutUrl),
      resourceUrl)
      .renderIntoDOM(dom.document.getElementById(elemId))
  }
}
