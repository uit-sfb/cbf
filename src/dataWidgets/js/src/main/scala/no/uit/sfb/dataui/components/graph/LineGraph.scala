package no.uit.sfb.dataui.components.graph

import japgolly.scalajs.react.{Callback, ~=>}
import japgolly.scalajs.react.vdom.html_<^._
import no.uit.sfb.dataui.utils.graph.{GraphLike, PropsLike}
import no.uit.sfb.dataui.utils.com.{ADTNull, DatabaseApi}
import no.uit.sfb.facade.d3.format.Format
import no.uit.sfb.facade.reactvis.obj.Point
import no.uit.sfb.facade.reactvis.series.LineMarkSeries
import no.uit.sfb.facade.reactvis._

import scala.scalajs.js
import scala.scalajs.js.{Date, undefined, |}

case class PropsLineGraph(dbApi: DatabaseApi,
                          ver: Option[String],
                          url: String,
                          xAxis: String = "x",
                          xType: String = "ordinal",
                          yAxis: String = "y",
                          yType: String = "linear",
                          lineColor: String = "",
                          strokeStyle: String = "dashed",
                          scientificNotation: Boolean = false,
                          percentNotation: Boolean = false,
                          forceY0: Boolean = false,
                          height: Int = 600,
                          error: Throwable ~=> Callback)
    extends PropsLike

object LineGraph extends GraphLike[PropsLineGraph, Point] {

  def renderGraph(p: PropsLineGraph, s: State, b: Backend): VdomNode = {
    val timal = Seq("time", "time-utc").contains(p.xType)
    val graph = s.graph.get
    def fn(v: String) =
      if (p.percentNotation)
        Format(".2%")
          .asInstanceOf[js.Function1[Double | Int | String, String]](v)
      else
        v
    XYPlot(
      margin = if (timal) js.Dynamic.literal(bottom = 120) else undefined,
      xType = p.xType,
      yType = p.yType,
      onMouseDown = _ => b.onChangeHintValue(None),
      height = p.height
      //colorType = "category",
      //colorDomain = Seq(0, 1, 2, 3, 4, 5),
    )(
      graph.allKeys.zipWithIndex
        .map {
          case (key, idx) =>
            LineMarkSeries(
              data = graph
                .data(key, fx = {
                  case ADTNull() =>
                    None
                  case v =>
                    Some(v)
                })
                .map {
                  case (x, y) =>
                    new Point(
                      if (timal) Date.parse(x.toString) else x,
                      y,
                      if (timal) x else undefined,
                      opacity = if (s.hoverValue.exists { v =>
                                      v.x == x || v.x0 == x
                                    } && s.hoverValue.exists { v =>
                                      v.y == y
                                    }) {
                        1.0
                      } else
                        0.7,
                      label = if (graph.allKeys.size > 1) key else undefined
                    )
                },
              curve = "curveMonotoneX",
              strokeStyle = p.strokeStyle,
              color =
                if (p.lineColor.nonEmpty) p.lineColor
                else
                  Palette(idx),
              onValueMouseOver =
                (datapoint, _) => b.onChangeHintValue(Some(datapoint)),
              onValueMouseOut = (datapoint, _) => {
                //When working with multiple lines in one graphs, for some reason, onValueMouseOut is triggered for points that are on the same X as the mouse, but at different Y
                //Therefore we need to filter out the event to match only the current Y position.
                if (s.hoverValue.exists { _.y == datapoint.y }) {
                  b.onChangeHintValue(None)
                } else {
                  Callback.empty
                }
              }
            )()
        } ++ Seq(
        LineMarkSeries(
          data =
            if (p.forceY0) {
              val y_0 = graph.allKeys.head
              graph.data(y_0).headOption.toSeq.map {
                case (x, _) => new Point(x, 0, opacity = 0)
              }
            } else
              Seq()
        )(),
        XAxis(
          p.xAxis,
          tickLabelAngle = if (timal) -45 else 0,
          scientificNotation = !timal
        )(),
        YAxis(
          p.yAxis,
          scientificNotation = p.scientificNotation,
          percentNotation = p.percentNotation,
        )()
      ) ++
        s.hoverValue.map { v =>
          val yVal: String = v.y0.map { _.toString }.getOrElse(v.y.toString)
          Hint(v)(
            <.div(
              ^.background := "black",
              if (v.label.isDefined)
                <.div(v.label)
              else <.div(),
              <.div(
                s"${p.xAxis}: ${v.x0.map { _.toString }.getOrElse(v.x.toString)}"
              ),
              <.div(s"${p.yAxis}: ${fn(yVal)}")
            )
          )
        }.toSeq: _*
    )
  }

  def apply(dbApi: DatabaseApi,
            ver: Option[String],
            xAxisVars: Tuple3[String, Option[String], String],
            yAxisVars: Tuple3[String, Option[String], String],
            filter: Tuple2[Option[String], String],
            sort: Option[String] = None,
            xAxis: String = "",
            xType: String = "ordinal",
            yAxis: String = "",
            yType: String = "linear",
            scientificNotation: Boolean = false,
            percentNotation: Boolean = false,
            forceY0: Boolean = false, //Force Y axis to include 0
            height: Int = 600,
            error: Throwable ~=> Callback) = {
    val urlAbs = dbApi
      .graph(ver, Seq(xAxisVars, yAxisVars), filter = Seq(filter), sort = sort)
      .toString()
    component(
      PropsLineGraph(
        dbApi,
        ver,
        url = urlAbs,
        xAxis = xAxis,
        xType = xType,
        yAxis = yAxis,
        yType = yType,
        scientificNotation = scientificNotation,
        percentNotation = percentNotation,
        forceY0 = forceY0,
        height = height,
        error = error
      )
    )
  }
}
