package no.uit.sfb.dataui.components.settings

import japgolly.scalajs.react.Ref.Simple
import japgolly.scalajs.react.component.Scala.BackendScope
import japgolly.scalajs.react.vdom.html_<^._
import japgolly.scalajs.react._
import no.uit.sfb.cbf.shared.config.{Constants, CustomType, DatasetConfig}
import no.uit.sfb.cbf.shared.types.{Enum, InternalType, StarTypeLike}
import no.uit.sfb.dataui.utils.comp.SelectOption
import no.uit.sfb.dataui.utils.comp.{ButtonField, SelectField, TextField, UncontrolledFormControlWithValidation}
import no.uit.sfb.facade.bootstrap.Table
import no.uit.sfb.facade.bootstrap.dropdown._
import no.uit.sfb.facade.bootstrap.form._
import no.uit.sfb.facade.bootstrap.inputGroup._
import no.uit.sfb.facade.bootstrap.overlay.{OverlayTrigger, Tooltip}
import no.uit.sfb.facade.icon.Glyphicon
import org.scalajs.dom
import org.scalajs.dom.html
import org.scalajs.dom.raw.HTMLInputElement

object CustomTypesSettingsComp {

  case class Props(draft: DatasetConfig,
                   updateDraft: DatasetConfig => Callback,
                   noEdit: Boolean)
  case class State(customTypeSelect: StarTypeLike = Enum(), selector: String = "")

  class Backend($ : BackendScope[Props, State]) {
    private val ref: Simple[html.Input] = Ref[html.Input]

    val changeCustomTypeSelect = Reusable.fn(
      (select: StarTypeLike) =>
        $.modState { (s, p) =>
          s.copy(customTypeSelect = select)
      }
    )

    def changeSelector(newSelector: String) =
      $.modState(_.copy(selector = newSelector))

    protected def renderSelectedItem(p: Props, ct: CustomType) = {
      val ctRef = ct.ref
      val it = InternalType(ctRef).map { _.asInstanceOf[StarTypeLike] }
      val name = it.map { _.shortName }.getOrElse("Unknown")
      if (it.isEmpty)
        dom.console.warn(s"'$ctRef' is not an internal type")
      <.tr(
        <.td(
          if (p.noEdit) <.div()
          else
            ButtonField(
              variant = "danger",
              icon = Glyphicon("Trash"),
              onClick =
                CallbackTo {
                  p.draft.copy(
                    customTypes =
                      p.draft.customTypes.filter(_.ref != ct.ref)
                  )
                } >>=
                  p.updateDraft,
              info = "Remove type"
            ),
          s" ${ct.ref}"
        ),
        <.td(
          InputGroup()(
            InputGroupText()(name),
            //Do NOT convert to TextFieldUncontrolled as it would interfere with the InputGroup
            UncontrolledFormControlWithValidation(
              disabled = p.noEdit,
              id = s"customTypeNameInput-$ctRef",
              value = it.map { _.label }.getOrElse(ctRef),
              placeholder = "Type name",
              onChange = value =>
                CallbackTo {
                  p.draft.copy(customTypes = p.draft.customTypes.map {
                    t =>
                      if (t.ref != ct.ref)
                        t
                      else
                        ct.copy(ref = it.map { _.shortRef } match {
                          case Some(st) => s"$st:$value"
                          case None     => value
                        })
                  })
                } >>=
                  p.updateDraft
            )
            //)
          )
        ),
        <.td(
          <.div(
            it.map { _.customFields }
              .getOrElse(Map())
              .map {
                case (k, field) =>
                  val v = ct.kvp.getOrElse(k, field.default)
                  val selectId = s"customType-$k-$v"
                  FormGroup(selectId)(
                    if (field.values.isEmpty) {
                      if (field.multiline) {
                        TextField(
                          label = k.capitalize,
                          value = v.split('|').filter{_.nonEmpty}.toSet.toSeq.sorted.mkString("\n"),
                          disabled = if (p.noEdit) Some("") else None,
                          onChange = value =>
                            CallbackTo {
                              p.draft.copy(
                                customTypes = p.draft.customTypes.map { t =>
                                  if (t.ref != ct.ref)
                                    t
                                  else
                                    ct.copy(kvp = ct.kvp ++ Map(k -> value.split('\n').filter{_.nonEmpty}.toSet.toSeq.sorted.mkString("|")))
                                }
                              )
                            } >>=
                              p.updateDraft,
                          textAreaLines = Some(5)
                        )()
                      } else {
                        TextField(
                          label = k.capitalize,
                          value = v,
                          disabled = if (p.noEdit) Some("") else None,
                          onChange = value =>
                            CallbackTo {
                              p.draft.copy(
                                customTypes = p.draft.customTypes.map { t =>
                                  if (t.ref != ct.ref)
                                    t
                                  else
                                    ct.copy(kvp = ct.kvp ++ Map(k -> value))
                                }
                              )
                            } >>=
                              p.updateDraft
                        )()
                      }
                    } else
                      SelectField(
                        disabled = if (p.noEdit) Some("") else None,
                        label = k.capitalize,
                        value = v,
                        onChange = newValue =>
                          CallbackTo {
                            p.draft.copy(
                              customTypes = p.draft.customTypes.map { t =>
                                if (t.ref != ct.ref)
                                  t
                                else
                                  ct.copy(
                                    kvp = ct.kvp ++ Map(k -> newValue)
                                  )
                              }
                            )
                          } >>=
                            p.updateDraft,
                        options = field.values.map { SelectOption(_) }
                      )(),
                    FormText()(field.descr)
                  )
              }
              .toSeq: _*
          )
        )
      )
    }

    def render(p: Props, s: State): VdomElement = {
      <.div(
        Table(hover = true)(
          <.thead(
            <.tr(
              <.th(OverlayTrigger(
                overlay = Tooltip
                  .text("Click on a line to edit.")
              )(
                "Reference",
                <.span(^.paddingLeft := "5px"),
                Glyphicon("Help")
              )
              ),
              <.th("Name"),
              <.th("Type")
            )
          ),
          <.tbody(p.draft.customTypes.map {
          case ct if ct.ref == s.selector =>
            renderSelectedItem(p, ct)
          case ct =>
            val it = InternalType(ct.ref).map { _.asInstanceOf[StarTypeLike] }
            <.tr(^.onClick --> changeSelector(ct.ref),
              <.td(ct.ref),
              <.td(it.map{_.label}.getOrElse(""): String),
              <.td(it.map{_.shortName}.getOrElse(""): String))
        }: _*)),
        if (p.noEdit) <.div()
        else
          FormGroup("newCustomTypeNameInput")(
            InputGroup()(
              DropdownButton(
                "dropdownCustomTypeFlavorInput",
                s.customTypeSelect.shortName,
                variant = "outline-secondary"
              )(Constants.customTypes.map { t =>
                DropdownItem(
                  active = s.customTypeSelect == t,
                  onClick = _ => changeCustomTypeSelect(t)
                )(t.shortName)
              }: _*),
              //Do NOT convert to a TextFieldUncontrolled as it would interfere with the layout of the Add button
              FormControl.uncontrolled(ref, placeholder = "Custom type name")(),
              ButtonField(
                variant = "outline-primary",
                icon = "Add",
                onClick =
                  CallbackTo {
                    val value = {
                      val elem = dom.document
                        .getElementById("newCustomTypeNameInput")
                        .asInstanceOf[HTMLInputElement]
                      val tmp = elem.value
                      //We rest the input
                      elem.value = elem.defaultValue
                      tmp
                    }
                    val newRef = s"${s.customTypeSelect.shortRef}:$value"
                    val exists = p.draft.customTypes.exists(_.ref == newRef)
                    if (value.nonEmpty && !exists) {
                      p.draft.copy(
                        customTypes = p.draft.customTypes :+ CustomType(
                          newRef,
                          Map()
                        )
                      )
                    } else {
                      if (exists)
                        dom.window.alert(s"Item '$value' already exists.")
                      p.draft
                    }
                  } >>=
                    p.updateDraft,
                info = "New custom type"
              )
            )
          )
      )
    }
  }

  private lazy val component = ScalaComponent
    .builder[Props]
    .initialState(State())
    .renderBackend[Backend]
    .build

  def apply(draft: DatasetConfig,
            updateDraft: DatasetConfig => Callback,
            noEdit: Boolean) =
    component(Props(draft, updateDraft, noEdit))
}
