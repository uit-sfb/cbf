package no.uit.sfb.dataui.components.filter

import japgolly.scalajs.react._
import japgolly.scalajs.react.vdom.html_<^._
import no.uit.sfb.cbf.shared.config.{AttributeDef, DatasetConfig}
import no.uit.sfb.facade.bootstrap.Table
import no.uit.sfb.facade.bootstrap.buttons.Button
import no.uit.sfb.facade.bootstrap.form.FormControl
import no.uit.sfb.facade.bootstrap.grid._
import no.uit.sfb.facade.bootstrap.overlay.{OverlayTrigger, Tooltip}
import no.uit.sfb.facade.icon.Glyphicon

object DisjunctionsComp {
  case class Props(statements: Seq[String], removeStatement: String => Callback)

  class Backend($ : BackendScope[Props, Unit]) {

    def render(p: Props) = {
      <.div(Table() {
        <.tbody(p.statements.map {
          stm =>
            <.tr(
              <.td(
                Container()(
                  Row()(
                    Col()("OR"),
                    Col(sm = 10)(stm),
                    Col()(
                      OverlayTrigger(
                        overlay = Tooltip.text("Remove statement")
                      )(
                        Button(
                          size = "sm",
                          variant = "outline-danger",
                          onClick = e =>
                            e.stopPropagationCB >> p.removeStatement(stm)
                        )(Glyphicon("Trash"))
                      )
                    ),
                  )
                )
              )
            )
        }: _*)
      })
    }
  }

  private val component = ScalaComponent
    .builder[Props]
    .renderBackend[Backend]
    .build

  def apply(statements: Seq[String], removeStatement: String => Callback) =
    component(Props(statements, removeStatement))
}
