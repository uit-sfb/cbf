# dataWidgets

Frontend UI components (or widgets) for user interaction with the databases. The backend part (server) is only
meant for testing purpose.

## Requirements

- Java 11 or later
- NPM & NodeJS 12 or later

## Getting started

Start up [databaseApi on port 9000](https://gitlab.com/uit-sfb/cbf/-/blob/master/src/databaseApi/README.md#getting-started)
Then run:
- `cd src`
- `sbt "dataWidgetsJVM / run 9002"`

The UI should be available at `http://localhost:9002`. SBT is able to detect
code change and compile on-the-fly.

## Geo

Topojson files MUST:

- contain an object called `main` (which is the only object displayed)
- contain a property `name` which is used to label features
- other properties may be present

## Notes about the bundling process

Steps:

- scalaJS compilation (scala -> JS): [sbt-scalajs plugin](http://www.scala-js.org/doc/sbt-plugin.html)
- JS transpilation and bundling: [scalajs-bundler](https://scalacenter.github.io/scalajs-bundler/)

Transpilation is performed by [webpack](https://webpack.js.org/concepts/) loaders (ts-loader, babel-loader,
source-map-loader, ...) and consists in converting flavors of JS -> plain old JS. In our case, we use `babel-loader`
followed by `source-map-loader`. The former comes from our custom webpack config (`webpack.config.js`), while the latter
is automatically added by the plugin to the default webpack config.

In terms of commands:

- To compile scala -> js: `fastLinkJS`.
- To transpile/bundle the code after compilation: `fastOptJS::webpack`.

