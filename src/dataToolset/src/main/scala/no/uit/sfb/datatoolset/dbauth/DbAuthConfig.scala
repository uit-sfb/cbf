package no.uit.sfb.datatoolset.dbauth

import no.uit.sfb.cbf.shared.model.v1.builder.DatasetConfigLike

case class DbAuthConfig(
  id: String = null,
  permission: String = "none", //none < read < write <  owner
  add: Boolean = true, //True to upsert authorization, false to delete authorization
  dbName: String = null,
  dbSsl: Boolean = false,
  dbHost: String = "localhost:27017",
  dbUserName: String = "admin",
  dbUserPassword: String = "salvador"
) extends DatasetConfigLike
