package no.uit.sfb.datatoolset.dbreleaseAll

import com.typesafe.scalalogging.LazyLogging
import no.uit.sfb.cbf.mongodb.MongodbConnect
import io.circe.generic.auto._
import io.circe.syntax._
import no.uit.sfb.cbf.mongodb.CbfDataset
import no.uit.sfb.cbf.shared.errors.BadRequestClientError
import no.uit.sfb.cbf.shared.model.v1.versioninfo.VersionInfo
import no.uit.sfb.cbf.shared.version.CbfVersion
import org.mongodb.scala.Document
import org.mongodb.scala.model.Filters
import org.mongodb.scala.model

import scala.concurrent.duration._
import scala.concurrent.{Await, ExecutionContext, Future}

object DbReleaseAllAction extends LazyLogging {
  implicit val ec = ExecutionContext.global

  def apply(cfg: DbReleaseAllConfig): Unit = {
    lazy val mgc = MongodbConnect(
      cfg.dbHost,
      cfg.dbUserName,
      cfg.dbUserPassword,
      "admin",
      cfg.dbSsl
    )
    try {

      val cbfs = cfg.dbNames.map { dbName =>
        new CbfDataset(mgc.db(dbName))
      }

      lazy val versionF =
        if (cfg.version.endsWith(".x")) {
          Future
            .sequence(cbfs.map {
              _.versions(true)
            })
            .map(_.flatten)
            .map {
              _.filter(
                v =>
                  (!v.deprecated && v._id
                    .startsWith(cfg.version.split('.').head))
              ).sorted(VersionInfo).lastOption //should be the latest version
            }
            .map {
              case Some(v) => CbfVersion(v._id)
              case None =>
                throw BadRequestClientError(s"No branch ${cfg.version} found!")
            }

        } else {
          Future(CbfVersion(cfg.version))
        }

      /**
        * Releasing version for multiple databases ( convert Seq[Future] into Future[Seq] )
        *
        * */
      def releaseAllF(cbfVersion: CbfVersion) =
        Future.sequence(cbfs.map { cbf =>
          cbf
            .version(cbfVersion.ver, false)
            .flatMap {
              case Some(_) =>
                logger.warn(
                  s"Version '${cbfVersion.ver} already exists. Release command has no effect."
                )
                Future.successful(())

              case None =>
                cbf.versionsCol
                  .replace(
                    Filters.eq("_id", cbfVersion.ver),
                    Document(VersionInfo.now(cbfVersion.ver).asJson.noSpaces),
                    true
                  )
                  .map { r =>
                    println(s"Released version:${cbfVersion.ver}")
                    r
                  }
            }
            .flatMap { _ =>
              if (cfg.createNextDraft) {
                val nextVer = cbfVersion.incr
                cbf.versionsCol
                  .replace(
                    Filters.eq("_id", nextVer.toString),
                    Document(
                      VersionInfo.draft(nextVer.toString).asJson.noSpaces
                    ),
                    true
                  )
              } else Future.successful(())
            }
        })

      def getNewOrModifiedRecordsF(cbfVersion: CbfVersion) = cbfs.map { cbf =>
        cbf
          .recordsCol(cbfVersion)
          .get(
            model.Filters.or(
              model.Filters.eq("valid.start", cbfVersion.release),
              model.Filters.eq("valid.end", cbfVersion.release)
            )
          )
      }

      /**
        * If at least one record in any of the listed databases was added/modified, make a new release
        *
        * */
      val f =
        versionF.flatMap { cbfVersion =>
          println(s"Releasing version '${cbfVersion.ver}'...")
          if (cfg.ifModified) {
            Future
              .sequence(getNewOrModifiedRecordsF(cbfVersion))
              .map(_.flatten)
              .flatMap {
                case resultsSeq: Seq[Document] if resultsSeq.nonEmpty =>
                  releaseAllF(cbfVersion)
                case _ =>
                  logger.warn("No records were added/modified")
                  Future.successful(())
              }

          } else {
            releaseAllF(cbfVersion)
          }

        }
      Await.result(f, 3.minute) //Not shorter otherwise it becomes complicated to debug connections errors

    } finally mgc.close

  }

}
