package no.uit.sfb.datatoolset.dbpatch

import com.typesafe.scalalogging.LazyLogging
import io.circe.generic.extras.Configuration
import no.uit.sfb.cbf.bulktransform.{TrPatch, TransformPushErrorsLike}
import no.uit.sfb.cbf.mongodb.{CbfDataset, MongodbConnect}
import no.uit.sfb.cbf.shared.utils.IteratorWithCallback

import java.util.UUID
import scala.concurrent.ExecutionContext
import scala.io.{BufferedSource, Source}

object DbPatchAction extends LazyLogging {
  implicit val ec = ExecutionContext.global
  implicit private val customConfig: Configuration =
    Configuration.default.withDefaults

  def apply(cfg: DbPatchConfig): Unit = {
    lazy val mgc = MongodbConnect(
      cfg.dbHost,
      cfg.dbUserName,
      cfg.dbUserPassword,
      "admin",
      cfg.dbSsl
    )
    try {
      val inputIt = cfg.in.getFileName.toString.split('.').last match {
        case "tsv" =>
          val io: BufferedSource = Source.fromFile(cfg.in.toFile)
          new IteratorWithCallback(io.getLines(), io.close())
        case other =>
          throw new Exception(
            s"${cfg.in.getFileName.toString}: File extension not supported: `$other`"
          )
      }
      val mdb = new CbfDataset(mgc.db(cfg.dbName))
      val conf = mdb.config().get
      val idAttr = conf.attr("id").get
      def getNext =
        if (idAttr.lType.isSubtypeOf("num")) {
          val i = mdb.counterKeySeed.incrementAndGet()
          if (i > 999999999)
            throw new Exception(
              "Automatic Id range may include at most 9 digits."
            )
          //If this exception happens, the solution is to increase the number of digits below.
          f"$i%09d"
        } else {
          UUID.randomUUID().toString
        }

      val transform =
        new TrPatch(mgc, cfg.dbName, cfg.version, inputIt, getNext, cfg.force)
        with TransformPushErrorsLike
      transform.run()
    } finally {
      mgc.close
    }
  }
}
