package no.uit.sfb.datatoolset.cdch

import akka.NotUsed
import akka.stream.scaladsl.Source
import no.uit.sfb.cbf.shared.config.AttributeDef
import no.uit.sfb.cbf.shared.model.v1.builder.RecordBuilder
import no.uit.sfb.cbf.shared.model.v1.nativ.Record

trait DataSourceLike {
  def source: Source[Record, NotUsed]

  def rb: RecordBuilder =
    new RecordBuilder(Seq("acc:genbank", "id").map { k =>
      AttributeDef(k, k)
    })
}
