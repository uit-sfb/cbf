package no.uit.sfb.datatoolset.dbpush

import com.typesafe.scalalogging.LazyLogging
import no.uit.sfb.cbf.bulktransform.{
  ErrorUtils,
  TrPush,
  TransformPushErrorsLike
}
import no.uit.sfb.cbf.mongodb.{CbfDataset, MongodbConnect}
import no.uit.sfb.cbf.shared.utils.IteratorWithCallback
import no.uit.sfb.scalautils.common.FileUtils

import java.io.{FileOutputStream, PrintStream}
import java.util.UUID
import scala.concurrent.Await
import scala.concurrent.duration._
import scala.concurrent.ExecutionContext
import scala.io.{BufferedSource, Source}

object DbPushAction extends LazyLogging {
  implicit val ec = ExecutionContext.global

  def apply(cfg: DbPushConfig): Unit = {
    lazy val mgc = MongodbConnect(
      cfg.dbHost,
      cfg.dbUserName,
      cfg.dbUserPassword,
      "admin",
      cfg.dbSsl
    )

    try {
      val inputIt = cfg.in.getFileName.toString.split('.').last match {
        case "tsv" =>
          val io: BufferedSource = Source.fromFile(cfg.in.toFile)
          new IteratorWithCallback(io.getLines(), io.close())
        case other =>
          throw new Exception(
            s"${cfg.in.getFileName.toString}: File extension not supported: `$other`"
          )
      }
      val mdb = new CbfDataset(mgc.db(cfg.dbName))
      val conf = mdb.config().get
      val idAttr = conf.attr("id").get
      def getNext =
        if (idAttr.lType.isSubtypeOf("num")) {
          val i = mdb.counterKeySeed.incrementAndGet()
          if (i > 999999999)
            throw new Exception(
              "Automatic Id range may include at most 9 digits."
            )
          //If this exception happens, the solution is to increase the number of digits below.
          f"$i%09d"
        } else {
          UUID.randomUUID().toString
        }
      lazy val transform =
        new TrPush(mgc, cfg.dbName, cfg.version, inputIt, getNext, cfg.force)
        with TransformPushErrorsLike {
          override protected def pre() = {
            super.pre()
            Await.result(db.errorsCol(cbfVersion).drop(), 3.minute)
          }

          override protected def post() = {
            val totalErrors =
              Await.result(db.errorsCol(cbfVersion).count(), 1.minute) //Not shorter otherwise it become complicated to debug connections errors
            //logger.warn("Because an error occurred, the data will be deleted...")
            //Await.result(col.drop(), 4.minutes)
            //logger.warn(s"Collection ${cfg.version} dropped")
            logger.info("Writing error file...")
            if (totalErrors > 0) {
              val output = cfg.out.resolve("errors.tsv")
              FileUtils.createParentDirs(output)
              val os = new PrintStream(new FileOutputStream(output.toFile))
              ErrorUtils.generateErrorTsv(db.errorsCol(cbfVersion)) foreach {
                os.println
              }
              os.close()
              logger.warn(
                s"$totalErrors errors found! See details in output files."
              )
              throw new Exception(s"$totalErrors errors found.")
            } else
              logger.info("Successfully wrote all records to Mongodb")
          }
        }
      transform.run()
    } finally mgc.close
  }
}
