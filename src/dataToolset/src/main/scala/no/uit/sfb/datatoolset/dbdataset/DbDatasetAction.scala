package no.uit.sfb.datatoolset.dbdataset

import com.typesafe.scalalogging.LazyLogging
import io.circe.generic.extras.Configuration
import io.circe.generic.extras.auto._
import io.circe.syntax._
import no.uit.sfb.cbf.mongodb.{CbfDataset, MongodbConnect, SharedDatabase}
import no.uit.sfb.cbf.shared.config.{AttributeDef, CustomResolver, CustomType, DatasetConfig, DefaultAttrs, Tab}
import no.uit.sfb.cbf.shared.dataset.DatasetRegistration
import no.uit.sfb.cbf.shared.model.v1.nativ.CompactIRI
import no.uit.sfb.cbf.shared.utils.Date
import no.uit.sfb.cbf.utils.UriUtils
import org.mongodb.scala.Document

import scala.concurrent.{Await, ExecutionContext}
import org.mongodb.scala.model.Filters

import scala.concurrent.duration._
import scala.util.Try

object DbDatasetAction extends LazyLogging {
  implicit val ec = ExecutionContext.global

  def apply(cfg: DbDatasetConfig): Unit = {
    implicit val config: Configuration = Configuration.default.withDefaults
    logger.info(s"Executing DatasetConfigAction with $cfg")

    lazy val mgc = MongodbConnect(
      cfg.dbHost,
      cfg.dbUserName,
      cfg.dbUserPassword,
      "admin",
      cfg.dbSsl
    )

    val f = {
      val rawConfig: DatasetConfig = DatasetConfig(UriUtils.getContent(cfg.config))

      //Later defined attributes replace earlier definitions
      //And we place '_' attrs at the very end (after resolving duplicates)
      def removeDuplicates(attrs: Seq[AttributeDef]): Seq[AttributeDef] = {
        def moduloStar(s: String): String = {
          if (s.startsWith("*"))
            s.drop(1)
          else
            s
        }

        //We resolve duplicates
        val tmp = attrs.foldRight(Seq[AttributeDef]()) {
          case (attr, acc) =>
            if (acc.exists(a => moduloStar(a.ref) == moduloStar(attr.ref)))
              acc
            else
              attr +: acc
        }

        //We place '_' attributes at the end (no need to care about '*' since they will be partitioned later on)
        val (underscore, regular) = tmp.partition {
          _.ref.startsWith("*")
        }
        regular ++ underscore
      }


      val dbName = cfg.dbNameOverride.getOrElse(rawConfig.name)
      SharedDatabase(mgc).datasets
        .insert(DatasetRegistration(dbName, Date.nowDate(), Date.nowDate())) match {
        case _ =>
          //We do not check the result of the previous op because it may perfectly have failed in case it existed already
          val db =
            new CbfDataset(mgc.db(dbName))
          val col = db.configsCol
          col
            .replace(
              Filters.eq("_id", rawConfig._id),
              Document(rawConfig.asJson.noSpaces),
              true
            )
            .map { _ =>
              ()
            }
      }
    }

    try {
      Await.result(f, 1.minute) //Not shorter otherwise it become complicated to debug connections errors
    } finally mgc.close
  }
}
