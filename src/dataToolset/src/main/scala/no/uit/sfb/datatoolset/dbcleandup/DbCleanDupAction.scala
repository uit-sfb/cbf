package no.uit.sfb.datatoolset.dbcleandup

import com.typesafe.scalalogging.LazyLogging
import no.uit.sfb.cbf.mongodb.{CbfDataset, MongodbConnect}
import org.mongodb.scala.model.Filters

import scala.concurrent.duration.DurationInt
import scala.concurrent.{Await, ExecutionContext, Future}
import scala.util.Try
import no.uit.sfb.cbf.shared.model.v1.nativ.Record

object DbCleanDupAction extends LazyLogging {
  implicit val ec = ExecutionContext.global

  def apply(cfg: DbCleanDupConfig): Unit = {
    lazy val mgc = MongodbConnect(
      cfg.dbHost,
      cfg.dbUserName,
      cfg.dbUserPassword,
      "admin"
    )

  /*
  Try block to catch an exception
   */
  try {

    val cbf = new CbfDataset(mgc.db(cfg.dbName))

    val getVersion = (v:String) => Try(cbf.parseVersion(v)).toOption match {
      case Some(v) => v
      case _ => throw new IllegalArgumentException("Failed to convert version!")
    }

    logger.info(s"Remove duplicates from collection ${cfg.version}")

    val version = getVersion(cfg.version)

    /*
    Check against all database
     */

     cbf.recordsCol(version).getIt().foreach { doc =>
      val f = cbf.recordsCol(version).get(Filters.eq("id", doc.last._2)).flatMap{
        case records if records.length > 1 => cbf.recordsCol(version).delete(Filters.eq("id", doc.last._2), true)
        case records => Future.successful(())
      }
      Await.result(f,cfg.limit.toInt.minutes)
    }
  } finally mgc.close


  }
}