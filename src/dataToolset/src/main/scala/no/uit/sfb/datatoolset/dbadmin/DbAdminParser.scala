package no.uit.sfb.datatoolset.dbadmin

import com.typesafe.scalalogging.LazyLogging
import scopt.OParser

object DbAdminParser extends LazyLogging {

  lazy val builder = OParser.builder[DbAdminConfig]

  lazy val parser = {
    import builder._
    OParser
      .sequence(
        head("Manage admin list"),
        help('h', "help")
          .text("Prints this usage text"),
        opt[Unit]('a', "add")
          .text("Add/update admin.")
          .action((_, cfg) => cfg.copy(add = true)),
        opt[Unit]('r', "rm")
          .text("Remove admin.")
          .action((_, cfg) => cfg.copy(add = false)),
        opt[String]("shared-db-name")
          .text("CBF shared db name (default: '_cbf')")
          .action((arg, cfg) => cfg.copy(sharedDbName = arg)),
        opt[String]("db-host")
          .text("Mongodb host.")
          .action((arg, cfg) => cfg.copy(dbHost = arg)),
        opt[String]("db-user-name")
          .text("Mongodb user name.")
          .action((arg, cfg) => cfg.copy(dbUserName = arg)),
        opt[String]("db-user-password")
          .text("Mongodb user password.")
          .action((arg, cfg) => cfg.copy(dbUserPassword = arg)),
        opt[Unit]("db-ssl")
          .text("Use SSL.")
          .action((_, cfg) => cfg.copy(dbSsl = true)),
        arg[String]("identifier")
          .required()
          .text("Elixir identifier"),
      )
  }
}
