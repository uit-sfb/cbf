package no.uit.sfb.datatoolset.json2tsv

import java.io.File

import com.typesafe.scalalogging.LazyLogging
import scopt.OParser

object Json2TsvParser extends LazyLogging {

  lazy val builder = OParser.builder[Json2TsvConfig]

  lazy val parser = {
    import builder._
    OParser.sequence(
      head("Convert provided JSON to TSV"),
      help('h', "help")
        .text("Prints this usage text"),
      opt[File]("in").required()
        .text("Path to input file")
        .action((arg, cfg) => cfg.copy(in = Some(arg.toPath))),
      opt[File]("out").required()
        .text("Path to output file")
        .action((arg, cfg) => cfg.copy(out = Some(arg.toPath))),
    )
  }
}
