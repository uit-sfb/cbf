package no.uit.sfb.datatoolset.validatation

import java.io.File
import java.net.URI

import com.typesafe.scalalogging.LazyLogging
import scopt.OParser

object ValidationParser extends LazyLogging {

  lazy val builder = OParser.builder[ValidationConfig]

  lazy val parser = {
    import builder._
    OParser.sequence(
      head("Validate JSON file with the provided JSON Schema"),
      help('h', "help")
        .text("Prints this usage text"),
      opt[String]("schema")
        .required()
        .text("Path to JSON schema")
        .action((arg, cfg) => cfg.copy(schema = new URI(arg))),
      opt[File]("in")
        .required()
        .text("Path to input file")
        .action((arg, cfg) => cfg.copy(in = arg.toPath))
    )
  }
}
