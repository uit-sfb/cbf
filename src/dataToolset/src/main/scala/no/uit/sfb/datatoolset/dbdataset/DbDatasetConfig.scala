package no.uit.sfb.datatoolset.dbdataset

import no.uit.sfb.cbf.shared.model.v1.builder.DatasetConfigLike

import java.net.URI

case class DbDatasetConfig(config: URI = null,
                           dbSsl: Boolean = false,
                           dbNameOverride: Option[String] = None,
                           dbHost: String = "localhost:27017",
                           dbUserName: String = "admin",
                           dbUserPassword: String = "salvador")
    extends DatasetConfigLike
