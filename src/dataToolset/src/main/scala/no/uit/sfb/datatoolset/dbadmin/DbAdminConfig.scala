package no.uit.sfb.datatoolset.dbadmin

import no.uit.sfb.cbf.shared.model.v1.builder.DatasetConfigLike

case class DbAdminConfig(
  id: String = null,
  add: Boolean = true, //True to upsert admin, false to delete admin
  sharedDbName: String = "_cbf",
  dbSsl: Boolean = false,
  dbHost: String = "localhost:27017",
  dbUserName: String = "admin",
  dbUserPassword: String = "salvador"
) extends DatasetConfigLike
