package no.uit.sfb.datatoolset.dbadmin

import com.typesafe.scalalogging.LazyLogging
import no.uit.sfb.cbf.mongodb.{MongodbConnect, SharedDatabase}
import org.mongodb.scala.Document
import org.mongodb.scala.model._

import scala.concurrent.duration._
import scala.concurrent.{Await, ExecutionContext}

object DbAdminAction extends LazyLogging {
  implicit val ec = ExecutionContext.global

  def apply(cfg: DbAdminConfig): Unit = {
    lazy val mgc = MongodbConnect(
      cfg.dbHost,
      cfg.dbUserName,
      cfg.dbUserPassword,
      "admin",
      cfg.dbSsl
    )
    try {
      val coll = SharedDatabase(mgc, cfg.sharedDbName).admin
      val f =
        if (cfg.add)
          coll.update(
            Filters.eq("_id", cfg.id),
            Document("_id" -> cfg.id),
            single = true,
            upsert = true
          )
        else
          coll.delete(Filters.eq("_id", cfg.id), true)
      Await.result(f, 1.minute) //Not shorter otherwise it become complicated to debug connections errors
    } finally mgc.close
  }
}
