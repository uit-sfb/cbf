package no.uit.sfb.datatoolset.json2tsv

import java.net.URI
import java.nio.file.Path

case class Json2TsvConfig(in: Option[Path] = None,
                          out: Option[Path] = None,
                          order: Option[URI] = None,
)
