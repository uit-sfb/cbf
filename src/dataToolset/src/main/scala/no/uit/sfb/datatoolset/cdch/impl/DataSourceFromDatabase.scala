package no.uit.sfb.datatoolset.cdch.impl

import akka.actor.ActorSystem
import akka.stream.scaladsl.Source
import no.uit.sfb.cbf.mongodb.{CbfDataset, Collection, MongodbConnect}
import no.uit.sfb.cbf.shared.model.v1.builder.RecordBuilder
import no.uit.sfb.cbf.transform.Expressed
import no.uit.sfb.cbf.transform.resolv.Resolution
import no.uit.sfb.datatoolset.cdch.DataSourceLike
import org.mongodb.scala.bson.Document
import org.mongodb.scala.model.Filters

import scala.concurrent.duration.DurationInt
import scala.concurrent.{Await, ExecutionContext}

class DataSourceFromDatabase(dsName: String, version: String)(
  implicit system: ActorSystem,
  mgc: MongodbConnect,
  rb: RecordBuilder
) extends DataSourceLike {
  implicit val ec: ExecutionContext = system.dispatcher

  val cdb: CbfDataset = new CbfDataset(mgc.db(dsName))

  val source = {
    val cbfVersion = Await
      .result(
        cdb
          .version(version, true),
        30.seconds
      ) match {
      case Some(vi) =>
        cdb.parseVersion(vi._id)
      case None =>
        throw new Exception(
          s"Version '$version' does not exist for dataset '$dsName'."
        )
    }
    val resolver = {
      val datasetConfig = cdb.config().get
      new Resolution(mgc, datasetConfig.customResolvers)
    }

    implicit val expressor =
      new Expressed(resolver, dsName, version, "")
    val col: Collection[Document] =
      cdb.recordsCol(cbfVersion)

    val it = {
      col
        .getIt(
          filter = Filters.eq("valid.start", cbfVersion.release) //We only send new or modified records
        )
        .map { doc =>
          expressor(rb.fromSingleJson(doc.toJson()))
        }
    }

    Source.fromIterator(() => it)
  }
}
