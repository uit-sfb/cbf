package no.uit.sfb.datatoolset.tsv2json

import java.io.File

import com.typesafe.scalalogging.LazyLogging
import scopt.OParser

object Tsv2JsonParser extends LazyLogging {

  lazy val builder = OParser.builder[Tsv2JsonConfig]

  lazy val parser = {
    import builder._
    OParser.sequence(
      head("Convert provided TSV to JSON"),
      help('h', "help")
        .text("Prints this usage text"),
      opt[File]("in").required()
        .text("Path to input file")
        .action((arg, cfg) => cfg.copy(in = Some(arg.toPath))),
      opt[File]("out").required()
        .text("Path to output file")
        .action((arg, cfg) => cfg.copy(out = Some(arg.toPath))),
      opt[Int]("pos")
        .text("Position of the record to convert (starting from 1).")
        .action((arg, cfg) => cfg.copy(pos = Some(arg)))
    )
  }
}
