package no.uit.sfb.datatoolset.dbpush

import java.io.File

import com.typesafe.scalalogging.LazyLogging
import scopt.OParser

object DbPushParser extends LazyLogging {

  lazy val builder = OParser.builder[DbPushConfig]

  lazy val parser = {
    import builder._
    OParser.sequence(
      head(
        "Push data from TSV table (existing matching records will be replaced)"
      ),
      help('h', "help")
        .text("Prints this usage text"),
      opt[File]("in")
        .required()
        .text("Path to input file containing all entries (TSV)")
        .action((arg, cfg) => cfg.copy(in = arg.toPath)),
      opt[File]("out")
        .text("Path to output dir")
        .action((arg, cfg) => cfg.copy(out = arg.toPath)),
      opt[String]("ver")
        .required()
        .text("The version being worked on.")
        .action((arg, cfg) => cfg.copy(version = arg)),
      opt[Unit]('f', "force")
        .text("Force push when data has already been released.")
        .action((_, cfg) => cfg.copy(force = true)),
      opt[String]("db-name")
        .required()
        .text("Name of the database.")
        .action((arg, cfg) => cfg.copy(dbName = arg)),
      opt[String]("db-host")
        .text("Mongodb host.")
        .action((arg, cfg) => cfg.copy(dbHost = arg)),
      opt[String]("db-user-name")
        .text("Mongodb user name.")
        .action((arg, cfg) => cfg.copy(dbUserName = arg)),
      opt[String]("db-user-password")
        .text("Mongodb user password.")
        .action((arg, cfg) => cfg.copy(dbUserPassword = arg)),
      opt[Unit]("db-ssl")
        .text("Use SSL.")
        .action((_, cfg) => cfg.copy(dbSsl = true))
    )
  }
}
