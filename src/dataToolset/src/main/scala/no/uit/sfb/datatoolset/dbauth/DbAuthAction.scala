package no.uit.sfb.datatoolset.dbauth

import com.typesafe.scalalogging.LazyLogging
import no.uit.sfb.cbf.mongodb.{CbfDataset, MongodbConnect}
import no.uit.sfb.cbf.shared.auth.UserInfo
import org.mongodb.scala.model._

import scala.concurrent.duration._
import scala.concurrent.{Await, ExecutionContext}

object DbAuthAction extends LazyLogging {
  implicit val ec = ExecutionContext.global

  def apply(cfg: DbAuthConfig): Unit = {
    lazy val mgc = MongodbConnect(
      cfg.dbHost,
      cfg.dbUserName,
      cfg.dbUserPassword,
      "admin",
      cfg.dbSsl
    )
    try {
      val coll = new CbfDataset(mgc.db(cfg.dbName)).authCol
      val f =
        if (cfg.add)
          coll.replace(
            Filters.eq("_id", cfg.id),
            UserInfo(cfg.id, "", cfg.permission),
            upsert = true
          )
        else
          coll.delete(Filters.eq("_id", cfg.id), true)
      Await.result(f, 1.minute) //Not shorter otherwise it become complicated to debug connections errors
    } finally mgc.close
  }
}
