package no.uit.sfb.datatoolset.cdch

import akka.actor.ActorSystem
import com.typesafe.scalalogging.LazyLogging
import no.uit.sfb.cbf.mongodb.MongodbConnect
import no.uit.sfb.cbf.shared.config.AttributeDef
import no.uit.sfb.cbf.shared.model.v1.builder.RecordBuilder

import scala.io.{Source => IoSource}
import no.uit.sfb.datatoolset.cdch.impl.{
  CdchSubmiter,
  DataSourceFromDatabase,
  DataSourceFromFile
}
import sttp.client3.asynchttpclient.future.AsyncHttpClientFutureBackend

import scala.concurrent.duration.{Duration, DurationInt}
import scala.concurrent.Await

object CdchAction extends LazyLogging {

  def apply(cfg: CdchConfig): Unit = {
    implicit val system = ActorSystem("QuickStart")
    implicit val backend = AsyncHttpClientFutureBackend()
    implicit lazy val mgc = MongodbConnect(
      cfg.dbHost,
      cfg.dbUserName,
      cfg.dbUserPassword,
      "admin",
      cfg.dbSsl
    )

    /**
      *  Mapping (ENA attribute -> CBF attribute)
      */
    lazy val mapping: Map[String, String] = {
      IoSource
        .fromFile(cfg.mapping.toFile)
        .getLines()
        .map { _.trim }
        .filter { _.nonEmpty }
        .map { l =>
          val splt = l.split('\t')
          val left = splt.head.trim
          val right = splt.tail.mkString("\t").trim
          assert(left.nonEmpty || right.nonEmpty, "Empty mapping")
          left -> right
        }
        .toMap
    }

    val regexp = "\\{\\S+\\}".r
    lazy val attrs = ("{id}" +: mapping.values.toSeq)
      .flatMap { str =>
        regexp.findAllIn(str).toSeq
      }
      .toSet
      .map { k: String =>
        AttributeDef(k, k)
      }
    implicit val rb = new RecordBuilder(attrs.toSeq)

    val ds =
      if (cfg.in == null)
        new DataSourceFromDatabase(cfg.dsName, cfg.ver)
      else
        new DataSourceFromFile(cfg.in)

    val f = new CdchSubmiter(cfg, mapping).run(ds)

    try {
      Await.result(f, Duration.Inf)
    } finally {
      val fClose = backend.close().zip(system.terminate())
      Await.result(fClose, 1.minute)
    }
  }
}
