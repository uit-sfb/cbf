package no.uit.sfb.datatoolset.tsv2json

import com.typesafe.scalalogging.LazyLogging
import no.uit.sfb.scalautils.common.FileUtils
import io.circe.generic.auto._
import io.circe.syntax._
import no.uit.sfb.cbf.shared.model.v1.builder.RecordBuilder
import no.uit.sfb.cbf.shared.utils.Tsv

object Tsv2JsonAction extends LazyLogging {
  def apply(cfg: Tsv2JsonConfig): Unit = {
    val rb = cfg.order match {
      case Some(url) =>
        RecordBuilder.fromUrl(url.toString)
      case _ =>
        new RecordBuilder()
    }
    cfg.pos match {
      case Some(pos) =>
        val input = Tsv.extractLineAndTranspose(cfg.in.get, pos)
        val record = rb.fromSingleTsv(input)
        FileUtils.createParentDirs(cfg.out.get)
        FileUtils.writeToFile(cfg.out.get, record.asJson.spaces2)
      case _ =>
        //We extract all the lines and write the result as an array
        val (records, errors) = rb.fromTsv(cfg.in.get)._2 partition {
          _.isRight
        }
        if (errors.nonEmpty)
          throw new Exception(
            (Seq("TSV parsing failed due to the following errors:") ++ (errors map {
              _.swap.getOrElse(throw new Exception(s"Should not happen"))
            }).map {
              _.asString
            }).mkString("\n")
          )
        else
          FileUtils.writeToFile(
            cfg.out.get,
            rb.toJson(records.map {
                _.getOrElse(throw new Exception(s"Should not happen"))._1
              })
              .mkString("\n")
          )
    }
  }
}
