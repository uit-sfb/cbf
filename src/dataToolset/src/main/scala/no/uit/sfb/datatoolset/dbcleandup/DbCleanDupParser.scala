package no.uit.sfb.datatoolset.dbcleandup

import com.typesafe.scalalogging.LazyLogging
import scopt.OParser

object DbCleanDupParser extends LazyLogging {

  lazy val builder = OParser.builder[DbCleanDupConfig]

  lazy val parser = {
    import builder._
    OParser.sequence(
      head("Move data from version to latest release"),
      help('h', "help")
        .text("Prints this usage text"),
      opt[String]("version")
        .required()
        .text("The collection to remove duplicates from.")
        .action((arg, cfg) => cfg.copy(version = arg)),
      opt[String]("limit")
        .required()
        .text("Waiting limit")
        .action((arg, cfg) => cfg.copy(limit = arg)),
      opt[String]("db-name")
        .required()
        .text("Name of the database.")
        .action((arg, cfg) => cfg.copy(dbName = arg)),
      opt[String]("db-host")
        .text("Mongodb host.")
        .action((arg, cfg) => cfg.copy(dbHost = arg)),
      opt[String]("db-user-name")
        .text("Mongodb user name.")
        .action((arg, cfg) => cfg.copy(dbUserName = arg)),
      opt[String]("db-user-password")
        .text("Mongodb user password.")
        .action((arg, cfg) => cfg.copy(dbUserPassword = arg)),
    )
  }
}
