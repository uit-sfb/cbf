package no.uit.sfb.datatoolset.validatation

import java.net.URI
import java.nio.file.Path

case class ValidationConfig(in: Path = null, schema: URI = null)
