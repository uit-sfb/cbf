package no.uit.sfb.datatoolset.cdch

import java.nio.file.{Path, Paths}

case class CdchConfig(
  in: Path = null,
  mapping: Path = null,
  accessionAttribute: String = null,
  out: Path = Paths.get("./cdch_err.tsv"),
  apiUrl: String = "https://wwwdev.ebi.ac.uk/ena/clearinghouse/api",
  loginUrl: String = "https://explore.api.aai.ebi.ac.uk/auth",
  userName: String = sys.env("EBI_CDCH_USER"),
  password: String = sys.env("EBI_CDCH_PASSWORD"),
  dbSsl: Boolean = false,
  dbHost: String = "localhost:27017",
  dbUserName: String = "admin",
  dbUserPassword: String = "salvador",
  dsName: String = "",
  ver: String = "",
)
