package no.uit.sfb.datatoolset.dbpushcoll

import com.typesafe.scalalogging.LazyLogging
import no.uit.sfb.cbf.mongodb.{CbfDataset, MongodbConnect}

import scala.concurrent.{Await, ExecutionContext, Future}
import scala.io.Source
import scala.util.Using
import io.circe._
import io.circe.parser._
import org.mongodb.scala.bson.{BsonDocument, BsonValue, Document}
import org.mongodb.scala.{MongoClient, MongoCollection, MongoDatabase}
import org.mongodb.scala.model.Filters

import scala.concurrent.duration.DurationInt
object DbPushCollAction extends LazyLogging {
  implicit val ec = ExecutionContext.global

  def apply(cfg: DbPushCollConfig): Unit = {
    lazy val mgc = MongodbConnect(
      cfg.dbHost,
      cfg.dbUserName,
      cfg.dbUserPassword,
      "admin",
    )

    try {

      /*
      Used to push existing data from one collection to another collection/version, only for manual usage
       */

      val cbf = new CbfDataset(mgc.db(cfg.dbName))

      Using(Source.fromFile(cfg.in)) { source =>

        val jsonContent = source.mkString
        parse(jsonContent) match {

          case Right(jsonVal) =>
            jsonVal.asArray match {
              case Some(arr) => {
                /*
                Get document id
                If id present, throw an exception
                If no document with such id exists, insert
                 */
                val f = Future.sequence {
                  arr.map{ json =>

                    json.asObject.get("_id") match { //we check if _id field exists
                      case Some(id) => {
                        val version = cbf.parseVersion(cfg.version)
                        val doc = BsonDocument(json.toString())
                        cbf.recordsCol(version).getOne(Filters.eq("_id", doc.getObjectId("_id"))).flatMap {
                          case Some(_) =>
                            Future.failed(new Exception(s"Document with the id ${id.asString.get} already exists"))
                          case None => {
                            cbf.recordsCol(version).insert(doc)
                          }
                        }

                      }
                    }
                  }
                }
                Await.result(f,3.minutes)
              }
              case None => println("Invalid JSON format")
            }
          case Left(err) => println(s"failed to parse JSON: ${err.message}")
        }
      }

    }
    finally {
      mgc.close
    }
  }













  }
