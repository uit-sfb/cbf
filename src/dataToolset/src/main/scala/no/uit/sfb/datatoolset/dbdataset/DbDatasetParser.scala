package no.uit.sfb.datatoolset.dbdataset

import java.net.URI

import com.typesafe.scalalogging.LazyLogging
import scopt.OParser

object DbDatasetParser extends LazyLogging {

  lazy val builder = OParser.builder[DbDatasetConfig]

  lazy val parser = {
    import builder._
    OParser.sequence(
      head(
        "Manage dataset configuration list (see https://gitlab.com/uit-sfb/cbf/-/wikis/Admin-guide/Dataset-configuration)"
      ),
      help('h', "help")
        .text("Prints this usage text"),
      opt[String]("config")
        .text("Path or URL to the JSON dataset configuration file.")
        .action((arg, cfg) => cfg.copy(config = new URI(arg))),
//      opt[String]("attributes")
//        .text("Path or URL to the attribute definition file (TSV).")
//        .action((arg, cfg) => cfg.copy(attributes = new URI(arg))),
//      opt[String]("tabs")
//        .text("Tabs definition: '<tab1>=<tabName>,...'.")
//        .action(
//          (arg, cfg) =>
//            cfg.copy(tabDefs = arg.split(',').toSeq.map { s =>
//              val splt = s.split(',')
//              splt.head -> splt.tail.mkString(",")
//            })
//        ),
//      opt[String]("custom-types")
//        .text("Path or URL to the custom types definition file (TSV).")
//        .action((arg, cfg) => cfg.copy(customTypes = new URI(arg))),
//      opt[String]("name")
//        .text("Dataset name.")
//        .action((arg, cfg) => cfg.copy(name = Some(arg))),
//      opt[String]("description")
//        .text("Dataset description.")
//        .action((arg, cfg) => cfg.copy(description = Some(arg))),
//      opt[String]("schema")
//        .text("Path or URL to custom JSON Schema instead of the default one.")
//        .action((arg, cfg) => cfg.copy(schema = Some(new URI(arg)))),
//      opt[Unit]("latest-only")
//        .text("Only the latest release is visible to regular users.")
//        .action((_, cfg) => cfg.copy(latestOnly = true)),
//      opt[String]("grant-read-access")
//        .text("Grant read access (one of: anonymous, none, reader).")
//        .action((arg, cfg) => cfg.copy(grantReadAccess = arg)),
      opt[String]("db-name-override")
        .text(
          "Name of the database (in case the name contains invalid characters)."
        )
        .action((arg, cfg) => cfg.copy(dbNameOverride = Some(arg))),
      opt[String]("db-host")
        .text("Mongodb host.")
        .action((arg, cfg) => cfg.copy(dbHost = arg)),
      opt[String]("db-user-name")
        .text("Mongodb user name.")
        .action((arg, cfg) => cfg.copy(dbUserName = arg)),
      opt[String]("db-user-password")
        .text("Mongodb user password.")
        .action((arg, cfg) => cfg.copy(dbUserPassword = arg)),
      opt[Unit]("db-ssl")
        .text("Use SSL.")
        .action((_, cfg) => cfg.copy(dbSsl = true)),
      checkConfig(
        cfg =>
          if (cfg.config == null)
            failure(
              "When --add is used, either --config or --name must be provided."
            )
          else if (cfg.config == null)
            failure(
              "When --add is used, either --config or --name must be provided, but not both at the same time."
            )
          else
          success
      )
    )
  }
}
