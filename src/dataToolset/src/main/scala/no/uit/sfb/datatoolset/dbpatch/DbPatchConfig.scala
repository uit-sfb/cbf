package no.uit.sfb.datatoolset.dbpatch

import no.uit.sfb.cbf.shared.model.v1.builder.DatasetConfigLike

import java.nio.file.{Path, Paths}

case class DbPatchConfig(in: Path = null,
                         out: Path = Paths.get("./target"),
                         dbName: String = null,
                         version: String = "",
                         force: Boolean = false,
                         dbSsl: Boolean = false,
                         dbHost: String = "localhost:27017",
                         dbUserName: String = "admin",
                         dbUserPassword: String = "salvador")
    extends DatasetConfigLike
