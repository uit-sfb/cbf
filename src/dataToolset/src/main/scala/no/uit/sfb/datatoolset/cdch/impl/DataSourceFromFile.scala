package no.uit.sfb.datatoolset.cdch.impl

import akka.NotUsed
import akka.stream.scaladsl._
import akka.util.ByteString
import no.uit.sfb.cbf.shared.model.v1.builder.RecordBuilder
import no.uit.sfb.cbf.shared.model.v1.nativ.Record
import no.uit.sfb.datatoolset.cdch.DataSourceLike

import java.nio.file.Path

final class DataSourceFromFile(path: Path)(implicit rb: RecordBuilder)
    extends DataSourceLike {
  val source: Source[Record, NotUsed] = {
    val filename = path.getFileName.toString
    val optDecompressFlow: Flow[ByteString, ByteString, NotUsed] =
      if (filename.endsWith(".gz") || filename.endsWith(".gzip"))
        Compression.gunzip()
      else
        Flow.apply[ByteString]
    FileIO
      .fromPath(path)
      .via(optDecompressFlow)
      .viaMat(JsonFraming.objectScanner(1000000))(Keep.none)
      .map { bs =>
        rb.fromSingleJson(bs.utf8String)
      }
  }
}
