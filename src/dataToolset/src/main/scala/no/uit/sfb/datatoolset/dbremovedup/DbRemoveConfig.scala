package no.uit.sfb.datatoolset.dbremovedup

import no.uit.sfb.cbf.shared.model.v1.builder.DatasetConfigLike

case class DbRemoveConfig(
                        dbName: String = null,
                        ver1:String = "",
                        ver2: String = "",
                        limit: String = "",
                        dbHost: String = "localhost:27017",
                        dbUserName: String = "admin",
                        dbUserPassword: String = "salvador")
  extends DatasetConfigLike