package no.uit.sfb.datatoolset.dbmove

import com.typesafe.scalalogging.LazyLogging
import no.uit.sfb.cbf.mongodb.{CbfDataset, MongodbConnect}
import no.uit.sfb.cbf.shared.model.v1.versioninfo.VersionInfo
import no.uit.sfb.datatoolset.dbpush.DbPushConfig
import org.mongodb.scala.model.Filters
import org.mongodb.scala.result.InsertOneResult

import scala.concurrent.duration.DurationInt
import scala.concurrent.{Await, ExecutionContext, Future}
import scala.util.{Failure, Success, Try}

object DbMoveAction extends LazyLogging {
  implicit val ec = ExecutionContext.global

  def apply(cfg: DbMoveConfig): Unit = {
    lazy val mgc = MongodbConnect(
      cfg.dbHost,
      cfg.dbUserName,
      cfg.dbUserPassword,
      "admin"
    )

  /*
  Try block to catch an exception
   */
  try {

    val cbf = new CbfDataset(mgc.db(cfg.dbName))

    val getVersion = (v:String) => Try(cbf.parseVersion(v)).toOption match {
      case Some(v) => v
      case _ => throw new IllegalArgumentException("Failed to convert version!")
    }

    logger.info(s"Moving from collection ${cfg.versionOld} to ${cfg.versionNew}")

    /*
    Connect to old collection
     */
    val oldVer = getVersion(cfg.versionOld)
    val newVer = getVersion(cfg.versionNew)

    cbf.recordsCol(oldVer).getIt().grouped(500).foreach{ docs =>
      val f = Future.sequence(
        docs.map{ doc =>
          cbf.recordsCol(newVer).insert(doc).flatMap {
            case inserted if inserted.wasAcknowledged() => cbf.recordsCol(oldVer).delete(Filters.eq("_id", doc.head._2), true)
          }
        }
      )
      Await.result(f,cfg.limit.toInt.minutes)
    }
  } finally mgc.close


  }
}