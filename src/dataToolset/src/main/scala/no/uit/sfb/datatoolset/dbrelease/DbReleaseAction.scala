package no.uit.sfb.datatoolset.dbrelease

import com.typesafe.scalalogging.LazyLogging
import no.uit.sfb.cbf.mongodb.MongodbConnect
import io.circe.generic.auto._
import io.circe.syntax._
import no.uit.sfb.cbf.mongodb.CbfDataset
import no.uit.sfb.cbf.shared.model.v1.versioninfo.VersionInfo
import org.mongodb.scala.Document
import org.mongodb.scala.model.Filters
import org.mongodb.scala.model
import scala.concurrent.duration._
import scala.concurrent.{Await, ExecutionContext, Future}

object DbReleaseAction extends LazyLogging {
  implicit val ec = ExecutionContext.global

  def apply(cfg: DbReleaseConfig): Unit = {
    lazy val mgc = MongodbConnect(
      cfg.dbHost,
      cfg.dbUserName,
      cfg.dbUserPassword,
      "admin",
      cfg.dbSsl
    )
    try {
      val cbf = new CbfDataset(mgc.db(cfg.dbName))
      val cbfVersion = cbf.parseVersion(cfg.version) //Needed to ensure the version is ok


      logger.info(s"Releasing version '${cfg.version}'...")

      /**
       * Releasing version
       *
       * */
       lazy val releaseF =
          cbf
          .version(cfg.version, false)
          .flatMap {
            case Some(_) =>
              logger.warn(
                s"Version '${cfg.version} already exists. Release command has no effect."
              )
              Future.successful(())

            case None =>
              cbf.versionsCol
                .replace(
                  Filters.eq("_id", cfg.version),
                  Document(VersionInfo.now(cfg.version).asJson.noSpaces),
                  true
                )
          }
          .flatMap { _ =>
            if (cfg.createNextDraft) {
              val nextVer = cbfVersion.incr
              cbf.versionsCol
                .replace(
                  Filters.eq("_id", nextVer.toString),
                  Document(VersionInfo.draft(nextVer.toString).asJson.noSpaces),
                  true
                )
            } else Future.successful(())
          }

      /**
       * If "if-modified" flag is true, we check if any new records were added or existing records were modified
       * else we just try to release version.
       */
      val f =
        if (cfg.ifModified) {
        cbf
          .recordsCol(cbfVersion).get(
          model.Filters.or(
            model.Filters.eq("valid.start",cbfVersion.release),
            model.Filters.eq("valid.end",cbfVersion.release)
          )
        )
          .flatMap {
            case seqOfDocs: Seq[Document] if (seqOfDocs.nonEmpty) =>
              releaseF
            case _ =>
            logger.warn("No records were added/modified")
            Future.successful(())
          }
        }
        else {
          releaseF
        }

      Await.result(f, 1.minute) //Not shorter otherwise it becomes complicated to debug connections errors
    } finally mgc.close


  }
}
