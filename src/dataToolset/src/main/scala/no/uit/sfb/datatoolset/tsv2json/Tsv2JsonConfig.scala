package no.uit.sfb.datatoolset.tsv2json

import java.net.URI
import java.nio.file.Path

case class Tsv2JsonConfig(in: Option[Path] = None,
                          out: Option[Path] = None,
                          order: Option[URI] = None,
                          pos: Option[Int] = None,
)
