package no.uit.sfb.datatoolset.dbmove

import no.uit.sfb.cbf.shared.model.v1.builder.DatasetConfigLike

import java.nio.file.{Path, Paths}

case class DbMoveConfig(
                        dbName: String = null,
                        versionOld:String = "",
                        versionNew: String = "",
                        limit: String = "",
                        force: Boolean = false,
                        dbHost: String = "localhost:27017",
                        dbUserName: String = "admin",
                        dbUserPassword: String = "salvador")
  extends DatasetConfigLike