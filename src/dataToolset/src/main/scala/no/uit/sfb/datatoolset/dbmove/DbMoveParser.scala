package no.uit.sfb.datatoolset.dbmove

import com.typesafe.scalalogging.LazyLogging
import scopt.OParser

object DbMoveParser extends LazyLogging {

  lazy val builder = OParser.builder[DbMoveConfig]

  lazy val parser = {
    import builder._
    OParser.sequence(
      head("Move data from version to latest release"),
      help('h', "help")
        .text("Prints this usage text"),
      opt[String]("verOld")
        .required()
        .text("The version to move from.")
        .action((arg, cfg) => cfg.copy(versionOld = arg)),
      opt[String]("verNew")
        .required()
        .text("The version to move to.")
        .action((arg, cfg) => cfg.copy(versionNew = arg)),
      opt[String]("limit")
        .required()
        .text("Waiting limit")
        .action((arg, cfg) => cfg.copy(limit = arg)),
      opt[String]("db-name")
        .required()
        .text("Name of the database.")
        .action((arg, cfg) => cfg.copy(dbName = arg)),
      opt[String]("db-host")
        .text("Mongodb host.")
        .action((arg, cfg) => cfg.copy(dbHost = arg)),
      opt[String]("db-user-name")
        .text("Mongodb user name.")
        .action((arg, cfg) => cfg.copy(dbUserName = arg)),
      opt[String]("db-user-password")
        .text("Mongodb user password.")
        .action((arg, cfg) => cfg.copy(dbUserPassword = arg)),
    )
  }
}
