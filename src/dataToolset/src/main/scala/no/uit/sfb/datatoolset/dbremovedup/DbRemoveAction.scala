package no.uit.sfb.datatoolset.dbremovedup

import com.typesafe.scalalogging.LazyLogging
import no.uit.sfb.cbf.mongodb.{CbfDataset, MongodbConnect}
import org.mongodb.scala.model.Filters

import scala.concurrent.duration.DurationInt
import scala.concurrent.{Await, ExecutionContext, Future}
import scala.util.Try

object DbRemoveAction extends LazyLogging {
  implicit val ec = ExecutionContext.global

  def apply(cfg: DbRemoveConfig): Unit = {
    lazy val mgc = MongodbConnect(
      cfg.dbHost,
      cfg.dbUserName,
      cfg.dbUserPassword,
      "admin"
    )

  /*
  Try block to catch an exception
   */
  try {

    val cbf = new CbfDataset(mgc.db(cfg.dbName))

    val getVersion = (v:String) => Try(cbf.parseVersion(v)).toOption match {
      case Some(v) => v
      case _ => throw new IllegalArgumentException("Failed to convert version!")
    }

    logger.info(s"Remove duplicates from collection ${cfg.ver1}")

    /*
    Connect to old collection
     */
    val checkCollVer = getVersion(cfg.ver1)
    val checkAgainstVer = getVersion(cfg.ver2)

    cbf.recordsCol(checkCollVer).getIt().grouped(500).foreach{ docs =>
      val f = Future.sequence(
        docs.map{ doc =>
          cbf.recordsCol(checkAgainstVer).getOne(Filters.eq("_id", doc.head._2)).flatMap{
            case Some(doc) => cbf.recordsCol(checkCollVer).delete(Filters.eq("_id", doc.head._2), true)
            case None => Future.successful(())
          }
        }
      )
      Await.result(f,cfg.limit.toInt.minutes)
    }
  } finally mgc.close


  }
}