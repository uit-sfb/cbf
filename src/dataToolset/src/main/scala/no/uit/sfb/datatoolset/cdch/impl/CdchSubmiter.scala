package no.uit.sfb.datatoolset.cdch.impl

import akka.NotUsed
import akka.actor.ActorSystem
import akka.stream.scaladsl.{FileIO, Source}
import akka.util.ByteString
import com.typesafe.scalalogging.LazyLogging
import no.uit.sfb.cbf.shared.model.v1.nativ.{Evidence, Record}
import no.uit.sfb.cdch4s
import no.uit.sfb.cdch4s.{CdchApi, CdchTokenProvider}
import no.uit.sfb.cdch4s.model.Curation
import no.uit.sfb.cdch4s.model.response.{CurationResponse, ErrorObj}
import no.uit.sfb.datatoolset.cdch.{CdchConfig, DataSourceLike}
import sttp.client3.circe.asJson
import sttp.client3.{SttpBackend, UriContext, basicRequest}

import java.util.concurrent.atomic.AtomicInteger
import scala.concurrent.duration.DurationInt
import scala.concurrent.{Future, blocking}
import no.uit.sfb.cdch4s.DefaultBackend._

class CdchSubmiter(cfg: CdchConfig, mapping: Map[String, String])(
  implicit system: ActorSystem,
  backend: SttpBackend[Future, Any]
) extends LazyLogging {
  private implicit val ec = system.dispatcher
  private implicit lazy val tokenProvider =
    new CdchTokenProvider(cfg.loginUrl, cfg.userName, cfg.password)
  private lazy val cdch = new CdchApi(cfg.apiUrl)

  /**
    * Fetch ENA entry via API
    * @param id
    * @return
    */
  private def enaEntry(id: String,
                       retry: Boolean = false): Future[Map[String, String]] = {
    val uri =
      uri"https://www.ebi.ac.uk/ena/portal/api/search?fields=all&format=json&result=sequence&query=accession=%22$id%22"
    val request = basicRequest
      .get(uri)
    val resp = request
      .response(asJson[Seq[Map[String, String]]])
      .send(backend)

    resp
      .flatMap { r =>
        r.body match {
          case Left(e) =>
            if (retry)
              enaEntry(id)
            else {
              logger.info(s"$uri -- ${r.statusText} ($e)")
              Future.successful(Map[String, String]())
            }
          case Right(Seq(m)) =>
            Future.successful(m)
          case _ => //Should not happen
            throw new Exception("Unexpected parsed type")
        }
      }
      .recoverWith {
        case e: Exception => //Mostly sttp.client3.SttpClientException$ReadException caused by TimeoutException
          if (retry) {
            Thread.sleep(10.seconds.toMillis)
            enaEntry(id)
          } else {
            logger.info(s"$uri -- $e")
            Future.successful(Map[String, String]())
          }
      }
  }

  private def mapEvidence(evi: Evidence): cdch4s.model.Evidence = {
    val eco = evi.iri
      .find { iri =>
        iri.startsWith("eco:") || iri.startsWith("ECO:")
      }
      .getOrElse("eco:0000305")
      .toUpperCase()
    cdch4s.model
      .Evidence(Some(eco), if (evi.`val`.nonEmpty) Some(evi.`val`) else None)
  }

  private def mapStatement(id: String,
                           url: Option[String],
                           accession: String,
                           enaAttr: String,
                           enaValue: Option[String],
                           value: String,
                           evi: Seq[Evidence]): Option[Curation] = {
    //Special handling of tax_id
    val insdcAttr = if (enaAttr == "tax_id") "db_xref" else enaAttr
    val v = {
      if (enaAttr == "tax_id")
        s"taxon:$value"
      else
        value
    }
    Some(
      Curation(
        recordType = "sequence",
        recordId = accession,
        providerName = "SfB",
        assertionMethod = "manual assertion",
        assertionEvidences = evi.map {
          mapEvidence
        },
        providerUrl = url,
        dataType = Some("covid19"),
        dataIdentifier = Some(id),
        attributePre = if (enaValue.isEmpty) None else Some(insdcAttr),
        valuePre = enaValue,
        attributePost = Some(insdcAttr),
        valuePost = Some(v),
        attributeDelete = false,
        assertionSource = evi.flatMap { _.src }.flatMap { _.urls }.headOption,
        assertionAdditionalInfo = None
      )
    )
  }

  private def findEvi(rec: Record, template: String): Seq[Evidence] = {
    val regexp = "\\{.*?\\}".r
    val keys = regexp
      .findAllIn(template)
      .map { tpl =>
        val splt = tpl.substring(1, tpl.length - 1).split('@')
        splt.head
      }
    keys.flatMap { key =>
      rec.stm(key).flatMap { stm =>
        stm.evi
      }
    }.toSeq
  }

  private lazy val cnt = new AtomicInteger()

  private def transform(rec: Record): Future[Seq[Curation]] = {
    val id = rec.getId
    val accessionVR = rec
      .stm(cfg.accessionAttribute)
      .flatMap {
        _.obj
      }
      .head
      .`val`
      .split('.')
      .head //We remove genbank versions
    enaEntry(accessionVR, true) map { enaRec =>
      val idVR = rec
        .stm("id")
        .flatMap {
          _.obj
        }
        .head
      mapping
        .flatMap {
          case (enaAttr, template) =>
            rec
              .resolveTemplate(template)
              .headOption
              .flatMap { v =>
                if (v.isEmpty) None
                else {
                  Some((enaAttr, v, findEvi(rec, template)))
                }
              }
        }
        .flatMap {
          case (enaAttr, value, evi) =>
            val enaValue = enaRec.get(enaAttr)
            if (value == enaValue.getOrElse(""))
              None
            else
              Some((enaAttr, enaValue, value, evi))
        }
        .flatMap {
          case (enaAttr, enaValue, value, evi) =>
            mapStatement(
              id,
              idVR.meta.get("_iri"),
              accessionVR,
              enaAttr,
              enaValue,
              value,
              evi
            )
        }
        .toSeq
    }
  }

  private def curationSource(
    ds: DataSourceLike
  ): Source[Seq[Curation], NotUsed] = {
    ds.source
      .mapAsyncUnordered(24) { //The amount of parallelism is adjusted accordingly to the answer time from EBI API
        rec =>
          if (cnt.incrementAndGet() % 100 == 0) {
            print("+")
            Console.flush()
          }
          transform(rec)
      }
      .mapConcat(identity)
      .grouped(100)
  }

  private val cntSuccess = new AtomicInteger()
  private val cntFailure = new AtomicInteger()

  private def submit(curations: Seq[Curation],
                     retry: Boolean = false): Future[CurationResponse] = {
    Future {
      blocking {
        cdch.create(curations)
      }
    }.recoverWith {
      case e =>
        if (retry) {
          logger.info(s"Submitting curations -- $e")
          submit(curations)
        } else {
          logger.warn(e.getMessage)
          Future.successful(CurationResponse("", Seq(), curations.map { cur =>
            ErrorObj(
              "cbf_intern",
              e.getMessage,
              Some(cur.recordId),
              cur.attributePost,
              cur.valuePre,
              cur.valuePost
            )
          }))
        }
    }
  }

  private def errors(ds: DataSourceLike): Source[ByteString, NotUsed] =
    curationSource(ds)
      .mapAsyncUnordered(8) { curations => //The amount of parallelism is adjusted accordingly to the answer time from EBI API
        submit(curations, true)
      }
      .map { cr =>
        val success = cr.curationIds.size - cr.errors.size
        cntSuccess.addAndGet(success)
        cntFailure.addAndGet(cr.errors.size)
        cr.errors
      }
      .mapConcat(identity)
      .map { err =>
        ByteString(
          s"${err.recordId}\t${err.field}\t${err.attributePre}\t${err.attributePost}\t${err.message}\n"
        )
      }

  def run(ds: DataSourceLike) = {
    errors(ds)
      .prepend(
        akka.stream.scaladsl.Source.single(
          ByteString("recordId\tfield\tattributePre\tattributePost\tmessage")
        )
      )
      .runWith(FileIO.toPath(cfg.out))
      .map { res =>
        if (cntFailure.get() > 0L) {
          throw new Exception(s"Failed submitting ${cntFailure
            .get()} curation objects! (Success: ${cntSuccess.get()})")
        } else {
          println(
            s"Successfully submitted ${cntSuccess.get()} curation objects."
          )
          res
        }
      }
  }
}
