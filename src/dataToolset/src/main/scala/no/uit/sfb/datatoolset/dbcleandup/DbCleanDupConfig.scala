package no.uit.sfb.datatoolset.dbcleandup

import no.uit.sfb.cbf.shared.model.v1.builder.DatasetConfigLike

case class DbCleanDupConfig(
                        dbName: String = null,
                        version:String = "",
                        limit: String = "",
                        dbHost: String = "localhost:27017",
                        dbUserName: String = "admin",
                        dbUserPassword: String = "salvador")
  extends DatasetConfigLike