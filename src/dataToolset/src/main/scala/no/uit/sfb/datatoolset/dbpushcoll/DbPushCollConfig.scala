package no.uit.sfb.datatoolset.dbpushcoll

import no.uit.sfb.cbf.shared.model.v1.builder.DatasetConfigLike

case class DbPushCollConfig(
                              dbName: String = null,
                              version: String = "",
                              in: String = "",
                              collName: String = "",
                              force: Boolean = false,
                              dbHost: String = "localhost:27017",
                              dbUserName: String = "admin",
                              dbUserPassword: String = "salvador")
  extends DatasetConfigLike {

}
