package no.uit.sfb.datatoolset.dbrelease

import no.uit.sfb.cbf.shared.model.v1.builder.DatasetConfigLike

case class DbReleaseConfig(dbName: String = null,
                           version: String = "",
                           dbSsl: Boolean = false,
                           dbHost: String = "localhost:27017",
                           dbUserName: String = "admin",
                           dbUserPassword: String = "salvador",
                           createNextDraft: Boolean = false,
                           ifModified: Boolean = false)
    extends DatasetConfigLike
