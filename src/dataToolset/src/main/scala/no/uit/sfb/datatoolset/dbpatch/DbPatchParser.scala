package no.uit.sfb.datatoolset.dbpatch

import java.io.File

import com.typesafe.scalalogging.LazyLogging
import scopt.OParser

object DbPatchParser extends LazyLogging {

  lazy val builder = OParser.builder[DbPatchConfig]

  lazy val parser = {
    import builder._
    OParser.sequence(
      head("Patch existing records from JSON table"),
      help('h', "help")
        .text("Prints this usage text"),
      opt[File]("in")
        .required()
        .text("Path to input file containing updates (TSV)")
        .action((arg, cfg) => cfg.copy(in = arg.toPath)),
      opt[File]("out")
        .text("Path to output dir")
        .action((arg, cfg) => cfg.copy(out = arg.toPath)),
      opt[String]("db-name")
        .required()
        .text("Name of the database.")
        .action((arg, cfg) => cfg.copy(dbName = arg)),
      opt[String]("ver")
        .required()
        .text("The version being worked on.")
        .action((arg, cfg) => cfg.copy(version = arg)),
      opt[Unit]('f', "force")
        .text("Force patch when data has already been released.")
        .action((_, cfg) => cfg.copy(force = true)),
      opt[String]("db-host")
        .text("Mongodb host.")
        .action((arg, cfg) => cfg.copy(dbHost = arg)),
      opt[String]("db-user-name")
        .text("Mongodb user name.")
        .action((arg, cfg) => cfg.copy(dbUserName = arg)),
      opt[String]("db-user-password")
        .text("Mongodb user password.")
        .action((arg, cfg) => cfg.copy(dbUserPassword = arg)),
      opt[Unit]("db-ssl")
        .text("Use SSL.")
        .action((_, cfg) => cfg.copy(dbSsl = true))
    )
  }
}
