package no.uit.sfb.datatoolset

import com.typesafe.scalalogging.LazyLogging
import no.uit.sfb.datatoolset.cdch.{CdchAction, CdchConfig, CdchParser}
import no.uit.sfb.datatoolset.dbpatch._
import no.uit.sfb.datatoolset.dbpush._
import no.uit.sfb.datatoolset.dbrelease._
import no.uit.sfb.datatoolset.dbreleaseAll._
import no.uit.sfb.datatoolset.dbadmin._
import no.uit.sfb.datatoolset.dbauth._
import no.uit.sfb.datatoolset.dbcleandup.{DbCleanDupAction, DbCleanDupConfig, DbCleanDupParser}
import no.uit.sfb.datatoolset.dbdataset._
import no.uit.sfb.datatoolset.dbmove.{DbMoveAction, DbMoveConfig, DbMoveParser}
import no.uit.sfb.datatoolset.dbpushcoll.{DbPushCollAction, DbPushCollConfig, DbPushCollParser}
import no.uit.sfb.datatoolset.dbremovedup.{DbRemoveAction, DbRemoveConfig, DbRemoveParser}
import no.uit.sfb.datatoolset.tsv2json._
import no.uit.sfb.datatoolset.json2tsv._
import no.uit.sfb.datatoolset.validatation._
import scopt.OParser
import no.uit.sfb.info.datatoolset.BuildInfo

import scala.concurrent.ExecutionContext
import scala.util.control.NonFatal

object Main extends App with LazyLogging {
  val name = BuildInfo.name
  val ver = BuildInfo.version
  val gitCommitId = BuildInfo.gitCommit

  implicit val ec = ExecutionContext.global

  try {
    val mainBuilder = OParser.builder[String]

    val parserMain = {
      import mainBuilder._
      OParser.sequence(
        programName(name),
        head(name, ver),
        head(s"Git: $gitCommitId"),
        head(s"Description: CBF command line tool"),
        help('h', "help")
          .text("Prints this usage text"),
        version('v', "version")
          .text("Prints the version"),
        cmd("dataset")
          .action((_, _) => "dataset")
          .text("Manage dataset list"),
        cmd("push")
          .action((_, _) => "push")
          .text("Push a TSV table"),
        cmd("patch")
          .action((_, _) => "patch")
          .text("Patch existing records from a TSV table"),
        cmd("release")
          .action((_, _) => "release")
          .text("Create a version release"),
        cmd("releaseAll")
          .action((_, _) => "releaseAll")
          .text("Create a version release for multiple databases"),
        cmd("admin")
          .action((_, _) => "admin")
          .text("Manage administrator list"),
        cmd("auth")
          .action((_, _) => "auth")
          .text("Manage authorization list"),
        cmd("tsv2json")
          .action((_, _) => "tsv2json")
          .text("Convert TSV to JSON"),
        cmd("json2tsv")
          .action((_, _) => "json2tsv")
          .text("Convert JSON to TSV"),
        cmd("validate")
          .action((_, _) => "validate")
          .text("Validate a record against json schema"),
        cmd("cdch")
          .action((_, _) => "cdch")
          .text("Push to ClearingHouse"),

        cmd("move")
          .action((_, _) => "move")
          .text("move data between collections"),

        cmd("removedup")
          .action((_, _) => "removedup")
          .text("remove duplicates between two collections"),

        cmd("cleandup")
          .action((_, _) => "cleandup")
          .text("remove duplicates in collection"),

        cmd("pushcoll")
          .action((_, _) => "pushcoll")
          .text("push data from json array into collection"),
      )
    }

    (
      OParser.parse(parserMain, args.headOption.toSeq, ""),
      if (args.nonEmpty) args.tail else Array("-h")
    ) match {
      case (Some("tsv2json"), arg) =>
        OParser.parse(Tsv2JsonParser.parser, arg, Tsv2JsonConfig()) match {
          case Some(config) => Tsv2JsonAction(config)
          case _            => throw new IllegalArgumentException()
        }
      case (Some("json2tsv"), arg) =>
        OParser.parse(Json2TsvParser.parser, arg, Json2TsvConfig()) match {
          case Some(config) => Json2TsvAction(config)
          case _            => throw new IllegalArgumentException()
        }
      case (Some("validate"), arg) =>
        OParser.parse(ValidationParser.parser, arg, ValidationConfig()) match {
          case Some(config) => ValidationAction(config)
          case _            => throw new IllegalArgumentException()
        }
      case (Some("push"), arg) =>
        OParser.parse(DbPushParser.parser, arg, DbPushConfig()) match {
          case Some(config) =>
            DbPushAction(config)
          case _ => throw new IllegalArgumentException()
        }
      case (Some("patch"), arg) =>
        OParser.parse(DbPatchParser.parser, arg, DbPatchConfig()) match {
          case Some(config) =>
            DbPatchAction(config)
          case _ => throw new IllegalArgumentException()
        }
      case (Some("dataset"), arg) =>
        OParser.parse(DbDatasetParser.parser, arg, DbDatasetConfig()) match {
          case Some(config) =>
            DbDatasetAction(config)
          case _ => throw new IllegalArgumentException()
        }
      case (Some("release"), arg) =>
        OParser.parse(DbReleaseParser.parser, arg, DbReleaseConfig()) match {
          case Some(config) =>
            DbReleaseAction(config)
          case _ => throw new IllegalArgumentException()
        }

      case (Some("releaseAll"), arg) =>
        OParser.parse(DbReleaseAllParser.parser, arg, DbReleaseAllConfig()) match {
          case Some(config) =>
            DbReleaseAllAction(config)
          case _ => throw new IllegalArgumentException()
        }

      case (Some("admin"), arg) =>
        OParser.parse(DbAdminParser.parser, arg, DbAdminConfig()) match {
          case Some(config) =>
            DbAdminAction(config)
          case _ => throw new IllegalArgumentException()
        }
      case (Some("auth"), arg) =>
        OParser.parse(DbAuthParser.parser, arg, DbAuthConfig()) match {
          case Some(config) =>
            DbAuthAction(config)
          case _ => throw new IllegalArgumentException()
        }

      case (Some("cdch"), arg) =>
        OParser.parse(CdchParser.parser, arg, CdchConfig()) match {
          case Some(config) =>
            if (config.in == null && (config.dsName.isEmpty || config.ver.isEmpty))
              throw new IllegalArgumentException(
                s"If no input file is provided (with --in), then --ds-name and --ver are required (as well as mongodb connection settings). See --help for more info."
              )
            CdchAction(config)
          case _ => throw new IllegalArgumentException()
        }

      case (Some("move"), arg) =>
        OParser.parse(DbMoveParser.parser, arg, DbMoveConfig()) match {
          case Some(config) =>
            DbMoveAction(config)
          case _ => throw new IllegalArgumentException()
        }

      case (Some("removedup"), arg) =>
        OParser.parse(DbRemoveParser.parser, arg, DbRemoveConfig()) match {
          case Some(config) =>
            DbRemoveAction(config)
          case _ => throw new IllegalArgumentException()
        }

      case (Some("cleandup"), arg) =>
        OParser.parse(DbCleanDupParser.parser, arg, DbCleanDupConfig()) match {
          case Some(config) =>
            DbCleanDupAction(config)
          case _ => throw new IllegalArgumentException()
        }

      case (Some("pushcoll"), arg) =>
        OParser.parse(DbPushCollParser.parser, arg, DbPushCollConfig()) match {
          case Some(config) =>
            DbPushCollAction(config)
          case _ => throw new IllegalArgumentException()
        }

      case _ =>
        // arguments are bad, error message will have been displayed
        throw new IllegalArgumentException()
    }
  } catch {
    case NonFatal(e) =>
      logger.error(e.toString, e) //The stack trace is incomplete
      e.printStackTrace() //So we print the full one here
      if (e.getCause != null)
        logger.error("Cause:", e.getCause)
      sys.exit(1)
  }
}
