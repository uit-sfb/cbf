package no.uit.sfb.datatoolset.cdch

import java.io.File

import com.typesafe.scalalogging.LazyLogging
import scopt.OParser

object CdchParser extends LazyLogging {

  lazy val builder = OParser.builder[CdchConfig]

  lazy val parser = {
    import builder._
    OParser.sequence(
      head("Push curations to Contextual Data ClearingHouse (CDCH)"),
      help('h', "help")
        .text("Prints this usage text"),
      opt[File]("in")
        .text(
          "Path to input file containing all entries (JSON). Support for .gz or .gzip extensions. " +
            "If not provided, data will be retrieved from the database (database connection details must then be provided)."
        )
        .action((arg, cfg) => cfg.copy(in = arg.toPath)),
      opt[File]("mapping")
        .required()
        .text(
          "Path to attribute mapping file: ENA attribute (TSV) -> {CBF attribute}"
        )
        .text(
          "Note that ENA attributes are based on INSDC attributes (https://www.insdc.org/documents/feature-table#7.2) with some additions and few modifications."
        )
        .action((arg, cfg) => cfg.copy(mapping = arg.toPath)),
      opt[String]("acc-attr")
        .required()
        .text("Attribute name containing the record accession.")
        .action((arg, cfg) => cfg.copy(accessionAttribute = arg)),
      opt[File]("out")
        .text("Path to output file (report).")
        .action((arg, cfg) => cfg.copy(out = arg.toPath)),
      opt[String]("api")
        .text("CDCH API URL (Default: test API)")
        .action((arg, cfg) => cfg.copy(apiUrl = arg)),
      opt[String]("login")
        .text("CDCH login URL (Default: test login service).")
        .action((arg, cfg) => cfg.copy(loginUrl = arg)),
      opt[String]('u', "username")
        .text("User name (Default: EBI_CDCH_USER envir).")
        .action((arg, cfg) => cfg.copy(userName = arg)),
      opt[String]('p', "password")
        .text("Password (Default: EBI_CDCH_PASSWORD envir).")
        .action((arg, cfg) => cfg.copy(password = arg)),
      opt[String]("db-host")
        .text("Mongodb host. Required if not input file provided.")
        .action((arg, cfg) => cfg.copy(dbHost = arg)),
      opt[String]("db-user-name")
        .text("Mongodb user name. Required if not input file provided.")
        .action((arg, cfg) => cfg.copy(dbUserName = arg)),
      opt[String]("db-user-password")
        .text("Mongodb user password. Required if not input file provided.")
        .action((arg, cfg) => cfg.copy(dbUserPassword = arg)),
      opt[Unit]("db-ssl")
        .text("Use SSL. Required if not input file provided.")
        .action((_, cfg) => cfg.copy(dbSsl = true)),
      opt[String]("db-name")
        .text("Dataset name. Required if not input file provided.")
        .action((arg, cfg) => cfg.copy(dsName = arg)),
      opt[String]("ver")
        .text("Dataset version. Required if not input file provided.")
        .action((arg, cfg) => cfg.copy(ver = arg))
    )
  }
}
