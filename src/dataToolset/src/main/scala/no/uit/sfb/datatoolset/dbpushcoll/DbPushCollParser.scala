package no.uit.sfb.datatoolset.dbpushcoll

import com.typesafe.scalalogging.LazyLogging
import scopt.OParser

import java.io.File

object DbPushCollParser extends LazyLogging {

  lazy val builder = OParser.builder[DbPushCollConfig]

  lazy val parser = {
    import builder._
    OParser.sequence(
      head(
        "Push data from TSV table (existing matching records will be replaced)"
      ),
      help('h', "help")
        .text("Prints this usage text"),
      opt[File]("in")
        .required()
        .text("Path to input file containing all entries (JSON Array)")
        .action((arg, cfg) => cfg.copy(in = arg.toString)),
      opt[String]("ver")
        .required()
        .text("The version being worked on.")
        .action((arg, cfg) => cfg.copy(version = arg)),
      opt[Unit]('f', "force")
        .text("Force push when data has already been released.")
        .action((_, cfg) => cfg.copy(force = true)),
      opt[String]("db-name")
        .required()
        .text("Name of the database.")
        .action((arg, cfg) => cfg.copy(dbName = arg)),
      opt[String]("db-host")
        .text("Mongodb host.")
        .action((arg, cfg) => cfg.copy(dbHost = arg)),
      opt[String]("db-user-name")
        .text("Mongodb user name.")
        .action((arg, cfg) => cfg.copy(dbUserName = arg)),
      opt[String]("db-user-password")
        .text("Mongodb user password.")
        .action((arg, cfg) => cfg.copy(dbUserPassword = arg)),
    )
  }
}
