package no.uit.sfb.datatoolset.dbremovedup

import com.typesafe.scalalogging.LazyLogging
import scopt.OParser

object DbRemoveParser extends LazyLogging {

  lazy val builder = OParser.builder[DbRemoveConfig]

  lazy val parser = {
    import builder._
    OParser.sequence(
      head("Move data from version to latest release"),
      help('h', "help")
        .text("Prints this usage text"),
      opt[String]("ver1")
        .required()
        .text("The collection to remove duplicates from.")
        .action((arg, cfg) => cfg.copy(ver1 = arg)),
      opt[String]("ver2")
        .required()
        .text("The collection to check against.")
        .action((arg, cfg) => cfg.copy(ver2 = arg)),
      opt[String]("limit")
        .required()
        .text("Waiting limit")
        .action((arg, cfg) => cfg.copy(limit = arg)),
      opt[String]("db-name")
        .required()
        .text("Name of the database.")
        .action((arg, cfg) => cfg.copy(dbName = arg)),
      opt[String]("db-host")
        .text("Mongodb host.")
        .action((arg, cfg) => cfg.copy(dbHost = arg)),
      opt[String]("db-user-name")
        .text("Mongodb user name.")
        .action((arg, cfg) => cfg.copy(dbUserName = arg)),
      opt[String]("db-user-password")
        .text("Mongodb user password.")
        .action((arg, cfg) => cfg.copy(dbUserPassword = arg)),
    )
  }
}
