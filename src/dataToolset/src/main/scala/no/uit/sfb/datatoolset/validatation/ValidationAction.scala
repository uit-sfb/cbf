package no.uit.sfb.datatoolset.validatation

import com.typesafe.scalalogging.LazyLogging
import no.uit.sfb.cbf.jsonschema.Validator

object ValidationAction extends LazyLogging {
  def apply(cfg: ValidationConfig): Unit = {
    val validator = new Validator(cfg.schema)
    validator.validate(cfg.in)
  }
}
