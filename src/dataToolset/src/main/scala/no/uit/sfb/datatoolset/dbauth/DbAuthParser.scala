package no.uit.sfb.datatoolset.dbauth

import com.typesafe.scalalogging.LazyLogging
import no.uit.sfb.cbf.shared.model.v1.role.RoleOrdering
import scopt.OParser

object DbAuthParser extends LazyLogging {

  lazy val builder = OParser.builder[DbAuthConfig]

  lazy val parser = {
    import builder._
    OParser
      .sequence(
        head("Manage authorization list"),
        help('h', "help")
          .text("Prints this usage text"),
        opt[Unit]('a', "add")
          .text("Add/update authorization.")
          .action((_, cfg) => cfg.copy(add = true)),
        opt[Unit]('r', "rm")
          .text("Remove authorization.")
          .action((_, cfg) => cfg.copy(add = false)),
        opt[String]("db-name")
          .required()
          .text("Db name")
          .action((arg, cfg) => cfg.copy(dbName = arg)),
        opt[String]("db-host")
          .text("Mongodb host.")
          .action((arg, cfg) => cfg.copy(dbHost = arg)),
        opt[String]("db-user-name")
          .text("Mongodb user name.")
          .action((arg, cfg) => cfg.copy(dbUserName = arg)),
        opt[String]("db-user-password")
          .text("Mongodb user password.")
          .action((arg, cfg) => cfg.copy(dbUserPassword = arg)),
        opt[Unit]("db-ssl")
          .text("Use SSL.")
          .action((_, cfg) => cfg.copy(dbSsl = true)),
        arg[String]("identifier")
          .required()
          .text("Elixir identifier"),
        arg[String]("permission")
          .required()
          .text(
            "Permission for this database (One of: none < read < write < owner)."
          )
          .validate { v =>
            val permissions = RoleOrdering.roles
            if (permissions.contains(v))
              success
            else
              failure(
                s"Permission must be one of: ${permissions.mkString(", ")}."
              )
          }
      )
  }
}
