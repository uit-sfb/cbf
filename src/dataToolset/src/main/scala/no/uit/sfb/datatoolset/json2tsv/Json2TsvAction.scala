package no.uit.sfb.datatoolset.json2tsv

import com.typesafe.scalalogging.LazyLogging
import no.uit.sfb.cbf.shared.model.v1.builder.RecordBuilder
import no.uit.sfb.scalautils.common.FileUtils

import scala.util.Try

object Json2TsvAction extends LazyLogging {
  def apply(cfg: Json2TsvConfig): Unit = {
    val rb = cfg.order match {
      case Some(url) =>
        RecordBuilder.fromUrl(url.toString)
      case _ =>
        new RecordBuilder()
    }
    val str = (Try {
      rb.fromSingleJson(FileUtils.readFile(cfg.in.get)).asString
    }).toOption match {
      case Some(res) => res
      case None =>
        rb.toTsv(rb.fromJson(FileUtils.readFile(cfg.in.get)))
    }
    FileUtils.writeToFile(cfg.out.get, str)
  }
}
