package no.uit.sfb.datatoolset.dbrelease

import com.typesafe.scalalogging.LazyLogging
import scopt.OParser

object DbReleaseParser extends LazyLogging {

  lazy val builder = OParser.builder[DbReleaseConfig]

  lazy val parser = {
    import builder._
    OParser.sequence(
      head("Create a new version release"),
      help('h', "help")
        .text("Prints this usage text"),
      opt[String]("ver")
        .required()
        .text("The version to release.")
        .action((arg, cfg) => cfg.copy(version = arg)),
      opt[Unit]("create-next")
        .text("Create next draft.")
        .action((_, cfg) => cfg.copy(createNextDraft = true)),
      opt[String]("db-name")
        .required()
        .text("Name of the database.")
        .action((arg, cfg) => cfg.copy(dbName = arg)),
      opt[String]("db-host")
        .text("Mongodb host.")
        .action((arg, cfg) => cfg.copy(dbHost = arg)),
      opt[String]("db-user-name")
        .text("Mongodb user name.")
        .action((arg, cfg) => cfg.copy(dbUserName = arg)),
      opt[String]("db-user-password")
        .text("Mongodb user password.")
        .action((arg, cfg) => cfg.copy(dbUserPassword = arg)),
      opt[Unit]("db-ssl")
        .text("Use SSL.")
        .action((_, cfg) => cfg.copy(dbSsl = true)),
      opt[Unit]("if-modified")
        .text("Make release if at least one record was added/modified")
        .action((_, cfg) => cfg.copy(ifModified = true)),
    )
  }
}
