import org.mongodb.scala.bson.Document
import org.scalatest.funspec.AnyFunSpec
import org.scalatest.matchers.should.Matchers
import utils.Fixture

import scala.util.Random

class GraphTest extends AnyFunSpec with Matchers {
  val fix = Fixture
  val version = fix.db.parseVersion("1.0")

  describe("Graph") {
    it(s"should be able to count, avg and sum host:age") {
      //It works to project all the values, but then you would get a combinational issue when more than one field is an array
      //It would work to only limit the full projection to x
      val projectAll = Document("""{
          |  $project: {
          |    _id: 0,
          |    y: {
          |      $map: {
          |        input: "$attrs.host:age.obj.val",
          |        as: "v",
          |        in: {
          |          $convert : {
          |            input: {
          |              $first: "$$v"
          |            },
          |            to: "double",
          |            onError: null
          |          }
          |        }
          |      }
          |    }
          |  }
          |}""".stripMargin)
      val unwind = Document(
        """
          |{ $unwind: { path: "$y", preserveNullAndEmptyArrays: true } }""".stripMargin
      )
      val project =
        Document("""{
          |  $project: {
          |    _id: 0,
          |    y: {
          |      $convert : {
          |        input: {
          |          $first: { $first: "$attrs.host:age.obj.val" }
          |        },
          |        to: "double",
          |        onError: null
          |      }
          |    }
          |  }
          |}""".stripMargin)
      val reduce =
        Document("""
          |{
          |  $group: {
          |    _id: null,
          |    count: {
          |      $sum: {
          |        $cond: {
          |          if: { $eq: [ "$y", null ] }, then: 0, else: 1
          |        }
          |      }
          |    },
          |    avg: { $avg: "$y" },
          |    sum: { $sum: "$y" },
          |    max: { $max: "$y" },
          |  }
          |}""".stripMargin)
      val pipeline = Seq(project, reduce)
      val docs = fix.db.recordsCol(version).aggregate(pipeline).toSeq
      docs.size should be(1)
      docs.head.getInteger("count") should be(9)
      //res.head.getDouble("avg") should be(40.7)
      docs.head.getDouble("sum") should be(355.0)
      docs.head.getDouble("max") should be(63.0)
    }

    it(
      s"should be able to group by countries, count and avg age for each group"
    ) {
      val project = Document(
        """{
          |  $project: {
          |    _id: 0,
          |    x: {
          |          $ifNull: [
          |            { $first: {
          |                $first: { $first: "$attrs.loc:country.obj.iri" }
          |              }
          |            },
          |            { $cond: {
          |                if: { $eq: [ { $first: { $first: "$attrs.loc:country.obj.val" } }, "" ] },
          |                then: null,
          |                else: { $first: { $first: "$attrs.loc:country.obj.val" } }
          |              }
          |            }
          |          ]
          |      },
          |    y: {
          |      $convert : {
          |        input: {
          |          $first: { $first: "$attrs.host:age.obj.val" }
          |        },
          |        to: "double",
          |        onError: null
          |      }
          |    }
          |  }
          |}""".stripMargin
      )
      val reduce = Document("""
          |{
          |  $group: {
          |    _id: "$x",
          |    count: {
          |      $sum: 1
          |    },
          |    avg: { $avg: "$y" }
          |  }
          |}""".stripMargin)
      val pipeline = Seq(project, reduce)
      val docs = fix.db.recordsCol(version).aggregate(pipeline).toSeq
      docs.length should be(7)
      docs.head.getInteger("count") > 0 should be(true)
    }

    it(
      s"should be able to group by (daily) date, percent and avg age for each group"
    ) {
      val project =
        Document(s"""{
           |  $$project: {
           |    _id: 0,
           |    x: {
           |      $$dateToString: {
           |        date: {
           |          $$dateFromString: {
           |            dateString: { $$first: { $$first: "$$attrs.seq:submission_date.obj.val" } },
           |            format: "%Y-%m-%d",
           |            onError: {
           |                $$dateFromString: {
           |                  dateString: { $$concat: [ { $$first: { $$first: "$$attrs.seq:submission_date.obj.val" } }, "-${Random
                      .nextInt()
                      .abs % 28 + 1}" ] },
           |                  format: "%Y-%m-%d",
           |                  onError: {
           |                    $$dateFromString: {
           |                      dateString: { $$concat: [ { $$first: { $$first: "$$attrs.seq:submission_date.obj.val" } }, "-${Random
                      .nextInt()
                      .abs % 11 + 1}-${Random.nextInt().abs % 29 + 1}" ] },
           |                      format: "%Y-%m-%d",
           |                      onError: null
           |                    }
           |                  }
           |                }
           |              }
           |          }
           |        },
           |        format: "%Y-%m-%d"
           |      }
           |    },
           |    y: {
           |      $$convert : {
           |        input: {
           |          $$first: { $$first: "$$attrs.host:age.obj.val" }
           |        },
           |        to: "double",
           |        onError: null
           |      }
           |    }
           |  }
           |}""".stripMargin)
      val unwind = Document("""{
          |  "$unwind": "$doc"
          |}""".stripMargin)
      val reduce = Document("""
          |{
          |  $group: {
          |    _id: "$x",
          |    count: {
          |      $sum: 1
          |    },
          |    avg: { $avg: "$y" }
          |  }
          |}""".stripMargin)
      val higherOrder = Document("""{
          | "$group": {
          |    _id: null,
          |    doc: { $push: "$$ROOT" },
          |    countAll: { $sum: "$count" }
          |  }
          |}""".stripMargin)
      //Note: the $max is because we need to choose an operator even though we now there is only one iterm per group. $max respects null.
      val regroup = Document(
        """{
          | "$group": {
          |    _id: "$doc._id",
          |    percent: { $max: { $divide: [ "$doc.count", "$countAll" ] } },
          |    avg: { $max: "$doc.avg" }
          |  }
          |}""".stripMargin
      )
      //higherOrder, unwind, and regroup are only here because of the percent
      val pipeline = Seq(project, reduce, higherOrder, unwind, regroup)
      val docs = fix.db.recordsCol(version).aggregate(pipeline).toSeq
      docs.size should be(1)
      docs.head.getString("_id") should be("2019-12-01")
      docs.head.getDouble("percent") should be(1.0)
    }
  }
}
