package utils

import no.uit.sfb.cbf.mongodb.{CbfDataset, MongodbConnect}
import no.uit.sfb.cbf.shared.model.v1.builder.RecordBuilder
import no.uit.sfb.datatoolset.dbauth.{DbAuthAction, DbAuthConfig}
import no.uit.sfb.datatoolset.dbdataset.{DbDatasetAction, DbDatasetConfig}
import no.uit.sfb.datatoolset.dbpush.{DbPushAction, DbPushConfig}

import java.nio.file.Paths
import scala.concurrent.duration.DurationInt
import scala.concurrent.{Await, ExecutionContext}

object Fixture {
  implicit val ec = ExecutionContext.global
  val dbName = "test1"
  val db = new CbfDataset(
    MongodbConnect()
      .db(dbName)
  )
  val tsvPath = Paths.get(getClass.getResource("/entries.tsv").getPath)
  val configUri = getClass.getResource("/config/test1-config.json").toURI
  val rb = new RecordBuilder()

  Await.result(db.drop(), 30.seconds)

  DbDatasetAction(DbDatasetConfig(config = configUri))

  DbAuthAction(
    DbAuthConfig(
      "2c35852383061897b6e363a3393259377696b130@elixir-europe.org",
      "owner",
      dbName = dbName
    )
  )

  DbPushAction(DbPushConfig(in = tsvPath, dbName = dbName, version = "1.0"))
}
