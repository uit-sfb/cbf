import no.uit.sfb.cbf.mongodb.{CbfDataset, MongodbConnect}
import no.uit.sfb.cbf.shared.config.DatasetConfig
import no.uit.sfb.cbf.shared.errors.BadRequestClientError
import no.uit.sfb.cbf.shared.model.v1.builder.RecordBuilder
import no.uit.sfb.cbf.shared.model.v1.nativ.Validity
import no.uit.sfb.cbf.utils.UriUtils
import no.uit.sfb.datatoolset.dbdataset.{DbDatasetAction, DbDatasetConfig}
import no.uit.sfb.datatoolset.dbpatch.{DbPatchAction, DbPatchConfig}
import no.uit.sfb.datatoolset.dbpush.{DbPushAction, DbPushConfig}
import no.uit.sfb.datatoolset.dbreleaseAll.{
  DbReleaseAllAction,
  DbReleaseAllConfig
}
import org.mongodb.scala.model.Filters
import org.scalatest.funspec.AnyFunSpec
import org.scalatest.matchers.should.Matchers

import java.nio.file.Paths
import scala.concurrent.duration.DurationInt
import scala.concurrent.{Await, ExecutionContext}

class ReleaseTest extends AnyFunSpec with Matchers {
  private val tsvPath1 = Paths.get(getClass.getResource("/entries.tsv").getPath)
  private val tsvPath2 =
    Paths.get(getClass.getResource("/entries2.tsv").getPath)
  private val configUri1 =
    getClass.getResource("/config/test4-config.json").toURI
  private val configUri2 =
    getClass.getResource("/config/test5-config.json").toURI
  private val datasetConfig1 = DatasetConfig(UriUtils.getContent(configUri1))
  private val datasetConfig2 = DatasetConfig(UriUtils.getContent(configUri2))
  private val updatePath =
    Paths.get(getClass.getResource("/update.tsv").getPath)
  private val dbName1 = datasetConfig1.name
  private val dbName2 = datasetConfig2.name

  private implicit val ec = ExecutionContext.global
  private val mgc = MongodbConnect()
  private val cbf1 = new CbfDataset(mgc.db(dbName1))
  private val cbf2 = new CbfDataset(mgc.db(dbName2))
  Await.result(cbf1.drop().flatMap(_ => cbf2.drop()), 2.minute)

  private val rb = new RecordBuilder()
  private val v1 = cbf1
    .parseVersion("1.0")
  private val v2 = cbf1
    .parseVersion("1.1")
  private val v3 = cbf1
    .parseVersion("1.2")

  describe("Config writer should") {
    it(s"should push configs for test4 db to MongoDb ($dbName1/_config)") {
      DbDatasetAction(DbDatasetConfig(config = configUri1))
      val f = cbf1.configsCol.count()
      val nbDocs = Await.result(f, 1.minute)
      nbDocs should be(1)
    }
  }

  describe("Config writer should") {
    it(s"should push configs for test5 db to MongoDb ($dbName2/_config)") {
      DbDatasetAction(DbDatasetConfig(config = configUri2))
      val f = cbf2.configsCol.count()
      val nbDocs = Await.result(f, 1.minute)
      nbDocs should be(1)
    }
  }

  describe("Pusher should") {
    it(s"should push version ${v1.ver} to MongoDb ($dbName1/${v1.branch})") {
      DbPushAction(
        DbPushConfig(in = tsvPath1, dbName = dbName1, version = v1.ver)
      )
      val f = cbf1.errorsCol(v1).count()
      val nbErrors = Await.result(f, 1.minute)
      nbErrors should be(0)
      val f2 = cbf1.recordsCol(v1).count()
      val nbDocs = Await.result(f2, 1.minute)
      nbDocs should be(24)
      val usaRecords =
        Await.result(
          cbf1
            .recordsCol(v1)
            .getOne(Filters.eq("id", "SFB_COVID19_MT184910"))
            .map {
              _.map { doc =>
                rb.fromSingleJson(doc.toJson())
              }
            },
          1.minute
        )

    }
  }

  describe("Pusher should") {
    it(s"should push version ${v1.ver} to MongoDb ($dbName2/${v1.branch})") {
      DbPushAction(
        DbPushConfig(in = tsvPath2, dbName = dbName2, version = v1.ver)
      )
      val f = cbf2.errorsCol(v1).count()
      val nbErrors = Await.result(f, 1.minute)
      nbErrors should be(0)
      val f2 = cbf2.recordsCol(v1).count()
      val nbDocs = Await.result(f2, 1.minute)
      nbDocs should be(3)
      val usaRecords =
        Await.result(
          cbf2
            .recordsCol(v1)
            .getOne(Filters.eq("id", "SFB_COVID19_MT184910"))
            .map {
              _.map { doc =>
                rb.fromSingleJson(doc.toJson())
              }
            },
          1.minute
        )

    }
  }

  describe("Pusher should") {
    it(s"should push version ${v2.ver} to MongoDb ($dbName1/${v2.branch})") {
      DbPushAction(
        DbPushConfig(in = tsvPath2, dbName = dbName1, version = v2.ver)
      )
      val f = cbf1.errorsCol(v2).count()
      val nbErrors = Await.result(f, 1.minute)
      nbErrors should be(0)
      val newRec =
        Await.result(
          cbf1
            .recordsCol(v2)
            .get(Filters.eq("id", "SFB_COVID19_MT350244"))
            .map {
              _.map { doc =>
                rb.fromSingleJson(doc.toJson())
              }
            },
          1.minute
        )
      newRec.size should be(1)
      newRec.exists(rec => rec.valid == Validity(Some(1), None)) should be(true)
      val updatedRecs =
        Await.result(
          cbf1
            .recordsCol(v2)
            .get(Filters.eq("id", "SFB_COVID19_MT126808"))
            .map {
              _.map { doc =>
                rb.fromSingleJson(doc.toJson())
              }
            },
          1.minute
        )
      updatedRecs.size should be(2)
      updatedRecs.exists(
        rec =>
          rec.valid == Validity(Some(0), Some(1)) && rec
            .stm("host:age")
            .head
            .simpleValue == "32"
      ) should be(true)
      updatedRecs.exists(
        rec =>
          rec.valid == Validity(Some(1), None) && rec
            .stm("host:age")
            .head
            .simpleValue == "52"
      ) should be(true)
      val copy =
        Await.result(
          cbf1
            .recordsCol(v2)
            .get(Filters.eq("id", "SFB_COVID19_MT093631"))
            .map {
              _.map { doc =>
                rb.fromSingleJson(doc.toJson())
              }
            },
          1.minute
        )
      copy.size should be(1)
      copy.exists(rec => rec.valid == Validity(Some(0), None)) should be(true)
      val deleted =
        Await.result(
          cbf1
            .recordsCol(v2)
            .get(Filters.eq("id", "SFB_COVID19_MT007544"))
            .map {
              _.map { doc =>
                rb.fromSingleJson(doc.toJson())
              }
            },
          1.minute
        )
      deleted.size should be(1)
      deleted.exists(rec => rec.valid == Validity(Some(0), Some(1))) should be(
        true
      )
      deleted.exists(rec => rec.valid == Validity(Some(1), None)) should be(
        false
      )
      val deleted2 =
        Await.result(
          cbf1
            .recordsCol(v2)
            .get(Filters.eq("id", "SFB_COVID19_MT999999"))
            .map {
              _.map { doc =>
                rb.fromSingleJson(doc.toJson())
              }
            },
          1.minute
        )
      deleted2.size should be(0)
      val f2 = cbf1.recordsCol(v2).count()
      val nbDocs = Await.result(f2, 1.minute)
      nbDocs should be(26) //24+2 (cf comment above)
    }
  }

  describe("Version releaser for multiple dbs should") {
    it(
      s"should release version ${v1.ver} for test4, test5 and create draft for ${v2.ver}"
    ) {
      DbReleaseAllAction(
        DbReleaseAllConfig(
          dbNames = Seq("test4", "test5"),
          version = v1.ver,
          ifModified = true,
          createNextDraft = true
        )
      )
      val f = for {
        r1 <- new CbfDataset(mgc.db(dbName1)).versionsCol.count()
        r2 <- new CbfDataset(mgc.db(dbName2)).versionsCol.count()
      } yield (r1 + r2)

      val nbDocs = Await.result(f, 1.minute)
      nbDocs should be(4)
    }
  }
  //
  //
  describe("Version releaser for multiple dbs should") {
    it(s"should release version ${v2.ver} for for test4, test5") {
      DbReleaseAllAction(
        DbReleaseAllConfig(
          dbNames = Seq("test4", "test5"),
          version = "1.x",
          ifModified = true,
          createNextDraft = false
        )
      )
      val f = for {
        r1 <- new CbfDataset(mgc.db(dbName1)).versionsCol.count()
        r2 <- new CbfDataset(mgc.db(dbName2)).versionsCol.count()
      } yield (r1 + r2)

      val nbDocs = Await.result(f, 1.minute)
      nbDocs should be(4)
    }
  }
  //
  describe("Version releaser for multiple dbs should") {
    it(s"should not release version ${v3.ver} for for test4, test5") {
      DbReleaseAllAction(
        DbReleaseAllConfig(
          dbNames = Seq("test4", "test5"),
          version = v3.ver,
          ifModified = true,
          createNextDraft = false
        )
      )
      val f = for {
        r1 <- new CbfDataset(mgc.db(dbName1)).versionsCol.count()
        r2 <- new CbfDataset(mgc.db(dbName2)).versionsCol.count()
      } yield (r1 + r2)

      val nbDocs = Await.result(f, 1.minute)
      nbDocs should be(4)
    }
  }
  //
  describe("Version releaser for multiple dbs should") {
    it(s"should create draft for ${v3.ver} for for test4, test5") {
      DbReleaseAllAction(
        DbReleaseAllConfig(
          dbNames = Seq("test4", "test5"),
          version = v2.ver,
          ifModified = true,
          createNextDraft = true
        )
      )
      val f = for {
        r1 <- new CbfDataset(mgc.db(dbName1)).versionsCol.count()
        r2 <- new CbfDataset(mgc.db(dbName2)).versionsCol.count()
      } yield (r1 + r2)

      val nbDocs = Await.result(f, 1.minute)
      nbDocs should be(6)
    }
  }

  describe("Version releaser for multiple dbs should") {
    it(s"should not release version 2.0 for for test4, test5") {
      assertThrows[BadRequestClientError] {
        DbReleaseAllAction(
          DbReleaseAllConfig(
            dbNames = Seq("test4", "test5"),
            version = "2.x",
            ifModified = true,
            createNextDraft = false
          )
        )
      }
    }
  }

}
