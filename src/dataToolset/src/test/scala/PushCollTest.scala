import no.uit.sfb.cbf.mongodb.{CbfDataset, MongodbConnect}
import no.uit.sfb.cbf.shared.config.DatasetConfig
import no.uit.sfb.cbf.utils.UriUtils
import no.uit.sfb.datatoolset.dbpushcoll.{DbPushCollAction, DbPushCollConfig}
import org.scalatest.funspec.AnyFunSpec
import org.scalatest.matchers.should.Matchers

import java.nio.file.Paths
import scala.concurrent.duration.DurationInt
import scala.concurrent.{Await, ExecutionContext}

class PushCollTest extends AnyFunSpec with Matchers {

  private val configUri =
    getClass.getResource("/config/test2-config.json").toURI
  private val datasetConfig = DatasetConfig(UriUtils.getContent(configUri))

  private val data =
    Paths.get(getClass.getResource("/json_arr_test.json").getPath)


  private val dbName = datasetConfig.name
  private implicit val ec = ExecutionContext.global
  private val mgc = MongodbConnect()
  private val cbf = new CbfDataset(mgc.db(dbName))

  private val v1 = cbf.parseVersion("1.0")
  private val v2 = cbf.parseVersion("2.0")

  /*
 Do not uncomment, use manually, not an obligatory test!
*/
//  describe("Data pusher should") {
//    it(s"should push data from json array to dedicated collection ${v1.ver}") {
//
//      DbPushCollAction(
//        DbPushCollConfig(
//          dbName = dbName,
//          version = v1.ver,
//          in = data.toString,
//        )
//      )
//
//      val f = new CbfDataset(mgc.db(dbName)).recordsCol(v1).count()
//
//      val nbDocs = Await.result(f,5.minute)
//
//      nbDocs should be(28)
//    }
//  }
}
