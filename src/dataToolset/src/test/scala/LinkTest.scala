import no.uit.sfb.cbf.mongodb.{CbfDataset, MongodbConnect}
import no.uit.sfb.cbf.shared.config.DatasetConfig
import no.uit.sfb.cbf.utils.UriUtils
import no.uit.sfb.datatoolset.dbdataset.{DbDatasetAction, DbDatasetConfig}
import org.scalatest.funspec.AnyFunSpec
import org.scalatest.matchers.should.Matchers

import scala.concurrent.duration.DurationInt
import scala.concurrent.{Await, ExecutionContext}

class LinkTest extends AnyFunSpec with Matchers {
  private val configUri =
    getClass.getResource("/config/test3-config.json").toURI
  private val datasetConfig = DatasetConfig(UriUtils.getContent(configUri))
  private val dbName = datasetConfig.name

  private implicit val ec = ExecutionContext.global
  private val mgc = MongodbConnect()
  private val cbf = new CbfDataset(mgc.db(dbName))
  Await.result(cbf.drop(), 1.minute)

  describe("Config writer should") {
    it(s"should push configs to MongoDb ($dbName/_config)") {
      DbDatasetAction(DbDatasetConfig(config = configUri))
      val f = cbf.configsCol.count()
      val nbDocs = Await.result(f, 1.minute)
      nbDocs should be(1)
    }
  }
}
