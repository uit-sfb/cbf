import no.uit.sfb.cbf.mongodb.{CbfDataset, MongodbConnect}
import no.uit.sfb.cbf.shared.config.DatasetConfig
import no.uit.sfb.cbf.shared.errors.BadRequestClientError
import no.uit.sfb.cbf.shared.model.v1.builder.RecordBuilder
import no.uit.sfb.cbf.shared.model.v1.nativ.Validity
import no.uit.sfb.cbf.utils.UriUtils
import no.uit.sfb.datatoolset.cdch.{CdchAction, CdchConfig}
import no.uit.sfb.datatoolset.dbauth.{DbAuthAction, DbAuthConfig}
import no.uit.sfb.datatoolset.dbdataset.{DbDatasetAction, DbDatasetConfig}
import no.uit.sfb.datatoolset.dbpatch.{DbPatchAction, DbPatchConfig}
import no.uit.sfb.datatoolset.dbpush.{DbPushAction, DbPushConfig}
import no.uit.sfb.datatoolset.dbrelease.{DbReleaseAction, DbReleaseConfig}
import no.uit.sfb.datatoolset.dbreleaseAll.{
  DbReleaseAllAction,
  DbReleaseAllConfig
}
import org.mongodb.scala.model.Filters
import org.scalatest.funspec.AnyFunSpec
import org.scalatest.matchers.should.Matchers

import java.nio.file.Paths
import scala.concurrent.duration._
import scala.concurrent.{Await, ExecutionContext}

class PushTest extends AnyFunSpec with Matchers {
  private val tsvPath1 = Paths.get(getClass.getResource("/entries_short_mar.tsv").getPath)
  private val tsvPath2 = Paths.get(getClass.getResource("/entries2.tsv").getPath)
  private val updatePath =
    Paths.get(getClass.getResource("/update.tsv").getPath)

  private val configUri =
    getClass.getResource("/config/MarDB-config.json").toURI

//  private val configSarsCovUri =
//    getClass.getResource("/config/test2-config.json").toURI

  private val datasetConfig = DatasetConfig(UriUtils.getContent(configUri))
  private val dbName = datasetConfig.name

  private implicit val ec = ExecutionContext.global

  private val mgc = MongodbConnect()
  private val cbf = new CbfDataset(mgc.db(dbName))
  Await.result(cbf.drop(), 1.minute)

  private val rb = new RecordBuilder()
  private val v1 = cbf
    .parseVersion("1.0")
  private val v2 = cbf
    .parseVersion("1.1")
  private val v3 = cbf
    .parseVersion("1.2")

  describe("Config writer should") {
    it(s"should push configs to MongoDb ($dbName/_config)") {
      DbDatasetAction(DbDatasetConfig(config = configUri))
      val f = cbf.configsCol.count()
      val nbDocs = Await.result(f, 1.minute)
      nbDocs should be(1)
    }
  }

  describe("Auth writer should") {
    it(s"should push configs to MongoDb ($dbName/_auth)") {
      DbAuthAction(
        DbAuthConfig(
          "2c35852383061897b6e363a3393259377696b130@elixir-europe.org",
          "owner",
          dbName = dbName
        )
      )
      DbAuthAction(
        DbAuthConfig(
          "acca23544f86d9e4bbe85b0c942e2f87f3b9ba1b@elixir-europe.org",
          "owner",
          dbName = dbName
        )
      )
      val f = cbf.authCol.count()
      val nbDocs = Await.result(f, 1.minute)
      nbDocs should be(2)
    }
  }

  describe("Pusher should") {
    it(s"should push version ${v1.ver} to MongoDb ($dbName/${v1.branch})") {
      DbPushAction(
        DbPushConfig(in = tsvPath1, dbName = dbName, version = v1.ver)
      )
      val f = cbf.errorsCol(v1).count()
      val nbErrors = Await.result(f, 1.minute)
      nbErrors should be(0)
      val f2 = cbf.recordsCol(v1).count()
      val nbDocs = Await.result(f2, 1.minute)
      nbDocs should be(20)
    }
  }

//  describe("Pusher should") {
//    it(s"should push version ${v1.ver} to MongoDb ($dbNameSarsCov/${v1.branch})") {
//      DbPushAction(
//        DbPushConfig(in = tsvPath2, dbName = dbNameSarsCov, version = v1.ver)
//      )
//      val f = cbfSarsCov.errorsCol(v1).count()
//      val nbErrors = Await.result(f, 1.minute)
//      nbErrors should be(0)
//      val f2 = cbfSarsCov.recordsCol(v1).count()
//      val nbDocs = Await.result(f2, 1.minute)
//      nbDocs should be(6)
//            val usaRecords =
//              Await.result(
//                cbfSarsCov
//                  .recordsCol(v1)
//                  .getOne(Filters.eq("id", "SFB_COVID19_MT184910"))
//                  .map {
//                    _.map { doc =>
//                      rb.fromSingleJson(doc.toJson())
//                    }
//                  },
//                1.minute
//              )
//
//    }
//  }

  describe("Version releaser should") {
    it(s"should release version ${v1.ver} ($dbName/_versions)") {
      DbReleaseAction(DbReleaseConfig(dbName = dbName, version = v1.ver))
      val f = cbf.versionsCol.count()
      val nbDocs = Await.result(f, 1.minute)
      nbDocs should be(1)
    }
  }

  describe("Version releaser with --is-modified flag should") {
    it(s"should release version ${v1.ver} ($dbName/_versions)") {
      DbReleaseAction(
        DbReleaseConfig(dbName = dbName, version = v1.ver, ifModified = true)
      )
      val f = cbf.versionsCol.count()
      val nbDocs = Await.result(f, 1.minute)
      nbDocs should be(1)
    }
  }
//
  describe("Version releaser should") {
    it(s"should not release version ${v3.ver} ($dbName/_versions)") {
      DbReleaseAction(
        DbReleaseConfig(dbName = dbName, version = v3.ver, ifModified = true)
      )
      val f = cbf.versionsCol.count()
      val nbDocs = Await.result(f, 1.minute)
      nbDocs should be(1)
    }
  }

  //l2: new record -> One new doc
  //l3: same record but updated -> no new doc (Note that this is not officially supported as the result is undefined)
  //l4: re-add an existing record without any changes -> no new doc
  //l5: re-add an existing record with changes -> one new doc
  //l6: delete existing record -> no new doc
  //l7: delete record which does not exist -> no new doc
//  describe("Pusher should") {
//    it(s"should push version ${v2.ver} to MongoDb ($dbNameSarsCov/${v2.branch})") {
//      DbPushAction(
//        DbPushConfig(in = tsvPath2, dbName = dbNameSarsCov, version = v2.ver)
//      )
//      val f = cbfSarsCov.errorsCol(v2).count()
//      val nbErrors = Await.result(f, 1.minute)
//      nbErrors should be(0)
//      val newRec =
//        Await.result(
//          cbfSarsCov
//            .recordsCol(v2)
//            .get(Filters.eq("id", "SFB_COVID19_MT350244"))
//            .map {
//              _.map { doc =>
//                rb.fromSingleJson(doc.toJson())
//              }
//            },
//          1.minute
//        )
//      newRec.size should be(1)
//      newRec.exists(rec => rec.valid == Validity(Some(1), None)) should be(true)
//      val updatedRecs =
//        Await.result(
//          cbfSarsCov
//            .recordsCol(v2)
//            .get(Filters.eq("id", "SFB_COVID19_MT126808"))
//            .map {
//              _.map { doc =>
//                rb.fromSingleJson(doc.toJson())
//              }
//            },
//          1.minute
//        )
//      updatedRecs.size should be(2)
//      updatedRecs.exists(
//        rec =>
//          rec.valid == Validity(Some(0), Some(1)) && rec
//            .stm("host:age")
//            .head
//            .simpleValue == "32"
//      ) should be(true)
//      updatedRecs.exists(
//        rec =>
//          rec.valid == Validity(Some(1), None) && rec
//            .stm("host:age")
//            .head
//            .simpleValue == "52"
//      ) should be(true)
//      val copy =
//        Await.result(
//          cbfSarsCov
//            .recordsCol(v2)
//            .get(Filters.eq("id", "SFB_COVID19_MT093631"))
//            .map {
//              _.map { doc =>
//                rb.fromSingleJson(doc.toJson())
//              }
//            },
//          1.minute
//        )
//      copy.size should be(1)
//      copy.exists(rec => rec.valid == Validity(Some(0), None)) should be(true)
//      val deleted =
//        Await.result(
//          cbfSarsCov
//            .recordsCol(v2)
//            .get(Filters.eq("id", "SFB_COVID19_MT007544"))
//            .map {
//              _.map { doc =>
//                rb.fromSingleJson(doc.toJson())
//              }
//            },
//          1.minute
//        )
//      deleted.size should be(1)
//      deleted.exists(rec => rec.valid == Validity(Some(0), Some(1))) should be(
//        true
//      )
//      deleted.exists(rec => rec.valid == Validity(Some(1), None)) should be(
//        false
//      )
//      val deleted2 =
//        Await.result(
//          cbfSarsCov
//            .recordsCol(v2)
//            .get(Filters.eq("id", "SFB_COVID19_MT999999"))
//            .map {
//              _.map { doc =>
//                rb.fromSingleJson(doc.toJson())
//              }
//            },
//          1.minute
//        )
//      deleted2.size should be(0)
//      val f2 = cbfSarsCov.recordsCol(v2).count()
//      val nbDocs = Await.result(f2, 1.minute)
//      nbDocs should be(26) //24+2 (cf comment above)
//    }
//  }

//  describe("Pusher should") {
//    it(s"should re-push version ${v2.ver} to MongoDb ($dbName/${v2.branch})") {
//      DbPushAction(
//        DbPushConfig(
//          in = tsvPath1,
//          dbName = dbName,
//          version = v2.ver,
//          force = true
//        )
//      )
//      val f = cbf.errorsCol(v2).count()
//      val nbErrors = Await.result(f, 1.minute)
//      nbErrors should be(0)
//      val f2 = cbf.recordsCol(v2).count()
//      val nbDocs = Await.result(f2, 1.minute)
//      nbDocs should be(11) //We should of course have the same number of docs than before!
//    }
//  }
//
//  //Here we update v1
//  describe("Updater should") {
//    it(s"should update version ${v1.ver} to MongoDb ($dbName/${v1.branch})") {
//      DbPatchAction(
//        DbPatchConfig(
//          in = updatePath,
//          dbName = dbName,
//          version = v1.ver,
//          force = true
//        )
//      )
//      val f = cbf.errorsCol(v1).count()
//      val nbErrors = Await.result(f, 1.minute)
//      nbErrors should be(0)
//      val rb = new RecordBuilder()
//      val updatedRecs =
//        Await.result(
//          cbf
//            .recordsCol(v1)
//            .get(Filters.eq("id", "SFB_COVID19_MN908947"))
//            .map {
//              _.map { doc =>
//                rb.fromSingleJson(doc.toJson())
//              }
//            },
//          1.minute
//        )
//      updatedRecs.size should be(2)
//      updatedRecs.exists(_.valid == Validity(Some(0), Some(1)))
//      updatedRecs.exists(_.valid == Validity(Some(1), None))
//      updatedRecs.foreach { rec =>
//        rec.attributes.count {
//          _._1 == "id"
//        } should be(1)
//      } //We check update did not mess up with the attributes
//      val updatedRecs2 =
//        Await.result(
//          cbf
//            .recordsCol(v1)
//            .get(Filters.eq("id", "SFB_COVID19_MT126808"))
//            .map {
//              _.map { doc =>
//                rb.fromSingleJson(doc.toJson())
//              }
//            },
//          1.minute
//        )
//      updatedRecs2.size should be(2)
//      updatedRecs2.exists(
//        rec =>
//          rec.valid == Validity(Some(0), Some(1)) && rec
//            .stm("lin:pangolin:note")
//            .head
//            .simpleValue == "Failed:Alignment<100000"
//      ) should be(true)
//      val f2 = cbf.recordsCol(v1).count()
//      val nbDocs = Await.result(f2, 1.minute)
//      nbDocs should be(27) //Since we update MN908947, we create a new record
//    }
//  }
//
//  //We update v2 with the same update we applied to v1.
//  describe("Updater should") {
//    it(s"should update version ${v2.ver} to MongoDb ($dbName/${v2.branch})") {
//      DbPatchAction(
//        DbPatchConfig(in = updatePath, dbName = dbName, version = v2.ver)
//      )
//      val f = cbf.errorsCol(v2).count()
//      val nbErrors = Await.result(f, 1.minute)
//      nbErrors should be(0)
//      val rb = new RecordBuilder()
//      val updatedRecs =
//        Await.result(
//          cbf
//            .recordsCol(v2)
//            .get(Filters.eq("id", "SFB_COVID19_MN908947"))
//            .map {
//              _.map { doc =>
//                rb.fromSingleJson(doc.toJson())
//              }
//            },
//          1.minute
//        )
//      updatedRecs.size should be(1) //When we apply the update we fallback to the same value as v1, therefore v1 and v2 should be merged merged -> only one document
//      updatedRecs.head.attributes.count {
//        _._1 == "id"
//      } should be(1)
//      updatedRecs.head.valid should be(Validity(Some(0), None))
//      val updatedRecs2 =
//        Await.result(
//          cbf
//            .recordsCol(v2)
//            .get(Filters.eq("id", "SFB_COVID19_MT126808"))
//            .map {
//              _.map { doc =>
//                rb.fromSingleJson(doc.toJson())
//              }
//            },
//          1.minute
//        )
//      updatedRecs2.size should be(2)
//      updatedRecs2.exists { rec =>
//        rec.valid == Validity(Some(1), None) && rec
//          .stm("lin:pangolin:note")
//          .head
//          .simpleValue == "Failed:Alignment<100000"
//      } should be(true)
//      val f2 = cbf.recordsCol(v2).count()
//      val nbDocs = Await.result(f2, 1.minute)
//      nbDocs should be(26) //Since MN908947 was unmodified, when we apply the update we fallback to the same value as v1, therefore v1 and v2 should be merged merged
//    }
//  }
//
//  private val mapping =
//    Paths.get(getClass.getResource("/cdch_mappings.tsv").getPath)
//  describe("Cdch") {
//    it(s"should push curation objects for v1.1 only") {
//      CdchAction(
//        CdchConfig(
//          dsName = dbName,
//          ver = "1.0",
//          mapping = mapping,
//          accessionAttribute = "acc:genbank",
//          out = Paths.get("target/cdch_err.tsv")
//        )
//      )
//    }
//  }
}
