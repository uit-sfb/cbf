import no.uit.sfb.cbf.mongodb.{CbfDataset, MongodbConnect}
import no.uit.sfb.cbf.shared.config.DatasetConfig
import no.uit.sfb.cbf.utils.UriUtils
import no.uit.sfb.datatoolset.dbmove.{DbMoveAction, DbMoveConfig}
import no.uit.sfb.datatoolset.dbremovedup.{DbRemoveAction, DbRemoveConfig}
import org.scalatest.funspec.AnyFunSpec
import org.scalatest.matchers.should.Matchers

import scala.concurrent.duration.DurationInt
import scala.concurrent.{Await, ExecutionContext}

class RemoveDuplicatesTest extends AnyFunSpec with Matchers {

  private val configUri =
    getClass.getResource("/config/test2-config.json").toURI
  private val datasetConfig = DatasetConfig(UriUtils.getContent(configUri))

  private val dbName = datasetConfig.name
  private implicit val ec = ExecutionContext.global
  private val mgc = MongodbConnect()
  private val cbf = new CbfDataset(mgc.db(dbName))

  private val v1 = cbf.parseVersion("1.0")
  private val v2 = cbf.parseVersion("2.0")

  /*
 Use manually in case of MoveAction modification
*/
//  describe("Remove duplicates should") {
//    it(s"should remove duplicates from version ${v1.ver}, which are present in ${v2.ver}") {
//      DbRemoveAction(
//        DbRemoveConfig(
//          dbName = dbName,
//          ver1 = v1.ver, // collection to check and modify
//          ver2 = v2.ver, // collection to check against
//          limit = "10"
//        )
//      )
//      val f = new CbfDataset(mgc.db(dbName)).recordsCol(v1).count()
//
//      val nbDocs = Await.result(f, 5.minute)
//
//      nbDocs should be(0)
//    }
//  }

}
