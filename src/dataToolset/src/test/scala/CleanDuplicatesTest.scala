import no.uit.sfb.cbf.mongodb.{CbfDataset, MongodbConnect}
import no.uit.sfb.cbf.shared.config.DatasetConfig
import no.uit.sfb.cbf.utils.UriUtils
import no.uit.sfb.datatoolset.dbcleandup.{DbCleanDupAction, DbCleanDupConfig}
import org.scalatest.funspec.AnyFunSpec
import org.scalatest.matchers.should.Matchers

import scala.concurrent.duration.DurationInt
import scala.concurrent.{Await, ExecutionContext}

class CleanDuplicatesTest extends AnyFunSpec with Matchers {

  private val configUri =
    getClass.getResource("/config/test2-config.json").toURI
  private val datasetConfig = DatasetConfig(UriUtils.getContent(configUri))

  private val dbName = datasetConfig.name
  private implicit val ec = ExecutionContext.global
  private val mgc = MongodbConnect()
  private val cbf = new CbfDataset(mgc.db(dbName))

  private val version = cbf.parseVersion("2.0")


  /*
 Use manually in case of MoveAction modification
*/
//  describe("Remove duplicates should") {
//    it(s"should remove duplicates from version ${version.ver}") {
//      DbCleanDupAction(
//        DbCleanDupConfig(
//          dbName = dbName,
//          version = version.ver, // collection to check and modify
//          limit = "10"
//        )
//      )
//      val f = new CbfDataset(mgc.db(dbName)).recordsCol(version).count()
//
//      val nbDocs = Await.result(f, 5.minute)
//
//      nbDocs should be(9)
//    }
//  }

}
