# Data toolset

Command line interface (CLI) for interacting with CBF datasets and some extra convenience standalone functions.
Note: most of the command's functionalities listed below are also accessible via the API.

| Command              | Description                         |
|----------------------|-------------------------------------|
| admin / auth         | Manage members and roles.           |
| dataset              | Manage datasets and configs.        |
| push / patch         | Upload TSV data (push and patch).   |
| release / releaseAll | Manage versions.                    |
| cdch                 | Export to CDCH (EBI Clearinghouse). |

| Command (function)  | Description                                                                                                                                                     |
|---------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------|
| tsv2json / json2tsv | [TSV](https://gitlab.com/uit-sfb/sarscovid19db/-/wikis/Data/TSV-format) - [JSON](https://gitlab.com/uit-sfb/sarscovid19db/-/wikis/Data/JSON-format) convertion. |
| validate            | Data validation.                                                                                                                                                |
| cdch                | Export to CDCH (no database access).                                                                                                                            |


## Requirements

- Java 11 or later