<?php
/**
 * Plugin Name: CBF
 * Plugin URI: https://gitlab.com/uit-sfb/cbf
 * Description: Add shortcodes to display CBF widgets as well as setting up an API route.
 * Version: 1.0
 **/

include(plugin_dir_path(__FILE__) . 'settings.php');
include(plugin_dir_path(__FILE__) . 'pages.php'); //Needs to be BEFORE routing.php
include(plugin_dir_path(__FILE__) . 'routing.php');
include(plugin_dir_path(__FILE__) . 'shortcodes.php');


//Use plugin_dir_url(__FILE__) to serve plugin files
function nqs_add_scripts() {
    global $wpdb;
    $arr = $wpdb->get_results('SELECT ID FROM ' . $wpdb->prefix . 'posts WHERE post_content LIKE "%[Cbf%]%" AND post_parent = 0');
    $fn = function($n) { return $n->ID; };
    $id_list = array_map($fn, $arr);
    $page_id = get_the_ID();
    $cbf_options = get_option('cbf_options'); // Array of All Options
    $resourceUrl = $cbf_options['cbf_assets_endpoint'];
    wp_register_style('cbf_stylesheet', "$resourceUrl/stylesheets/main.css");
    wp_register_style('cbf_stylesheet_react-vis', 'https://unpkg.com/react-vis/dist/style.css');
    wp_register_style('cbf_stylesheet_leaflet', 'https://unpkg.com/leaflet@1.7.1/dist/leaflet.css');
    wp_register_style('cbf_stylesheet_bootstrap', 'https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css');
    if (in_array($page_id, $id_list)) {
        wp_enqueue_style('cbf_stylesheet');
        wp_enqueue_style('cbf_stylesheet_bootstrap');
        wp_enqueue_style('cbf_stylesheet_react-vis');
        wp_enqueue_style('cbf_stylesheet_leaflet');
        wp_enqueue_script('cbf_bundle', "$resourceUrl/datawidgets-fastopt-bundle.js");
    }
}

add_action( 'wp_enqueue_scripts', 'nqs_add_scripts' );