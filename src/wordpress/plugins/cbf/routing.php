<?php

/**
 * Register a rewrite rule.
 * For example /MarRef/records is converted to /index.php?page_id=<page_id>&dsname=MarRef
 */
function cbf_rewrite_rule_db($type)
{
    $page_id = get_option("cbf_page_$type");
    add_rewrite_rule(
        "^([-_\w]+)/$type/?",
        "index.php?page_id=$page_id&dsname=\$matches[1]",
        'top'
    );
}

/**
 * Register a rewrite rule.
 * For example /MarRef/records/MMP7339714 is converted to /index.php?page_id=<page_id>&dsname=MarRef&id=MMP7339714
 */
function cbf_rewrite_rule_db_with_id($type)
{
    $page_id = get_option("cbf_page_$type");
    // api/ is legacy routing
    add_rewrite_rule(
        "^(?:api/)?([-_\w]+)/$type/(?:([-_.\w]+))/?",
        "index.php?page_id=$page_id&dsname=\$matches[1]&id=\$matches[2]",
        'top'
    );
}

function cbf_rewrite_rules() {
    cbf_rewrite_rule_db("browser");
    cbf_rewrite_rule_db("settings");
    cbf_rewrite_rule_db_with_id("records");
}

add_action('init', 'cbf_rewrite_rules');

/**
 * Register the all query parameters here (including the ones added via the rewrite rules)
 */
function cbf_query_vars($query_vars)
{
    $query_vars[] = 'id';
    $query_vars[] = 'dsname';
    $query_vars[] = 'ver';
    $query_vars[] = 'target-id';
    $query_vars[] = 'target-ds';
    return $query_vars;
}

add_filter('query_vars', 'cbf_query_vars');
