<?php

function cbf_create_custom_page($type, $title, $content) {
    $opt_name = "cbf_page_$type";
    $slug = "cbf-$type";
    //If the option exists but not the page, we reset the option
    if (get_option($opt_name) && !get_post_status(get_option($opt_name))) {
        delete_option($opt_name);
    }
    $page_spec = array(
        'ID' => get_option($opt_name), //If empty, a new page is created, if not, the page is updated
        //'menu_order' => [ <order> ] //If new post is a page, sets the order should it appear in the tabs.
        //'page_template' => [ <template file> ] //Sets the template for the page.
        'comment_status' => "closed", //[ 'closed' | 'open' ] // 'closed' means no comments.
        //'ping_status' => [ ? ] //Ping status?
        //'pinged' => [ ? ] //?
        //'post_author' => [ <user ID> ] //The user ID number of the author.
        //'post_category' => [ array(<category id>, <...>) ] //Add some categories.
        'post_content' => $content, //The full text of the post.
        //'post_date' => [ Y-m-d H:i:s ] //The time post was made.
        //'post_date_gmt' => [ Y-m-d H:i:s ] //The time post was made, in GMT.
        //'post_excerpt' => [ <an excerpt> ] //For all your post excerpt needs.
        'post_name' => $slug, // The name (slug) for your post
        //'post_parent' => [ <post ID> ] //Sets the parent of the new post.
        //'post_password' => [ ? ] //password for post?
        'post_status' => "publish",//[ 'draft' | 'publish' | 'pending' ] //Set the status of the new post.
        'post_title' => $title, //The title of your post.
        'post_type' => "page",//[ 'post' | 'page' ] //Sometimes you want to post a page.
        //'tags_input' => [ '<tag>, <tag>, <...>' ] //For tags.
        //'to_ping' => [ ? ] //?
    );

    // Insert the post into the database
    $page_id = wp_insert_post($page_spec);
    add_option($opt_name, $page_id); //If the option exists already, no action
    // We test if the option is set. If not, there is most likely an issue with permissions and the option cannot be set and we delete the page in order to avoid ending up with hundreds of them.
    if (!get_option($opt_name)) {
        wp_delete_post($page_id, true);
    }
}

function cbf_create_custom_pages() {
    cbf_create_custom_page("browser", "", "[CbfBrowser]");
    cbf_create_custom_page("records", "", "[CbfRecord]");
    cbf_create_custom_page("settings", "", "[CbfSettings]");
}

add_action('init', 'cbf_create_custom_pages');
