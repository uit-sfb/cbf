<?php


function CbfBrowser($atts = [], $content = null, $tag = '')
{
    global $wp;
    $cbf_options = get_option('cbf_options'); // Array of All Options
    $endpoint = $cbf_options['cbf_api_endpoint'];
    $resourceUrl = $cbf_options['cbf_assets_endpoint'];
    $user_id = get_current_user_id();
    $access_token = get_user_meta($user_id, 'openid-connect-generic-last-token-response', true)["access_token"];
    $openid_settings = get_option('openid_connect_generic_settings', array());
    $openid_end_session = $openid_settings['endpoint_end_session'];
    $openid_callback_url = do_shortcode('[openid_connect_generic_auth_url]');
    // normalize attribute keys, lowercase
    $atts = array_change_key_case((array)$atts, CASE_LOWER);
    $single_record_atts = shortcode_atts(
        array(
            'dsname' => (empty($wp->query_vars['dsname'])) ? '' : $wp->query_vars['dsname'],
            'target-id' => (empty($wp->query_vars['target-id'])) ? '' : $wp->query_vars['target-id'],
            'target-ds' => (empty($wp->query_vars['target-ds'])) ? '' : $wp->query_vars['target-ds'],
            'prefilter' => '',
        ), $atts, $tag
    );
    $dsName = $single_record_atts['dsname'];
    $prefilter = urldecode($single_record_atts['prefilter']);
    $targetId = urldecode($single_record_atts['target-id']);
    $targetDs = urldecode($single_record_atts['target-ds']);
    $entrypoint = bin2hex(random_bytes(10));
    return <<<EOD
    <div id="$entrypoint"></div>
    <script>
        BrowserWidget.renderInto("$endpoint", "$access_token", "$openid_callback_url", "$openid_end_session", "$resourceUrl", "$dsName", '$prefilter', "$targetId", "$targetDs", "$entrypoint")
    </script>
EOD;
}

add_shortcode('CbfBrowser', 'CbfBrowser');

function CbfRecord($atts = [], $content = null, $tag = '')
{
    global $wp;
    $cbf_options = get_option('cbf_options'); // Array of All Options
    $endpoint = $cbf_options['cbf_api_endpoint'];
    $resourceUrl = $cbf_options['cbf_assets_endpoint'];
    $user_id = get_current_user_id();
    $access_token = get_user_meta($user_id, 'openid-connect-generic-last-token-response', true)["access_token"];
    $openid_settings = get_option('openid_connect_generic_settings', array());
    $openid_end_session = $openid_settings['endpoint_end_session'];
    $openid_callback_url = do_shortcode('[openid_connect_generic_auth_url]');
    // normalize attribute keys, lowercase
    $atts = array_change_key_case((array)$atts, CASE_LOWER);
    //Define all supported attributes for this shortcode as well as defaults
    //We need to use SARS-CoV-2 as default db because of registration in identifiers.org
    $single_record_atts = shortcode_atts(
        array(
            'dsname' => (empty($wp->query_vars['dsname']) or $wp->query_vars['dsname'] == "api") ? "SARS-CoV-2" : $wp->query_vars['dsname'],
            'ver' => (empty($wp->query_vars['ver'])) ? "" : $wp->query_vars['ver'],
            'id' => (empty($wp->query_vars['id'])) ? "" : $wp->query_vars['id'],
        ), $atts, $tag
    );
    $dsName = $single_record_atts['dsname'];
    $ver = $single_record_atts['ver'];
    $id = $single_record_atts['id'];
    $entrypoint = bin2hex(random_bytes(10));
    return <<<EOD
  <div id="$entrypoint"></div>
    <script>
        RecordWidget.renderInto("$endpoint", "$access_token", "$openid_callback_url", "$openid_end_session", "$resourceUrl", "$dsName", "$ver", "$id", "$entrypoint");
        new JsonldScript("$endpoint", "$access_token", "$dsName", "$ver", "$id").add;
    </script>
EOD;
}

add_shortcode('CbfRecord', 'CbfRecord');

function CbfSettings($atts = [], $content = null, $tag = '')
{
    global $wp;
    $cbf_options = get_option('cbf_options'); // Array of All Options
    $endpoint = $cbf_options['cbf_api_endpoint'];
    $resourceUrl = $cbf_options['cbf_assets_endpoint'];
    $user_id = get_current_user_id();
    $access_token = get_user_meta($user_id, 'openid-connect-generic-last-token-response', true)["access_token"];
    $openid_settings = get_option('openid_connect_generic_settings', array());
    $openid_end_session = $openid_settings['endpoint_end_session'];
    $openid_callback_url = do_shortcode('[openid_connect_generic_auth_url]');
    // normalize attribute keys, lowercase
    $atts = array_change_key_case((array)$atts, CASE_LOWER);
    //Define all supported attributes for this shortcode as well as defaults
    $single_record_atts = shortcode_atts(
        array(
            'dsname' => (empty($wp->query_vars['dsname'])) ? '' : $wp->query_vars['dsname']
        ), $atts, $tag
    );
    $dsName = $single_record_atts['dsname'];
    $entrypoint = bin2hex(random_bytes(10));
    return <<<EOD
  <div id="$entrypoint"></div>
    <script>
        SettingsWidget.renderInto("$endpoint", "$access_token", "$openid_callback_url", "$openid_end_session", "$resourceUrl", "$dsName", "$entrypoint");
    </script>
EOD;
}

add_shortcode('CbfSettings', 'CbfSettings');

function CbfGraph($atts = [], $content = null, $tag = '')
{
    $cbf_options = get_option('cbf_options'); // Array of All Options
    $endpoint = $cbf_options['cbf_api_endpoint'];
    $resourceUrl = $cbf_options['cbf_assets_endpoint'];
    $user_id = get_current_user_id();
    $access_token = get_user_meta($user_id, 'openid-connect-generic-last-token-response', true)["access_token"];
    $openid_settings = get_option('openid_connect_generic_settings', array());
    $openid_end_session = $openid_settings['endpoint_end_session'];
    $openid_callback_url = do_shortcode('[openid_connect_generic_auth_url]');
    // normalize attribute keys, lowercase
    $atts = array_change_key_case((array)$atts, CASE_LOWER);
    $single_record_atts = shortcode_atts(
        array(
            'dsname' => '',
            'ver' => '',
            'search' => '',
            'x' => '_time:implementation_date',
            'y' => '>countC',
            'pres' => 'linear',
            'height' => 600
        ), $atts, $tag
    );
    $dsName = $single_record_atts['dsname'];
    $ver = $single_record_atts['ver'];
    $search = $single_record_atts['search'];
    $x = $single_record_atts['x'];
    $y = $single_record_atts['y'];
    $pres = $single_record_atts['pres'];
    $height = $single_record_atts['height'];
    $entrypoint = bin2hex(random_bytes(10));
    return <<<EOD
  <div id="$entrypoint"></div>
    <script>
    GraphWidget.renderInto("$endpoint", "$access_token", "$openid_callback_url", "$openid_end_session", "$resourceUrl", "$dsName", "$ver", "$search", "$x", "$y", "$pres", $height, "$entrypoint");
    </script>
EOD;
}

add_shortcode('CbfGraph', 'CbfGraph');

function CbfMap($atts = [], $content = null, $tag = '')
{
    $cbf_options = get_option('cbf_options'); // Array of All Options
    $endpoint = $cbf_options['cbf_api_endpoint'];
    $resourceUrl = $cbf_options['cbf_assets_endpoint'];
    $user_id = get_current_user_id();
    $access_token = get_user_meta($user_id, 'openid-connect-generic-last-token-response', true)["access_token"];
    $openid_settings = get_option('openid_connect_generic_settings', array());
    $openid_end_session = $openid_settings['endpoint_end_session'];
    $openid_callback_url = do_shortcode('[openid_connect_generic_auth_url]');
    // normalize attribute keys, lowercase
    $atts = array_change_key_case((array)$atts, CASE_LOWER);
    $single_record_atts = shortcode_atts(
        array(
            'dsname' => '',
            'ver' => '',
            'search' => '',
            'attr' => '',
            'center' => '',
            'zoom' => 2,
            'height' => 600
        ), $atts, $tag
    );
    $dsName = $single_record_atts['dsname'];
    $ver = $single_record_atts['ver'];
    $search = $single_record_atts['search'];
    $attr = $single_record_atts['attr'];
    $center = $single_record_atts['center'];
    $zoom = $single_record_atts['zoom'];
    $height = $single_record_atts['height'];
    $entrypoint = bin2hex(random_bytes(10));
    return <<<EOD
  <div id="$entrypoint"></div>
    <script>
      MapWidget.renderInto("$endpoint", "$access_token", "$openid_callback_url", "$openid_end_session", "$resourceUrl", "$dsName", "$ver", "$search", "$attr", "$center", $zoom, $height, "$entrypoint");
    </script>
EOD;
}

add_shortcode('CbfMap', 'CbfMap');

function CbfGeo($atts = [], $content = null, $tag = '')
{
    $cbf_options = get_option('cbf_options'); // Array of All Options
    $endpoint = $cbf_options['cbf_api_endpoint'];
    $resourceUrl = $cbf_options['cbf_assets_endpoint'];
    $user_id = get_current_user_id();
    $access_token = get_user_meta($user_id, 'openid-connect-generic-last-token-response', true)["access_token"];
    $openid_settings = get_option('openid_connect_generic_settings', array());
    $openid_end_session = $openid_settings['endpoint_end_session'];
    $openid_callback_url = do_shortcode('[openid_connect_generic_auth_url]');
    // normalize attribute keys, lowercase
    $atts = array_change_key_case((array)$atts, CASE_LOWER);
    $single_record_atts = shortcode_atts(
        array(
            'dsname' => '',
            'ver' => '',
            'search' => '',
            'x' => '',
            'y' => '>count',
            'height' => 600
        ), $atts, $tag
    );
    $dsName = $single_record_atts['dsname'];
    $ver = $single_record_atts['ver'];
    $search = $single_record_atts['search'];
    $x = $single_record_atts['x'];
    $y = $single_record_atts['y'];
    $height = $single_record_atts['height'];
    $entrypoint = bin2hex(random_bytes(10));
    return <<<EOD
    <div id="$entrypoint" height="${height}px"></div>
    <script>
      GeoWidget.renderInto("$endpoint", "$access_token", "$openid_callback_url", "$openid_end_session", "$resourceUrl", "$dsName", "$ver", "$search", "$x", "$y", "$entrypoint");
    </script>
EOD;
}

add_shortcode('CbfGeo', 'CbfGeo');