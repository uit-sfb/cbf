# Data Widget

Add shortcodes to display CBF widgets as well as setting up an API route.

## Getting started

- Install and activate the plugin (see [here](../README.md)).
- Save the permalinks: Go to Settings -> Permalinks and just save the page without making any change.
- Set up the plugin: Go to CBF and fill up the api endpoint
- Add the shortcodes described below where appropriate.

## Shortcodes

### [CbfBrowser]

Display the record browser widget.

Note: A page containing this shortcode is created automatically and is available at:
- `/cbf-browser?dsname=<dsname>`
- `/<dsname>/browser`

#### Attributes

- `dsname`: Name of the dataset (may be provided as query parameter)
- `prefilter`: A filter to be applied to all requests (AND'ed). May be:
  - `'text'`: Text search 
  - `{json doc}`: [native Mongodb query document](https://docs.mongodb.com/manual/tutorial/query-documents/)
- `target-ds`: Open create view with a link to this dataset (record id `target-id`)
- `target-id`: Open create view with a link to this record in dataset `target-ds`

Examples:
- `[CbfBrowser dsname=SARS-CoV-2 prefilter=finland]`
- `[CbfBrowser dsname=SARS-CoV-2 target-id=12345 target-ds=secondary-database]`

When using a json document, it needs to be urlencoded.
For instance `{"attrs.loc:country.obj.iri": {"$in": ["gaz:00002699", "GAZ:00002699"]}}` is encoded in `%7B%22attrs.loc%3Acountry.obj.iri%22%3A%20%7B%22%24in%22%3A%20%5B%22gaz%3A00002699%22%2C%20%22GAZ%3A00002699%22%5D%7D%7D`
Example: `[CbfBrowser dsname=SARS-CoV-2 prefilter='%7B%22attrs.loc%3Acountry.obj.iri%22%3A%20%7B%22%24in%22%3A%20%5B%22gaz%3A00002699%22%2C%20%22GAZ%3A00002699%22%5D%7D%7D']`

### [CbfSettings]

Display the settings widget.

Note: A page containing this shortcode is created automatically and is available at:
- `/cbf-settings?dsname=<dsname>`
- `/<dsname>/settings`

#### Attributes

- `dsname`: Name of the dataset (may be provided as query parameter)

Example: `[CbfSettings dsname=SARS-CoV-2]`

### [CbfRecord]

Display a single record.

Note: A page containing this shortcode is created automatically and is available at:
- `/cbf-records?dsname=<dsname>&id=<record-id>`
- `/<dsname>/records/<record-id>`
- `/api/<dsname>/records/<record-id>` (legacy)

Note: because SARS-CoV-2 was registered to identifiers.org without including the dbName, `SARS-CoV-2` is used by default
if the route does not contain any `<dsname>`.

#### Attributes

Alternatively, any of the following attributes may be provided in the shortcode:

- `dsname`: name of the dataset (may be provided as query parameter)
- `ver`: version to query. When not provided, query the latest available version of this record. (may be provided as query parameter)
- `id`: record id (may be provided as query parameter)

Example: `[CbfRecord dsname=SARS-CoV-2 ver="2.5" id="SFB_COVID19_MW644516"]`

### [CbfGraph]

Display a graph.

#### Attributes

- `dsname`: Name of the dataset
- `ver`: version (default: latest)
- `search`: searchQuery
- `x`: attribute reference for the X axis
- `y`: attribute reference for the Y axis
- `pres`: presentation option (`linear` (default), `percent`, or `log`)
- `height`: height of the widget in pixel (default: 600)

Example: `[CbfGraph dbName="SARS-CoV-2"  ver="2.5" search="" x="_time:implementation_date" y=">countC" pres=linear height=800]`

### [CbfMap]

Display a map.

#### Attributes

- `dsname`: Name of the dataset
- `ver`: version (default: latest)
- `search`: searchQuery
- `attr`: attribute of type latlon to use
- `center`: coordinates of the center of the map (default: `51.505,-0.09`)
- `zoom`: zoom level (default: 2)
- `height`: height of the widget in pixel (default: 600)

Example: `[CbfMap dbName="SARS-CoV-2"  ver="2.5" search="" attr="sample:loc" center="1.4,0.3" zoom=4 height=800]`

### [CbfGeo]

Display a geography.

#### Attributes

- `dsname`: Name of the dataset
- `ver`: version (default: latest)
- `search`: searchQuery
- `x`: X axis (default: any attribute of type under `cat:geo`)
- `y`: Y axis (default: `>count`)

Example: `[CbfGeo dbName="SARS-CoV-2"  ver="2.5" search="" y="host:age" height=800]`
