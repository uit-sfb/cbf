# Wordpress plugins

## How to load a plugin to Wordpress?

- Download the
  plugin: `curl https://gitlab.com/api/v4/projects/21772258/packages/generic/cbf_wordpress/<version>/cbf_wordpress.zip --output cbf_wordpress.zip`
  , where `<version>` is one of the versions listed [here](https://gitlab.com/uit-sfb/cbf/-/releases)
- Log in to Wordpress and go to `Plugins` -> `Add New` -> `Upload Plugin` -> `Install Now`
- Select `cbf_wordpress.zip`
- Click on `Activate`

## Troubleshooting

If the URL are not resolved properly, it might be that the rules defined by the plugin were not picked up by Wordpress.
To fix this, go to `Settings` -> `Permalinks` then click on `Save` without doing any modifications.