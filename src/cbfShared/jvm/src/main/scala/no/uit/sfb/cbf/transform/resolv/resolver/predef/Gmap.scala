package no.uit.sfb.cbf.transform.resolv.resolver.predef

import no.uit.sfb.cbf.shared.config.CustomResolver
import no.uit.sfb.cbf.transform.resolv.resolver.StaticResolverLike

trait Gmap extends StaticResolverLike {
      lazy val ct = CustomResolver(
        "gmap",
        "^(?:-?\\d+(?:\\.\\d+)?),(?:-?\\d+(?:\\.\\d+)?)$",
        Map("_iri" -> "http://maps.google.com/maps?q={id}")
      )
}
