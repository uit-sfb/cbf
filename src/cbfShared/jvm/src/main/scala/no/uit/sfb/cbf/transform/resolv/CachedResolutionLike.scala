package no.uit.sfb.cbf.transform.resolv

import com.github.blemale.scaffeine.{AsyncLoadingCache, Scaffeine}
import com.typesafe.scalalogging.LazyLogging
import no.uit.sfb.cbf.shared.model.v1.nativ.CompactIRI

import scala.concurrent.duration._
import scala.concurrent.Future
import scala.util.{Failure, Success, Try}

trait CachedResolutionLike extends ResolutionLike with LazyLogging {

  /**
    *
    * @param ciri
    * @return
    */
  final protected override def fetchOption(
    ciri: CompactIRI,
    dbName: String = "{dsName}",
    version: String = "{ver}",
    apiHost: String = "{api}"
  ): Future[Option[Map[String, String]]] = {
    resolver(ciri) match {
      case Some(resolver) =>
        if (resolver.cacheable)
          cache.get(ciri).map { _.get } else
          fetch(ciri, dbName, version, apiHost)
      case None =>
        Future.successful(None)
    }
  }

  //Note: No extra parameter here since caching does not allow flexibility
  protected def getData(ciri: CompactIRI): Future[Option[Map[String, String]]] =
    fetch(ciri)

  private lazy val cache
    : AsyncLoadingCache[CompactIRI, Try[Option[Map[String, String]]]] =
    Scaffeine()
      .expireAfterAccess(1.hour)
      .maximumSize(1000)
      .buildAsyncFuture { ciri =>
        getData(ciri).transform {
          case Success(v) => Success(Success(v))
          case Failure(e) => Success(Failure(e))
        }
      }

  def invalidateCache(ciri: CompactIRI): Future[Unit] = {
    Future(resolver(ciri) foreach { resolver =>
      if (resolver.cacheable) {
        logger.info(s"Invalidating cache for ciri ${ciri.asString}.")
        cache.synchronous().invalidate(ciri)
      }
    })
  }
}
