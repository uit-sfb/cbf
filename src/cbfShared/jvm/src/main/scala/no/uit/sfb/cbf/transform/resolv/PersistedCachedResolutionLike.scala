package no.uit.sfb.cbf.transform.resolv

import no.uit.sfb.cbf.mongodb.SharedDatabase
import no.uit.sfb.cbf.shared.model.v1.nativ.CompactIRI
import org.mongodb.scala.model.Filters

import scala.concurrent.{Await, Future, blocking}
import scala.concurrent.duration._

trait PersistedCachedResolutionLike extends CachedResolutionLike {
  protected def db: SharedDatabase

  override protected def getData(
    ciri: CompactIRI
  ): Future[Option[Map[String, String]]] = {
    val tmp = resolver(ciri) match {
      case Some(resolver) =>
        if (resolver.persistable) {
          db.ciris.flatMap { cirisCol =>
            cirisCol.getOne(Filters.eq("_id", ciri.asString)) flatMap {
              case Some(meta) =>
                Future.successful(Some(meta))
              case None =>
                //We are persisted, so we have no room for flexibility
                val fMeta = fetch(ciri)
                fMeta map {
                  _.map { doc => //We only push valid CIRI
                    cirisCol.insert(doc)
                  }
                }
                fMeta //No need to wait for the push to be complete
            }
          }
        } else {
          //We are persisted, so we have no room for flexibility
          fetch(ciri)
        }
      case None =>
        Future.successful(None)
    }
    //We add val in case it is missing
    tmp.map {
      _.map { m =>
        m.get("val") match {
          case Some(_) =>
            m
          case None =>
            val v = m("_key")
            m + ("val" -> v)
        }
      }
    }
  }

  override def invalidateCache(ciri: CompactIRI): Future[Unit] = {
    Future(blocking(resolver(ciri).collect {
      case resolver if resolver.persistable =>
        val f = db.ciris.flatMap { col =>
          col.delete(Filters.eq("_id", ciri.asString), true)
        }
        Await.ready(f, 15.seconds)
    })).flatMap { _ =>
      super.invalidateCache(ciri)
    }
  }
}
