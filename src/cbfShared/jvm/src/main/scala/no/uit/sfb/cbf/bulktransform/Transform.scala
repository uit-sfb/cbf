package no.uit.sfb.cbf.bulktransform

import com.typesafe.scalalogging.LazyLogging
import no.uit.sfb.cbf.mongodb.Collection
import no.uit.sfb.cbf.shared.model.v1.builder.RecordBuilder
import no.uit.sfb.cbf.shared.model.v1.nativ.{Record, Validity}
import no.uit.sfb.cbf.shared.version.CbfVersion
import org.mongodb.scala.bson.{Document, ObjectId}
import org.mongodb.scala.model._

import scala.concurrent.duration._
import scala.concurrent.{Await, ExecutionContext, Future}
import scala.jdk.CollectionConverters._
import scala.collection.parallel.ParIterable

object Transform extends LazyLogging {
  //Note: Creation/update/deletion of a same record multiple times in one push is not supported.
  //May it happen, the behavior is undefined (meaning one of the action will be performed last but we don't know which one)
  def pushRecords(version: CbfVersion,
                  col: Collection[Document],
                  rb: RecordBuilder,
                  releasedVersions: Seq[String])(
    batch: ParIterable[Record]
  )(implicit ec: ExecutionContext): Future[Seq[ObjectId]] = {
    val alreadyReleased = releasedVersions.contains(version.ver)
    val release = version.release
    val models: Seq[WriteModel[_ <: Document]] = batch
      .flatMap { rec =>
        val id = rec.id.get

        //Current release filter (may return empty or one item)
        val filterCurrent =
          Filters.and(Filters.eq("id", id), Filters.eq("valid.start", release))
        //Still active anteriors (may return empty or one item)
        val filterAnterior = Filters.and(
          Filters.eq("id", id),
          Filters.lt("valid.start", release),
          Filters.or(
            Filters.exists("valid.end", false),
            Filters.gte("valid.end", release)
          )
        )

        def updateStart(doc: Document,
                        start: Option[Int]): WriteModel[Nothing] = {
          val end = doc.getEmbedded(Seq("valid", "end").asJava, -1)
          start match {
            case Some(s) if end < 0 || s < end =>
              UpdateOneModel(
                Filters.eq("_id", doc.getObjectId("_id")),
                Updates.set("valid.start", s)
              )
            case _ =>
              DeleteOneModel(Filters.eq("_id", doc.getObjectId("_id")))
          }
        }

        def updateEnd(doc: Document, end: Option[Int]): WriteModel[Nothing] = {
          val start = doc.getEmbedded(Seq("valid", "start").asJava, -1)
          end match {
            case Some(e) if start < e =>
              UpdateOneModel(
                Filters.eq("_id", doc.getObjectId("_id")),
                Updates.set("valid.end", e)
              )
            case Some(_) =>
              DeleteOneModel(Filters.eq("_id", doc.getObjectId("_id")))
            case None =>
              UpdateOneModel(
                Filters.eq("_id", doc.getObjectId("_id")),
                Updates.unset("valid.end")
              )
          }
        }

        def fromCurrentAndAnterior(
          oCurrent: Option[Document],
          oAnterior: Option[Document]
        ): Seq[WriteModel[_ <: Document]] = {
          lazy val newEnd =
            if (alreadyReleased)
              Some(release + 1)
            else
              oCurrent.flatMap { c =>
                val e =
                  c.getEmbedded(Seq("valid", "end").asJava, -1)
                if (e >= 0)
                  Some(e)
                else None
              }
          if (rec.toBeDeleted) {
            Seq(oCurrent.map { _ =>
              DeleteManyModel(filterCurrent)
            }, oAnterior.map { a =>
              updateEnd(a, Some(release))
            }).flatten
          } else {
            (oCurrent, oAnterior) match {
              case (Some(c), _)
                  if rec.equivalent(rb.fromSingleJson(c.toJson())) =>
                oAnterior match {
                  case Some(a)
                      if rec.equivalent(rb.fromSingleJson(a.toJson())) => //Should not happen since we have a current already but we keep this case for robustness
                    val end = {
                      val e = c.getEmbedded[Int](Seq("valid", "end").asJava, -1)
                      if (e < 0)
                        None
                      else
                        Some(e)
                    }
                    Seq(updateEnd(a, end), DeleteOneModel(Filters.eq("_id", c)))
                  case Some(a) => //Should not happen since we have a current already but we keep this case for robustness
                    Seq(updateEnd(a, Some(release)))
                  case None =>
                    Seq()
                }
              case (_, Some(a))
                  if rec.equivalent(rb.fromSingleJson(a.toJson())) =>
                oCurrent match {
                  case Some(c) =>
                    Seq(updateStart(c, newEnd), updateEnd(a, newEnd))
                  case None =>
                    val e = a.getEmbedded[Int](Seq("valid", "end").asJava, -1)
                    if (e < 0)
                      Seq()
                    else
                      Seq(updateEnd(a, Some(Seq(e, release + 1).max)))
                }
              case _ =>
                val recWithValidity = {
                  rec.copy(valid = Validity(Some(release), newEnd))
                }
                Seq(
                  oAnterior.map { a =>
                    updateEnd(a, Some(release))
                  },
                  oCurrent.map { c =>
                    updateStart(c, newEnd)
                  },
                  Some(
                    ReplaceOneModel( //We use Replace instead of insert because if we have duplicates, it might happen that we want to replace
                      filterCurrent,
                      Document(recWithValidity.serialize()),
                      ReplaceOptions().upsert(true)
                    )
                  )
                ).flatten
            }
          }
        }

        //Should not be needed, but we do it anyways for robustness
        def removeDuplicates(
          s: Seq[Document]
        ): (Seq[WriteModel[Nothing]], Option[Document]) = {
          //We need to handle the case where s is empty explicitly because init fails on empty coll
          if (s.isEmpty)
            Seq() -> None
          else
            s.init.map { doc =>
              DeleteOneModel(Filters.eq("_id", doc.getObjectId("_id")))
            } -> Some(s.last)
        }

        val f = (col.get(filterCurrent) zip
          col.get(filterAnterior))
          .map {
            case (currents, anteriors) =>
              val (seq1, oCurrent) = removeDuplicates(currents)
              val (seq2, oAnterior) = removeDuplicates(anteriors)
              seq1 ++ seq2 ++ fromCurrentAndAnterior(oCurrent, oAnterior)
          }
        Await.result(f, 1.minute)
      }
      .seq
      .toSeq
    //We NEED to keep the ordering because of the ReplaceOneModel which MUST come after updateStart(c)
    col.bulkWrite(models, true).map { resp =>
      resp.getInserts.asScala.toSeq
        .map { _.getId.asObjectId().getValue } ++ resp.getUpserts.asScala.toSeq
        .map { _.getId.asObjectId().getValue }
    }
  }

  //The point of touching is to make sure all the versions are in sync with the current data model and that new CIRIs kinds are used in older data.
  //No metadata is changed (especially timestamps and valid objects are not altered)
  def touchRecords(
    col: Collection[Document],
    recoverRelease: Option[Int] = None
  )(batch: ParIterable[Record])(implicit ec: ExecutionContext): Future[Unit] = {
    val models: Seq[WriteModel[_ <: Document]] = batch
      .map { rec =>
        val id = rec.id.get
        val filter = Filters.and(
          Filters.eq("id", id),
          Filters.eq("valid.start", rec.valid.start.get)
        )
        val record = recoverRelease match {
          case Some(rel) if rec.valid.end.contains(rel) =>
            rec.copy(valid = rec.valid.copy(end = None))
          case _ =>
            rec
        }
        //Note: using DeleteMany followed by InsertOne in order to clear any duplicate will lead to an infinite loop!
        //If needed this should be implemented by skipping the delete part and instead insert to a virgin temp collection, then
        //replace the official collection with temp.
        //ReplaceOne will work as long as the couple (id, valid.start) are unique (which should normally always be the case)
        ReplaceOneModel(filter, Document(record.serialize()))
      }
      .seq
      .toSeq
    col.bulkWrite(models).map { _ =>
      ()
    }
  }
}
