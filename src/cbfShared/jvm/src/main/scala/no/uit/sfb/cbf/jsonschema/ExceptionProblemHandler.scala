package no.uit.sfb.cbf.jsonschema

import com.typesafe.scalalogging.LazyLogging
import org.leadpony.justify.api.Problem

case class ExceptionProblemHandler(ctx: String)
    extends ProblemHandlerLike
    with LazyLogging {
  import scala.jdk.CollectionConverters._
  protected def printBranch(pbs: Seq[Problem], idx: Int): String = {
    if (pbs.size > 1)
      (s"  $idx)" +: pbs.map { _.getMessage }).mkString("\n    - ")
    else
      (s"  $idx)" +: pbs.map { _.getMessage }).mkString(" ")
  }
  protected def print(pb: Problem): String = {
    val ptr = Option(pb.getPointer) match {
      case Some(pointer) =>
        val reg = "\\/attrs\\/(?:\\d+)\\/([^\\/]+)\\/obj/(?:\\d+)/val".r
        pointer match {
          case reg(attr) =>
            attr
          case _ =>
            s"[$pointer]"
        }
      case None =>
        ""
    }
    val branches = for (i <- 0 until pb.countBranches()) yield {
      printBranch(pb.getBranch(i).asScala.toSeq, i)
    }
    (Seq(ptr, pb.getMessage).mkString(" > ") +: branches).mkString("\n")
  }

  override def handle(problems: Seq[Problem]): Unit =
    if (problems.nonEmpty) {
      throw new Exception(problems.map { print }.mkString("\n"))
    } else
      logger.info(s"'$ctx' passed validation")
}
