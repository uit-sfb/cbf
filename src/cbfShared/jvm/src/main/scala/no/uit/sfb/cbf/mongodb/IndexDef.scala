package no.uit.sfb.cbf.mongodb

case class IndexDef(keyPath: String,
                    idx: Collection.IndexLike,
                    unique: Boolean = false)
