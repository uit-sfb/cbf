package no.uit.sfb.cbf.utils

import java.nio.file.{Path, Paths}
import no.uit.sfb.scalautils.common.FileUtils

import scala.io.Source
import scala.util.Using

object GetResource {
  def apply(path: String): Path = {
    val outPath = Paths.get(s"/tmp/${path.replace("/", "_")}")
    Using(Source.fromResource(path)){
      source =>
        FileUtils.writeToFile(outPath, source.mkString)
    }
    outPath
  }
}
