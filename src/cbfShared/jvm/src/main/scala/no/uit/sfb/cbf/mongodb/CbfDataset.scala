package no.uit.sfb.cbf.mongodb

import com.github.blemale.scaffeine.{LoadingCache, Scaffeine}
import com.mongodb.client.model.Filters
import com.typesafe.scalalogging.LazyLogging
import io.circe.generic.extras.Configuration
import io.circe.generic.extras.auto._
import org.mongodb.scala.MongoDatabase
import org.mongodb.scala.bson.Document
import io.circe.parser.decode
import io.circe.syntax._
import no.uit.sfb.cbf.shared.auth.UserInfo
import no.uit.sfb.cbf.shared.config.{AttributeDef, DatasetConfig}
import no.uit.sfb.cbf.shared.model.v1.builder.RecordBuilder
import no.uit.sfb.cbf.shared.model.v1.versioninfo.VersionInfo
import no.uit.sfb.cbf.shared.utils.Date
import no.uit.sfb.cbf.shared.version.CbfVersion
import org.bson.codecs.configuration.CodecRegistries.{fromProviders, fromRegistries}
import org.mongodb.scala.bson.codecs.Macros._
import org.mongodb.scala.MongoClient.DEFAULT_CODEC_REGISTRY

import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.concurrent.atomic.AtomicInteger
import scala.concurrent.duration._
import scala.concurrent.{Await, ExecutionContext, Future}

//Representation of a SFB database
class CbfDataset(protected val _db: MongoDatabase)(
  implicit protected val _ec: ExecutionContext
) extends Database(_db)(_ec)
    with LazyLogging {
  implicit val customConfig =
    Configuration.default.withDefaults
  protected val codecRegistry =
    fromRegistries(fromProviders(classOf[UserInfo]), DEFAULT_CODEC_REGISTRY)

  def parseVersion(rawVersion: String): CbfVersion = CbfVersion(rawVersion)

  private lazy val compulsoryIndexes =
    Set("valid.start", "valid.end", "id", "attrs.id.obj.num", "$**")
  private lazy val maxNbCacheIndexes = 64 - (compulsoryIndexes.size + 1) //64 limit from mongodb, +1 for the default index _id

  private lazy val recordsColCache: LoadingCache[String, Collection[Document]] =
    Scaffeine()
      .maximumSize(100)
      .expireAfterWrite(7.days)
      .build { dbName =>
        val col = new Collection[Document](db.getCollection(dbName))
        col
          .listIndexes()
          .map { docs =>
            docs
              .map { d =>
                //This only works for simple indexes
                d.getString("name")
              }
              .filterNot { x =>
                compulsoryIndexes.exists(y => x.startsWith(y + "_"))
              }
          }
          .flatMap { presentIndexes =>
            //We add the indexes to the index cache
            val mutable = keyCache.asMap() //Mutable reference: is kept in sync with the cache
            presentIndexes.foreach { k =>
              mutable.updateWith(k)(_ => Some(Date.now().toString))
            }
            //We make space for the compulsory indexes
            val toDrop = presentIndexes.size - maxNbCacheIndexes
            val f = if (toDrop > 0) {
              val keysToDrop = presentIndexes.takeRight(toDrop)
              keysToDrop.map { mutable.remove }
              Future
                .sequence(keysToDrop.map { col.dropIndex })
                .map { _ =>
                  ()
                }
                .recover { e =>
                  logger.warn(e.getMessage)
                }
            } else Future.successful(())
            f.flatMap { _ =>
              //We (re)create the compulsory indexes
              col
                .createIndexes(compulsoryIndexes.collect {
                  case k =>
                    IndexDef(
                      k,
                      if (k == "$**") Collection.Text
                      else Collection.Desc
                    )
                })
            }
          }
        // We do not wait for the futures to complete as it may take a lot of time to create the indexes
        // -> we let indexing run in the background
        col
      }

  private lazy val keyCache: LoadingCache[String, String] =
    Scaffeine()
      .build { _ =>
        Date.now().toString
      }

  //We do not use a Collection[Record] because Bson is not happy with ListMap
  //Indexes are created dynamically using keys seq
  //When too many indexes are created (Mongodb limits to 64 indexes per collection), some are deleted (oldest first)
  def recordsCol(version: CbfVersion,
                 keys: Seq[String] = Seq()): Collection[Document] = {
    val col = recordsColCache.get(version.branch)
    val mutable = keyCache.asMap() //Mutable reference: is kept in sync with the cache
    keys.foreach { k =>
      mutable.updateWith(k + "_1")(_ => Some(Date.now().toString))
    } //We register the new keys (without using keyCache.refresh since there is a delay)
    val sorted = mutable.toSeq.sortBy { case (_, v) => v }
    val toDrop = sorted.size - maxNbCacheIndexes
    val f = if (toDrop > 0) {
      val indexesToDrop = sorted.take(toDrop).map { _._1 }
      Future
        .sequence(indexesToDrop.map { k =>
          mutable.remove(k)
          col.dropIndex(k)
        })
        .map { _ =>
          ()
        }
        .recover { e =>
          logger.warn(e.getMessage)
        }
    } else Future.successful(())
    val ff = f
      .flatMap { _ =>
        val toCreate = keys.toSet.intersect(mutable.keySet.map {
          _.split("_").init.mkString("_")
        })
        col.createIndexes(toCreate.map { k =>
          IndexDef(k, Collection.Asc)
        })
      }
      .recover { e =>
        logger.warn(e.getMessage)
      }
      .map { _ =>
        col
      }
    //We do wait for the indexes to be built otherwise we may get 'No query solutions' errors
    Await.result(ff, 15.minutes)
  }

  lazy val versionsCol: Collection[Document] = {
    new Collection[Document](db.getCollection[Document]("_versions"))
  }

  //Return latest version
  def latest(withUnreleased: Boolean): Future[Option[VersionInfo]] = {
    versions(withUnreleased) map { _.lastOption }
  }

  /**
    * Return all registered version.
    * Note: Virtual version == version with an empty releaseDate
    * @param all If false, filter out any draft or deprecated versions. If true, return all.
    * @return
    */
  def versions(all: Boolean): Future[Seq[VersionInfo]] = {
    versionsCol.getAll().flatMap { docs =>
      if (docs.isEmpty) {
        //We use upsert instead of insert here because multiple concurrent calls to this function may happen.
        //Because it is recursive it is scary to use synchronized so we simply allow upserting.
        versionsCol
          .replace(
            Filters.eq("_id", "1.0"),
            Document(VersionInfo.draft("1.0").asJson.noSpaces),
            upsert = true
          )
          .flatMap { _ =>
            versions(all)
          }
      } else {
        Future.successful {
          val tmp = docs.map { doc =>
            decode[VersionInfo](doc.toJson()).toTry.get
          }
          val filtered =
            if (all)
              tmp
            else
              tmp
                .filter {
                  !_.isDraft
                }
                .filter {
                  !_.deprecated
                }
          filtered.sorted(VersionInfo)
        }
      }
    }
  }

  def version(ver: String, withDrafts: Boolean): Future[Option[VersionInfo]] = {
    versions(withDrafts).map { _.find(_._id == ver) }
  }

  lazy val configsCol: Collection[Document] = {
    new Collection[Document](db.getCollection[Document]("_config"))
  }

  lazy val authCol: Collection[UserInfo] = {
    new Collection[UserInfo](
      db.getCollection[UserInfo]("_auth")
        .withCodecRegistry(codecRegistry)
    )
  }

  def errorsCol(version: CbfVersion): Collection[Document] = {
    new Collection[Document](
      db.getCollection[Document](s"_${version.branch}_errors")
    )
  }

  //If id == None, returns the current one
  def config(id: Option[String] = None): Option[DatasetConfig] = {

    val _id = id.getOrElse(
      default =
      configsCol
        .getIt()
        .map { _.getString("_id") }
        .to(Seq)
         match {
        case s:Seq[String] if s.nonEmpty => s.max
        case _ => {
          println("_id collection is empty, set id to current")
          val format = new SimpleDateFormat("y-M-d")
          val date = format.format(Calendar.getInstance().getTime).toString
          println(date)
          date
        } //in case if no _id found
      }
    )
    val f = configsCol.getOne(Filters.eq("_id", _id)) map {
      _.map { doc =>
        DatasetConfig(doc.toJson())
      }
    }
    Await
      .result(f, 15.seconds)
  }

  lazy val counterKeySeed = {
    val f = latest(true)
      .flatMap {
        case Some(vi) =>
          val ver = CbfVersion(vi._id)
          val col = recordsCol(ver)
          //Faster than aggregate because we use a key
          col
            .getOne(
              sort = Document("""{ "attrs.id.obj.num": -1 }"""),
              project = Document("""{ "id": 1 }"""),
              limit = Some(1)
            )
            .map {
              _.flatMap { doc =>
                doc.getString("id").toIntOption
              }
            }
        case None =>
          Future.successful(None)
      }
      .map { o =>
        new AtomicInteger(o.getOrElse(0))
      }
    Await.result(f, 1.minute)
  }
}
