package no.uit.sfb.cbf.bulktransform

import no.uit.sfb.cbf.mongodb.MongodbConnect
import no.uit.sfb.cbf.shared.model.v1.nativ.Record
import no.uit.sfb.cbf.transform.{Merged, Unexpressed}

import scala.collection.immutable.ListMap
import scala.concurrent.ExecutionContext

class TrPatch(mgc: MongodbConnect,
              dbName: String,
              version: String,
              inputIt: Iterator[String],
              idProvider: => String,
              force: Boolean = false)(implicit ec: ExecutionContext)
    extends TrPush(mgc, dbName, version, inputIt, idProvider, force) {
  protected lazy val merger = new Merged(col, rb, cbfVersion.release)

  //Since the patches do not contain any template, it would not make sense to include the template stage.
  //Templating requires an express stage before and enhance stage after: there are replaced here with merging stage
  override protected lazy val transforms: ListMap[String, Record => Record] =
    ListMap(
      "enhancing" -> (enhancer(_)), //Note: The extra parenthesis are necessary
      "merging" -> (merger(_)),
      "expressing" -> (expressor(_)), //Needed for templating
      "templating" -> (templator(_)),
      "stamping" -> (stamper(_)),
      "re-enhancing" -> (enhancer(_)), //Needed after templating
      "re-expressing" -> (expressor(_)),
      "validating" -> (validator(_)), //Need to validate expressed record otherwise validation may fail when a value is a CIRI
      "un-expressing" -> (Unexpressed(_))
    )
}
