package no.uit.sfb.cbf.transform

import no.uit.sfb.cbf.shared.config.DatasetConfig
import no.uit.sfb.cbf.shared.model.v1.nativ._
import no.uit.sfb.cbf.utils.graph._

import scala.concurrent.duration.DurationInt
import scala.concurrent.{Await, ExecutionContext, Future}

/**
  * Transform a record by building indirect links.
  */
class Linked(
  graph: OrientedGraph,
  dsName: String,
  dsConfig: DatasetConfig,
  getRecord: (String, String) => Future[Option[Record]]
)(implicit ec: ExecutionContext) {
  private def extractId(stm: Statement): String = {
    val v = stm.simpleValue
    CompactIRI(v) match {
      case Some(ciri) =>
        ciri.id
      case None =>
        v
    }
  }

  private def getRecords(
    rec: Record,
    attrRef: String,
    dsname: String
  ): Future[Vector[(String, Option[Record])]] = {
    val stms = rec.stm(attrRef)
    Future
      .sequence(
        stms
          .map { extractId }
          .map { id =>
            getRecord(dsname, id).map { id -> _ }
          }
      )
  }

  private def fetchAll(rec: Record,
                       path: Seq[Edge]): Future[Vector[Statement]] = {
    path.headOption match {
      case None =>
        //We reached destination
        Future.successful(rec.stm("id").headOption.toVector)
      case Some(Edge(attrRef, nextDsName)) =>
        getRecords(rec, attrRef, nextDsName)
          .map { _.flatMap { _._2 } }
          .flatMap { records =>
            Future
              .sequence(records.map { rec =>
                fetchAll(rec, path.tail)
              })
              .map {
                _.flatten
              }
          }
    }
  }

  def apply(record: Record): Record = {
    //First we remove broken links
    val fStmsWithoutBrokenLinks = Future
      .sequence(
        dsConfig.attrs
          .collect {
            case attr if attr.lType.isSubtypeOf("dblink:direct") =>
              attr.ref -> attr.lType.tertiary
          }
          .map {
            case (attrRef, target) =>
              getRecords(record, attrRef, target).map { x =>
                val brokenLinks = x.collect {
                  case (id, None) => id
                }
                attrRef -> record.stm(attrRef).filter { stm =>
                  !brokenLinks.contains(extractId(stm))
                }
              }
          }
      )
      .map { _.toMap }
    //Then we add the indirect links
    val f = fStmsWithoutBrokenLinks
      .map { stmsWithoutBrokenLinks =>
        record.addAttrs(stmsWithoutBrokenLinks, true)
      }
      .flatMap { recordWithoutBrokenLinks =>
        val fStms = Future
          .sequence(
            dsConfig.attrs
              .filter { attr =>
                attr.lType.isSubtypeOf("dblink:indirect")
              }
              .flatMap { attr =>
                graph.getPath(dsName, attr.lType.tertiary).map {
                  attr.ref -> _
                }
              }
              .map {
                case (ref, path) =>
                  fetchAll(recordWithoutBrokenLinks, path).map {
                    ref -> _
                  }
              }
          )
          .map {
            _.toMap
          }
        fStms.map { stms =>
          recordWithoutBrokenLinks.addAttrs(stms, true)
        }
      }
    Await.result(f, 1.minute)
  }
}
