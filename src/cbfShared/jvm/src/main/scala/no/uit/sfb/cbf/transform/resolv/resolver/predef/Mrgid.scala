package no.uit.sfb.cbf.transform.resolv.resolver.predef

import io.circe.parser.parse
import no.uit.sfb.cbf.shared.config.CustomResolver
import no.uit.sfb.cbf.transform.resolv.resolver._

import scala.concurrent.Future

trait Mrgid extends ExternalResolverLike with StaticResolverLike {
  lazy val ct = CustomResolver(
    "mrgid",
    "^\\d+$",
    Map("_iri" -> "https://marineregions.org/gazetteer.php?p=details&id={id}")
  )

  protected def dataUrl(id: String): String = {
    s"https://marineregions.org/rest/getGazetteerRecordByMRGID.json/$id/" //The last slash is important
  }

  protected def dataJsonldUrl(id: String): String = {
    s"https://marineregions.org/rest/getGazetteerRecordByMRGID.jsonld/$id/" //The last slash is important
  }

  /**
    * Parse the current document and extracts the id of the first parent and its type (only world_region or country)
    * @param str
    * @return
    */
  private def parent(str: String): Seq[(String, Option[String])] = {
    val countryTypes = Set("mrt:Nation")
    val seaTypes =
      Set("mrt:Ocean", "mrt:IHOSeaArea", "mrt:GeneralSeaArea", "mrt:HighSeas")
    val continentTypes = Set("mrt:Continent")

    parse(str).toOption
      .map {
        _.hcursor
      }
      .map { json =>
        json
          .downField("mr:isPartOf")
          .values
          .toSeq
          .flatten
          .flatMap { j =>
            val types = j.hcursor
              .downField("@type")
              .as[Seq[String]]
              .toOption
              .getOrElse(Seq())
            j.hcursor
              .downField("@id")
              .as[String]
              .toOption
              .flatMap {
                _.split('/').lastOption
              }
              .map {
                _ -> (if (types.toSet.intersect(countryTypes).nonEmpty)
                        Some("country")
                      else if (types.toSet
                                 .intersect(seaTypes)
                                 .nonEmpty)
                        Some("sea")
                      else if (types.toSet
                                 .intersect(continentTypes)
                                 .nonEmpty)
                        Some("continent")
                      else None)
              }
          }
      }
      .toSeq
      .flatten
  }

  private def lineage(
    id: String,
    acc: Map[String, String] = Map()
  ): Future[Map[String, String]] = {
    getData(dataJsonldUrl(id)).flatMap { str =>
          val parents = parent(str)
          if (parents.isEmpty)
            Future.successful(acc) //We are done
          else {
            Future
              .sequence(parents.map {
                case (parentId, Some(tpe)) =>
                  lineage(parentId, acc + (tpe -> parentId))
                case (parentId, None) =>
                  lineage(parentId, acc)
              })
              .map {
                _.flatten.toMap //We are not loosing information as different branches would not overlap (one is continent, the other is sea)
              }
          }
    }
  }

  private def getJsonLdData(id: String): Future[Map[String, String]] = {
    getData(dataJsonldUrl(id)).zip(lineage(id)).map {
      case (str, rawLineage) =>
        //We privilege sea over continent (remove nation bias for maritime areas)
        val worldRegion =
          rawLineage.get("sea").orElse(rawLineage.get("continent"))
        val l = (worldRegion match {
          case Some(wr) => rawLineage + ("world_region" -> wr)
          case _        => rawLineage
        }).view
        .mapValues { "mrgid:" + _ }
          .collect{
            case (k @ "country", v) =>
              k -> Mrgid.countriesToGaz.getOrElse(v, v)
            case (k @ "world_region", v) =>
              k -> (Mrgid.continentsToGaz ++ Mrgid.seasToGaz).getOrElse(v, v)
          }
          parseJsonLdData(str) ++ l
    }
  }

  private def parseJsonLdData(str: String): Map[String, String] = {
    val json = parse(str).toTry.get.hcursor
    val synonyms = json
      .downField("skos:altLabel")
      .values
      .toSeq
      .flatMap {
        _.flatMap { x =>
          x.hcursor
            .downField("@value")
            .as[String]
            .toOption
        }
      }
    Map("synonyms" -> {
      if (synonyms.isEmpty) None else Some(synonyms.mkString("|"))
    }).collect { case (k, Some(v)) => k -> v }
  }

  protected def parseData(str: String): Map[String, String] = {
    val json = parse(str).toTry.get.hcursor
        Map(
          "val" -> json.downField("preferredGazetteerName").as[String].toOption,
          "description" -> json
            .downField("placeType")
            .as[String]
            .toOption,
          "latitude" -> json
            .downField("latitude")
            .as[Float]
            .toOption
            .map { _.toString },
          "longitude" -> json
            .downField("longitude")
            .as[Float]
            .toOption
            .map { _.toString },
        ).collect { case (k, Some(v)) => k -> v }
  }

  override def metaExternal(id: String): Future[Map[String, String]] = {
    val dataFromJson = getData(dataUrl(id)).map {
        parseData
    }
    lazy val dataFromJsonLd = getJsonLdData(id)
    dataFromJson.zip(dataFromJsonLd).map {
      case (json, jsonld) => json ++ jsonld
    }
  }
}

object Mrgid {
  val countriesToGaz = Map(
      "mrgid:2152" -> "gaz:00000563", //Algeria
      "mrgid:2150" -> "gaz:00001095", //Angola
      "mrgid:2139" -> "gaz:00000904", //Benin
      "mrgid:2164" -> "gaz:00001097", //Botswana
      "mrgid:2173" -> "gaz:00000905", //Burkina Faso
      "mrgid:2172" -> "gaz:00001090", //Burundi
      "mrgid:2170" -> "gaz:00001093", //Cameroon
      "mrgid:2168" -> "gaz:00001227", //Cape Verde
      "mrgid:2167" -> "gaz:00001089", //Central African Republic
      "mrgid:2155" -> "gaz:00000586", //Chad
      "mrgid:2163" -> "gaz:00005820", //Comores
      "mrgid:2198" -> "gaz:00001086", //Democratic Republic of The Congo
      "mrgid:2131" -> "gaz:00000582", //Djibouti
      "mrgid:3868" -> "gaz:00003934", //Egypt
      "mrgid:2112" -> "gaz:00001091", //Equatorial Guinea
      "mrgid:2111" -> "gaz:00000581", //Eritrea
      "mrgid:2109" -> "gaz:00000567", //Ethiopia
      "mrgid:2187" -> "gaz:00001104", //Federal Republic of Somalia
      "mrgid:2104" -> "gaz:00001092", //Gabon
      "mrgid:2113" -> "gaz:00000907", //Gambia
      "mrgid:2100" -> "gaz:00000908", //Ghana
      "mrgid:2122" -> "gaz:00000909", //Guinea
      "mrgid:8685" -> "gaz:00000910", //Guinea-Bissau
      "mrgid:2161" -> "gaz:00000906", //Ivory Coast
      "mrgid:2118" -> "gaz:00001101", //Kenya
      "mrgid:2107" -> "gaz:00001098", //Leshoto
      "mrgid:2105" -> "gaz:00000911", //Liberia
      "mrgid:2194" -> "gaz:00000566", //Lybia
      "mrgid:7963" -> "gaz:00001108", //Madagascar
      "mrgid:2231" -> "gaz:00001105", //Malawi
      "mrgid:2228" -> "gaz:00000584", //Mali
      "mrgid:2236" -> "gaz:00000583", //Mauritania
      "mrgid:2221" -> "gaz:00000565", //Morocco
      "mrgid:2220" -> "gaz:00001100", //Mozambique
      "mrgid:2218" -> "gaz:00001096", //Namibia
      "mrgid:2254" -> "gaz:00000585", //Niger
      "mrgid:2253" -> "gaz:00000912", //Nigeria
      "mrgid:8614" -> "gaz:00003745", //Republic of Mauritius
      "mrgid:8633" -> "gaz:00001088", //Republic of the Congo
      "mrgid:2239" -> "gaz:00001087", //Rwanda
      //"mrgid:2238" -> "gaz:", //Sao Tome and Principe
      "mrgid:2225" -> "gaz:00000913", //Senegal
      "mrgid:2192" -> "gaz:00005821", //Seychelles
      "mrgid:2191" -> "gaz:00000914", //Sierra Leone
      "mrgid:2176" -> "gaz:00001094", //South Africa
      "mrgid:31475" -> "gaz:00233439", //South Sudan
      "mrgid:2183" -> "gaz:00000560", //Sudan
      "mrgid:2181" -> "gaz:00001099", //Swaziland
      "mrgid:2205" -> "gaz:00001103", //Tanzania
      "mrgid:8632" -> "gaz:00000915", //Togo
      "mrgid:2214" -> "gaz:00000562", //Tunisia
      "mrgid:2209" -> "gaz:00001102", //Uganda
      "mrgid:2197" -> "gaz:00001107", //Zambia
      "mrgid:2234" -> "gaz:00001106", //Zimbabwe
      "mrgid:2256" -> "gaz:00006882", //Afghanistan
      "mrgid:2148" -> "gaz:00004094", //Armenia
      "mrgid:2134" -> "gaz:00004941", //Azerbaijan
      "mrgid:2154" -> "gaz:00005281", //Bahrain
      "mrgid:2142" -> "gaz:00003750", //Bangladesh
      "mrgid:2138" -> "gaz:00003920", //Bhutan
      "mrgid:2145" -> "gaz:00003901", //Brunei
      "mrgid:2171" -> "gaz:00006888", //Cambodia
      "mrgid:8603" -> "gaz:00002845", //China
      "mrgid:8757" -> "gaz:00006913", //East Timor
      "mrgid:2128" -> "gaz:00002839", //India
      "mrgid:2127" -> "gaz:00003727", //Indonesia
      "mrgid:2126" -> "gaz:00004474", //Iran
      "mrgid:2125" -> "gaz:00004483", //Iraq
      "mrgid:2123" -> "gaz:00002476", //Israel
      "mrgid:2121" -> "gaz:00002747", //Japan
      "mrgid:2120" -> "gaz:00002473", //Jordan
      "mrgid:2119" -> "gaz:00004999", //Kazakhstan
      "mrgid:2115" -> "gaz:00005285", //Kuwait
      "mrgid:2117" -> "gaz:00006893", //Kyrgyzstan
      "mrgid:17551" -> "gaz:00006889", //Laos
      "mrgid:2098" -> "gaz:00002478", //Lebanon
      "mrgid:2230" -> "gaz:00003902", //Malaysia
      "mrgid:2229" -> "gaz:00006896", //Maldives
      "mrgid:17584" -> "gaz:00008744", //Mongolia
      "mrgid:2219" -> "gaz:00006899", //Myanmar
      "mrgid:2246" -> "gaz:00004399", //Nepal
      "mrgid:8601" -> "gaz:00002801", //North Korea
      "mrgid:2251" -> "gaz:00005283", //Oman
      "mrgid:2250" -> "gaz:00005246", //Pakistan
      "mrgid:15525" -> "gaz:00002475", //Palestine
      "mrgid:2245" -> "gaz:00004525", //Philippines
      "mrgid:2242" -> "gaz:00005286", //Qatar
      "mrgid:2215" -> "gaz:00005279", //Saudi Arabia
      "mrgid:2190" -> "gaz:00003923", //Singapore
      "mrgid:8600" -> "gaz:00002802", //South Korea
      "mrgid:2195" -> "gaz:00003924", //Sri Lanka
      "mrgid:3384" -> "gaz:00002474", //Syria
      "mrgid:2177" -> "gaz:00005341", //Taiwan
      "mrgid:2178" -> "gaz:00006912", //Tajikistan
      "mrgid:2207" -> "gaz:00003744", //Thailand
      "mrgid:2213" -> "gaz:00000558", //Turkey
      "mrgid:2212" -> "gaz:00005018", //Turkmenistan
      "mrgid:2206" -> "gaz:00005282", //United Arab Emirates
      "mrgid:2203" -> "gaz:00004979", //Uzbekistan
      "mrgid:2200" -> "gaz:00003756", //Vietnam
      "mrgid:2199" -> "gaz:00005284", //Yemen
      "mrgid:2153" -> "gaz:00002953", //Albania
      "mrgid:2151" -> "gaz:00002948", //Andorra
      "mrgid:2146" -> "gaz:00002942", //Austria
      "mrgid:2141" -> "gaz:00006886", //Belarus
      "mrgid:14" -> "gaz:00002938", //Belgium
      "mrgid:2136" -> "gaz:00006887", //Bosnia and Herzegovina
      "mrgid:2174" -> "gaz:00002950", //Bulgaria
      "mrgid:2160" -> "gaz:00002719", //Croatia
      "mrgid:2159" -> "gaz:00004007", //Cyprus
      "mrgid:2158" -> "gaz:00002954", //Czech Republic
      "mrgid:2157" -> "gaz:00002635", //Denmark
      "mrgid:2110" -> "gaz:00002959", //Estonia
      "mrgid:2106" -> "gaz:00002937", //Finland
      "mrgid:17" -> "gaz:00002940", //France
      "mrgid:2102" -> "gaz:00004942", //Georgia
      "mrgid:2101" -> "gaz:00002646", //Germany
      "mrgid:2233" -> "gaz:00002947", //Luxembourg
      "mrgid:2099" -> "gaz:00002945", //Greece
      "mrgid:2130" -> "gaz:00002952", //Hungary
      "mrgid:2129" -> "gaz:00000843", //Iceland
      "mrgid:2114" -> "gaz:00002943", //Ireland
      "mrgid:2133" -> "gaz:00002650", //Italy
      "mrgid:2132" -> "gaz:00002958", //Latvia
      "mrgid:2235" -> "gaz:00003858", //Lichtenstein
      "mrgid:2156" -> "gaz:00002960", //Lithuania
      "mrgid:2232" -> "gaz:00006895", //Macedonia
      "mrgid:2217" -> "gaz:00004017", //Malta
      "mrgid:2223" -> "gaz:00003897", //Moldova
      "mrgid:2222" -> "gaz:00003857", //Monaco
      "mrgid:17831" -> "gaz:00006898", //Montenegro
      "mrgid:2252" -> "gaz:00002699", //Norway
      "mrgid:2244" -> "gaz:00002939", //Poland
      "mrgid:2243" -> "gaz:00002944", //Portugal
      "mrgid:2241" -> "gaz:00002951", //Romania
      "mrgid:21449" -> "gaz:00002957", //Serbia
      "mrgid:2189" -> "gaz:00002956", //Slovakia
      "mrgid:2188" -> "gaz:00002955", //Slovenia
      "mrgid:2185" -> "gaz:00000591", //Spain
      "mrgid:2180" -> "gaz:00002729", //Sweden
      "mrgid:2179" -> "gaz:00002941", //Switzerland
      "mrgid:2196" -> "gaz:00002724", //Ukraine
      "mrgid:2208" -> "gaz:00002637", //UK
      "mrgid:21447" -> "gaz:00003103", //Vatican
      "mrgid:8645" -> "gaz:00006883", //Antigua and Barbuda
      "mrgid:2144" -> "gaz:00002733", //Bahamas
      "mrgid:8649" -> "gaz:00001251", //Barbados
      "mrgid:8681" -> "gaz:00002934", //Belize
      "mrgid:2169" -> "gaz:00002560", //Canada
      "mrgid:2162" -> "gaz:00002901", //Costa Rica
      "mrgid:8637" -> "gaz:00003762", //Cuba
      "mrgid:8648" -> "gaz:00006890", //Dominica
      "mrgid:8640" -> "gaz:00003952", //Dominican Republic
      "mrgid:2135" -> "gaz:00002935", //El Salvador
      "mrgid:8650" -> "gaz:02000573", //Grenada
      "mrgid:2097" -> "gaz:00002936", //Guatemala
      "mrgid:8639" -> "gaz:00003953", //Haiti
      "mrgid:2103" -> "gaz:00002894", //Honduras
      "mrgid:8682" -> "gaz:00003781", //Jamaica
      "mrgid:2224" -> "gaz:00002852", //Mexico
      "mrgid:2255" -> "gaz:00002978", //Nicaragua
      "mrgid:2249" -> "gaz:00002892", //Panama
      //"mrgid:8644" -> "gaz:", //Saint Kitts and Nevis
      "mrgid:8647" -> "gaz:00006909", //Saint Lucia
      "mrgid:8651" -> "gaz:02000565", //Saint Vincent and the Grenadines
      "mrgid:2186" -> "gaz:00003767", //Trinidad and Tobago
      "mrgid:2204" -> "gaz:00002459", //USA
      "mrgid:2147" -> "gaz:00000463", //Australia
      "mrgid:2108" -> "gaz:00006891", //Fiji
      "mrgid:2116" -> "gaz:00006894", //Kiribati
      "mrgid:2226" -> "gaz:00006470", //Marshall Islands
      "mrgid:8595" -> "gaz:00006897", //Mircronesia
      "mrgid:8596" -> "gaz:00006900", //Nauru
      "mrgid:2227" -> "gaz:00000469", //New Zealand
      "mrgid:8594" -> "gaz:00006905", //Palau
      "mrgid:2237" -> "gaz:00003922", //Papua New Guinea
      "mrgid:8671" -> "gaz:00006910", //Samoa
      "mrgid:8593" -> "gaz:00005275", //Solomon Islands
      "mrgid:8674" -> "gaz:00006916", //Tonga
      "mrgid:2210" -> "gaz:00009715", //Tuvalu
      "mrgid:2202" -> "gaz:00006918", //Vanatu
      "mrgid:2149" -> "gaz:00002928", //Argentina
      "mrgid:2137" -> "gaz:00002511", //Bolivia
      "mrgid:2143" -> "gaz:00002828", //Brazil
      "mrgid:2165" -> "gaz:00002825", //Chile
      "mrgid:2175" -> "gaz:00002929", //Colombia
      "mrgid:2166" -> "gaz:00002912", //Ecuador
      "mrgid:2124" -> "gaz:00002522", //Guyana
      "mrgid:2247" -> "gaz:00002933", //Paraguay
      "mrgid:2257" -> "gaz:00002932", //Peru
      "mrgid:2182" -> "gaz:00002525", //Suriname
      "mrgid:2216" -> "gaz:00002930", //Uruguay
      "mrgid:2201" -> "gaz:00002931", //Venezuela
    )

  val continentsToGaz = Map(
      "mrgid:1922" -> "gaz:00000468", //Oceania
      "mrgid:1921" -> "gaz:00000465", //Asia
      "mrgid:1920" -> "gaz:00000464", //Europe
      "mrgid:1925" -> "gaz:00000459", //S America
      "mrgid:1924" -> "gaz:00000458", //N America
      "mrgid:1926" -> "gaz:00000062", //Antarctica
      "mrgid:1923" -> "gaz:00000457" //Africa
    )

  val seasToGaz = Map(
      "mrgid:1906" -> "gaz:00051067", //Arctic Ocean
      "mrgid:1902" -> "gaz:00051071", //Atlantic Ocean
      "mrgid:2401" -> "gaz:00002283", //Baltic Sea
      //"mrgid:63203" -> "gaz:", //High Seas
      "mrgid:1904" -> "gaz:00051084", //Indian Ocean
      "mrgid:4278" -> "gaz:00002287", //Mediterranean sea
      "mrgid:1903" -> "gaz:00051073", //Pacific ocean
      "mrgid:4331" -> "gaz:00008989", //Southern China and Archipelagic Seas
      "mrgid:1907" -> "gaz:00000373", //Southern Ocean
    )
}
