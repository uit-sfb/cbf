package no.uit.sfb.cbf.transform.resolv.resolver.predef

import scala.xml.XML
import no.uit.sfb.cbf.transform.resolv.resolver._

trait ERC extends ExternalResolverLike {
  val prefix = "erc"
  override lazy val regexp = "^ERC\\d+$".r

  override def metaStatic(id: String,
                          dbName: String,
                          version: String,
                          apiHost: String,
                          groups: Array[String]): Map[String, String] = {
    super.metaStatic(id, dbName, version, apiHost, groups) + ("_iri" -> s"https://www.ebi.ac.uk/ena/browser/view/$id")
  }

  protected def dataUrl(id: String): String = {
    s"https://www.ebi.ac.uk/ena/browser/api/xml/$id"
  }

  protected def parseData(str: String): Map[String, String] = {
    val doc = XML.loadString(str)
    val descriptor = doc \ "CHECKLIST" \ "DESCRIPTOR"
    val name = descriptor \ "NAME"
    val description = descriptor \ "DESCRIPTION"
    Map("val" -> name.text, "description" -> description.text)
  }
}
