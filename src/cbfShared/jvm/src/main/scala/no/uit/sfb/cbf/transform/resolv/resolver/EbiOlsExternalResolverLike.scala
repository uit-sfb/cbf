package no.uit.sfb.cbf.transform.resolv.resolver

import io.circe.parser.parse

/**
  * EBI OLS external resolver
  * ATTENTION: make sure to add any ExternalResolver to Resolution.resolvers()
  */
trait EbiOlsExternalResolverLike extends ExternalResolverLike {
  def baseUri: String

  protected def dataUrl(id: String): String = {
    s"https://www.ebi.ac.uk/ols/api/ontologies/$prefix/terms?iri=$baseUri$id"
  }

  protected def parseData(str: String): Map[String, String] = {
    val json = parse(str).toTry.get.hcursor
    val term = json.downField("_embedded").downField("terms").downArray
    val annotations = term.downField("annotation")
    val synonyms =
      Seq(
        "has_related_synonym",
        "has_exact_synonym",
        "has_broad_synonym",
        "synonym",
        "synonym"
      ).map { t =>
          annotations
            .downField(t)
            .as[Seq[String]]
            .toOption
            .getOrElse(Seq())
        }
        .foldLeft(Seq[String]()) { case (acc, s) => acc ++ s }
    Map(
      "val" -> term.downField("label").as[String].toOption.map {
        _.capitalize
      },
      "description" -> term
        .downField("description")
        .downArray
        .as[String]
        .toOption
        .map {
          _.capitalize
        },
      "synonyms" -> {
        if (synonyms.isEmpty) None else Some(synonyms.mkString("|"))
      },
    ).collect { case (k, Some(v)) => k -> v }
  }
}
