package no.uit.sfb.cbf.utils

import no.uit.sfb.cbf.jsonld.DataRecord
import org.json4s.native.Serialization._
import org.json4s._

//Circe cannot serialize DataRecord because it contains Any.
object JsonLdSerializer {
  def serializeArray(array: Seq[DataRecord]): String = {
    implicit val formats = DefaultFormats
    writePretty[Seq[DataRecord]](array)
  }

  def serialize(data: DataRecord): String = {
    implicit val formats = DefaultFormats
    write[DataRecord](data)
  }
}
