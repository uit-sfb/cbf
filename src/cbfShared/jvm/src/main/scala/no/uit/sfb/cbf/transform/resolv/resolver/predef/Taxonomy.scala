package no.uit.sfb.cbf.transform.resolv.resolver.predef

//import io.circe.ACursor
//import io.circe.parser.parse
import io.circe._
import io.circe.parser._

import scala.concurrent.{Await, Future}
import scala.concurrent.duration.DurationInt
import no.uit.sfb.cbf.transform.resolv.resolver._

import scala.io.Source
import scala.util.{Failure, Success, Try, Using}
import scala.xml.XML
import sys.process._

//Too much difference too use EbiOlsExternalResolverLike
trait Taxonomy extends ExternalResolverLike {

  protected def dataUrl(id: String): String = {
    val formatedId = s"NCBITaxon_$id"
    s"https://www.ebi.ac.uk/ols/api/ontologies/ncbitaxon/terms?iri=http://purl.obolibrary.org/obo/$formatedId"
  }

  //Returns a Map of rank -> id
  protected def lineage(cursor: ACursor): Map[String, String] = {
    val hierarchicalAncestors = cursor
      .downField("_links")
      .downField("hierarchicalAncestors")
      .downField("href")
      .as[String]
      .toOption
    hierarchicalAncestors
      .map {
        getData
      }
      .map { res =>
        parse(Await.result(res, 45.seconds)).toTry.get
      }
      .flatMap { parseRes =>
        val json = parseRes.hcursor
        val terms = json
          .downField("_embedded")
          .downField("terms")
        terms.values
      }
      .map { terms =>
        val withCurrentTerm = cursor +: terms.map {
          _.hcursor
        }.toSeq
        withCurrentTerm.flatMap { term =>
          val oId = term
            .downField("obo_id")
            .as[String]
            .toOption
            .map {
              _.split(':').last
            }
            .map { str =>
              s"taxonomy:$str"
            }
          oId flatMap { id =>
            term
              .downField("annotation")
              .downField("has_rank")
              .downArray
              .as[String]
              .toOption map { rank =>
              rank.split("NCBITaxon_").last -> id
            }
          }
        }.toMap
      }
      .getOrElse(Map())
  }

  protected def parseData(str: String): Map[String, String] = {
    val json = parse(str).toTry.get.hcursor
    val term = json.downField("_embedded").downField("terms").downArray
    val annotations = term.downField("annotation")
    val synonyms =
      Seq("has_related_synonym", "has_exact_synonym", "has_broad_synonym")
        .map { t =>
          annotations
            .downField(t)
            .as[Seq[String]]
            .toOption
            .getOrElse(Seq())
        }
        .foldLeft(Seq[String]()) { case (acc, s) => acc ++ s }
        .map {
          _.capitalize
        }
    Map(
      "val" -> term.downField("label").as[String].toOption.map {
        _.capitalize
      },
      "description" -> term
        .downField("description")
        .downArray
        .as[String]
        .toOption,
      "synonyms" -> {
        if (synonyms.isEmpty) None else Some(synonyms.mkString("|"))
      },
    ).collect { case (k, Some(v)) => k -> v } ++ lineage(term)
  }

  private def runCurl(url: String) = {
    Try {
      val s: String = s"curl -sb -H $url".!!
      s
    }
  }

  //Pass new id if record is obsolete, None otherwise.
  private def checkObsolete(id: String) = {
    Future.fromTry(runCurl(s"https://rest.uniprot.org/taxonomy/${id}.json")).map { str =>
      val jsonResponse = parse(str)
      jsonResponse match {
        case Right(jsonVal) =>
          (jsonVal \\ "mergedTo").headOption
          .flatMap { s =>
            s.asNumber
          }.filter(_.toString.nonEmpty)
          match {
          case Some(newId) if newId.toString != id => Some(newId.toString)
          case _ => None
        }
        case Left(e) => throw new Exception(e.message)
      }
    }
  }

  override def metaExternal(id: String): Future[Map[String, String]] = {
    /**
     * We parse updated taxonomy id instead of throwing an error.
     * Any errors will be passed to the resulted Future
     */
    checkObsolete(id).flatMap {
      case Some(newId:String) => getData(dataUrl(newId)).map {parseData}
      case None => getData(dataUrl(id)).map {parseData}
    }
  }
}
