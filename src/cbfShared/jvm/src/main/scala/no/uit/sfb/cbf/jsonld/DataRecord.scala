package no.uit.sfb.cbf.jsonld

import no.uit.sfb.cbf.shared.model.v1.nativ

case class DataRecord(`@type`: String = "DataRecord",
                      `@context`: Seq[Any] = DataRecord.context,
                      override val `@id`: Option[String],
                      identifier: String,
                      url: String,
                      dateCreated: String,
                      dateModified: String,
                      datePublished: String,
                      mainEntity: Sample,
                      additionalProperty: Set[PropertyValue] = Set(),
                      distribution: Set[Distribution] = Set(),
                      isPartOf: Option[IsPartOf] = None)
    extends JsonLdLike

object DataRecord {
  val context: Seq[Any] = {
    //Fixme
    Seq("https://schema.org") /* ++
      Seq(
        Resolution.resolvers(customResolvers)
          .flatMap { acc =>
            acc.metaStatic("", "", Array()).get("_iri").map{
              acc.prefix -> _
            }
          }
          .toMap
          .asInstanceOf[Map[String, Any]] ++
          Map[String, Any](
            "contentURL" -> Map("@type" -> "@id"),
            "source" -> Map("@type" -> "@id")
          )
      )*/
  }

  def apply(rec: nativ.Record, url: Option[String]): DataRecord = {
    val compactId = rec.stm("id").headOption.map {
      _.simpleValue
    }
    val biosampleAcc =
      rec.stm("acc:biosample").headOption.map {
        _.simpleValue
      } match {
        case Some("") => None
        case v        => v
      }
    val (_, props) = rec.attributes.partition {
      case (attr, _) =>
        val collName = nativ.Attribute(attr).collectionName
        collName
          .startsWith("_") || collName.isEmpty
    }
    val sample = Sample(
      `@id` = compactId,
      identifier = compactId.orNull,
      url = url.orNull,
      name = rec.id.orNull,
      subjectOf = Set(biosampleAcc).flatten,
      additionalProperty = Set(props.map {
        case (name, stm) =>
          PropertyValue(
            name = name,
            value = stm.valueOnly.asString,
            valueReference = stm.obj.flatMap { value =>
              if (value.iri.nonEmpty)
                Some(ValueReference(url = value.iri))
              else None
            }.toSet,
            `custom:evidence` = stm.evi.toSet.map {
              ev: nativ.Evidence =>
                val evi =
                  ev.predicate.iri.headOption match {
                    case Some(ref) =>
                      Evidence(
                        codeValue = ref,
                        inCodeSet = ref.split(':').headOption
                      )
                    case None =>
                      Evidence(codeValue = ev.predicate.`val`, inCodeSet = None)
                  }
                CustomEvidence(evidence = evi, source = ev.src.flatMap { ref =>
                  ref.iri.toSet
                })
            }
          )
      }).flatten
    )
    DataRecord(
      `@id` = compactId,
      identifier = compactId.orNull,
      url = url.orNull,
      dateCreated = rec
        .stm("_time.creation_date")
        .headOption
        .map {
          _.simpleValue
        }
        .orNull,
      dateModified = rec
        .stm("_time.update_date")
        .headOption
        .map {
          _.simpleValue
        }
        .orNull,
      datePublished = rec
        .stm("_time.creation_date")
        .headOption
        .map {
          _.simpleValue
        }
        .orNull,
      mainEntity = sample,
      additionalProperty = Set(
        rec
          .stm("_time.release_version")
          .headOption
          .map { rv =>
            PropertyValue(name = "time:release_version", value = rv.simpleValue)
          }
      ).flatten,
      distribution = Set(),
      isPartOf = None
    )
  }

  def fromRecords(recs: Iterator[nativ.Record],
                  url: Option[String]): Iterator[DataRecord] = {
    recs map { rec =>
      DataRecord(rec, url)
    }
  }
}
