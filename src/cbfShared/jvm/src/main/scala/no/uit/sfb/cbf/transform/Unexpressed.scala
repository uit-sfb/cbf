package no.uit.sfb.cbf.transform

import no.uit.sfb.cbf.shared.model.v1.nativ._

/**
  * Inverse operation of expression (Expressed)
  * The record looses its meta object, and `val` is emptied in case it can be retrieved by a CIRI
  */
object Unexpressed {
  def apply(vr: ValueRef): ValueRef = {
    val v = vr.`val`.toLowerCase
    val redundant = vr.meta.getOrElse("val", "").toLowerCase == v ||
      vr.meta.getOrElse("_key", "").toLowerCase == v ||
      vr.meta.getOrElse("_id", "").toLowerCase == v ||
      vr.meta
        .getOrElse("synonyms", "")
        .split('|')
        .map {
          _.toLowerCase
        }
        .contains(v)
    vr.copy(
      `val` =
        if (redundant) {
          ""
        } else
          vr.`val`,
      meta = Map()
    )
  }

  def apply(evidence: Evidence): Evidence = {
    evidence.copy(
      meta = Map(),
      src = evidence.src
        .map {
          apply
        }
        .filterNot { _.isEmpty }
    )
  }

  def apply(statement: Statement): Statement = {
    statement.copy(
      obj = statement.obj
        .map {
          apply
        }
        .filterNot { _.isEmpty },
      evi = statement.evi
        .map {
          apply
        }
        .filterNot { _.isEmpty }
    )
  }

  def apply(record: Record): Record = {
    record.copy(
      attrs = record.attributes
        .map {
          case (k, stm) =>
            k -> apply(stm)
        }
        .filterNot { case (_, stm) => stm.isEmpty }
        .map {
          Map(_)
        }
    )
  }
}
