package no.uit.sfb.cbf.transform.resolv.resolver.predef

import io.circe.Decoder
import io.circe.parser.decode
import io.circe.generic.auto._
import no.uit.sfb.cbf.transform.resolv.resolver._

import scala.util.Try

trait RRID extends ExternalResolverLike {
  protected def dataUrl(id: String): String = {
    //The key is a private key. To get a new one:
    //- create an account on https://scicrunch.org/
    //- Click on My Account -> API keys
    val key = "wuSUfcRC2xzWUPlCMeCG8hfY7X94GSMR"
    s"https://scicrunch.org/api/1/resource/fields/view/$id?key=$key"
  }

  case class RridData(data: Data)

  case class Data(fields: List[Field], scicrunch_id: String)

  case class Field(field: String, value: ArrayOrStringOrNull)

  class ArrayOrStringOrNull(val v: Seq[String]) {
    val simpleValue = v.headOption
  }

  implicit val decodeArrayOrStringOrNull: Decoder[ArrayOrStringOrNull] = {
    Decoder
      .decodeArray[String]
      .emapTry { a =>
        Try {
          new ArrayOrStringOrNull(a.toList)
        }
      }
      .or(Decoder.decodeString.emapTry { a =>
        Try {
          new ArrayOrStringOrNull(Seq(a))
        }
      })
      .or(Decoder.decodeNone.emapTry { _ =>
        Try {
          new ArrayOrStringOrNull(Seq())
        }
      })
  }

  protected def parseData(str: String): Map[String, String] = {
    val d = decode[RridData](str).toTry.get
    Map(
      "val" ->
        Some(
          d.data.fields
            .find(_.field == "Resource Name")
            .flatMap {
              _.value.simpleValue
            }
            .getOrElse(d.data.scicrunch_id)
        ),
      //"description" -> d.data.fields.find(_.field == "Description").flatMap{_.value.simpleValue},
      "synonyms" -> d.data.fields.find(_.field == "Synonyms").flatMap { s =>
        val syns = s.value.v.mkString("|")
        if (syns.isEmpty)
          None
        else
          Some(syns)
      },
      //"url" -> d.data.fields.find(_.field == "Resource URL").flatMap{_.value},
    ).collect { case (k, Some(v)) => k -> v }
  }
}
