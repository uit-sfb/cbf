package no.uit.sfb.cbf.jsonschema

import java.io.StringReader
import java.net.URI
import java.nio.file.{Path, Paths}
import com.github.blemale.scaffeine.{LoadingCache, Scaffeine}
import com.typesafe.scalalogging.LazyLogging
import no.uit.sfb.cbf.utils.UriUtils
import no.uit.sfb.scalautils.common.StringUtils
import org.leadpony.justify.api._

import scala.annotation.tailrec
import scala.concurrent.duration._
import scala.io.Source
import scala.util.{Failure, Success, Try}

/**
  * Note: Using a URL is much slower than using a Path because it takes time to download all the schemas (even though they are cached)
  *
  * @param schemaUri
  */
class Validator(schemaUri: URI) extends LazyLogging {
  //Cannot be a lazy val!
  protected val service = JsonValidationService.newInstance()

  //We use a cache because otherwise the same auxiliary schema is loaded again and again for each new attribute
  private lazy val schemaCache: LoadingCache[URI, JsonSchema] = Scaffeine()
    .maximumSize(100)
    .expireAfterAccess(1.hours)
    .build(resolveSchemaImpl(_))

  protected lazy val readerFactory: JsonSchemaReaderFactory = service
    .createSchemaReaderFactoryBuilder()
    .withSchemaResolver(new JsonSchemaResolver {
      def resolveSchema(id: URI) = schemaCache.get(id)
    })
    .build()

  protected def reader(path: Path): Unit = {
    val handler = ExceptionProblemHandler(path.toString).asProblemHandler
    val reader = service.createReader(path, schema, handler)
    reader.readValue()
  }

  protected def reader(str: String): Unit = {
    val handler = ExceptionProblemHandler(StringUtils.truncateString(str, 100)).asProblemHandler
    val strReader = new StringReader(str)
    try {
      val reader = service.createReader(strReader, schema, handler)
      reader.read()
    } finally strReader.close()
  }

  def validate[T](obj: T): Unit = {
    obj match {
      case str: String =>
        reader(str)
      case path: Path =>
        reader(path)
      case unknown =>
        throw new Exception(s"Type ${unknown.getClass.getName} not supported.")
    }
  }

  def validateAll[T](records: Iterator[T]): Unit = {
    @tailrec
    def validateImpl(records: Iterator[T],
                     errors: Seq[String] = Seq()): Seq[String] = {
      if (records.hasNext) {
        Try {
          validate[T](records.next())
        } match {
          case Success(_) =>
            validateImpl(records, errors)
          case Failure(e) =>
            validateImpl(records, errors :+ e.getMessage)
        }
      } else
        errors
    }

    val errors = validateImpl(records)
    if (errors.nonEmpty)
      throw new Exception(("Validation error:" +: errors).mkString("\n"))
  }

  //Do not implement it as schemaCache.get(schemaUri) otherwise one get a recursive error (not sure why...)
  protected lazy val schema: JsonSchema = {
    val reader = new StringReader(UriUtils.getContent(schemaUri))
    try {
      readerFactory.createSchemaReader(reader).read()
    } finally reader.close()
  }

  //Attention: do NOT attempt to merge schema and resolveSchemaImpl like this: 'lazy val (schema, resolveSchemaImpl)'
  //It somehow creates an infinite loop!
  private lazy val resolveSchemaImpl: URI => JsonSchema = {
    Try {
      schemaUri.toURL
    } match {
      case Success(url) if url.getProtocol != "file" =>
        id =>
          {
            val reader = new StringReader({
              val io = Source.fromURL(id.toString)
              try {
                io.mkString
              } finally io.close()
            })
            try {
              readerFactory.createSchemaReader(reader).read()
            } finally reader.close()
          }
      case _ =>
        val schemaPath = Paths.get(schemaUri.normalize())
        id =>
          {
            val path =
              schemaPath.getParent.resolve(Paths.get(id.getPath).getFileName)
            readerFactory.createSchemaReader(path).read()
          }
    }
  }
}
