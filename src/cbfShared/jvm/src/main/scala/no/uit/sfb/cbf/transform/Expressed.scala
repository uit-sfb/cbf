package no.uit.sfb.cbf.transform

import no.uit.sfb.cbf.shared.model.v1.nativ._
import no.uit.sfb.cbf.transform.resolv.{
  CachedResolutionLike,
  Resolution,
  ResolutionLike
}

import scala.concurrent.duration._
import scala.concurrent.{Await, ExecutionContext, Future}
import scala.util.Try

/**
  * Transform a record by building a meta object populated with data from each CIRIs it owns (first CIRI has higher precedence).
  * In addition `val` field is populated if it is empty and a CIRI is available.
  * @param resolvers
  * @param dbName
  * @param version
  * @param apiHost
  * @param ec
  * @param checkTaxonomy If true, we check taxonomy affiliated CIRIs
  */
class Expressed(resolvers: CachedResolutionLike,
                dbName: String,
                version: String,
                apiHost: String,
                checkTaxonomy: Boolean = true)(implicit ec: ExecutionContext) {
  def apply(vr: ValueRef, retry: Boolean = true): Future[ValueRef] = {
    val fMeta = Future.sequence(vr.ciris.reverse map { ciri =>
      resolvers.get(ciri, dbName, version, apiHost)
    }) map {
      _.flatten.toMap
    }
    fMeta flatMap { meta =>
      val value =
        if (vr.`val`.isEmpty)
          meta.getOrElse("val", "")
        else
          vr.`val`
      lazy val res = vr.copy(`val` = value, meta = meta, num = Try {
        value.toDouble
      }.toOption)
      if (checkTaxonomy && meta.getOrElse("_type", "") == "taxonomy" && retry) {
        //Because NCBI taxonomy ontology is unstable (they take the freedom to change record ids),
        //we need to double check all the lineage ciris present in the meta object (coming from the cache) which may be used later in templates.
        //In case we discover a CIRI that has become invalid, we discard the cached entry.
        Future
          .sequence(
            meta
              .collect {
                case (_, v) if v.startsWith("taxonomy:") => CompactIRI(v)
              }
              .flatten
              .flatMap { ciri =>
                resolvers.resolver(ciri).map { _ =>
                  resolvers.getOption(ciri).map { _.isEmpty }
                }
              }
          )
          .map { _.exists(_ == true) }
          .flatMap { error =>
            if (error) {
              //In case of error, we invalidate the cache and re-enter the function
              resolvers
                .invalidateCache(
                  CompactIRI(
                    meta.getOrElse("_type", ""),
                    meta.getOrElse("_key", "")
                  )
                )
                .flatMap(_ => apply(vr, false))
            } else
              Future.successful(res)
          }
      } else Future.successful(res)
    }
  }

  def apply(evidence: Evidence): Future[Evidence] = {
    val fMeta = Future.sequence(evidence.ciris.reverse map { ciri =>
      resolvers.get(ciri, dbName, version, apiHost)
    }) map {
      _.flatten.toMap
    }
    val fSrc = Future.sequence(evidence.src map {
      apply(_)
    })
    for {
      meta <- fMeta
      source <- fSrc
    } yield evidence.copy(src = source, meta = meta)
  }

  def apply(statement: Statement): Future[Statement] = {
    val fStm = Future.sequence {
      statement.obj.map {
        apply(_)
      }
    }
    val fEvi = Future.sequence {
      statement.evi.map {
        apply
      }
    }
    for {
      stmRes <- fStm
      eviRes <- fEvi
    } yield statement.copy(obj = stmRes, evi = eviRes)
  }

  def apply(record: Record): Record = {
    val f = Future.sequence(record.attributes map {
      case (k, v) =>
        apply(v) map { v =>
          Map(k -> v)
        }
    })
    val f2 = f map { attrs =>
      record.copy(attrs = attrs)
    }
    Await.result(f2, 1.minute)
  }
}
