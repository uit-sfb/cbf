package no.uit.sfb.cbf.utils.graph

import com.github.blemale.scaffeine.Scaffeine

/**
  * Oriented graph
  * Note: Supports cycles.
  *
  * @param name
  * @param nodes
  */
class OrientedGraph(nodes: Set[Node]) {
  private val cache = Scaffeine()
    .maximumSize(100)
    .build[(String, String), Option[Seq[Edge]]] { k: (String, String) =>
      computePath(k._1, k._2)
    }

  //Throws if a node with the same name already exists
  def addNode(node: Node) = {
    if (nodes.map { _.name }.contains(node.name))
      throw new Exception(s"Node ${node.name} already exists in graph.")
    else
      new OrientedGraph(nodes + node)
  }
  def get(nodeName: String): Option[Node] = {
    nodes.find(_.name == nodeName)
  }
  def getEdges(nodeName: String): Seq[Edge] = {
    get(nodeName).toSeq.flatMap {
      _.edges
    }
  }

  //node name -> edge name
  def getParents(nodeName: String): Set[(String, String)] = {
    nodes.collect {
      case n if n.edges.exists { _.target == nodeName } =>
        val edge = n.edges.find { _.target == nodeName }.get //We just checked there was one
        n.name -> edge.name
    }
  }

  def getPath(startNode: String, targetNode: String): Option[Seq[Edge]] = {
    cache.get((startNode, targetNode))
  }

  //Note: will not return an empty path in case where currentNode == targetNode.
  // Instead it will try to find a self transition or larger cycle to come back to the starting point
  def computePath(startNode: String, targetNode: String): Option[Seq[Edge]] = {
    def computePathImpl(
      endNodes: Map[String, Seq[Edge]] = Map(targetNode -> Seq()),
    ): Option[Seq[Edge]] = {
      endNodes.get(startNode) match {
        case Some(p) if p.nonEmpty =>
          Some(p)
        case _ =>
          val newMap = endNodes.flatMap {
            case (end, p) =>
              getParents(end).collect {
                case (pNode, pEdge) if !p.map { _.target }.contains(pNode) =>
                  pNode -> (Edge(pEdge, end) +: p)
              }
          }
          if (newMap.isEmpty)
            None
          else
            computePathImpl(newMap)
      }
    }

    computePathImpl()
  }

  override def toString = {
    nodes
      .map { n =>
        n.toString()
      }
      .mkString("\n")
  }
}
