package no.uit.sfb.cbf.transform.resolv

import com.typesafe.scalalogging.LazyLogging
import no.uit.sfb.cbf.mongodb.{MongodbConnect, SharedDatabase}
import no.uit.sfb.cbf.shared.config.{Constants, CustomResolver}
import no.uit.sfb.cbf.shared.model.v1.nativ.CompactIRI
import no.uit.sfb.cbf.transform.resolv.resolver.{
  EbiOlsExternalResolverLike,
  ResolverLike,
  StaticResolverLike
}
import no.uit.sfb.cbf.transform.resolv.resolver.predef._
import org.mongodb.scala.bson.conversions.Bson
import org.mongodb.scala.model.Filters

import scala.concurrent.{ExecutionContext, Future}

class Resolution(
  mgc: MongodbConnect,
  customResolvers: Seq[CustomResolver],
  sharedDbName: String = "_cbf"
)(implicit override val ec: ExecutionContext)
    extends PersistedCachedResolutionLike
    with LazyLogging {

  protected lazy val resolvers = Resolution.resolvers(customResolvers)

  protected lazy val db = SharedDatabase(mgc, sharedDbName)

  //searchString should typically NOT be valid CIRI
  //Search in key/type and text (val / synonyms)
  protected def searchFilter(searchString: String): Bson =
    Filters.or(
      Filters.eq("_id", searchString),
      Filters.eq("_key", searchString),
      Filters.eq("_type", searchString),
      Filters.text(searchString)
    )

  //The search is only for persisted CIRIs as there is no way to know for un-persisted CIRIs!
  //The search is case insensitive
  //Use "this is a phrase" to search for a phrase (only works with the text search)
  //Prefix with - to negate (only works with the text search)
  //Note: We are searching for an input string, so an empty string will result in an empty set
  def search(searchString: String,
             offset: Option[Int] = None,
             size: Option[Int] = None): Future[Set[CompactIRI]] = {
    if (searchString.replace("\"", "").isEmpty)
      Future.successful(Set())
    else
      db.ciris
        .map { ciris =>
          val it = {
            ciris
              .getIt(
                skip = offset,
                limit = size,
                filter = searchFilter(searchString)
              )
          }.map {
              _("_id")
            }
            .to(LazyList)
            .distinct
            .iterator
            .flatMap { ciri =>
              val o = CompactIRI(ciri)
              if (o.isEmpty)
                logger.info(
                  s"Non-valid CIRI '$ciri' found in ciriResolution collection."
                )
              o
            }
          (size match {
            case Some(s) =>
              it.take(s)
            case None =>
              it
          }).toSet
        }
  }
}

object Resolution extends LazyLogging {
  //Add here all the custom resolvers
  def resolvers(customResolvers: Seq[CustomResolver]): Set[ResolverLike] = {
    //customResolvers is the list of resolvers found in the dataset configuration
    customResolvers.toSet.map { resolv: CustomResolver =>
      resolv.prefix match {
        case "biotools" =>
          new Biotools with StaticResolverLike {
            lazy val ct = resolv
          }
        case "erc" =>
          new ERC {}
        case "gaz" =>
          new GAZ with StaticResolverLike {
            lazy val ct = resolv
            lazy val baseUri = resolv.ebiOlsBaseUri.get
          }
        case "gmap" =>
          new Gmap {}
        case "mrgid" =>
          new Mrgid {}
        case "rrid" =>
          new RRID with StaticResolverLike {
            lazy val ct = resolv
          }
        case "taxonomy" =>
          new Taxonomy with StaticResolverLike {
            lazy val ct = resolv
          }
        case _ =>
          if (resolv.isEbiOls)
            new EbiOlsExternalResolverLike with StaticResolverLike {
              lazy val ct = resolv
              lazy val baseUri = resolv.ebiOlsBaseUri.get
            } else
            new StaticResolverLike {
              lazy val ct = resolv
            }
      }
    }
  }
}
