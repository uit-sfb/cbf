package no.uit.sfb.cbf.transform.resolv.resolver

import no.uit.sfb.cbf.shared.config.CustomResolver

import scala.util.matching.Regex

/**
  * Static part of the resolver (adding information without accessing an external database)
  */
trait StaticResolverLike extends ResolverLike {
  def ct: CustomResolver
  val prefix = ct.prefix
  override lazy val regexp: Regex =
    if (ct.regexp.trim.nonEmpty) ct.regexp.trim.r else super.regexp

  override def metaStatic(id: String,
                          dbName: String,
                          version: String,
                          apiHost: String,
                          groups: Array[String]): Map[String, String] = {
    def expand(pattern: String): Option[String] = {
      if (pattern.nonEmpty) {
        val tmp = replace(pattern)(id, dbName, version, apiHost)
        Some(groups.indices.foldLeft(tmp) {
          case (acc, idx) => acc.replace(s"""{${idx + 1}}""", groups(idx))
        })
      } else
        None
    }

    val extra = ct.kvp.collect {
      case (k, pattern) if !k.startsWith("+") =>
        expand(pattern.trim).map {
          k -> _
        }
    }
    super.metaStatic(id, dbName, version, apiHost, groups) ++ extra.flatten
  }
}
