package no.uit.sfb.cbf.transform.resolv.resolver.predef

import io.circe.parser.parse

import scala.concurrent.Await
import scala.concurrent.duration.DurationInt
import no.uit.sfb.cbf.transform.resolv.resolver._

trait GAZ extends EbiOlsExternalResolverLike {
  protected def findParent(hierarchicalAncestors: Option[String], method: String => Option[String]): Option[String] = {
    hierarchicalAncestors
      .map {
        getData
      }
      .map {
        Await.result(_, 45.seconds)
      }
      .flatMap {
        str =>
          val json = parse(str).toTry.get.hcursor
            val terms = json.downField("_embedded").downField("terms")
            terms.values
      } flatMap { terms =>
      terms
        .flatMap {
          _.hcursor.downField("obo_id").as[String].toOption
        }.flatMap {
        method(_)
      }
        .headOption
    }
  }


  private def searchIn(ciri: String, data: Map[String, Set[String]]): Option[String] = {
    data.find {
      case (region, subRegions) =>
        (region + subRegions).contains(ciri)
    } map {
      _._1
    }
  }

  protected def country(ciri: String): Option[String] = {
    searchIn(ciri, GAZ.countries)
  }

  protected def worldRegion(ciri: String): Option[String] = {
    searchIn(ciri, GAZ.worldRegions)
  }

  override protected def parseData(str: String): Map[String, String] = {
    val overrides = Map(
      "GAZ:00002637" -> "UK",
      "GAZ:00002459" -> "USA",
      "GAZ:00002940" -> "France",
      "GAZ:00002635" -> "Denmark",
      "GAZ:00002699" -> "Norway",
      "GAZ:00000591" -> "Spain",
      "GAZ:00001549" -> "Netherlands",
      "GAZ:00002944" -> "Portugal",
      "GAZ:00005341" -> "Taiwan"
    )
    val json = parse(str).toTry.get.hcursor
        val term = json.downField("_embedded").downField("terms").downArray
        val overrideVal = term.downField("obo_id").as[String].toOption.flatMap {
          overrides.get
        }
        val origVal = term.downField("label").as[String].toOption.map {
          _.capitalize
        }
        val origSynonyms: Set[String] = {
          //Looks like GAZ has glitches and sometimes looses the 's'...
          Seq("synonym", "synonyms")
            .flatMap { t =>
              term
                .downField(t)
                .as[Seq[String]]
                .toOption
                .getOrElse(Seq())
                .map{
                  _.split(" \\{").headOption
                    .map {
                      _.capitalize
                    }
                }
            }
            .foldLeft(Seq[String]()) { case (acc, s) => acc ++ s }.toSet
        }
        val synonyms: Option[Set[String]] = {
          val set: Set[String] = overrideVal match {
            case Some(overrideV) =>
              (origVal match {
                case Some(ov) => Set(ov)
                case None => Set()
              }) ++ origSynonyms -- Set(overrideV)
            case None =>
              origSynonyms
          }
          if (set.isEmpty)
            None
          else
            Some(set)
        }
        Map(
          "val" -> overrideVal.orElse(origVal),
          /*"description" -> term
            .downField("description")
            .downArray
            .as[String]
            .toOption,*/
          "synonyms" -> synonyms.map {
            _.mkString("|")
          },
          "country" -> {
            val id = term.downField("obo_id").as[String].toOption.getOrElse("")
            country(id).orElse {
              findParent(term
                .downField("_links")
                .downField("hierarchicalAncestors")
                .downField("href")
                .as[String]
                .toOption,
                country
              )
            }.map {
              _.toLowerCase
            }
          },
          "world_region" -> {
            val id = term.downField("obo_id").as[String].toOption.getOrElse("")
            worldRegion(id).orElse {
              findParent(term
                .downField("_links")
                .downField("hierarchicalAncestors")
                .downField("href")
                .as[String]
                .toOption,
                worldRegion)
            }.map {
              _.toLowerCase
            }
          }
        ).collect { case (k, Some(v)) => k -> v }
  }
}

object GAZ {
  val countries: Map[String, Set[String]] =
    Map(
      "GAZ:00006882" -> Set(), //Afghanistan
      "GAZ:00005282" -> Set(), //United Arab Emirates
      "GAZ:00002953" -> Set(), //Albania
      "GAZ:00000563" -> Set(), //Algeria
      "GAZ:00002948" -> Set(), //Andorra
      "GAZ:00001095" -> Set(), //Angola
      "GAZ:00000462" -> Set(), //Antarctica
      "GAZ:00006883" -> Set(), //Antigua and Barbuda
      "GAZ:00002928" -> Set(), //Argentina
      "GAZ:00004094" -> Set(), //Armenia
      "GAZ:00000463" -> Set(), //Australia
      "GAZ:00002942" -> Set(), //Austria
      "GAZ:00004941" -> Set(), //Azerbaijan
      "GAZ:00002733" -> Set(), //The Bahamas
      "GAZ:00005281" -> Set(), //Bahrain
      "GAZ:00003750" -> Set(), //Bangladesh
      "GAZ:00001251" -> Set(), //Barbados
      "GAZ:00005810" -> Set(), //Ile Bassas da India
      "GAZ:00006886" -> Set(), //Belarus
      "GAZ:00002938" -> Set(), //Belgium
      "GAZ:00002934" -> Set(), //Belize
      "GAZ:00000904" -> Set(), //Benin
      "GAZ:00003920" -> Set(), //Bhutan
      "GAZ:00002511" -> Set(), //Bolivia
      "GAZ:00006887" -> Set(), //Bosnia and Herzegovina
      "GAZ:00001097" -> Set(), //Botswana
      "GAZ:00002828" -> Set(), //Brazil
      "GAZ:00003901" -> Set(), //Brunei
      "GAZ:00002950" -> Set(), //Bulgaria
      "GAZ:00000905" -> Set(), //Burkina Faso
      "GAZ:00001090" -> Set(), //Burundi
      "GAZ:00006888" -> Set(), //Cambodia
      "GAZ:00001093" -> Set(), //Cameroon
      "GAZ:00002560" -> Set(), //Canada
      "GAZ:00001227" -> Set(), //Cape Verde
      "GAZ:00001089" -> Set(), //Central African Republic
      "GAZ:00000586" -> Set(), //Chad
      "GAZ:00002825" -> Set(), //Chile
      "GAZ:00002845" -> Set( //China
        "GAZ:00003203", //Hong Kong
        "GAZ:00004219" //Tibet
      ),
      "GAZ:00002929" -> Set(), //Colombia
      "GAZ:00005820" -> Set(), //Comoros
      "GAZ:00001088" -> Set(), //Republic of Congo
      "GAZ:00001086" -> Set(), //Democratic Republic of Congo
      "GAZ:00002901" -> Set(), //Costa Rica
      "GAZ:00000906" -> Set(), //Côte d'Ivoire
      "GAZ:00002719" -> Set(), //Croatia
      "GAZ:00003762" -> Set(), //Cuba
      "GAZ:00004007" -> Set(), //Cyprus
      "GAZ:00002954" -> Set(), //Czech Republic
      "GAZ:00002635" -> Set( //Denmark
        "GAZ:00001507" //Greenland
      ),
      "GAZ:00000582" -> Set(), //Djibouti
      "GAZ:00006890" -> Set(), //Dominica
      "GAZ:00003952" -> Set(), //Dominican Republic
      "GAZ:00002912" -> Set(), //Ecuador
      "GAZ:00003934" -> Set(), //Egypt
      "GAZ:00002935" -> Set(), //El Salvado
      "GAZ:00001091" -> Set(), //Equatorial Guinea
      "GAZ:00000581" -> Set(), //Eritrea
      "GAZ:00002959" -> Set(), //Estonia
      "GAZ:00000567" -> Set(), //Ethiopia
      "GAZ:00006891" -> Set(), //Fiji
      "GAZ:00002937" -> Set(), //Finland
      "GAZ:00002940" -> Set( //France
        "GAZ:00003941", //Collectivite d'outre-mer
        "GAZ:00003753" //Terres australes et antarctiques francaises
      ),
      "GAZ:00001092" -> Set(), //Gabon
      "GAZ:00002475" -> Set(), //Palestinian Territories
      "GAZ:00004942" -> Set(), //Georgia
      "GAZ:00002646" -> Set(), //Germany
      "GAZ:00000908" -> Set(), //Ghana
      "GAZ:00002945" -> Set(), //Greece
      "GAZ:02000573" -> Set(), //Grenada
      "GAZ:00002936" -> Set(), //Guatemala
      "GAZ:00000909" -> Set(), //Guinea
      "GAZ:00000910" -> Set(), //Guinea-Bissau
      "GAZ:00002522" -> Set(), //Guyana
      "GAZ:00003953" -> Set(), //Haiti
      "GAZ:00003103" -> Set(), //Vatican
      "GAZ:00002894" -> Set(), //Honduras
      "GAZ:00002952" -> Set(), //Hungary
      "GAZ:00000843" -> Set(), //Iceland
      "GAZ:00002839" -> Set(), //India
      "GAZ:00003727" -> Set(), //Indonesia
      "GAZ:00004474" -> Set(), //Iran
      "GAZ:00004483" -> Set(), //Iraq
      "GAZ:00002943" -> Set(), //Republic of Ireland
      "GAZ:00002476" -> Set(), //Israel
      "GAZ:00002650" -> Set(), //Italy
      "GAZ:00003781" -> Set(), //Jamaica
      "GAZ:00002747" -> Set(), //Japan
      "GAZ:00002473" -> Set(), //Jordan
      "GAZ:00004999" -> Set(), //Kazakhstan
      "GAZ:00001101" -> Set(), //Kenya
      "GAZ:00006894" -> Set(), //Kiribati
      "GAZ:00002801" -> Set(), //North Korea
      "GAZ:00002802" -> Set(), //South Korea
      "GAZ:00011337" -> Set(), //Kosovo
      "GAZ:00005285" -> Set(), //Kuwait
      "GAZ:00006893" -> Set(), //Kyrgyzstan
      "GAZ:00006889" -> Set(), //Laos
      "GAZ:00002958" -> Set(), //Latvia
      "GAZ:00002478" -> Set(), //Lebanon
      "GAZ:00001098" -> Set(), //Lesotho
      "GAZ:00000911" -> Set(), //Liberia
      "GAZ:00000566" -> Set(), //Libya
      "GAZ:00003858" -> Set(), //Liechtenstein
      "GAZ:00002960" -> Set(), //Lithuania
      "GAZ:00002947" -> Set(), //Luxembourg
      "GAZ:00001108" -> Set(), //Madagascar
      "GAZ:00001105" -> Set(), //Malawi
      "GAZ:00003902" -> Set(), //Malaysia
      "GAZ:00006896" -> Set(), //Republic of the Maldives
      "GAZ:00000584" -> Set(), //Mali
      "GAZ:00004017" -> Set(), //Malta
      "GAZ:00006470" -> Set(), //Republic of the Marshall Islands
      "GAZ:00000583" -> Set(), //Mauritania
      "GAZ:00003745" -> Set(), //Mauritius
      "GAZ:00002852" -> Set(), //Mexico
      "GAZ:00006897" -> Set(), //Federated States of Micronesia
      "GAZ:00003897" -> Set(), //Moldova
      "GAZ:00003857" -> Set(), //Monaco
      "GAZ:00008744" -> Set(), //Mongolia
      "GAZ:00006898" -> Set(), //Montenegro
      "GAZ:00000565" -> Set(), //Morocco
      "GAZ:00001100" -> Set(), //Mozambique
      "GAZ:00006899" -> Set(), //Myanmar
      "GAZ:00001096" -> Set(), //Namibia
      "GAZ:00006900" -> Set(), //Nauru
      "GAZ:00004399" -> Set(), //Nepal
      "GAZ:00001549" -> Set(), //Netherlands
      "GAZ:00000469" -> Set(), //New Zealand
      "GAZ:00002978" -> Set(), //Nicaragua
      "GAZ:00000585" -> Set(), //Niger
      "GAZ:00000912" -> Set(), //Nigeria
      "GAZ:00006902" -> Set(), //Niue
      "GAZ:00006895" -> Set(), //Macedonia
      "GAZ:00002699" -> Set( //Norway
        "GAZ:00005396", //Svalbard
        "GAZ:00005853" //Jan Mayen
      ),
      "GAZ:00005283" -> Set(), //Oman
      "GAZ:00005246" -> Set(), //Pakistan
      "GAZ:00006905" -> Set(), //Palau
      "GAZ:00002892" -> Set(), //Panama
      "GAZ:00003922" -> Set(), //Papua New Guinea
      "GAZ:00002933" -> Set(), //Paraguay
      "GAZ:00002932" -> Set(), //Peru
      "GAZ:00004525" -> Set(), //Philippines
      "GAZ:00002939" -> Set(), //Poland
      "GAZ:00002944" -> Set(), //Portugal
      "GAZ:00002822" -> Set(), //Puerto Rico
      "GAZ:00005286" -> Set(), //Qatar
      "GAZ:00002951" -> Set(), //Romania
      "GAZ:00002721" -> Set(), //Russia
      "GAZ:00001087" -> Set(), //Rwanda
      "GAZ:00006909" -> Set(), //Saint Lucia
      "GAZ:02000565" -> Set(), //Saint Vincent and the Grenadines
      "GAZ:00006910" -> Set(), //Samoa
      "GAZ:00003102" -> Set(), //San Marino
      "GAZ:00006927" -> Set(), //São Tomé and Principe
      "GAZ:00005279" -> Set(), //Saudi Arabia
      "GAZ:00000913" -> Set(), //Senegal
      "GAZ:00002957" -> Set(), //Serbia
      "GAZ:00005821" -> Set(), //Seychelles
      "GAZ:00000914" -> Set(), //Sierra Leone
      "GAZ:00003923" -> Set(), //Singapore
      "GAZ:00002956" -> Set(), //Slovak Republic
      "GAZ:00002955" -> Set(), //Slovenia
      "GAZ:00005275" -> Set(), //Solomon Islands
      "GAZ:00001104" -> Set(), //Somalia
      "GAZ:00001094" -> Set(), //Republic of South Africa
      "GAZ:00233439" -> Set(), //South Sudan
      "GAZ:00000591" -> Set(), //Spain
      "GAZ:00003924" -> Set(), //Sri Lanka
      "GAZ:00000560" -> Set(), //Sudan
      "GAZ:00002525" -> Set(), //Suriname
      "GAZ:00001099" -> Set(), //Swaziland
      "GAZ:00002729" -> Set(), //Sweden
      "GAZ:00002941" -> Set(), //Switzerland
      "GAZ:00002474" -> Set(), //Syria
      "GAZ:00006912" -> Set(), //Tajikistan
      "GAZ:00001103" -> Set(), //Tanzania
      "GAZ:00003744" -> Set(), //Thailand
      "GAZ:00000907" -> Set(), //Gambia
      "GAZ:00006913" -> Set(), //Timor-Leste
      "GAZ:00000915" -> Set(), //Togo
      "GAZ:00006916" -> Set(), //Tonga
      "GAZ:00003767" -> Set(), //Trinidad and Tobago
      "GAZ:00000562" -> Set(), //Tunisia
      "GAZ:00000558" -> Set(), //Turkey
      "GAZ:00005018" -> Set(), //Turkmenistan
      "GAZ:00009715" -> Set(), //Tuvalu
      "GAZ:00001102" -> Set(), //Uganda
      "GAZ:00002724" -> Set(), //Ukraine
      "GAZ:00002637" -> Set( //UK
        "GAZ:00003981",
        "GAZ:00001540",
        "GAZ:00001539",
      ),
      "GAZ:00002459" -> Set(), //USA
      "GAZ:00002930" -> Set(), //Uruguay
      "GAZ:00004979" -> Set(), //Uzbekistan
      "GAZ:00006918" -> Set(), //Vanuatu
      "GAZ:00002931" -> Set(), //Venezuela
      "GAZ:00003756" -> Set(), //Vietnam
      "GAZ:00005284" -> Set(), //Yemen
      "GAZ:00001107" -> Set(), //Zambia
      "GAZ:00001106" -> Set(), //Zimbabwe
      "GAZ:00005341" -> Set(), //Taiwan
    )

  val worldRegions: Map[String, Set[String]] = Map(
    "GAZ:00000457" -> Set( //Africa
      "GAZ:00003934" //Egypt
    ),
    "GAZ:00000062" -> Set(), //Antarctica
    "GAZ:00000458" -> Set( //N America
      "GAZ:00002459", //USA
      "GAZ:00002820", //Carabean
    ),
    "GAZ:00000459" -> Set(), //S America
    "GAZ:00000464" -> Set( //Europe
      "GAZ:00008171", //Black sea
      "GAZ:00051074", //European seas
      "GAZ:00002940", //France
      "GAZ:00002635", //Denmark
      "GAZ:00002699", //Norway
      "GAZ:00000591", //Spain
      "GAZ:00001549", //Netherlands
      "GAZ:00002944", //Portugal
    ),
    "GAZ:00000465" -> Set( //Asia
      "GAZ:00002469", //Caucasus
      "GAZ:00002721", //Russia
      "GAZ:00000558", //Turkey
      "GAZ:00051031", //Australasia sea
    ),
    "GAZ:00000468" -> Set(), //Oceania
    "GAZ:00051067" -> Set(), //Ocean arctic
    "GAZ:00051071" -> Set(), //Ocean atlantic
    "GAZ:00051084" -> Set(), //Indian ocean
    "GAZ:00051073" -> Set(), //Pacific ocean
    "GAZ:00000373" -> Set(), //Southern ocean
  )
}
