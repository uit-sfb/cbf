package no.uit.sfb.cbf.jsonld

case class Evidence(`@type`: String = "CategoryCode",
                    codeValue: String,
                    inCodeSet: Option[String])
    extends JsonLdLike
