package no.uit.sfb.cbf.bulktransform

import io.circe.jawn.decode
import io.circe.generic.extras.Configuration
import no.uit.sfb.cbf.mongodb.Collection
import no.uit.sfb.cbf.shared.utils.Tsv
import no.uit.sfb.cbf.shared.utils.context.ErrorAnnotation
import org.mongodb.scala.bson.Document

object ErrorUtils {
  def generateErrorTsv(col: Collection[Document]): Iterator[String] = {
    import io.circe.generic.extras.auto._
    implicit val customConfig: Configuration =
      Configuration.default.withDefaults
    import scala.jdk.CollectionConverters._
    def parse(doc: Document): (String, String) = {
      val (header, lines) = Tsv.parse(doc.getString("source"))
      val (cHeader, cLines) = if (header.contains("_error:annotations")) {
        val cleanHeader = header.filter { _ != "_error:annotations" }
        val idx = header.indexWhere(_ == "_error:annotations")
        val cleanLines = lines.map {
          _.zipWithIndex.filter { case (_, i) => i != idx }.map { _._1 }
        }
        cleanHeader -> cleanLines
      } else {
        header -> lines
      }
      val extendedHeader =
        ("_error:annotations" +: cHeader).mkString("\t")
      val annotations: Seq[ErrorAnnotation] =
        doc
          .get("annotations")
          .map { t =>
            t.asArray()
              .getValues
              .asScala
              .toSeq
              .flatMap { v =>
                decode[ErrorAnnotation](v.asDocument().toJson()).toOption
              }
          }
          .getOrElse(Seq())
      val stage = doc.getString("stage")
      val extendedLine = (
        s"$stage: ${annotations.map { _.asString.replace('\n', ' ') }.mkString(" | ")}" +: cLines.headOption
          .getOrElse(Seq())
      ).mkString("\t")
      extendedHeader -> extendedLine
    }
    val it = col.getIt()
    val (header, first) = parse(it.next())
    Iterator(header, first) ++ it.map { doc =>
      parse(doc)._2
    }
  }
}
