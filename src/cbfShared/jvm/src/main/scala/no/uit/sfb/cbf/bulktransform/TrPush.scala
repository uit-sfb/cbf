package no.uit.sfb.cbf.bulktransform

import no.uit.sfb.cbf.mongodb.{CbfDataset, Collection, MongodbConnect}
import no.uit.sfb.cbf.shared.config.DatasetConfig
import no.uit.sfb.cbf.shared.model.v1.builder.RecordBuilder
import no.uit.sfb.cbf.shared.model.v1.nativ.Record
import no.uit.sfb.cbf.transform._
import no.uit.sfb.cbf.transform.resolv.Resolution
import org.mongodb.scala.bson.{Document, ObjectId}

import java.net.URI
import scala.collection.immutable.ListMap
import scala.collection.parallel.ParIterable
import scala.concurrent.duration.DurationInt
import scala.concurrent.{Await, ExecutionContext, Future}

class TrPush(mgc: MongodbConnect,
             dbName: String,
             version: String,
             inputIt: Iterator[String],
             idProvider: => String,
             force: Boolean = false)(implicit ec: ExecutionContext)
    extends TransformExec {
  protected implicit val db = new CbfDataset(mgc.db(dbName))
  protected val datasetConfig = db.config().get
  protected implicit val templator =
    new Templated(datasetConfig.persistedAttributes, idProvider)
  protected lazy val resolvers =
    new Resolution(mgc, datasetConfig.customResolvers)
  protected lazy val enhancer =
    new Enhanced(datasetConfig.persistedAttributes, resolvers)
  protected implicit val rb: RecordBuilder =
    new RecordBuilder(
      datasetConfig.persistedAttributes,
      datasetConfig.valueSeparatorChar,
      datasetConfig.subValueSeparators
    )
  protected val cbfVersion = db
    .parseVersion(version)
  Await.result(db.version(cbfVersion.ver, false).map {
    res =>
      if (res.nonEmpty) {
        if (force)
          logger.warn(
            s"Dataset '$dbName' version '${cbfVersion.ver}' has already been released. Action will proceed due to --force flag."
          )
        else
          throw new Exception(
            s"Dataset '$dbName' version '${cbfVersion.ver}' has already been released and may therefore not be modified."
          )
      }
  }, 15.seconds)
  protected lazy val expressor =
    new Expressed(resolvers, dbName, cbfVersion.ver, "{api}")
  protected lazy val expressor2 =
    new Expressed(
      resolvers,
      dbName,
      cbfVersion.ver,
      "{api}",
      checkTaxonomy = false
    )
  protected implicit val col: Collection[Document] = db.recordsCol(cbfVersion)
  protected val versions = Await.result(db.versions(false), 1.minute)
  protected val schemaUri = (if (datasetConfig.schema.isEmpty)
                               Some(
                                 DatasetConfig.getClass
                                   .getResource("/schema/record.schema.json")
                                   .toString
                               )
                             else Some(datasetConfig.schema))
    .map { new URI(_) }
  protected lazy val stamper = new Stamped(cbfVersion.ver, col)
  protected val validator = new Validated(schemaUri, datasetConfig)
  protected lazy val (ctx, it) = {
    val (c, i) = rb.fromTsv(inputIt)
    c -> i.map {
      _.map {
        case (rec, items) =>
          Record.toDelete(rec) -> items
      }
    }
  }
  protected lazy val transforms: ListMap[String, Record => Record] =
    ListMap(
      "enhancing" -> (enhancer(_)), //Note: The extra parenthesis are necessary
      "expressing" -> (expressor(_)), //Needed for templating
      "templating" -> (templator(_)),
      "stamping" -> (stamper(_)),
      "re-enhancing" -> (enhancer(_)), //Needed after templating
      "re-expressing" -> (expressor2(_)),
      "validating" -> (validator(_)), //Need to validate expressed record otherwise validation may fail when a value is a CIRI
      "un-expressing" -> (Unexpressed(_))
    )

  //Returns IDs submitted in this batch
  protected def consumeRecords(
    batch: ParIterable[Record]
  ): Future[Seq[ObjectId]] = {
    Transform
      .pushRecords(cbfVersion, col, rb, versions.map { _._id })(batch)
  }
}
