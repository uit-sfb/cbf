package no.uit.sfb.cbf.mongodb

import no.uit.sfb.cbf.shared.dataset.DatasetRegistration
import org.bson.codecs.configuration.CodecRegistries.{
  fromProviders,
  fromRegistries
}
import org.mongodb.scala.bson.codecs.Macros._
import org.mongodb.scala.MongoClient.DEFAULT_CODEC_REGISTRY
import org.mongodb.scala.{Document, MongoDatabase}

import scala.collection.immutable.ListMap
import scala.concurrent.{ExecutionContext, Future}

class SharedDatabase(protected val _db: MongoDatabase)(
  implicit protected val _ec: ExecutionContext
) extends Database(_db)(_ec) {
  protected val codecRegistry = fromRegistries(
    fromProviders(classOf[DatasetRegistration]),
    DEFAULT_CODEC_REGISTRY
  )

  lazy val ciris: Future[Collection[Map[String, String]]] = {
    val col = new Collection[Map[String, String]](
      db.getCollection[Map[String, String]]("ciriResolution")
        .withCodecRegistry(codecRegistry)
    )
    val f1 = col
      .createCompoundIndex( //Weird that we have to use a compound index to create a text index
        ListMap("val" -> Collection.Text, "synonyms" -> Collection.Text)
      )
    val f2 = col
      .createIndexes(
        Seq(
          IndexDef("_key", Collection.Asc),
          IndexDef("_type", Collection.Asc),
        )
      )
    Future.sequence(Seq(f1, f2)) map { _ =>
      col
    }
  }

  lazy val admin: Collection[Document] = {
    new Collection[Document](db.getCollection[Document]("admin"))
  }

  lazy val datasets: Collection[DatasetRegistration] = {
    new Collection[DatasetRegistration](
      db.getCollection[DatasetRegistration]("datasets")
        .withCodecRegistry(codecRegistry)
    )
  }
}

object SharedDatabase {
  def apply(mgc: MongodbConnect,
            colName: String = "_cbf")(implicit _ec: ExecutionContext) = {
    new SharedDatabase(mgc.db(colName))
  }
}
