package no.uit.sfb.cbf.jsonld

case class IsPartOf(`@type`: String = "Dataset",
                    override val `@id`: Option[String] = Some("sfb_sars_cov-2"))
    extends JsonLdLike
