package no.uit.sfb.cbf.jsonld

trait JsonLdLike {
  def `@type`: String
  def `@id`: Option[String] = None
}
