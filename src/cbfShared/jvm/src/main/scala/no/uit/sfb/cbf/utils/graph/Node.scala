package no.uit.sfb.cbf.utils.graph

case class Node(name: String, edges: Set[Edge]) {
  def addEdge(edge: Edge) = Node(name, edges + edge)

  override def toString = {
    edges
      .map { e =>
        s"$name -> $e"
      }
      .mkString("\n")
  }
}
