package no.uit.sfb.cbf.mongodb

import org.mongodb.scala.MongoDatabase
import org.mongodb.scala.bson.Document
import org.mongodb.scala.bson.conversions.Bson

import scala.concurrent.{ExecutionContext, Future, blocking}

class Database(protected val db: MongoDatabase)(
  implicit val ec: ExecutionContext
) {

  def collection(colName: String): Collection[Document] = {
    new Collection(db.getCollection(colName))
  }

  def listCollections(): Future[Seq[String]] = blocking {
    db.listCollectionNames().toFuture()
  }

  def drop(): Future[Any] = blocking {
    db.drop().toFuture()
  }

  /**
    *
    * @param name     Name of the view
    * @param viewOn   Backing collection/view
    * @param pipeline Pipeline Build with org.mongodb.scala.model.Aggregates???
    */
  def createView(name: String,
                 viewOn: String,
                 pipeline: Seq[Bson]): Future[Any] = blocking {
    db.createView(name, viewOn, pipeline).toFuture()
  }

  //createCollection: create collection with parameters such as
  //    - validation (https://docs.mongodb.com/manual/core/schema-validation/)
  //    - capped
  //    - maxDocuments
}
