package no.uit.sfb.cbf.bulktransform

import no.uit.sfb.cbf.shared.utils.context.ErrorObject
import org.mongodb.scala.bson.Document
import org.mongodb.scala.model.InsertOneModel

import scala.collection.parallel.ParIterable
import scala.concurrent.{ExecutionContext, Future}
import io.circe.syntax._
import io.circe.generic.auto._

trait TransformPushErrorsLike {
  this: TrPush =>

  override protected def consumeErrors(
    batch: ParIterable[ErrorObject]
  )(implicit ec: ExecutionContext): Future[Seq[String]] = {
    def toDoc(errObj: ErrorObject) = {
      val extendedErrObject = errObj.withExtendedAnnotations
      logger.warn(extendedErrObject.asString)
      val json = extendedErrObject.asJson
      InsertOneModel[Document](Document(json.noSpaces))
    }
    val errors = batch.map { err =>
      val trimmedMessages = err.copy(annotations = err.annotations.map {
        annot =>
          annot.copy(
            message =
              if (annot.message.contains("due to:"))
                annot.message
                  .split("due to:")
                  .toSeq
                  .tail
                  .mkString("due to:")
              else annot.message
          )
      })
      toDoc(trimmedMessages)
    }
    db.errorsCol(cbfVersion).bulkWrite(errors.seq.toSeq).map { _ =>
      Seq()
    }
  }
}
