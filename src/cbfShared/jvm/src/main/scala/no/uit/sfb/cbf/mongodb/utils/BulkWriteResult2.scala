package no.uit.sfb.cbf.mongodb.utils

import org.mongodb.scala.BulkWriteResult

class BulkWriteResult2(bwr: BulkWriteResult) extends BulkWriteResult {
  def getUpsertedCount(): Int = {
    bwr.getUpserts.size
  }
  def getDeletedCount(): Int = bwr.getDeletedCount
  def getInsertedCount(): Int = bwr.getInsertedCount
  def getInserts(): java.util.List[com.mongodb.bulk.BulkWriteInsert] =
    bwr.getInserts
  def getMatchedCount(): Int = bwr.getMatchedCount
  def getModifiedCount(): Int = bwr.getModifiedCount
  def getUpserts(): java.util.List[com.mongodb.bulk.BulkWriteUpsert] =
    bwr.getUpserts
  def wasAcknowledged(): Boolean = bwr.wasAcknowledged()

  override def toString(): String = {
    if (wasAcknowledged())
      s"""AcknowledgedBulkWriteResult{
         |  insertedCount=${getInsertedCount()}
         |  upsertedCount=${getUpsertedCount()}
         |  matchedCount=${getMatchedCount()}
         |  removedCount=${getDeletedCount()}
         |  modifiedCount=${getModifiedCount()}
         |}""".stripMargin
    else
      "UnacknowledgedBulkWriteResult{}"
  }
}
