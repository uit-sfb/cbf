package no.uit.sfb.cbf.transform

import no.uit.sfb.cbf.shared.config.AttributeDef
import no.uit.sfb.cbf.shared.model.v1.nativ.{Record, Statement}

/**
  * Apply templates on a record.
  */
class Templated(persistedAttributes: Seq[AttributeDef], idProvider: => String) {
  def apply(expressedRecord: Record): Record = {
    expressedRecord
      .copy(attrs = persistedAttributes.flatMap { attr =>
        val key = attr.ref
        (attr.template match {
          case Some(template) if template.nonEmpty =>
            //If a template is defined, we discard any entries
            val raw = expressedRecord.resolveTemplate(template)
            //If the template would yield an ECO without data, we delete it
            raw.flatMap { s =>
              if (s.startsWith("<"))
                None
              else
                Some(Statement(s))
            }
          case _ =>
            //we handle the special case of the id attribute
            if (key == "id") {
              val stm = expressedRecord.stm(key).headOption.toVector
              if (attr.automatic && (stm.isEmpty || stm.head.isEmpty))
                Vector(Statement(idProvider))
              else
                stm
            } else
              expressedRecord.stm(key)
        }).map { stm =>
          Map(key -> stm)
        }
      }.toVector)
  }
}
