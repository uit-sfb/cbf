package no.uit.sfb.cbf.jsonld

case class CustomEvidence(`@type`: String = "custom:evidenceType",
                          evidence: Evidence,
                          source: Set[String])
    extends JsonLdLike
