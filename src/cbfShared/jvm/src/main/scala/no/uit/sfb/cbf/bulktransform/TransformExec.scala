package no.uit.sfb.cbf.bulktransform

import com.typesafe.scalalogging.LazyLogging
import no.uit.sfb.cbf.shared.model.v1.nativ.Record
import no.uit.sfb.cbf.shared.utils.context.{
  ErrorAnnotation,
  ErrorObject,
  ParsingContext
}
import org.mongodb.scala.bson.ObjectId

import scala.collection.immutable.ListMap
import scala.collection.parallel.ParIterable
import scala.concurrent.{Await, ExecutionContext, Future}
import scala.util.Try
import scala.collection.parallel.CollectionConverters._
import scala.concurrent.duration._

trait TransformExec extends LazyLogging {
  protected def bulkTransform(
    batch: ParIterable[Either[ErrorObject, (Record, Seq[String])]],
    transform: Record => Record,
    stage: String,
    ctx: ParsingContext
  ): ParIterable[Either[ErrorObject, (Record, Seq[String])]] = {
    batch map {
      case Right((rec, items)) =>
        if (rec.toBeDeleted)
          Right((rec, items))
        else {
          Try {
            transform(rec)
          }.toEither match {
            case Left(e) =>
              println(e.getMessage)
              Left(
                ErrorObject(
                  stage,
                  ctx.source(items),
                  Some(rec),
                  Seq(ErrorAnnotation(e.getMessage))
                )
              )
            case Right(rec) => Right((rec, items))
          }
        }
      case left =>
        left
    }
  }

  protected def incrNumProcessed(): Unit = ()

  protected def it: Iterator[Either[ErrorObject, (Record, Seq[String])]]
  protected def ctx: ParsingContext

  protected def transforms: ListMap[String, Record => Record]

  //Return list of submitted record id
  protected def consumeRecords(
    batch: ParIterable[Record]
  ): Future[Seq[ObjectId]]

  protected def grpSize =
    100 //Higher values increases mem consumption without increasing speed

  protected def consumeErrors(
    batch: ParIterable[ErrorObject]
  )(implicit ec: ExecutionContext): Future[Seq[String]] = {
    Future.successful(
      batch
        .map { _.asString }
        .iterator
        .toSeq
    )
  }

  protected def pre(): Unit =
    logger.info("Starting to parse input, validate and write all records...")

  protected def afterBatch(): Unit = {
    print("+")
    Console.flush()
  }

  protected def post(): Unit = {
    logger.info("Finished writing records to Mongodb.")
  }

  //May return a list of error messages depending on how consumeErrors is implemented and the list of submitted record id
  def run()(implicit ec: ExecutionContext): (Seq[String], Seq[ObjectId]) = {
    val batchedIt = it.grouped(grpSize).map { b =>
      val batch: ParIterable[Either[ErrorObject, (Record, Seq[String])]] = b.par
      val transformed = transforms.foldLeft(batch) {
        case (acc, (stage, transform)) =>
          bulkTransform(acc, transform, stage, ctx)
      }
      val (left, right) = transformed.partition {
        _.isLeft
      }
      val f = consumeErrors(
        left
          .map {
            _.left.getOrElse(throw new Exception(s"Should not happen"))
          }
      ).zip(consumeRecords(right.map {
        _.getOrElse(throw new Exception(s"Should not happen"))._1
      }))
      Await.result(f, 5.minute) //Only one batch
    }
    pre()
    val errors = batchedIt.fold((Seq(), Seq())) {
      case ((accErr, accIds), (errs, ids)) =>
        afterBatch()
        (accErr ++ errs) -> (accIds ++ ids)
    }
    post()
    errors
  }
}
