package no.uit.sfb.cbf.jsonschema

import no.uit.sfb.cbf.shared.model.v1.nativ.Record

import java.io.StringReader
import java.net.URI

case class JsonSchemaRecordValidator(schemaUri: URI)
    extends Validator(schemaUri) {
  override def validate[T](obj: T): Unit = {
    //We already have support for String and Path.
    //We add support for Record
    obj match {
      case jr: Record =>
        reader(jr)
      case _ =>
        super.validate[T](obj)
    }
  }

  protected def reader(rec: Record): Unit = {
    val handler = ExceptionProblemHandler(s"${rec.getId}").asProblemHandler
    val strReader = new StringReader(rec.serialize())
    try {
      val reader = service.createReader(strReader, schema, handler)
      reader.read()
    } finally strReader.close()
  }
}
