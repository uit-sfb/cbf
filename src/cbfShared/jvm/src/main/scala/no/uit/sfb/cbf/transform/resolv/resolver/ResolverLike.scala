package no.uit.sfb.cbf.transform.resolv.resolver

import no.uit.sfb.cbf.shared.model.v1.nativ.CompactIRI
import no.uit.sfb.cbf.utils.FutureWithTimeout

import scala.concurrent.duration.DurationInt
import scala.concurrent.{ExecutionContext, Future}
import scala.util.matching.Regex

/**
  * A ResolverLike defines a component able to add metadata
  * from analysis of the CIRI or some external sources.
  */
trait ResolverLike {
  implicit def ec: ExecutionContext = ExecutionContext.global

  //The CIRI prefix for which this resolver is defined
  //Prefix MUST be lower case and non-empty
  def prefix: String

  //If false, data will not be cached (by a CachedResolutionLike)
  def cacheable: Boolean = false

  //If false, data will not be persisted (by a PersistedCachedResolutionLike)
  def persistable: Boolean = false

  def regexp: Regex = s"^${CompactIRI.idReg}$$".r

  //Given an id, returns the metadata associated

  /**
    * Note: We wrapped metaExternal into a FutureWithTimeout so that
    * in case an external resource is down, we do not end up with a TimeoutException
    * later down the code.
    * Given that the cache timeout is 1h, if the external resource becomes available again
    * then it will properly resolved passed this period of time.
    *
    * @param id
    * @param version
    * @return A future of None if the id doesn't match the regexp, otherwise Some(kv pairs).
    *         Note that it will throw an exception in case something goes wrong with the external metadata fetching.
    */
  final def meta(id: String,
                 dbName: String,
                 version: String,
                 apiHost: String): Future[Option[Map[String, String]]] = {
    val r = regexp //Needed: a case cannot contain a def
    id match {
      case r(groups @ _*) =>
        FutureWithTimeout(metaExternal(id), 20.seconds)
          .map { m =>
            Some(metaStatic(id, dbName, version, apiHost, groups.toArray) ++ m)
          }
      case _ =>
        Future.successful(None)
    }
  }

  final protected def ciri(id: String) = CompactIRI(prefix, id)

  /**
    *
    * @param id
    * @param version
    * @return
    */
  def metaStatic(id: String,
                 dbName: String,
                 version: String,
                 apiHost: String,
                 groups: Array[String]): Map[String, String] = {
    val tmp = Map(
      "_id" -> ciri(id).asString,
      "_key" -> id,
      "val" -> id //Will be overwritten by meta() if 'val' is defined there
    )
    val tmp2 =
      if (prefix.nonEmpty)
        tmp + ("_type" -> prefix)
      else
        tmp
    if (prefix.startsWith("resource_"))
      tmp2 + ("_iri" -> s"$apiHost/rpc/v1/$dbName/resources/$prefix:$id")
    else tmp2
  }

  def metaExternal(id: String): Future[Map[String, String]] =
    Future.successful(Map())

  protected def replace(
    str: String
  )(id: String, dbName: String, version: String, apiHost: String) =
    str
      .replace("{id}", id)
      .replace("{ver}", version)
      .replace("{dsName}", dbName)
      .replace("{api}", apiHost)
}
