package no.uit.sfb.cbf.jsonschema

import org.leadpony.justify.api.{Problem, ProblemHandler}

trait ProblemHandlerLike {
  def handle(problems: Seq[Problem]): Unit

  final lazy val asProblemHandler: ProblemHandler = {
    new ProblemHandler {
      override def handleProblems(problems: java.util.List[Problem]): Unit = {
        import scala.jdk.CollectionConverters._
        handle(problems.asScala.toSeq)
      }
    }
  }
}
