package no.uit.sfb.cbf.transform.resolv.resolver.predef

import io.circe.parser.parse
import no.uit.sfb.cbf.transform.resolv.resolver._

trait Biotools extends ExternalResolverLike {
  protected def dataUrl(id: String): String = {
    val formatedId = s"$prefix:$id"
    s"https://bio.tools/api/tool/$formatedId?format=json"
  }

  protected def parseData(str: String): Map[String, String] = {
    val json = parse(str).toTry.get.hcursor
    Map(
      "val" -> json.downField("name").as[String].toOption.map {
        _.capitalize
      },
      "description" -> json
        .downField("description")
        .as[String]
        .toOption,
      "homepage" -> json
        .downField("homepage")
        .as[String]
        .toOption,
    ).collect { case (k, Some(v)) => k -> v }
  }
}
