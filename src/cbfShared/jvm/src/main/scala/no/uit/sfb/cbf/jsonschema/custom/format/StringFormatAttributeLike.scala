package no.uit.sfb.cbf.jsonschema.custom.format

import jakarta.json.{JsonString, JsonValue}
import org.leadpony.justify.api.InstanceType
import org.leadpony.justify.spi.FormatAttribute

//Do not forget to register your format in src/main/resources/org/leadpony/justify/spi/FormatAttribute
trait StringFormatAttributeLike extends FormatAttribute {
  override lazy val valueType: InstanceType = {
    InstanceType.STRING;
  }

  final override def test(value: JsonValue): Boolean = {
    val string = value.asInstanceOf[JsonString].getString
    test(string)
  }

  /**
    * Checks if the string value conforms to this format.
    *
    * @param value the string value to check, which cannot be {@code null}.
    * @return {@code true} if the value conforms to the format, or {@code false}.
    */
  def test(value: String): Boolean
}
