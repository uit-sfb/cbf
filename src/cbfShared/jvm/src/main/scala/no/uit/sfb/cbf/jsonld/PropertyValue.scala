package no.uit.sfb.cbf.jsonld

case class PropertyValue(`@type`: String = "PropertyValue",
                         name: String,
                         value: String,
                         valueReference: Set[ValueReference] = Set(),
                         `custom:evidence`: Set[CustomEvidence] = Set())
    extends JsonLdLike
