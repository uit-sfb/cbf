package no.uit.sfb.cbf.utils

import java.net.URI
import java.nio.file.Paths
import scala.io.Source
import scala.util.{Failure, Success, Try, Using}

object UriUtils {
  def getContent(uri: URI): String = {
    val io = Try {
      uri.toURL
    } match {
      case Success(url) =>
        if (url.getProtocol != "file") {
          Source.fromURL(url.toString)
        } else
          Source.fromFile(uri.normalize())
      case _ =>
        Source.fromFile(Paths.get(uri.toString).toAbsolutePath.toFile)
    }
    Using(io){io => io.mkString} match {
      case Success(value) => value
      case Failure(exception) => throw exception
    }
//    try {
//      io.mkString
//    } finally io.close()
  }
}
