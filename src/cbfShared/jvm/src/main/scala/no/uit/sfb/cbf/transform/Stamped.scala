package no.uit.sfb.cbf.transform

import no.uit.sfb.cbf.mongodb.Collection
import no.uit.sfb.cbf.shared.model.v1.nativ.Record
import org.mongodb.scala.Document
import org.mongodb.scala.model.Filters

import scala.concurrent.duration._
import scala.concurrent.{Await, ExecutionContext, Future}

/**
  * Set the id field and update the timestamps
  * @param version
  * @param col
  * @param quick If true, we do not look at existing records to get the stamp
  */
class Stamped(version: String,
              col: Collection[Document],
              quick: Boolean = false) {
  def apply(rec: Record)(implicit ec: ExecutionContext): Record = {
    val parentRecord =
      if (quick)
        Future.successful(None)
      else {
        //Doesn't mater which one we take since they are all supposed to contain the same stamps
        //(except the fields that will be overwritten with new values anyway)
        col.getOne(Filters.eq("id", rec.getId))
      }
    val f = parentRecord.map {
      case Some(r) =>
        rec.stamped(version, Some(Record(r.toJson())))
      case None =>
        rec.stamped(version)
    }
    Await.result(f, 1.minute)
  }
}
