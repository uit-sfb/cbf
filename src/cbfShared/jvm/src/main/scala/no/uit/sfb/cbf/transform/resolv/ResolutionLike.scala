package no.uit.sfb.cbf.transform.resolv

import no.uit.sfb.cbf.shared.model.v1.nativ.CompactIRI
import no.uit.sfb.cbf.transform.resolv.resolver.ResolverLike

import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Failure, Success, Try}

trait ResolutionLike {
  implicit protected def ec: ExecutionContext = ExecutionContext.global

  protected def resolvers: Set[ResolverLike]

  /**
    *
    * @param ciri
    * @return
    *
    * Note: Throws when does not exist
    */
  final def get(ciri: CompactIRI,
                dbName: String = "{dsName}",
                version: String = "{ver}",
                apiHost: String = "{api}"): Future[Map[String, String]] = {
    fetchOption(ciri, dbName, version, apiHost)
      .map{
        case Some(m) => m
        case None =>
          resolver(ciri) match {
            case Some(resolver) => throw new Exception(s"CIRI '${ciri.asString}' could not be resolved (suffix does not match regexp '${resolver.regexp}'.)")
            case None => throw new Exception(s"CIRI '${ciri.asString}' could not be resolved (no resolver defines prefix '${ciri.prefix}').")
          }
      }.recover{
        case e:Exception if e.getMessage == null => Map[String,String]().empty // sent empty value
      }
  }

  /**
    *
    * @param ciri
    * @param extra
    * @return
    *
    * Throws when is not a CIRI
    */
  final def getFromString(
    ciri: String,
    dbName: String = "{dsName}",
    version: String = "{ver}",
    apiHost: String = "{api}"
  ): Future[Map[String, String]] = {
    CompactIRI(ciri) match {
      case Some(c) => get(c, dbName, version, apiHost)
      case None =>
        throw new Exception(s"'$ciri' is not a compact IRI")
    }
  }

  /**
    *
    * @param ciri
    * @return A future of None if anything happened, otherwise Some(kv pairs).
    */
  def getOption(
    ciri: CompactIRI,
    dbName: String = "{dsName}",
    version: String = "{ver}",
    apiHost: String = "{api}"
  ): Future[Option[Map[String, String]]] =
    fetchOption(ciri, dbName, version, apiHost).recover { case _ => None }

  /**
    *
    * @param ciri
    * @return A future of None if the prefix was unknown or the id doesn't match the regexp, otherwise Some(kv pairs).
    *         Note that it will throw an exception in case something goes wrong with the external metadata fetching.
    */
  protected def fetchOption(
    ciri: CompactIRI,
    dbName: String = "{dsName}",
    version: String = "{ver}",
    apiHost: String = "{api}"
  ): Future[Option[Map[String, String]]] =
    fetch(ciri, dbName, version, apiHost)

  def resolver(ciri: CompactIRI): Option[ResolverLike] = {
    resolvers.find {
      _.prefix.toLowerCase ==
        ciri.prefix.toLowerCase()
    }
  }

  /**
    *
    * @param ciri
    * @return A future of None if the prefix was unknown or the id doesn't match the regexp, otherwise Some(kv pairs).
    *         Note that it will throw an exception in case something goes wrong with the external metadata fetching.
    */
  final protected def fetch(
    ciri: CompactIRI,
    dbName: String = "{dsName}",
    version: String = "{ver}",
    apiHost: String = "{api}"
  ): Future[Option[Map[String, String]]] = {
    resolver(ciri) match {
      case Some(resolver) =>
        resolver.meta(ciri.id, dbName, version, apiHost)
      case None =>
        println(s"'${ciri.prefix}' does not exist!")
        Future.successful(None)
    }
  }
}
