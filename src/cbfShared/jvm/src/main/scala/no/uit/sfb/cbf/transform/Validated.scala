package no.uit.sfb.cbf.transform

import no.uit.sfb.cbf.jsonschema.{JsonSchemaRecordValidator, Validator}
import no.uit.sfb.cbf.shared.config.DatasetConfig
import no.uit.sfb.cbf.shared.model.v1.nativ.Record
import no.uit.sfb.cbf.shared.validate.QuickValidator

import java.net.URI
import java.nio.file.Paths

/**
  * Validate a record, throwing an exception in case of validation error. The same record is returned otherwise.
  * @param schema
  * @param datasetConfig
  */
class Validated(schema: Option[URI], datasetConfig: DatasetConfig) {
  protected lazy val quickValidator = new QuickValidator(datasetConfig)

  //For some weird reason, do NOT use a lazy val here (or the object would be re-created each time apply is invoked)
  protected val validator: Option[Validator] = {
    schema.map { uri =>
      val schemaUri =
        if (uri.getScheme == null)
          Paths.get(uri.toString).toAbsolutePath.toFile.toURI
        else
          uri
      JsonSchemaRecordValidator(schemaUri)
    }
  }

  def apply(record: Record): Record = {
    val recWithErrors = quickValidator(record)
    recWithErrors.attributes.foreach {
      case (ref, stm) =>
        if (stm.hasError)
          throw new Exception(s"$ref: ${stm.errors.mkString(" | ")}")
    }
    validator foreach {
      _.validate(record)
    } //Throws in case of failure
    record
  }
}
