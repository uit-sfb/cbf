package no.uit.sfb.cbf.jsonld

case class Sample(`@type`: String = "Sample",
                  override val `@id`: Option[String],
                  identifier: String,
                  url: String,
                  name: String,
                  subjectOf: Set[String],
                  additionalProperty: Set[PropertyValue])
    extends JsonLdLike
