package no.uit.sfb.cbf.jsonld

case class ValueReference(`@type`: String = "CategoryCode", url: Seq[String])
    extends JsonLdLike
