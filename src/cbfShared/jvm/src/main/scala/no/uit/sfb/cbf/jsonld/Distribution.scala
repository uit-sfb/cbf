package no.uit.sfb.cbf.jsonld

case class Distribution(`@type`: String = "DataDownload",
                        name: String,
                        fileFormat: String,
                        contentURL: String)
    extends JsonLdLike
