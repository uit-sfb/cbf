package no.uit.sfb.cbf.mongodb

import com.mongodb.bulk.BulkWriteResult
import com.typesafe.scalalogging.LazyLogging
import no.uit.sfb.cbf.mongodb.utils.BulkWriteResult2
import no.uit.sfb.cbf.shared.errors.BadGatewayError
import org.bson.conversions.Bson
import org.mongodb.scala.bson.Document
import org.mongodb.scala.model.Indexes._
import org.mongodb.scala.model.{
  BulkWriteOptions,
  Collation,
  IndexOptions,
  ReplaceOptions,
  UpdateOptions,
  WriteModel
}
import org.mongodb.scala.result.{
  DeleteResult,
  InsertManyResult,
  InsertOneResult,
  UpdateResult
}
import org.mongodb.scala.{
  FindObservable,
  MongoCollection,
  Observer,
  Subscription
}

import java.util.concurrent.ConcurrentLinkedQueue
import java.util.concurrent.atomic.{AtomicBoolean, AtomicInteger}
import scala.collection.immutable.ListMap
import scala.concurrent.duration._
import scala.concurrent.{Await, ExecutionContext, Future, blocking}
import scala.jdk.CollectionConverters._
import scala.reflect.ClassTag

class Collection[T: ClassTag](col: MongoCollection[T])(
  implicit ec: ExecutionContext
) extends LazyLogging {
  def listIndexes(): Future[Seq[Document]] = blocking {
    col.listIndexes().toFuture()
  }

  //Use replace() with upsert option if want to insert or upsert
  def insert(doc: T): Future[InsertOneResult] = {
    blocking {
      col.insertOne(doc).toFuture()
    } map { res =>
      if (res.wasAcknowledged())
        logger.info(res.toString)
      else
        throw BadGatewayError(res.toString)
      res
    }
  }

  def insert(docs: Seq[T]): Future[InsertManyResult] = {
    blocking {
      col.insertMany(docs).toFuture()
    } map { res =>
      if (res.wasAcknowledged())
        logger.info(res.toString)
      else
        throw BadGatewayError(res.toString)
      res
    }
  }

  /** Counts the number of documents optionally satisfying a filter
    *
    * @param filter A Bson filter. If None, the estimatedDocumentCount method is used instead
    * @return
    */
  def count(filter: Bson = Document.empty): Future[Long] = {
    blocking {
      (if (filter == Document.empty)
         col.estimatedDocumentCount()
       else
         col.countDocuments(filter))
        .toFuture()
    }
  }

  /*
  Note: creating an already existing index is a no-op
   */
  def createCompoundIndex(keys: ListMap[String, Collection.IndexLike],
                          unique: Boolean = false,
                          name: Option[String] = None): Future[String] = {
    val bsonSpec: Bson = {
      val specs = keys map {
        case (k, indexType) =>
          indexType.bson(k)
      }
      compoundIndex(specs.toSeq: _*)
    }
    blocking {
      col
        .createIndex(
          bsonSpec,
          new IndexOptions().unique(unique).name(name.orNull)
        )
        .toFuture()
    }
  }

  def createIndexes(keys: Iterable[IndexDef]): Future[Seq[String]] = {
    Future
      .sequence(
        keys
          .map {
            case IndexDef(k, indexType, unique) =>
              val opt = new IndexOptions().unique(unique)
              indexType.bson(k) -> opt
          }
          .map {
            case (idx, opt) =>
              col.createIndex(idx, opt).toFuture()
          }
      )
      .map {
        _.toList
      }
  }

  //Attention, it is the index name that has to be used, not the key it is based on
  def dropIndex(indexName: String): Future[Any] = {
    blocking {
      col.dropIndex(indexName).toFuture()
    }
  }

  protected def getImpl(filter: Bson = Document.empty,
                        sort: Bson = Document.empty,
                        project: Bson = Document.empty,
                        collation: Collation = Collation.builder().build,
                        skip: Option[Int] = None,
                        limit: Option[Int] = None,
                        allowDiskUse: Boolean = false,
                        maxTime: Duration = 0.second): FindObservable[T] = {
    val tmp = col
      .find(filter)
      .sort(sort)

    val tmp2 = skip match {
      case Some(s) =>
        tmp.skip(s)
      case _ => tmp
    }

    val tmp3 = limit match {
      case Some(l) =>
        tmp2.limit(l)
      case _ => tmp2
    }

    tmp3
      .projection(project)
      .collation(collation)
      .allowDiskUse(allowDiskUse)
      .maxTime(maxTime)
  }

  def getAll(): Future[Seq[T]] = {
    get()
  }

  /**
    *
    * @param filter    Build with org.mongodb.scala.model.Filters
    * @param sort      Build with org.mongodb.scala.model.Sorts
    * @param project   Build with org.mongodb.scala.model.Projections
    * @param collation Build with org.mongodb.scala.model.PCollation
    * @param skip      Number of documents to skip
    * @param limit     Max number of documents returned
    * @return
    */
  def get(filter: Bson = Document.empty,
          sort: Bson = Document.empty,
          project: Bson = Document.empty,
          collation: Collation = Collation.builder().build,
          skip: Option[Int] = None,
          limit: Option[Int] = None,
          allowDiskUse: Boolean = false,
          maxTime: Duration = 0.second): Future[Seq[T]] = {
    blocking {
      getImpl(
        filter,
        sort,
        project,
        collation,
        skip,
        limit,
        allowDiskUse,
        maxTime
      ).toFuture()
    }
  }

  //Memory footprint: 2 x batchSize x docSize
  protected class ObserverIterator(batchSize: Int = 500)
      extends Observer[T]
      with Iterator[T] {
    assert(batchSize >= 0)
    private var subscription: Option[Subscription] = None
    private val buffer = new ConcurrentLinkedQueue[T]()
    private val finished = new AtomicBoolean(false)
    @volatile private var error: Option[Throwable] = None
    private val count = new AtomicInteger(0)

    protected def requestNewBatch = {
      //logger.info(s"New batch ${this.hashCode()}")
      subscription match {
        case Some(subs) =>
          count.addAndGet(batchSize)
          subs.request(batchSize)
        case None =>
          throw new Exception(
            "next() called before onSubscribe(). Please make sure to call onSubscribe first."
          )
      }
    }

    override def onSubscribe(subscription: Subscription): Unit = {
      this.subscription = Some(subscription)
      requestNewBatch
    }

    override def onNext(result: T): Unit =
      buffer.add(result)

    //No need to synchronize since error is used exclusively in hasNext after the wait()
    override def onError(e: Throwable): Unit = synchronized {
      error = Some(e)
      onComplete()
    }

    override def onComplete(): Unit = synchronized {
      finished.set(true)
      notifyAll()
    }

    //Blocking when buffer is empty and finished flag is not yet set
    override def hasNext: Boolean = {
      //Note: cannot use count to determine termination as the last returned batch may only contain up to batchSize items.
      synchronized {
        //Although an "if (...) wait()" would suffice in theory, in practice we use "while (...) wait()" because of the possibility of spurious awakening https://stackoverflow.com/a/25096851/4965515
        //In addition we use a small timeout because we only get notified when finish gets true, but not when the buffer gets new elements (not to have to get too heavy on the notification side).
        while (buffer.isEmpty && !finished.get()) {
          wait(500)
        }
      }
      if (error.nonEmpty)
        throw error.get
      !buffer.isEmpty
    }

    //Blocking when buffer is empty and finished flag is not yet set
    override def next(): T = {
      //Note: we cannot use buffer.iterator() as it is only weakly consistent (meaning it would not necessarily reflect additions that happened)
      if (hasNext) {
        val res = buffer.poll()
        //Res should not be null since hasNext returns true only if the buffer is non-empty
        if (res != null) {
          val newCount = count.decrementAndGet()
          //If the number of items remaining to be seen is less than 20% of batchSize, we fetch a new batch
          if (newCount <= batchSize / 20) {
            requestNewBatch
          }
          return res
        } else
          throw new NoSuchElementException()
      }
      throw new NoSuchElementException()
    }
  }

  /** Make a request and get the result as an Iterator
    *
    * @param filter
    * @param sort
    * @param project
    * @param collation
    * @param skip
    * @param limit
    * @param timeout Max cumulative time limit for processing operations on the cursor
    * @param batchSize It might be needed to set a smaller batch size in cases where processing the whole batch takes longer than the cursor timeout (10 min)
    * @return an Iterator
    */
  def getIt(filter: Bson = Document.empty,
            sort: Bson = Document.empty,
            project: Bson = Document.empty,
            collation: Collation = Collation.builder().build,
            skip: Option[Int] = None,
            limit: Option[Int] = None,
            //timeout: Duration = 0.second,
            batchSize: Int = 500): Iterator[T] = {
    //For some reason we need to handle the case where limit is 0 manually otherwise the whole collection is returned
    if (limit.contains(0))
      Iterator.empty
    else {
      //No need to optimize the batch size with capping with the limit.
      val observer = new ObserverIterator(batchSize)
      getImpl(filter, sort, project, collation, skip, limit)
        .subscribe(observer)
      observer
    }
  }

  def getOne(filter: Bson = Document.empty,
             sort: Bson = Document.empty,
             project: Bson = Document.empty,
             collation: Collation = Collation.builder().build(),
             skip: Option[Int] = None,
             limit: Option[Int] = None,
             allowDiskUse: Boolean = false,
             maxTime: Duration = 0.second): Future[Option[T]] = {
    getImpl(
      filter,
      sort,
      project,
      collation,
      skip,
      limit,
      allowDiskUse,
      maxTime
    ).headOption()
  }

  /**Build with import org.mongodb.scala.model.Aggregates
    * Limit of 16MB, unless allowDiskUse is set but then it is much slower, or use out to export data
    *
    * @param pipelines
    * @param allowDiskUse
    * @param maxTime
    * @param batchSize It might be needed to set a smaller batch size in cases where processing the whole batch takes longer than the cursor timeout (10 min)
    * @return
    */
  def aggregate(pipelines: Seq[Bson],
                allowDiskUse: Boolean = false,
                maxTime: Duration = 0.second,
                batchSize: Int = 500): Iterator[T] = {
    val observer = new ObserverIterator(batchSize)
    col
      .aggregate(pipelines)
      .allowDiskUse(allowDiskUse)
      .maxTime(maxTime)
      .subscribe(observer)
    observer
  }

  def drop(): Future[Any] = blocking {
    col.drop().toFuture()
  }

  /**
    * @param filter Build import org.mongodb.scala.model.Filters
    * @param single Apply to only one document
    * @return
    */
  def delete(filter: Bson, single: Boolean = false): Future[DeleteResult] = {
    val func =
      if (single)
        col.deleteOne(filter)
      else
        col.deleteMany(filter)
    blocking {
      func.toFuture()
    } map { res =>
      if (res.wasAcknowledged())
        logger.info(res.toString)
      else
        throw BadGatewayError(res.toString)
      res
    }
  }

  /**
    *
    * @param filter Build with org.mongodb.scala.model.Filters
    * @param update Build with org.mongodb.scala.model.Updates
    * @param single Apply to only one document
    * @param upsert If true, inserts the document if it does not exist
    * @return
    */
  def update(filter: Bson,
             update: Bson,
             single: Boolean = false,
             upsert: Boolean = false): Future[UpdateResult] = {
    val func =
      if (single)
        col.updateOne(filter, update, UpdateOptions().upsert(upsert))
      else
        col.updateMany(filter, update, UpdateOptions().upsert(upsert))
    blocking {
      func.toFuture()
    } map { res =>
      if (res.wasAcknowledged())
        logger.info(res.toString)
      else
        throw BadGatewayError(res.toString)
      res
    }
  }

  /**
    * Replace ONE
    *
    * @param filter      Build with org.mongodb.scala.model.Filters
    * @param replacement Replacement document
    * @param upsert      If true, inserts the document if it does not exist
    * @return
    */
  def replace(filter: Bson,
              replacement: T,
              upsert: Boolean = false): Future[UpdateResult] = {
    blocking {
      col
        .replaceOne(filter, replacement, ReplaceOptions().upsert(upsert))
        .toFuture()
    } map { res =>
      if (res.wasAcknowledged())
        logger.info(res.toString)
      else
        throw BadGatewayError(res.toString)
      res
    }
  }

  /** bulk write
    *
    * @param writes List built with XxxOneModel (ex: InsertOneModel)
    * @return
    */
  def bulkWrite(writes: Seq[WriteModel[_ <: T]],
                ordered: Boolean = false): Future[BulkWriteResult] = {
    val f =
      if (writes.nonEmpty) {
        blocking {
          col
            .bulkWrite(writes, BulkWriteOptions().ordered(ordered))
            .toFuture()
        }
      } else
        Future.successful(
          BulkWriteResult
            .acknowledged(0, 0, 0, 0, List().asJava, List().asJava)
        )
    f map { res =>
      if (res.wasAcknowledged())
        logger.info(new BulkWriteResult2(res).toString())
      else
        throw BadGatewayError(res.toString)
      res
    }
  }

  //findOneAndDelete/Update/Replace
}

object Collection {

  sealed trait IndexLike {
    def bson(str: String): Bson
  }

  case object Asc extends IndexLike {
    def bson(str: String) = ascending(str)
  }

  case object Desc extends IndexLike {
    def bson(str: String) = descending(str)
  }

  case object Text extends IndexLike {
    def bson(str: String) = text(str)
  }
}
