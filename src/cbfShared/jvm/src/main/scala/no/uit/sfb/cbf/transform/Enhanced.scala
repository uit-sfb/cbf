package no.uit.sfb.cbf.transform

import no.uit.sfb.cbf.shared.config._
import no.uit.sfb.cbf.shared.model.v1.nativ._
import no.uit.sfb.cbf.transform.resolv.Resolution

import scala.concurrent.duration._
import scala.concurrent.{Await, ExecutionContext, Future}

/**
  * Quick enhancement for records
  *   - Append a suitable identifier if applicable
  *   - Trim suffix if any
  *   - If fallback is true, CIRIs with unknown prefix are moved back to `val` if `val` is empty
  *
  */
class Enhanced(attributesDefs: Seq[AttributeDef],
               resolvers: Resolution,
               fallback: Boolean = false)(implicit ec: ExecutionContext) {
  protected def trimSuffix(vr: ValueRef,
                           attribute: Option[AttributeDef]): ValueRef = {
    attribute match {
      case Some(attr) =>
        vr.copy(`val` = vr.`val`.stripSuffix(attr.suffix).trim)
      case None => vr
    }
  }

  // Essentially add missing '§' and even try to add missing prefixes
  // If fallback is true, CIRIs with unknown prefix are moved back to `val` if `val` is empty
  protected def findCiri(vr: ValueRef,
                         attribute: Option[AttributeDef],
                         isEvidence: Boolean = false,
                         fallback: Boolean): Future[ValueRef] = {
    val autoPrefixes = attribute
      .map { attr =>
        attr.resolvers
          .filter {
            _.nonEmpty
          }
          .map {
            _.toLowerCase
          }
      }
      .getOrElse(Seq())
    if (vr.ciris.isEmpty && vr.`val`.nonEmpty) {
      (CompactIRI(vr.`val`) match {
        case Some(ciri) if autoPrefixes.contains(ciri.prefix) || isEvidence =>
          //We have an auto prefix, or we are in a statement
          //The CIRI will be validated in the Expressed stage
          //Note: for evidence we accept any prefix (which includes eco, but also all the doi, pubmed, ... for sources)
          Future.successful(Some(ciri))
        case Some(_) =>
          //Unknown prefix
          //We do not consider it as a CIRI
          Future.successful(None)
        case None =>
          //Not a ciri, but we try the list of allowed prefixes one by one to try to find one that fits
          Future
            .traverse(autoPrefixes) { prefix =>
              val ciri = CompactIRI(prefix, vr.`val`)
              resolvers
                .getOption(ciri) //It is ok to not have dbName, version and apiHost since we are just enhancing
                .map {
                  case Some(_) =>
                    Some(ciri)
                  case None =>
                    None
                }
            }
            .map {
              _.flatten.headOption
            }
      }).map {
        case Some(ciri) =>
          vr.copy(`val` = "", iri = Seq(ciri.asString))
        case None =>
          vr
      }
    } else
      Future.successful(
        if (fallback && vr.`val`.isEmpty && vr.ciris.nonEmpty && !autoPrefixes
              .contains(vr.ciris.head.prefix)) {
          vr.copy(`val` = vr.ciris.head.asString, iri = vr.ciris.tail.map {
            _.asString
          } ++ vr.urls)
        } else
          vr
      )
  }

  def apply(vr: ValueRef,
            attribute: Option[AttributeDef] = None,
            isEvidence: Boolean = false): Future[ValueRef] = {
    val trimmed = trimSuffix(vr, attribute)
    findCiri(trimmed, attribute, isEvidence, fallback)
  }

  def apply(evidence: Evidence): Future[Evidence] = {
    val k: Future[ValueRef] = apply(evidence.predicate, None, true)
    val l: Future[Set[ValueRef]] = Future.sequence(evidence.src map { source =>
      apply(source, None, true)
    })
    k zip l map {
      case (vr, sources) =>
        evidence.copy(`val` = vr.`val`, iri = vr.iri, src = sources)
    }
  }

  def apply(statement: Statement,
            attribute: Option[AttributeDef]): Future[Statement] = {
    val fStm = Future.sequence {
      statement.obj.map {
        apply(_, attribute)
      }
    }
    val fEvi = Future.sequence {
      statement.evi.map {
        apply
      }
    }
    for {
      stmRes <- fStm
      eviRes <- fEvi
    } yield statement.copy(obj = stmRes, evi = eviRes)
  }

  def apply(record: Record): Record = {
    val f = Future.sequence(record.attributes map {
      case (name, st) =>
        apply(st, attributesDefs.find(_.ref == name)) map { v =>
          Map(name -> v)
        }
    })
    val f2 = f map { attrs =>
      record.copy(attrs = attrs)
    }
    Await.result(f2, 1.minute)
  }
}
