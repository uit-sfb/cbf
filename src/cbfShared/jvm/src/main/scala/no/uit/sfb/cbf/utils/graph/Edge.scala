package no.uit.sfb.cbf.utils.graph

case class Edge(name: String, target: String)
