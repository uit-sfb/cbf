package no.uit.sfb.cbf.transform

import no.uit.sfb.cbf.mongodb.Collection
import no.uit.sfb.cbf.shared.model.v1.builder.RecordBuilder
import no.uit.sfb.cbf.shared.model.v1.nativ.Record
import org.mongodb.scala.Document
import org.mongodb.scala.model.Filters

import scala.concurrent.duration._
import scala.concurrent.{Await, ExecutionContext}

/**
  * Merge a patch into a record
  * @param col
  * @param rb
  * @param release
  * @param ec
  */
class Merged(col: Collection[Document], rb: RecordBuilder, release: Int)(
  implicit ec: ExecutionContext
) {
  def apply(record: Record): Record = {
    val id = record.getId
    val f = col
      .getOne(
        Filters.and(
          Filters.eq("id", id),
          Filters.lte("valid.start", release),
          Filters.or(
            Filters.exists("valid.end", false),
            Filters.gt("valid.end", release)
          )
        )
      )
      .map {
        case Some(doc) =>
          val orig = rb.fromSingleJson(doc.toJson())
          val m = record.attrs
            .filter { _.nonEmpty }
            .groupBy(_.head._1)
            .view
            .mapValues {
              _.flatMap { _.headOption.map { _._2 } }
            }
            .toMap
          orig.addAttrs(m, true)
        case None =>
          throw new Exception(s"Could not find original record '$id'")
      }
    Await.result(f, 1.minute)
  }
}
