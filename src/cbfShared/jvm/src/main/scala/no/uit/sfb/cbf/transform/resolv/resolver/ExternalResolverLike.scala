package no.uit.sfb.cbf.transform.resolv.resolver

import scala.concurrent.{Future, blocking}
import scala.io.Source
import scala.util.Using

/**
  * Resolver able to fetch data from external URL
  * ATTENTION: make sure to add any ExternalResolver to Resolution.resolvers()
  */
trait ExternalResolverLike extends ResolverLike {

  /**
    * URL pointing to data (machine readable)
    * Attention it is not the same as the url field,
    * which is the URL the CIRI is pointing to (human readable)
    */
  protected def dataUrl(id: String): String

  /**
    * Should always define:
    * - value as "val"
    * - synonyms as "synonyms" (if applicable)
    * - description as "description" (if applicable)
    */
  protected def parseData(str: String): Map[String, String]

  override def cacheable = true

  override def persistable = true

  override def metaExternal(id: String): Future[Map[String, String]] = {
    getData(dataUrl(id)).map {
      parseData
    }
  }

  protected def getData(url: String): Future[String] = Future {
    blocking {
    Using(Source.fromURL(url)) {
      io => io.mkString
    }.get
    }
  }
}
