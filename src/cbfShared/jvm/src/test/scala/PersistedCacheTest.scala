import no.uit.sfb.cbf.mongodb.{Collection, MongodbConnect, SharedDatabase}
import no.uit.sfb.cbf.shared.config.{CustomResolver, DatasetConfig}
import no.uit.sfb.cbf.shared.model.v1.nativ.CompactIRI
import no.uit.sfb.cbf.transform.resolv.{
  PersistedCachedResolutionLike,
  Resolution
}
import no.uit.sfb.cbf.utils.UriUtils
import org.mongodb.scala.bson.Document
import org.scalatest.funspec.AnyFunSpec
import org.scalatest.matchers.should.Matchers

import java.nio.file.Paths
import scala.concurrent.duration._
import scala.concurrent.{Await, ExecutionContext}

class PersistedCacheTest extends AnyFunSpec with Matchers {
  implicit val ec = ExecutionContext.global

  protected val mgc = MongodbConnect()

  private def sdb = SharedDatabase(mgc)

  private val configUri =
    Paths.get("dataToolset/src/test/resources/config/test1-config.json").toUri

  private val datasetConfig = DatasetConfig(UriUtils.getContent(configUri))

  lazy val customResolvers = datasetConfig.customResolvers

  //Important to use a def so that the cache of the first step does not affect the second
  private def reslover: PersistedCachedResolutionLike =
    new PersistedCachedResolutionLike {

      val resolvers = Resolution.resolvers(customResolvers)

      val db = sdb
    }

  //Important to use a lazy val so that the cache of the first step does not affect the second
  private lazy val resolv = new Resolution(mgc, customResolvers)

  val ciri = "gaz:00033469"
  Await.result(sdb.drop(), 45.seconds)

  describe("GAZ resolver without override") {
    it(s"should get info") {
      val info = reslover.get(CompactIRI(ciri).get)
      val res = Await.result(info, 45.seconds)
      res.get("val") should be(Some("City of Melbourne"))
      //println(res)
    }
  }

  describe("GAZ resolver with override") {
    it(s"should get info") {
      val info = reslover.get(CompactIRI("gaz:00002459").get)
      val res = Await.result(info, 45.seconds)
      res.get("val") should be(Some("USA"))
      res.get("synonyms") should be(Some("United States of America|U.S.A"))
      //println(res)
    }
  }

  describe("Resolver search") {
    it(s"should return none") {
      val ids = Await.result(resolv.search(""), 15.seconds)
      ids.size should be(0)
    }

    it(s"should find ciri from label") {
      val ids = Await.result(resolv.search("USA"), 15.seconds)
      ids.size should be(1)
    }

    it(s"should find ciri from synonym") {
      val ids = Await.result(resolv.search("U.S.A"), 15.seconds)
      ids.size should be(1)
    }

    it(s"should find ciri from key") {
      val ids = Await.result(resolv.search("00033469"), 15.seconds)
      ids.size should be(1)
    }

    it(s"should find ciri from id") {
      val ids = Await.result(resolv.search(ciri), 15.seconds)
      ids.size should be(1)
    }
  }
}
