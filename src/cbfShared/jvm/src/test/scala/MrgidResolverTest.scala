import no.uit.sfb.cbf.shared.config.CustomResolver
import no.uit.sfb.cbf.shared.model.v1.nativ.CompactIRI
import no.uit.sfb.cbf.transform.resolv.{CachedResolutionLike, Resolution}
import org.scalatest.funspec.AnyFunSpec
import org.scalatest.matchers.should.Matchers

import scala.concurrent.Await
import scala.concurrent.duration._

class MrgidResolverTest extends AnyFunSpec with Matchers {
  val resolv = Resolution.resolvers(Seq(CustomResolver("mrgid")))
  describe("MRGID resolver") {
    it(s"should get info with synonyms") {
      val ciri = "mrgid:3329"
      val resolver = new CachedResolutionLike {
        val resolvers = resolv
      }
      val info = resolver.get(CompactIRI(ciri).get)
      val res = Await.result(info, 45.seconds)
      //println(res)
      res.keySet should be(
        Set(
          "val",
          "description",
          "_id",
          "_iri",
          "latitude",
          "longitude",
          "_key",
          "_type",
          "synonyms",
          "country",
          "world_region"
        )
      )
      res.get("country") should be(Some("gaz:00002940"))
      res.get("world_region") should be(Some("gaz:00002287"))
      res.get("synonyms") should be(Some("Corse"))
    }
    it(s"should return error") {
      val ciri = "mrgid:11115555555"
      val resolver = new CachedResolutionLike {
        val resolvers = resolv
      }
      val info = resolver.getOption(CompactIRI(ciri).get)
      val res = Await.result(info, 45.seconds)
      res should be(None)
    }
  }
}
