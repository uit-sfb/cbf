import no.uit.sfb.cbf.utils.UriUtils

import java.net.URI
import java.nio.file.Paths
import org.scalatest.funspec.AnyFunSpec
import org.scalatest.matchers.should.Matchers

class UriUtilsTest extends AnyFunSpec with Matchers {
  describe("UriUtils") {
    it(s"should parse relative path") {
      val path = new URI("dataToolset/README.md")
      println(path.normalize())
      UriUtils.getContent(path)
    }
    it(s"should parse full path") {
      val path = Paths.get("dataToolset/README.md").toUri
      UriUtils.getContent(path)
    }
    it(s"should parse URL") {
      UriUtils.getContent(new URI("https://www.wikipedia.org/"))
    }
  }
}
