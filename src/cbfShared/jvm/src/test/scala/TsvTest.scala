import no.uit.sfb.cbf.shared.utils.Tsv

import java.nio.file.Paths
import no.uit.sfb.scalautils.common.FileUtils
import org.scalatest.funspec.AnyFunSpec
import org.scalatest.matchers.should.Matchers

class TsvTest extends AnyFunSpec with Matchers {
  private val tsvPath = Paths.get(getClass.getResource("/example.tsv").getPath)
  private val multiTsvPath =
    Paths.get(getClass.getResource("/example_multi.tsv").getPath)
  describe("Tsv") {
    it(s"should extract and transpose") {
      val res =
        Tsv
          .extractLineAndTranspose(multiTsvPath, 1)
      //println(res)
    }
    it(s"should extract and transpose second line") {
      Tsv.extractLineAndTranspose(multiTsvPath, 2)
    }
  }
}
