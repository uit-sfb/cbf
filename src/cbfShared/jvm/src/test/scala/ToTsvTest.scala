import no.uit.sfb.cbf.shared.model.v1.builder.RecordBuilder
import no.uit.sfb.cbf.shared.model.v1.nativ.Record

import java.nio.file.Paths
import no.uit.sfb.scalautils.common.FileUtils
import org.scalatest.funspec.AnyFunSpec
import org.scalatest.matchers.should.Matchers

class ToTsvTest extends AnyFunSpec with Matchers {
  private val tsvPath = Paths.get(getClass.getResource("/example.tsv").getPath)
  private val multiTsvPath =
    Paths.get(getClass.getResource("/example_multi.tsv").getPath)
  describe(classOf[Record].getName) {
    it(s"should convert model to TSV") {
      val entry = new RecordBuilder().fromSingleTsv(FileUtils.readFile(tsvPath))
      val res = entry.asString
      //println(res)
    }
    it(s"should convert multi to TSV") {
      val (ctx, it) = new RecordBuilder().fromTsv(multiTsvPath)
      val (records, errors) = it partition {
        _.isRight
      }
      errors.isEmpty should be(true)
      val res = new RecordBuilder().toTsv(records.map {
        _.getOrElse(throw new Exception("Should not happen"))._1
      }.toSeq)
      //println(res)
    }
  }
}
