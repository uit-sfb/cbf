import no.uit.sfb.cbf.mongodb.{CbfDataset, MongodbConnect}
import org.mongodb.scala.bson.Document
import org.scalatest.funspec.AnyFunSpec
import org.scalatest.matchers.should.Matchers

import scala.concurrent.{Await, ExecutionContext}
import scala.concurrent.duration.DurationInt

class DbTest extends AnyFunSpec with Matchers {
  implicit val ec = ExecutionContext.global
  private val db = new CbfDataset(
    MongodbConnect()
      .db("_test")
  )
  private val colName = "_myCol"
  describe("MongodbConnect") {
    it(s"should be able to list the datasets") {
      Await.result(MongodbConnect().listDb, 45.seconds).nonEmpty should be(true)
    }
    it(s"should be able to drop a db") {
      Await.result(db.drop(), 45.seconds)
    }
    it(s"should be able to insert a document") {
      val doc = Document(
        "name" -> "MongoDB",
        "type" -> "database",
        "count" -> 1,
        "info" -> Document("x" -> 203, "y" -> 102)
      )
      Await
        .result(db.collection(colName).insert(doc), 45.seconds)
    }
    it(s"should be able to count the number of documents") {
      Await
        .result(
          db.collection(colName)
            .count(),
          45.seconds
        ) should be(1)
    }
    it(s"should be able to list the collections") {
      Await
        .result(db.listCollections(), 45.seconds)
        .nonEmpty should be(true)
    }
    it(s"should be able to display the indexes") {
      Await
        .result(
          db.collection(colName)
            .listIndexes(),
          45.seconds
        )
    }
  }
}
