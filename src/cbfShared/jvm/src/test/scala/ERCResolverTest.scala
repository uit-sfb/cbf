import no.uit.sfb.cbf.shared.config.CustomResolver
import no.uit.sfb.cbf.shared.model.v1.nativ.CompactIRI
import no.uit.sfb.cbf.transform.resolv.{CachedResolutionLike, Resolution}
import org.scalatest.funspec.AnyFunSpec
import org.scalatest.matchers.should.Matchers

import scala.concurrent.Await
import scala.concurrent.duration._

class ERCResolverTest extends AnyFunSpec with Matchers {

  //TODO: Fix this test, apparently 'ERC:ERC000012' was removed

//  val resolv = Resolution.resolvers(Seq(CustomResolver("erc")))
//  describe("ERC resolver") {
//    it(s"should get info") {
//      val ciri = "ERC:ERC000012"
//      val resolver = new CachedResolutionLike {
//        val resolvers = resolv
//      }
//      val info = resolver.get(CompactIRI(ciri).get)
//      val res = Await.result(info, 45.seconds)
//      res.keySet should be(
//        Set("val", "_id", "_iri", "_key", "_type", "description")
//      )
//      res("val") should be("GSC MIxS air")
//    }
//    it(s"should return error") {
//      val ciri = "ERC:ERC999999"
//      val resolver = new CachedResolutionLike {
//        val resolvers = resolv
//      }
//      val info = resolver.getOption(CompactIRI(ciri).get)
//      val res = Await.result(info, 45.seconds)
//      res should be(None)
//    }
//  }

}
