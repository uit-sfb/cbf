import no.uit.sfb.cbf.shared.config.CustomResolver
import no.uit.sfb.cbf.shared.model.v1.nativ.CompactIRI
import no.uit.sfb.cbf.transform.resolv.{CachedResolutionLike, Resolution}
import org.scalatest.funspec.AnyFunSpec
import org.scalatest.matchers.should.Matchers

import scala.concurrent.duration._
import scala.concurrent.Await

class EcoResolverTest extends AnyFunSpec with Matchers {
  val resolv = Resolution.resolvers(Seq(CustomResolver("eco", "^\\d{7}$", Map("_iri" -> "https://identifiers.org/ECO:{id}"), Some("http://purl.obolibrary.org/obo/ECO_"))))
  describe("ECO resolver") {
    it(s"should get info") {
      val ciri = "ECO:0000311"
      val resolver = new CachedResolutionLike {
        val resolvers = resolv
      }
      val info = resolver.get(CompactIRI(ciri).get)
      val res = Await.result(info, 45.seconds)
      res.keySet should be(
        Set("val", "description", "_type", "_id", "_iri", "_key")
      )
      //println(res)
    }
    it(s"should return error") {
      val ciri = "ECO:9999999"
      val resolver = new CachedResolutionLike {
        val resolvers = resolv
      }
      val info = resolver.getOption(CompactIRI(ciri).get)
      val res = Await.result(info, 45.seconds)
      res should be(None)
    }
  }
}
