import no.uit.sfb.cbf.shared.model.v1.nativ.{CompactIRI, Evidence}
import org.scalatest.funspec.AnyFunSpec
import org.scalatest.matchers.should.Matchers

class EvidenceTest extends AnyFunSpec with Matchers {
  describe(classOf[Evidence].getName) {
    {
      val test = "§ECO:123"
      val descr = "parse evidence without source"
      it(s"should $descr: '$test'") {
        val evidence = Evidence(test)
        val defObj = evidence.predicate
        defObj.ciris should be(Seq(CompactIRI("eco", "123")))
        evidence.src.size should be(0)
      }
    }
    {
      val test = "§ECO:123>§doi:12345"
      val descr = "parse evidence with one source"
      it(s"should $descr: '$test'") {
        val evidence = Evidence(test)
        val defObj = evidence.predicate
        defObj.ciris should be(Seq(CompactIRI("eco", "123")))
        evidence.src.size should be(1)
        val srcObj = evidence.src.head
        srcObj.ciris should be(Seq(CompactIRI("doi", "12345")))
        srcObj.iri.size should be(1)
      }
    }
    {
      val test =
        " §ECO:123§http://example.com >§doi:12345 >https://example.com/12345 "
      val descr = "parse evidence with two sources"
      it(s"should $descr: '$test'") {
        val evidence = Evidence(test)
        val defObj = evidence.predicate
        defObj.ciris should be(Seq(CompactIRI("eco", "123")))
        defObj.iri.size should be(2)
        evidence.src.size should be(2)
        //Keep an empty line below

        {
          val srcObj = evidence.src.head
          srcObj.ciris should be(Seq(CompactIRI("doi", "12345")))
          srcObj.iri.size should be(1)
        }
        {
          val srcObj = evidence.src.tail.head
          srcObj.ciris.isEmpty should be(true)
          srcObj.ciris.size should be(0)
          srcObj.iri should be(Seq("https://example.com/12345"))
        }
      }
    }
  }
}
