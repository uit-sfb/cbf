import no.uit.sfb.cbf.shared.validate.TypeValidator
import org.scalatest.funspec.AnyFunSpec
import org.scalatest.matchers.should.Matchers

class TypeValidatorTest extends AnyFunSpec with Matchers {
  val typeValidator = new TypeValidator(
    Map("cat:enum:sex" -> "Male|Female|Unknown")
  )
  describe("TypeValidator") {
    {
      val test = "12345"
      val tpe = "num"
      it(s"should validate $tpe properly: '$test'") {
        typeValidator(tpe)(test).isEmpty should be(true)
      }
    }
    {
      val test = "-0.3456"
      val tpe = "num"
      it(s"should validate $tpe properly: '$test'") {
        typeValidator(tpe)(test).isEmpty should be(true)
      }
    }
    {
      val test = "-0.34.56"
      val tpe = "num"
      it(s"should validate $tpe properly: '$test'") {
        typeValidator(tpe)(test).isEmpty should be(false)
      }
    }
    {
      val test = "-12345"
      val tpe = "num:int"
      it(s"should validate $tpe properly: '$test'") {
        typeValidator(tpe)(test).isEmpty should be(true)
      }
    }
    {
      val test = "12345"
      val tpe = "num:int+"
      it(s"should validate $tpe properly: '$test'") {
        typeValidator(tpe)(test).isEmpty should be(true)
      }
    }
    {
      val test = "-12345"
      val tpe = "num:int+"
      it(s"should validate $tpe properly: '$test'") {
        typeValidator(tpe)(test).isEmpty should be(false)
      }
    }
    {
      val test = "2018"
      val tpe = "date"
      it(s"should validate $tpe properly: '$test'") {
        typeValidator(tpe)(test).isEmpty should be(true)
      }
    }
    {
      val test = "2018-12"
      val tpe = "date"
      it(s"should validate $tpe properly: '$test'") {
        typeValidator(tpe)(test).isEmpty should be(true)
      }
    }
    {
      val test = "2018-13"
      val tpe = "date"
      it(s"should validate $tpe properly: '$test'") {
        typeValidator(tpe)(test).isEmpty should be(false)
      }
    }
    {
      val test = "2018-12-01"
      val tpe = "date"
      it(s"should validate $tpe properly: '$test'") {
        typeValidator(tpe)(test).isEmpty should be(true)
      }
    }
    {
      val test = "2018-12-1"
      val tpe = "date"
      it(s"should validate $tpe properly: '$test'") {
        typeValidator(tpe)(test).isEmpty should be(false)
      }
    }
    {
      val test = ""
      val tpe = "date"
      it(s"should validate $tpe properly: '$test'") {
        typeValidator(tpe)(test).isEmpty should be(false)
      }
    }
    {
      val test = ""
      val tpe = "date"
      it(
        s"should validate $tpe properly empty string when allowEmptyString is true"
      ) {
        typeValidator(tpe, true)(test).isEmpty should be(true)
      }
    }
    {
      val test = "18.087,-34.55679"
      val tpe = "latlon"
      it(s"should validate $tpe properly: '$test'") {
        typeValidator(tpe)(test).isEmpty should be(true)
      }
    }
    {
      val test = "18.087, -34.55679"
      val tpe = "latlon"
      it(s"should validate $tpe properly: '$test'") {
        typeValidator(tpe)(test).isEmpty should be(false)
      }
    }
    {
      val test = "189.087,-34.55679"
      val tpe = "latlon"
      it(s"should validate $tpe properly: '$test'") {
        typeValidator(tpe)(test).isEmpty should be(false)
      }
    }
    {
      val test = "Female"
      val tpe = "cat:enum:sex"
      it(s"should validate $tpe properly: '$test'") {
        typeValidator(tpe)(test).isEmpty should be(true)
      }
    }
    {
      val test = "female"
      val tpe = "cat:enum:sex"
      it(s"should validate $tpe properly: '$test'") {
        typeValidator(tpe)(test).isEmpty should be(false)
      }
    }
    {
      val test = "female"
      val tpe = "undefined"
      it(s"should validate $tpe properly: '$test'") {
        typeValidator(tpe)(test).isEmpty should be(true)
      }
    }
  }
}
