import no.uit.sfb.cbf.shared.config.CustomResolver
import no.uit.sfb.cbf.shared.model.v1.nativ.CompactIRI
import no.uit.sfb.cbf.transform.resolv.{CachedResolutionLike, Resolution}
import org.scalatest.funspec.AnyFunSpec
import org.scalatest.matchers.should.Matchers

import scala.concurrent.Await
import scala.concurrent.duration._

class BtoResolverTest extends AnyFunSpec with Matchers {
  val resolv = Resolution.resolvers(Seq(CustomResolver("bto", "^\\d{7}$", Map("_iri" -> "https://identifiers.org/BTO:{id}"), Some("http://purl.obolibrary.org/obo/BTO_"))))
  describe("BTO resolver") {
    it(s"should get info") {
      val ciri = "BTO:0004755"
      val resolver = new CachedResolutionLike {
        val resolvers = resolv
      }
      val info = resolver.get(CompactIRI(ciri).get)
      val res = Await.result(info, 45.seconds)
      res.keySet should be(
        Set("val", "description", "_id", "_iri", "_key", "_type", "synonyms")
      )
      //println(res)
    }
    it(s"should return error") {
      val ciri = "BTO:9999999"
      val resolver = new CachedResolutionLike {
        val resolvers = resolv
      }
      val info = resolver.getOption(CompactIRI(ciri).get)
      val res = Await.result(info, 45.seconds)
      res should be(None)
    }
  }
}
