import no.uit.sfb.cbf.shared.model.v1.builder.RecordBuilder
import no.uit.sfb.cbf.shared.model.v1.nativ.Record

import java.nio.file.Paths
import no.uit.sfb.scalautils.common.FileUtils
import org.scalatest.funspec.AnyFunSpec
import org.scalatest.matchers.should.Matchers

class FromTsvTest extends AnyFunSpec with Matchers {
  private val tsvPath = Paths.get(getClass.getResource("/example.tsv").getPath)
  private val multiTsvPath =
    Paths.get(getClass.getResource("/example_multi.tsv").getPath)
  describe(classOf[Record].getName) {
    it(s"should modelize from TSV") {
      val tsv = new RecordBuilder()
        .toTsv(
          Seq(new RecordBuilder().fromSingleTsv(FileUtils.readFile(tsvPath)))
        )
      //println(tsv)
    }
    it(s"should modelize from multi Tsv") {
      val (ctx, it) = new RecordBuilder().fromTsv(multiTsvPath)
      val (records, errors) = it partition {
        _.isRight
      }
      errors.isEmpty should be(true)
      records foreach { _ =>
        () //We just make sure the iterator is consumed
      }
    }
  }
}
