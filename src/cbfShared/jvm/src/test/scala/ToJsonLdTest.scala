import no.uit.sfb.cbf.jsonld.DataRecord
import no.uit.sfb.cbf.shared.model.v1.builder.RecordBuilder
import no.uit.sfb.cbf.utils.JsonLdSerializer

import java.nio.file.Paths
import no.uit.sfb.scalautils.common.FileUtils
import org.scalatest.funspec.AnyFunSpec
import org.scalatest.matchers.should.Matchers

class ToJsonLdTest extends AnyFunSpec with Matchers {
  private val tsvPath = Paths.get(getClass.getResource("/example.tsv").getPath)
  private val multiTsvPath =
    Paths.get(getClass.getResource("/example_multi.tsv").getPath)
  describe("JsonLd") {
    it(s"should convert model to JsonLd") {
      val entry = new RecordBuilder().fromSingleTsv(FileUtils.readFile(tsvPath))
      val res = JsonLdSerializer.serialize(DataRecord(entry, None))
      //println(res)
    }
  }
}
