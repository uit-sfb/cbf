import no.uit.sfb.cbf.shared.config.CustomResolver
import no.uit.sfb.cbf.shared.model.v1.nativ.CompactIRI
import no.uit.sfb.cbf.transform.resolv.{CachedResolutionLike, Resolution}
import org.scalatest.funspec.AnyFunSpec
import org.scalatest.matchers.should.Matchers

import scala.concurrent.Await
import scala.concurrent.duration._

class BiotoolsResolverTest extends AnyFunSpec with Matchers {
  val resolv = Resolution.resolvers(
    Seq(
      CustomResolver(
        "biotools",
        "^[-A-Za-z0-9\\_]*$",
        Map("_iri" -> "https://identifiers.org/biotools:{id}")
      )
    )
  )
  describe("Biotools resolver") {
    it(s"should get info") {
      val ciri = "biotools:a5-miseq"
      val resolver = new CachedResolutionLike {
        val resolvers = resolv
      }
      val info = resolver.get(CompactIRI(ciri).get)
      val res = Await.result(info, 45.seconds)
      res.keySet should be(
        Set("val", "description", "_id", "_iri", "homepage", "_key", "_type")
      )
      //println(res)
    }
    it(s"should return error") {
      val ciri = "biotools:idontexist"
      val resolver = new CachedResolutionLike {
        val resolvers = resolv
      }
      val info = resolver.getOption(CompactIRI(ciri).get)
      val res = Await.result(info, 45.seconds)
      res should be(None)
    }
  }
}
