import no.uit.sfb.cbf.shared.config.{AttributeDef, DatasetConfig}
import no.uit.sfb.cbf.utils.UriUtils

import java.nio.file.Paths
import org.scalatest.funspec.AnyFunSpec
import org.scalatest.matchers.should.Matchers
import no.uit.sfb.scalautils.common.FileUtils

class UiConfigTest extends AnyFunSpec with Matchers {

  describe("UiConfig") {
    val configUri =
      Paths.get("dataToolset/src/test/resources/config/test1-config.json").toUri
    val datasetConfig = DatasetConfig(UriUtils.getContent(configUri))
    it("should get both files and create the output") {
      datasetConfig.attrs
    }
  }
}
