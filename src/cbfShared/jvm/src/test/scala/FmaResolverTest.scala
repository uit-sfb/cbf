import no.uit.sfb.cbf.shared.config.CustomResolver
import no.uit.sfb.cbf.shared.model.v1.nativ.CompactIRI
import no.uit.sfb.cbf.transform.resolv.{CachedResolutionLike, Resolution}
import org.scalatest.funspec.AnyFunSpec
import org.scalatest.matchers.should.Matchers

import scala.concurrent.Await
import scala.concurrent.duration._

class FmaResolverTest extends AnyFunSpec with Matchers {
  val resolv = Resolution.resolvers(Seq(CustomResolver("fma", "^\\d+$", Map("_iri" -> "https://identifiers.org/FMA:{id}"), Some("http://purl.obolibrary.org/obo/FMA_"))))
  describe("FMA resolver") {
    it(s"should get info") {
      val ciri = "FMA:67112"
      val resolver = new CachedResolutionLike {
        val resolvers = resolv
      }
      val info = resolver.get(CompactIRI(ciri).get)
      val res = Await.result(info, 45.seconds)
      res.keySet should be(
        Set("val", "description", "_id", "_type", "_iri", "_key")
      )
      //println(res)
    }
    it(s"should return error") {
      val ciri = "FMA:99999999"
      val resolver = new CachedResolutionLike {
        val resolvers = resolv
      }
      val info = resolver.getOption(CompactIRI(ciri).get)
      val res = Await.result(info, 45.seconds)
      res should be(None)
    }
  }
}
