import no.uit.sfb.cbf.shared.config.DatasetConfig
import no.uit.sfb.cbf.shared.model.v1.builder.RecordBuilder
import no.uit.sfb.cbf.shared.model.v1.nativ.Record
import no.uit.sfb.cbf.utils.UriUtils

import java.nio.file.Paths
import no.uit.sfb.scalautils.common.FileUtils
import org.scalatest.funspec.AnyFunSpec
import org.scalatest.matchers.should.Matchers

import scala.io.Source

class FromJsonTest extends AnyFunSpec with Matchers {
  private val jsonPath =
    Paths.get(getClass.getResource("/example.json").getPath)
  private val multiJsonPath =
    Paths.get(getClass.getResource("/example_multi.json").getPath)
  private val configUri =
    Paths.get("dataToolset/src/test/resources/config/test1-config.json").toUri

  private val datasetConfig = DatasetConfig(UriUtils.getContent(configUri))

  describe(classOf[Record].getName) {
    it(s"should modelize from JSON") {
      val rec = new RecordBuilder().fromSingleJson(FileUtils.readFile(jsonPath))
      //println(rec)
    }
    it(s"should modelize from multi JSON") {
      val rb = RecordBuilder.fromUrl(configUri.toString)
      val tsv = rb.toTsv(
        rb.fromJson(
          Source.fromFile(multiJsonPath.toFile).getLines().mkString("\n")
        )
      )
      //println(tsv)
    }
  }
}
