import no.uit.sfb.cbf.shared.utils.ListUtils
import org.scalatest.funspec.AnyFunSpec
import org.scalatest.matchers.should.Matchers

class ListUtilsTest extends AnyFunSpec with Matchers {
  def replaceTemplates(str: String): String = {
    val regexp = "\\{.*?\\}".r
    val sep = 0.toChar //Unlikely to find this one in a string
    val l1 = regexp.replaceAllIn(str, sep.toString).split(sep).toList
    val l2 = regexp
      .findAllIn(str)
      .map { _.toUpperCase() }
      .toList
    ListUtils.interleave(l1, l2).mkString("")
  }
  describe("ListUtils") {
    it("should properly interleave") {
      replaceTemplates("{xxx}bbb{yyy}") should be("{XXX}bbb{YYY}")
      replaceTemplates("aaa{xxx}bbb{yyy}ccc") should be("aaa{XXX}bbb{YYY}ccc")
      replaceTemplates("{{xxx}yyy}") should be("{{XXX}yyy}") //replaceTemplate is not recursive
    }
  }
}
