import no.uit.sfb.cbf.shared.model.v1.nativ.CompactIRI
import org.scalatest.funspec.AnyFunSpec
import org.scalatest.matchers.should.Matchers

class CompactIriTest extends AnyFunSpec with Matchers {
  describe(classOf[CompactIRI].getName) {
    {
      val test = "ECO:123"
      val descr = "parse ciri"
      it(s"should $descr: '$test'") {
        CompactIRI(test).nonEmpty should be(true)
      }
    }
    {
      val test = "ECO+kl:123"
      val descr = "not parse ciri"
      it(s"should $descr: '$test'") {
        CompactIRI(test).nonEmpty should be(false)
      }
    }
    {
      val test = "ena.embl:123.45_kl-lk"
      val descr = "parse ciri"
      it(s"should $descr: '$test'") {
        CompactIRI(test).nonEmpty should be(true)
      }
    }
  }
}
