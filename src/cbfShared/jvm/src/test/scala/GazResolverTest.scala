import no.uit.sfb.cbf.shared.config.CustomResolver
import no.uit.sfb.cbf.shared.model.v1.nativ.CompactIRI
import no.uit.sfb.cbf.transform.resolv.{CachedResolutionLike, Resolution}
import org.scalatest.funspec.AnyFunSpec
import org.scalatest.matchers.should.Matchers

import scala.concurrent.Await
import scala.concurrent.duration._

class GazResolverTest extends AnyFunSpec with Matchers {
  val resolv = Resolution.resolvers(Seq(CustomResolver("gaz", "", Map("_iri" -> "https://www.ebi.ac.uk/ols/ontologies/gaz/terms?iri=http%3A%2F%2Fpurl.obolibrary.org%2Fobo%2FGAZ_{id}"), Some("http://purl.obolibrary.org/obo/GAZ_"))))
  describe("GAZ resolver") {
    it(s"should get info") {
      val ciri = "GAZ:00634670"
      val resolver = new CachedResolutionLike {
        val resolvers = resolv
      }
      val info = resolver.get(CompactIRI(ciri).get)
      val res = Await.result(info, 45.seconds)
      res.keySet should be(
        Set(
          "val",
          //"description",
          "country",
          "world_region",
          "_id",
          "_type",
          "_iri",
          //"synonyms", //This particular entry doesn't have any synonym
          "_key"
        )
      )
      res.get("country") should be(Some("gaz:00002646"))
      res.get("world_region") should be(Some("gaz:00000464"))
      //println(res)
    }
    it(s"should return error") {
      val ciri = "GAZ:99999999"
      val resolver = new CachedResolutionLike {
        val resolvers = resolv
      }
      val info = resolver.getOption(CompactIRI(ciri).get)
      val res = Await.result(info, 45.seconds)
      res should be(None)
    }
  }
}
