import no.uit.sfb.cbf.shared.utils.Str
import org.scalatest.funspec.AnyFunSpec
import org.scalatest.matchers.should.Matchers

class StrTest extends AnyFunSpec with Matchers {
  describe("Str") {
    {
      val test = "abc|def\\|ijk"
      val descr = "split properly"
      it(s"should $descr: '$test'") {
        Str.split(test, '|') should be(Seq("abc", "def|ijk"))
      }
    }
    {
      val test = "\\|abc\\|def\\|ijk\\|"
      val descr = "split properly"
      it(s"should $descr: '$test'") {
        Str.split(test, '|') should be(Seq("|abc|def|ijk|"))
      }
    }
  }
}
