import no.uit.sfb.cbf.utils.graph._
import org.scalatest.funspec.AnyFunSpec
import org.scalatest.matchers.should.Matchers

class OrientedGraphTest extends AnyFunSpec with Matchers {
  val graph = new OrientedGraph(
    Set(
      Node("A", Set()),
      Node("B", Set(Edge("B->C", "C"), Edge("B->B", "B"), Edge("B->D", "D"))),
      Node("C", Set(Edge("C->B", "B"), Edge("C->D", "D"), Edge("C->F", "F"))),
      Node("D", Set(Edge("D->B", "B"), Edge("D->E", "E"))),
      Node("E", Set(Edge("E->F", "F"), Edge("E->C", "C"))),
    )
  )
  describe(classOf[OrientedGraph].getName) {
    it("should find no path A -> A") {
      graph.computePath("A", "A") should be(None)
    }
    it("should find path B -> B") {
      graph.computePath("B", "B") should be(Some(Seq(Edge("B->B", "B"))))
    }
    it("should find path B -> F") {
      graph.computePath("B", "F") should be(
        Some(Seq(Edge("B->C", "C"), Edge("C->F", "F")))
      )
    }
    it("should find no path F -> B") {
      graph.computePath("F", "B") should be(None)
    }
  }
}
