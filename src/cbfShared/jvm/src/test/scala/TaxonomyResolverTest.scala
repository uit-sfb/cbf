import no.uit.sfb.cbf.shared.config.CustomResolver
import no.uit.sfb.cbf.shared.model.v1.nativ.CompactIRI
import no.uit.sfb.cbf.transform.resolv.{CachedResolutionLike, Resolution}
import org.scalatest.funspec.AnyFunSpec
import org.scalatest.matchers.should.Matchers

import scala.concurrent.{Await, ExecutionContext}
import scala.concurrent.duration._
import scala.util.{Failure, Success}

class TaxonomyResolverTest extends AnyFunSpec with Matchers {
  implicit val ec =  ExecutionContext.global
  val resolv = Resolution.resolvers(
    Seq(
      CustomResolver(
        "taxonomy",
        "^\\d+$",
        Map("_iri" -> "https://identifiers.org/taxonomy:{id}"),
        Some("http://purl.obolibrary.org/obo/NCBITaxon#")
      )
    )
  )
  describe("Taxonomy resolver") {
    it(s"should get info") {
      val ciri = "taxonomy:2697049" //1873 //2697049
      val resolver = new CachedResolutionLike {
        val resolvers = resolv
      }
      val info = resolver.get(CompactIRI(ciri).get)

      val res = Await.result(info, 45.seconds)
      res.keySet should be(
        Set(
          "val",
          "_id",
          "_iri",
          "_key",
          "_type",
          "synonyms",
          "species",
          "subgenus",
          "genus",
          "subfamily",
          "clade",
          "class",
          "phylum",
          "kingdom",
          "family",
          "suborder",
          "order",
          "superkingdom"
        )
      )
    }
    it(s"should return error") {
      val ciri = "taxonomy:9999999"
      val resolver = new CachedResolutionLike {
        val resolvers = resolv
      }
      val info = resolver.getOption(CompactIRI(ciri).get)
      val res = Await.result(info, 45.seconds)
      res should be(None)
    }
  }
}
