import no.uit.sfb.cbf.shared.model.v1.nativ.{CompactIRI, Statement, ValueRef}
import org.scalatest.funspec.AnyFunSpec
import org.scalatest.matchers.should.Matchers

class StatementTest extends AnyFunSpec with Matchers {
  describe(classOf[Statement].getName) {
    {
      val test = ""
      val descr = "parse an empty value"
      it(s"should $descr: '$test'") {
        val dataObj = Statement(test)
        dataObj.obj.size should be(0)
        dataObj.evi.size should be(0)
      }
    }
    {
      val test = "lung"
      val descr = "parse a value without def or evidence"
      it(s"should $descr: '$test'") {
        val dataObj = Statement(test)
        dataObj.obj.size should be(1)
        dataObj.evi.size should be(0)
        dataObj.obj.head should be(ValueRef("lung").get)
        dataObj.evi.size should be(0)
      }
    }
    {
      val test = "lung§FMA:123"
      val descr = "parse a value with def without evidence"
      it(s"should $descr: '$test'") {
        val dataObj = Statement(test)
        dataObj.obj.size should be(1)
        dataObj.evi.size should be(0)
        val obj = dataObj.obj.head
        obj.`val` should be("lung")
        obj.iri.nonEmpty should be(true)
        obj.ciris should be(Seq(CompactIRI("fma", "123")))
      }
    }
    {
      val test = "§FMA:123"
      val descr = "parse a value with def without evidence"
      it(s"should $descr: '$test'") {
        val dataObj = Statement(test)
        dataObj.obj.size should be(1)
        dataObj.evi.size should be(0)
        val obj = dataObj.obj.head
        obj.`val` should be("")
        obj.iri.nonEmpty should be(true)
        obj.ciris should be(Seq(CompactIRI("fma", "123")))
      }
    }
    {
      val test = "https://example.com"
      val descr = "interpret URL without leading §"
      it(s"should $descr: '$test'") {
        val dataObj = Statement(test)
        dataObj.obj.size should be(1)
        dataObj.evi.size should be(0)
        val obj = dataObj.obj.head
        obj.`val` should be("")
        obj.iri.nonEmpty should be(true)
        obj.iri should be(Seq("https://example.com"))
      }
    }
    {
      val test = "lung§FMA:123<§ECO:456>§doi:789"
      val descr = "parse a value with def and evidence"
      it(s"should $descr: '$test'") {
        val dataObj = Statement(test)
        dataObj.obj.size should be(1)
        val obj = dataObj.obj.head
        obj.`val` should be("lung")
        obj.iri.nonEmpty should be(true)
        obj.ciris should be(Seq(CompactIRI("fma", "123")))
        dataObj.evi.size should be(1)
        val evidence = dataObj.evi.head
        evidence.predicate.ciris should be(Seq(CompactIRI("eco", "456")))
        evidence.src.size should be(1)
        evidence.src.head.ciris should be(Seq(CompactIRI("doi", "789")))
      }
    }
    {
      val test = "lung§FMA:123>gnul§FMA:321<§ECO:654>§doi:987"
      val descr = "parse two values with def and evidence"
      it(s"should $descr: '$test'") {
        val dataObj = Statement(test)
        dataObj.obj.size should be(2)
        dataObj.evi.size should be(1)
        val evidence = dataObj.evi.head
        evidence.predicate.ciris should be(Seq(CompactIRI("eco", "654")))
        evidence.src.size should be(1)
        evidence.src.head.ciris should be(Seq(CompactIRI("doi", "987")))
        //Keep empty line below

        {
          val obj = dataObj.obj.head
          obj.`val` should be("lung")
          obj.iri.nonEmpty should be(true)
          obj.ciris should be(Seq(CompactIRI("fma", "123")))
        }
        {
          val obj = dataObj.obj.tail.head
          obj.`val` should be("gnul")
          obj.iri.nonEmpty should be(true)
          obj.ciris should be(Seq(CompactIRI("fma", "321")))
        }
      }
    }
  }
}
