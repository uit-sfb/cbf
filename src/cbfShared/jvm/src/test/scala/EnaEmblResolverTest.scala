import no.uit.sfb.cbf.shared.config.CustomResolver
import no.uit.sfb.cbf.shared.model.v1.nativ.CompactIRI
import no.uit.sfb.cbf.transform.resolv.{Resolution, ResolutionLike}
import org.scalatest.funspec.AnyFunSpec
import org.scalatest.matchers.should.Matchers

import scala.concurrent.Await
import scala.concurrent.duration._

class EnaEmblResolverTest extends AnyFunSpec with Matchers {
  val resolv = Resolution.resolvers(Seq(CustomResolver("ena.embl", "^([A-Z]+[0-9]+)(?:\\.\\d+)?$", Map("_iri" -> "https://identifiers.org/ena.embl:{id}", "raw_id" -> "{1}"))))
  describe("EnaEmbl resolver") {
    {
      it(s"should get info") {
        val ciri = "ena.embl:LC521925.1"
        val resolver = new ResolutionLike {
          val resolvers = resolv
        }
        val info = resolver.get(CompactIRI(ciri).get)
        val res = Await.result(info, 45.seconds)
        res.keySet should be(
          Set("_type", "_id", "_iri", "_key", "raw_id", "val")
        ) //No val here since we are not using an instance of PersistedCache
        //println(res)
        res.getOrElse("raw_id", "nothing") should be("LC521925")
      }
    }
  }
}
