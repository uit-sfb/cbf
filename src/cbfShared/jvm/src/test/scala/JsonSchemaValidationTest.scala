import no.uit.sfb.cbf.jsonschema.Validator

import java.nio.file.Paths
import org.scalatest.funspec.AnyFunSpec
import org.scalatest.matchers.should.Matchers

class JsonSchemaValidationTest extends AnyFunSpec with Matchers {
  private val recordPath =
    Paths.get(getClass.getResource("/example.json").getPath)
  describe(
    "Json example, describing proj attribute validated against json schema"
  ) {
    it("should validate using UrlValidator") {
      val schemaPath = Paths
        .get(s"dataToolset/src/main/resources/schema/record.schema.json")
        .toAbsolutePath
      new Validator(schemaPath.toFile.toURI).validate(recordPath)
    }
  }
}
