import java.nio.file.Paths
import org.scalatest.funspec.AnyFunSpec
import org.scalatest.matchers.should.Matchers
import io.circe.generic.auto._
import io.circe.syntax._
import no.uit.sfb.cbf.shared.model.v1.builder.RecordBuilder
import no.uit.sfb.scalautils.common.FileUtils

class ToJsonTest extends AnyFunSpec with Matchers {
  private val tsvPath = Paths.get(getClass.getResource("/example.tsv").getPath)
  private val multiTsvPath =
    Paths.get(getClass.getResource("/example_multi.tsv").getPath)
  describe("Json") {
    it(s"should convert model to Json") {
      val entry = new RecordBuilder().fromSingleTsv(FileUtils.readFile(tsvPath))
      val res = entry.asJson.spaces2
      //println(res)
    }
    it(s"should convert multi to Json") {
      val (ctx, it) = new RecordBuilder().fromTsv(multiTsvPath)
      val (records, errors) = it partition {
        _.isRight
      }
      errors.isEmpty should be(true)
      val res =
        new RecordBuilder().toJson(records map {
          _.getOrElse(throw new Exception("Should not happen"))._1
        })
      //println(res)
    }
  }
}
