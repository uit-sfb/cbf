import no.uit.sfb.cbf.shared.model.v1.builder.RecordBuilder
import no.uit.sfb.cbf.shared.model.v1.nativ.Record
import org.scalatest.funspec.AnyFunSpec
import org.scalatest.matchers.should.Matchers
import no.uit.sfb.cbf.shared.model.v1.nativ.Attribute._

class RecordTest extends AnyFunSpec with Matchers {
  describe(classOf[Record].getName) {
    {
      val test = "attr1\tlung\ncol:attr2\tgnul"
      val descr = "parse tsv"
      it(s"should $descr: '$test'") {
        val entry = new RecordBuilder().fromSingleTsv(test)
        entry.attrs.size should be(3) //2 + the id which is automatically added
        //Keep empty line below

        {
          val prop = entry.attributes.head
          prop._1.name should be("attr1")
          prop._1.collectionName should be("")
          prop._2.obj.head.`val` should be("lung")
        }
        {
          val prop = entry.attributes.tail.head
          prop._1.name should be("attr2")
          prop._1.collectionName should be("col")
          prop._2.obj.head.`val` should be("gnul")
        }
      }
    }
  }
}
