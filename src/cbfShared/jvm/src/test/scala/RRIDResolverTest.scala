import no.uit.sfb.cbf.shared.config.CustomResolver
import no.uit.sfb.cbf.shared.model.v1.nativ.CompactIRI
import no.uit.sfb.cbf.transform.resolv.{CachedResolutionLike, Resolution}
import org.scalatest.funspec.AnyFunSpec
import org.scalatest.matchers.should.Matchers

import scala.concurrent.Await
import scala.concurrent.duration._

class RRIDResolverTest extends AnyFunSpec with Matchers {
  val resolv = Resolution.resolvers(Seq(CustomResolver("rrid", "^[a-zA-Z]+.+$", Map("_iri" -> "https://identifiers.org/RRID:{id}"))))
  describe("RRID resolver") {
    it(s"should get info") {
      val ciri = "RRID:SCR_010233"
      val resolver = new CachedResolutionLike {
        val resolvers = resolv
      }
      val info = resolver.get(CompactIRI(ciri).get)
      val res = Await.result(info, 45.seconds)
      res.keySet should be(
        Set("val", "_id", "_iri", "_key", "_type", "synonyms")
      )
      res("val") should be("Illumina")
      //println(res)
    }
    it(s"should return error") {
      val ciri = "RRID:SCR_999999"
      val resolver = new CachedResolutionLike {
        val resolvers = resolv
      }
      val info = resolver.getOption(CompactIRI(ciri).get)
      val res = Await.result(info, 45.seconds)
      res should be(None)
    }
  }
}
