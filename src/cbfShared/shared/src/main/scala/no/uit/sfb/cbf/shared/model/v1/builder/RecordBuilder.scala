package no.uit.sfb.cbf.shared.model.v1.builder

import no.uit.sfb.cbf.shared.config.AttributeDef
import no.uit.sfb.cbf.shared.model.ModelLike
import no.uit.sfb.cbf.shared.model.v1.nativ.Record

import scala.io.Source
import scala.util.{Failure, Success, Using}

class RecordBuilder(val attributes: Seq[AttributeDef] = Seq(),
                    val valueSeparator: Char = '\t',
                    val subValueSeparators: Seq[Char] = Seq('|'))
    extends ModelLike[Record]
    with OrderedLike
    with TsvSupportLike
    with JsonSupportLike
    with DuplicateDetectLike {
  lazy val orderedKeys: Seq[String] = attributes.map { _.ref }
}

object RecordBuilder {
  def fromUrl(url: String): RecordBuilder = {
    val io = Source.fromURL(url)
    Using(io){io =>
      val lines = io.getLines()
      lines.next() //We remove the header
      val keys = lines flatMap {
        _.split('\t').headOption
      }
      val rb = new RecordBuilder(keys.map { k =>
        AttributeDef(k, k)
      }.toSeq)
      rb
    } match {
      case Success(rb) => rb
      case Failure(exception) => throw exception
    }
//    io.close()
//        rb
  }
}
