package no.uit.sfb.cbf.shared.utils

import java.time.{LocalDateTime, ZoneId}

object Date {
  def now() = LocalDateTime.now(ZoneId.of("UTC"))

  def nowDate(): String = {
    //We need to specify ZoneId UTC, otherwise it uses the system one which breaks in Scalajs since timezones must be downloaded as a separate library
    LocalDateTime.now(ZoneId.of("UTC")).toString.take(10)
  }
  def nowDateTime(): String = {
    //We need to specify ZoneId UTC, otherwise it uses the system one which breaks in Scalajs since timezones must be downloaded as a separate library
    LocalDateTime.now(ZoneId.of("UTC")).toString.take(19) + "Z"
  }
}
