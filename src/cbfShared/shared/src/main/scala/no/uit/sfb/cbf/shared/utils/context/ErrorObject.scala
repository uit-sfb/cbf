package no.uit.sfb.cbf.shared.utils.context

import no.uit.sfb.cbf.shared.model.v1.nativ.Record

import scala.util.Try

case class ErrorObject(stage: String,
                       source: String, //header + record (i.e 2 lines TSV)
                       obj: Option[Record],
                       annotations: Seq[ErrorAnnotation]) {
  private lazy val id: Option[String] = obj.flatMap { o =>
    Try { o.getId }.toOption
  }

  lazy val withExtendedAnnotations: ErrorObject = {
    copy(annotations = obj match {
      case Some(o) if Try { o.getId }.toOption.isEmpty =>
        annotations :+ ErrorAnnotation(
          "'id' (or the attr on which 'id' is based on via a template) is missing. Or maybe you would like to patch instead of push?"
        )
      case _ =>
        annotations
    })
  }

  def asString = {
    s"Error while $stage" + (id match {
      case Some(i) =>
        s" record '$i':\n"
      case None => ":\n"
    }) + annotations
      .map {
        _.asString
      }
      .mkString("\n")
  }
}
