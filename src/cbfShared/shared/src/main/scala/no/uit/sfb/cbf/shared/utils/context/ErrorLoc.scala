package no.uit.sfb.cbf.shared.utils.context

case class ErrorLoc(line: Int, item: Int, pos: Int) {
  lazy val asString = s"line $line (record #$item), tab $pos"
}
