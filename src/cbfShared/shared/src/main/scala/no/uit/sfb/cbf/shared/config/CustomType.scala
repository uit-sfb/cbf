package no.uit.sfb.cbf.shared.config

case class CustomType(ref: String, kvp: Map[String, String]) {
  def apply(key: String): Option[String] = kvp.get(key)
  lazy val lt = LineagedType(ref)
}

