package no.uit.sfb.cbf.shared.model.v1.builder

import no.uit.sfb.cbf.shared.model.v1.nativ.Record

import scala.annotation.tailrec

trait DuplicateDetectLike {
  this: RecordBuilder =>

  def isTemplate(id: String) = {
    id.contains('{')
  }

  protected def checkDuplicates(records: Seq[Record]): Unit = {
    val ids: Seq[String] = records.flatMap {
      _.id
    }

    @tailrec
    def checkDuplicatesImpl(recs: Seq[String],
                            duplicates: List[String] = List()): Seq[String] = {
      if (recs.isEmpty)
        duplicates
      else {
        val add =
          if (!isTemplate(recs.head) && recs.tail.contains(recs.head))
            recs.head :: duplicates
          else
            duplicates
        checkDuplicatesImpl(recs.tail, add)
      }
    }

    val duplicates = checkDuplicatesImpl(ids)

    if (duplicates.nonEmpty)
      throw new Exception(
        s"The following ids have duplicates: ${duplicates.toSet.mkString(", ")}"
      )
  }
}
