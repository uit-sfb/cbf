package no.uit.sfb.cbf.shared.dataset

import no.uit.sfb.cbf.shared.errors.BadRequestClientError

case class DatasetRegistrationForm(name: String = "",
                                   description: String = "",
                                   dmp: String = "")

object DatasetRegistrationForm {
  val forbiddenDatabaseNameCharacters = Seq('\u0000', '/', '\\', ' ', '"', '.')
  def validate(drf: DatasetRegistrationForm): Unit = {
    if (drf.name.isEmpty)
      throw BadRequestClientError(s"A dataset name may not be empty.")
    else
      forbiddenDatabaseNameCharacters foreach { c =>
        if (drf.name.contains(c))
          throw BadRequestClientError(
            s"A dataset name may not contains any of the following characters: ${forbiddenDatabaseNameCharacters
              .map { _.toString }
              .map {
                case c if c == " "       => "<space>"
                case c if c == "\\u0000" => "\\0"
                case c                   => c
              }
              .mkString(" ")}, but got '${drf.name}'."
          )
      }
  }
}
