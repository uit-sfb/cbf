package no.uit.sfb.cbf.shared.jsonapi

import io.circe.Encoder

trait ResponseLike {
  def data: Any
  def links: Map[String, Option[String]]
  def errors: Seq[Err]
  def meta: Map[String, String]
}

object ResponseLike {
  val contentType = "application/vnd.api+json"
}

case class SingleResponse[T: Encoder](
  data: Option[Data[T]],
  links: Map[String, Option[String]] = Map(),
  errors: Seq[Err] = Seq(),
  meta: Map[String, String] = Map()
) extends ResponseLike

case class MultipleResponse[T: Encoder](data: Seq[Data[T]],
                                        links: Map[String, Option[String]] =
                                          Map(),
                                        errors: Seq[Err] = Seq(),
                                        meta: Map[String, String] = Map())
    extends ResponseLike

case class ErrorResponse(errors: Seq[Err], meta: Map[String, String] = Map())
    extends ResponseLike {
  lazy val data = null
  lazy val links = Map()
}
