package no.uit.sfb.cbf.shared.config

import io.circe.generic.extras.Configuration
import no.uit.sfb.cbf.shared.config.ui.TabObject
import no.uit.sfb.cbf.shared.model.v1.role.RoleOrdering

case class DatasetConfig(
  _id: String = "default",
  name: String = "", //Dataset name
  description: String = "",
  contextUrl: String = "", //External URL where the database can be accessed
  dmp: String = "", //DMP URL
  schema: String = "", //Path or URL
  latestOnly: Boolean = false, //If true, the version select is not displayed
  releaseFromExternal: Boolean = false, //If true, the release button is disabled as releases are created from an external pipeline
  grantReadAccess: String = "anonymous", //anonymous, none or reader
  header: String = "id", //Ref of the attribute to use as header in table view
  valueSeparator: String = "\t", //Used for parsing from TSV only. Attention: only one character allowed
  subValueSeparator: String = "|", //Used for parsing from TSV only. Multiple characters allowed
  tableAttributes: Seq[String] = Seq(), //If empty, all the attributes are shown
  tabs: Seq[Tab] = Seq(), //If empty, a tab "Attributes" shows all the attributes
  attrs: Seq[AttributeDef] = Seq(),
  customResolvers: Seq[CustomResolver] = Seq(),
  customTypes: Seq[CustomType] = Seq(),
) {
  assert(
    RoleOrdering.lteq(grantReadAccess, "reader"),
    s"grantReadAccess ('$grantReadAccess') must be <= 'reader'."
  )
  val valueSeparatorChar = valueSeparator.headOption.getOrElse('\t')
  val subValueSeparators =
    if (subValueSeparator.isEmpty) Seq('|')
    else subValueSeparator.toCharArray.toSeq
  val (persistedAttributes, virtualAttributes) = attrs.partition(!_.virtual)

  lazy val keys: Vector[String] = persistedAttributes.map {
    _.ref
  }.toVector

  def tab(name: String): Option[Tab] = tabs.find(_.name == name)

  def tabObj(name: String): Option[TabObject] = tab(name) map { t =>
    TabObject(
      t.name,
      (if (t.attrs.isEmpty)
         keys
       else
         t.attrs).flatMap {
        attr
      }
    )
  }

  def tabObj(refs: Seq[String]): TabObject =
    TabObject(
      "",
      (if (refs.isEmpty)
         keys
       else
         refs).flatMap {
        attr
      }
    )

  def attr(ref: String): Option[AttributeDef] =
    attrs.find(_.ref == ref)

  //Resource attributes are attributes that have one resolver whose prefix starts with "resource_"
  //For performance reason, resource attributes MUST be persisted (i.e. not virtual).
  //See ResourceController.fetchResourceCiris
  lazy val resourceAttributes =
    persistedAttributes.filter(_.resolvers.exists(_.startsWith("resource_")))

  lazy val resourceAttributesWithModes: Seq[(AttributeDef, Seq[String])] = {
    resourceAttributes.map { attr =>
      val resolverRef = attr.resolvers.find(_.startsWith("resource_")).get //We know there is at least one because we use resourceAttributes
      val modes = customResolvers.find(_.prefix == resolverRef).toSeq flatMap {
        resolv =>
          val modes = resolv.kvp
            .getOrElse("+modes", "")
            .split('|')
            .toSeq
            .distinct
            .filter { _.nonEmpty }
          if (modes.isEmpty)
            Seq("default")
          else
            modes
      }
      attr -> modes
    }
  }

  /**
    * Returns the ref of the matching link attribute if any
    * @param requestingDsName Dataset name of the requesting dataset
    * @return
    */
  def getMatchingLinkAttrRef(requestingDsName: String): Option[String] = {
    //DbLink type matching the requesting dsName (should be at most one)
    customTypes
      .find(
        ct =>
          ct.lt
            .isSubtypeOf("dblink:direct") && ct.lt.tertiary == requestingDsName
      )
      .flatMap { t =>
        attrs
          .find { attr =>
            attr.`type` == t.ref
          } //Should be at most one
          .map { _.ref }
      }
  }
}

object DatasetConfig {

  def apply(str: String): DatasetConfig = {
    import io.circe.parser.decode
    import io.circe.generic.extras.auto._
    implicit val config: Configuration = Configuration.default.withDefaults

    decode[DatasetConfig](str).toTry.get
  }
}
