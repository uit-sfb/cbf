package no.uit.sfb.cbf.shared.validate

import no.uit.sfb.cbf.shared.types.InternalType

class TypeValidator(enums: Map[String, String]) {
  def valid(ref: String): String => Option[String] = {
    ref match {
      case x if enums.keySet.contains(x) =>
        str =>
          if (enums(x).split('|').contains(str))
            None
          else
            Some(
              s"Value '$str' is not of type '$ref' (enum=[${enums(x).split('|').mkString(", ")}])."
            )
      case x =>
        str =>
          InternalType(x).flatMap { _.valid(str) }
    }
  }

  def apply(ref: String,
            allowEmptyString: Boolean = false): String => Option[String] = {
    str =>
      {
        if (str.trim.isEmpty) {
          if (allowEmptyString)
            None
          else
            Some("This field is compulsory.")
        } else
          valid(ref)(str)
      }
  }
}
