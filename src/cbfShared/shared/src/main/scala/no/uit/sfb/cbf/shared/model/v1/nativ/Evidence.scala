package no.uit.sfb.cbf.shared.model.v1.nativ

import no.uit.sfb.cbf.shared.utils.Str

/**
  * Represents an evidence as a predicate + sources
  */
case class Evidence(`val`: String,
                    iri: Seq[String],
                    src: Set[ValueRef],
                    meta: Map[String, String] = Map())
    extends EvidenceRefLike {
  lazy val predicate = ValueRef(`val`, iri)

  override lazy val asString: String = {
    super.asString ++ (src.map { seq =>
      s">${seq.asString}"
    }).mkString
  }

  lazy val isEmpty = this == Evidence("", Seq(), Set())
}

object Evidence {
  def apply(str: String): Evidence =
    Str.split(str, '>') match {
      case Nil => throw new Exception("Evidence '<' must not be empty")
      case head :: tail =>
        val predicate = ValueRef(head).orElse(ValueRef("§eco:0000000")).get
        val sources: Set[ValueRef] = tail.flatMap {
          ValueRef(_)
        }.toSet
        Evidence(predicate.`val`, predicate.iri, sources)
    }
}
