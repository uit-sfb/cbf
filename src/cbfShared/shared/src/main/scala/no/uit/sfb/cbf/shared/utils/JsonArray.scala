package no.uit.sfb.cbf.shared.utils

object JsonArray {
  def toJson[A](recs: Iterator[A], transform: A => String): Iterator[String] = {
    val temp = recs map { rec =>
      val str = transform(rec)
      if (recs.hasNext)
        str + ",\n"
      else
        str
    }
    Iterator("[\n") ++ temp ++ Iterator("\n]")
  }
}
