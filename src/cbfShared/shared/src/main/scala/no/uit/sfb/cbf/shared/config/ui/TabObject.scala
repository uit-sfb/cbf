package no.uit.sfb.cbf.shared.config.ui

import no.uit.sfb.cbf.shared.config.AttributeDef

case class TabObject(name: String, attrs: Seq[AttributeDef])
