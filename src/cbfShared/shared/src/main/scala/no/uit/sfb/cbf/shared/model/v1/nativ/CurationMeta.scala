package no.uit.sfb.cbf.shared.model.v1.nativ

case class CurationMeta(tag: Seq[String] = Seq())
