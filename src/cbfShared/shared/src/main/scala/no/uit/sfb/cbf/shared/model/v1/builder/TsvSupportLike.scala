package no.uit.sfb.cbf.shared.model.v1.builder

import no.uit.sfb.cbf.shared.model.v1.nativ.{Record, Statement}
import no.uit.sfb.cbf.shared.utils.context.{
  ErrorAnnotation,
  ErrorLoc,
  ErrorObject,
  ParsingContext
}
import no.uit.sfb.cbf.shared.utils.{IteratorWithCallback, Str, Tsv}

import java.nio.file.Path
import scala.annotation.tailrec
import scala.io.{BufferedSource, Source}

trait TsvSupportLike {
  this: RecordBuilder =>

  protected def getRef(k: String): String = {
    attributes
      .find(_.allNames.contains(k))
      .map { _.ref }
      .getOrElse(k)
  }

  //Note that we expect headers as first column and data as second column
  def fromSingleTsv(str: String): Record = {
    val attrs: Vector[(String, String)] = Tsv
      .cleanIterator(str)
      .flatMap { item =>
        Str.split(item, valueSeparator) match {
          case Nil           => None
          case k :: Nil      => Some(k -> "")
          case k :: v :: Nil => Some(k -> v)
          case x @ _ =>
            throw new Exception(
              s"A line should contain exactly one key followed by one value, but '${x
                .mkString("\t")}' found."
            )
        }
      }
      .toVector
    Record(attrs.flatMap {
      case (k, v) =>
        val key = getRef(k)
        subValueSeparators
          .foldLeft(List(v)) {
            case (acc, c) =>
              acc.flatMap { x =>
                Str.split(x, c)
              }
          }
          .map { s =>
            Map(key -> Statement(s))
          }
    }).ensureIdStmPresent
  }

  def toSingleTsv(rec: Record): String =  rec.asString

  def fromTsv(
    it: Iterator[String]
  ): (ParsingContext, Iterator[Either[ErrorObject, (Record, Seq[String])]]) = {
    if (it.isEmpty)
      (ParsingContext(), Iterator())
    else {
      val cleanIt = Tsv.cleanIt(it)
      val header = cleanIt.next()
      val ctx = ParsingContext(Str.split(header, valueSeparator))
      var lineNr = 0
      var recordNr = 0
      val outIt = cleanIt.map { line =>
        lineNr += line.count(_ == '\n') + 1
        recordNr += 1
        val items = Str.split(line, valueSeparator)
        if (items.size > ctx.headers.length)
          Left(
            ErrorObject(
              "parsing",
              ctx.source(items),
              None,
              Seq(
                ErrorAnnotation(
                  s"Found ${items.size} items while the header only has ${ctx.headers.length}",
                  Some(ErrorLoc(lineNr, recordNr, items.size))
                )
              )
            )
          )
        else {
          val emptyTail = ctx.headers.length - items.size
          val filledItems = items ++ Seq.fill(emptyTail)("")
          val statements =
            filledItems.foldLeft(Seq[Seq[Statement]]()) {
              case (acc, item) =>
                val s = subValueSeparators
                  .foldLeft(List(item)) {
                    case (acc, c) =>
                      acc.flatMap { x =>
                        Str.split(x, c)
                      }
                  }
                  .map {
                    Statement(_)
                  }
                acc :+ s
            }
          val rec =
            Record(
              ctx.headers
                .zip(statements)
                .toVector
                .flatMap {
                  case (k, stms) =>
                    val key = getRef(k)
                    stms.map { stm =>
                      Map(key -> stm)
                    }
                }
            ).ensureIdStmPresent -> items
          Right(rec)
        }
      }
      ctx -> outIt
    }
  }

  def fromTsv(
    p: Path
  ): (ParsingContext, Iterator[Either[ErrorObject, (Record, Seq[String])]]) = {
    val io: BufferedSource = Source.fromFile(p.toFile)
    val it = new IteratorWithCallback(io.getLines(), io.close()) //Note it doesn't solve the problem of closing the file handle if an exception occurs during processing
    fromTsv(it)
  }

  //It is not possible to produce an iterable version since we need to know all the keys
  //An alternative would be to assume the keys using a spec
  def toTsv(recs: Seq[Record]): String = {
    @tailrec
    def toTsvImpl(recs: Seq[Record],
                  output: String,
                  keys: Seq[String]): (String, Seq[String]) = {
      if (recs.isEmpty)
        (output, keys)
      else {
        val rec = recs.head
        val newKeys: Set[String] = rec.keys.toSet -- keys.toSet //Important! keys is ordered
        val updatedKeys: Seq[String] = keys ++ newKeys
        val newLine = updatedKeys
          .map { key =>
            val tmp = rec
              .stm(key)
              .map {
                _.asString
              }
              .mkString("|") //We do NOT use subvalueSeparators here (which is used only for parsing TSV)
            if (tmp.contains('\n'))
              s"\"$tmp\""
            else
              tmp
          }
          .mkString("\t")
        toTsvImpl(recs.tail, output ++ "\n" ++ newLine, updatedKeys)
      }
    }

    //It is important to use the default to generate the initial list of keys so that we ensure consistent ordering
    val (out, keys) = toTsvImpl(recs, "", orderedKeys)
    keys.mkString("\t") ++ out
  }

  //Attention: we make the assumption that a spec has been provided and all unknown keys (for that spec) will be dropped
  //This is because we are working with Iterators so we need to know the keys beforehand
  def toTsv(it: Iterator[Record]): Iterator[String] = {
    assert(orderedKeys.nonEmpty, "At least one attribute is needed to output a TSV file.")
    Iterator(orderedKeys.mkString("\t")) ++ it.map { rec =>
      val str = orderedKeys
        .map { key =>
          val tmp = rec
            .stm(key)
            .map {
              _.asString
            }
            .mkString("|") //We do NOT use subvalueSeparators here (which is used only for parsing TSV)
          if (tmp.contains('\n'))
            s"\"$tmp\""
          else
           tmp
        }.mkString("\t")
      "\n" ++ str
    }
  }
}
