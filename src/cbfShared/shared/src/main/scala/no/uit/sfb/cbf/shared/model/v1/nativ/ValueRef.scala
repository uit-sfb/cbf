package no.uit.sfb.cbf.shared.model.v1.nativ

import io.lemonlabs.uri.AbsoluteUrl
import no.uit.sfb.cbf.shared.utils.Str

case class ValueRef(
  `val`: String,
  iri: Seq[String] = Seq(),
  meta: Map[String, String] = Map(),
  num: Option[Double] = None, //Numeric representation for numeric ordering purpose
  err: Option[String] = None //Error message from QuickValidator
) extends ValueRefLike {
  lazy val isEmpty = this == new ValueRef("")
  lazy val simpleValue = {
    val simpleVal = `val`
    if (simpleVal.isEmpty)
      iri.headOption.getOrElse("")
    else
      simpleVal
  }
  lazy val hasError = err.nonEmpty
}

object ValueRef {
  val empty = ValueRef("", Seq())

  def apply(str: String): Option[ValueRef] = {
    Str.split(str, '§') match {
      case Nil => None
      case head :: tail =>
        val filteredTail = tail.filter(_.trim.nonEmpty)
        AbsoluteUrl.parseOption(head) match {
          case Some(_) =>
            //If the value is a URL, we set it as a reference
            Some(ValueRef("", head +: filteredTail))
          case None =>
            //Otherwise, it is a simple value
            //Note: CIRI is recognized by the enhanced module
            Some(ValueRef(head, filteredTail))
        }
    }
  }
}
