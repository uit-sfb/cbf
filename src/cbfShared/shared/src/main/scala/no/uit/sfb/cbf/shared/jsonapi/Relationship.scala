package no.uit.sfb.cbf.shared.jsonapi

case class Relationship(id: String, `type`: String)
