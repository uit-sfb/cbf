package no.uit.sfb.cbf.shared.config

import no.uit.sfb.cbf.shared.model.v1.nativ.CompactIRI

case class CustomResolver(prefix: String,
                          regexp: String = CompactIRI.idReg,
                          kvp: Map[String, String] = Map(),
                          ebiOlsBaseUri: Option[String] = None
                         ) {
  lazy val isEbiOls = ebiOlsBaseUri.nonEmpty
}
