package no.uit.sfb.cbf.shared.model.v1.role

object RoleOrdering extends Ordering[String] {
  //Roles by increasing order of rights
  val roles = Seq("none", "reader", "writer", "owner")
  protected val roleMap = roles.zipWithIndex.toMap
  override def compare(x: String, y: String) =
    implicitly[Ordering[Int]]
      .compare(roleMap.getOrElse(x, -1), roleMap.getOrElse(y, -1))
}
