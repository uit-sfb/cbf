package no.uit.sfb.cbf.shared.validate

import no.uit.sfb.cbf.shared.config.{AttributeDef, DatasetConfig}
import no.uit.sfb.cbf.shared.model.v1.nativ.{
  CompactIRI,
  Evidence,
  Record,
  Statement,
  ValueRef
}

/**
  * This validator adds error messages in the meta object of the misbehaving ValueRefs
  *
  * @param datasetConfig
  * @param relax If true, ignore templates and ignore prefix missing when and strict (as it will be added later on during expression phase)
  */
class QuickValidator(datasetConfig: DatasetConfig, relax: Boolean = false) {
  protected lazy val enums = datasetConfig.customTypes.collect {
    case t if t.lt.isSubtypeOf("cat:enum") =>
      t.lt.asString -> t.kvp.getOrElse("terms", "")
  }.toMap
  protected lazy val typeValidator = new TypeValidator(enums)

  //List of compulsory attributes (forceType and is NOT automatic or a template)
  protected val compulsoryAttrs =
    datasetConfig.attrs.filter(a => a.forceType && !a.isFullyControlled).map {
      _.ref
    }

  private def validate(attr: AttributeDef, vr: ValueRef): Option[String] = {
    //Check resolvers
    if (attr.resolvers.nonEmpty) {
      if (attr.forceResolver && vr.ciris.nonEmpty && !vr.ciris.exists { ciri =>
            attr.resolvers.contains(ciri.prefix)
          }) {
        if (!(relax && attr.resolvers.size == 1))
          return Some(
            s"Must use one of the following prefixes: [${attr.resolvers
              .mkString(", ")}]."
          )
      }
      vr.ciris.foreach { ciri =>
        datasetConfig.customResolvers.find(_.prefix == ciri.prefix).map {
          resolv =>
            val r = resolv.regexp.r
            ciri.id match {
              case r(groups @ _*) =>
                ()
              case _ =>
                return Some(s"Must satisfy '${resolv.regexp}'")
            }
        }
      }
    }

    //Check type of the head value
    typeValidator(attr.`type`, !attr.forceType)(vr.simpleValue)
  }

  def apply(ref: String, vr: ValueRef): ValueRef = {
    datasetConfig.attrs.find(_.ref == ref) match {
      case Some(attr) if !attr.hasActiveTemplate || relax =>
        //Enhancing by factorizing allowed prefixes
        val enhancedVr = CompactIRI(vr.`val`) match {
          case Some(ciri) if attr.resolvers.contains(ciri.prefix) =>
            vr.copy(`val` = "", iri = ciri.asString +: vr.iri)
          case _ =>
            vr
        }
        val err = validate(attr, enhancedVr)
        enhancedVr.copy(err = err)
      case _ =>
        vr //We ignore unknown attributes
    }
  }

  def apply(ref: String, evi: Evidence): Evidence =
    evi // No validation on evidences

  def apply(ref: String, stm: Statement): Statement = {
    //Only the head ValueRef is validated. The remaining objects may contain sub-values or precisions which are mostly of type string.
    val validatedHead = apply(ref, stm.obj.headOption.getOrElse(ValueRef.empty))
    val tail =
      if (stm.obj.nonEmpty)
        stm.obj.tail
      else
        Seq()
    stm.copy(obj = validatedHead +: tail, evi = stm.evi.map {
      apply(ref, _)
    })
  }

  def apply(rec: Record): Record = {
    val attrs = rec.attributes
    //First we start by creating an empty instance for all compulsory attributes that are not defined
    //This is needed to be able to get error messages for those.
    val attrsToAdd = compulsoryAttrs.collect {
      case ref if !attrs.exists { case (attr, _) => ref == attr } =>
        ref -> Statement(Seq(ValueRef.empty))
    }.toVector
    rec.copy(
      attrs = (attrsToAdd ++ rec.attributes)
        .map {
          case (ref, stm) =>
            ref -> apply(ref, stm)
        }
        .map {
          Map(_)
        }
    )
  }
}
