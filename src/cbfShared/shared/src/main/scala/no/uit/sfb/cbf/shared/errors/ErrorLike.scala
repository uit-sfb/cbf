package no.uit.sfb.cbf.shared.errors

import no.uit.sfb.cbf.shared.jsonapi.Err

//Throw from within any Action to trigger the EventHandler

trait ErrorLike extends Exception {
  def statusCode: Int
  def title: String
  def details: String

  override def getMessage() = s"$statusCode: $title\n$details"

  lazy val toErr = Err(statusCode.toString, title, details)
}

case class BadRequestClientError(details: String) extends ErrorLike {
  val title = "Bad request"
  val statusCode = 400
}

case class UnauthorizedClientError(details: String = "") extends ErrorLike {
  val title = "Login required"
  val statusCode = 401
}

case class ForbiddenClientError(details: String = "") extends ErrorLike {
  val title = "Access denied"
  val statusCode = 403
}

case class NotFoundClientError(details: String) extends ErrorLike {
  val title = "Not found"
  val statusCode = 404
}

case class ConflictClientError(details: String) extends ErrorLike {
  val title = "Conflict"
  val statusCode = 409
}

case class UnprocessableEntityClientError(details: String) extends ErrorLike {
  val title = "Unprocessable entity"
  val statusCode = 422
}

case class InternalServerErrorError(details: String) extends ErrorLike {
  val title = "Internal server error"
  val statusCode = 500
}

case class BadGatewayError(details: String = "") extends ErrorLike {
  val title = "Internal service error"
  val statusCode = 502
}

case class GatewayTimeoutError(details: String = "") extends ErrorLike {
  val title = "Internal service not responding"
  val statusCode = 504
}
