package no.uit.sfb.cbf.shared.utils

import scala.annotation.tailrec

case class Versionning(protected val vers: Set[String]) {
  lazy val versions = GenericVersioning.sort(vers.toSeq)

  def isIn(ver: String): Boolean = vers.contains(ver)

  def comesBefore(ver: String): Boolean = {
    comesBeforeImpl(ver, versions)
  }

  private def comesBeforeImpl(ver: String, seq: Seq[String]): Boolean = {
    seq.headOption match {
      case None => true
      case Some(f) =>
        GenericVersioning.compare(ver, f) < 0
    }
  }

  def comesAfter(ver: String): Boolean = {
    comesAfterImpl(ver, versions)
  }

  private def comesAfterImpl(ver: String, seq: Seq[String]): Boolean = {
    seq.lastOption match {
      case None => true
      case Some(l) =>
        GenericVersioning.compare(ver, l) > 0
    }
  }

  lazy val first = versions.headOption
  lazy val last = versions.lastOption

  def previous(ver: String): Option[String] = {
    @tailrec
    def previousImpl(seq: Seq[String]): Option[String] = {
      if (comesAfterImpl(ver, seq))
        seq.lastOption
      else {
        previousImpl(seq.init)
      }
    }

    previousImpl(versions)
  }

  def next(ver: String): Option[String] = {
    @tailrec
    def nextImpl(seq: Seq[String]): Option[String] = {
      if (comesBeforeImpl(ver, seq))
        seq.headOption
      else {
        nextImpl(seq.tail)
      }
    }

    nextImpl(versions)
  }
}
