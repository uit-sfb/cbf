package no.uit.sfb.cbf.shared.utils

object ListUtils {
  def interleave[T](l1: List[T], l2: List[T]): List[T] = {
    val m1 = l1.grouped(1).toList
    val m2 = l2.grouped(1).toList
    m1.zipAll(m2, Nil, Nil)
      .flatMap {
        case (e1, e2) =>
          e1 ++ e2
      }
  }
}
