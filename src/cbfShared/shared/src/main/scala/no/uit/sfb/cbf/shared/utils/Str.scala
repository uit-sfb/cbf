package no.uit.sfb.cbf.shared.utils

object Str {
  /*Split and:
      - un-escape separator in items
      - trim items
   */
  def split(str: String, c: Char): List[String] = {
    val rawSplit = {
      val split_tmp = str.split(c)
      if (str.endsWith(s"$c"))
        split_tmp :+ ""
      else
        split_tmp
    }
    val untrimmedSplit = rawSplit.foldLeft(Seq[String]()) {
      case (acc, item) =>
        if (acc.nonEmpty && acc.last.endsWith("\\"))
          acc.init :+ (acc.last.init ++ s"$c$item")
        else
          acc :+ item
    }
    (untrimmedSplit map {
      Tsv.sanitizeValues
    }).toList
  }
}
