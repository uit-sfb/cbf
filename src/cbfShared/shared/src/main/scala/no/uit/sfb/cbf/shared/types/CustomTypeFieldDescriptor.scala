package no.uit.sfb.cbf.shared.types

case class CustomTypeFieldDescriptor(
  descr: String,
  default: String = "",
  values: Seq[String] = Seq(), //Not compatible with multiline set to true
  multiline: Boolean = false //If true, a multiline field is displayed and each newline is replaced by | when storing the value
) {
  assert(
    !(values.nonEmpty && multiline),
    "CustomTypeDescriptor multiline is not compatible with values."
  )
}
