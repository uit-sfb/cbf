package no.uit.sfb.cbf.shared.jsonapi

import io.circe.Encoder

case class Data[T: Encoder](`type`: String,
                            id: String = "",
                            attributes: T,
                            relationships: Map[String, Relationships] = Map())
