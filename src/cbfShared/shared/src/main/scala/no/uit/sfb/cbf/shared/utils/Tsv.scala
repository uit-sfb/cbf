package no.uit.sfb.cbf.shared.utils

import java.nio.file.Path

import scala.io.Source

object Tsv {
  //Support multiline
  def cleanIt(it: Iterator[String]): Iterator[String] = {
    new Iterator[String] {
      override def next(): String = {
        var buffer: String = cleanLine(it.next())
        while (buffer.count{_ == '"'} % 2 != 0)
          buffer += s"\n${cleanLine(it.next())}"
        buffer
      }
      override def hasNext: Boolean = it.hasNext
    }
  }

  def transpose(str: String): String = {
    val aarr = str.split('\n') map {
      _.split('\t')
    }
    aarr.transpose
      .map { x =>
        x.mkString("\t")
      }
      .mkString("\n")
  }

  //We assume the file is written with headers at the first line
  //n is the record to extract and matches the line number
  def extractLineAndTranspose(from: Path, n: Int): String = {
    assume(n > 0)
    val file = Source.fromFile(from.toFile)
    try {
      val src = file.getLines()
      val headers = src.next().split('\t')
      for (_ <- 1 until n) {
        src.next()
      }
      val record = src.next().split('\t')
      //We transpose
      (for { (header, value) <- headers.zipAll(record, "", "") } yield {
        s"$header\t$value"
      }).mkString("\n")
    } finally {
      file.close()
    }
  }

  def cleanLine(str: String): String = {
    str.replace("\r", "")
  }

  def cleanIterator(str: String): Iterator[String] = {
    cleanIt(
      str.replaceAll("\\r\\n?", "\n").split('\n').iterator
    )
  }

  //Full TSV parsing returning an iterator of KV pairs
  //Assuming first line being header
  //If a header is missing, its column number is used instead
  def kvIterator(str: String): Iterator[Map[String, String]] = {
    val it = cleanIterator(str)
    if (it.hasNext) {
      val header = it.next().split('\t')
      it.map { line =>
        val vals = line.split('\t')
        header
          .zipAll(vals, "", "")
          .zipWithIndex
          .map {
            case ((h, v), idx) =>
              (if (h.isEmpty)
                idx.toString
              else
                h) -> sanitizeValues(v)
          }
          .toMap
      }
    } else
      Iterator.empty
  }

  //Header, lines[values]
  def parse(str: String): (Seq[String], Seq[Seq[String]]) = {
    val it = cleanIterator(str)
    if (it.hasNext) {
      val header = it.next().split('\t')
      val values = it.map { line =>
        line.split('\t').toSeq
      }
      header.toSeq -> values.toSeq.map{_.map{sanitizeValues}}
    } else
      Seq() -> Seq()
  }

  def sanitizeValues(v: String) = {
    val vTrimmed = v.trim
    val tmp = if (vTrimmed.startsWith("\"") || vTrimmed.startsWith("\n"))
      v.drop(1)
    else
      v
    val tmp2 = if (tmp.endsWith("\"") || tmp.endsWith("\n"))
      tmp.dropRight(1)
    else
      tmp
    tmp2.trim
  }
}
