package no.uit.sfb.cbf.shared.model.v1.versioninfo

import no.uit.sfb.cbf.shared.utils.{Date, GenericVersioning}

case class VersionInfo(_id: String,
                       releaseDate: String = "",
                       label: String = "",
                       description: String = "",
                       deprecated: Boolean = false) {
  lazy val isDraft = releaseDate.isEmpty
  lazy val displayStr =
    if (label.nonEmpty) s"$label (${_id})" else _id
}

object VersionInfo extends Ordering[VersionInfo] {
  def compare(v1: VersionInfo, v2: VersionInfo): Int = {
    GenericVersioning.compare(v1._id, v2._id)
  }

  //Do not call from frontend as Date is not supported
  def now(_id: String): VersionInfo = {
    VersionInfo(_id, Date.nowDate())
  }

  def draft(_id: String) = VersionInfo(_id, "")
}
