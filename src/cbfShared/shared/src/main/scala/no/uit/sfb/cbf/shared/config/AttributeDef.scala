package no.uit.sfb.cbf.shared.config

import no.uit.sfb.cbf.shared.utils.Tsv

case class AttributeDef(
  ref: String,
  name: String,
  alternateNames: Seq[String] = Seq(), //Used for parsing if ref or name are not found
  descr: String = "",
  suffix: String = "",
  resolvers: Seq[String] = Seq(),
  forceResolver: Boolean = false, //If set to true, one of the resolvers must match (no effect if resolver list is empty). Empty string would be fine though (as long as forceType is set to false)
  template: Option[String] = None, //Automatically filled by template. Is not exported in curator's tsv mode.
  `type`: String = "", //num, cat, date ... Only for the ead ValueRef
  forceType: Boolean = false, //If set to true, emtpy string is not allowed, and the head ValueRef must be present for this attribute
  virtual: Boolean = false, //Is not persisted
  hidden: Boolean = false, //Only exported in curator's tsv mode
  automatic: Boolean = false, //Is filled automatically by external pipeline. Is not exported in curator's tsv mode. Note: Template implies automatic.
  sticky: Boolean = false //Always exported in TSV (override hidden and automatic).
) {
  //An active template is a template that is not empty and do not comport any self-reference
  //List of all linked attributes from the template (excluding self-reference)
  lazy val (hasActiveTemplate, dependencies) = {
    val templ = template.getOrElse("")
    if (templ.isEmpty)
      false -> Seq()
    else {
      val regexp = "\\{.*?\\}".r
      val rawList = regexp
        .findAllIn(templ)
        .map { tpl =>
          val splt = tpl.substring(1, tpl.length - 1).split('@')
          splt.head
        }
        .distinct
        .toSeq
      val (tmp, res) = rawList.partition(_ == ref)
      tmp.isEmpty -> res
    }
  }
  lazy val isFullyControlled = automatic || hasActiveTemplate
  lazy val lType = LineagedType(`type`)
  lazy val allNames = ref +: name +: alternateNames
}

object AttributeDef {
  def apply(m: Map[String, String]): AttributeDef = {
    val default = AttributeDef("", "")
    def force(raw: String): (Boolean, String) = {
      val reg = "!(.*)".r
      raw match {
        case reg(t) =>
          true -> t
        case _ =>
          false -> raw
      }
    }
    val (forceFlag, rawType) = force(m.getOrElse("Type", ""))
    val splt = rawType.split('§') //Do not remove empty!!
    val nSplt = m("Name").split('|')
    val name = {
      val h = nSplt.headOption.getOrElse("")
      if (h.isEmpty)
        m("Attribute")
      else h
    }
    val tpl = {
      val tmp = m.getOrElse("Template", "")
      if (tmp.isEmpty)
        None
      else
        Some(tmp)
    }
    val (ref, virtual) = {
      val tmp = m("Attribute")
      if (m("Attribute").startsWith("*"))
        tmp.drop(1) -> true
      else
        tmp -> false
    }
    AttributeDef(
      ref,
      name,
      nSplt.tail.toSeq,
      m.getOrElse("Description", default.descr),
      m.getOrElse("Suffix", default.suffix),
      splt.tail.toSeq,
      forceFlag,
      tpl,
      splt.headOption.getOrElse(""),
      forceFlag,
      virtual
    )
  }

  def readAll(str: String, dbName: String): Seq[AttributeDef] = {
    Tsv
      .kvIterator(str)
      .flatMap { m =>
        //If no column matches the db name then we assume all the attributes should be added
        m.get(dbName) match {
          case Some(v) if v.trim.isEmpty =>
            None
          case _ =>
            Some(AttributeDef(m))
        }
      }
      .toSeq
  }
}
