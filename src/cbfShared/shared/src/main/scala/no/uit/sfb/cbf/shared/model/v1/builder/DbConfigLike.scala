package no.uit.sfb.cbf.shared.model.v1.builder

trait DatasetConfigLike {
  def dbHost: String

  def dbUserName: String

  def dbUserPassword: String
}
