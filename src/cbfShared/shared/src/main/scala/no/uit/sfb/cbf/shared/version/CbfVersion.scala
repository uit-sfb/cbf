package no.uit.sfb.cbf.shared.version

import no.uit.sfb.cbf.shared.errors.BadRequestClientError

case class CbfVersion(ver: String) {
  private val reg = "(\\w+)\\.([0-9]+)".r
  private val (a, b) = ver match {
    case reg(prefix, nb) =>
      prefix -> nb.toInt
    case _ =>
      throw BadRequestClientError(
        s"The version must be of the form v.r where r is an integer, but got '$ver'."
      )
  }
  val (branch, release) = s"$a.x" -> b

  lazy val incr = CbfVersion(s"$a.${b + 1}")

  override def toString = ver
}
