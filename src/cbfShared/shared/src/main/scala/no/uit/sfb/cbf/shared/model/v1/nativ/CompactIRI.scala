package no.uit.sfb.cbf.shared.model.v1.nativ

case class CompactIRI(prefix: String, id: String) {
  lazy val asString = s"${prefix.toLowerCase}:$id"
}

object CompactIRI {
  //Attention, be SURE you know what you are doing if you change this!!
  //Note: We exclude '{' and '}' because they are used for templating
  val idReg = "[-\\w][^{}\\s]*" //allowed '-' for gmap
  protected val regex = s"^(\\w[\\w.-]*):($idReg)$$".r

  def apply(str: String): Option[CompactIRI] = {
    str match {
      case regex(prefix, id) =>
        Some(CompactIRI(prefix.toLowerCase, id))
      case _ =>
        None
    }
  }

  def withoutValidation(str: String): Option[CompactIRI] = {
    if (str.isEmpty)
      None
    else {
      val splt = str.split(':')
      Some(CompactIRI(splt.head, splt.last))
    }
  }
}
