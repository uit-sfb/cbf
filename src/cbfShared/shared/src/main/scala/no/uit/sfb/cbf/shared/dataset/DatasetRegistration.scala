package no.uit.sfb.cbf.shared.dataset

case class DatasetRegistration(
  _id: String,
  creationDate: String = "",
  lastAccess: String = "", //Date of last read/write access.
  deleteAfter: Int = 0, //<=0: no action, > 0: delete db after x days
  capped: Int = 0, //<=0: uncapped, > 0: max number of docs including all versions
  sharedDbName: String = "_cbf"
)
