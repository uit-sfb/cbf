package no.uit.sfb.cbf.shared.model.v1.nativ

import no.uit.sfb.cbf.shared.utils.Str

/**
  * Represents an attribute
  */
case class Attribute(name: String, collectionName: String) {
  lazy val asString = {
    if (collectionName.nonEmpty)
      s"$collectionName:$name"
    else
      name
  }
}

object Attribute {
  import scala.language.implicitConversions
  implicit def apply(str: String): Attribute = {
    Str.split(str, ':') match {
      case Nil            => throw new Exception("A key must not be empty.")
      case key :: Nil     => Attribute(key, "")
      case colName :: key => Attribute(key.mkString(":"), colName)
    }
  }
}
