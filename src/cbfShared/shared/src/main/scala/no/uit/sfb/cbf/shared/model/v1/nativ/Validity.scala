package no.uit.sfb.cbf.shared.model.v1.nativ

case class Validity(start: Option[Int] = None, end: Option[Int] = None)
