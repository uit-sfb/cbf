package no.uit.sfb.cbf.shared.model.v1.builder

import no.uit.sfb.cbf.shared.model.v1.nativ.Record
import no.uit.sfb.cbf.shared.utils.JsonArray

import scala.util.Try

trait JsonSupportLike {
  this: RecordBuilder =>

  //The Json parser cannot return an iterator, so we have to work with Seq
  def fromJson(str: String): Seq[Record] = {
    val res = (Try {
      Record.extractArray(str).map { _.ensureIdStmPresent }
    }).toOption.getOrElse(Seq(fromSingleJson(str)))
    checkDuplicates(res)
    res
  }

  //Note that we expect only one record (not an array)
  def fromSingleJson(str: String): Record = {
    Record(str).ensureIdStmPresent
  }

  def toJson(recs: Iterator[Record]): Iterator[String] = {
    JsonArray.toJson[Record](recs, _.serialize())
  }
}
