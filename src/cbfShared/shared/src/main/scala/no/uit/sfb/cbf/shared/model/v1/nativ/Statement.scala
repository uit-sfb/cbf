package no.uit.sfb.cbf.shared.model.v1.nativ

import no.uit.sfb.cbf.shared.utils.Str

case class Statement(obj: Seq[ValueRef], evi: Seq[Evidence] = Seq()) {
  lazy val isEmpty: Boolean = this == Statement.empty

  lazy val asString: String = {
    val valuesStr = (obj map {
      _.asString
    }).mkString(">")
    val evidencesStr = evi map { ev =>
      ev.asString
    }
    (Seq(valuesStr) ++ evidencesStr).mkString("<")
  }

  //Transform

  lazy val valueOnly: Statement = {
    copy(obj = obj.map {
      _.valueOnly
    }, evi = Seq())
  }

  lazy val simpleValue: String = {
    val vr = obj.headOption
      .getOrElse(ValueRef.empty)
    vr.simpleValue
  }

  lazy val errors = obj.flatMap { _.err }
  lazy val hasError = errors.nonEmpty

  lazy val trimmed: Statement = {
    copy(obj = obj.filter(!_.isEmpty))
  }
}

object Statement {
  lazy val empty = Statement(Seq())

  protected def parseValues(str: String): Seq[ValueRef] = {
    if (str.isEmpty)
      Seq()
    else {
      val rawList = Str.split(str, '>')
      rawList.flatMap {
        ValueRef(_)
      }
    }
  }

  def apply(str: String): Statement = {
    val (valuesStr, evidencesStr): (String, Seq[String]) =
      Str.split(str, '<') match {
        case Nil => ("", Seq())
        case head :: tail =>
          (head, tail)
      }
    val items = parseValues(valuesStr)
    Statement(items, evidencesStr.map { s =>
      Evidence(s)
    })
  }
}
