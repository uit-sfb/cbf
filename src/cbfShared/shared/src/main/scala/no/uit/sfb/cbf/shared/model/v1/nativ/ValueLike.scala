package no.uit.sfb.cbf.shared.model.v1.nativ

import io.lemonlabs.uri.AbsoluteUrl

trait ValueLike {
  def `val`: String
}

trait ValueRefLike extends ValueLike {
  //Compact IRI or URLs
  def iri: Seq[String]
  //Metadata automatically derived from CIRIs
  def meta: Map[String, String]

  private lazy val (u, c, o) = {
    val (urls, other) = iri
      .partition {
        AbsoluteUrl.parseOption(_).nonEmpty
      }

    val (c, o) = other
      .map { s =>
        s -> CompactIRI(s)
      }
      .partition {
        _._2.nonEmpty
      }
    (urls, c.flatMap { _._2 }, o.map { _._1 })
  }
  lazy val ciris: Seq[CompactIRI] = c
  lazy val urls: Seq[String] = u
  lazy val otherValues: Seq[String] = o

  protected lazy val minimized: ValueRefLike = {
    ciris.find { ciri =>
      ciri.id == `val`
    } match {
      case None => this
      case Some(ciri) =>
        this match {
          case _: ValueRef =>
            ValueRef(ciri.asString, iri.filter { _ != ciri.asString })
          case v: Evidence =>
            Evidence(ciri.asString, iri.filter { _ != ciri.asString }, v.src)
          case _ => this
        }
    }
  }

  def asString: String = {
    val escaped = {
      minimized.`val`
        .replace("<", "\\<")
        .replace(">", "\\>")
        .replace("|", "\\|")
        .replace("§", "\\§")
    }
    val ref = minimized.iri.mkString("§")
    if (ref.isEmpty)
      escaped
    else
      s"$escaped§$ref"
  }

  lazy val valueOnly = ValueRef(`val`).getOrElse(ValueRef.empty)
}

trait EvidenceRefLike extends ValueRefLike {
  def src: Set[ValueRef]
}
