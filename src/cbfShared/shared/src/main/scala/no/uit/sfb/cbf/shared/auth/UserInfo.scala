package no.uit.sfb.cbf.shared.auth

import no.uit.sfb.cbf.shared.model.v1.role.RoleOrdering

case class UserInfo(
                     _id: String,
                     email: String = "",
                     role: String = "none", //none, owner, writer, reader, none, anonymous
                     name: Option[String] = None,
                     pending: Option[String] = None, //desired role. If None: no ongoing request.
) {
  def is(r: String) = RoleOrdering.gteq(role, r)
  lazy val isLoggedIn = role != "anonymous"
}
