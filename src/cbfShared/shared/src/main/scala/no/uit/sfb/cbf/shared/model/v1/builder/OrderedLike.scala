package no.uit.sfb.cbf.shared.model.v1.builder

import no.uit.sfb.cbf.shared.model.v1.nativ.{Record, Statement}

trait OrderedLike {
  //The ordered list of attributes
  //Any missing attribute is inserted with an empty value
  //Any extra attribute is discarded
  //If empty, no ordering is performed
  protected def orderedKeys: Seq[String]

  def ordered(rec: Record): Record = {
    if (orderedKeys.isEmpty)
      rec
    else
      rec.copy(attrs = orderedKeys.toVector.flatMap { k =>
        val stms = rec.stm(k)
        if (stms.isEmpty)
          Vector(Map(k -> Statement.empty))
        else {
          rec.stm(k).map { x =>
            Map(k -> x)
          }
        }
      })
  }
}
