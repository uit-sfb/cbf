package no.uit.sfb.cbf.shared.jsonapi

case class Err(status: String, title: String, detail: String)
