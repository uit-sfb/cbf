package no.uit.sfb.cbf.shared.model.v1.nativ

import io.circe.Printer
import io.circe.generic.extras.auto._
import io.circe.generic.extras.Configuration
import io.circe.syntax._
import io.circe.parser.decode
import no.uit.sfb.cbf.shared.utils.{Date, ListUtils}

import java.util.UUID
import scala.annotation.tailrec

/**
 * Represents a database record (set of statements about one sample)
 *
 * Note: the Map is supposed to contain exclusively one item
 * Note: A record that would have an empty attrs (and non-empty id) is plan for deletion
 */
case class Record(attrs: Vector[Map[String, Statement]] = Vector(),
                  valid: Validity = Validity(),
                  cur: CurationMeta = CurationMeta(),
                  id: Option[String] = None, //Copy of the id attribute
                  //Attention: will be empty until stamped() is called. Use getId instead!
                 ) {
  val attributes = attrs.flatMap {
    _.headOption
  }

  @tailrec
  final def addAttrs(newAttrs: Map[String, Vector[Statement]],
                     clearExisting: Boolean = false,
                     patchFriendly: Boolean = true
                    ): Record = {
    if (newAttrs.isEmpty)
      this
    else {
      val (key, vs) = newAttrs.head
      val cleaned =
        if (clearExisting)
          attrs.filter {
            !_.contains(key)
          } else
          attrs
      //For patching purpose, we need an empty statement
      val stms = if (vs.isEmpty && patchFriendly)
        Vector(Statement.empty)
      else vs
      copy(attrs = cleaned ++ stms.map { stm =>
        Map(key -> stm)
      }).addAttrs(newAttrs.tail, clearExisting)
    }
  }

  def stm(key: String): Vector[Statement] = attributes.collect {
    case (k, stm) if k == key =>
      stm
  }

  lazy val keys: Seq[String] = attributes.map {
    _._1
  }.distinct

  lazy val toBeDeleted = attrs.isEmpty && id.nonEmpty //Do NOT use getId here (since it would return None all the time)

  //To single TSV
  lazy val asString: String = {
    (keys map { key =>
      s"$key\t${
        val tmp = stm(key)
          .map { stm =>
            stm.asString
          }
          .mkString("|")
        if (tmp.contains('\n'))
          s"\"$tmp\""
        else
          tmp
      }"
    }).mkString("\n")
  }

  //Transform
  lazy val valueOnly: Record = {
    copy(attrs = attrs.map {
      _.view.mapValues {
        _.valueOnly
      }.toMap
    })
  }

  def serialize(pretty: Boolean = false): String = {
    import io.circe.generic.auto._ //Needed!
    val json = this.asJson
    if (pretty) {
      Printer.spaces2.copy(dropNullValues = true).print(json)
    } else
      Printer.noSpaces.copy(dropNullValues = true).print(json)
  }

  def stamped(releaseVersion: String,
              original: Option[Record] = None): Record = {
    lazy val date = Date.nowDate()

    //Get value from origin if provided and if defined
    //Otherwise get the one from current record
    def getValue(key: String): Option[String] = {
      def getValueImpl(key: String, record: Record): Option[String] = {
        record.stm(key).headOption
          .map {
            _.simpleValue
          }
          .filter {
            _.nonEmpty
          }
      }

      original.flatMap { rec =>
        getValueImpl(key, rec)
      }.orElse(getValueImpl(key, this))
    }

    addAttrs(Map(
      "_time:implementation_date" -> Vector(Statement(getValue("_time:implementation_date").getOrElse(date))),
      "_time:creation_date" -> Vector(Statement(getValue("_time:creation_date").getOrElse(date))),
      "_time:release_version" -> Vector(Statement(getValue("_time:release_version").getOrElse(releaseVersion))),
      "_time:update_date" -> Vector(Statement(getValue("_time:creation_date") match { //If creation_date is empty then the entry is being created -> this is not an update
        case Some(_) => date
        case None => ""
      })),
    ), true).copy(id = Some(getId))
  }

  lazy val getId: String = {
    val raw_id = id.getOrElse(stm("id").head.simpleValue)
    CompactIRI(raw_id) match {
      case Some(ciri) =>
        ciri.id
      case None =>
        raw_id
    }
  }

  /*We ignore:
    - validity
    - cur
    - id
    - attribute order
    - stamped attributes (i.e. _time attributes)
   */
  def equivalent(other: Record): Boolean = {
    attributes.filter { case (key, _) => !key.startsWith("_time:") }.toSet == other.attributes.filter {
      case (key, _) => !key.startsWith("_time:")
    }.toSet
  }

  //Recursive templates are not supported
  //Returns a Seq because:
  //  - a template may contain '|'
  //  - a template may refer to an attribute that is replicated
  //Templates cannot contain virtual attributes.
  def resolveTemplate(template: String): Seq[String] =
    template.split('|').toIndexedSeq.flatMap {
      resolveStraightTemplate
    }

  protected def resolveStraightTemplate(template: String): Seq[String] = {
    val regexp = "\\{.*?\\}".r
    val sep = 0.toChar //Unlikely to find this one in a string...
    val base: List[String] =
      regexp.replaceAllIn(template, sep.toString).split(sep).toList
    val tParts: List[Vector[String]] = regexp
      .findAllIn(template)
      .map { tpl =>
        val splt = tpl.substring(1, tpl.length - 1).split('@')
        val key = splt.headOption.getOrElse("")
        val metaKey = splt.tail.mkString("@")
        stm(key).map { stm =>
          if (metaKey.nonEmpty) {
            stm.obj.headOption.flatMap {
              _.meta.get(metaKey)
            } match {
              case Some(res) =>
                res
              case None =>
                //println(s"Could not find template meta key '$metaKey' in '$str'")
                ""
              /*throw new Exception(
                    s"Could not find template meta key '$metaKey' in '$str'")*/
            }
          } else
            stm.simpleValue
        }
      }
      .toList
    if (tParts.nonEmpty) {
      //In case multiple attributes are used in one template with different replicate counts, we fill the less replicated with ""
      val maxElems = tParts.map {
        _.size
      }.max
      val vect = tParts.map { vect =>
        vect ++ Vector.fill(maxElems - vect.size)(vect.headOption.getOrElse(""))
      }.transpose
      vect.map { l =>
        ListUtils.interleave(base, l).mkString("")
      }
    } else
      List(base.mkString(""))
  }

  lazy val ensureIdStmPresent: Record = {
    if (stm("id").isEmpty || stm("id").head.isEmpty) {
      id match {
        case Some(i) =>
          //This case happens when projecting without attrs.id
          addAttrs(Map("id" -> Vector(Statement(i))), true)
        case None =>
          //This case happens when the key is automatic
          //In case of a template or counter key, we use a UUID now, but it will be replaced during templating
          addAttrs(Map("id" -> Vector(Statement.empty)), true)
      }
    } else
      this
  }

  //Remove empty statements
  lazy val trimmed: Record =
    copy(attributes.flatMap{
          case attr -> stm =>
            val trimmedStm = stm.trimmed
            if (trimmedStm.isEmpty)
              None
            else
              Some(attr -> trimmedStm)
        }.map{
      case attr -> stm =>
        Map(attr -> stm)
    })
}

object Record {

  implicit val customConfig: Configuration = Configuration.default.withDefaults

  def apply(str: String): Record = {
    decode[Record](str).toTry.get
  }

  def extractArray(str: String): Seq[Record] = {
    decode[Seq[Record]](str).toTry.get
  }

  //Records to delete have their id set and body removed
  //See Record.toBeDeleted
  def toDelete(rec: Record): Record = {
    val rawId = rec.getId
    if (rawId.startsWith("!")) {
      val stripped = rawId.takeRight(rawId.length - 1)
      val id = CompactIRI(stripped) match {
        case Some(ciri) =>
          ciri.id
        case None =>
          stripped
      }
      Record(Vector(), id = Some(id))
    } else
      rec
  }
}
