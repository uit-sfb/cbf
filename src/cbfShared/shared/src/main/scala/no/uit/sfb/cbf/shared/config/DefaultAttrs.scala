package no.uit.sfb.cbf.shared.config

object DefaultAttrs {
  def apply() = Seq(
    AttributeDef(
      "_time:implementation_date",
      "Implementation date",
      descr =
        "Date at which the metadata for this entry were collected from primary databases.",
      `type` = "date",
      forceType = true,
      automatic = true
    ),
    AttributeDef(
      "_time:creation_date",
      "Creation date",
      descr =
        "Date of entry creation (entry curated and initial insertion into the dataset).",
      `type` = "date",
      forceType = true,
      automatic = true
    ),
    AttributeDef(
      "_time:release_version",
      "Release version",
      descr = "Dataset version this entry appeared for the first time.",
      `type` = "cat:semver",
      forceType = true,
      automatic = true
    ),
    AttributeDef(
      "_time:update_date",
      "Update date",
      descr = "Latest entry update date.",
      `type` = "date",
      automatic = true
    ),
    AttributeDef(
      "id",
      "Unique ID",
      descr = "Unique identifier.",
      sticky = true,
      `type` = "acc",
      forceType = true
    ),
  )
}
