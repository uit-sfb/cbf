package no.uit.sfb.cbf.shared.utils.context

case class ParsingContext(headers: Seq[String] = Seq()) {
  protected lazy val header = headers.mkString("\t")
  def source(items: Seq[String]) = header + "\n" + items.mkString("\t")
}
