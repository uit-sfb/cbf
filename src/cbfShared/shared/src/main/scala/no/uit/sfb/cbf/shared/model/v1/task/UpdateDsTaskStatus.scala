package no.uit.sfb.cbf.shared.model.v1.task

case class UpdateDsTaskStatus(dsName: String,
                              ver: String,
                              method: String,
                              progress: Int,
                              nbRecords: Long,
                              cnt: Long,
                              errors: Long,
                              state: String //Starting, Running, Success, Failed
) {
  val ratioFailed: Option[Int] =
    if (nbRecords < 0)
      None
    else if (nbRecords == 0)
      Some(0)
    else
      Some((errors * 100 / nbRecords).toInt)
}
