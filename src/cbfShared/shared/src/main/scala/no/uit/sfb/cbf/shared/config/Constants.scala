package no.uit.sfb.cbf.shared.config

import no.uit.sfb.cbf.shared.types.InternalType

object Constants {
  val defaultTypes = InternalType.fixTypes
  val customTypes = InternalType.starTypes

  val defaultAttributes = DefaultAttrs().map { _.ref }
}
