package no.uit.sfb.cbf.shared.config

import no.uit.sfb.cbf.shared.utils.Tsv

//Empty category means the tab makes its own category
case class Tab(name: String, category: String = "", attrs: Seq[String])

object Tab {
  def readAll(str: String,
              dbName: String,
              tabDefs: Seq[(String, String)]): Seq[Tab] = {
    val tabAndAttr: Seq[(String, String)] = Tsv
      .kvIterator(str)
      .flatMap { m =>
        //If no column matches the db name then we assume all the attributes should be added
        m.get(dbName) match {
          case Some(v) if v.trim.isEmpty =>
            Seq()
          case _ =>
            m.get("Tabs") match {
              case None => Seq()
              case Some(tabs) =>
                tabs.split('|').filter { _.nonEmpty }.map {
                  _ -> m("Attribute").replace("*", "")
                }
            }
        }
      }
      .toSeq

    //We need to keep "_table" for compatibility purpose
    val allTabDefs = ("_table", "") +: tabDefs :+ ("meta", "Record metadata")
    val allAttrs = Seq(
      "meta" -> "_time:creation_date",
      "meta" -> "_time:update_date",
      "meta" -> "_time:implementation_date",
      "meta" -> "_time:release_version",
    ) ++ tabAndAttr
    val unordered = allAttrs.groupMap(_._1) { _._2 }.toSeq
    val undef = unordered.collect {
      case (t, _) if !allTabDefs.map { _._1 }.contains(t) =>
        t
    }
    assert(
      undef.isEmpty,
      s"The following tabs were not defined in the main configuration file: [${undef.mkString(", ")}]"
    )
    allTabDefs.flatMap {
      case (k, name) =>
        unordered.find(_._1 == k).map {
          case (_, attrs) => Tab(name = name, attrs = attrs)
        }
    }
  }
}
