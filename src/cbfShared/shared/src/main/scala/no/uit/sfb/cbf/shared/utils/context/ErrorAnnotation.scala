package no.uit.sfb.cbf.shared.utils.context

case class ErrorAnnotation(message: String, loc: Option[ErrorLoc] = None) {
  lazy val asString = loc match {
    case Some(location) =>
      s"${location.asString}: $message"
    case None =>
      message
  }
}
