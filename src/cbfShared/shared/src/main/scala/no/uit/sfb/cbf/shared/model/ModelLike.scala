package no.uit.sfb.cbf.shared.model

import no.uit.sfb.cbf.shared.utils.context.{ErrorObject, ParsingContext}

trait ModelLike[T] {

  /**
    * Build models from a whole tsv file content
    * We expect headers at the first line and data line by line
    *
    * @param str
    * @return
    */
  def fromTsv(
    str: Iterator[String]
  ): (ParsingContext, Iterator[Either[ErrorObject, (T, Seq[String])]])

  /**
    * Represent models as TSV (header as first line)
    *
    * @param it
    * @return
    */
  def toTsv(s: Seq[T]): String

  def toTsv(it: Iterator[T]): Iterator[String]

  /**
    * Build model from one record (JSON)
    *
    * @param str
    * @return
    */
  def fromSingleJson(str: String): T

  /**
    * Build models from an array of records (JSON)
    *
    * @param str
    * @return
    */
  def fromJson(str: String): Seq[T]

  /**
    * Represent models as JSON (array)
    *
    * @param it
    * @return
    */
  def toJson(it: Iterator[T]): Iterator[String]
}
