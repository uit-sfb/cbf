package no.uit.sfb.cbf.shared.utils

import scala.util.{Failure, Success, Try}

/** An Iterator which executes callback when hasNext returns false
  *
  * Note: Callback needs to be idempotent as hasNext may be called multiple times
  * */
class IteratorWithCallback[T](it: Iterator[T], callback: => Unit)
    extends Iterator[T] {
  override def hasNext: Boolean = {
    Try {
      it.hasNext
    } match {
      case Success(true) =>
        true
      case Success(false) =>
        callback
        false
      case Failure(e) =>
        //With complex iterators such as PartitionIterator, it may happen that hasNext is called multiple times after the callback has been executed
        //Note: cannot use logger.info since this code is included in js code too
        println(
          s"IteratorWithCallback got exception '${e.getMessage}' (this may not indicate an error)."
        )
        callback
        false
    }
  }

  override def next(): T = {
    if (hasNext) {
      Try {
        it.next()
      } match {
        case Success(v) =>
          v
        case Failure(e) =>
          callback
          throw e

      }
    } else
      throw new NoSuchElementException()
  }
}
