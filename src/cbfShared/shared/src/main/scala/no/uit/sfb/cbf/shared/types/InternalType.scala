package no.uit.sfb.cbf.shared.types

import java.time.LocalDate
import java.time.format.DateTimeFormatter
import scala.util.{Failure, Success, Try}

sealed trait InternalType {
  def ref: String
  def name: String
  def definition: String
  def valid(str: String): Option[String]
  //None -> allow user to define the resolver list he wants
  //Some(Seq()) -> no resolvers allowed
  //Some(Seq(...)) -> resolver list is locked with the specified items.
  def overrideResolverList: Option[Seq[String]] = Some(Seq())
  //None: Allow user to select the setting
  //Some(...): lock the setting to the specified value
  final def forceResolvers: Option[Boolean] = overrideResolverList.map {
    _.nonEmpty
  }
  def contains(str: String): Boolean
  def unapply(str: String): Option[InternalType]

  //If true, the use of this type implies the automatic flag should be set
  def impliesAutomatic: Boolean = false
  //If true, the use of this type implies the virtual flag should be set
  def impliesVirtual: Boolean = false
  //If true, this type may be used by at most one attribute
  def unique: Boolean = false
}

trait FixTypeLike extends InternalType {
  final def contains(str: String) = ref == str
  def unapply(str: String): Option[FixTypeLike] =
    if (contains(str)) Some(this) else None
}

case object NoType extends FixTypeLike {
  val ref = ""
  val name = "Free text"
  val definition = "Free text. Cannot be plotted."
  def valid(str: String): Option[String] = None
}

case object Num extends FixTypeLike {
  val ref = "num"
  val name = "Numeric"
  val definition = "Any integer or decimal values."
  def valid(str: String): Option[String] = {
    str.toDoubleOption match {
      case Some(_) =>
        None
      case None =>
        Some(s"Value '$str' is not a number.")
    }
  }
}

case object IntAll extends FixTypeLike {
  val ref = "num:int"
  val name = "Integer"
  val definition = "Positive or negative integer."
  def valid(str: String): Option[String] = {
    str.toLongOption match {
      case Some(_) =>
        None
      case None =>
        Some(s"Value '$str' is not an integer.")
    }
  }
}

case object IntPlus extends FixTypeLike {
  val ref = "num:int+"
  val name = "Positive integer"
  val definition = "Null or positive integer."
  protected val reg = "[+]?\\d+".r
  def valid(str: String): Option[String] = {
    str.toLongOption match {
      case Some(l) if l >= 0 =>
        None
      case _ =>
        Some(s"Value '$str' is not a positive integer.")
    }
  }
}

case object LatLon extends FixTypeLike {
  val ref = "latlon"
  val name = "Latitude longitude"
  val definition =
    "Latitude longitude expressed in Decimal degrees (dd). Ex: '21.3069,-157.85848'. Used in the Map view."
  protected val reg = "([-+]?\\d+(?:\\.\\d+)?),([-+]?\\d+(?:\\.\\d+)?)".r

  def valid(str: String): Option[String] = {
    str match {
      case reg(lat, lon) =>
        Try {
          if (lat.toFloat <= -90.0 ||
              lat.toFloat > 90.0)
            Some(
              s"Value '$str' is not of type '$name' (latitude should be between -90 and 90)."
            )
          else if (lon.toFloat <= -180.0 ||
                   lon.toFloat > 180.0)
            Some(
              s"Value '$str' is not of type '$name' (longitude should be between -180 and 180)."
            )
          else
            None
        }.getOrElse(
          Some(
            s"Value '$str' is not of type '$name' (regexp matched, but an exception was thrown)."
          )
        )
      case _ =>
        Some(s"Value '$str' is not of type '$name' (regexp: '$reg').")
    }
  }
  override val overrideResolverList = Some(Seq("gmap"))
}

case object Date extends FixTypeLike {
  val ref = "date"
  val name = "Date"
  val definition =
    "Date expressed as 'YYYY', or 'YYYY-MM' or 'YYYY-MM-dd'. Ex: '2020-03-16'."
  protected val reg1 = "(\\d{4})-(\\d{2})-(\\d{2})".r
  protected val reg2 = "(\\d{4})-(\\d{2})".r
  protected val reg3 = "(\\d{4})".r

  def valid(str: String): Option[String] = {
    str match {
      case reg1(yyyy, mm, dd) =>
        Try { LocalDate.parse(str, DateTimeFormatter.ISO_LOCAL_DATE) } match {
          case Success(_) =>
            None
          case Failure(e) =>
            Some(s"Value '$str' is not of type '$name' (${e.getMessage}).")
        }
      case reg2(yyyy, mm) =>
        if (yyyy.toInt >= 1000 &&
            yyyy.toInt < 2100 &&
            mm.toInt <= 12)
          None
        else
          Some(
            s"Value '$str' is not of type '$name' (regexp matched, but other condition failed)."
          )
      case reg3(yyyy) =>
        if (yyyy.toInt >= 1000 &&
            yyyy.toInt < 2100)
          None
        else
          Some(
            s"Value '$str' is not of type '$name' (regexp matched, but other condition failed)."
          )
      case _ =>
        Some(s"Value '$str' is not of type '$name' (all regexps failed).")
    }
  }
}

case object Semver extends FixTypeLike {
  val ref = "cat:semver"
  val name = "Semver"
  val definition =
    "Semantic version loosely defined as a series of integers separated by dots. Ex: '2.4'."
  protected val reg = "([0-9]+)(\\.([0-9]+))*$".r

  def valid(str: String): Option[String] = {
    if (reg.matches(str))
      None
    else
      Some(s"Value '$str' is not of type '$name' (regexp: '$reg').")
  }
}

//A term belonging to a (finite) collection
case object Term extends FixTypeLike {
  val ref = "cat"
  val name = "Term"
  val definition = "Term belonging to one or more ontologies."

  def valid(str: String): Option[String] = {
    if (str.nonEmpty)
      None
    else
      Some(s"A value of type '$name' may not be empty.")
  }
  override val overrideResolverList = None
}

//Essentially the same as Term except that Accession cannot be plotted
case object Accession extends FixTypeLike {
  val ref = "acc"
  val name = "Accession"
  val definition =
    "Unique identifiers from external databases. Cannot be plotted."

  def valid(str: String): Option[String] = {
    if (str.nonEmpty)
      None
    else
      Some(s"A value of type '$name' may not be empty.")
  }
  override val overrideResolverList = None
}

trait StarTypeLike extends InternalType {
  def label: String
  def shortRef: String //Should finish WITHOUT :
  def shortName: String
  final lazy val ref = s"$shortRef:$label"
  final lazy val name = s"$shortName [$label]"
  def contains(str: String) = str.startsWith(s"$shortRef:")
  def unapply(str: String): Option[StarTypeLike]
  def customFields: Map[String, CustomTypeFieldDescriptor] = Map()
  def valid(str: String): Option[String] = {
    if (str.nonEmpty)
      None
    else
      Some(s"A value of type '$name' may not be empty.")
  }
}

case class Enum(label: String = "*") extends StarTypeLike {
  val shortRef = "cat:enum"
  val shortName = "Enumeration"
  val definition = "Strict set of allowed terms."

  //Cannot be factorized
  def unapply(str: String): Option[StarTypeLike] =
    if (contains(str)) Some(copy(str.replace(s"$shortRef:", ""))) else None

  override def customFields = Map(
    "terms" -> CustomTypeFieldDescriptor(
      "Enumeration terms (one per line).",
      multiline = true
    )
  )
}

case class GazLocation(label: String = "*") extends StarTypeLike {
  val shortRef = "cat:geo"
  val shortName = "Location (GAZ)"
  val definition =
    "Location provided using the GAZ ontology and used by the Geo view."

  //Cannot be factorized
  def unapply(str: String): Option[StarTypeLike] =
    if (contains(str)) Some(copy(str.replace(s"$shortRef:", ""))) else None

  override val overrideResolverList = Some(Seq("gaz"))

  override def customFields = Map(
    "topojson" -> CustomTypeFieldDescriptor(
      "URL/path of topojson file",
      "/geo/countries.topojson"
    ),
    "projection" -> CustomTypeFieldDescriptor(
      "Map projection",
      "naturalEarth1",
      Seq("naturalEarth1", "mercator")
    )
  )
}

case class DbLinkDirect(label: String = "*") extends StarTypeLike {
  val shortRef = "dblink:direct"
  val shortName = "Db link (direct)"
  val definition =
    "Cross-dataset link denoting a direct parenting/affiliation relationship."

  //Cannot be factorized
  def unapply(str: String): Option[StarTypeLike] =
    if (contains(str)) Some(copy(str.replace(s"$shortRef:", ""))) else None

  override val unique = true
}

case class DbLinkIndirect(label: String = "*") extends StarTypeLike {
  val shortRef = "dblink:indirect"
  val shortName = "Db link (indirect)"
  val definition =
    "Cross-dataset link denoting an indirect parenting/affiliation relationship."

  //Cannot be factorized
  def unapply(str: String): Option[StarTypeLike] =
    if (contains(str)) Some(copy(str.replace(s"$shortRef:", ""))) else None

  override val impliesAutomatic = true
  override val impliesVirtual = true
  override val unique = true
}

object InternalType {
  val fixTypes: Seq[FixTypeLike] =
    Seq(NoType, Num, IntAll, IntPlus, LatLon, Date, Semver, Term, Accession)
  val starTypes: Seq[StarTypeLike] =
    Seq(Enum(), GazLocation(), DbLinkDirect(), DbLinkIndirect())

  def apply(ref: String): Option[InternalType] =
    (fixTypes ++ starTypes).flatMap { _.unapply(ref) }.headOption
}
