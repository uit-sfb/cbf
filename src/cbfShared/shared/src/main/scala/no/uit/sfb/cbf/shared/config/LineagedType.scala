package no.uit.sfb.cbf.shared.config

case class LineagedType(lineage: List[String]) {
  val primary = nAry(1)
  val secondary = nAry(2)
  val tertiary = nAry(3)

  def nAry(n: Int): String = lineage.drop(n - 1).headOption.getOrElse("")

  def isSubtypeOf(other: String): Boolean = isSubtypeOf(LineagedType(other))
  def isSubtypeOf(other: LineagedType): Boolean =
    lineage.startsWith(other.lineage)

  def factorize(other: String) = {
    val otherLt = LineagedType(other)
    if (isSubtypeOf(otherLt))
      LineagedType(lineage.drop(otherLt.lineage.length))
    else
      this
  }

  val asString = lineage.mkString(":")
}

object LineagedType {
  def apply(str: String): LineagedType = {
    LineagedType(str.split(':').toList)
  }
}
